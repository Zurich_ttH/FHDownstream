#!/bin/bash
MAKECARDS=true
RUNCOMBINE=true
RUNPLOTS=true
RUNASIMOV=true
RUNDATA=false
RUNBKGFIT=true

GENERALSETTINGS=" -M FitDiagnostics  -m 125 --cminDefaultMinimizerStrategy 0 --cminPreScan  --saveNormalizations --saveShapes --saveOverallShapes"
# --saveWithUncertainties "
RSETTINGSREST=" --rMin -5 --rMax 5 "
RSETTINGS3B=" --rMin -15 --rMax 15 "
# --saveShapes --saveWithUncertainties
TOL=" --cminDefaultMinimizerTolerance 0.001 --setRobustFitTolerance 0.001 --setCrossingTolerance 0.001"
POSTFIX="_v3p3_v4_tol0p001_robTolp001_crossTolp001"


if [ "${MAKECARDS}" = true ]; then

  combineCards.py \
      fh_j7_t4_DNN_Node0=fh_j7_t4_DNN_Node0.txt \
      fh_j8_t4_DNN_Node0=fh_j8_t4_DNN_Node0.txt \
      fh_j9_t4_DNN_Node0=fh_j9_t4_DNN_Node0.txt \
     > fh_t4_DNN_Node0.txt


  text2workspace.py fh_t4_DNN_Node0.txt
  
  echo -e "text2workspace.py fh_j7_t4_DNN_Node0.txt"
  text2workspace.py fh_j7_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_j8_t4_DNN_Node0.txt"
  text2workspace.py fh_j8_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_j9_t4_DNN_Node0.txt"
  text2workspace.py fh_j9_t4_DNN_Node0.txt
fi

echo "Finished text2Workspace"
DIFFNUI="/work/koschwei/slc7/ttH/LegacyRun2/combine/CMSSW_10_2_13/src/HiggsAnalysis/CombinedLimit/test/diffNuisances.py"
if [ "${RUNCOMBINE}" = true ]; then
    echo "Running fits"
    if [ "${RUNDATA}" = true ]; then
	echo "================================================ RUNNING DATA ================================================"
	echo -e "combine ttH_hbb_13TeV_2017_fh_7j_4t.root -n _7j4t${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST}"
	combine ttH_hbb_13TeV_2017_fh_7j_4t.root -n _7j4t${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} &> combine_7j4t${POSTFIX}.out.txt
	cat combine_7j4t${POSTFIX}.out.txt
	echo -e "combine ttH_hbb_13TeV_2017_fh_8j_4t.root -n _8j4t${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST}"
	combine ttH_hbb_13TeV_2017_fh_8j_4t.root -n _8j4t${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} &> combine_8j4t${POSTFIX}.out.txt
	cat combine_8j4t${POSTFIX}.out.txt
	echo -e "combine ttH_hbb_13TeV_2017_fh_9j_4t.root -n _9j4t${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST}"
	combine ttH_hbb_13TeV_2017_fh_9j_4t.root -n _9j4t${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} &> combine_9j4t${POSTFIX}.out.txt
	cat combine_9j4t${POSTFIX}.out.txt
	echo -e "combine ttH_hbb_13TeV_2017_fh_4b.root -n fh_4b_${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST}"
	combine ttH_hbb_13TeV_2017_fh_4b.root -n _fh_4b${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} &> combine_fh_4b${POSTFIX}.out.txt
	cat combine_fh_4b${POSTFIX}.out.txt


	echo -e "python ${DIFFNUI} --all fitDiagnostics_7j4t${POSTFIX}.root &> diffNui_7j4t${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_7j4t${POSTFIX}.root &> diffNui_7j4t${POSTFIX}.txt
	echo -e "python ${DIFFNUI} --all fitDiagnostics_8j4t${POSTFIX}.root &> diffNui_8j4t${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_8j4t${POSTFIX}.root &> diffNui_8j4t${POSTFIX}.txt
	echo -e "python ${DIFFNUI} --all fitDiagnostics_9j4t${POSTFIX}.root &> diffNui_9j4t${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_9j4t${POSTFIX}.root &> diffNui_9j4t${POSTFIX}.txt


	echo -e "python ${DIFFNUI} --all fitDiagnostics_fh_4b${POSTFIX}.root &> diffNui_fh_4b${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_fh_4b${POSTFIX}.root &> diffNui_fh_4b${POSTFIX}.txt
    fi

    if [ "${RUNASIMOV}" = true ]; then


	echo "=============================================== RUNNING ASIMOV ==============================================="
	echo "============================================= SIGNAL + BACKGROUND ============================================"
	echo "combine fh_j7_t4_DNN_Node0.root -n _asimov_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_j7_t4${POSTFIX}.out.txt"
	combine fh_j7_t4_DNN_Node0.root -n _asimov_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_j7_t4${POSTFIX}.out.txt
	cat combine_asimov_sig_j7_t4${POSTFIX}.out.txt
	echo "combine fh_j8_t4_DNN_Node0.root -n _asimov_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_j8_t4${POSTFIX}.out.txt"
	combine fh_j8_t4_DNN_Node0.root -n _asimov_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_j8_t4${POSTFIX}.out.txt
	cat combine_asimov_sig_j8_t4${POSTFIX}.out.txt
	echo "combine fh_j9_t4_DNN_Node0.root -n _asimov_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_j9_t4${POSTFIX}.out.txt"
	combine fh_j9_t4_DNN_Node0.root -n _asimov_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_j9_t4${POSTFIX}.out.txt
	cat combine_asimov_sig_j9_t4${POSTFIX}.out.txt
	echo "combine fh_t4_DNN_Node0.root -n _asimov_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_comb${POSTFIX}.out.txt"
	combine fh_t4_DNN_Node0.root -n _asimov_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_sig_comb${POSTFIX}.out.txt
	cat combine_asimov_sig_comb${POSTFIX}.out.txt

	if [ "${RUNBKGFIT}" = true ]; then
	    echo "=============================================== BACKGROUND ONLY =============================================="
	    echo "combine fh_j7_t4_DNN_Node0.root -n _asimov_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_j7_t4${POSTFIX}.out.txt"
	    combine fh_j7_t4_DNN_Node0.root -n _asimov_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_j7_t4${POSTFIX}.out.txt
	    cat combine_asimov_bkg_j7_t4${POSTFIX}.out.txt
	    echo "combine fh_j8_t4_DNN_Node0.root -n _asimov_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_j8_t4${POSTFIX}.out.txt"
	    combine fh_j8_t4_DNN_Node0.root -n _asimov_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_j8_t4${POSTFIX}.out.txt
	    cat combine_asimov_bkg_j8_t4${POSTFIX}.out.txt
	    echo "combine fh_j9_t4_DNN_Node0.root -n _asimov_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_j9_t4${POSTFIX}.out.txt"
	    combine fh_j9_t4_DNN_Node0.root -n _asimov_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_j9_t4${POSTFIX}.out.txt
	    cat combine_asimov_bkg_j9_t4${POSTFIX}.out.txt
	    echo "combine fh_t4_DNN_Node0.root -n _asimov_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_comb${POSTFIX}.out.txt"
	    combine fh_t4_DNN_Node0.root -n _asimov_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_bkg_comb${POSTFIX}.out.txt
	    cat combine_asimov_bkg_comb${POSTFIX}.out.txt
	fi
	
	echo "============================================== RUNNING DIFFNUI ==============================================="
	echo "============================================= SIGNAL + BACKGROUND ============================================"
	echo "python ${DIFFNUI} --all fitDiagnostics_asimov_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_sig_7j4t${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_asimov_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_sig_7j4t${POSTFIX}.txt
	echo "python ${DIFFNUI} --all fitDiagnostics_asimov_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_sig_8j4t${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_asimov_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_sig_8j4t${POSTFIX}.txt
	echo "python ${DIFFNUI} --all fitDiagnostics_asimov_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_sig_9j4t${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_asimov_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_sig_9j4t${POSTFIX}.txt
	echo "python ${DIFFNUI} --all fitDiagnostics_asimov_sig_comb${POSTFIX}.root &> diffNui_asimov_sig_comb${POSTFIX}.txt"
	python ${DIFFNUI} --all fitDiagnostics_asimov_sig_comb${POSTFIX}.root &> diffNui_asimov_sig_comb${POSTFIX}.txt


	if [ "${RUNBKGFIT}" = true ]; then
	    echo "=============================================== BACKGROUND ONLY =============================================="
	    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_bkg_7j4t${POSTFIX}.txt"
	    python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_bkg_7j4t${POSTFIX}.txt
	    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_bkg_8j4t${POSTFIX}.txt"
	    python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_bkg_8j4t${POSTFIX}.txt
	    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_bkg_9j4t${POSTFIX}.txt"
	    python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_bkg_9j4t${POSTFIX}.txt
	    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_comb${POSTFIX}.root &> diffNui_asimov_bkg_comb${POSTFIX}.txt"
	    python ${DIFFNUI} --all fitDiagnostics_asimov_bkg_comb${POSTFIX}.root &> diffNui_asimov_bkg_comb${POSTFIX}.txt
	fi
    fi
fi

PLOTPULLS="plotDiffNuisances.py"

OUTPUT="$(pwd)/"
ISDATA="--isData"


if [ "${RUNPLOTS}" = true ]; then
  cd /work/koschwei/slc7/ttH/LegacyRun2/v3p3/CMSSW_10_2_15_patch2/src/TTH/FHDownstream/Datacards/
  echo "========================================== RUNNING NUICANCE PLOTS  ==========================================="
 
  if [ "${RUNDATA}" = true ]; then
      
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_7j3t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_7j3t${POSTFIX}.txt --output pulls_7j3t${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_7j3t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_7j3t${POSTFIX}.txt --output pulls_7j3t${POSTFIX} ${ISDATA}
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_7j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_7j4t${POSTFIX}.txt --output pulls_7j4t${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_7j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_7j4t${POSTFIX}.txt --output pulls_7j4t${POSTFIX} ${ISDATA}
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_8j3t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_8j3t${POSTFIX}.txt --output pulls_8j3t${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_8j3t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_8j3t${POSTFIX}.txt --output pulls_8j3t${POSTFIX} ${ISDATA}
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_8j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_8j4t${POSTFIX}.txt --output pulls_8j4t${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_8j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_8j4t${POSTFIX}.txt --output pulls_8j4t${POSTFIX} ${ISDATA}
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_9j3t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_9j3t${POSTFIX}.txt --output pulls_9j3t${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_9j3t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_9j3t${POSTFIX}.txt --output pulls_9j3t${POSTFIX} ${ISDATA}
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_9j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_9j4t${POSTFIX}.txt --output pulls_9j4t${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_9j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_9j4t${POSTFIX}.txt --output pulls_9j4t${POSTFIX} ${ISDATA}


      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_fh${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_fh${POSTFIX}.txt --output pulls_fh${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_fh${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_fh${POSTFIX}.txt --output pulls_fh${POSTFIX} ${ISDATA}
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_fh_3b${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_fh_3b${POSTFIX}.txt --output pulls_fh_3b${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_fh_3b${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_fh_3b${POSTFIX}.txt --output pulls_fh_3b${POSTFIX} ${ISDATA}
      echo -e "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_fh_4b${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_fh_4b${POSTFIX}.txt --output pulls_fh_4b${POSTFIX} ${ISDATA}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_fh_4b${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_fh_4b${POSTFIX}.txt --output pulls_fh_4b${POSTFIX} ${ISDATA}
  fi

  
  if [ "${RUNASIMOV}" = true ]; then

      echo "=========================================== RUNNING ASIMOV PLOTS  ============================================"
      echo "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_7j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_7j4t${POSTFIX}.txt --output pulls_7j4t${POSTFIX}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_7j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_7j4t${POSTFIX}.txt --output pulls_7j4t${POSTFIX}
      echo "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_8j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_8j4t${POSTFIX}.txt --output pulls_8j4t${POSTFIX}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_8j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_8j4t${POSTFIX}.txt --output pulls_8j4t${POSTFIX}
      echo "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_9j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_9j4t${POSTFIX}.txt --output pulls_9j4t${POSTFIX}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_9j4t${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_9j4t${POSTFIX}.txt --output pulls_9j4t${POSTFIX}
      echo "python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_comb${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_comb${POSTFIX}.txt --output pulls_comb${POSTFIX}"
      python ${PLOTPULLS} --inSplusB ${OUTPUT}diffNui_asimov_sig_comb${POSTFIX}.txt --inBOnly ${OUTPUT}diffNui_asimov_bkg_comb${POSTFIX}.txt --output pulls_comb${POSTFIX}
  fi
  cd -
fi

