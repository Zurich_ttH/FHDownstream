# Instructions and notes for running the RunII2017 Analysis

## 1 Running the nTupler

### 1.1 Running datasets on crab

All scripst and configuration are in `$CMSSW_BASE/src/TTH/MEAnalysis/crab_nano`. The dataset are defined multiple `.json` files. For this analysis all files with the `CMSSW_9X` prefix are important.      
The crab submission is done with `multicrab_94X.py`. The scirpt required to flag to be set in the command line options:
- `--tag` : This will be the name used for the folder in crab_projects, as identifier in the task name and subfolder on the SE
- `--workflow` : In the script there are different workflows hardcoded (setting the FW confiuration and the run datasets). The important ones for the FH analyisis are `hadronic` and `hadronic_nome`

For bulk interaction with the crab jobs the `FHDownstream` repository provides the srcipt `crabTools.py` which is linked to the `crab_nano` folder in the setup process.

## 2 Cross sections
For some samples no theory prediction is available. The get the cross sections for such samples. Either consult the [XSDB](https://cms-gen-dev.cern.ch/xsdb/) or run the calculation yourself using the [GenXSecAnalyzer](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HowToGenXSecAnalyzer#Automated_scripts_to_compute_the)

One examples (on lxplus) would be

```bash
cern-get-sso-cookie -u https://cms-pdmv-dev.cern.ch/mcm/ -o ~/private/dev-cookie.txt --krb --reprocess
cern-get-sso-cookie -u https://cms-pdmv.cern.ch/mcm/ -o ~/private/prod-cookie.txt --krb --reprocess
source /afs/cern.ch/cms/PPD/PdmV/tools/McM/getCookie.sh

voms-proxy-init -voms cms

cmsrel CMSSW_9_4_9
cd CMSSW_9_4_9/src
cmsenv
scram b -j8
cd ../../

# download the genproduction reposotory
git clone https://github.com/cms-sw/genproductions.git
cd genproductions/test/calculateXSectionAndFilterEfficiency

./calculateXSectionAndFilterEfficiency.sh -f datasets.txt -c RunIIFall17 -d MINIAODSIM -n 1000000
```

The `datasets.txt` file contains a list of DAS request names that should be run.

## 3 Procuding the skims
Use the `gc/confs/projectSkimFH.conf`. Might be necessary to update `gc/scripts/projectSkimFH.py` (if variables changed) or `gc/executables/projectSkimFH.sh` (if groups have to be disabled/enabled).

The GC jobs are configured to write the Skim to the T3SE. In order to merge the job output files, please use `utils/haddSE.py`:

| option        | description                                                                                                                                                               |
| ------        | -----------                                                                                                                                                               |
| `--type`      | Can be *Single* or *Multi*. Use *Multi* if `--dir` is pointed to full GC output folder (`path/to/se/GCXXXXXX`). Use *Single* if pointed to a single dataset output folder |
| `--dir`       | Point to GC output folder on SE                                                                                                                                           |
| `--altOutput` | Point to desired output folder on SE. Without this the output is written to the GC output folder that also containes the folders for the datasets                         |
| `--skip`      | Define multiple substring (separated by whitespace)  that will be used to exclude dataset                                                                                 |


Furthermore there is the `--split` option which will cause total hadd to be split in multiple steps (defined by `--splitting`). This can be necessary if the dataset contains so many files that the command will be longer than the `subprocess.Popen()` can handle
Also keep in mind that it is not possible to produce a root file with hadd that is larger than 100GB. There is currently no solution to this implemented. It issuggested to skim more variable to get an overall smaller output.

To create the final data skim (*JetHT* and *BTagCSV* skims are set up in a way so there is no double counting if the trigger selection in specified) use
```bash
cd /scratch/$USER
export PATHTOSKIM=/pnfs/psi.ch/cms/trivcat/store/user/path/to/skims #Set this according to your output
hadd data.root root://t3dcachedb.psi.ch/$PATHTOSKIM/JetHT.root root://t3dcachedb.psi.ch/$PATHTOSKIM/BTagCSV.root
xrdcp data.root root://t3dcachedb.psi.ch/$PATHTOSKIM/data.root 
```

**IMPORTANT**: In the current version new b-tagging scale factors and the corresponding normalization will be added to each skim. This is configured in `FHDownstream/utils/addbTagWeights.py`. If this is not desired in can be commented out in the shell script (in `FHDownstream/gc/scripts/`) corresponding to the skim you are running.

## 4 Corrections and reweighting
This sections details all necessary steps for calculating corrections and reweightings from the finished nTuples. Remember to update all GC configs to the correct set of dataset filess

### 4.1 b-tagging normalizations
In order to make sure the b-tag reweighting does not chnage the normaization a nJets dependent noramlization factor is derived (before anaylsis cuts are apply). All jets with `pT > 30 GeV` and an `abs(eta) < 2.4` are considered.

The systeamtics and the input .csv is currently hardcoded in `gc/scripts/getWeightNormalizations.py` and `gc/scripts/classes.h`. Update and change if needed.

```bash
cd gc/
./go.py -c confs/getWeightNormalizations.conf
#Hadd all output files. Use utiles/haddAllFiles.sh -> copy to gc output directors and run 
#cd back to utils/
python calcbTagNorm.py --input /path/to/GC/output/ --folder some/folder/name/ #Input is defined in gc-config, folder will be created 
```

### 4.2 nGen in current dataset
Use the GC script `gc/conf/countFH.conf`. The gen cound will be generated considering `genWeight` and `puWeight` 

```bash
cd gc/
./go.py -c confs/countFH.conf
#Hadd all output files. Use utiles/haddAllFiles.sh -> copy to gc output directors and run 
#cd back to utils/
python getReCalcCounts.py /path/to/GC/output/
```

This will print a Markdown table in the terminal

### 4.3 Trigger Scale factor
For the trigger scale factor nTuples with a SL selection are required. The MEAnalysis config for this is `MEAnalysis/python/cfg_FH_val.py`.

```bash
cd gc/
./go.py -c confs/MEAnalysis_TriggerSF.conf # Or MEAnalysis_SLValidation.conf if you also want the minor backgrounds
#What for GC to finish
# cd to utils
python getFilesCrab.py --outpath . --path /pnfs/path/to/output --onlytthbb # --onlytthbb will disable the counting done on the nanoAOD files
# cd back to gc. We need skims of those files
# update the dataset in confs/projectSkimTriggerSF.conf
./go.py -c confs/projectSkimTriggerSF.conf
#wait... go to utils once finished
python haddSE.py .....
```

#### 4.3.1 Getting the SF
The scripts required for the Trigger SF are located in `FHDownstream/Plotting`.

Use the `Trigger_SFs.py` scripts. Update the nGen numbers and dataset paths in the `TriggerSF()` function. Set a nice name with the `--tag` flag.

1. Run the script with w/o any other flags to get efficiency plots with offline selection
2. Run with `--looseOfflinecuts` to get the SF with a looser selection. This will basically add one additional bin below the usual offline cut. The SF form this step will be used for the actual SF calculation in further steps.
3. Update the path to the SF file in `classes.h` and run with `--closure` to get closure check plots with the new SF

With the `plotSFslices.py` you can plot 2D histograms (x: HT, y: 6th pt) for each bin in CSV of the 3D SF. Update the `SFFile` variable and set `afterEdit = False` (if not updated. see next paragraph).

If the errors are weird or there is some value that is obviouly wrong due to statistics (usually this happens in the bins with large nBCSV --> use output of `plotSFslices.py` to checkt this!) it cna be editied with the `editSFFile.py` script. It is an interactiv scripts. Just follow what it asks :D

**Copy the stable version to `MEAnalysis/data/` and update the name in `MEAnalysis/python/MEAnalysis_cfg_heppy.py`.**

### 4.4 Update the Helper functions
Before going further `FFHDownstream/Plotting/Helper.py` has to be update with nGen, xsec, path etc.

### 4.5 CR Correction
Cacluation of the CR correction factor for the QCD estimate. All Values are set in the configuration file that is used for the correction. An example can be found here: `FHDownstream/data/getCorrection_LegacyRun2_2017.cfg`. Make sure that the xsec, nGen, skim-folder, b-tag WPs and b-tag variables are set correctly.

Run `FHDownstream/Plotting/CRcorrections/getCorrection.py`. Switch between data and MC by passing the `--runData` flag. Also get the additionl fit with a second eta fit.     
__Note:__ Currently the corresponding flag `redoEta` in the config is not working. Add `eta2` to the `fitOrder` and run again with a different tag.

If both are run, create `btagCorrections.h` in `Plotting/CRcorrections` and copy the correction functions from the output folder into that file with names `btagCorrMC`, `btagCorrData`.

__TODO:__    
_[DEPRECATED]_ For the sideband validation an additional correction has be calculated. This is done by adding `bML` to the tag. This will chnage the btag High and Low values so that the corrected jets are not between loose and medium but between loose and mediumloose. This new cut is estimated from skims by splitting the number of jets between loose and medium info half. The correction from this is used in the QCD_estimate.py script.

### 4.6 HT reweighting [TO BE EVALUATED]
This is necessary because we observe larger differences in the HT disctribution after estimating the ddQCD than for other control variables. In order to derive the SF, run a ddQCD estimation with `FHDownstream/Plotting/QCD_estimate_v2.py` in the `ge7j4t` category for the `6jHT` variable. See Section [5.1](#5.1-QCD-estimation) for more information on the script

Update the files in `HTReweighting/get_ddQCD_HTweights.py` and run the script. This will printout the weights in a c++ like form (`forCpp = True`) or a python like form (`forCpp = False`).

Update in functions.h

```cpp
float HTWeight(float HT, int threeb){
  if(HT<500.0)......
}
```

Run `FHDownstream/Plotting/QCD_estimate_v2.py` again with `applyHTWeight : true` option in the config for closure plots.

### 4.7 Reweighting for PDF, muF and muR
The scalefactors were provided by KIT since they are not dependent on the framework. If we need to calculate them for MEAnalysis we need to add calculation of the final up and down PDF weight to nanoAOD or run the sparsinator on samples with passAll (without MEM!).

These weights are only needed for 

- ttHbb
- ttHnonbb
- TTTo*

## 5 Validation
### 5.1 QCD estimation
The script for producing the ddQCD valiadtion plots from skims is`FHDownstream/Plotting/QCD_estimate_v2.py`. The script is fully setup by configuration files like `FHDownstream/data/QCD_estimate_LegacyRun2_2017_Baseline_v1_DNN_ttHbbOdd.cfg`.


### 5.2 Sideband QCD Estimation
For the sideband validation CR2+CR (xRegions) and CR+VR (yRegions) are split in a similar way to the general strategya

Update/copy-and-change config so the regions are selection differently

####  5.2.1 yRegions
TODO...

#### 5.2.2 xRegions
**Make sure the functions for the sideband correction (as described in 4.5) are set properly.**

TODO...

### 5.3 QCD estimation for VR fitDiagnostics

TODO...

## 6 Sparsinator and getting the final files for making datacards
### 6.1 Code preparation
Make sure the setting in the sparsinator code are correct.
Check:

- that bTag Normalization weights are correct for current version of DeepFlav SF
- the CRCorrection functions `btagCorrData`, `btagCorrMC`, `btagCorrDataUp` and `btagCorrMCUp` are set according to the results from 4.5
- <del>WP (loose and medium) are correct in `get_CRCorr`</del> _(Moved to config)_
- if new weights for the QCD MC nJets reweighting in `QCDReweighting` are needed (only necessary for preselection and atm 2017 MC)
- Factors in `getNormFactor` for PDF, muF and muR weights
- <del>HTReweighing factors are according to new estimation in 4.6</del> _(Moved to config)_
- if spltting of FH trigger by sample still correct in `pass_HLT_fh` (in 2018 they should be in the same dataset again)
- if all necessary weights are defined weight lists for data (only HTReweighting and CRCorrection in CR and CR2) and mc

### 6.2 Prepping the configs
This is the first time most of the config actually matters. Make sure that the **dataset paths**, **xsec** and **ngen** are set correctly. Also it is important to put all processes you want to run in the `base_processes_fh` list. Otherwise the output files will be empty even if you run the sparsinator over the files. 

For the FH anylsis some options are set in `MEAnalysis/data/config_FH.cfg` (it overwrites the `default.cfg`). Check in both if you are unsur
e.
There are also some options directly effecting the sparsinor in the `[sparsinator]` section. Important for FH are:

- `do_hadronic` : Has to be `true`. Otherwise the "general" EventModel will be used.
- For using the most up-to-date b-tag SF: `reCalcBTagSF`, `reCalcBTagSFType`, `reCalcBTagSFFile`
	-  Currently it is not set up to read the new SF even if they are rerun in MEAnalysis. The EventModel only looks at the nanoAOD ones which are not setup to deal with the special SF of ttH
-  `doCRCorr` : This will completely disable the CRCorrtions functionality. Only really needed for Preselection since it is be construction only applied in CR and CR2
-  `useJESbTagWeights` : If `true` the JES variied b-tag SF will be used in the corresponding shifted distributon instead of the nominal
-  `addJESbTagTemples` : If `true` the JES variied b-tag SF will be saved as templates. Only really needed for debugging or validation
- `btagWPM` : Medium b-tag WP. Needed for DNN and CRCorrection
- `btagWPL` : Loose b-tag WP. Needed for DNN and CRCorrection
- `reCalcPUWeight` : Recalculate the PU weight. Only works with ntuples that also contain the postprocessed nanoAOD. Necessary because of bug in initial Run2 ntuples
- `PUweightEra` : Era for the pu weights (`2016`, `2017`, `2018`)
- `getL1FromNano` : If `true` L1 weights will be taken from the corresponding nanoAOD file. Only works with ntuples that also contain the postprocessed nanoAOD. Necessary because of bug in initial Run2 ntuples
- `FHTriggerSFFile` : File for the Trigger SF

There is also a config for the Preselection region `MEAnalysis/data/config_FH_preSel.cfg` which works essentially the same.

At this point also the systemtics that are considered are set. Check the `[systematics]` section in `default.cfg`. **Only needed for systematics that are taken from the Event model!**

**NEW FOR LEGACY ANALYSIS** CR and SR are implemented as categories that can be simulaneously run in the sparsinator.

### 6.3 Running the sparsinator
The sparsinator need to run over all samples. Use the GC configs `sparse_*.conf`.

### 6.4 Postprocessing the templates files

### 6.4.1 Merging ttbar+*b and creating additional systeamtic
In the datacard there is only one process called `ttbb` that contains what is setup as `ttbarPulsBBbar`, `ttbarPuls2B`,`ttbarPulsB`. They are run separately because we need an additonal systematic where `ttbarPuls2B` is scaled up/down by 50%. 

Use the script `FHDownstream/Datacards/mergeTTtemplates.py` to create the merged historgrams and the uncertainty. Use:

```bash
export SPARSEOUTDIR=/path/to/merged/files/per/samples
python mergeTTtemplates.py --inputs $SPARSEOUTDIR/TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root  $SPARSEOUTDIR/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root $SPARSEOUTDIR/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root --outPostfix merged 
```



#### 6.4.2 Getting the rate parameter for HDAMP and UE
For this you need the sparsinator output of the HDAMP and UE variied samples. Use the script `FHDownstream/Datacards/getPSrates.py`. Updates files and run once for HDAMP and once for UE
	
#### 6.4.3 Adding the ddQCD templates and rate templates [DEPRECATED]

_This is not needed anymore. But since it might come in handy the description will stay here_

Additional the ddQCD histograms have the be added to the SR and VR files. This is done, using the script `FHDownstream/Datacards/Add_ddQCDv2.py`. The script is configured by a config file. Use one of the `.cfg` present in the directory and save you chnages under a new name (for bookkeeping). In the config, the inputfiles (use the `*_new.root` files from step 6.4.1) and the rate systamtics + values are set (there are some other options).

Run the script by passing the config with the `--config` argument.

This will produce a output file (same directory as imput) with an appended `_ddQCD_added` to the name. This file will contain the ddQCD templates as well as the templates for the rate variations in the SR.

Hadd the `_new` file and the `_ddQCD_added` file to get the final template file required for the datacard builder.

### 6.5 Making datacards
Datacards are created from sparsinator output with the script `FHDownstream/Datacards/makeDatacards.py`. It requires are config that sets up all the processes, systematics and rateParamters. It can also rename processes and systematics to make synchrionization with other channels easier.

The datacards are setup to create the ddQCD templates "on-the-fly" by adding CR data and CR MC as separate processes. The CR MC processes are expected to have a rateParamter that is fixed to -1. All CR processes have to free-floating rateParamter. 

## 7 Final plots, yields, etc.
### 7.1 Pulls
Run `diffNuisances.py` from Higgs Combine repo (Setup Combine CMSSW version. Located in `$CMSSW_BASE/src/HiggsAnalysis/CombinedLimit/test/diffNuisances.py`) and pipe output into a file.

Run in Analysis CMSSW version: 
`python $CMSSW_BASE/src/TTH/FHDownstream/Datacards/plotDiffNuisances.py --inSplusB diffNuisance.txt --inBOnly diffNuisance.txt --output pulls`. This will produce horizontal pull plots split into three groups

### 7.2 Discriminator plots
All discriminator plots were created with the common [datacard/limit framework](https://gitlab.cern.ch/ttH/datacards/tree/master) of the ttHbb groups. The plot script is `datacards/PreFitPostFitPlots/PrePostFitPlots.py`.

