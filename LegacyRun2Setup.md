# Legacy Run2 Analysis Setup and reference

## ToDo

- Make b-tagging normalization configurable

## General configuration
One config per year. Each of them is derived from `config_FH.cfg` for common settings. Current reference configues are:

- **2018**: `MEAnalysis/data/config_FH_2018.cfg`
- **2017**: `MEAnalysis/data/config_FH_2017.cfg`
- **2016**: `MEAnalysis/data/config_FH_2016.cfg`

### Important Settings for all years:

- Section: `[sparsinator]`
	- TF loose: `CRCorr : true`
	- General setting: `do_hadronic: true`
	- b-tagging uncertainties: `true` for `useJESbTagWeights` and `addJESbTagTemples`
- Section: `[fhDNNModels]`
	- Only use even ttHbb events: `runEvenTTH`
	- Subfolder for dnn cats: `7jModel`, `8jModel` and `9jModel` (same subfolder names encouraged)


### Important Settings per year:

- Reset all sections for the different dataset with the corresponding
	- Dataset files
	- nGen
- Section: `[general]`
	- nTupler configuration: `mem_python_config`
- Section: `[sparsinator]`
	- b-tagging SF: `reCalcBTagSF`,`reCalcBTagSFType` and `reCalcBTagSFFile`
	- b-tagging normalization: `doBTagNrom`
	- b-tagging: `btagWPM` and `btagWPL` (required for TFLoose correction and DNN)
	- PU weights: `reCalcPUWeight` and `PUweightEra` (only required for 2016 and 2017 v3 ntuples bc. of configuration mistake)
	- L1Prefiring weights: `getL1FromNano` (required for all v3 ntuples)
	- Trigger SF: `FHTriggerSFFile`
- Section: `[fhDNNModels]`
	- Base folder for each year: `base`
- Section: `[MultiJetShapeCorrection]`
	- Set scale factors with `scalefactors`
- In each category section set `control_variables` to DNN input variables for validation

## Configuration files for other tasks

All configuration files should be located in `FHDownstream/data` and subfolders withing this directory.

### CRCorrection

| Era  | File |
|------|------|
| 2016 |      |
| 2017 | [getCorrection\_LegacyRun2\_2017.cfg](https://gitlab.cern.ch/Zurich_ttH/FHDownstream/blob/FullRunII_dev/data/getCorrection_LegacyRun2_2017.cfg) |
| 2018 |      |

### DNN Flattner

located in: `nTupleProcessing/classification/`

| Era  | File |
|------|------|
| 2016 |      |
| 2017 | [Run2Baseline\_2017\_v1.cfg](https://gitlab.cern.ch/Zurich_ttH/FHDownstream/blob/FullRunII_dev/data/nTupleProcessing/classification/Run2Baseline_2017_v1.cfg) |
| 2018 |      |

### Multijet estimation

| Era  | File |
|------|------|
| 2016 |      |
| 2017 | [QCD\_estimate\_LegacyRun2\_2017\_Baseline\_v1\_DNN\_ttHbbOdd.cfg](https://gitlab.cern.ch/Zurich_ttH/FHDownstream/blob/FullRunII_dev/data/QCD_estimate_LegacyRun2_2017_Baseline_v1_DNN_ttHbbOdd.cfg)|
| 2018 |      |

### Datacards

located in: `datacards/`

| Era  | File |
|------|------|
| 2016 |      |
| 2017 | [LegacyRun2\_2017\_Baseline.cfg](https://gitlab.cern.ch/Zurich_ttH/FHDownstream/blob/FullRunII_dev/data/datacards/LegacyRun2_2017_Baseline.cfg) |
| 2018 |      |

