import sys
import os

import ROOT
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging, makeCanvasOfHistos, saveCanvasListAsPDF


if __name__ == "__main__":
    inFile = sys.argv[1]
    
    variations = ["ttbarPlusBBbarMerged__%%CAT%%__DNN_Node0__CMS_ttHbb_UETransplated_ttbarPlusBBbarMerged",
                  "ttbarPlusBBbarMerged__%%CAT%%__DNN_Node0__CMS_ttHbb_HDAMP_ttbarPlusBBbarMerged",
                  "ttbarPlusCCbar__%%CAT%%__DNN_Node0__CMS_ttHbb_HDAMP_ttbarPlusCCbar",
                  "ttbarPlusCCbar__%%CAT%%__DNN_Node0__CMS_ttHbb_UE_ttbarPlusCCbar",
                  "ttbarOther__%%CAT%%__DNN_Node0__CMS_ttHbb_HDAMP_ttbarOther",
                  "ttbarOther__%%CAT%%__DNN_Node0__CMS_ttHbb_UE_ttbarOther"]


    cats = ["fh_j7_t4_CR",
            "fh_j7_t4_SR",
            "fh_j8_t4_CR",
            "fh_j8_t4_SR",
            "fh_j9_t4_CR",
            "fh_j9_t4_SR"]

    validation = True

    if not validation:
        ROOT.gStyle.SetOptStat(0)
        ROOT.gROOT.SetBatch(1)
    
    c1 = ROOT.TCanvas("c1", "c1")
    rFile = ROOT.TFile.Open(inFile)
    for cat in cats:
        for var in variations:

            nom = "__".join(var.split("__")[0:3])
            up = var+"Up"
            down = var+"Down"

            nom = nom.replace("%%CAT%%", cat)
            up = up.replace("%%CAT%%", cat)
            down = down.replace("%%CAT%%", cat)

            print nom
            print up
            print down
            
            hNom = rFile.Get(nom)
            hUp = rFile.Get(up)
            hDown = rFile.Get(down)

            hNom.SetTitle(var.split("__")[3]+" -- "+cat)
            
            hNom.SetLineColor(ROOT.kBlack)
            hUp.SetLineColor(ROOT.kRed)
            hDown.SetLineColor(ROOT.kBlue)

            hNom.Draw()
            hUp.Draw("same")
            hDown.Draw("same")

            c1.Update()

            if validation:
                raw_input("Check %s -- Pre ret for next"%var.replace("%%CAT%%", cat))
            else:
                fileName = inFile.split("/")[-1].replace(".root", "")
                saveCanvasListAsPDF([c1], fileName+"_"+cat+"_"+var.split("__")[3]+"_TRF_TemplateVal.pdf", "out_templateVal/")
            
    
    






