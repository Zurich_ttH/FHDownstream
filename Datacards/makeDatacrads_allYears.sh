#!/bin/bash
# FILE2016="LegacyRun2_2016_v3p3_v6p5_TTMerged_rebinnedv3_wVariationRelTRF_forced.root"
# FILE2017="LegacyRun2_2017_v3p3_v6p5_TTMerged_rebinnedv3_wVariationsRelTRF_forced.root"
# FILE2018="LegacyRun2_2018_v3p3_v6p5_TTMerged_rebinnedv3_wVariationsRelTRF_forced.root " 

# FILE2016="LegacyRun2_2016_v3p3_v8_TTMerged_rebinnedv3_wVariations.root"
# FILE2017="LegacyRun2_2017_v3p3_v8_TTMerged_rebinnedv3_wVariations.root"
# FILE2018="LegacyRun2_2018_v3p3_v8_TTMerged_rebinnedv3_wVariations.root" 

FILE2016="LegacyRun2_2016_v4_v1_TTMerged_rebinnedv3_wVariations.root"
FILE2017="LegacyRun2_2017_v4_v1_TTMerged_rebinnedv3_wVariations.root"
FILE2018="LegacyRun2_2018_v4_v1_TTMerged_rebinnedv3_wVariations.root" 


OUTBASE="/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/noMEMDNN/allVars/"


# python makeDatacards.py \
# --config ../data/datacards/LegacyRun2_2016_v3p3_freeFloating.cfg \
# --inputFile /t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2016/${FILE2016} \
# --outputPath ${OUTBASE}/2016/ \
# --cardPrefix card_ \
# --tag ttFreeFloating --force

# python makeDatacards.py \
# --config ../data/datacards/LegacyRun2_2017_v3p3_freeFloating.cfg \
# --inputFile /t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2017/${FILE2017} \
# --outputPath ${OUTBASE}/2017/ \
# --cardPrefix card_ \
# --tag ttFreeFloating --force 

# python makeDatacards.py \
# --config ../data/datacards/LegacyRun2_2018_v3p3_freeFloating.cfg \
# --inputFile /t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2018/${FILE2018} \
# --outputPath ${OUTBASE}/2018/ \
# --cardPrefix card_ \
# --tag ttFreeFloating  --force


python makeDatacards.py \
--config ../data/datacards/LegacyRun2_2016_v3p3.cfg \
--inputFile /t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2016/${FILE2016} \
--outputPath ${OUTBASE}//2016/ \
--cardPrefix card_ \
--tag ttRateparam --force

python makeDatacards.py \
--config ../data/datacards/LegacyRun2_2017_v3p3.cfg \
--inputFile /t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2017/${FILE2017} \
--outputPath ${OUTBASE}/2017/ \
--cardPrefix card_ \
--tag ttRateparam --force 


python makeDatacards.py \
--config ../data/datacards/LegacyRun2_2018_v3p3.cfg \
--inputFile /t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2018/${FILE2018} \
--outputPath ${OUTBASE}/2018/ \
--cardPrefix card_ \
--tag ttRateparam --force 


