import sys, os
import ROOT
import logging

sys.path.insert(0, os.path.abspath('../Plotting/'))
from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF, initLogging
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def main(inFile, channel, folder):
    rFile = ROOT.TFile.Open(inFile)
    
    directory = "{0}/{1}".format(folder, channel)
    _dir = rFile.GetDirectory(directory)
    
    histos = {}
    for key in _dir.GetListOfKeys():
        keyName = key.GetName()
        histos[keyName] = rFile.Get(directory+"/"+keyName)

    ddQCD = None
    ddQCDInit = False
    for key in histos:
        if not  "CR" in key:
            continue
        logging.debug("Adding %s to ddQCD [Int = %s]", key, histos[key].Integral())
        if not ddQCDInit:
            ddQCD = histos[key].Clone("ddQCD")
            ddQCDInit = True
        else:
            ddQCD.Add(histos[key])

    logging.debug("Found ddQCD with integral %s", ddQCD.Integral())
    mcHistos = {}
    for key in histos:
        if "CR" in key or "total" in key or key == "data":
            continue
        mcHistos[key] = histos[key].Clone()
        logging.debug("Additional SR processes found: %s [Int = %s]", key, mcHistos[key].Integral())

    signal = None
    signalInit = False
    for key in mcHistos:
        if not key.startswith("ttH"):
            continue

        logging.debug("Adding %s to Signal [Int = %s]", key, mcHistos[key].Integral())
        if not signalInit:
            signal = mcHistos[key].Clone("Signal")
            signalInit = True
        else:
            signal.Add(mcHistos[key])
    logging.debug("Found Signal with integral %s", signal.Integral())

    
    for histo in mcHistos:
        if histo.startswith("ttH"):
            continue
        mcHistos[histo].SetFillColor(Helper.colors[histo])
        mcHistos[histo].SetLineColor(ROOT.kBlack)
        mcHistos[histo].SetFillStyle(1001)
        
    ddQCD.SetFillColor(Helper.colors["ddQCD"])
    ddQCD.SetLineColor(ROOT.kBlack)
    ddQCD.SetFillStyle(1001)

    backgroundSum = histos["total_background"]

    #Add flag later to plot data instead of background
    hData = backgroundSum
    
    hData.SetLineColor(ROOT.kBlack)
    hData.SetMarkerColor(ROOT.kBlack)
    hData.SetMarkerStyle(20)
    hData.SetMarkerSize(1)

    legendObjwText = []
    

    
    stack = ROOT.THStack("stack", "stack")
    stackSum = ddQCD.Clone("StackSum")
    stack.Add(ddQCD)
    legendObjwText.append((ddQCD, "Multijet"))#
    for histo in mcHistos:
        if histo in ["ttbb","ttcc", "ttlf"]:
            stack.Add(mcHistos[histo])
            legendObjwText.append((mcHistos[histo], histo.replace("tt","t#bar{t}+")))#
            stackSum.Add(mcHistos[histo])
    for histo in mcHistos:
        if histo in ["ttbb","ttcc", "ttlf"]:
            continue
        if histo.startswith("ttH"):
            continue
        stack.Add(mcHistos[histo])
        stackSum.Add(mcHistos[histo])
        legendObjwText.append((mcHistos[histo], histo))#
    stack.Add(signal)
    stackSum.Add(signal)

    output = RatioPlot(name = "SimpleDatacardPlot_"+channel)
    output.legendSize = (0.65, 0.45, 0.95, 0.76)
    output.passHistos([hData, stack])
    thisPlot = output.drawPlot(
        None,
        "HT [GeV]",
        isDataMCwStack = True,
        stacksum=stackSum,
        #errorband = errorband,
        #drawCMS = True,
        histolegend = legendObjwText,
        #drawPulls = drawPulls,
        #floatingMax = 1.23
    )

    outfileName = "SimpleDatacardPlot_"+channel+"_"+folder
    saveCanvasListAsPDF([thisPlot], outfileName,".")
    
if __name__ == "__main__":
    inFile = sys.argv[1]
    channel = sys.argv[2] # Like: fh_j7_t4_ht30
    folder = sys.argv[3] #  Like: shapes_prefit
    initLogging(10, funcLen = 21)
    main(inFile, channel, folder)
