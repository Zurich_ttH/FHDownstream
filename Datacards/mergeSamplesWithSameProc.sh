#!/bin/bash

BASEFOLDER=$1
OUTFOLDER=$2
YEAR=$3

mkdir ${OUTFOLDER}

hadd ${OUTFOLDER}/TTbb.root ${BASEFOLDER}/TTbb_*.root

hadd ${OUTFOLDER}/TTIncl.root ${BASEFOLDER}/TTTo*.root

hadd ${OUTFOLDER}/ST.root ${BASEFOLDER}/ST_*.root

hadd ${OUTFOLDER}/ttH.root ${BASEFOLDER}/ttH*.root

hadd ${OUTFOLDER}/VV.root ${BASEFOLDER}/WW_*.root ${BASEFOLDER}/WZ_*.root ${BASEFOLDER}/ZZ_*.root

hadd ${OUTFOLDER}/ZJets.root ${BASEFOLDER}/ZJets*.root

hadd ${OUTFOLDER}/WJets.root ${BASEFOLDER}/WJets*.root

hadd ${OUTFOLDER}/THQ.root ${BASEFOLDER}/THQ*.root

hadd ${OUTFOLDER}/THW.root ${BASEFOLDER}/THW*.root

hadd ${OUTFOLDER}/ttV.root ${BASEFOLDER}/TTW*.root ${BASEFOLDER}/TTZ*.root

if [ $YEAR == "2017" ]
then
    hadd ${OUTFOLDER}/data.root ${BASEFOLDER}/JetHT.root ${BASEFOLDER}/BTagCSV.root
else
    hadd ${OUTFOLDER}/data.root ${BASEFOLDER}/JetHT.root
fi

