#!/bin/bash
FOLDER=$1
FOLDERMERGED=$2
VAR=$3


#REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins.json"
#REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins.json"
#REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins.json"

REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json"
REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json"
REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json"

# REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner_lessleft.json"
# REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner_lessleft.json"
# REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner_lessleft.json"


python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1

hadd -f ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root 
hadd -f ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root

rm -v ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root
rm -v ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root

python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ttbarPlusBBbarMerged
