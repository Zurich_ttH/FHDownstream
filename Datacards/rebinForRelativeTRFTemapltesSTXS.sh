#!/bin/bash
FOLDER=$1

# REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins.json"
# REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins.json"
# REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins.json"

REBININFO7_0="../data/templateBinning/STXS_7J_BIN0_v3p3_60Bins_innerv2.json"
REBININFO7_1="../data/templateBinning/STXS_7J_BIN1_v3p3_60Bins_inner.json"
REBININFO7_2="../data/templateBinning/STXS_7J_BIN2_v3p3_60Bins_inner.json"
REBININFO7_3="../data/templateBinning/STXS_7J_BIN3_v3p3_60Bins_inner.json"
REBININFO7_4="../data/templateBinning/STXS_7J_BIN4_v3p3_60Bins_innerv2.json"

REBININFO8_0="../data/templateBinning/STXS_8J_BIN0_v3p3_60Bins_inner.json"
REBININFO8_1="../data/templateBinning/STXS_8J_BIN1_v3p3_60Bins_inner.json"
REBININFO8_2="../data/templateBinning/STXS_8J_BIN2_v3p3_60Bins_inner.json"
REBININFO8_3="../data/templateBinning/STXS_8J_BIN3_v3p3_60Bins_inner.json"
REBININFO8_4="../data/templateBinning/STXS_8J_BIN4_v3p3_60Bins_innerv2.json"

REBININFO9_0="../data/templateBinning/STXS_9J_BIN0_v3p3_60Bins_innerv2.json"
REBININFO9_1="../data/templateBinning/STXS_9J_BIN1_v3p3_60Bins_inner.json"
REBININFO9_2="../data/templateBinning/STXS_9J_BIN2_v3p3_60Bins_inner.json"
REBININFO9_3="../data/templateBinning/STXS_9J_BIN3_v3p3_60Bins_inner.json"
REBININFO9_4="../data/templateBinning/STXS_9J_BIN4_v3p3_60Bins_innerv2.json"



PROCS=( ttbarOther ttbarPlusBBbarMerged ttbarPlusCCbar)
FILES=( TTIncl_hdamp_down_ttbbMerged TTIncl_hdamp_up_ttbbMerged TTIncl_nom_ttbbMerged TTIncl_tune_down_ttbbMerged TTIncl_tune_up_ttbbMerged TTbb_hdamp_down_ttbbMerged TTbb_hdamp_up_ttbbMerged TTbb_nom_ttbbMerged )



for PROC in "${PROCS[@]}"
do
    for FILE in "${FILES[@]}"
    do
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_0_j7_t4 --rebinInfo ${REBININFO7_0} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_1_j7_t4 --rebinInfo ${REBININFO7_1} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_2_j7_t4 --rebinInfo ${REBININFO7_2} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_3_j7_t4 --rebinInfo ${REBININFO7_3} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_4_j7_t4 --rebinInfo ${REBININFO7_4} --binEdgeLow 0 --binEdgeHigh 1
	
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_0_j8_t4 --rebinInfo ${REBININFO8_0} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_1_j8_t4 --rebinInfo ${REBININFO8_1} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_2_j8_t4 --rebinInfo ${REBININFO8_2} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_3_j8_t4 --rebinInfo ${REBININFO8_3} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_4_j8_t4 --rebinInfo ${REBININFO8_4} --binEdgeLow 0 --binEdgeHigh 1
	
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_0_j9_t4 --rebinInfo ${REBININFO9_0} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_1_j9_t4 --rebinInfo ${REBININFO9_1} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_2_j9_t4 --rebinInfo ${REBININFO9_2} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_3_j9_t4 --rebinInfo ${REBININFO9_3} --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input ${FOLDER}/${FILE}_${PROC}.root --outFolder ${FOLDER} --cat fh_STXS_4_j9_t4 --rebinInfo ${REBININFO9_4} --binEdgeLow 0 --binEdgeHigh 1


	hadd -f ${FOLDER}/${FILE}_${PROC}_rebinned.root ${FOLDER}/${FILE}_${PROC}_*DNN_Node0*.root


	rm -v ${FOLDER}/${FILE}_${PROC}_*DNN_Node0_fh*.root
    done
done
