"""
Script for adding the ddQCD + uncertainties to SR/VR template (made with sparsinator) files

The script will create a new file that containes all histograms for the ddQCD
- Rate uncertainties that are applied to the other background need to be set in file passed with the --config argument
  --> Standard configuration will not add the scaled histograms to the output file. Use the option addRate in config to overwirte this behaviour
- Scale uncertainties will be taken from the input CR/SR (CR2/VR) files and applied the the ddQCD
- Uncertainties that are so for ddQCD in the config will be applied to SR nominal and saved.

General workflow:
0.) Dertemine categories, variable, and systemtics
1.) Get all shape variations form the CR for backgrounds
2.) Create all rate histograms from CR nominal templates
3.) Get shape for ddQCD in CR 
4.) Add dedicated ddQCD uncertainties to output file
4.1) (optional) Add rate uncertainties as shape in SR
5.) Write all ddQCD templates to output file
"""
from __future__ import division
import ROOT
import os
import sys
import logging
from copy import copy, deepcopy

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
        datefmt="%H:%M:%S"
    )

class runConfig(object):
    def __init__(self, pathtoConfig, overWriteVar = None):
        import ConfigParser
        thisconfig = ConfigParser.ConfigParser()
        thisconfig.optionxform = str #Use this so the section names keep Uppercase letters
        thisconfig.read(pathtoConfig)

        self.inFileSR = thisconfig.get("General", "SRFile")
        self.inFileCR = thisconfig.get("General", "CRFile")
        self.inFileVR = thisconfig.get("General", "VRFile")
        self.inFileCR2 = thisconfig.get("General", "CR2File")

        self.runValidation = thisconfig.getboolean("General","runValidation")
        logging.debug("==== InputFiles ====")
        logging.debug("  SR: %s", self.inFileSR)
        logging.debug("  CR: %s", self.inFileCR)
        logging.debug("  VR: %s", self.inFileVR)
        logging.debug("  CR2: %s", self.inFileCR2)


        if self.runValidation:
            logging.warning("Will run in validation region")
            self.useFileSR = self.inFileVR
            self.useFileCR = self.inFileCR2
        else:
            logging.warning("Will run in signal region")
            self.useFileSR = self.inFileSR
            self.useFileCR = self.inFileCR
        
        self.addRateAsShape = thisconfig.getboolean("General","addRate")
        self.saveAllHistos = thisconfig.getboolean("General","saveAll")

        if thisconfig.has_option("General", "saveSRddQCD"):
            self.saveSRddQCD = thisconfig.getboolean("General", "saveSRddQCD")
        else:
            self.saveSRddQCD = False
            
        logging.debug("Add rate as shpe uncertainties: %s", self.addRateAsShape)
        # Set varibales indentifier.
        self.varIdentifier = thisconfig.get("General", "variable")
        if "," in self.varIdentifier:
            self.varIdentifier = self.varIdentifier.split(",")
        else:
            self.varIdentifier = [self.varIdentifier ]
        if overWriteVar is not None:
            self.varIdentifier = overWriteVar
        logging.debug("Variables to run: %s",self.varIdentifier)
            
        #Get systemtics that are exluded (replaced w/ nominal) for minor bkgs
        self.excludeForMinor = thisconfig.get("General","excludeforMinor")
        self.excludeforSignal = thisconfig.get("General","excludeforSignal")

        if thisconfig.has_option("General", "outPrefix"):
            self.outPrefix = "_"+thisconfig.get("General", "outPrefix")
        else:
            self.outPrefix = ""

        logging.warning("Will use prefix **%s** for output file (**** means no prefix)", self.outPrefix)
            
        if "," in self.excludeForMinor:
            self.excludeForMinor = self.excludeForMinor.split(",")

        if "," in self.excludeforSignal:
            self.excludeforSignal = self.excludeforSignal.split(",")
        
        _excludeForMinor = []
        for excl in self.excludeForMinor:
            _excludeForMinor.append(excl.replace(" ",""))
        self.excludeForMinor = _excludeForMinor

        _excludeforSignal = []
        for excl in self.excludeforSignal:
            _excludeforSignal.append(excl.replace(" ",""))
        self.excludeforSignal = _excludeforSignal
        
        logging.debug("Systemtiocs excluded for signal: %s",self.excludeforSignal)
        logging.debug("Systemtiocs excluded for minor background: %s",self.excludeForMinor)
        
        self.signalProcesses = thisconfig.get("ProcessNames", "signal")
        if "," in self.signalProcesses:
            self.signalProcesses = self.signalProcesses.split(",")
        self.backgroundProcesses = thisconfig.get("ProcessNames", "bkg")
        if "," in self.backgroundProcesses:
            self.backgroundProcesses = self.backgroundProcesses.split(",")
        self.ttbarProcesses = thisconfig.get("ProcessNames", "ttbar")
        if "," in self.ttbarProcesses:
            self.ttbarProcesses = self.ttbarProcesses.split(",")

        logging.debug("==== Process names ====")
        logging.debug("   ttbar: %s",self.ttbarProcesses )
        logging.debug("  signal: %s",self.signalProcesses )
        logging.debug("     bkg: %s",self.backgroundProcesses )
            
        #Get rate systematics to be added
        self.systematics = {}
        systematics = thisconfig.options("Systematics")
        for sys in systematics:
            if sys not in self.systematics.keys():
                self.systematics[sys] = {}
            procUncs = thisconfig.get("Systematics",sys).split("\n")
            for unc in procUncs:
                if unc == "":
                    continue
                #print "-",unc.split(" ")
                uncName, uncVal = unc.split(" ")
                if "/" in uncVal:
                    plusVal, minusVal = uncVal.split("/")
                else:
                    #Is expecting value > 1 as symmetric uncertainty value
                    plusVal, minusVal = float(uncVal), 1/float(uncVal)
                self.systematics[sys][uncName] = [float(plusVal), float(minusVal)]
        self.catSystemtics = {}
        for section in thisconfig.sections():
            #if section.startswith("Systematics__"):
            if len(section.split("__")) == 2:
                cat = section.split("__")[-1]
                self.catSystemtics[cat] = {}
                systematics = thisconfig.options(section)
                for sys in systematics:
                    self.catSystemtics[cat][sys] = {}
                    procUncs = thisconfig.get(section,sys).split("\n")
                    for unc in procUncs:
                        if unc == "":
                            continue
                        #print "-",unc.split(" ")
                        procName, uncVal = unc.split(" ")
                        procName = procName.replace(":","")
                        if "/" in uncVal:
                            plusVal, minusVal = uncVal.split("/")
                        else:
                            #Is expecting value > 1 as symmetric uncertainty value
                            plusVal, minusVal = float(uncVal), 1/float(uncVal)
                        self.catSystemtics[cat][sys][procName] = [float(plusVal), float(minusVal)]
        catSystemtics_ = {}
        self.uniqCatSysts = []
        for cat in self.catSystemtics:
            catSystemtics_[cat] = {}
            for syst in self.catSystemtics[cat]:
                self.uniqCatSysts.append(syst)
                for proc in self.catSystemtics[cat][syst]:
                    if proc not in catSystemtics_[cat].keys():
                        catSystemtics_[cat][proc] = {}
                    if syst in catSystemtics_[cat][proc].keys():
                        raise RuntimeError
                    else:
                        catSystemtics_[cat][proc][syst] = self.catSystemtics[cat][syst][proc]
        
        self.catSystemtics = catSystemtics_
        self.uniqCatSysts = list(set(self.uniqCatSysts))
        
        if logging.getLogger().level < 20:
            logging.debug("==== Uncertainties from config ====")
            for key in self.systematics:
                logging.debug("== Systemtics: %s ==", key)
                for unc in self.systematics[key]:
                    logging.debug("   Unc: %s - %s",unc, self.systematics[key][unc])
                logging.debug("==== Category Uncertainties from config ====")
                for cat in self.catSystemtics:
                    logging.debug("== Cat: %s ==", cat)
                    for key in self.catSystemtics[cat]:
                        logging.debug("== process: %s ==", key)
                        for unc in self.catSystemtics[cat][key]:
                            logging.debug("   Unc: %s - %s",unc, self.catSystemtics[cat][key][unc])

        self.hasCatSF = False
        self.hasRegionRates = False

        for section in thisconfig.sections():
            if section.startswith("GeneralSF"):
                self.hasCatSF = True
                logging.debug("Found region dependent global SF")
                break
            
        for section in thisconfig.sections():
            if len(section.split("__")) == 3 and section.startswith("Systematics"):
                logging.debug("Found region depedent systematics")
                self.hasRegionRates = True
                break

        self.GlobalSF = {}
        if self.hasCatSF:
            for section in thisconfig.sections():
                if section.startswith("GeneralSF"):
                    self.GlobalSF[section.split("_")[1]] = {}
                    for cat in thisconfig.options(section):
                        self.GlobalSF[section.split("_")[1]][cat] = {}
                        procUncs = thisconfig.get(section,cat).split("\n")
                        for procUnc in procUncs:#
                            procUnc = procUnc.split(" ")
                            if len(procUnc) != 2:
                                continue
                            proc, SF = procUnc[0], procUnc[1]
                            self.GlobalSF[section.split("_")[1]][cat][proc] = float(SF)
                            logging.debug("Read global SF {0} for {1}, {2}, {3}".format(self.GlobalSF[section.split("_")[1]][cat][proc], section.split("_")[1],cat, proc))

        self.regionCatSystemtics = {}
        self.uniqRegionCatSysts = []
        if self.hasRegionRates:
            for section in thisconfig.sections():
                if section.startswith("Systematics") and len(section.split("__")) == 3:
                    cat, region = section.split("__")[1], section.split("__")[2]
                    if not self.regionCatSystemtics.has_key(region):
                        self.regionCatSystemtics[region] = {}
                    if not self.regionCatSystemtics[region].has_key(cat):
                        self.regionCatSystemtics[region][cat] = {}
                    #self.regionCatSystemtics[region][cat] = {}
                    for syst in thisconfig.options(section):
                        self.uniqRegionCatSysts.append(syst)
                        self.regionCatSystemtics[region][cat][syst] = {}
                        procUncs = thisconfig.get(section,syst).split("\n")
                        for procUnc in procUncs:#
                            procUnc = procUnc.split(" ")
                            if len(procUnc) != 2:
                                continue
                            proc, uncVal = procUnc[0], procUnc[1]
                            if "/" in uncVal:
                                plusVal, minusVal = uncVal.split("/")
                            else:
                                #Is expecting value > 1 as symmetric uncertainty value
                                plusVal, minusVal = float(uncVal), 1/float(uncVal)
                            if not self.regionCatSystemtics[region][cat].has_key(proc):
                                self.regionCatSystemtics[region][cat][proc] = {}
                            self.regionCatSystemtics[region][cat][proc][syst] = [plusVal, minusVal]
                            logging.debug("Read region depedent systematic {0} for {1}, {2}, {3}, {4}".format(self.regionCatSystemtics[region][cat][proc][syst], region,cat, proc, syst))
        self.uniqRegionCatSysts = list(set(self.uniqRegionCatSysts))

        
        
def LaunchAdder(config):
    """
    Base script that is called to run all parts required for the QCD estimate. THe basic steps are:
    1.) Getting all variables, categories, processes from the CR file (information form the SR would be identical)
        and sorting them according to config definitions
        --> You will be asked to check the outcome before proceeding
    2.) Getting all ddQCD tempaltes by processing the CR 
        --> see description of getCRDistributions()
    3.) Transferring the templates to the the SR and making all rate templates for the SR processes
        --> see description of transferddToSR()
    4.) Write output file containing the ddQCD templates and (if activated n config) rate templates in the SR 
    
    Args:
    config (runConfig) : Config object form the runConfig class in the file
    """
    logging.info("Launching Adder")
    ################################################################################################################
    CRFile = ROOT.TFile.Open(config.useFileCR)
    features = getTemplateFeatures(CRFile, config.ttbarProcesses, config.backgroundProcesses, config.signalProcesses)
    logging.info("==== CR features ====")
    logging.debug("  Categories : %s",features["cats"])
    logging.debug("  Variables : %s",features["variables"])
    logging.info("  systematics : %s",features["systematics"])
    logging.info(" uniqCatSysts : %s", config.uniqCatSysts)
    logging.info("uniqRegionCatSysts : %s",config.uniqRegionCatSysts)
    logging.info("  ttbartypes : %s",features["ttbartypes"])
    logging.info("  signals : %s",features["signals"])
    logging.info("  backgrounds : %s",features["backgrounds"])
    logging.info("  montecarlo : %s",features["montecarlo"])
    #Filter variables for definition in config
    print config.varIdentifier

    newfeatures = []
    for variable in features["catvars"]:
        for varId in config.varIdentifier:
            if varId in variable:
                newfeatures.append(variable)
    features["catvars"] = list(set(newfeatures))
    if len(features["catvars"]) == 0:
        raise RuntimeError("No variables from config found in features")
    logging.info("Post filter variables: %s", features["catvars"])
    raw_input("Check features")
    ################################################################################################################
    ddHistosCR = getCRDistributions(CRFile, config, features["catvars"], features["systematics"],
                                    features["backgrounds"], features["ttbartypes"], features["signals"] )
    ################################################################################################################
    SRFile = ROOT.TFile.Open(config.useFileSR)
    ddHistos, backgrounds, data = transferddToSR(ddHistosCR, SRFile, config,features["catvars"], features["systematics"],
                                           features["backgrounds"], features["ttbartypes"], features["signals"] )
    ################################################################################################################
    # for key in backgrounds:
    #     if key.startswith("diboson"):
    #         if key.endswith("_p"):
    #             logging.info("INtegral %s = %s", key, backgrounds[key].Integral())
    #         if "QCDscale_VV" in key:
    #             logging.info("INtegral %s = %s", key, backgrounds[key].Integral())
    # raw_input("dasd")
    writeOutput(config, ddHistos, backgrounds, config.systematics.keys()+config.uniqCatSysts, data)
    ################################################################################################################
    ROOT.gROOT.GetListOfFiles().Remove(CRFile)
    ROOT.gROOT.GetListOfFiles().Remove(SRFile)
    
def getCRDistributions(CRFile, config, catvars, systemtics, background, ttbartypes, signals):
    """
    Top-level function to process the CR templates to get the ddQCD templpates for the SR (only the normalization has
    to be modified afterwards. The following steps are processed:
    1.) Read all necessary Histograms from the input file (data, ttbar backgrounds and minor background). Note: ttH is 
        not considered at this stage!
    1.1) (OPTIONAL) Scale all histos by global SF (if defined in config)
    2.) The nominal CR templates for the considered backgrounds are used to derive the rate variations as defined 
        in the config.
        --> There are also category dependent rate systematics which are correlated which are handled in separate step
    2.1) (OPTIONAL) Add region and category depedent rate parameters if defined in config
    3.) Creation of the ddQCD histograms
        3.1.) In a first step this is done by cloning the data histograms 
        3.2.) In a second step the background histograms are substracted from the according histograms
    
    Args:
    =====
    CRFile (ROOT.TFile) : Input file for the CR
    config (runConfig) : Config object form the runConfig class in the file
    catvars (list) : List of all considered category-variable combinations (format: {cat}__{var})
    systemtics (list) : List of systemtics present in the input file
    background (list) :  List of processes considered minor background 
    ttbartypes (list) : List of processes considered ttbar 
    signals (list) : List of processes considered signal

    Returns:
    ========
    ddHistos (dict) : Dict containing name, ROOT.TH1* pairs
    """
    logging.info("Getting CR distributions of background histograms")
    ################################################################################################################
    logging.info("Getting shape systemtic templates")
    dataHistos = getHistosFromFile(CRFile, ["data"], catvars, systemtics)
    # for key in dataHistos:
    #     if "CMS_ttHbb_HTreweight4b_2017" in key:
    #         raw_input("found CMS_ttHbb_HTreweight4b_2017 - "+key)
    #     if "CMS_ttHbb_HTreweight3b_2017" in key:
    #         raw_input("found CMS_ttHbb_HTreweight3b_2017 - "+key)
    
    ttHistos = getHistosFromFile(CRFile, ttbartypes, catvars, systemtics)
    backgroundHistos = getHistosFromFile(CRFile, background, catvars, systemtics, config.excludeForMinor)
    if config.GlobalSF:
        logging.info("Scaling all histos with global SF")
        scaelHistos(config, "CR" ,histoSets = [ttHistos, backgroundHistos])
    nom_ttHistos = getNominalHistos(ttHistos)
    nom_backgroundHistos = getNominalHistos(backgroundHistos)
    ################################################################################################################
    logging.info("Creating rate systemtic templates")
    ttHistos.update(getRateTemplates(nom_ttHistos,  ttbartypes, catvars, config.systematics))
    backgroundHistos.update(getRateTemplates(nom_backgroundHistos, background, catvars, config.systematics))
    logging.info("Creating rate templates for category dependend systemtics")
    ttHistos.update(getCatRateTemplates(nom_ttHistos, ttbartypes, catvars, config.catSystemtics))
    backgroundHistos.update(getCatRateTemplates(nom_backgroundHistos, background, catvars, config.catSystemtics))
    #backgroundHistos.update(getCatRateTemplates(CRFile, background, catvars, config.catSystemtics))
    if config.hasRegionRates:
        logging.info("Creating rate templates for region dependent systematics")
        ttHistos.update(getRegionCatRateTemplates(nom_ttHistos, "CR", ttbartypes, catvars, config.regionCatSystemtics))
        backgroundHistos.update(getRegionCatRateTemplates(nom_backgroundHistos, "CR", background, catvars, config.regionCatSystemtics))

    ################################################################################################################
    logging.info("Creating ddQCD templates")
    ddHistos = createddQCDHistos(dataHistos, catvars, list(systemtics), config.systematics.keys()+config.uniqCatSysts+config.uniqRegionCatSysts)
    allbkgHistos = {}
    allbkgHistos.update(ttHistos)
    allbkgHistos.update(backgroundHistos)
    #intbefore = ddHistos["ddQCD__fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_bgnorm_ttbarPlus2B_2017Down"].Integral()
    makeddQCDHistos(ddHistos, allbkgHistos, list(background)+list(ttbartypes))
    ################################################################################################################
    # for key in ddHistos:
    #     if "2017" in key:
    #         print key, ddHistos[key]
    # raw_input("cont")
    logging.info("Finished CR")
    # for key in backgroundHistos:
    #     if key.startswith("diboson"):
    #         if key.endswith("_p"):
    #             logging.info("INtegral %s = %s", key, backgroundHistos[key].Integral())
    #         if "QCDscale_VV" in key:
    #             logging.info("INtegral %s = %s", key, backgroundHistos[key].Integral())
    return deepcopy(ddHistos)
    
def transferddToSR(ddHistosCR, SRFile, config, catvars, systemtics, background, ttbartypes, signals):
    """
    Top-level function to transfer the ddQCD templates to the SR and add the rate variated templates to the SR.
    The following steps are processed:
    1.) Read all necessary Histograms from the input file (data, signal, ttbar backgrounds and minor background).
    1.1) (OPTIONAL) Scale all histos by global SF (if defined in config)
    2.) Create rate systematics templates for mc
    2.1) (OPTIONAL) Add region and category depedent rate parameters if defined in config
    3.) Create ddQCD templates from SR data + mc. This is only required to estmated the expected rate in the SR
    4.) Calculated yields (integral of templates - done for each systemtic) of CR and SR ddQCD templates 
        --> Scale CR ddQCD to the SR yield
    5.) In principle we could have ddQCD specific rate uncertainties, which will be added next
    
    Args:
    =====
    ddHistosCR (dict) : Return value from getCRDistributions()
    SRFile (ROOT.TFile) : Input file for the SR
    config (runConfig) : Config object form the runConfig class in the file
    catvars (list) : List of all considered category-variable combinations (format: {cat}__{var})
    systemtics (list) : List of systemtics present in the input file
    background (list) :  List of processes considered minor background 
    ttbartypes (list) : List of processes considered ttbar 
    signals (list) : List of processes considered signal

    Returns:
    ========
    ddHistos (dict) : Dict containing name, ROOT.TH1* pairs
    bkgPlusSignal : All templates of the mc processes 
    """
    logging.info("Getting SR distributions")
    ################################################################################################################
    dataHistos = getHistosFromFile(SRFile, ["data"], catvars, systemtics, isSR=True)
    ttHistos = getHistosFromFile(SRFile, ttbartypes, catvars, systemtics, isSR=True)
    backgroundHistos = getHistosFromFile(SRFile, background, catvars, systemtics, config.excludeForMinor, isSR=True)
    signalHistos = getHistosFromFile(SRFile, signals, catvars, systemtics, config.excludeforSignal, isSR=True)

    if config.GlobalSF:
        logging.info("Scaling all histos with global SF")
        scaelHistos(config, "SR" ,histoSets = [ttHistos, backgroundHistos, signalHistos])
    nom_ttHistos = getNominalHistos(ttHistos)
    nom_backgroundHistos = getNominalHistos(backgroundHistos)
    nom_signalHistos = getNominalHistos(signalHistos)
    ################################################################################################################
    logging.info("Creating rate systemtic templates")
    ttHistos.update(getRateTemplates(nom_ttHistos,  ttbartypes, catvars, config.systematics, isSR=True))
    backgroundHistos.update(getRateTemplates(nom_backgroundHistos, background, catvars, config.systematics, isSR=True))
    signalHistos.update(getRateTemplates(nom_signalHistos,  signals, catvars, config.systematics, isSR=True))
    logging.info("Creating rate templates for category dependend systemtics")
    ttHistos.update(getCatRateTemplates(nom_ttHistos, ttbartypes, catvars, config.catSystemtics))
    backgroundHistos.update(getCatRateTemplates(nom_backgroundHistos, background, catvars, config.catSystemtics))
    signalHistos.update(getCatRateTemplates(nom_signalHistos, signals, catvars, config.catSystemtics))
    if config.hasRegionRates:
        logging.info("Creating rate templates for region dependent systematics")
        ttHistos.update(getRegionCatRateTemplates(nom_ttHistos, "SR", ttbartypes, catvars, config.regionCatSystemtics))
        backgroundHistos.update(getRegionCatRateTemplates(nom_backgroundHistos, "SR", background, catvars, config.regionCatSystemtics))
        signalHistos.update(getRegionCatRateTemplates(nom_signalHistos, "SR", signals, catvars, config.regionCatSystemtics))
    bkgPlusSignal = {}
    bkgPlusSignal.update(ttHistos)
    bkgPlusSignal.update(backgroundHistos)
    bkgPlusSignal.update(signalHistos)

    ################################################################################################################
    logging.info("Creating ddQCD templates (For yields only)")
    ddHistos_4Yield = createddQCDHistos(dataHistos, catvars, list(systemtics), config.systematics.keys()+config.uniqCatSysts+config.uniqRegionCatSysts, isSR=True)
    makeddQCDHistos(ddHistos_4Yield, bkgPlusSignal, list(background)+list(ttbartypes)+list(signals))
    logging.info("Calculating yields for all temples in SR and CR")
    logging.debug("SR yields")
    nQCD_SR = getYields(ddHistos_4Yield)
    raw_input("..")
    logging.debug("CR yields")
    nQCD_CR = getYields(ddHistosCR)
    raw_input("..")
    if config.saveSRddQCD:
        writeDict(config, ddHistos_4Yield, "ddQCDSR")
    
    ################################################################################################################
    ddHistos = deepcopy(ddHistosCR)
    logging.info("Scaling ddQCD to SR yield")
    scaleddQCDtoSR(ddHistos, nQCD_SR, nQCD_CR, ScaleToSRNom = True)
    logging.info("Adding ddQCD specific uncertainties")
    addSRddQCDSystemtics(ddHistos, config.systematics)
    ################################################################################################################
    logging.info("Finished SR")
    return deepcopy(ddHistos), deepcopy(bkgPlusSignal), dataHistos

def writeDict(config, histoDict, name):
    outputFile = config.useFileSR.replace(".root","_{0}.root".format(name))
    outFile = ROOT.TFile(outputFile, "RECREATE")
    outFile.cd()
    for key in histoDict:
        logging.debug("Writing: %s",key)
        histoDict[key].Write()

    outFile.Close()
    
    ROOT.gROOT.GetListOfFiles().Remove(outFile)

    
def writeOutput(config, ddHistos, allbkgHistos, rateuncertainties, dataHistos = {}):
    if config.saveAllHistos:
        outputFile = config.useFileSR.replace(".root",config.outPrefix+"_ddQCD_all.root")
    else:
        outputFile = config.useFileSR.replace(".root",config.outPrefix+"_ddQCD_added.root")
    logging.warning("Will use %s as outout file", outputFile)
    outFile = ROOT.TFile(outputFile, "RECREATE")
    outFile.cd()

    if config.saveAllHistos:
        for key in dataHistos:
            logging.debug("Writing: %s",key)
            dataHistos[key].Write()
        
    for key in ddHistos:
        logging.debug("Writing: %s",key)
        logging.info("%s: %s/%s",key, ddHistos[key].Integral(),ddHistos["__".join(key.split("__")[0:3])].Integral())
        ddHistos[key].Write()

        
    outHistos = []
    
    if config.addRateAsShape:
        for key in sorted(allbkgHistos.keys()):
            writeThis = False
            if config.saveAllHistos:
                writeThis = True
            else:
                for unc in rateuncertainties:
                    if unc in key:
                        writeThis = True
                        break
            if writeThis:
                logging.info("Writing: %s",key)
                outHistos.append(key)
                allbkgHistos[key].Write()
        
    with open(config.useFileSR.replace(".root","_ddQCD_all.txt"), "w") as f:
        for line in outHistos:
            f.write(line+"\n")

    outFile.Close()
    ROOT.gROOT.GetListOfFiles().Remove(outFile)


def getNominalHistos(histoDict):
    histos = {}
    for key in histoDict:
        if len(key.split("__")) == 3:
            histos[key] = histoDict[key]

    return deepcopy(histos)
    
def getHistosFromFile(CRFile, prefixes, catvars, systemtics, ignore = None, isSR = False):
    histos = {}
    for prefix in prefixes:
        logging.info("Getting histos for process %s", prefix)
        for catvar in catvars:
            _catvar = catvar
            if "_CR__" in catvar and isSR:
                _catvar = _catvar.replace("_CR__", "_SR__")
                print _catvar
            nomName = "{0}__{1}".format(prefix, _catvar)
            histos[nomName] = deepcopy(CRFile.Get(nomName))
            for sys in systemtics:
                for sdir in ["Up", "Down"]:
                    systemtic = sys+sdir
                    sysName = "{0}__{1}__{2}".format(prefix, _catvar, systemtic)
                    skip = False
                    if ignore is not None:
                        #print ignore
                        for ignoreSys in ignore:
                            if ignoreSys in systemtic:
                                skip = True
                                logging.debug("Skipping %s because of passed exclusion criterion", systemtic)
                    if "t3" in _catvar and "4b" in systemtic:
                        skip = True
                    elif "t4" in _catvar and "3b" in systemtic:
                        skip = True
                    if "ttbar" in sys:
                        if not prefix in sys:
                            logging.debug("prefix %s not in sys %s", prefix, sys)
                            skip = True
                    if skip:
                        logging.debug("Replacing %s with nominal",sysName)
                        histos[sysName] = CRFile.Get(nomName).Clone(sysName)
                    else:
                        if sysName not in CRFile.GetListOfKeys():
                            #if not "data" in prefix:
                            logging.debug("Could not find histo with name %s in file", sysName)
                            histos[sysName] = CRFile.Get(nomName).Clone(sysName)
                        else:
                            logging.debug("Using hitogram %s from file (int = %s)",sysName, CRFile.Get(sysName).Integral())
                            histos[sysName] = deepcopy(CRFile.Get(sysName))
    return histos

def getRateTemplates(nomHistos, prefixes, catvars, rateSystematics, isData = False, isSR = False):
    histos = {}
    for prefix in prefixes:
        logging.info("Creating templates for process %s", prefix)
        for catvar in catvars:
            _catvar = catvar
            if "_CR__" in catvar and isSR:
                _catvar = _catvar.replace("_CR__", "_SR__")
                print _catvar
            nomName = "{0}__{1}".format(prefix, _catvar)
            nomHisto = nomHistos[nomName]
            for sys in rateSystematics:
                for proc in rateSystematics[sys]:
                    if proc != prefix and proc != "allMC":
                        continue
                    for pos, sdir in [(0, "Up"), (1, "Down")]:
                        systemtic = sys+sdir
                        sysName = "{0}__{1}__{2}".format(prefix, catvar, systemtic)
                        logging.debug("Creating template %s [Scale: %s]", sysName, rateSystematics[sys][proc][pos])
                        histos[sysName] = nomHisto.Clone(sysName)
                        histos[sysName].Scale(rateSystematics[sys][proc][pos])
                        histos[sysName].SetTitle(sysName)
                        logging.debug("Integral nom %s - Integral scaled %s", nomHisto.Integral(), histos[sysName].Integral())
    return deepcopy(histos)                

def getCatRateTemplates(nomHistos, prefixes, catvars, rateCatSystematics, isData = False):
    histos = {}
    for catvar in catvars:
        cat = catvar.split("__")[0]
        if cat not in rateCatSystematics.keys():
            continue
        logging.debug("Found systemtic for proc %s", cat)
        for prefix in prefixes:
            logging.debug("Checking process %s in %s", prefix,rateCatSystematics[cat].keys())
            if prefix not in rateCatSystematics[cat].keys():
                continue
            logging.info("Creating templates for process %s", prefix)
            nomName = "{0}__{1}".format(prefix, catvar)
            nomHisto = nomHistos[nomName]
            for sys in rateCatSystematics[cat][prefix]:
                for pos, sdir in [(0, "Up"), (1, "Down")]:
                    systemtic = sys+sdir
                    sysName = "{0}__{1}__{2}".format(prefix, catvar, systemtic)
                    logging.debug("Creating template %s [Scale: %s]", sysName, rateCatSystematics[cat][prefix][sys][pos])
                    histos[sysName] = nomHisto.Clone(sysName)
                    histos[sysName].Scale(rateCatSystematics[cat][prefix][sys][pos])
                    histos[sysName].SetTitle(sysName)
    return deepcopy(histos)

def getRegionCatRateTemplates(nomHistos, regionType, prefixes, catvars, rateRegionSystematics):
    if regionType == "CR" and config.runValidation:
        region = "CR2"
    elif regionType == "CR" and not config.runValidation:
        region = "CR"
    elif regionType == "SR" and config.runValidation:
        region = "VR"
    elif regionType == "SR" and not config.runValidation:
        region = "SR"
    else:
        raise KeyError("Only CR and SR are supported arguments for regionType")

    histos = {}
    for catvar in catvars:
        cat = catvar.split("__")[0]
        if cat not in rateRegionSystematics[region].keys():
            logging.debug("%s not in %s",cat,rateRegionSystematics[region].keys() )
            continue
        logging.debug("Found systemtic for proc %s", cat)
        for prefix in prefixes:
            logging.debug("Checking process %s in %s", prefix, rateRegionSystematics[region][cat].keys())
            if prefix not in rateRegionSystematics[region][cat].keys():
                continue
            logging.info("Creating templates for process %s", prefix)
            nomName = "{0}__{1}".format(prefix, catvar)
            nomHisto = nomHistos[nomName]
            for sys in rateRegionSystematics[region][cat][prefix]:
                for pos, sdir in [(0, "Up"), (1, "Down")]:
                    systemtic = sys+sdir
                    sysName = "{0}__{1}__{2}".format(prefix, catvar, systemtic)
                    logging.debug("Creating template %s [Scale: %s - Region %s]", sysName, rateRegionSystematics[region][cat][prefix][sys][pos], region)
                    histos[sysName] = nomHisto.Clone(sysName)
                    histos[sysName].Scale(rateRegionSystematics[region][cat][prefix][sys][pos])
                    histos[sysName].SetTitle(sysName)
    print "returning",histos
    return deepcopy(histos)

def createddQCDHistos(dataHistos, catvars, shapeSystematics, rateSystematics, isSR=False):
    histos = {}
    for catvar in catvars:
        _catvar = catvar
        if "_CR__" in catvar and isSR:
            _catvar = _catvar.replace("_CR__", "_SR__")
            print _catvar
        nomName = "data__{0}".format(_catvar)
        histos[nomName.replace("data","ddQCD")] =  dataHistos[nomName].Clone(nomName.replace("data","ddQCD"))
        histos[nomName.replace("data","ddQCD")].SetTitle(nomName.replace("data","ddQCD"))
        for syst in shapeSystematics+rateSystematics:
            for sdir in ["Up", "Down"]:
                systemtic = syst+sdir
                sysName = "data__{0}__{1}".format(_catvar, systemtic)
                if "t3" in _catvar and "4b" in systemtic:
                    continue
                if "t4" in _catvar and "3b" in systemtic:
                    continue
                if not sysName in dataHistos.keys():
                    logging.debug("Creating template %s from nominal data", sysName.replace("data","ddQCD"))
                    histos[sysName.replace("data","ddQCD")] = dataHistos[nomName].Clone(sysName.replace("data","ddQCD"))
                    histos[sysName.replace("data","ddQCD")].SetTitle(sysName.replace("data","ddQCD"))
                else:
                    histos[sysName.replace("data","ddQCD")] = dataHistos[sysName].Clone(sysName.replace("data","ddQCD"))
                    histos[sysName.replace("data","ddQCD")].SetTitle(sysName.replace("data","ddQCD"))
                    #logging.info("Getting histo %s - INt %s",histos[sysName.replace("data","ddQCD")], histos[sysName.replace("data","ddQCD")].Integral() )
    return deepcopy(histos)           

def makeddQCDHistos(ddHistos, allbkgHistos, processes):
    for hKey in ddHistos:
        for proc in processes:
            procSystName = hKey.replace("ddQCD__", proc+"__")
            if procSystName not in allbkgHistos:
                nominalName = procSystName.split("__")
                nominalName = "__".join(nominalName[0:3])
                logging.debug("Using nomianl for %s [%s]", procSystName, nominalName)
                ddHistos[hKey] = ddHistos[hKey] - allbkgHistos[nominalName]
            else:
                logging.debug("Substracting %s from %s [Int %s]",procSystName, hKey, allbkgHistos[procSystName].Integral())
                #if "bgnorm" in procSystName:
                #    print "Intgral post substraction {0}".format(ddHistos[hKey].Integral())
                ddHistos[hKey] = ddHistos[hKey] - allbkgHistos[procSystName]
                #if "bgnorm" in procSystName:
                #    print "Intgral post substraction {0}".format(ddHistos[hKey].Integral())
                #    raw_input("Press ret")

def scaelHistos(config, regionType, histoSets):
    if regionType == "CR" and config.runValidation:
        region = "CR2"
    elif regionType == "CR" and not config.runValidation:
        region = "CR"
    elif regionType == "SR" and config.runValidation:
        region = "VR"
    elif regionType == "SR" and not config.runValidation:
        region = "SR"
    else:
        raise KeyError("Only CR and SR are supported arguments for regionType")

    print config.GlobalSF.keys()
    for key in config.GlobalSF:
        print config.GlobalSF[key]
    
    for histoSet in histoSets:
        for syst in sorted(histoSet):
            parts = syst.split("__")
            proc, cat = parts[0], parts[1]
            intPre = histoSet[syst].Integral()
            logging.debug("%s %s %s",region,cat,proc)
            logging.debug("Scaling %s with %s [%s][%s][%s]", syst, config.GlobalSF[region][cat][proc],region,cat,proc)
            histoSet[syst].Scale(config.GlobalSF[region][cat][proc])
            logging.debug("Integral - Pre = %s | Post = %s", intPre, histoSet[syst].Integral())
            # if "ttH_hbb" in  histoSet[syst].GetName():
            #     raw_input("next")

def getYields(histoDict):
    yields = {}
    for key in histoDict:
        yields[key] = histoDict[key].Integral()
        if len(key.split("__")) == 3:
            logging.debug("Yield %s = %s", key, yields[key])
    return yields
        
def addSRddQCDSystemtics(ddQCDHistos, rateSystematics):
    nomHistoNames = []
    for key in ddQCDHistos:
        if len(key.split("__")) == 3:
            nomHistoNames.append(key)
    for syst in rateSystematics:
        for proc in rateSystematics[syst]:
            if proc == "ddQCD":
                for key in nomHistoNames:
                    if "t3" in key and "4b" in syst:
                        continue
                    if "t4" in key and "3b" in syst:
                        continue
                    logging.info("Adding %s__%sUp/Down with scale %s",key, syst, rateSystematics[syst][proc][0])
                    change = rateSystematics[syst][proc][0]
                    
                    ddQCDHistos["{0}__{1}Up".format(key, syst)] = ddQCDHistos[key].Clone("{0}__{1}Up".format(key, syst))
                    ddQCDHistos["{0}__{1}Up".format(key, syst)].SetTitle("{0}__{1}Up".format(key, syst))
                    ddQCDHistos["{0}__{1}Down".format(key, syst)] = ddQCDHistos[key].Clone("{0}__{1}Down".format(key, syst))
                    ddQCDHistos["{0}__{1}Down".format(key, syst)].SetTitle("{0}__{1}Down".format(key, syst))

                    #ContentUp = rateSystematics[syst][proc][0]*ddQCDHistos["{0}__{1}Up".format(key, syst)].GetBinContent(1)
                    #ContentDown = rateSystematics[syst][proc][0]*ddQCDHistos["{0}__{1}Down".format(key, syst)].GetBinContent(1)
                    diffUp = change*ddQCDHistos["{0}__{1}Up".format(key, syst)].GetBinContent(1)
                    diffDown = -change*ddQCDHistos["{0}__{1}Up".format(key, syst)].GetBinContent(1)
                    ContentUp = diffUp + ddQCDHistos["{0}__{1}Up".format(key, syst)].GetBinContent(1)
                    ContentDown = diffDown + ddQCDHistos["{0}__{1}Up".format(key, syst)].GetBinContent(1)

                    integral = ddQCDHistos["{0}__{1}Up".format(key, syst)].Integral() - ddQCDHistos["{0}__{1}Up".format(key, syst)].GetBinContent(1)
                    Fullintegral = ddQCDHistos["{0}__{1}Up".format(key, syst)].Integral()
                    ddQCDHistos["{0}__{1}Up".format(key, syst)].Scale((integral-diffUp)/integral) #All ddQCD specific rate uncertainties are onesided atm
                    ddQCDHistos["{0}__{1}Down".format(key, syst)].Scale((integral-diffDown)/integral) #All ddQCD specific rate uncertainties are onesided atm
                    ddQCDHistos["{0}__{1}Up".format(key, syst)].SetBinContent(1, ContentUp)
                    ddQCDHistos["{0}__{1}Down".format(key, syst)].SetBinContent(1, ContentDown)

                    logging.debug("1st bin uncertainties %s__%sUp: Diff %s | Cont %s | Integral %s | New Int %s",
                                  key,syst, diffUp, ContentUp, Fullintegral,ddQCDHistos["{0}__{1}Up".format(key, syst)].Integral()
                    )
                    logging.debug("1st bin uncertainties %s__%sDown: Diff %s | Cont %s | Integral %s | New Int %s",
                                  key,syst, diffDown, ContentDown, Fullintegral,ddQCDHistos["{0}__{1}Down".format(key, syst)].Integral()
                    )
                    
                    
def scaleddQCDtoSR(ddHistos, SRYields, CRYields, ScaleToSRNom):
    """
    Function to scale the ddQCD CR histograms to the SR yield. Use ScaleToSRNom to fix the
    ddQCD yield for all systemtics to the nominal. THis results in all systemtics to only being
    shape uncertainties w/o rate change. This is usefull because the free floating normalization
    should take care of the rate.

    Args:
    =====
    ddHistos (dict of TH1) : Dict containing all ddQCD histograms (from the CR)
    SRYields, CRYields (dict of float) : Dicts with all the yields (output of getYields())
    ScaleToSRNom (bool) : If True, all systemtics will be scaled to the norminal yield
    """
    for key in ddHistos:
        if not ScaleToSRNom:
            ddHistos[key].Scale(SRYields[key]/CRYields[key])
        else:
            if len(key.split("__")) == 4:
                nomkey = "{0}__{1}__{2}".format(key.split("__")[0], key.split("__")[1], key.split("__")[2])
                if "_CR__" in nomkey:
                    nomkey = nomkey.replace("_CR__","_SR__")
                logging.debug("Scaling %s (CRKey) to %s (SRKey)", key, nomkey)
            else:
                nomkey = key
                if "_CR__" in nomkey:
                    nomkey = nomkey.replace("_CR__","_SR__")
                logging.debug("This %s is the nominal (SRKey %s)", key, nomkey)
            ddHistos[key].Scale(SRYields[nomkey]/CRYields[key])


    
def getTemplateFeatures(rFile, ttbarNames, bkgNames, signalNames):
    """
    Function for getting categories, samples and systemtics for sparse file
    """
    logging.info("Getting template features for file: %s",rFile)
    catvars = set()
    cats = set()
    variables = set()
    systematics = set()
    ttbartypes = set()
    backgrounds = set()
    signals = set()
    montecarlo = set()
    nominalHistos = []
    for key in rFile.GetListOfKeys():
        parts = key.GetName().split("__")
        if len(parts) == 3:
            nominalHistos.append(key.GetName())
        if len(parts)>3:
            systematics.add( parts[3] )
        if not "data" in parts[0]:
            montecarlo.add(parts[0])
        if parts[0] in ttbarNames:
            ttbartypes.add(parts[0])
        elif parts[0] in bkgNames:
            backgrounds.add( parts[0] )
        elif parts[0] in signalNames:
             signals.add( parts[0] )

    for name in nominalHistos:
        parts = name.split("__")
        catvars.add(parts[1]+"__"+parts[2])
        cats.add(parts[1])
        variables.add(parts[2])
    onlyNames = []
    for sys in systematics:
        if sys.endswith("Up"):
            onlyNames.append(sys[0:-len("Up")])
        elif sys.endswith("Down"):
            onlyNames.append(sys[0:-len("Down")])
        else:
            logging.debug("Removing systemtic: %s",sys)
    systematics = set(onlyNames)
    return {"catvars" : catvars, "cats" : cats, "variables" : variables,"systematics" : systematics,
            "ttbartypes" : ttbartypes, "signals" : signals, "montecarlo" : montecarlo, "backgrounds" : backgrounds}

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "Config that defines options and inputfiles",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--useVar",
        action = "store",
        help = "Overwrite variable set in config",
        type = str,
        required = False
    )
    
    args = argumentparser.parse_args()
    
    initLogging(args.logging)
    config = runConfig(args.config, overWriteVar = args.useVar)

    LaunchAdder(config)
