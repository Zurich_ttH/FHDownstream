from __future__ import print_function

import sys
import os
from glob import glob

from TTH.MEAnalysis.ParHadd import par_hadd

def mergeCTCVCPGCOut(basepath, outpath, skipMerge=False):

    print("Basepath : %s"%basepath)
    print("outpath : %s"%outpath)
    
    samples = os.listdir(basepath)
    prefixes = []
    
    for sample in samples:
        for weight in os.listdir(basepath+"/"+sample):
            filePrefix = "P"+str(weight.split("_")[-1])
            if filePrefix not in prefixes:
                prefixes.append(filePrefix)
            files = glob(basepath+"/"+sample+"/"+weight+"/*.root")
            if not skipMerge:
                par_hadd(outpath + "{}_{}.root".format(filePrefix, sample),
                         files,
                         250, 10, 3)
            
    return samples, prefixes
   
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Process the CTCVCP samples'
    )
    argumentparser.add_argument(
        "--gcPaths",
        action = "store",
        help = "Base paths for GC outputs",
        nargs="+",
        required= True
    )
    argumentparser.add_argument(
        "--outputPath",
        action = "store",
        help = "Output path.",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--mergeOutPrefix",
        action = "store",
        help = "Prefix for the merged output file.",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--datacardConfigCP",
        action = "store",
        help = "Config file for the datacard",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--datacardConfigKappa",
        action = "store",
        help = "Config file for the datacard",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--datacardTag",
        action = "store",
        help = "Tag for the datacard",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--rebinJSONS",
        action = "store",
        help = "Tag for the datacard",
        nargs = 3,
        required = True
    )
    argumentparser.add_argument(
        "--addTemplates",
        action = "store",
        help = "additonal templates added to the merged file for hte datacard generation",
        type = str,
        nargs="+",
        default = []
    )
    argumentparser.add_argument(
        "--skipGCMerge",
        action = "store_true",
    )
    argumentparser.add_argument(
        "--skipSampleMerge",
        action = "store_true",
    )
    argumentparser.add_argument(
        "--skipRebining",
        action = "store_true",
    )
    argumentparser.add_argument(
        "--skipDatacardGen",
        action = "store_true",
    )

    args = argumentparser.parse_args()
    
    allProcessedSamples = []
    allPrefixes = None
    for gcOutBase in args.gcPaths:
        processedSamples, prefixes = mergeCTCVCPGCOut(gcOutBase, args.outputPath, args.skipGCMerge)
        if allPrefixes is None:
            allPrefixes = prefixes
        else:
            for p in prefixes:
                if p not in allPrefixes:
                    raise RuntimeWarning("Prefix %s is new. Make sure this is okay"%p)
        for sample in processedSamples:
            if sample in allProcessedSamples:
                raise RuntimeError
        allProcessedSamples+=processedSamples
        
    print(allPrefixes)
    mergedFiles = []
    for p in allPrefixes:
        print(args.outputPath, p)
        files2Merge = glob("{}/{}_*.root".format(args.outputPath, p))
        outputFile = args.outputPath + "/{}_{}.root".format(args.mergeOutPrefix, p)
        mergedFiles.append(outputFile)
        if not args.skipSampleMerge:
            print("par_hadd arg 1 : %s"%outputFile)
            print("par_hadd arg 2 : "+str(files2Merge+args.addTemplates))
            par_hadd(outputFile,
                     files2Merge+args.addTemplates,
                     250, 10, 3)
            #raw_input("Next merge")


    if not args.skipRebining:
        print("Rebinning")
        for file_ in mergedFiles:
            for icat,cat in enumerate(["fh_j7_t4", "fh_j8_t4", "fh_j9_t4"]):
                rebinCommand =  "python {}/src/TTH/FHDownstream/Datacards/genOptimizedTemplates.py".format(os.environ["CMSSW_BASE"])
                rebinCommand += " --input {}".format(file_)
                rebinCommand += " --outFolder {}".format(args.outputPath)
                rebinCommand += " --cat {}".format(cat)
                rebinCommand += " --rebinInfo {}".format(args.rebinJSONS[icat])
                rebinCommand += " --binEdgeLow 0 --binEdgeHigh 1"
                print(rebinCommand)
                os.system(
                    rebinCommand+" &> /dev/null"
                )

                
            inFileName = file_.split("/")[-1].replace(".root","")
            outFiles = glob(args.outputPath+"/"+inFileName+"*fh_*.root")
            haddCommand = "hadd {}_rebinned.root".format(file_.replace(".root",""))
            rmCommand = "rm"
            for outFile in outFiles:
                haddCommand += " "+outFile
                rmCommand += " "+outFile
            print(haddCommand)
            os.system(
                haddCommand
            )
            print(rmCommand)
            os.system(
                rmCommand
            )



                
    kappaScanIDs = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 21, 23, 24, 26, 27, 29, 30, 32, 34, 35, 37, 47, 49, 50]
    CPScan = [0, 12, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69]

    linkCreatorKappa = ""
    linkCreatorCP = ""
    
    if not args.skipDatacardGen:
        print("Generation kappa datacards")
        for id_ in kappaScanIDs:
            thisPrefix = "P"+str(id_)
            datacardCommand =  "python {}/src/TTH/FHDownstream/Datacards/makeDatacards.py".format(os.environ["CMSSW_BASE"])
            datacardCommand += " --config {} --inputFile {}/{}_{}_rebinned.root".format(args.datacardConfigKappa,
                                                                               args.outputPath,
                                                                               args.mergeOutPrefix, thisPrefix)
            datacardCommand += " --outputPath {}/kappa/".format(args.outputPath)
            datacardCommand += " --tag {}/{}".format(args.datacardTag, thisPrefix)
            print(datacardCommand)
            os.system(
                datacardCommand+" &> log_datacard_{}.txt".format(thisPrefix)
            )
            # for card in glob("{}/kappa/{}/{}/cards/*.txt".format(args.outputPath,args.datacardTag, thisPrefix)):
            #     fileName = card.split("/")[-1]
            #     relName = card.replace(args.outputPath+"/kappa/"+args.datacardTag+"/","")
            #     lnCommand = "ln -s {} {}_{}".format(relName, thisPrefix, fileName)
            #     linkCreatorKappa += lnCommand + "\n"

        # with open(args.outputPath+"/kappa/"+args.datacardTag+"/createLinks.sh","w") as f:
        #     f.write(linkCreatorKappa)
                
        print("Generation CP datacards")
        for id_ in CPScan:
            thisPrefix = "P"+str(id_)
            datacardCommand =  "python {}/src/TTH/FHDownstream/Datacards/makeDatacards.py".format(os.environ["CMSSW_BASE"])
            datacardCommand += " --config {} --inputFile {}/{}_{}_rebinned.root".format(args.datacardConfigCP,
                                                                               args.outputPath,
                                                                               args.mergeOutPrefix, thisPrefix)
            datacardCommand += " --outputPath {}/CP/".format(args.outputPath)
            datacardCommand += " --tag {}/{}".format(args.datacardTag, thisPrefix)
            print(datacardCommand)
            os.system(
                datacardCommand+" &> log_datacard_{}.txt".format(thisPrefix)
            )
            # for card in glob("{}/CP/{}/{}/cards/*.txt".format(args.outputPath,args.datacardTag, thisPrefix)):
            #     fileName = card.split("/")[-1]
            #     relName = card.replace(args.outputPath+"/CP/"+args.datacardTag+"/","")
            #     lnCommand = "ln -s {} {}_{}".format(relName, thisPrefix, fileName)
            #     linkCreatorCP += lnCommand + "\n"

        # with open(args.outputPath+"/CP/"+args.datacardTag+"/createLinks.sh","w") as f:
        #     f.write(linkCreatorCP)
