#!/bin/bash
BASEFOLDER="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v3/STXS/v1/ttHSplit/initMerge"

echo "python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THW.root --outPostfix MergedPDF --templatePostfix _tHW &> TH.log &"
python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THW.root --outPostfix MergedPDF --templatePostfix _tHW &> TH.log &
echo "python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THQ.root --outPostfix MergedPDF --templatePostfix _tHW &> TH.log &"
python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THQ.root --outPostfix MergedPDF --templatePostfix _tHq &> TH.log &
echo "python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs ttH.root --outPostfix MergedPDF &> ttH.log &"
python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs ttH.root --outPostfix MergedPDF &> ttH.log &
