#!/bin/bash
BASEFOLDER=$1

echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl.root &> TTIncl.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl.root &> TTIncl.log &
echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb.root &> TTbb.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb.root &> TTbb.log &
