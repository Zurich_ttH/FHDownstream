import sys, os
import logging
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

import ROOT

def getYieldFromSparse(inputFile, variable="numJets", cat="fh_presel"):
    logging.info("Processing %s", inputFile)

    rFile = ROOT.TFile.Open(inputFile)

    allKeys = [x.GetName() for x in rFile.GetListOfKeys()]
    nominals = [x for x in allKeys if len(x.split("__"))==3]
    
    allProcs = list(set([x.split("__")[0] for x in nominals]))

    logging.info("All processes : %s", allProcs)
    yields = {}
    errors = {}
    
    for proc in allProcs:
        histoName = "{}__{}__{}".format(proc, cat, variable)
        logging.debug("Getting histos: %s", histoName)
        thisHisto = rFile.Get(histoName)
        yields[proc] = thisHisto.GetBinContent(1)
        errors[proc] = thisHisto.GetBinError(1)

    return yields, errors
        

def makeTable(yields, errors, names, order=None):
    tableLines = [""]*(len(yields[names[0]].keys())+1)
    for ikey, key in enumerate(names):
        if order is None:
            procs = yields[key].keys()
        else:
            procs = order
        for iproc, proc in enumerate(procs):
            if ikey == 0:
                tableLines[iproc+1] += "{:15} \t ".format(proc)
            tableLines[iproc+1] += "{:10.2f} \t +- \t {:10.2f} \t".format(yields[key][proc], errors[key][proc])

    tableLines[0] = "{:15} \t".format("")
    for key in names:
        tableLines[0] += "{:10} \t     \t {:10} \t".format(key, "")
        
    for line in tableLines:
        print line
        
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        help = "Input file",
        nargs = "+",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--names",
        action = "store",
        help = "Names (will be used for the table)",
        nargs = "+",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--variable",
        action = "store",
        help = "Variable names",
        type = str,
        default = "numJets"
    )
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    assert len(args.inputs) == len(args.names)
    
    yields = {}
    errors = {}
    for i, input_ in enumerate(args.inputs):
        yields[args.names[i]], errors[args.names[i]] = getYieldFromSparse(input_, args.variable)
    
    
    makeTable(yields, errors, args.names)
