#!/bin/bash

FOLDER=$1

VARS=(DNNInput_Detaj_5 DNNInput_Detaj_6 DNNInput_Detaj_7 DNNInput_bJets_Mass_jj_max DNNInput_R_2)

for VAR in "${VARS[@]}"
do
    python extractVariable.py --input ${FOLDER}/TTbb_ttbarOther_systamtics.root --var ${VAR} --postfix extractedVariables
    python extractVariable.py --input ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics.root --var ${VAR} --postfix extractedVariables
    python extractVariable.py --input ${FOLDER}/TTbb_ttbarPlusCCbar_systamtics.root--var ${VAR} --postfix extractedVariables
    
    python extractVariable.py --input ${FOLDER}/TTIncl_ttbarOther_systamtics.root --var ${VAR} --postfix extractedVariables
    python extractVariable.py --input ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics.root --var ${VAR} --postfix extractedVariables
    python extractVariable.py --input ${FOLDER}/TTIncl_ttbarPlusCCbar_systamtics.root --var ${VAR} --postfix extractedVariables
done

