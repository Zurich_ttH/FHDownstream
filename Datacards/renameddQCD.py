import sys
import ROOT
from copy import deepcopy
inputFile = sys.argv[1]
outfolder = sys.argv[2]

rFileIn = ROOT.TFile.Open(inputFile)

SRHistos = []


for key in rFileIn.GetListOfKeys():
    keyName = key.GetName()
    SRHistos.append(deepcopy(rFileIn.Get(keyName)))

rFileIn.Close()

for hitso in SRHistos:
    if hitso.GetName().endswith("_CR"):
        hitso.SetName(hitso.GetName().replace("_CR","_SR"))
        hitso.SetTitle(hitso.GetName().replace("_CR","_SR"))
        
    if "_CR__" in hitso.GetName():
        hitso.SetName(hitso.GetName().replace("_CR__","_SR__"))
        hitso.SetTitle(hitso.GetName().replace("_CR__","_SR__"))


rOutSR = ROOT.TFile(outfolder+"/merged_SR_ddQCD_all_renamed.root", "RECREATE")
rOutSR.cd()
for histo in SRHistos:
    histo.Write()

    
