from __future__ import print_function, division

import sys, os
from math import sqrt
import logging
from copy import copy, deepcopy

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging, makeCanvasOfHistos, saveCanvasListAsPDF
from classes.PLotDrawers import makeCanvasOfHistosCustomLeg

def main(inputFile, tag):
    rFile = ROOT.TFile.Open(inputFile)
    logging.info("File: %s", inputFile)
    allKeys = [key.GetName() for key in rFile.GetListOfKeys()]

    allProcs = []
    allVars = []
    allCats = []
    for key in allKeys:
        elem = key.split("__")
        if elem[0] not in allProcs:
            allProcs.append(elem[0])
        if elem[1] not in allCats:
            allCats.append(elem[1])
        if elem[2] not in allVars:
            allVars.append(elem[2])

    logging.info("Found processes: %s", allProcs)
    logging.info("Found variables: %s", allVars)
    logging.info("Found categories: %s", allCats)

    for cat in allCats:
        logging.info("Processing categories: %s", cat)
        for proc in allProcs:
            if proc == "data":
                logging.warning("Skipping data")
                continue
            logging.info("Processing process: %s", proc)
            canvases = []
            for var in allVars:
                logging.debug("Processing variable: %s", var)
                nominalHistoName = "{}__{}__{}".format(proc, cat, var)
                logging.debug("Nominal Histo Name: %s", nominalHistoName)
                if nominalHistoName not in allKeys:
                    logging.warning("Skipping %s", nominalHistoName)
                    continue
                nomHisto = rFile.Get(nominalHistoName)
                logging.debug("Nominal histogram: %s", nomHisto)
                systHistos = [key for key in allKeys if key.startswith(nominalHistoName) and key is not nominalHistoName]
                logging.debug("Systeamtics considered: %s", systHistos)
                sysErrorsUp = nomHisto.GetNbinsX()*[0.0]
                sysErrorsDown = nomHisto.GetNbinsX()*[0.0]
                largestSysUp = nomHisto.GetNbinsX()*[0.0]
                largestSysDown = nomHisto.GetNbinsX()*[0.0]
                largestFlucUp = 0.0
                largestFlucDown = 0.0
                largestNameUp = None
                largestNameDown = None
                maxUncertainyUp = 0.0
                maxUncertainyDown = 0.0
                maxSysUp = nomHisto.GetNbinsX()*[0.0]
                maxSysDown = nomHisto.GetNbinsX()*[0.0]
                maxUncertainyNameUp = None
                maxUncertainyNameDown = None
                #print(nomHisto.GetNbinsX())
                for syst in systHistos:
                    islargestSystUp = False
                    islargestSystDown = False
                    thisUncertainty = nomHisto.GetNbinsX()*[0.0]
                    if not ("Down" in syst or "Up" in syst):
                        continue
                    # if "Up" in syst:
                    #     print(syst, sysErrorsUp[1], rFile.Get(syst).GetBinContent(1), nomHisto.GetBinContent(1) )
                    for iBin in range(nomHisto.GetNbinsX()):
                        if "Up" in syst:
                            unc = rFile.Get(syst).GetBinContent(iBin+1) - nomHisto.GetBinContent(iBin+1)
                            sysErrorsUp[iBin] += pow(unc, 2)
                            thisUncertainty[iBin] = pow(unc, 2)
                            if unc > largestFlucUp:
                                islargestSystUp = True
                                largestFlucUp = unc
                        else:
                            unc = nomHisto.GetBinContent(iBin+1)-rFile.Get(syst).GetBinContent(iBin+1)
                            sysErrorsDown[iBin] += pow(unc, 2)
                            thisUncertainty[iBin] = pow(unc, 2)
                            if unc > largestFlucDown:
                                islargestSystDown = True
                                largestFlucDown = unc
                    if islargestSystDown:
                        largestNameDown = syst.split("__")[-1]
                        for iBin in range(nomHisto.GetNbinsX()):
                            unc = nomHisto.GetBinContent(iBin+1)-rFile.Get(syst).GetBinContent(iBin+1)
                            largestSysDown[iBin] += pow(unc, 2)
                    if islargestSystUp:
                        largestNameUp = syst.split("__")[-1]
                        for iBin in range(nomHisto.GetNbinsX()):
                            unc = rFile.Get(syst).GetBinContent(iBin+1) - nomHisto.GetBinContent(iBin+1)
                            largestSysUp[iBin] += pow(unc, 2)

                    uncertainty = sqrt(sum(thisUncertainty))
                    if "Up" in syst:
                        if uncertainty > maxUncertainyUp:
                            maxUncertainyUp = uncertainty
                            maxUncertainyNameUp = syst.split("__")[-1]
                            maxSysUp = thisUncertainty
                    else:
                        if uncertainty > maxUncertainyDown:
                            maxUncertainyDown = uncertainty
                            maxUncertainyNameDown = syst.split("__")[-1]
                            maxSysDown = thisUncertainty
                            
                totalErrorsUp = copy(sysErrorsUp)
                totalErrorsDown = copy(sysErrorsDown)
                largestErrerUp = copy(largestSysUp)
                largestErrerDown = copy(largestSysDown)
                maxErrorsUp = copy(maxSysUp)
                maxErrorsDown = copy(maxSysDown)
                
                statErrorsUp = nomHisto.GetNbinsX()*[0.0]
                for iBin in range(nomHisto.GetNbinsX()):
                    statErrorsUp[iBin] = nomHisto.GetBinError(iBin+1)
                    totalErrorsUp[iBin] += pow(nomHisto.GetBinError(iBin+1), 2)
                    totalErrorsDown[iBin] += pow(nomHisto.GetBinError(iBin+1), 2)
                    largestErrerUp[iBin] += pow(nomHisto.GetBinError(iBin+1), 2)
                    largestErrerDown[iBin] += pow(nomHisto.GetBinError(iBin+1), 2)
                    maxErrorsUp[iBin] += pow(nomHisto.GetBinError(iBin+1), 2)
                    maxErrorsDown[iBin] += pow(nomHisto.GetBinError(iBin+1), 2)
                
                    
                sysErrorsUp = [sqrt(err) for err in sysErrorsUp]
                sysErrorsDown = [sqrt(err) for err in sysErrorsDown]

                totalErrorsUp = [sqrt(err) for err in totalErrorsUp]
                totalErrorsDown = [sqrt(err) for err in totalErrorsDown]

                largestErrerUp = [sqrt(err) for err in largestErrerUp] 
                largestErrerDown = [sqrt(err) for err in largestErrerDown]

                maxErrorsUp  = [sqrt(err) for err in maxErrorsUp] 
                maxErrorsDown = [sqrt(err) for err in maxErrorsDown] 
                
                
                hTotalUp = nomHisto.Clone(nomHisto.GetName()+"_"+"TotalUncUp")
                hTotalDown = nomHisto.Clone(nomHisto.GetName()+"_"+"TotalUncDown")
                hStatUp = nomHisto.Clone(nomHisto.GetName()+"_"+"StatUncUp")
                hStatDown = nomHisto.Clone(nomHisto.GetName()+"_"+"StatUncDown")
                hLargestUp = nomHisto.Clone(nomHisto.GetName()+"_"+"LargestUncUp")
                hLargestDown = nomHisto.Clone(nomHisto.GetName()+"_"+"LargestUncDown")
                hMaxUp = nomHisto.Clone(nomHisto.GetName()+"_"+"MaxUncUp")
                hMaxDown = nomHisto.Clone(nomHisto.GetName()+"_"+"MaxUncDown")

                for iBin in range(nomHisto.GetNbinsX()):
                    hTotalUp.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)+totalErrorsUp[iBin])
                    hTotalDown.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)-totalErrorsDown[iBin])
                    hStatUp.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)+statErrorsUp[iBin])
                    hStatDown.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)-statErrorsUp[iBin]) 
                    hLargestUp.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)+largestErrerUp[iBin])
                    hLargestDown.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)-largestErrerDown[iBin]) 
                    hMaxUp.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)+maxErrorsUp[iBin])
                    hMaxDown.SetBinContent(iBin+1, nomHisto.GetBinContent(iBin+1)-maxErrorsDown[iBin]) 
                    
                hTotalUp.SetLineColor(ROOT.kBlue)
                hStatUp.SetLineColor(ROOT.kRed)
                nomHisto.SetLineColor(ROOT.kBlack)
                hStatDown.SetLineColor(ROOT.kRed)
                hTotalDown.SetLineColor(ROOT.kBlue)

                if largestNameDown is None:
                    logging.error("largestNameDown is None in %s/%s/%s",proc, cat, var)
                    continue
                if largestNameUp is None:
                    logging.error("largestNameUp is None in %s/%s/%s",proc, cat, var)
                    continue
                
                legendObj = [
                    (deepcopy(nomHisto), "Nominal Tempalte"),
                    (None, " ",""),
                    (deepcopy(hTotalUp), "Stat + Syst Unc."),
                    (deepcopy(hStatUp), "Stat Unc."),
                ]

                if largestNameUp.replace("Up","") == largestNameDown.replace("Down",""):
                    hLargestUp.SetLineColor(ROOT.kGreen-2)
                    hLargestDown.SetLineColor(ROOT.kGreen-2)
                    
                    legendObj.append(
                        (deepcopy(hLargestUp), "Stat + Largest Unc. - "+largestNameUp.replace("Up","")) 
                    )
                    legendObj.append(
                        (None, " ",""),
                        #(None, largestNameUp.replace("Up",""), "") 
                    )


                else:
                    hLargestUp.SetLineColor(ROOT.kGreen-2)
                    hLargestDown.SetLineColor(ROOT.kGreen+2)

                    legendObj += [
                        (deepcopy(hLargestUp), "Stat + Largest Fluc. - "+largestNameUp),
                        #(None, largestNameUp, ""),
                        (None, " ",""),
                        (deepcopy(hLargestDown), "Stat + Largest Fluc. - "+largestNameDown,) ,
                        #(None, largestNameDown, ""),
                        (None, " ",""),
                    ]

                if maxUncertainyNameUp.replace("Up","") == maxUncertainyNameDown.replace("Down",""):
                    hMaxUp.SetLineColor(ROOT.kMagenta-2)
                    hMaxDown.SetLineColor(ROOT.kMagenta-2)
                    
                    legendObj.append(
                        (deepcopy(hMaxUp), "Stat + Max Unc. - "+maxUncertainyNameUp.replace("Up","")) 
                    )
                    legendObj.append(
                        #(None, maxUncertainyNameUp.replace("Up",""), "")
                        (None, " ",""),
                    )


                else:
                    hMaxUp.SetLineColor(ROOT.kMagenta-2)
                    hMaxDown.SetLineColor(ROOT.kMagenta+2)

                    legendObj += [
                        (deepcopy(hMaxUp), "Stat + Largest Unc. - "+maxUncertainyNameUp),
                        #(None, maxUncertainyNameUp, ""),
                        (None, " ",""),
                        (deepcopy(hMaxDown), "Stat + Largest Unc. - "+maxUncertainyNameDown) ,
                        #(None, maxUncertainyNameDown, ""),
                        (None, " ",""),
                    ]

                
                    
                canvases.append(
                    makeCanvasOfHistosCustomLeg(
                        "{}__{}__{}".format(proc, cat, var),
                        var,
                        [hTotalUp, hStatUp, nomHisto, hStatDown, hTotalDown, hLargestUp, hLargestDown, hMaxUp, hMaxDown],
                        legendObj,
                        drawAs = "HISTO",
                        labelpos = (0.13,0.955),
                        addCMS = True,
                        addLabel = "{} - {}".format(cat, proc),
                        topScale = 1.4,
                        legendColumns = 2,
                        legendSize = (0.16,0.7,0.6,0.9),
                    )
                )
            if len(canvases) == 0:
                logging.error("No canvases")
                continue
            saveCanvasListAsPDF(canvases, tag+"_"+proc+"_"+cat, "out_systVariations")
        
if __name__ == "__main__":
    initLogging(20, funcLen = 21)
    
    
    inFile = sys.argv[1]
    tag = sys.argv[2]
    
    main(inFile, tag)
    
