import ROOT
import sys
from copy import deepcopy


infile = sys.argv[1]

rFile = ROOT.TFile.Open(infile)

keys = rFile.GetListOfKeys()

histos = []
hDict = {}

nomYieldQCD = {}
nomBkgYield = {}
for key in keys:
    keyName = key.GetName()
    histos.append(deepcopy(rFile.Get(keyName)))
    hDict[keyName] = deepcopy(rFile.Get(keyName))
    if len(keyName.split("__")) == 3 and "qcd" in keyName:
        nomYieldQCD[keyName] = rFile.Get(keyName).Integral()
    if len(keyName.split("__")) == 3 and not "data" in keyName:
        if nomBkgYield.has_key(keyName.split("__")[2]):
            nomBkgYield[keyName.split("__")[2]] += rFile.Get(keyName).Integral()
        else:
            nomBkgYield[keyName.split("__")[2]] = rFile.Get(keyName).Integral()
SFs = {}
for hKey in hDict:
    if not "qcd" in hKey:
        continue
    if len(hKey.split("__")) == 3:
        SF = ( nomYieldQCD[hKey] + (hDict[hKey.replace("qcd","data")].Integral() - nomBkgYield[hKey.split("__")[2]]))/float(nomYieldQCD[hKey])
        SFs[hKey.split("__")[2]] = SF

rFile.Close()
print nomYieldQCD
print nomBkgYield
print SFs

outfile = ROOT.TFile(infile.replace(".root","_QCDScaled.root"),"RECREATE")
outfile.cd()
#(QCDYield+(dataYield-mcYield))/float(QCDYield))

for histo in histos:
    if not "qcd" in histo.GetName():
        histo.Write()
        continue
    if len(histo.GetName().split("__")) >= 3:
        varName = histo.GetName().split("__")[2]
        histo.Scale(SFs[varName])
        histo.Write()

