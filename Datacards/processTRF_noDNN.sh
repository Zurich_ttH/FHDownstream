#!/bin/bash
FOLDERBASE=$1
FOLDERVARBINNING=$2
MAINFILETTMERGED=$3
MAINFILETTINC=$4
FOLDERINIT=$5

VAR="RecoHiggsPt"

echo "mkdir -p ${FOLDERBASE}/${FOLDERVARBINNING}/"
mkdir -p ${FOLDERBASE}/${FOLDERVARBINNING}/


echo "./mergeVariatedSamplesWithSameProc.sh ${FOLDERBASE}/SR/  ${FOLDERBASE}/CR/  ${FOLDERBASE}/${FOLDERVARBINNING}/"
./mergeVariatedSamplesWithSameProc.sh ${FOLDERBASE}/SR/  ${FOLDERBASE}/CR/  ${FOLDERBASE}/${FOLDERVARBINNING}/

echo "./processTTbarVariated.sh  ${FOLDERBASE}/${FOLDERVARBINNING}/"
./processTTbarVariated.sh  ${FOLDERBASE}/${FOLDERVARBINNING}/

echo "./rebinForRelativeTRFTemapltesnoDNN.sh  ${FOLDERBASE}/${FOLDERVARBINNING}/"
./rebinForRelativeTRFTemapltesnoDNN.sh  ${FOLDERBASE}/${FOLDERVARBINNING}/

echo "./calcRelativeTRFTemplates.sh ${FOLDERBASE}/${FOLDERVARBINNING}/ ${MAINFILETTMERGED} ${MAINFILETTINC} ${VAR}"
./calcRelativeTRFTemplates.sh ${FOLDERBASE}/${FOLDERVARBINNING}/ ${MAINFILETTMERGED} ${MAINFILETTINC} ${VAR}

echo "./transplantUErelTRF_noDNN_rebin.sh ${FOLDERBASE}/${FOLDERVARBINNING}/ ${FOLDERINIT}"
./transplantUErelTRF_noDNN_rebin.sh ${FOLDERBASE}/${FOLDERVARBINNING}/ ${FOLDERINIT}

echo "hadd ${FOLDERBASE}/${FOLDERVARBINNING}/VariatedTTMerged.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics_TransplantedUE.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTIncl_ttbarPlusCCbar_${VAR}_relTRF_systamtics.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTIncl_ttbarOther_${VAR}_relTRF_systamtics.root"
hadd ${FOLDERBASE}/${FOLDERVARBINNING}/VariatedTTMerged.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics_TransplantedUE.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTIncl_ttbarPlusCCbar_${VAR}_relTRF_systamtics.root ${FOLDERBASE}/${FOLDERVARBINNING}/TTIncl_ttbarOther_${VAR}_relTRF_systamtics.root
