import sys, os
import logging
from copy import deepcopy

import ROOT

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from makeDatacards import replChannel, replVariable, replProcess, replSystematic

def mergeTemplates(infile, outPostfix, carryOverOtherProcs,
                   mergeProcs = ["ttbarPlus2B", "ttbarPlusB", "ttbarPlusBBbar"],
                   outProc = "ttbarPlusBBbarMerged",
                   templateSchemaNominal = "$PROCESS__$CHANNEL__$VARIABLE",
                   templateSchemaSystematic = "$PROCESS__$CHANNEL__$VARIABLE__$SYSTEMATIC",
                   addGluSplitUnc = True):
    folder = "/".join(infile.split("/")[0:-1])
    logging.info("Operating folder: %s", folder)

    rFile = ROOT.TFile.Open(infile)

    allKeys = [x.GetName() for x in rFile.GetListOfKeys()]

    allMergeTemplates = []
    for proc in mergeProcs:
        allMergeTemplates += [x for x in allKeys if x.startswith(proc)]

    nominals = [x for x in allMergeTemplates if len(x.split("__"))==3]
    systematics = [x for x in allMergeTemplates if len(x.split("__"))==4]

    _cats = list(set([x.split("__")[1] for x in nominals]))
    _vars = list(set([x.split("__")[2] for x in nominals]))
    _systs = list(set([x.split("__")[3] for x in systematics]))
    
    if carryOverOtherProcs:
        logging.info("Finding carryOverOtherProcs keys")
        carryOverNames = [x for x in allKeys if x not in allMergeTemplates]
    else:
        carryOverNames = []
    carryOver = []
    logging.info("Copying carryOverOtherProcs keys")
    for ikey,key in enumerate(carryOverNames):
        if ikey%1000 == 0:
            logging.info("Copying key %s / %s",ikey, len(carryOverNames) )
        carryOver.append(deepcopy(rFile.Get(key)))
    
    
    logging.info("Creating output names")
    mergedTemplates = {}
    for cat in _cats:
        for var in _vars:
            nominalName = replVariable(replChannel(replProcess(templateSchemaNominal, outProc), cat), var)
            logging.debug("Adding %s [nominal]", nominalName)
            mergedTemplates[nominalName] = None
            for syst in _systs:
                systName = replSystematic(replVariable(replChannel(replProcess(templateSchemaSystematic, outProc), cat), var), syst)
                logging.debug("Adding %s [systematic]", systName)
                mergedTemplates[systName] = None


    
    for cat in _cats:
        logging.info("Processing cat %s", cat)
        for var in _vars:
            logging.debug("Processing var %s", var)
            try:
                rFile.Get(replVariable(replChannel(replProcess(templateSchemaNominal, mergeProcs[0]), cat), var)).GetNbinsX()
            except AttributeError:
                logging.error("Skipping %s", replVariable(replChannel(replProcess(templateSchemaNominal, mergeProcs[0]), cat), var))
                continue
            for iproc, proc in enumerate(mergeProcs):
                nominalName =  replVariable(replChannel(replProcess(templateSchemaNominal, proc), cat), var)
                nominalOutput = replVariable(replChannel(replProcess(templateSchemaNominal, outProc), cat), var)
                if iproc == 0:
                    mergedTemplates[nominalOutput] = deepcopy(rFile.Get(nominalName).Clone(nominalOutput))
                    mergedTemplates[nominalOutput].SetTitle(nominalOutput)
                else:
                    mergedTemplates[nominalOutput].Add(rFile.Get(nominalName))
                for syst in _systs:
                    systName = replSystematic(replVariable(replChannel(replProcess(templateSchemaSystematic, proc), cat), var), syst)
                    systOutput = replSystematic(replVariable(replChannel(replProcess(templateSchemaSystematic, outProc), cat), var), syst)
                    if iproc == 0:
                        mergedTemplates[systOutput] = deepcopy(rFile.Get(systName).Clone(systOutput))
                        mergedTemplates[systOutput].SetTitle(systOutput)
                    else:
                        mergedTemplates[systOutput].Add(rFile.Get(systName))
            if not addGluSplitUnc:
                logging.warning("Skipping addition of CMS_ttHbb_tt2b_glusplit")
                continue
            logging.debug("Adding CMS_ttHbb_tt2b_glusplit")
            for sdir in ["Up","Down"]:
                logging.debug("  sdir : %s", sdir)
                systOutput = replSystematic(replVariable(replChannel(replProcess(templateSchemaSystematic, outProc), cat), var), "CMS_ttHbb_tt2b_glusplit"+sdir)
                mergedTemplates[systOutput] = None
                for iproc, proc in enumerate(mergeProcs):
                    nominalName =  replVariable(replChannel(replProcess(templateSchemaNominal, proc), cat), var)
                    sf = 1.0
                    if proc == "ttbarPlus2B":
                        if sdir == "Up":
                            sf = 1.5
                        else:
                            sf = 0.5
                    logging.debug("  Proc: %s | SF: %s", proc, sf)
                    if iproc == 0:
                        thisHisto = deepcopy(rFile.Get(nominalName).Clone(systOutput))
                        thisHisto.Scale(sf)
                        thisHisto.SetTitle(systOutput)
                        mergedTemplates[systOutput] = thisHisto
                    else:
                        thisHisto = deepcopy(rFile.Get(nominalName).Clone(systOutput))
                        thisHisto.Scale(sf)
                        mergedTemplates[systOutput].Add(thisHisto)
                
                        
    outName = infile.split("/")[-1].replace(".root","")+"_"+outPostfix+".root"
    logging.info("Will use output file %s", outName)
    outFile = ROOT.TFile(folder+"/"+outName, "RECREATE")
    outFile.cd()

    for template in carryOver:
        template.Write()

    for templateName in mergedTemplates:
        if mergedTemplates[templateName] is not None:
            mergedTemplates[templateName].Write()

    outFile.Close()
    
    return folder+"/"+outName
                        
if __name__ == "__main__":
    import argparse
    ############################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        help = "Input file",
        nargs = "+",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outPostfix",
        action = "store",
        help = "Postfix for output file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--carryOverOtherProcs",
        action = "store_true",
        help = "If passed ttBarOther and ttbarCCbar will be added to the output file"
    )
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)


    for infile in args.inputs:
        logging.info("Processing %s", infile)
        mergeTemplates(infile, args.outPostfix, args.carryOverOtherProcs)
