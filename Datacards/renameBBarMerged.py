import sys
import ROOT
from copy import deepcopy
inputFile = sys.argv[1]
outfolder = sys.argv[2]

rFileIn = ROOT.TFile.Open(inputFile)

SRHistos = []


for key in rFileIn.GetListOfKeys():
    keyName = key.GetName()
    SRHistos.append(deepcopy(rFileIn.Get(keyName)))

rFileIn.Close()

for hitso in SRHistos:
    if "ttbarPlusBBbarMerged" in hitso.GetName():
        hitso.SetName(hitso.GetName().replace("ttbarPlusBBbarMerged","ttbarPlusBBbar"))
        hitso.SetTitle(hitso.GetName().replace("ttbarPlusBBbarMerged","ttbarPlusBBbar"))
        


rOutSR = ROOT.TFile(outfolder+"/merged_SR_ddQCD_all_renamed_2.root", "RECREATE")
rOutSR.cd()
for histo in SRHistos:
    histo.Write()

    
