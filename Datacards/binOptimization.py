from __future__ import print_function, division

import sys, os
import logging
import json
from copy import deepcopy

import numpy as np
import math 
import ROOT
from array import array

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/"))
from classes.PlotHelpers import initLogging, project, makeCanvas2D, saveCanvasListAsPDF, moveOverflow2D, moveOverUnderFlow
from classes.ratios import RatioPlot
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)


def getTemplateNames(rFile, category, variable, forSTXS = False):
    """
    Function for filtering the nominal templates for a certain category 
    from the file containing all tempaltes for the datacard
    """
    nominalKeys = [x.GetName() for x in rFile.GetListOfKeys() if (len(x.GetName().split("__")) == 3 and variable in x.GetName())]

    if len(nominalKeys) == 0:
        raise RuntimeError("Did not find any nominal histograms")
        
    nominalKeys = [x for x in nominalKeys if category in x]
    if len(nominalKeys) == 0:
        raise RuntimeError("Could not match any histograms to category: %s"%category)

    signalID = "tH"
    if forSTXS:
        signalID = "TTH"
    
    signalTemplates = [x for x in nominalKeys if signalID in x and not "CR" in x]
    signalCRTemplates = [x for x in nominalKeys if signalID in x and "CR" in x]
    CRTemplates = [x for x in nominalKeys if "CR" in x and not signalID in x]
    backgroundTemplates = [x for x in nominalKeys if (x not in signalTemplates and
                                                      x not in CRTemplates and
                                                      x not in signalCRTemplates and
                                                      "data" not in x)]

    logging.debug("Signal templates: %s", signalTemplates)
    logging.debug("Background templates: %s", backgroundTemplates)
    logging.debug("CR Templates: %s", CRTemplates)

    return signalTemplates, backgroundTemplates, CRTemplates

def getMergedBinsStep1(binning, content, threshold):
    """
    Merge bins with content below passed threshold with adjacent bin left of it.
    If right most bin has content zero after the loop, it will be added to the one
    left of it.
    
    Args:
      binning (list) : List containing 2-element-lists with bin edges
      contents (list) : List with bin content
      threshold (float, int) : Threshold at which bins should be merged
    """

    if len(binning) != len(content):
        raise RuntimeError("Binning and contents have different lengths")
    
    mergedBinning = []
    newBinning = None
    for ic, c in enumerate(content):
        if c <= threshold:
            if newBinning is None:
                newBinning = binning[ic]
            else:
                newBinning[1] = binning[ic][1]
        else:
            if newBinning is not None:
                newBinning[1] = binning[ic][1]
                mergedBinning.append(newBinning)
                newBinning = None
            else:
                mergedBinning.append(binning[ic])
    if newBinning is not None:
        if sum(content[newBinning[0]:newBinning[1]]) <= threshold:
            mergedBinning[-1] = [mergedBinning[-1][0], newBinning[1]]
        else:
            mergedBinning.append(newBinning)

    # For debugging
    for left, right in mergedBinning:
        if right - left != 1:
            logging.debug("Merged bins: [%s, %s]", left, right)
            
    return mergedBinning

def getMergedBinsStep2(binning, sumw2, background, signal, threshold):
    """
    Merge bins with relative error below a threshold. Bin will be merged with bin closest 
    in S/B. If left most merge with right or it and the other way around.

    Args:
      binning (list) : List containing 2-element-lists with bin edges
      sumw2 (list) : List sum of squares for this bin (square of the bin error)
      background (list) : List with background bin content
      signal (list) : List with signal bin content
      threshold (float, int) : Threshold at which bins should be merged. Use 0.01 for e.g. 1%
    """
    if len(binning) != len(sumw2):
        raise RuntimeError("Binning and sumw2 have different lengths")
    if len(binning) != len(background):
        raise RuntimeError("Binning and background have different lengths")
    if len(binning) != len(signal):
        raise RuntimeError("Binning and signal have different lengths")

    relativeErrAboveThreshold = lambda sumw2, content, threshold=threshold : math.sqrt(sumw2)/content >= threshold

    _binning = deepcopy(binning)
    _sumw2 = deepcopy(sumw2)
    _background = deepcopy(background)
    _signal = deepcopy(signal)

    # print(_background)
    # print(_sumw2)
    mergedAll = False
    while not mergedAll:
        # Get next bin that requires a action
        for i in range(len(_binning)):
            foundBin = False
            if relativeErrAboveThreshold(_sumw2[i], _background[i]):
                foundBin = True
                break
        if not foundBin:
            logging.debug("Counld not find bins to merge")
            mergedAll = True
        else:
            logging.debug("Bin %s requires action --> Rel Error = %s", _binning[i], math.sqrt(_sumw2[i])/_background[i])
            logging.debug("Bin %s sumw2 = %s | err = %s | content = %s", _binning[i], _sumw2[i], math.sqrt(_sumw2[i]),_background[i])
            if i == 0:
                logging.debug("This is the left most bin. Will merge with right one")
                i2Merge = i+1
            elif i == len(_binning)-1:
                logging.debug("This is the right most bin. Will merge with left one")
                i2Merge = i-1
            else:
                logging.debug("merging by S/B")
                SB = _signal[i]/_background[i]
                SBDiffLeft = abs(_signal[i-1]/_background[i-1] - SB)
                SBDiffRight = abs(_signal[i+1]/_background[i+1] - SB)
                logging.debug("SBLeft = %s | SBRight = %s", SBDiffLeft, SBDiffRight)
                if SBDiffLeft < SBDiffRight:
                    i2Merge = i-1
                else:
                    i2Merge = i+1
            logging.debug("Will merge with bin %s", i2Merge)

            # Update the lists
            if i2Merge > i:
                _binning[i2Merge] = [_binning[i][0], _binning[i2Merge][1]]
            else:
                _binning[i2Merge] = [_binning[i2Merge][0], _binning[i][1]]
            _sumw2[i2Merge] += _sumw2[i]
            _background[i2Merge] += _background[i]
            _signal[i2Merge] += _signal[i]

            # Remove merged elements
            _binning.pop(i)
            _sumw2.pop(i)
            _background.pop(i)
            _signal.pop(i)

    #print("============================================================")
    #print(_background)
    #print(_binning)
    #print("============================================================")
    
    return _binning


def getMergedBinsStep3(binning, background, signal, threshold):
    """
    Merge adjacent bins whose S/B is closer than the passed threshold.

    Args:
      binning (list) : List containing 2-element-lists with bin edges
      background (list) : List with background bin content
      signal (list) : List with signal bin content
      threshold (float, int) : Absolute difference
    """
    if len(binning) != len(background):
        raise RuntimeError("Binning and background have different lengths")
    if len(binning) != len(signal):
        raise RuntimeError("Binning and signal have different lengths")

    _binning = deepcopy(binning)
    _background = deepcopy(background)
    _signal = deepcopy(signal)

    #print(_background)
    
    mergedAll = False
    while not mergedAll:
        # Get next bin that requires a action
        for i in range(len(_binning)):
            SB = _signal[i]/_background[i]
            rightIsOption = False
            leftIsOption = False
            if i != 0:
                SBDiffLeft = abs(_signal[i-1]/_background[i-1] - SB)
                leftIsOption  = SBDiffLeft <= threshold
            if i != len(_binning)-1:
                SBDiffRight = abs(_signal[i+1]/_background[i+1] - SB)
                rightIsOption = SBDiffRight <= threshold

            
            if (rightIsOption and not leftIsOption):
                logging.debug("Only right is an option. So I'll use that")
                mergeRight = True
                mergeLeft = False
            elif (leftIsOption and not rightIsOption):
                logging.debug("Only left is an option. So I'll use that")
                mergeRight = False
                mergeLeft = True
            elif (rightIsOption and leftIsOption):                
                if SBDiffRight < SBDiffLeft:
                    logging.debug("Both possible. I'll use right")
                    mergeRight = True
                    mergeLeft = False
                else:
                    logging.debug("Both possible. I'll use left")
                    mergeRight = False
                    mergeLeft = True
            else:
                logging.debug("None is an option")
                mergeRight = False
                mergeLeft = False
            
            if mergeRight or mergeLeft:
                break
            
        if not mergeRight and not mergeLeft:
            mergedAll = True
        else:
            if mergeRight:
                logging.debug("Will merge with right bin")
                i2Merge = i+1
            else:
                logging.debug("Will merge with left bin")
                i2Merge = i-1
                

            # Update the lists
            if i2Merge > i:
                _binning[i2Merge] = [_binning[i][0], _binning[i2Merge][1]]
            else:
                _binning[i2Merge] = [_binning[i2Merge][0], _binning[i][1]]
            _background[i2Merge] += _background[i]
            _signal[i2Merge] += _signal[i]

            # Remove merged elements
            _binning.pop(i)
            _background.pop(i)
            _signal.pop(i)

    return _binning
            
        
def applyBinning(binning, content):
    """
    Function for applying a new binning (e.g. output from getMergedBinsStep1) to the passed 
    list of bin contents and sumw2
    """
    newContent = []
    for edges in binning:
        newContent.append(sum(content[edges[0]:edges[1]]))

    return newContent


def resetBinning(binning):
    return [[x, x+1] for x in range(len(binning))]

def getTotalBinning(initialBinnings, finalBinning):
    mapBack = finalBinning
    initialBinnings.reverse()
    for binning in initialBinnings:
        logging.debug("Mapping back %s to %s", mapBack, binning)
        _mapBack = []
        #print(mapBack)
        for group in mapBack:
            if group[1]-group[0] == 1:
                _mapBack.append(binning[group[0]])
            else:
                splitGoups = [[x,x+1] for x in range(group[0], group[1])]
                #print(splitGoups)
                intermediateMap = []
                for splitGroup in splitGoups:
                    #print("Intermediate",binning[splitGroup[0]])
                    intermediateMap.append(binning[splitGroup[0]])
                
                _mapBack.append([intermediateMap[0][0], intermediateMap[-1][1]])

        logging.debug("Mapping %s to %s", mapBack, _mapBack)
        mapBack = _mapBack
        
    return mapBack

def calcAsimovSignificance(signal, background):
    s ,b = np.array(signal), np.array(background)
    return np.sqrt(np.sum(2* ((s + b) * np.log(1 + s/b) - s)))

def getBinLabels(inputHisto, updatedBinning):
    origBinning = []
    for i in range(inputHisto.GetNbinsX()):
        origBinning.append([inputHisto.GetBinLowEdge(i+1), inputHisto.GetBinLowEdge(i+2)])

    labels = []
        
    for _ibin, _bin in enumerate(updatedBinning):
        if _ibin < len(updatedBinning)-1:
            LowEdge, HighEdge = origBinning[_bin[0]][0], origBinning[_bin[1]][0]
            isLastBin = False
        else:
            LowEdge, HighEdge = origBinning[_bin[0]][0], origBinning[-1][1]
            isLastBin = True

        logging.debug("%s | %s - %s",_bin, LowEdge, HighEdge)

        if isLastBin:
            labels.append("[{:1.2f}, {:1.2f}]".format(LowEdge, HighEdge))
        else:
            labels.append("[{:1.2f}, {:1.2f})".format(LowEdge, HighEdge))

    return labels

def translateBinning(inputHisto, updatedBinning):
    origBinning = []
    for i in range(inputHisto.GetNbinsX()):
        origBinning.append([inputHisto.GetBinLowEdge(i+1), inputHisto.GetBinLowEdge(i+2)])

    translated = []
    for _ibin, _bin in enumerate(updatedBinning):
        if _ibin < len(updatedBinning)-1:
            LowEdge, HighEdge = origBinning[_bin[0]][0], origBinning[_bin[1]][0]
        else:
            LowEdge, HighEdge = origBinning[_bin[0]][0], origBinning[-1][1]

        logging.debug("%s | %s - %s",_bin, LowEdge, HighEdge)
        translated.append([LowEdge, HighEdge])        
        
    return translated   
        
def getRebinnedHisto(inputHisto, binning, useEqualSizedBins=True, overwriteEdges=None):
    """ Function for applying the new binning to a histogram  """
    # Translate the binning of  the histogram to the purly counted binning form the optimization
    if useEqualSizedBins:
        if overwriteEdges is None:
            binLow = binning[0][0]
            binHigh = binning[-1][1]
        else:
            binLow = overwriteEdges[0]
            binHigh = overwriteEdges[1]
            
        newHisto = ROOT.TH1D(inputHisto.GetName()+"___rebinned___",
                             inputHisto.GetName()+"___rebinned___",
                             len(binning),
                             binLow,
                             binHigh
                             # inputHisto.GetBinLowEdge(1),
                             # inputHisto.GetBinLowEdge(inputHisto.GetNbinsX()+1)
        )
        newHisto.Sumw2()
    else:
        #if overwriteEdges is None:
        translatedBinning = translateBinning(inputHisto, binning)
        binning_ = translatedBinning # binning
        customBinning = [binning_[0][0], binning_[0][1]]
        for _bin in binning_[1:-1]:
            customBinning.append(_bin[1])
        customBinning.append(binning_[-1][1])
        logging.debug("Custom binning: %s", customBinning)
        # else:
        #     pass
        newHisto = ROOT.TH1D(inputHisto.GetName()+"___rebinned___",
                             inputHisto.GetName()+"___rebinned___",
                             len(binning),
                             array("d", customBinning))
        newHisto.Sumw2()

    origValues = list(inputHisto)
    origSumw2 = list(inputHisto.GetSumw2())
    #print(origValues)
    for _ibin, _bin in enumerate(binning):
        # Bins that need mergeing e.g. [0,2] means 1 and 2 have to be merged so list(range()) gives [0,1,2]
        bins2Merge = list(range(_bin[0], _bin[1]+1))[1::] # List not necessary in python 2 but in 3
        newValue = 0.0
        newError = 0.0
        logging.debug("BNins 2 merge: %s",bins2Merge)
        for _bin2Merge in bins2Merge:
            newValue += origValues[_bin2Merge]
            newError += origSumw2[_bin2Merge]

        newError = math.sqrt(newError)

        logging.debug("Setting bin %s to %s +- %s", _ibin+1, newValue, newError)
        newHisto.SetBinContent(_ibin+1, newValue)
        newHisto.SetBinError(_ibin+1, newError)

    #Generate bin labels
    if overwriteEdges is None:
        newBinLabels = getBinLabels(inputHisto, binning)
        newHisto.GetXaxis().CenterLabels()
        for i in range(len(newBinLabels)):
            newHisto.GetXaxis().SetBinLabel(i+1, newBinLabels[i])
    return newHisto

def makeStackPlot(rFile, newbinning, signal, background, totalBackground, ddQCD , tag, folder, useEqualSizedBins=True, sigLabelText=None):
    logging.debug("Rebinning signal")
    signalHisto = getRebinnedHisto(signal, newbinning, useEqualSizedBins=useEqualSizedBins)
    logging.debug("Rebinning ddQCD")
    ddQCDHisto = getRebinnedHisto(ddQCD, newbinning, useEqualSizedBins=useEqualSizedBins)
    logging.debug("Rebinning ttbar")
    simulation = getRebinnedHisto(background, newbinning, useEqualSizedBins=useEqualSizedBins)
    logging.debug("Rebinning minor")
    signalLine = signalHisto.Clone("Signal_line")
    total_Background = getRebinnedHisto(totalBackground, newbinning, useEqualSizedBins=useEqualSizedBins)

    total_Background.SetLineColor(ROOT.kBlack)
    total_Background.SetMarkerColor(ROOT.kBlack)
    total_Background.SetMarkerStyle(20)
    total_Background.SetMarkerSize(1)
    
    ddQCDHisto.SetFillColor(Helper.colors["ddQCD"])
    ddQCDHisto.SetLineColor(ROOT.kBlack)
    ddQCDHisto.SetFillStyle(1001)

    simulation.SetFillColor(Helper.colors["TTbar_inc"])
    simulation.SetLineColor(ROOT.kBlack)
    simulation.SetFillStyle(1001)

    signalHisto.SetFillColor(Helper.colors["total_signal"])
    signalHisto.SetLineColor(ROOT.kBlack)
    signalHisto.SetFillStyle(1001)

    signalLine.SetLineColor(Helper.colors["total_signal"])
    signalLine.Scale(100)
    signalLine.SetLineWidth(2)
    
    stackSum = ddQCDHisto.Clone("StackSum")
    stackSum.Add(simulation)
    stackSum.Add(signalHisto)

    stack = ROOT.THStack("stack", "stack")
    stack.Add(signalHisto)
    stack.Add(simulation)
    stack.Add(ddQCDHisto)
    legendObjwText = [
        (signalHisto, "Signal"),
        (simulation, "t#bar{t}+Jets + Minor bkgs."),
        (ddQCDHisto, "ddQCD"),
        (signalLine, "Signal * 100", "L"),
    ]

    output = RatioPlot(name = "RebinnedPlot_"+tag)
    output.legendSize = (0.57, 0.5, 0.87, 0.79)
    output.passHistos([total_Background, stack])
    output.ratioRange = (0.7, 1.3)
    output.invertRatio = True
    output.ratioText = "#frac{B only}{S+B}"
    output.CMSLeft = True
    output.CMSOneLine = True
    output.moveCMSTop = 0.09
    output.CMSscale = (1.2, 1.2)
    output.moveCMS = -0.02
    if sigLabelText is not None:
        output.addLabel(sigLabelText, 0.40, 0.952)
    thisPlot = output.drawPlot(
        None,
        "DNN output",
        isDataMCwStack = True,
        stacksum=stackSum,
        addHisto = [(signalLine, "HISTO")],
        #errorband = errorband,
        drawCMS = True,
        histolegend = legendObjwText,
        #drawPulls = drawPulls,
        #floatingMax = 1.23
    )
    
    outFileName = "/RebinnedStack_"+tag
    saveCanvasListAsPDF([thisPlot], outFileName,folder)

    oFile = ROOT.TFile(folder+"/"+outFileName+".root", "RECREATE")
    oFile.cd()
    signalHisto.Write()
    simulation.Write()
    ddQCDHisto.Write()
    total_Background.Write()
    stack.Write()
    oFile.Close()
    
    return [total_Background, ddQCDHisto, simulation, signalHisto, stack]

def getFinalTemplatesSR(rFile, templates, name):
    logging.debug("Initializing %s with %s",name,  templates[0])    

    retTemplate = rFile.Get(templates[0]).Clone(name)
    for template in templates[1::]:
        logging.debug("Adding %s to %s  (Integral = %s)", template, name, rFile.Get(template).Integral())
        retTemplate.Add(rFile.Get(template))

    return retTemplate

def getFinalTemplateddQCD(rFile, templates):
    CRDataName = [x for x in templates if "data" in x][0]
    logging.debug("Initializing ddQCD from %s (Integral = %s)", CRDataName, rFile.Get(CRDataName).Integral())
    ddQCD = rFile.Get(CRDataName).Clone("ddQCD")
    logging.debug("%s -- Sumw2 = %s", CRDataName, list(rFile.Get(CRDataName).GetSumw2())[1:-1])
    for CRBkg in templates:
        if CRBkg == CRDataName:
            continue
        logging.debug("Adding %s to ddQCD (Integral = %s)", CRBkg, rFile.Get(CRBkg).Integral())
        #logging.debug("%s -- Sumw2 = %s", CRBkg, list(rFile.Get(CRBkg).GetSumw2())[1:-1])
        ddQCD.Add(rFile.Get(CRBkg), -1)
        logging.debug("New integral = %s", ddQCD.Integral())

        
    return ddQCD
    
def binOptimization(templates, category, variable, folder = ".", tag = "SomeTag", doPlots=True,
                    threshold_step2=0.03, threshold_step3=0.001, threshold_step1=0, forSTXS=False):
    """
    Main scirpt for producing the optimized bining

    Args:
      templates (str) : Path to file name containing templates (merged file used for datacard creating)
      category (str) : Category to process (fh_j7_t4, fh_j8_t4 or fh_j9_t4)
      variable (str) : Variable to used
      folder (str) : Path to outfolder 
      tag (str) : Tag used for the output filename
      doPlots (bool) : Flag for enabling or disabling the plotting
      threshold_step1 (float, int) : Threshold for step 1 - Merging bins with less than passed entries
      threshold_step2 (float) : Threshold for step 2 - Theshold set the realtive error of the bin at which it should be merged.
                                E.g. passing 0.01 corresponds to relative error of 1% per bin
      threshold_step3 (float) : Threshold for step 3 - Absoulte difference in S/B.

    """
    rFile = ROOT.TFile.Open(templates)

    # Filter the nominal templates from the file
    signalTemplates, backgroundTemplates, CRTemplates = getTemplateNames(rFile, category, variable, forSTXS)

    #Build the background and signal histogrmas
    # Code is a bit more complex than necessary but wanted to make sure if can be checked
    # logging.debug("Initializing background with %s", backgroundTemplates[0])
    # background = rFile.Get(backgroundTemplates[0]).Clone("Background")
    # for SRBkg in backgroundTemplates[1::]:
    #     logging.debug("Adding %s to background  (Integral = %s)", SRBkg, rFile.Get(SRBkg).Integral())
    #     background.Add(rFile.Get(SRBkg))
    background = getFinalTemplatesSR(rFile, backgroundTemplates, "background")

    # CRDataName = [x for x in CRTemplates if "data" in x][0]
    # logging.debug("Initializing ddQCD from %s (Integral = %s)", CRDataName, rFile.Get(CRDataName).Integral())
    # ddQCD = rFile.Get(CRDataName).Clone("ddQCD")
    # for CRBkg in CRTemplates:
    #     if CRBkg == CRDataName:
    #         continue
    #     logging.debug("Adding %s to ddQCD (Integral = %s)", CRBkg, rFile.Get(CRBkg).Integral())
    #     ddQCD.Add(rFile.Get(CRBkg), -1)

    ddQCD = getFinalTemplateddQCD(rFile, CRTemplates)

    logging.debug("Background - Content =%s",list(background)[1:-1])
    logging.debug("ddQCD - Content =%s",list(ddQCD)[1:-1])
    logging.debug("Background - Sumw2 =%s",list(background.GetSumw2())[1:-1])
    logging.debug("ddQCD - Sumw2 =%s",list(ddQCD.GetSumw2())[1:-1])

    
    totalBackground = background.Clone("totalBackground")
    totalBackground.Add(ddQCD)

    # logging.debug("Initializing signal with %s", signalTemplates[0])
    # signal = rFile.Get(signalTemplates[0]).Clone("Signal")
    # for SRSig in signalTemplates[1::]:
    #     logging.debug("Adding %s to signal  (Integral = %s)", SRSig, rFile.Get(SRSig).Integral())
    #     signal.Add(rFile.Get(SRSig))

    signal = getFinalTemplatesSR(rFile, signalTemplates, "signal")
        
    logging.debug("Background %s; Integral = %s", background, background.Integral())
    logging.debug("ddQCD %s; Integral = %s", ddQCD, ddQCD.Integral())
    logging.debug("totalBackground %s; Integral = %s", totalBackground, totalBackground.Integral())
    logging.debug("Signal %s; Integral = %s", signal, signal.Integral())

    #Let the Merging begin
    initBinning = [[x, x+1] for x in range(totalBackground.GetNbinsX())]
    background_Content = list(totalBackground)[1:-1]
    signal_Content = list(signal)[1:-1]
    background_SumW2 = list(totalBackground.GetSumw2())[1:-1]

    assert len(initBinning) == len(background_Content)
    assert len(background_Content) == len(signal_Content)
    assert len(signal_Content) == len(background_SumW2)

    # 1. step: Merge bins with <= threshold_step1 (default = 0)  entries
    logging.info("Processing step 1 - Merging bins with less than %s entries", threshold_step1)
    binning_step1 = getMergedBinsStep1(initBinning, background_Content, threshold_step1)
    
    background_step1 = applyBinning(binning_step1, background_Content)
    background_SumW2_step1 = applyBinning(binning_step1, background_SumW2)
    signal_step1 = applyBinning(binning_step1, signal_Content)
    logging.info("Removed %s bins in step 1", len(initBinning) - len(binning_step1))

    # Reset the binning to have only binwidth 1
    cleanBinning_step1 = resetBinning(binning_step1)

    # 2. step: Merge bins where the rel. error of the bin in larger than  threshold_step2
    logging.info("Processing step 2 - Merging bins with relative error <= %s", threshold_step2)
    binning_step2 = getMergedBinsStep2(cleanBinning_step1, background_SumW2_step1, background_step1, signal_step1, threshold_step2)
    
    background_step2 = applyBinning(binning_step2, background_step1)
    background_SumW2_step2 = applyBinning(binning_step2, background_SumW2_step1)
    signal_step2 = applyBinning(binning_step2, signal_step1)
    logging.info("Removed %s bins in step 2 (total: %s)", len(binning_step1) - len(binning_step2), len(initBinning) - len(binning_step2))
    
    # Reset the binning to have only binwidth 1
    cleanBinning_step2 = resetBinning(binning_step2)
    
    # 3. step: Merge adjacent bins with S/B less than threshold_step3 apart
    logging.info("Processing step 3 - Merging adjacent bins with similar S/B. Threshold = %s", threshold_step3)
    
    binning_step3 = getMergedBinsStep3(cleanBinning_step2, background_step2, signal_step2, threshold_step3)

    background_final = applyBinning(binning_step3, background_step2)
    background_SumW2_final = applyBinning(binning_step3, background_SumW2_step2)
    signal_final = applyBinning(binning_step3, signal_step2)
    logging.info("Removed %s bins in step 3 (total: %s)", len(binning_step2) - len(binning_step3), len(initBinning) - len(binning_step3))

    asimov_sig = calcAsimovSignificance(signal_final, background_final)
    logging.info("Asimov significance for final binning: %s", asimov_sig)

    finalBinning = getTotalBinning([initBinning, binning_step1, binning_step2], binning_step3)
    logging.info("Final binning is %s", finalBinning)
    logging.debug("  Step 3  : %s",  binning_step3)
    logging.debug("  Step 2  : %s",  binning_step2)
    logging.debug("  Step 1  : %s",  binning_step1)
    logging.debug("  initial : %s",  initBinning)
    if doPlots:
        logging.info("Producing plots")
        makeStackPlot(rFile,
                      finalBinning,
                      signal, background, totalBackground, ddQCD,
                      tag, folder)


    #Save info for further processing
    outInfo = {
        # Settings
        "category" : category,
        "variables" : variable,
        "theshold_step1" : threshold_step1,
        "theshold_step2" : threshold_step2,
        "theshold_step3" : threshold_step3,
        # Results
        "binning_final" : finalBinning,
        # Not really necessary
        "binning_step3" : binning_step3,
        "binning_step2" : binning_step2,
        "binning_step1" : binning_step1,
        "binning_step2_clean" : cleanBinning_step2,
        "binning_step1_clean" : cleanBinning_step1,
        "binning_initial" : initBinning,
        # Aux info
        "Asimov_sig" : asimov_sig,
        #To be filled later
        "Fit_mu" : None,
        "Fit_err_up" : None,
        "Fit_err_down" : None,
    }

    logging.debug("\n %s", json.dumps(outInfo, sort_keys=True,
                                      separators=(',', ': ')))

    jsonOutFile = folder+"/binningOptimiziaton_"+tag+".json"
    logging.info("Writing results to output file to: %s", jsonOutFile)
    
    with open(jsonOutFile, "w") as o:
        json.dump(outInfo, o, sort_keys=True, indent=4, separators=(',', ': '))


if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description='Description')
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--input",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--cat",
        action = "store",
        help = "Pass category to process",
        type = str,
        required = True
    )    
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Tag for output file",
        type = str,
        required = True
    )    
    argumentparser.add_argument(
        "--var",
        action = "store",
        help = "Variable to process",
        type = str,
        default="DNN_Node0"
    )    
    argumentparser.add_argument(
        "--folder",
        action = "store",
        help = "Variable to process",
        type = str,
        default="out_binOptimization"
    )
    argumentparser.add_argument(
        "--step1",
        action = "store",
        help = "Threshold for step one",
        type = float,
        default=0.0
    )    
    argumentparser.add_argument(
        "--step2",
        action = "store",
        help = "Threshold for step two",
        type = float,
        default=0.03
    )    
    argumentparser.add_argument(
        "--step3",
        action = "store",
        help = "Threshold for step three",
        type = float,
        default=0.001
    )    
    argumentparser.add_argument(
        "--forSTXS",
        action = "store_true",
        help = "Pass if processing STXS samples"
    )
    args = argumentparser.parse_args()

    initLogging(args.logging, funcLen = 25)

    
    binOptimization(args.input, args.cat, args.var, args.folder, args.tag,
                    threshold_step1 = args.step1,
                    threshold_step2 = args.step2,
                    threshold_step3 = args.step3,
                    forSTXS = args.forSTXS)
