import sys
import os
import logging
import ROOT
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from copy import deepcopy


def calcRelativeTRFTemplates(nomTRFFile, varTRFFile, nomFile, sysName, var="DNN_Node0", forceDir=False):
    logging.debug("Got Arguments")
    logging.debug("  nomTRFFile : %s", nomTRFFile)
    logging.debug("  varTRFFile : %s", varTRFFile)
    logging.debug("  nomFile    : %s", nomFile)
    logging.debug("  sysName    : %s", sysName)
    logging.debug("  var        : %s", var)

    rNomTRF = ROOT.TFile.Open(nomTRFFile)
    rVarTRF = ROOT.TFile.Open(varTRFFile)
    rNom = ROOT.TFile.Open(nomFile)

    logging.info("rNomTRF: %s",rNomTRF)
    logging.info("rVarTRF: %s",rVarTRF)
    logging.info("rNom: %s", rNom)
    
    allKeys = [key.GetName() for key in rNomTRF.GetListOfKeys()]
    cats = list(set([key.split("__")[1] for key in allKeys ]))
    procs = list(set([key.split("__")[0] for key in allKeys ]))
    
    logging.info("Got cats: %s", cats)
    logging.info("Got procs: %s", procs)

    newVariatedTemplates = []
    for cat in cats:
        for proc in procs:
            logging.info("Processing: var %s | cat %s | proc %s", var, cat, proc)
            TRFKeyName = "{}__{}__{}".format(proc, cat, var)
            if TRFKeyName not in allKeys:
                logging.warning("Skipping %s because not in all keys", TRFKeyName)
                continue 
            
            binValsNomTRF = list(rNomTRF.Get(TRFKeyName))
            binValsVarTRF = list(rVarTRF.Get(TRFKeyName))
            logging.debug("binValsNomTRF: %s", binValsNomTRF)
            logging.debug("binValsVarTRF: %s", binValsVarTRF)
            diff = [binValsVarTRF[i]-binValsNomTRF[i] for i in range(len(binValsNomTRF))]
            assert len(diff) == len(binValsVarTRF)

            if forceDir:
                if "up" in varTRFFile.split("/")[-1]:
                    logging.info("Forcing up")
                    diff = [abs(x) for x in diff]
                elif "down" in varTRFFile.split("/")[-1]:
                    logging.info("Forcing down")
                    diff = [-abs(x) for x in diff]
                else:
                    raise RuntimeError
                
            logging.debug("bin diff: %s", diff)
            
            nomCat = cat.replace("_TRF", "")

            nomKey = "{}__{}__{}".format(proc, nomCat, var)

            logging.info("NomKey: %s", nomKey)
            
            thisNomHisto = deepcopy(rNom.Get(nomKey))
            thisNomHisto.SetName(TRFKeyName)
            thisNomHisto.SetTitle(TRFKeyName)
            logging.debug("Nominal template bin content: %s", list(thisNomHisto))

            for i in range(len(diff)):
                thisNomHisto.SetBinContent(i, thisNomHisto.GetBinContent(i)+diff[i])

            newVariatedTemplates.append(deepcopy(thisNomHisto))

    outFileName = varTRFFile.replace(".root", "_{}_relTRF.root".format(var))
    #outFileName = varTRFFile.replace(".root", "_{}_relTRF{}.root".format(var, "_forceDir" if forceDir else ""))

    logging.info("Will save new templates as %s", outFileName)
    rOut = ROOT.TFile(outFileName, "RECREATE")
    rOut.cd()

    for h in newVariatedTemplates:
        h.Write()

    rOut.Close()
    

            
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description='Description')
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--nomTRF",
        action = "store",
        help = "Input file:  Nominal samples w/ TRF applied",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--varTRF",
        action = "store",
        help = "Input file:  Variated sample w/ TRF applied",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--nom",
        action = "store",
        help = "Input file:  Nominal samples w/o TRF applied",
        type = str,
        required = True
    )
    # argumentparser.add_argument(
    #     "--sysName",
    #     action = "store",
    #     help = "Name of the systematic in the output",
    #     type = str,
    #     required = True,
    #     choices = [
            
    #         ]
    # )
    argumentparser.add_argument(
        "--var",
        action = "store",
        help = "Variable",
        type = str,
        default = "DNN_Node0"
    )
    # argumentparser.add_argument(
    #     "--isCR",
    #     action = "store_true",
    #     help = "Pass if you want to process CR templates",
    # )
    argumentparser.add_argument(
        "--forceDir",
        action = "store_true",
        help = "Pass if you want to force up to fluctuate up and down to fluctuate down",
    )

    
    args = argumentparser.parse_args()

    initLogging(args.logging, funcLen = 25)

    
    calcRelativeTRFTemplates(
        args.nomTRF,
        args.varTRF,
        args.nom,
        None, #args.sysName,
        args.var,
        args.forceDir
    )
    
    
    
