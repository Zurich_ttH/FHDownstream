import ROOT
import sys
from copy import deepcopy

inFile = sys.argv[1]
outfile = sys.argv[2]

rFile = ROOT.TFile.Open(inFile)
keys = rFile.GetListOfKeys()

outHistos = []


mergeMap = {
    "ttbarZ" : "ttbarV",
    "ttbarW" : "ttbarV",
    "wjets" : "vjets",
    "zjets" : "vjets",

    }

merge = {
    "ttbarV" : ["ttbarZ", "ttbarW"],
    "vjets" : ["wjets", "zjets"]
}



rOutFile = ROOT.TFile(outfile, "RECREATE")
rOutFile.cd()
keyMap = {}
mergedHistos = {}
for ikey, key in enumerate(keys):
    histo = key.ReadObj()
    keyMap[key.GetName()] = deepcopy(key)
    Namekey = key.GetName()
    proc = Namekey.split("__")[0]
    if proc in mergeMap.keys():
        mergedName = Namekey.replace(proc, mergeMap[proc])
        histo = key.ReadObj()
        if mergedName not in mergedHistos:
            mergedHistos[mergedName] = histo.Clone(mergedName)
            print "Cloning",Namekey,"to",mergedName,"Output int", mergedHistos[mergedName].Integral()
        else:
            print "int pre", mergedHistos[mergedName].Integral(), "adding", histo.Integral()
            mergedHistos[mergedName].Add(histo)
            print "Adding",Namekey,"to",mergedName,"Output int", mergedHistos[mergedName].Integral()
    if proc in mergeMap.keys():
        continue
    histo.Write()

for key in mergedHistos:
    mergedHistos[key].Write()

print "INput keys {0}".format(len(keys))
print "Output keys {0}".format(len(rOutFile.GetListOfKeys()))
        
rOutFile.Close()
