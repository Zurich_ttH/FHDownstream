import sys, os
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/classes/"))
from PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos
from ratios import RatioPlot
import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.DEBUG,
)
#############################################################
#############################################################
baseDir = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v2"
plot4process = ["ddQCD","ttbarOther","ttH_hbb","ttbarPlusBBbar","ttbarPlusCCbar","ttbarPlusB","ttbarPlus2B"]
preSel = False
tag = baseDir.split("/")[-1]
identifier = "L1Pre"
if not preSel:
    thisFile = "merged_SR_ddQCD_nom.root"
    thisFile = "merged_SR_new_ddQCD_all.root"
    inputFile = {
        "fh_j9_t4" : baseDir+"/"+thisFile,
        "fh_j8_t4" : baseDir+"/"+thisFile,
        "fh_j7_t4" : baseDir+"/"+thisFile,
        "fh_j9_t3" : baseDir+"/"+thisFile,
        "fh_j8_t3" : baseDir+"/"+thisFile,
        "fh_j7_t3" : baseDir+"/"+thisFile
    }
    plot4cat = ["fh_j9_t4","fh_j8_t4","fh_j7_t4", "fh_j9_t3","fh_j8_t3","fh_j7_t3"]
    plot4disc = {
        "fh_j9_t4" : "mem_FH_4w2h2t_p",
        "fh_j8_t4" : "mem_FH_3w2h2t_p",
        "fh_j7_t4" : "mem_FH_3w2h2t_p",
        "fh_j9_t3" : "mem_FH_4w2h1t_p",
        "fh_j8_t3" : "mem_FH_4w2h1t_p",
        "fh_j7_t3" : "mem_FH_4w2h1t_p"
    }
else:
    baseDir = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/Datacards/sparsev5_final_v1_unblind_V4_PreSel/cards"
    plot4process = ["ttH_hbb","ttbarPlusBBbar","singlet","ttbarOther","ttbarPlusCCbar","ttbarPlusB","ttbarPlus2B","qcd"]
    identifier = "Presel_Datacard" 
    inputFile = {
        "fh_presel" : baseDir+"/fh_presel__numJets.root",
    }
    plot4cat = ["fh_presel"]
    plot4disc = {
        "fh_presel" : "numJets",
    }



for proc in plot4process:
    logging.info("Processing: %s", proc)
    for cat in plot4cat:
        rFile = ROOT.TFile.Open(inputFile[cat])
        allHistos = []
        for key in rFile.GetListOfKeys():
            allHistos.append(key.GetName())
        allHistos = set(allHistos)
        logging.info("Processing: %s", cat)
        allCanvases = []
        hnames = []
        for key in rFile.GetListOfKeys():
            keyName = key.GetName()
            if proc in keyName and cat in keyName:
                hnames.append(keyName)
        systematics = []
        for hname in hnames:
            #only look at systeamtics
            if len(hname.split("__")) == 4:
                systName = hname.split("__")[-1]
                if systName.endswith("Down"):
                    systematics.append(systName[:-len("Down")])
                if systName.endswith("Up"):
                    systematics.append(systName[:-len("Up")])
        systematics = set(systematics)
        for systematic in systematics:
            print systematic
            upSys = "{0}__{1}__{2}__{3}Up".format(proc, cat, plot4disc[cat], systematic)
            downSys = "{0}__{1}__{2}__{3}Down".format(proc, cat, plot4disc[cat], systematic)
            nom = "{0}__{1}__{2}".format(proc, cat, plot4disc[cat])
            if upSys not in allHistos:
                logging.warning("Did not fine Up systematic for %s", systematic)
                logging.debug("Query for: %s",upSys)
                logging.info("Replacing with nominal: %s", nom)
                upSys = nom
            if downSys not in allHistos:
                logging.warning("Did not fine Down systematic for %s", systematic)
                logging.debug("Query for: %s",downSys)
                logging.info("Replacing with nominal: %s", nom)
                downSys = nom
            
            
                
            histoList = [rFile.Get(nom).Clone("nom_{0}_{1}_{2}".format(proc, cat, systematic)),
                         rFile.Get(upSys).Clone("up_{0}_{1}_{2}".format(proc, cat, systematic)),
                         rFile.Get(downSys).Clone("down_{0}_{1}_{2}".format(proc, cat, systematic))]


            histoList[0].SetLineColor(ROOT.kBlack)
            histoList[1].SetLineColor(ROOT.kRed)
            histoList[2].SetLineColor(ROOT.kBlue)
            thisRatio = RatioPlot(systematic)
            thisRatio.ratioRange = (0.95, 1.05)
            thisRatio.legendSize =  (0.3,0.5,0.7,0.95)
            thisRatio.passHistos(histoList)
            allCanvases.append(
                thisRatio.drawPlot(
                    ["Nominal", "{0}Up".format(systematic), "{0}Down".format(systematic)],
                    xTitle =  plot4disc[cat],
                    noErr = True
                )
            )
            # allCanvases.append(makeCanvasOfHistos(systematic, plot4disc[cat], histoList,
            #                                       legendText = ["Nominal", "{0}Up".format(systematic), "{0}Down".format(systematic)],
            #                                       colorList = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed], lineWidth = 2))
        print allCanvases
        saveCanvasListAsPDF(allCanvases, "sparseSysComp_{0}_{1}_{2}_{3}".format(tag, identifier, proc, cat), ".")



        
