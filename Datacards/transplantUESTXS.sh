#!/bin/bash
FOLDER=$1
FOLDERMERGED=$2

python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 

python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 

python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1 


hadd ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_DNN_Node0_fh_STXS_*.root
hadd ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_DNN_Node0_fh_STXS*.root

python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics_rebinned.root \
       ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics_rebinned.root \
       ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ttbarPlusBBbarMerged
