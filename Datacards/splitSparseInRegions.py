import sys
import ROOT
from copy import deepcopy
inputFile = sys.argv[1]
outfolder = sys.argv[2]

rFileIn = ROOT.TFile.Open(inputFile)

SRHistos = []
CRHistos = []

for key in rFileIn.GetListOfKeys():
    keyName = key.GetName()
    if "_CR" in keyName:
        CRHistos.append(deepcopy(rFileIn.Get(keyName)))
    elif "SR" in keyName:
        SRHistos.append(deepcopy(rFileIn.Get(keyName)))
    else:
        raise RuntimeError("Somethign weird happend")

rFileIn.Close()

# for hitso in CRHistos:
#     hitso.SetName(hitso.GetName().replace("_CR__","__"))
#     hitso.SetTitle(hitso.GetName().replace("_CR__","__"))

# for hitso in SRHistos:
#     hitso.SetName(hitso.GetName().replace("_SR__","__"))
#     hitso.SetTitle(hitso.GetName().replace("_SR__","__"))


rOutSR = ROOT.TFile(outfolder+"/merged_SR.root", "RECREATE")
rOutSR.cd()
for histo in SRHistos:
    histo.Write()

rOutCR = ROOT.TFile(outfolder+"/merged_CR.root", "RECREATE")
rOutCR.cd()
for histo in CRHistos:
    histo.Write()

    
