import sys, os
import logging
from copy import deepcopy

import ROOT

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from makeDatacards import replChannel, replVariable, replProcess, replSystematic


def renameVariatedSamples(inputFile, outputSystmatic, sdir, cats,
                          templateSchemaNominal = "$PROCESS__$CHANNEL__$VARIABLE",
                          templateSchemaSystematic = "$PROCESS__$CHANNEL__$VARIABLE__$SYSTEMATIC"):
    
    folder = "/".join(inputFile.split("/")[0:-1])
    logging.info("Operating folder: %s", folder)

    rFile = ROOT.TFile.Open(inputFile)

    allKeys = [x.GetName() for x in rFile.GetListOfKeys()]

    nominals = [x for x in allKeys if len(x.split("__"))==3]

    _procs = list(set([x.split("__")[0] for x in nominals]))
    _cats = list(set([x.split("__")[1] for x in nominals]))
    _vars = list(set([x.split("__")[2] for x in nominals]))

    logging.info("Procs: %s", _procs)
    logging.info("Vars: %s", _vars)
    logging.info("Cats: %s", _cats)

    renamedTemplates = []
    
    for proc in _procs:
        for var in _vars:
            for cat in _cats:
                nominalName = replVariable(replChannel(replProcess(templateSchemaNominal, proc), cat), var)
                systName = replSystematic(replVariable(replChannel(replProcess(templateSchemaSystematic, proc), cat.replace("_TRF","")), var), outputSystmatic+"_"+proc+sdir)
                logging.debug("Renaming:")
                logging.debug("  %s", nominalName)
                logging.debug("    to:")
                logging.debug("  %s", systName)

                try:
                    tempTemplate = rFile.Get(nominalName).Clone(systName)
                    tempTemplate.SetTitle(systName)
                except ReferenceError:
                    logging.error("Skipping %s", nominalName)
                    continue

                renamedTemplates.append(
                    deepcopy(tempTemplate)
                )

    outName = inputFile.split("/")[-1].replace(".root","")+"_renamed.root"
    logging.info("Will use output file %s", outName)
    outFile = ROOT.TFile(folder+"/"+outName, "RECREATE")
    outFile.cd()

    for template in renamedTemplates:
        logging.info("Writing: %s (%s)", template.GetName(), template)
        template.Write()
    
    
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--input",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--systematic",
        action = "store",
        help = "Systamtic prostfix",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--systematicDir",
        action = "store",
        help = "Systamtic direction",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--cats",
        action = "store",
        help = "Categories to process",
        nargs = "+",
        default = ["fh_j7_t4", "fh_j8_t4", "fh_j9_t4"]
    )
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    
    renameVariatedSamples(args.input, args.systematic, args.systematicDir, args.cats)
