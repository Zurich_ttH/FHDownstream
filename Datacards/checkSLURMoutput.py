import sys
import glob


folder = sys.argv[1]

for file_ in glob.glob(folder+"/*.out"):
    with open(file_, "r") as f:
        data = f.read()
        if "SUCCESS" in data:
            print "File %s is good"%file_.split("/")[-1]
        elif "FAILED" in data:
            print "File %s is bad"%file_.split("/")[-1]
            print "Check: %s"%file_.split("/")[-1].replace(".out", ".err")
        else:
            print "File %s is weird"%file_.split("/")[-1]
            
