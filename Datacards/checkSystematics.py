import sys, os
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/classes/"))
from PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos
from ratios import RatioPlot
import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.DEBUG,
)
#############################################################
#############################################################
baseDir = "/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/Baseline_2017_v1p2"
inputFile = {
    "fh_j9_t4" : baseDir+"/fh_j9_t4_ht30.root",
    "fh_j8_t4" : baseDir+"/fh_j8_t4_ht30.root",
    "fh_j7_t4" : baseDir+"/fh_j7_t4_ht30.root",
}
plot4process = ["ttlf"]#,"ttH_hbb","ttbarOther","ttbarPlusBBbar","ttbarPlusCCbar","ttbarPlusB","ttbarPlus2B"]
plot4cat = ["fh_j8_t4"]#,"fh_j9_t4","fh_j7_t4", "fh_j9_t3","fh_j8_t3","fh_j7_t3"]
plot4disc = {
    "fh_j9_t4" : "ht30",
    "fh_j8_t4" : "ht30",
    "fh_j7_t4" : "ht30",
}



for proc in plot4process:
    logging.info("Processing: %s", proc)
    for cat in plot4cat:
        rFile = ROOT.TFile.Open(inputFile[cat])
        allHistos = []
        for key in rFile.GetListOfKeys():
            allHistos.append(key.GetName())
        allHistos = set(allHistos)
        logging.info("Processing: %s", cat)
        allCanvases = []
        hnames = []
        for key in rFile.GetListOfKeys():
            keyName = key.GetName()
            if proc in keyName and cat in keyName:
                hnames.append(keyName)
        systematics = []
        for hname in hnames:
            #only look at systeamtics
            if len(hname.split("__")) == 3:
                systName = hname.split("__")[-1]
                if systName.endswith("Down"):
                    systematics.append(systName[:-len("Down")])
                if systName.endswith("Up"):
                    systematics.append(systName[:-len("Up")])
        systematics = set(systematics)
        logging.info("Found %s systematics", len(list(systematics)))
        for systematic in systematics:
            print systematic
            upSys = "{0}_{1}__{2}__{3}Up".format(cat, plot4disc[cat], proc, systematic)
            downSys = "{0}_{1}__{2}__{3}Down".format(cat, plot4disc[cat], proc, systematic)
            nom = "{0}_{1}__{2}".format(cat, plot4disc[cat], proc)
            if upSys not in allHistos:
                logging.warning("Did not fine Up systematic for %s", systematic)
                logging.debug("Query for: %s",upSys)
                logging.info("Replacing with nominal: %s", nom)
                upSys = nom
            if downSys not in allHistos:
                logging.warning("Did not fine Down systematic for %s", systematic)
                logging.debug("Query for: %s",downSys)
                logging.info("Replacing with nominal: %s", nom)
                downSys = nom
            
            
                
            histoList = [rFile.Get(nom).Clone("nom_{0}_{1}_{2}".format(proc, cat, systematic)),
                         rFile.Get(upSys).Clone("up_{0}_{1}_{2}".format(proc, cat, systematic)),
                         rFile.Get(downSys).Clone("down_{0}_{1}_{2}".format(proc, cat, systematic))]


            histoList[0].SetLineColor(ROOT.kBlack)
            histoList[1].SetLineColor(ROOT.kRed)
            histoList[2].SetLineColor(ROOT.kBlue)
            thisRatio = RatioPlot(systematic)
            thisRatio.ratioRange = (0.65, 1.35)
            thisRatio.legendSize = (0.3,0.7,0.7,0.90)
            thisRatio.passHistos(histoList)
            allCanvases.append(thisRatio.drawPlot(["Nominal", "{0}Up".format(systematic), "{0}Down".format(systematic)], xTitle =  plot4disc[cat]))
            # allCanvases.append(makeCanvasOfHistos(systematic, plot4disc[cat], histoList,
            #                                       legendText = ["Nominal", "{0}Up".format(systematic), "{0}Down".format(systematic)],
            #                                       colorList = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed], lineWidth = 2))
            
        saveCanvasListAsPDF(allCanvases, "sysComp_{0}_{1}".format(proc, cat), "sysCheck_datacards/Baseline_v1p2/")



        
