from __future__ import print_function

import sys, os
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/classes/"))

from PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, checkNcreateFolder, getFullColorList
from ratios import RatioPlot
import ROOT
import logging
from copy import deepcopy
from glob import glob

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def compareSpareOutput(referenceFile, files, proc, cat, var, outPrefix, outfolder):
    histos = {"nominal" : []}
    
    refKeys = [key.GetName() for key in referenceFile.GetListOfKeys()]
    nomKey, systKeys = getKeys(refKeys, proc, cat, var)

    systs = [syst.split("__")[-1] for syst in systKeys]
    
    histos["nominal"].append(referenceFile.Get(nomKey))
    
    for sys in systs:
        histos[sys] = []
        for systKey in systKeys:
            if sys in systKey:
                histos[sys].append(referenceFile.Get(systKey))

#    iFiles = 0# TEMP
    for _file in sorted(files.keys()):
        refKeys = [key.GetName() for key in files[_file].GetListOfKeys()]
        nomKey, systKeys = getKeys(refKeys, proc, cat, var)
        histos["nominal"].append(files[_file].Get(nomKey))
        for sys in systs:
            for systKey in systKeys:
                if sys in systKey:
                    histos[sys].append(files[_file].Get(systKey))
#        iFiles += 1# TEMP
#        if iFiles == 3:# TEMP
#            break # TEMP
    canvases = []

    allColors = getFullColorList()
    for tempalteGropu in sorted(histos.keys()):
        logging.info("Processing group %s", tempalteGropu)
        
        thisRatioPlot = RatioPlot(tempalteGropu)

        for ih, h in enumerate(histos[tempalteGropu]):
            if ih==0:
                h.SetLineWidth(2)
                h.SetLineColor(1)
            else:
                h.SetLineColor(allColors[ih-1])


        thisRatioPlot.ratioRange = (0, 2.5)
        thisRatioPlot.ratioText = "#frac{Variation}{Nominal}"
        thisRatioPlot.invertDrawOrder = True
        thisRatioPlot.passHistos(histos[tempalteGropu])
        print(thisRatioPlot.histos)
        
        thisRatioPlot.addLabel(tempalteGropu, yStart=0.95)
        canvases.append(
            thisRatioPlot.drawPlot(
                 None
            )
        )

    saveCanvasListAsPDF(
        canvases,
        "{}_{}_{}_{}".format(outPrefix, proc, cat, var),
        outfolder
    )
    
                
def getKeys(keys, proc, cat, var):
    nomKeys = None
    systKeys = []
    
    for key in keys:
        if not (proc in key and cat in key and var in key):
            continue
        
        if len(key.split("__")) == 3:
            nomKeys = key
        else:
            if "Up" in key or "Down" in key:
                systKeys.append(key)

    return nomKeys, systKeys


if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description="Compare all files in a directory by plotting the same template")
    argumentparser.add_argument("--logging", help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages", type=int,default=20)
    argumentparser.add_argument("--input", help="Input folder containing the files", type=str,required=True)
    argumentparser.add_argument("--folder", help="Ouptut folder", type=str,default=".")
    argumentparser.add_argument("--prefix", help="Ouptut prfix", type=str,required=True)
    argumentparser.add_argument("--referenceID", help="Identifier of the reference file", type=str,default="_nom")
    argumentparser.add_argument("--proc", nargs="+", help="ProcessName", type=str, default=["tHq_hbb"])
    argumentparser.add_argument("--var", nargs="+", help="Variables", type=str, default=["DNN_Node0"])
    argumentparser.add_argument("--cat", nargs="+", help="Categories", type=str, default=["fh_j7_t4_SR","fh_j8_t4_SR","fh_j9_t4_SR","fh_j7_t4_CR","fh_j8_t4_CR","fh_j9_t4_CR"])
    args = argumentparser.parse_args()
     
    initLogging(args.logging, "25")

    checkNcreateFolder(args.folder)

    rFiles = {}
    rFileReference = None
    
    for _file in glob(args.input+"/*.root"):
        fileName = _file.split("/")[-1]
        if fileName.replace(".root","").endswith(args.referenceID):
            logging.info("Reference file: %s", _file)
            rFileReference = ROOT.TFile.Open(_file)
        else:
            rFiles[fileName] = ROOT.TFile.Open(_file)

    if rFileReference is None:
        raise RuntimeError("Could not find reference File")

    logging.info("Found  %s file", len(rFiles.keys()))

    
    for proc in args.proc:
        logging.info("Processing process %s", proc)
        for var in args.var:
            logging.info("Processing variable %s", var)
            for cat in args.cat:
                logging.info("Processing category %s", cat)
    
                compareSpareOutput(rFileReference, rFiles, proc, cat, var, args.prefix, args.folder)
                
