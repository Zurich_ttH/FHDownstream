#!/bin/bash
FOLDER=$1
FILEBASE=$2
POSTFIX="rebinnedv3"


python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_0_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN0_v3p3_60Bins_innerv2.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_1_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN1_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_2_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN2_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_3_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN3_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_4_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN4_v3p3_60Bins_innerv2.json --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_0_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN0_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_1_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN1_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_2_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN2_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_3_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN3_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_4_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN4_v3p3_60Bins_innerv2.json --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_0_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN0_v3p3_60Bins_innerv2.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_1_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN1_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_2_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN2_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_3_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN3_v3p3_60Bins_inner.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input ${FOLDER}/${FILEBASE}.root --outFolder ${FOLDER} --cat fh_STXS_4_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN4_v3p3_60Bins_innerv2.json --binEdgeLow 0 --binEdgeHigh 1


hadd ${FOLDER}/${FILEBASE}_${POSTFIX}.root ${FOLDER}/${FILEBASE}_DNN_Node0*.root 
rm -v ${FOLDER}/${FILEBASE}_DNN_Node0*.root 
