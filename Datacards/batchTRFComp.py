from makeTRFComp import main as makeTRFComp
import itertools

if __name__ == "__main__":
    files = [
        "TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root",
        "TTToHadronic_TuneCP5down_13TeV-powheg-pythia8.root",
        "TTToHadronic_TuneCP5up_13TeV-powheg-pythia8.root",
        "TTToHadronic_hdampDOWN_TuneCP5_13TeV-powheg-pythia8.root",
        "TTToHadronic_hdampUP_TuneCP5_13TeV-powheg-pythia8.root",
        "TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root",
        "TTTo2L2Nu_TuneCP5down_13TeV-powheg-pythia8.root",
        "TTTo2L2Nu_TuneCP5up_13TeV-powheg-pythia8.root",
        "TTTo2L2Nu_hdampDOWN_TuneCP5_13TeV-powheg-pythia8.root",
        "TTTo2L2Nu_hdampUP_TuneCP5_13TeV-powheg-pythia8.root",
        "TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root",
        "TTToSemiLeptonic_TuneCP5down_13TeV-powheg-pythia8.root",
        "TTToSemiLeptonic_TuneCP5up_13TeV-powheg-pythia8.root",
        "TTToSemiLeptonic_hdampDOWN_TuneCP5_13TeV-powheg-pythia8.root",
        "TTToSemiLeptonic_hdampUP_TuneCP5_13TeV-powheg-pythia8.root",
        "TTbb_4f_TTTo2l2nu_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTTo2l2nu_hdampDOWN_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTTo2l2nu_hdampUP_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTToHadronic_hdampDOWN_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTToHadronic_hdampUP_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTToSemiLeptonic_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTToSemiLeptonic_hdampDOWN_TuneCP5-Powheg-Openloops-Pythia8.root",
        "TTbb_4f_TTToSemiLeptonic_hdampUP_TuneCP5-Powheg-Openloops-Pythia8.root",
    ]


    for Region, jetCat, year in itertools.product(["SR","CR"], ["7J", "8J", "9J"], ["2016", "2017", "2018"]):
        print "-----------------------------------------------------"
        print Region, jetCat, year
        print "-----------------------------------------------------"

        if year == "2017":
            lumi = "41.5"
        if year == "2016":
            lumi = "35.9"
        if year == "2018":
            lumi = "59.7"
        
        if Region == "SR":
            if year == "2017": 
                folderTRF = "~/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v5/TRFTesting/v3/SR_inclKNN_MapsPerProc/allCats"
                #folderTRF2 = "~/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v5/TRFTesting/v3/SR_perProcKNN_MapsPerProc"
                folderNom = "~/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v5/TRFTesting/v3/SR_nominal"
                outFolder = "out_spare/v3/v2/2017/SR_CompKNNs/"+jetCat+"/"
            if year == "2016":
                folderTRF = "/scratch/koschwei/ttH/sparse/LegacyRun2/2016/v3p3/v5/TRFTesting/v3/SR_inclKNN_MapsPerProc/allCats"
                folderNom = "/scratch/koschwei/ttH/sparse/LegacyRun2/2016/v3p3/v5/TRFTesting/v3/SR_nominal/aalCats"
                outFolder = "out_spare/v3/v2/2016/SR_CompKNNs/"+jetCat+"/"
            if year == "2018":
                folderTRF = "/scratch/koschwei/ttH/sparse/LegacyRun2/2018/v3p3/v5/TRFTesting/v3/SR_inclKNN_MapsPerProc/allCats"
                folderNom = "/scratch/koschwei/ttH/sparse/LegacyRun2/2018/v3p3/v5/TRFTesting/v3/SR_nominal/allCats"
                outFolder = "out_spare/v3/v2/2018/SR_CompKNNs/"+jetCat+"/"

            if jetCat == "9J":
                catTRF = ["fh_j9_t4_TRF_SR"]
                catNom = "fh_j9_t4_TRF_SR"
            if jetCat == "8J":
                catTRF = ["fh_j8_t4_TRF_SR"]
                catNom = "fh_j8_t4_TRF_SR"
            if jetCat == "7J":
                catTRF = ["fh_j7_t4_TRF_SR"]
                catNom = "fh_j7_t4_TRF_SR"

            legNom = "SR"
            legTRF = ["TRF SR"]

            plotVars = ["DNN_Node0"]
            isSR = True


        if Region == "CR":
            if year == "2017":
                folderTRF = "~/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v5/TRFTesting/v3/CR_inclKNN_MapsPerProc"
                #folderTRF2 = "~/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v5/TRFTesting/v3/CR_perProcKNN_MapsPerProc"
                folderNom = "~/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v5/TRFTesting/v3/CR_nominal/allCats"
                outFolder = "out_spare/v3/v2/2017/CR_CompKNNs/"+jetCat+"/"
            if year == "2016":
                folderTRF = "/scratch/koschwei/ttH/sparse/LegacyRun2/2016/v3p3/v5/TRFTesting/v3/CR_inclKNN_MapsPerProc/allCats"
                folderNom = "/scratch/koschwei/ttH/sparse/LegacyRun2/2016/v3p3/v5/TRFTesting/v3/CR_nominal/allCats"
                outFolder = "out_spare/v3/v2/2016/CR_CompKNNs/"+jetCat+"/"
            if year == "2018":
                folderTRF = "/scratch/koschwei/ttH/sparse/LegacyRun2/2018/v3p3/v5/TRFTesting/v3/CR_inclKNN_MapsPerProc/allCats"
                folderNom = "/scratch/koschwei/ttH/sparse/LegacyRun2/2018/v3p3/v5/TRFTesting/v3/CR_nominal/allCats"
                outFolder = "out_spare/v3/v2/2018/CR_CompKNNs/"+jetCat+"/"


            #catTRF = ["fh_j9_t4_TRF_CR", "fh_j9_t4_TRF_CR"]

            if jetCat == "9J":
                catTRF = ["fh_j9_t4_TRF_CR"]
                catNom = "fh_j9_t4_TRF_CR"
            if jetCat == "8J":
                catTRF = ["fh_j8_t4_TRF_CR"]
                catNom = "fh_j8_t4_TRF_CR"
            if jetCat == "7J":
                catTRF = ["fh_j7_t4_TRF_CR"]
                catNom = "fh_j7_t4_TRF_CR"

            legNom = "CR"
            legTRF = ["TRF CR"]
            #legTRF = ["Inlc. KNN", "PerProc KNN"]

            plotVars = ["DNN_Node0"]
            isSR = False

        for _file in files:
            makeTRFComp(
                folderNom+"/"+_file,
                [folderTRF+"/"+_file
                # folderTRF2+"/"+_file
                ],
                catNom,
                catTRF,
                outFolder,
                _file.replace(".root",""),
                legNom,
                legTRF,
                plotVars = plotVars,
                isSR = isSR,
                lumi=lumi
            )
