import sys
import os
import datetime
import logging
from copy import deepcopy
from shutil import copyfile

import ROOT
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/utils/'))
from ConfigReader import ConfigReaderBase
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

class DatacardConfig(ConfigReaderBase):
    def __init__(self, pathtoConfig):
        super(DatacardConfig, self).__init__(pathtoConfig)
        
        #General
        self.templateFile = self.readConfig.get("General", "templates")
        self.templateSchemaNominal = self.readConfig.get("General", "templateSchema")
        self.templateSchemaUnc = self.readConfig.get("General", "templateSchemaUnc")
        self.channels = self.getList(self.readConfig.get("General","channels"))
        self.variable = self.readConfig.get("General", "variable")
        self.isPresel = self.setOptionWithDefault("General", "isPresel", False, "bool")
        self.scaleQCDMC = self.setOptionWithDefault("General", "scaleQCDMC", False, "bool")
        self.fixNegBins = self.setOptionWithDefault("General", "fixNegBins", False, "bool")
        self.fixNormCR = self.setOptionWithDefault("General", "fixNormCR", False, "bool")
        self.forceNominal = self.setOptionWithDefault("General", "forceNominal", [], "List")
        self.forceNominal = [ tuple(elem.split("__")) for elem in self.forceNominal]
        self.replaceDatawAsimov = self.setOptionWithDefault("General", "asimovData", False, "bool")
        self.asimovType = self.setOptionWithDefault("General", "asimovType", "s+b")
        
        self.skipCR = self.setOptionWithDefault("General", "skipCR", False, "bool")
        self.skipData = self.setOptionWithDefault("General", "skipData", False, "bool")

        self.forCTCVCP = self.setOptionWithDefault("General", "forCTCVCP", False, "bool")
        self.procsCTCVCP = self.setOptionWithDefault("General", "procsCTCVCP", None, "List")

        if self.forCTCVCP and self.procsCTCVCP is None:
            raise RuntimeError("Procs for CTCVCP processing are not set")
        
        if self.skipCR:
            logging.warning("Will skip CR!!!!")
        
        #Conversion of input templates
        self.inputSchemaNominal = self.readConfig.get("Conversion", "inputSchema")
        self.inputSchemaUnc = self.readConfig.get("Conversion", "inputSchemaUnc")
        self.conversionSRId = self.readConfig.get("Conversion", "SRIdentifier")
        self.conversionCRId = self.readConfig.get("Conversion", "CRIdentifier")
            
        #General process stuff
        self.observation = self.readConfig.get("Processes", "observation") 
        self.signals = self.getList(self.readConfig.get("Processes", "signals"))
        self.backgrounds = self.getList(self.readConfig.get("Processes", "backgrounds"))
        self.SRprefix = self.setOptionWithDefault("Processes","SRpostfix", "")
        self.CRprefix = self.readConfig.get("Processes","CRpostfix")
        
        #Control region stuff
        self.CRData = self.readConfig.get("ControlRegion", "data")
        self.CRProcessSubstract = self.getList(self.readConfig.get("ControlRegion", "substract"))
        self.CRScaleFactor = self.readConfig.getfloat("ControlRegion", "scalefactor")
        
        #Read additional setting for the card itself
        self.MCStatsOption = self.readConfig.get("Card", "mcStats")
        self.RateParam = self.readMulitlineOption("Card", "rateParam", "List", " ")
        if self.readConfig.has_option("Card","addLines"):
            self.addLines = self.getMultilineList("Card", "addLines")
        else:
            self.addLines = []
        self.ParamValues = self.readMulitlineOption("Card", "paramValues", "List", " ")
        self.PerChannelRateParams = self.setOptionWithDefault("Card", "perChannel", [], "List")
        for rateParam in self.PerChannelRateParams:
            if rateParam not in self.RateParam.keys():
                raise RuntimeError("Rateparam %s not in defined parameters"%rateParam)
        assert self.ParamValues.keys() == self.RateParam.keys()
        #Now deal with all the uncertainties
        #Generate final processes
        self.observation = self.observation+self.SRprefix
        self.signals = [x+self.SRprefix for x in self.signals]
        self.backgrounds = [x+self.SRprefix for x in self.backgrounds]
        self.CRData = self.CRData+self.CRprefix
        self.CRProcessSubstract = [x+self.CRprefix for x in self.CRProcessSubstract]

        if self.skipCR:
             self.CRData = self.CRData+self.CRprefix
             self.CRProcessSubstract = []

        
        self.allSimulation = self.signals+self.backgrounds
        if not self.isPresel:
            self.allSimulation += self.CRProcessSubstract

        #First: Deal with common uncertainties
        self.commonShapeNames = self.readConfig.options("Common_shape_uncertainties")
        self.commonScaleNames = self.readConfig.options("Common_scale_uncertainties")
        self.commonShapeCRNames = self.readConfig.options("Common_shape_uncertainties_CR")
        self.commonShape = {}
        self.commonScale = {}
        self.procPerUnc = {}
        self.ShapeUncPerProc = {}
        self.ScaleUncPerProc = {}
        for proc in self.allSimulation+[self.CRData]:
            self.ShapeUncPerProc[proc] = {}
            self.ScaleUncPerProc[proc] = {}
        for name in self.commonShapeNames:
            self.commonShape[name] = self.readConfig.get("Common_shape_uncertainties",name)
            self.procPerUnc[name] = self.allSimulation
            for proc in self.allSimulation:
                self.ShapeUncPerProc[proc][name] = self.readConfig.get("Common_shape_uncertainties",name)
        for name in self.commonShapeCRNames:
            for proc in self.allSimulation:
                if self.CRprefix in proc:
                    self.ShapeUncPerProc[proc][name] = self.readConfig.get("Common_shape_uncertainties_CR",name)
            self.ShapeUncPerProc[self.CRData][name] = self.readConfig.get("Common_shape_uncertainties_CR",name)
        for name in self.commonScaleNames:
            self.commonScale[name] = self.readConfig.get("Common_scale_uncertainties",name)
            for proc in self.allSimulation:
                self.ScaleUncPerProc[proc][name] = self.readConfig.get("Common_scale_uncertainties",name)

        #Second: Deal with process specific uncertainties
        #All uncertainties listed in the shape_uncertainty option are applied to both, CR and SR.
        self.procShapeUnc = []
        shapeProcs = self.readConfig.options("shape_uncertainties")
        for proc in shapeProcs:
            uncerts = self.readMulitlineOption("shape_uncertainties", proc, "Single", " ")
            for region in [self.SRprefix, self.CRprefix]:
                if proc+region in self.ShapeUncPerProc.keys():
                    for source in uncerts:
                        self.procShapeUnc.append(source)
                        self.ShapeUncPerProc[proc+region][source] = uncerts[source]
                        #print proc+region, source, self.ShapeUncPerProc[proc+region][source]
        #Next get the rate uncertainties (applied to both)
        self.procRateUnc = []
        rateProcs = self.readConfig.options("scale_uncertainties")
        for proc in rateProcs:
            uncerts = self.readMulitlineOption("scale_uncertainties", proc, "Single", " ")
            for region in [self.SRprefix, self.CRprefix]:
                if proc+region in self.ScaleUncPerProc.keys():
                    for source in uncerts:
                        self.procRateUnc.append(source)
                        self.ScaleUncPerProc[proc+region][source] = uncerts[source]

        self.procShapeUnc = list(set(self.procShapeUnc))
        self.procRateUnc = list(set(self.procRateUnc))

        self.allUnc = list(set(self.commonShapeNames + self.commonScaleNames + self.commonShapeCRNames + self.procShapeUnc + self.procRateUnc))
        self.allUnc = sorted(self.allUnc)
        self.nUnc = len(self.allUnc)

        
        self.procOrder = self.signals + self.backgrounds
        if not self.isPresel:
            if not self.skipCR:
                self.procOrder += [self.CRData] + self.CRProcessSubstract

        #Get stuff that needs renaming
        self.renameSystematics = {}
        self.renameSystematicsPerProc = {}
        if self.readConfig.has_section("Rename_systematics"):
            for option in self.readConfig.options("Rename_systematics"):
                if "," in self.readConfig.get("Rename_systematics", option):
                    # sys2Rename,proc = self.readConfig.get("Rename_systematics", option).split(",")
                    # if proc not in self.renameSystematicsPerProc.keys():
                    #     self.renameSystematicsPerProc[proc] = {}
                    # self.renameSystematicsPerProc[proc][sys2Rename] = option
                    for syst in self.readConfig.get("Rename_systematics", option).split(","):
                        if syst != "" and syst != " ":
                            self.renameSystematics[syst] = option
                else:
                    self.renameSystematics[self.readConfig.get("Rename_systematics", option)] = option
        self.renameProcs = {}
        if self.readConfig.has_section("Rename_processes"):
            for option in self.readConfig.options("Rename_processes"):
                self.renameProcs[self.readConfig.get("Rename_processes", option)] = option

                
        logging.debug("====== CONFIG ======")
        logging.debug("templates : %s", self.templateFile)
        logging.debug("channels : %s", self.channels)
        logging.debug("variables : %s", self.variable)
        logging.debug("forceNominal : %s", self.forceNominal)
        logging.debug("Signals:")
        for proc in self.signals:
            logging.debug("   %s",proc)
        logging.debug("Backgrounds:")
        for proc in self.backgrounds:
            logging.debug("   %s",proc)
        logging.debug("Main proc for CR: %s",self.CRData)
        logging.debug("Substact procs CR:")
        for proc in self.CRProcessSubstract:
            logging.debug("   %s",proc)
        logging.debug("Common scale unc. %s", self.commonScaleNames)
        logging.debug("Common shape unc. %s", self.commonShapeNames)
        logging.debug("Common shape unc. (CR only) %s", self.commonShapeCRNames)
        logging.debug("------ UNCERTAINTIES ------")
        for proc in self.allSimulation:
            logging.debug("---- %s ----", proc)
            for unc in self.ShapeUncPerProc[proc]:
                logging.debug("  %s -> %s", unc, self.ShapeUncPerProc[proc][unc])
            for unc in self.ScaleUncPerProc[proc]:
                logging.debug("  %s -> %s", unc, self.ScaleUncPerProc[proc][unc])
        logging.debug("---- %s ----", self.CRData)
        for unc in self.ShapeUncPerProc[self.CRData]:
            logging.debug("  %s -> %s", unc, self.ShapeUncPerProc[self.CRData][unc])
        for unc in self.ScaleUncPerProc[self.CRData]:
                logging.debug("  %s -> %s", unc, self.ScaleUncPerProc[self.CRData][unc])
        logging.debug("Number of uncertainties: %s", self.nUnc)
        if len(self.renameSystematics.keys())>0:
            logging.debug("------ RENAMING ------")
            for sys in self.renameSystematics:
                logging.debug("  %s : %s", sys, self.renameSystematics[sys])
            logging.debug("  ---------------------------")
            for proc in self.renameProcs:
                logging.debug("  %s : %s", proc, self.renameProcs[proc])
            logging.debug("  ---------------------------")
            for proc in self.renameSystematicsPerProc:
                logging.debug("  ------ Syst 4 proc: %s ------", proc)
                for sys in self.renameSystematicsPerProc[proc]:
                    logging.debug("    %s : %s", sys, self.renameSystematicsPerProc[proc][sys])
        logging.debug("------ CARD ------")
        logging.debug("  mcStats option: %s", self.MCStatsOption)
        for key in self.RateParam:
            logging.debug("  Rateparam %s for %s",key,self.RateParam[key])
            logging.debug("       Startvalues %s",self.ParamValues[key])
        logging.debug("  Additional lines:")
        for key in self.addLines:
            logging.debug("  -> %s", key)

        #exit()

replChannel = lambda  schema, channel : schema.replace("$CHANNEL", channel)
replVariable = lambda  schema, channel : schema.replace("$VARIABLE", channel)
replProcess = lambda  schema, proc : schema.replace("$PROCESS", proc)
replSystematic = lambda  schema, sys : schema.replace("$SYSTEMATIC", sys) 
        
def makeCard(config, channel, nObs, rates, fileName, delim = "\t", addSep = True):
    """
    Combined function for generating the cards.
    
    Args:
      config (DatacardConfig) :  Configuration
      channel (str) : Channel name for datacard
      nObs (int) : Number of observed Events (data or -1)
      rate (dict) : Rates per channel
      fileName (str) : Name of the file with the templates for the card
      delim (str) : Delimiter for line 
      addSep (bool) : If True lines with ---- will be added

    returns datacardlines (list) : List with text lines 
    """
    logging.info("Generating Datacard")
    allLines = []
    allLines += makeHeader(config)
    if addSep: 
        allLines.append("----------------------") 
    allLines += makeShapeBlock(config, channel, fileName)
    if addSep: 
        allLines.append("----------------------") 
    allLines += makeObservationBlock(config, nObs, channel)
    if addSep: 
        allLines.append("----------------------") 
    allLines += makeProcBlock(config, channel, rates)
    if addSep: 
        allLines.append("----------------------") 
    allLines += generateUncMatrix(config)

    _allLines = []
    for line in allLines:
        if isinstance(line, list):
            _allLines.append(delim.join(line))
        else:
            _allLines.append(line)

    allLines = _allLines

    print(config.RateParam.keys())
    
    for key in sorted(config.RateParam.keys()):
        for proc in config.RateParam[key]:
            if len(config.ParamValues[key]) == 1:
                startingVals = config.ParamValues[key][0]
            elif len(config.ParamValues[key]) == 3:
                startingVals = "{0} [{1},{2}]".format(config.ParamValues[key][0],
                                         config.ParamValues[key][1],
                                         config.ParamValues[key][2])
            else:
                raise RuntimeError("Invalid param starting values")
            # if key in config.PerChannelRateParams:
            #     key = key+"_"+channel
            allLines.append("{0} rateParam {1}_{2} {3} {4}".format(key+"_"+channel if key in config.PerChannelRateParams else key,
                                                                   channel,
                                                                   config.variable,
                                                                   proc,
                                                                   startingVals))
     
    allLines.append(config.MCStatsOption)
    for addLine in config.addLines:
        allLines.append(addLine)
    return allLines
    
def makeHeader(config):
    header = ["kmax 1"]
    header.append("jmax {0}".format(len(config.procOrder)-1))
    header.append("kmax *")
    return header

def makeShapeBlock(config, channel, filename):
    mapping = "shapes * {0}_{1} ../{2} {3} {4}".format(channel,
                                                    config.variable,
                                                    filename,
                                                    "$CHANNEL__$PROCESS",
                                                    "$CHANNEL__$PROCESS__$SYSTEMATIC")
    return [mapping]

def makeObservationBlock(config, nObs, channel):
    return [ "bin {0}_{1}".format(channel, config.variable),
             "observation {0}".format(nObs) ]

def makeProcBlock(config, channel, rate, printMatrix=False):
    if not isinstance(rate, dict):
        raise TypeError("rate arg must be dict but is %s"%type(rate))
    if set(rate.keys()) != set(config.procOrder):
        logging.error("Not in rate: %s", set(config.procOrder).difference(set(rate.keys())))
        raise KeyError("Not all processes are defined in rate")

    nColumns = len(config.procOrder)+1
    binLine = ["bin",""]
    procLine1 = ["process",""]
    procLine2 = ["process",""]
    rateLine = ["rate",""]
    iSignals = 0
    iBackgrounds = 1
    for proc in config.procOrder:
        binLine.append("{0}_{1}".format(channel, config.variable))
        procLine1.append(proc)
        if proc in config.signals:
            iproc = iSignals
            iSignals -= 1
        else:
            iproc = iBackgrounds
            iBackgrounds += 1
        procLine2.append(str(iproc))
        rateLine.append(str(rate[proc]))

    lines = [binLine,
             procLine1,
             procLine2,
             rateLine]

    if printMatrix:
        for line in lines:
            for elem in line:
                print elem,";",
            print " "

    return lines


def generateUncMatrix(config, printMatrix=False):
    uncMatrix = None
    nLines = config.nUnc
    nColumns = len(config.procOrder)
    uncMatrix = []
    for unc in config.allUnc:
        thisLine = []
        thisLine.append(unc)
        isShape = False
        if unc in (config.commonShapeNames + config.commonShapeCRNames + config.procShapeUnc):
            thisLine.append("shape")
            isShape = True
        else:
            thisLine.append("lnN")
        for proc in config.procOrder:
            if isShape:
                if unc in config.ShapeUncPerProc[proc].keys():
                    thisLine.append(config.ShapeUncPerProc[proc][unc])
                else:
                    thisLine.append("-")
            else:
                if unc in config.ScaleUncPerProc[proc].keys():
                    thisLine.append(config.ScaleUncPerProc[proc][unc])
                else:
                    thisLine.append("-")
        _thisLine = [str(x) for x in thisLine]
        uncMatrix.append(_thisLine)

    if printMatrix:
        for line in uncMatrix:
            for elem in line:
                print elem,";",
            print " "
    
    return uncMatrix

def checkValidKey(config, keyName):
    """
    Checks if the the nominal histogram is present for all process and the systematic
    templates for shapes are present for their respective processes
    """
    # print keyName
    inSchemaElems = config.templateSchemaUnc.split("__")
    idProc, idCh, idSys = inSchemaElems.index("$PROCESS"),inSchemaElems.index("$CHANNEL"),inSchemaElems.index("$SYSTEMATIC")

    keyElem = keyName.split("__")
    proc, channel = keyElem[idProc], keyElem[idCh]

    if proc not in config.procOrder or proc == config.observation:
        # print "Not valid proc"
        return False


    if channel.replace("_"+config.variable, "") not in config.channels:
        # print "Not valid channel"
        return False

    if len(keyElem) == len(inSchemaElems):
        systematic = keyElem[idSys]
        if systematic.endswith("Up"):
            systematic = systematic[0:-len("Up")]
        if systematic.endswith("Down"):
            systematic = systematic[0:-len("Down")]

        if config.CRData in proc:
            sys2Check =  config.ShapeUncPerProc[proc].keys()
        else:
            sys2Check =  config.commonShapeNames + config.ShapeUncPerProc[proc].keys()
        # print systematic,sys2Check
        if systematic not in sys2Check:            
            # print "Not valid syst"
            return False
    return True
        
def comvertInputTemplates(config, rFile, checkValidity = True, skipMissingCheck=False):
    """
    Converts the histogram names of the input file to the schema defined in the general section of the config
    """
    logging.info("Converting input histograms")
    histograms = {}
    inSchemaElems = config.inputSchemaUnc.split("__")
    idProc, idCh, idVariable, idSys = inSchemaElems.index("$PROCESS"),inSchemaElems.index("$CHANNEL"),inSchemaElems.index("$VARIABLE"),inSchemaElems.index("$SYSTEMATIC")
    observationHistosPairs = []
    foundChannels = []
    for key in rFile.GetListOfKeys():
        putNominal = False
        keyName = key.GetName()

        if config.variable not in keyName:
            continue
        
        keyElem = keyName.split("__")

        
        proc, channel, variable = keyElem[idProc], keyElem[idCh], keyElem[idVariable]
        
        isSR = False
        if config.conversionSRId in channel:
            isSR = True
            channel = channel.replace(config.conversionSRId, "")
        elif config.conversionCRId in channel:
            channel = channel.replace(config.conversionCRId, "")
        else:
            raise RuntimeError("There should be a SR/CR id in the templates")

        if channel not in foundChannels:
            foundChannels.append(channel)

        #logging.debug(keyElem)
        
        isNominal = True
        if len(keyElem) == 4:
            systematic = keyElem[idSys]
            isNominal = False

            if systematic.endswith("off"):
                #logging.warning("Skipping %s", systematic)
                continue
            
            if systematic.endswith("Up"):
                systematicCheck = systematic[0:-len("Up")]
            if systematic.endswith("Down"):
                systematicCheck = systematic[0:-len("Down")]

            for syst, direction in config.forceNominal:
                if syst in systematic and direction in systematic:
                    logging.warning("Will force syst %s in %s to nominal", systematic, proc)
                    putNominal= True
            if systematicCheck in config.renameSystematics.keys():
                logging.debug("Repalcing systematic %s to %s", systematicCheck, config.renameSystematics[systematicCheck])
                systematic = systematic.replace(systematicCheck, config.renameSystematics[systematicCheck])
            if proc in config.renameSystematicsPerProc:
                if systematicCheck in config.renameSystematicsPerProc[proc]:
                    logging.debug("Repalcing systematic %s to %s", systematicCheck, config.renameSystematicsPerProc[proc][systematicCheck])
                    systematic = systematic.replace(systematicCheck, config.renameSystematicsPerProc[proc][systematicCheck])
        else:
            systematic = ""

        if proc in config.renameProcs:
            logging.debug("Replacing proc name %s with %s",proc,config.renameProcs[proc])
            proc = config.renameProcs[proc]

        if isSR and config.observation == proc and len(keyElem) == 3:
            observationHistosPairs.append((keyName, replChannel(replProcess(config.templateSchemaNominal, "data_obs"), channel+"_"+variable)))
        proc = proc + (config.SRprefix if isSR else config.CRprefix)
        if isNominal:
            newName = replChannel(replProcess(config.templateSchemaNominal, proc), channel+"_"+variable)
        else:
            newName = replChannel(replProcess(replSystematic(config.templateSchemaUnc, systematic), proc), channel+"_"+config.variable)

            
        if checkValidKey(config, newName) or not checkValidity:
            logging.debug("%s --> %s", keyName , newName)
            if putNominal:
                nomNameForReplace = keyName.replace("__"+systematic,"")
                logging.warning(keyName)
                logging.warning("Will use key %s to replace systamtic ",nomNameForReplace )
                histograms[newName] = deepcopy(rFile.Get(nomNameForReplace))
            else:
                histograms[newName] = deepcopy(rFile.Get(keyName))
            histograms[newName].SetName(newName)
            histograms[newName].SetTitle(newName)

        # if proc == "ttbb_CR" and "HTreweight4b" in systematic:
        #     raw_input("aa")

    if not skipMissingCheck:
        if len(observationHistosPairs) != len(config.channels):
            raise RuntimeError("Expected %s data_obs histograms. Found %s"%(len(config.channels), len(observationHistosPairs)))
    logging.debug("----- Data ------")
    for keyName, newName in observationHistosPairs:
        logging.debug("%s --> %s", keyName , newName)
        histograms[newName] = deepcopy(rFile.Get(keyName))
        histograms[newName].SetName(newName)
        histograms[newName].SetTitle(newName)

    return histograms, foundChannels

def getRates(config, channel, histos):
    """
    Get the rates of all processes in the datacard
    """
    rates = {}
    fullChannelName = channel+"_"+config.variable
    for proc in config.procOrder:
        thisNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, proc), fullChannelName)
        rates[proc] = histos[thisNominalTemplate].Integral()

    return rates

def scaleCRTemplates(config, channel, histos, sf):
    """
    Scale CR tamples by scalefactor and make substract templates negative
    """
    logging.info("Processing CR templates")
    fullChannelName = channel+"_"+config.variable
    for proc in config.CRProcessSubstract:
        factor = 1.0 * config.CRScaleFactor * sf

        thisNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, proc), fullChannelName)
        for name in histos:
            if name.startswith(thisNominalTemplate):
                logging.debug("Applying SF %s to %s", factor, name)
                histos[name].Scale(factor)
    dataNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, config.CRData ), fullChannelName)
    factor = config.CRScaleFactor * sf
    for name in histos:
        if name.startswith(dataNominalTemplate):
            logging.debug("Applying SF %s to %s", factor, name)
            histos[name].Scale(factor)

def getCRScaleFactor(config, channel, histos):
    logging.info("Getting SR/CR scalefactor")
    fullChannelName = channel+"_"+config.variable
    CRYield = 0
    for proc in config.CRProcessSubstract:
        thisNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, proc), fullChannelName)
        for name in histos:
            if name == (thisNominalTemplate):
                logging.debug("Found CR histos for proc %s with integral %s",proc, histos[name].Integral())
                CRYield -= histos[name].Integral()
    dataNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, config.CRData ), fullChannelName)
    for name in histos:
        if name == dataNominalTemplate:
            logging.debug("Found CR data with integral = %s", histos[name].Integral())
            CRYield += histos[name].Integral()

    logging.debug("CR Yield: %s", CRYield)

    SRYield = 0
    for proc in config.backgrounds+config.signals:
        thisNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, proc), fullChannelName)
        for name in histos:
            if name == (thisNominalTemplate):
                logging.debug("Found SR histos for proc %s with integral %s",proc, histos[name].Integral())
                SRYield -= histos[name].Integral()
    dataNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, "data_obs" ), fullChannelName)
    for name in histos:
        if name == (dataNominalTemplate):
            logging.debug("Found SR data with integral = %s", histos[name].Integral())
            SRYield += histos[name].Integral()

    logging.debug("SR Yield: %s", SRYield)

    sf = SRYield/CRYield
    logging.info("SR/CR = %s", sf)
    return sf

def scaleQCDMC(config, channel, histos, debug=False):
    fullChannelName = channel+"_"+config.variable
    simulationYield = 0
    for proc in config.allSimulation:
        thisNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, proc), fullChannelName)
        for name in histos:
            if name == (thisNominalTemplate):
                simulationYield += histos[name].Integral()

    dataNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, "data_obs" ), fullChannelName)
    qcdNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, "qcd" ), fullChannelName)
    QCDscalefac= 1 + (histos[dataNominalTemplate].Integral()-simulationYield)/histos[qcdNominalTemplate].Integral()

    logging.info("Scaling QCD with %s",QCDscalefac)
    logging.info("Yields: Data = %s | Sim (pre) = %s | Sim (Pre w/o QCD) = %s | QCD (pre) = %s | QCD (post) = %s",
                 histos[dataNominalTemplate].Integral(),
                 simulationYield,
                 simulationYield-histos[qcdNominalTemplate].Integral(),
                 histos[qcdNominalTemplate].Integral(),
                 histos[qcdNominalTemplate].Integral()*QCDscalefac)

    for name in histos:
        if "qcd" in name:
            if debug:
                logging.debug("Scaling %s with %s", name, QCDscalefac)
            histos[name].Scale(QCDscalefac)

def asimovData(config, channel, histos, debug = True):
    asimovData = None

    fullChannelName = channel+"_"+config.variable

    if config.asimovType == "b-only":
        SRTempltes = config.backgrounds
    elif config.asimovType == "s+b":
        SRTempltes = config.backgrounds+config.signals
    else:
        raise NotImplementedError("Only *b-only* and *s+b* are valid asimov types")

    logging.warning("Will use procs for SR tempaltes: %s", SRTempltes)
    
    for proc in SRTempltes:
        thisNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, proc), fullChannelName)
        if asimovData is None:
            asimovData = histos[thisNominalTemplate].Clone(thisNominalTemplate.replace(proc,"data_obs_asimov"))
            logging.warning("Initializing asimov data from %s : %s", proc, histos[thisNominalTemplate].Integral())
        else:
            asimovData.Add(histos[thisNominalTemplate])
            logging.warning("Adding %s to asimov data: %s", proc, histos[thisNominalTemplate].Integral())

    dataNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, config.CRData ), fullChannelName)
    asimovData.Add(histos[dataNominalTemplate])

    logging.warning("Adding CR data: %s", histos[dataNominalTemplate].Integral())
    
    for proc in config.CRProcessSubstract:
        thisNominalTemplate = replChannel(replProcess(config.templateSchemaNominal, proc), fullChannelName)
        logging.warning("Substracting %s: %s", proc, histos[thisNominalTemplate].Integral())
        asimovData.Add(histos[thisNominalTemplate], -1)
        

    logging.warning("Asimov data yield: %s", asimovData.Integral())
    logging.warning("Name / Title : %s/%s", asimovData.GetName(), asimovData.GetTitle())
    
    dataTempalteName = replChannel(replProcess(config.templateSchemaNominal, "data_obs" ), fullChannelName)
    logging.warning("Integral real data %s", histos[dataTempalteName].Integral())
    if debug:
        logging.warning("Should be the real one")
        for i in range(histos[dataTempalteName].GetNbinsX()):
            diff = abs(histos[dataTempalteName].GetBinContent(i) - asimovData.GetBinContent(i))
            logging.warning("Check bin %s --> Same?  %s", i, diff == 0.0)


    
    histos[dataTempalteName] = asimovData
    histos[dataTempalteName].SetName(dataTempalteName)
    histos[dataTempalteName].SetTitle(dataTempalteName)
    logging.warning("(Now should be asimov) Integral real data %s", histos[dataTempalteName].Integral())

    if debug:
        logging.warning("Should be the asimov one")
        for i in range(histos[dataTempalteName].GetNbinsX()):
            diff = abs(histos[dataTempalteName].GetBinContent(i) - asimovData.GetBinContent(i))
            logging.warning("Check bin %s --> Same?  %s", i, diff == 0.0)
        
def addOnsidedShapes(config, keys, histos):
    unshiftedKeys = []
    for key in keys:
        if key.endswith("Up"):
            unshiftedKeys.append(key[0:-len("Up")])
        if key.endswith("Down"):
            unshiftedKeys.append(key[0:-len("Down")])
    unshiftedKeys = list(set(unshiftedKeys))
    for unc in unshiftedKeys:
        if not unc+"Up" in keys:
            nominal = unc.split("__")[1:3]
            histos[unc+"Up"] = histos[nominal].Clone(unc+"Up")
            histos[unc+"Up"].SetTitle(unc+"Up")
            logging.warning("Added: %s",unc+"Up")
        if not unc+"Down" in keys:
            nominal = "__".join(unc.split("__")[0:2])
            histos[unc+"Down"] = histos[nominal].Clone(unc+"Down")
            histos[unc+"Down"].SetTitle(unc+"Down")
            logging.warning("Added: %s",unc+"Down")

def pullNegBins(config, keys, histos):
    nBins = histos[keys[0]].GetNbinsX()
    for key in keys:
        for iBin in range(nBins+2):
            if histos[key].GetBinContent(iBin) < 0:
                logging.warning("Histo %s bin %s is <0 (Value : %s)", key, iBin, histos[key].GetBinContent(iBin))
                if config.fixNegBins:
                    logging.warning("Fixing bin value to 0")
                    histos[key].SetBinContent(iBin, 0.0)

def fixCRTemplatesToNorm(config, keys, histos):
    """
    Function for fixing shape uncertainties to not chnage yield for the CR templates
    """
    for proc in config.CRProcessSubstract:
        pass
        # First find the nomnial key

        nominakKey = None
        for key in keys:
            keyElem = key.split("__")
            if len(keyElem) == 2 and config.CRprefix in key and proc in key:
                nominakKey = key

        logging.warning("Nominal key: %s", nominakKey)
        # Now process all systematic templates     
        for key in keys:
            keyElem = key.split("__")
            if len(keyElem) == 3 and config.CRprefix in key and proc in key:
                logging.warning("Yield %s = %s vs. nominal %s", key, histos[key].Integral(), histos[nominakKey].Integral())
    
                    
def checkEmtpyTemplates(config, keys, histos):
    for key in keys:
        if histos[key].Integral() == 0:
            nominal = "__".join(key.split("__")[0:2])
            if histos[nominal].Integral() != 0:
                logging.warning("Key %s has 0.0 integral. Will add norminal to this histogram", key)
                histos[key].Add(histos[nominal])
                logging.debug("Int: Nom %s | Sys %s (SECOND SHOULD BE SAME AS NOM)", histos[nominal].Integral(), histos[key].Integral())
            
            
def checkVarInChannel(channel, histos):
    
    return True
            
def makeDatacards(path2Config, inputFile, variable, category, outBasePath, tag, forceCreation, skipMissing, cardPrefix):
    logging.info("Starting datacard generation")

    # Check and create output directory
    logging.debug("Checking bashpath: %s", outBasePath)
    if not os.path.exists(outBasePath):
        logging.info("Creating output direcory")
        os.makedirs(outBasePath)
    if os.path.exists(outBasePath+"/"+tag) and not forceCreation:
        logging.error("Tag %s has already directory on FS. Use --force to overwrite")
        return False

    if not os.path.exists(outBasePath+"/"+tag+"/cards"):
        os.makedirs(outBasePath+"/"+tag+"/cards")

    config = DatacardConfig(path2Config)

    if variable is not None:
        logging.warning("Overwriting variable set in config with %s", variable)
        config.variable = variable
    if inputFile is not None:
        logging.warning("Overwriting file defined in config with passed value")
        inputTemplates = ROOT.TFile.Open(inputFile)
    else:
        inputTemplates = ROOT.TFile.Open(config.templateFile)

    inputHistos, foundChannels = comvertInputTemplates(config, inputTemplates, skipMissingCheck=skipMissing)
    
    runChannels = config.channels
    if skipMissing:
        runChannels = foundChannels
        logging.warning("Using channels: %s", runChannels)

    if len(runChannels) == 0:
        raise RuntimeError("No channel found")

    if category is not None:
        if category in runChannels:
            logging.warning("Forcing category to: %s", category)
            runChannels = [category]
        else:
            logging.warning("Category %s not in category list", category)
            runChannels = []
    
    for channel in runChannels:
        logging.info("Processing channel %s", channel)
        channelKeys = [x for x in inputHistos.keys() if channel in x]
        pullNegBins(config, channelKeys, inputHistos)


        if config.fixNormCR:
            fixCRTemplatesToNorm(config, channelKeys, inputHistos)
        
        
        outFilePrefix = channel+"_"+config.variable

        if not config.isPresel:
            if not config.skipCR:
                scaleCRTemplates(config, channel, inputHistos, getCRScaleFactor(config, channel, inputHistos))
            #raw_input("..")

        if config.isPresel and config.scaleQCDMC:
            logging.info("Will scale QCD MC to match yield")
            scaleQCDMC(config, channel, inputHistos)

        if config.replaceDatawAsimov:
            logging.warning("Will replace data_obs with asimov data")
            asimovData(config, channel, inputHistos)
            
        rates = getRates(config, channel, inputHistos)
        
        cardLines = makeCard(config, channel, -1, rates, outFilePrefix+".root")
        
        datacardName = outBasePath+"/"+tag+"/cards/"+cardPrefix+outFilePrefix+".txt"
        logging.info("Writing datacard to %s",datacardName)
        with open(datacardName, "w") as f:
            for line in cardLines:
                f.write(line+"\n")


        addOnsidedShapes(config, channelKeys, inputHistos)

        checkEmtpyTemplates(config, channelKeys, inputHistos)
        
        logging.info("Wrinting templates to root file")
        outFile = ROOT.TFile(outBasePath+"/"+tag+"/"+outFilePrefix+".root", "RECREATE")
        outFile.cd()
        channelKeys = [x for x in inputHistos.keys() if channel in x]
        for key in channelKeys:
            inputHistos[key].Write()
        outFile.Close()

    logging.info("Finished datacard generation")
    return True
    
    
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Make datacards from sparsinator outut'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "config file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file. Will overwrite config!",
        type = str,
        default = None
    )
    argumentparser.add_argument(
        "--variable",
        action = "store",
        help = "Variable. Will overwrite config!",
        type = str,
        default = None
    )
    argumentparser.add_argument(
        "--category",
        action = "store",
        help = "Category. Force to use this category if present",
        type = str,
        default = None
    )
    argumentparser.add_argument(
        "--outputPath",
        action = "store",
        help = "Output path.",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Will be used as subfolder in --outputPath. Is required to be unique",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--cardPrefix",
        action = "store",
        help = "Prefix for datacard txt file",
        default = ""
    )
    argumentparser.add_argument(
        "--force",
        action = "store_true",
        help = "If set, old datacards with same tag will be overwritten",
    )
    argumentparser.add_argument(
        "--skipMissing",
        action = "store_true",
        help = "If set, variable that miss in a cateory will be skipped",
    )
    
    args = argumentparser.parse_args()
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging, funcLen = 21)

    madeCards = makeDatacards(args.config, args.inputFile, args.variable, args.category, args.outputPath, args.tag, args.force, args.skipMissing, args.cardPrefix)

    if madeCards:
        #Write log to ouput folder
        logging.info("Wrinting infos (for bookkeeping) to output folder")
        outputFolder = args.outputPath + "/" + args.tag

        infoLines = ["Created on "+datetime.datetime.now().strftime("%A %d. %B %Y - %H:%M:%S")]
        infoLines += ["$CMSSW_BASE: "+str(os.environ["CMSSW_BASE"])]
        infoLines += ["Command line options:"]
        infoLines += [" ".join(sys.argv).replace("--","\n  --")]

        with open(outputFolder+"/makeDatacardInfo.txt", "w") as f:
            for line in infoLines:
                f.write(line+"\n")

        copyfile(args.config, outputFolder+"/usedConfig.cfg")
    
    
    

