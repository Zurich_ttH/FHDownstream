from __future__ import print_function
import sys
import os 
import logging

import ROOT

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging


def transplantDataHisto(fitDiagnosticsFile, templates, outputPostFix, category,  dataName = "data_obs", folder="shapes_prefit"):
    import os
    logging.info("File: %s", fitDiagnosticsFile)
    os.system("cp {} {}".format(fitDiagnosticsFile, fitDiagnosticsFile.replace(".root", "_"+outputPostFix+".root")))
    rTemplates = ROOT.TFile.Open(templates)


    dataHisto = None
    for key in rTemplates.GetListOfKeys():
        if key.GetName().endswith(dataName):
            if dataHisto is not None:
                logging.error("Some problem here. There should only be one data_obs")
                return 0
            dataHisto = rTemplates.Get(key.GetName()).Clone()

    if dataHisto is None:
        logging.error("Seems I could not find a data templates.")
        return 0


    rFitDiagnostics = ROOT.TFile(fitDiagnosticsFile.replace(".root", "_"+outputPostFix+".root"), "update")

    
    dataHistoFitDiagnostics = rFitDiagnostics.Get(folder+"/"+category+"/data")

    logging.info(dataHisto)
    logging.info(dataHistoFitDiagnostics)
    dataHistoFitDiagnostics.Print()
    
    for iBin in range(dataHisto.GetNbinsX()+1):
        if iBin == 0:
            continue
        val = dataHisto.GetBinContent(iBin)
        err = dataHisto.GetBinError(iBin)
        logging.info("Bin %s - %s +- %s", iBin, val, err)
        
        dataHistoFitDiagnostics.SetPoint(iBin-1, iBin-0.5, val)
        dataHistoFitDiagnostics.SetPointEYhigh(iBin-1,err)
        dataHistoFitDiagnostics.SetPointEYlow(iBin-1,err)
        
    dataHistoFitDiagnostics.Print()
    rFitDiagnostics.cd(folder+"/"+category)
    dataHistoFitDiagnostics.Write("",ROOT.TFile.kOverwrite)
    
if __name__ == "__main__":
    fitDiagnisticsFile = sys.argv[1]
    templates = sys.argv[2]
    outputPostFix = sys.argv[3]
    cat = sys.argv[4]

    initLogging(10, funcLen = 21)
    
    transplantDataHisto(fitDiagnisticsFile, templates, outputPostFix, cat)
