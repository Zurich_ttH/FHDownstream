import sys
import os
import logging

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from makeVariationsRadiation import makeVariationsRadiation

if __name__ == "__main__":
    initLogging(10, funcLen = 21)
    
    file2016 = sys.argv[1]
    file2017 = sys.argv[2]
    file2018 = sys.argv[3]
    ttHProc = "TTH_PTH" # ttH or TTH_PHT
    
    makeVariationsRadiation(file2016, file2017, file2018, ttHProc, True)
