import ROOT
import sys
import os

from glob import glob


if __name__ == "__main__":
    baseFolder = sys.argv[1]
    template = sys.argv[2]

    for file_ in glob(baseFolder+"/*.root"):
        print "Rpcoessing file: %s"%file_
        rFile = ROOT.TFile(file_)
        rFile.Get(template).Draw()
        raw_input("Next file")
