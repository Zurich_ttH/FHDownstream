#!/bin/bash
# Script that runs combine on all datacrads found in the current direcotry. Idea is to link this script to datacard directory
echo "//==========================================================\\\\"
echo "|| Script for fitting DNN input variable datacards          ||" 
echo "|| First argument will be used as the output folder         ||"
echo "|| All futher arguemnts will be combined to select files    ||"
echo "|| Add --dryrun to not run the fits but only see commands   ||"
echo "\\\\==========================================================//"
if [[ $# == 0 ]]
then
    echo "Please pass at least on argument"
    exit 1
else
    OUTPUTFOLDER=$1
    echo "Will use $OUTPUTFOLDER as output directory "
fi
if [[ $# -ge 1 ]]
   then
       WILDCARD="card*"
       for ARG in ${@:2}
       do
	   if [[ $ARG != "--"* ]]
	   then
	       echo "Got arguement $ARG. Will add $ARG* to wildcard selection"
	       WILDCARD="${WILDCARD}$ARG*"
	   fi

	   if [[ $ARG == "--dryrun" ]]
	   then
	       RUNCOMBINE=false     
	   else
	       RUNCOMBINE=true
	   fi
       done
fi

echo "RUNCOMBINE set to $RUNCOMBINE"
echo "Selection wildcard: $WILDCARD"
COUNT=$(ls $WILDCARD.txt | wc -l)
echo "Files matching selection $COUNT"
if [[ $COUNT == 0 ]]
then
    echo "No datacards matching selection. Aborting......."
    exit 1
fi
#############################################################################
if [[ $RUNCOMBINE == true ]]
then
    if [[ -d $OUTPUTFOLDER ]]; then
	echo ""
	echo "$OUTPUTFOLDER already exists. Aborting......."
	exit 1
    fi
    mkdir $OUTPUTFOLDER
fi
COLUMNS=$(tput cols)
echo ""
echo "Starting fits"
seq -s- $COLUMNS|tr -d '[:digit:]'
echo ""
GENERALSETTINGS=" -M FitDiagnostics  -m 125 --cminDefaultMinimizerStrategy 0 --cminPreScan --saveWithUncertainties --saveNormalizations --saveShapes --saveOverallShapes --cminDefaultMinimizerTolerance 0.4  --rMin -40 --rMax 40 --skipBOnlyFit"
for FILE in ${WILDCARD}.txt
do
    FILENAME=$(echo $FILE| cut -d'.' -f 1)
    echo "text2workspace.py ${FILE}"
    if [[ $RUNCOMBINE == true ]]; then
    	text2workspace.py ${FILE}  &> ${OUTPUTFOLDER}/text2workspace_${FILENAME}.out.txt
	cat ${OUTPUTFOLDER}/text2workspace_${FILENAME}.out.txt
    fi
    ls -l
    echo "combine ${FILENAME}.root -n _${FILENAME} ${GENERALSETTINGS} &> ${OUTPUTFOLDER}/combine_${FILENAME}.out.txt"
    if [[ $RUNCOMBINE == true ]]; then
    	combine ${FILENAME}.root -n _${FILENAME} ${GENERALSETTINGS} &> ${OUTPUTFOLDER}/combine_${FILENAME}.out.txt
	cat ${OUTPUTFOLDER}/combine_${FILENAME}.out.txt
    fi
    echo "mv higgsCombine_${FILENAME}* ${OUTPUTFOLDER}/"
    echo "mv fitDiagnostics_${FILENAME}* ${OUTPUTFOLDER}/"
    echo "mv combine_logger.out ${OUTPUTFOLDER}/${FILENAME}_combine_logger.out"
    echo "mv ${FILE} ${OUTPUTFOLDER}/"
    echo "mv ${FILENAME}.root ${OUTPUTFOLDER}/"
    echo "mv ../*.root ${OUTPUTFOLDER}/"
    if [[ $RUNCOMBINE == true ]]; then
	mv higgsCombine_${FILENAME}* ${OUTPUTFOLDER}/
	mv fitDiagnostics_${FILENAME}* ${OUTPUTFOLDER}/
	mv combine_logger.out ${OUTPUTFOLDER}/${FILENAME}_combine_logger.out
	mv ${FILE} ${OUTPUTFOLDER}/
	mv ${FILENAME}.root ${OUTPUTFOLDER}/
	mv ../*.root ${OUTPUTFOLDER}/
    fi

    echo ""
    seq -s= $COLUMNS|tr -d '[:digit:]'
    echo ""
done
    
