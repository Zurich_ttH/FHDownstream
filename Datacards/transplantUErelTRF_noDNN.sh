#!/bin/bash
FOLDER=$1
FOLDERINIT=$2
VAR=$3

python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDERINIT}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDERINIT}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root \
       ttbarPlusBBbarMerged
