import sys, os
import logging
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from calcPDFWeightFromMembers import calcPDFWeightFromMembers
from mergeTTtemplates import mergeTemplates
from splitFileIntoProcs import splitFileIntoProcs

if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        help = "Input file",
        nargs = "+",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--dataset",
        action = "store",
        help = "Dataset name",
        type = str,
        default = None,
    )
    argumentparser.add_argument(
        "--mergeOnly",
        action = "store_true",
        help = "If set, the PDF merging step will be skipped",
    )
    argumentparser.add_argument(
        "--noGluSplitUnc",
        action = "store_false",
        help = "If set, glusplitting uncertainty will not be added",
    )

    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    outfiles = []
    
    for input_ in args.inputs:
        if "TTbb" in input_ :
            postfix = "_ttbbNLO"
        else:
            postfix = ""

        if args.mergeOnly:
            file_step2 = mergeTemplates(input_, "ttbbMerged", True, addGluSplitUnc=args.noGluSplitUnc)
        else:
            file_step1 = calcPDFWeightFromMembers(input_, "PDFMerged", postfix)
            file_step2 = mergeTemplates(file_step1, "ttbbMerged", True)
        
        outfiles.append(splitFileIntoProcs(file_step2, args.dataset))

        with open("file2Archive.txt", "w") as f:
            f.write(" ".join(outfiles))

        if not args.mergeOnly:
            logging.warning("Removing %s", file_step1)
            os.system("rm %s"%file_step1)
        logging.warning("Removing %s", file_step2)
        os.system("rm %s"%file_step2)
