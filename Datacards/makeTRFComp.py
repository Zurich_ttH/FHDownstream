from __future__ import print_function

import sys, os
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/classes/"))

from PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, checkNcreateFolder
from ratios import RatioPlot
import ROOT
import logging
from copy import deepcopy

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)


def main(inputFile, inputFileTRF, cat, catTRF, folder, output, leg = None, legTRF = None, plotVars = None, plotProcs = None, isSR=True, lumi="41.5"):
    logging.debug("cat: %s", cat)
    logging.debug("catTRF: %s", catTRF)

    logging.debug("inputFile : %s", inputFile)
    logging.debug("inputFileTRF : %s", inputFileTRF)
    rFile = ROOT.TFile.Open(inputFile)
    logging.debug("rFile : %s", rFile)
    rFilesTRF = []
    for inputFile in inputFileTRF:
        rFilesTRF.append(ROOT.TFile.Open(inputFile))

    logging.debug("rFilesTRF : %s", rFilesTRF)
    allKeys = [x.GetName() for x in rFile.GetListOfKeys() if cat in x.GetName()]
    allKeysTRF = [x.GetName() for x in rFilesTRF[0].GetListOfKeys() if catTRF[0] in x.GetName()]
    
    procs =  list(set([x.split("__")[0] for x in allKeys]))
    procsTRF =  list(set([x.split("__")[0] for x in allKeysTRF]))

    if plotProcs is not None:
        procs_ = []
        procsTRF_ = []
        for p in procs:
            if p in plotProcs:
                procs_.append(p)
                procsTRF_.append(p)

        if len(procs_) == 0:
            raise RuntimeError("No procs selected")

        procs = procs_
        procsTRF = procsTRF_
    
    logging.warning("Procs in --input : %s", procs)
    logging.warning("Procs in --inputTRF : %s", procsTRF)
    
    vars_ = list(set([x.split("__")[2] for x in allKeys]))
    varsTRF_ = list(set([x.split("__")[2] for x in allKeysTRF]))

    logging.debug("Vars in --input : %s", vars_ )
    logging.debug("Vars in --inputTRF : %s", varsTRF_)

    runVars = [x for x in varsTRF_ if x in vars_]
    logging.info("Will process variables: %s", runVars)

    if plotVars is not None:
        runVars_ = []
        for v in runVars:
            if v in plotVars:
                runVars_.append(v)
        if len(runVars_) == 0:
            raise RuntimeError("No variable selected")
        runVars = runVars_
                
    runProcs = [x for x in procsTRF if x in procs]
    logging.info("Will process processes: %s", runProcs)

    canvases = []
    canvases_nom = []

    if isSR:
        ratioText = "#frac{SR}{SR_{TRF}}"
    else:
        ratioText = "#frac{CR}{CR_{TRF}}"
        
    for var in runVars:
        hTRF_sum = None
        h_sum = None
        h_sum_b = None
        hTRF_sum_b = None
        for ivar, proc in enumerate(runProcs):
            logging.info("=== Process: %s ===", proc)
            nameTRF = "{}__{}__{}".format(proc, catTRF[0], var)
            name =  "{}__{}__{}".format(proc, cat, var)


            legendElem = [cat if leg is None else leg.replace("_"," ")]
            for iTRF in range(len(rFilesTRF)):
                legendElem.append( catTRF[iTRF] if legTRF[iTRF] is None else legTRF[iTRF].replace("_"," ") )

            logging.debug("legendElem : %s", legendElem)
                
        
            colors = [ROOT.kRed, ROOT.kMagenta, ROOT.kGreen+2, ROOT.kCyan]

            hTRFs = []
            for iTRF, rFileTRF in enumerate(rFilesTRF):
                logging.debug("Loading %s from %s", nameTRF,rFileTRF)
                hTRFs.append(rFileTRF.Get(nameTRF))
                logging.debug("hTRFs = %s", hTRFs[iTRF])
                hTRFs[iTRF].SetLineColor(colors[iTRF])

                
            h = rFile.Get(name)
            h.SetLineColor(ROOT.kBlue)
            
            if ivar == 0:
                hTRF_sum = []
                for iTRF in range(len(rFilesTRF)):
                    hTRF_sum.append(hTRFs[iTRF].Clone("combinedtt_"+hTRFs[iTRF].GetName()))
                h_sum =  h.Clone("combinedtt_"+h.GetName())
            else:
                for iTRF in range(len(rFilesTRF)):
                    hTRF_sum[iTRF].Add(hTRFs[iTRF])
                h_sum.Add(h)


            if proc in ["ttbarPlusB", "ttbarPlus2B", "ttbarPlusBBbar"]:
                if h_sum_b is None:
                    h_sum_b = h.Clone("ttB_"+h.GetName())
                    hTRF_sum_b = []
                    for iTRF in range(len(rFilesTRF)):
                        hTRF_sum_b.append(hTRFs[iTRF].Clone("ttB_"+hTRFs[iTRF].GetName()))
                else:
                    for iTRF in range(len(rFilesTRF)):
                        hTRF_sum_b[iTRF].Add(hTRFs[iTRF])
                    h_sum_b.Add(h)

                
            thisRatio = RatioPlot("{}_{}".format(proc, var))
            thisRatio.passHistos([h]+hTRFs)
            thisRatio.addLabel("Process: "+proc, xStart= 0.46, yStart=0.96)
            thisRatio.ratioText = ratioText
            thisRatio.thisCMSLabel = "Simulation"
            thisRatio.thisLumi = lumi
            thisRatio.moveCMSTop = 0.085
            thisRatio.CMSLeft = True
            thisRatio.CMSOneLine = True
            thisRatio.addKSTest = False
            thisRatio.KSxStart = 0.15
            thisRatio.KSyStart = 0.86
            thisRatio.drawRatioLineErrors = True
            thisRatio.ratioRelUncertaintySelf = True
            thisRatio.legendSize = (0.5,0.7,0.9,0.9)

            canvases.append(
                thisRatio.drawPlot(
                    legendElem,
                    xTitle = var,
                    logscale = True,
                    #noErrRatio = True,
                    drawCMS = True
                )
            )

            thisRatio_nom = RatioPlot("nom_{}_{}".format(proc, var))
            thisRatio_nom.passHistos([h]+hTRFs, normalize=True)
            thisRatio_nom.addLabel("Process: "+proc, xStart= 0.46, yStart=0.96)
            thisRatio_nom.ratioText = ratioText
            thisRatio_nom.thisCMSLabel = "Simulation"
            thisRatio_nom.thisLumi = lumi
            thisRatio_nom.moveCMSTop = 0.085
            thisRatio_nom.CMSLeft = True
            thisRatio_nom.CMSOneLine = True
            thisRatio_nom.addKSTest = False
            thisRatio_nom.KSxStart = 0.15
            thisRatio_nom.KSyStart = 0.86
            thisRatio_nom.drawRatioLineErrors = True
            thisRatio_nom.ratioRelUncertaintySelf = True
            thisRatio_nom.legendSize = (0.5,0.7,0.9,0.9)
            canvases_nom.append(
                thisRatio_nom.drawPlot(
                    legendElem,
                    xTitle = var,
                    floatingMax=  1.35,
                    #noErrRatio = False,
                    drawCMS = True
                )
            )

        ######################################################################

        thisRatio = RatioPlot("combinedtt_{}_{}".format(proc, var))
        thisRatio.passHistos([h_sum]+hTRF_sum)
        thisRatio.addLabel("Process: Summed tt", xStart= 0.46, yStart=0.96)
        thisRatio.ratioText = ratioText
        thisRatio.thisCMSLabel = "Simulation"
        thisRatio.thisLumi = lumi
        thisRatio.moveCMSTop = 0.085
        thisRatio.CMSLeft = True
        thisRatio.CMSOneLine = True
        thisRatio.addKSTest = False
        thisRatio.KSxStart = 0.15
        thisRatio.KSyStart = 0.86
        thisRatio.drawRatioLineErrors = True
        thisRatio.ratioRelUncertaintySelf = True
        thisRatio.legendSize = (0.5,0.7,0.9,0.9)

        canvases.append(
            thisRatio.drawPlot(
                legendElem,
                xTitle = var,
                logscale = True,
                #noErrRatio = True,
                drawCMS = True
            )
        )

        thisRatio_nom = RatioPlot("combinedtt_nom_{}_{}".format(proc, var))
        thisRatio_nom.passHistos([h_sum]+hTRF_sum, normalize=True)
        thisRatio_nom.addLabel("Process: Summed tt", xStart= 0.46, yStart=0.96)
        thisRatio_nom.ratioText = ratioText
        thisRatio_nom.thisCMSLabel = "Simulation"
        thisRatio_nom.thisLumi = lumi
        thisRatio_nom.moveCMSTop = 0.085
        thisRatio_nom.CMSLeft = True
        thisRatio_nom.CMSOneLine = True
        thisRatio_nom.addKSTest = False
        thisRatio_nom.KSxStart = 0.15
        thisRatio_nom.KSyStart = 0.86
        thisRatio_nom.drawRatioLineErrors = True
        thisRatio_nom.ratioRelUncertaintySelf = True
        thisRatio_nom.legendSize = (0.5,0.7,0.9,0.9)
        canvases_nom.append(
            thisRatio_nom.drawPlot(
                legendElem,
                xTitle = var,
                floatingMax=  1.35,
                #noErrRatio = True,
                drawCMS = True
            )
        )


        thisRatio_b = RatioPlot("ttB_{}_{}".format(proc, var))
        thisRatio_b.passHistos([h_sum_b]+hTRF_sum_b)
        thisRatio_b.addLabel("Process: ttB", xStart= 0.46, yStart=0.96)
        thisRatio_b.ratioText = ratioText
        thisRatio_b.thisCMSLabel = "Simulation"
        thisRatio_b.thisLumi = lumi
        thisRatio_b.moveCMSTop = 0.085
        thisRatio_b.CMSLeft = True
        thisRatio_b.CMSOneLine = True
        thisRatio_b.addKSTest = False
        thisRatio_b.KSxStart = 0.15
        thisRatio_b.KSyStart = 0.86
        thisRatio_b.drawRatioLineErrors = True
        thisRatio_b.ratioRelUncertaintySelf = True
        thisRatio_b.legendSize = (0.5,0.7,0.9,0.9)

        canvases.append(
            thisRatio_b.drawPlot(
                legendElem,
                xTitle = var,
                logscale = True,
                #noErrRatio = True,
                drawCMS = True
            )
        )

        thisRatio_b_nom = RatioPlot("ttB_nom_{}_{}".format(proc, var))
        thisRatio_b_nom.passHistos([h_sum_b]+hTRF_sum_b, normalize=True)
        thisRatio_b_nom.addLabel("Process: ttB", xStart= 0.46, yStart=0.96)
        thisRatio_b_nom.ratioText = ratioText
        thisRatio_b_nom.thisCMSLabel = "Simulation"
        thisRatio_b_nom.thisLumi = lumi
        thisRatio_b_nom.moveCMSTop = 0.085
        thisRatio_b_nom.CMSLeft = True
        thisRatio_b_nom.CMSOneLine = True
        thisRatio_b_nom.addKSTest = False
        thisRatio_b_nom.KSxStart = 0.15
        thisRatio_b_nom.KSyStart = 0.86
        thisRatio_b_nom.drawRatioLineErrors = True
        thisRatio_b_nom.ratioRelUncertaintySelf = True
        thisRatio_b_nom.legendSize = (0.5,0.7,0.9,0.9)
        canvases_nom.append(
            thisRatio_b_nom.drawPlot(
                legendElem,
                xTitle = var,
                floatingMax=  1.35,
                #noErrRatio = True,
                drawCMS = True
            )
        )

    
    saveCanvasListAsPDF(canvases, output, folder)
    saveCanvasListAsPDF(canvases_nom, "normalized_"+output, folder)
            


if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description="Plot all systematics in the sparsinator output file")
    argumentparser.add_argument("--logging", help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages", type=int,default=20)
    argumentparser.add_argument("--input", nargs="+", help="Input files", type=str,required=True)
    argumentparser.add_argument("--cat", nargs="+", help="Categories", type=str,required=True)
    argumentparser.add_argument("--output", help="Ouptut filename", type=str,required=True)
    argumentparser.add_argument("--folder", help="Ouptut folder", type=str,default=".")
    argumentparser.add_argument("--leg", nargs ="+", help="Categories w/o TRF", type=str,default=None)
    argumentparser.add_argument("--isSR", action = "store_true", help = "Set SR or CR")
    
    args = argumentparser.parse_args()
    
    initLogging(args.logging, "25")

    checkNcreateFolder(args.folder)

    assert len(args.cat) == len(args.input)
    thisCat = args.cat[0]
    thisCatTRF = args.cat[1:]
    thisInput = args.input[0]
    thisInputTRF = args.input[1:]
    if args.leg is None:
        leg = None
        legTRF = [None]*len(thisCatTRF)
    else:
        assert len(args.leg) == len(args.input)
        leg = args.leg[0]
        legTRF = args.leg[1:]
        
    
    main(thisInput, thisInputTRF, thisCat, thisCatTRF, args.folder, args.output, leg, legTRF, isSR=args.isSR)
