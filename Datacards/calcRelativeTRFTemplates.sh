#!/bin/bash
FOLDER=$1
NOMTEMPLATES=$2
NOMTEMPLATES_TTIncl=$3
VAR=$4

ADDFLAG=" --forceDir"
#ADDFLAG=""

#POSTFIX="_rebinned"
POSTFIX=""

python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarOther${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarOther${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR} ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --nom ${NOMTEMPLATES_TTIncl} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}

python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarOther${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarOther${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --nom ${NOMTEMPLATES_TTIncl} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}

python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarOther${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_tune_down_ttbbMerged_ttbarOther${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_tune_down_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --nom ${NOMTEMPLATES_TTIncl} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_tune_down_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}

python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarOther${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_tune_up_ttbbMerged_ttbarOther${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_tune_up_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --nom ${NOMTEMPLATES_TTIncl} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTIncl_nom_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --varTRF ${FOLDER}/TTIncl_tune_up_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}

#python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTbb_nom_ttbbMerged_ttbarOther${POSTFIX}.root --varTRF ${FOLDER}/TTbb_hdamp_down_ttbbMerged_ttbarOther${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTbb_nom_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --varTRF ${FOLDER}/TTbb_hdamp_down_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}
#python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTbb_nom_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --varTRF ${FOLDER}/TTbb_hdamp_down_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --nom ${NOMTEMPLATE} --var ${VAR}  ${ADDFLAG}

#python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTbb_nom_ttbbMerged_ttbarOther${POSTFIX}.root --varTRF ${FOLDER}/TTbb_hdamp_up_ttbbMerged_ttbarOther${POSTFIX}.root --nom ${NOMTEMPLATESl} --var ${VAR}  ${ADDFLAG}
python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTbb_nom_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --varTRF ${FOLDER}/TTbb_hdamp_up_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}
#python calcRelativeTRFTemplates.py --nomTRF ${FOLDER}/TTbb_nom_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --varTRF ${FOLDER}/TTbb_hdamp_up_ttbbMerged_ttbarPlusCCbar${POSTFIX}.root --nom ${NOMTEMPLATES} --var ${VAR}  ${ADDFLAG}


python renameVariatedSamples.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarOther${POSTFIX}_${VAR}_relTRF.root --systematic CMS_ttHbb_HDAMP --systematicDir Down 
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_HDAMP --systematicDir Down
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarPlusCCbar${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_HDAMP --systematicDir Down

python renameVariatedSamples.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarOther${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_HDAMP --systematicDir Up 
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_HDAMP --systematicDir Up 
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarPlusCCbar${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_HDAMP --systematicDir Up

python renameVariatedSamples.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_ttbarOther${POSTFIX}_${VAR}_relTRF.root --systematic CMS_ttHbb_UE --systematicDir Down
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}_${VAR}_relTRF.root --systematic CMS_ttHbb_UE --systematicDir Down
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_ttbarPlusCCbar${POSTFIX}_${VAR}_relTRF.root --systematic CMS_ttHbb_UE --systematicDir Down

python renameVariatedSamples.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_ttbarOther${POSTFIX}_${VAR}_relTRF.root --systematic CMS_ttHbb_UE --systematicDir Up
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_UE --systematicDir Up
python renameVariatedSamples.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_ttbarPlusCCbar${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_UE --systematicDir Up

python renameVariatedSamples.py --input ${FOLDER}/TTbb_hdamp_down_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}_${VAR}_relTRF.root    --systematic CMS_ttHbb_HDAMP --systematicDir Down
python renameVariatedSamples.py --input ${FOLDER}/TTbb_hdamp_up_ttbbMerged_ttbarPlusBBbarMerged${POSTFIX}_${VAR}_relTRF.root  --systematic CMS_ttHbb_HDAMP --systematicDir Up 


rm -v ${FOLDER}/TTbb_*ttbarPlusBBbarMerged*${VAR}*_relTRF.root
rm -v ${FOLDER}/TTIncl_*ttbarOther*${VAR}*_relTRF.root
rm -v ${FOLDER}/TTIncl_*ttbarPlusBBbarMerged*${VAR}*_relTRF.root
rm -v ${FOLDER}/TTIncl_*ttbarPlusCCbar*${VAR}*_relTRF.root

#hadd ${FOLDER}/TTbb_ttbarOther_systamtics.root  ${FOLDER}/TTbb_*ttbarOther*_renamed.root
hadd -f ${FOLDER}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root  ${FOLDER}/TTbb_*ttbarPlusBBbarMerged*${VAR}*_renamed.root
rm -v ${FOLDER}/TTbb_*ttbarPlusBBbarMerged*${VAR}*_renamed.root
#hadd ${FOLDER}/TTbb_ttbarPlusCCbar_systamtics.root  ${FOLDER}/TTbb_*ttbarPlusCCbar*_renamed.root

hadd -f ${FOLDER}/TTIncl_ttbarOther_${VAR}_relTRF_systamtics.root  ${FOLDER}/TTIncl_*ttbarOther*${VAR}*_renamed.root
rm  -v ${FOLDER}/TTIncl_*ttbarOther*${VAR}*_renamed.root

hadd -f ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root  ${FOLDER}/TTIncl_*ttbarPlusBBbarMerged*${VAR}*_renamed.root
rm -v ${FOLDER}/TTIncl_*ttbarPlusBBbarMerged*${VAR}*_renamed.root

hadd -f ${FOLDER}/TTIncl_ttbarPlusCCbar_${VAR}_relTRF_systamtics.root  ${FOLDER}/TTIncl_*ttbarPlusCCbar*${VAR}*_renamed.root
rm -v ${FOLDER}/TTIncl_*ttbarPlusCCbar*${VAR}*_renamed.root


