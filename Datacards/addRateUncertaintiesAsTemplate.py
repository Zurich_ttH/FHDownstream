from __future__ import print_function

import copy
import time
import logging
import sys
import os

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging, makeCanvasOfHistos, saveCanvasListAsPDF

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def addRateUncertaintiesAsTemplate(inFile, rateUp, rateDown, sysName, processes, cat, rectify=True):
    logging.debug("Got arguments:")
    logging.debug("  inFile    : %s",inFile)
    logging.debug("  rateUp    : %s",rateUp)
    logging.debug("  rateDown  : %s",rateDown)
    logging.debug("  sysName   : %s",sysName)
    logging.debug("  processes : %s",processes)
    logging.debug("  cat       : %s",cat)
    logging.debug("  rectify   : %s",rectify)

    if rectify and rateUp <= 1 and rateDown > 1:
        rateUp_ = rateDown
        rateDown_ = rateUp
        
        rateUp = rateUp_
        rateDown = rateDown_
        
        logging.warning("Inverted Rates")

    logging.info("UP : %s    ----    DOWN : %s", rateUp, rateDown)
        
    rFile = ROOT.TFile.Open(inFile)
    logging.info("Opened: %s", rFile)


    nomKeys = [key.GetName() for key in rFile.GetListOfKeys() if len(key.GetName().split("__")) == 3]
    nomKeys = [key for key in nomKeys if key.split("__")[0] in processes and cat in key]

    logging.info("Relevant nominal keys: %s", nomKeys)
    if not nomKeys:
        logging.error("No relevant nominal keys")
        exit()


    outHistos = []
    for nomKey in nomKeys:
        hNom = rFile.Get(nomKey)

        hUp = hNom.Clone(nomKey+"__"+sysName+"Up")
        hUp.SetTitle(nomKey+"__"+sysName+"Up")
        hDown = hNom.Clone(nomKey+"__"+sysName+"Down")
        hDown.SetTitle(nomKey+"__"+sysName+"Down")
        
        hUp.Scale(rateUp)
        hDown.Scale(rateDown)

        outHistos.append(copy.deepcopy(hUp))
        outHistos.append(copy.deepcopy(hDown))

    outFileName = inFile.replace(".root", "_rateAdded_{}_{}_{}.root".format("_".join(processes), cat, sysName))
    logging.info("Will output histos to: %s", outFileName)
    rOut = ROOT.TFile(outFileName, "RECREATE")
    rOut.cd()

    for h in outHistos:
        h.Write()

    rOut.Close()    

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script generating a file with Up/Down rate variation of a sparse file as separate template',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--input",
        action = "store",
        help = "Input file name",
        type = str,
        required=True
    ) 
    argumentparser.add_argument(
        "--rateup",
        action = "store",
        help = "Rate of the up variation. Should be 1.XX",
        type = float,
        required=True
    )
    argumentparser.add_argument(
        "--ratedown",
        action = "store",
        help = "Rate of the down variation. Should be 0.XX",
        type = float,
        required=True
    )
    argumentparser.add_argument(
        "--name",
        action = "store",
        help = "Name of the rate variation template",
        type = str,
        required=True
    )
    argumentparser.add_argument(
        "--processes",
        action = "store",
        help = "Name of the processes.",
        type = str,
        nargs = "+",
        required=True
    )    
    argumentparser.add_argument(
        "--cat",
        action = "store",
        help = "Name of the category",
        type = str,
        required=True
    )    


    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging)

    addRateUncertaintiesAsTemplate(args.input, args.rateup, args.ratedown, args.name, args.processes, args.cat)
