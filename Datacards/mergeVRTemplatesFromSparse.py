import sys
import os 
from glob import glob
import logging
import time
import copy

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

import ROOT

def mergeVRTemplatesFromSparse(folderA, folderOut):
    t0 = time.time()
    filesFolderA = [x.split("/")[-1] for x in glob(folderA+"/*.root")]

    for file_ in filesFolderA:
        tfile = time.time()
        logging.info("Proceessing file %s", file_)

        histosA = {}
        
        rFileA = ROOT.TFile.Open(folderA+"/"+file_)
        for ikey, key in enumerate(rFileA.GetListOfKeys()):
            if ikey%5000 == 0:
                logging.info("Rebinning template {0}/{3} | Total time: {1:8f} | File time {2:8f}".format(ikey, time.time()-t0,time.time()-tfile, len(rFileA.GetListOfKeys())))
            keyName = key.GetName()
            cleanName = keyName.replace("_left","").replace("_right","")
            if cleanName not in histosA.keys():
                histosA[cleanName] = {"left" : None, "right" : None}
            if "_left" in keyName:
                if histosA[cleanName]["left"] is None:
                    histosA[cleanName]["left"] = copy.deepcopy(rFileA.Get(keyName))
                else:
                    raise RuntimeError                    
            if "_right" in keyName:
                if histosA[cleanName]["right"] is None:
                    histosA[cleanName]["right"] = copy.deepcopy(rFileA.Get(keyName))
                else:
                    raise RuntimeError

        rFileA.Close() 


        rOut = ROOT.TFile(folderOut+"/"+file_, "RECREATE")
        rOut.cd()

        logging.info("Created rOut: %s", rOut)
        
        for key in histosA:
            if  histosA[cleanName]["left"] is None or  histosA[cleanName]["right"] is None:
                raise RuntimeError
            
            outHisto = histosA[key]["left"].Clone(key)
            outHisto.SetTitle(key)
            outHisto.Add(histosA[key]["right"])

            outHisto.Write()

        rOut.Close()
            
if __name__ == "__main__":
    folderA = sys.argv[1]
    folderOut = sys.argv[2]

    initLogging(20)
    
    mergeVRTemplatesFromSparse(folderA, folderOut)
