import sys
import os
import logging
import ROOT
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from copy import deepcopy

def makeVariationsUE(file5FSNominal, file5FS, file4FSNominal, file4FS, proc, skipVars=None):
    logging.info("Got file5FSNominal : %s", file5FSNominal)
    logging.info("Got file5FS : %s", file5FS)
    logging.info("Got file4FSNominal : %s", file4FSNominal)
    logging.info("Got file4FS : %s", file4FS)


    
    r5FS = ROOT.TFile.Open(file5FS)
    
    allKeys = [key.GetName() for key in r5FS.GetListOfKeys() if proc in key.GetName()]

    
    allProcs = []
    allVars = []
    allCats = []
    relevantSysts = []
    for key in allKeys:
        elem = key.split("__")
        if elem[0] not in allProcs:
            allProcs.append(elem[0])
        if elem[1] not in allCats:
            allCats.append(elem[1])
        if elem[2] not in allVars:
            allVars.append(elem[2])
        if len(elem) > 3:
            if "UE" in elem[3]:
                if elem[3] not in relevantSysts:
                    relevantSysts.append(elem[3])
    logging.info("Found processes: %s", allProcs)
    logging.info("Found variables: %s", allVars)
    logging.info("Found categories: %s", allCats)
        
    logging.info("Radiation syst names: %s", relevantSysts)

    r5FSNom = ROOT.TFile.Open(file5FSNominal)
    r4FSNom = ROOT.TFile.Open(file4FSNominal)

    nomKeys = [key.GetName() for key in r5FSNom.GetListOfKeys() if len(key.GetName().split("__")) == 3]
    
    if skipVars is not None:
        for skipVar in skipVars:
            allVars = [v for v in allVars if v != skipVar]
    

    variations = []
    for cat in allCats:
        logging.info("Processing cat: %s", cat)
        for var in allVars:
            logging.info("Processing var: %s", var)

            if "{}__{}__{}".format(proc, cat, var) not in nomKeys:
                logging.warning("%s not in allKeys. Skipping", "{}__{}__{}".format(proc, cat, var))
                continue
            
            nom5FS = r5FSNom.Get("{}__{}__{}".format(proc, cat, var))
            nom4FS = r4FSNom.Get("{}__{}__{}".format(proc, cat, var))
            
            logging.info("Nominal 4FS: %s", nom4FS)
            logging.debug("Integral Nominal 4FS: %s", nom4FS.Integral())
            #logging.debug("Bins nom 4FS: %s", list(nom4FS))
            logging.info("Nominal 5FS: %s", nom5FS)
            logging.debug("Integral Nominal 5FS: %s", nom5FS.Integral())
            #]logging.debug("Bins nom 5FS: %s", list(nom5FS))
            for syst in relevantSysts:
                thisSystTemplateName = "{}__{}__{}__{}".format(proc, cat, var, syst)
                logging.info(thisSystTemplateName)
                thisSystRel = r5FS.Get(thisSystTemplateName).Clone(thisSystTemplateName.replace("UE", "UETransplated"))

                logging.debug("Init integral: %s", thisSystRel.Integral())
                #logging.debug("Init bins: %s", list(thisSystRel))
                thisSystRel.Divide(nom5FS)

                logging.debug("Integral after div 5FSNom: %s", thisSystRel.Integral())
                #logging.debug("Bins after div 5FSNom: %s", list(thisSystRel))
                
                thisSystRel.Multiply(nom4FS)

                logging.debug("Integral after mult 4FSNom: %s", thisSystRel.Integral())
                #logging.debug("Bins after mult 4FSNom: %s", list(thisSystRel))
                
                variations.append(deepcopy(thisSystRel))

    r5FS.Close()
    r5FSNom.Close()
    r4FSNom.Close()

    outFile = ROOT.TFile(file4FS.replace(".root", "_TransplantedUE.root"), "RECREATE")
    
    for h in variations:
        logging.info("Writing: %s", h.GetName())
        h.Write()

    outFile.Close()


    
if __name__ == "__main__":
    initLogging(10, funcLen = 21)

    file5FS = sys.argv[1]
    file5FSNominal = sys.argv[2]
    file4FS = sys.argv[3]
    file4FSNominal = sys.argv[4]
    proc = sys.argv[5]
    if len(sys.argv) > 6:
        skipVars = sys.argv[6:]
    else:
        skipVars = None

    print skipVars
    
    makeVariationsUE(file5FSNominal, file5FS, file4FSNominal, file4FS, proc, skipVars)
