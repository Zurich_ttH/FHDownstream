from __future__ import print_function

import sys
import os
import datetime
from glob import glob

from TTH.MEAnalysis.ParHadd import par_hadd
from calcPDFWeightFromMembers import calcPDFWeightFromMembers
import random
import string

def get_random_string(length):
    # With combination of lower and upper case
    result_str = ''.join(random.choice(string.ascii_letters) for i in range(length))
    # print random string
    print(result_str)
    return result_str 

def mergeCTCVCPGCOut(basepath, outpath, skipMerge=False):

    print("Basepath : %s"%basepath)
    print("outpath : %s"%outpath)
    
    samples = os.listdir(basepath)
    prefixes = []
    
    for sample in samples:
        for weight in os.listdir(basepath+"/"+sample):
            filePrefix = "P"+str(weight.split("_")[-1])
            if filePrefix not in prefixes:
                prefixes.append(filePrefix)
            files = glob(basepath+"/"+sample+"/"+weight+"/*.root")
            if not skipMerge:
                par_hadd(outpath + "{}_{}.root".format(filePrefix, sample),
                         files,
                         250, 10, 3, prefix = get_random_string(8))
            
    return samples, prefixes
   
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Process the CTCVCP samples'
    )
    argumentparser.add_argument(
        "--gcPaths",
        action = "store",
        help = "Base paths for GC outputs",
        nargs="+",
        required= True
    )
    argumentparser.add_argument(
        "--outputPath",
        action = "store",
        help = "Output path.",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--mergeOutPrefix",
        action = "store",
        help = "Prefix for the merged output file.",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--rebinJSONS",
        action = "store",
        help = "Tag for the datacard",
        nargs = 3,
        required = True
    )
    argumentparser.add_argument(
        "--addTemplates",
        action = "store",
        help = "additonal templates added to the merged file for hte datacard generation",
        type = str,
        nargs="+",
        default = []
    )
    argumentparser.add_argument(
        "--skipGCMerge",
        action = "store_true",
    )
    argumentparser.add_argument(
        "--skipSampleMerge",
        action = "store_true",
    )
    argumentparser.add_argument(
        "--skipRebining",
        action = "store_true",
    )
    argumentparser.add_argument(
        "--istH",
        action = "store_true",
    )
    

    args = argumentparser.parse_args()


    infoLines = ["Created on "+datetime.datetime.now().strftime("%A %d. %B %Y - %H:%M:%S")]
    infoLines += ["$CMSSW_BASE: "+str(os.environ["CMSSW_BASE"])]
    infoLines += ["Command line options:"]
    infoLines += ["python "+" ".join(sys.argv).replace("--","\n  --")]
    with open(args.outputPath+"/processNEWCPSamples_"+args.mergeOutPrefix+".txt", "w") as f:
        for line in infoLines:
            f.write(line+"\n")

    
    allProcessedSamples = []
    allPrefixes = None
    for gcOutBase in args.gcPaths:
        processedSamples, prefixes = mergeCTCVCPGCOut(gcOutBase, args.outputPath, args.skipGCMerge)
        if allPrefixes is None:
            allPrefixes = prefixes
        else:
            for p in prefixes:
                if p not in allPrefixes:
                    raise RuntimeWarning("Prefix %s is new. Make sure this is okay"%p)
        for sample in processedSamples:
            if sample in allProcessedSamples:
                raise RuntimeError
        allProcessedSamples+=processedSamples


    for sample in allProcessedSamples:
        if sample.startswith("THQ"):
            templatepostfixPDF = "_tHq"
        elif sample.startswith("THW"):
            templatepostfixPDF = "_tHW"
        else:
            templatepostfixPDF = ""

        for p in allPrefixes:
            print("Merging PDFs for %s"%("{}/{}_{}.root".format(args.outputPath, p, sample)))
            calcPDFWeightFromMembers("{}/{}_{}.root".format(args.outputPath, p, sample), "MergedPDF", templatepostfixPDF)

    mergedFiles = []
    for p in allPrefixes:
        print(args.outputPath, p)
        files2Merge = glob("{}/{}_*_MergedPDF.root".format(args.outputPath, p))
        outputFile = args.outputPath + "/{}_{}.root".format(args.mergeOutPrefix, p)
        mergedFiles.append(outputFile)
        if not args.skipSampleMerge:
            print("par_hadd arg 1 : %s"%outputFile)
            print("par_hadd arg 2 : "+str(files2Merge+args.addTemplates))
            par_hadd(outputFile,
                     files2Merge+args.addTemplates,
                     250, 10, 3)
            #raw_input("Next merge")
            
    if not args.skipRebining:
        print("Rebinning")
        for file_ in mergedFiles:
            for icat,cat in enumerate(["fh_j7_t4", "fh_j8_t4", "fh_j9_t4"]):
                rebinCommand =  "python {}/src/TTH/FHDownstream/Datacards/genOptimizedTemplates.py".format(os.environ["CMSSW_BASE"])
                rebinCommand += " --input {}".format(file_)
                rebinCommand += " --outFolder {}".format(args.outputPath)
                rebinCommand += " --cat {}".format(cat)
                rebinCommand += " --rebinInfo {}".format(args.rebinJSONS[icat])
                rebinCommand += " --binEdgeLow 0 --binEdgeHigh 1"
                print(rebinCommand)
                os.system(
                    rebinCommand+" &> /dev/null"
                )

                
            inFileName = file_.split("/")[-1].replace(".root","")
            outFiles = glob(args.outputPath+"/"+inFileName+"*fh_*.root")
            haddCommand = "hadd {}_rebinned.root".format(file_.replace(".root",""))
            rmCommand = "rm"
            for outFile in outFiles:
                haddCommand += " "+outFile
                rmCommand += " "+outFile
            print(haddCommand)
            os.system(
                haddCommand
            )
            print(rmCommand)
            os.system(
                rmCommand
            )
