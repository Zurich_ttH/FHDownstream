#!/bin/bash
FOLDER=$1
FOLDERINIT=$2

VAR="RecoHiggsPt"
EDGELOW="0"
EDGEHIGH="600"

REBININFO7="../data/templateBinning/rebin_HiggsPt_60_to_STXS.json"
REBININFO8="../data/templateBinning/rebin_HiggsPt_60_to_STXS.json"
REBININFO9="../data/templateBinning/rebin_HiggsPt_60_to_STXS.json"


python genOptimizedTemplates.py --input $FOLDERINIT/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH} --var ${VAR}
python genOptimizedTemplates.py --input $FOLDERINIT/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH} --var ${VAR}
python genOptimizedTemplates.py --input $FOLDERINIT/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH} --var ${VAR}

python genOptimizedTemplates.py --input $FOLDERINIT/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH} --var ${VAR}
python genOptimizedTemplates.py --input $FOLDERINIT/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH} --var ${VAR}
python genOptimizedTemplates.py --input $FOLDERINIT/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH} --var ${VAR}

hadd -f ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_${VAR}*.root 
hadd -f ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_${VAR}*.root

rm -v ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_${VAR}*.root
rm -v ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_${VAR}*.root



python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ttbarPlusBBbarMerged
