#!/bin/bash
FOLDER=$1

#Add an underscore to the VAR!
VAR=""
POSTFIX="_rebinned"
#POSTFIX="_removed_DNN_Node0"
#POSTFIX=""


#hadd ${FOLDER}/TTbb_ttbarOther_systamtics.root  ${FOLDER}/TTbb_*ttbarOther*_renamed.root
hadd -f ${FOLDER}/TTbb_ttbarPlusBBbarMerged${VAR}_systamtics*${POSTFIX}.root  ${FOLDER}/TTbb_*ttbarPlusBBbarMerged*${VAR}*${POSTFIX}.root
rm -v ${FOLDER}/TTbb_*ttbarPlusBBbarMerged*${VAR}*${POSTFIX}.root
#hadd ${FOLDER}/TTbb_ttbarPlusCCbar_systamtics.root  ${FOLDER}/TTbb_*ttbarPlusCCbar*_renamed.root

hadd -f ${FOLDER}/TTIncl_ttbarOther${VAR}_systamtics*${POSTFIX}.root  ${FOLDER}/TTIncl_*ttbarOther*${VAR}*${POSTFIX}.root
rm  -v ${FOLDER}/TTIncl_*ttbarOther*${VAR}*${POSTFIX}.root

hadd -f ${FOLDER}/TTIncl_ttbarPlusBBbarMerged${VAR}_systamtics*${POSTFIX}.root  ${FOLDER}/TTIncl_*ttbarPlusBBbarMerged*${VAR}*${POSTFIX}.root
rm -v ${FOLDER}/TTIncl_*ttbarPlusBBbarMerged*${VAR}*${POSTFIX}.root

hadd -f ${FOLDER}/TTIncl_ttbarPlusCCbar${VAR}_systamtics*${POSTFIX}.root  ${FOLDER}/TTIncl_*ttbarPlusCCbar*${VAR}*${POSTFIX}.root
rm -v ${FOLDER}/TTIncl_*ttbarPlusCCbar*${VAR}*${POSTFIX}.root


