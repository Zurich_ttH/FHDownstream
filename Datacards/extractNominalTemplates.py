import sys

from glob import glob
from copy import deepcopy

import ROOT

def extractNominal(inFolder, outFolder):
    for file_ in glob(inFolder+"/*.root"):
        outHistos = []
        rFile = ROOT.TFile.Open(file_)
        rFile.cd()

        for key in rFile.GetListOfKeys():
            keyName = key.GetName()
            if len(keyName.split("__")) == 3:
                outHistos.append(
                    deepcopy(rFile.Get(keyName))
                )

        rFile.Close()

        fileNmae = file_.split("/")[-1]
        rOut = ROOT.TFile(outFolder+"/"+fileNmae, "RECREATE")
        rOut.cd()

        for h in outHistos:
            h.Write()
        

        
        

if __name__ == "__main__":
    inFolder = sys.argv[1]
    outFolder = sys.argv[2]

    print "Input",inFolder
    print "Output",outFolder

    
    if inFolder == outFolder:
        raise RuntimeError

    extractNominal(inFolder, outFolder)
