#!/bin/bash

FILE2016=LegacyRun2_2016_v5_v1_VR_inputs_v4_TTMerged_wVariationsForRate

#POSTDIX="rebinnedInputs"
POSTDIX="DNNRebinned"

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM
# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM
# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM

python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0


# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_5_25_to_12.json --binEdgeLow 0.5 --binEdgeHigh 4 --tag inputs_rebinned --var DNNInput_Detaj_5

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_7_30_to_10.json --binEdgeLow 0 --binEdgeHigh 5 --tag inputs_rebinned --var DNNInput_Detaj_7

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_6_30_to_13.json --binEdgeLow 0 --binEdgeHigh 5 --tag inputs_rebinned --var DNNInput_Detaj_6

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_bJets_Mass_jj_max_34_to_12.json --binEdgeLow 100 --binEdgeHigh 1800 --tag inputs_rebinned --var DNNInput_bJets_Mass_jj_max 

#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_R2_9J_v2.json --binEdgeLow 0 --binEdgeHigh 0.9 --tag inputs_rebinned --var DNNInput_R_2

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_H_0_7J.json --binEdgeLow 0.5 --binEdgeHigh 4 --tag inputs_rebinned --var DNNInput_H_0

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_H_0_9J.json --binEdgeLow 0 --binEdgeHigh 5.5 --tag inputs_rebinned --var DNNInput_H_0

hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2016/${FILE2016}_${POSTDIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2016/*inputs_rebinned.root

rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2016/*inputs_rebinned.root


###############################################################################

FILE2017=LegacyRun2_2017_v5_v1_VR_inputs_v4_TTMerged_wVariationsForRate

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM
# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM
# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM

python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0


# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_5_25_to_12.json --binEdgeLow 0.5 --binEdgeHigh 4 --tag inputs_rebinned --var DNNInput_Detaj_5

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_7_30_to_10.json --binEdgeLow 0 --binEdgeHigh 5 --tag inputs_rebinned --var DNNInput_Detaj_7

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_6_30_to_13.json --binEdgeLow 0 --binEdgeHigh 5 --tag inputs_rebinned --var DNNInput_Detaj_6

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_bJets_Mass_jj_max_34_to_12.json --binEdgeLow 100 --binEdgeHigh 1800 --tag inputs_rebinned --var DNNInput_bJets_Mass_jj_max 

#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_R2_9J_v2.json --binEdgeLow 0 --binEdgeHigh 0.9 --tag inputs_rebinned --var DNNInput_R_2

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_H_0_7J.json --binEdgeLow 0.5 --binEdgeHigh 4 --tag inputs_rebinned --var DNNInput_H_0

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_H_0_9J.json --binEdgeLow 0 --binEdgeHigh 5.5 --tag inputs_rebinned --var DNNInput_H_0

hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2017/${FILE2017}_${POSTDIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2017/*inputs_rebinned.root

rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2017/*inputs_rebinned.root



###############################################################################

FILE2018=LegacyRun2_2018_v5_v1_VR_inputs_v4_TTMerged_wVariationsForRate

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM
# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM
# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_MEM_40_to_10.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNNInput_MEM

python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json --binEdgeLow 0 --binEdgeHigh 1 --tag inputs_rebinned --var DNN_Node0


# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_5_25_to_12.json --binEdgeLow 0.5 --binEdgeHigh 4 --tag inputs_rebinned --var DNNInput_Detaj_5

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_7_30_to_10.json --binEdgeLow 0 --binEdgeHigh 5 --tag inputs_rebinned --var DNNInput_Detaj_7

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_DetaJ_6_30_to_13.json --binEdgeLow 0 --binEdgeHigh 5 --tag inputs_rebinned --var DNNInput_Detaj_6

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j8_t4 --rebinInfo ../data/templateBinning/rebin_bJets_Mass_jj_max_34_to_12.json --binEdgeLow 100 --binEdgeHigh 1800 --tag inputs_rebinned --var DNNInput_bJets_Mass_jj_max 

#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_R2_9J_v2.json --binEdgeLow 0 --binEdgeHigh 0.9 --tag inputs_rebinned --var DNNInput_R_2

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j7_t4 --rebinInfo ../data/templateBinning/rebin_H_0_7J.json --binEdgeLow 0.5 --binEdgeHigh 4 --tag inputs_rebinned --var DNNInput_H_0

# python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j9_t4 --rebinInfo ../data/templateBinning/rebin_H_0_9J.json --binEdgeLow 0 --binEdgeHigh 5.5 --tag inputs_rebinned --var DNNInput_H_0

hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2018/${FILE2018}_${POSTDIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2018/*inputs_rebinned.root

rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2018/*inputs_rebinned.root



