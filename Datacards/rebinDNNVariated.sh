#!/bin/bash
FOLDER=$1

REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json"
REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json"
REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json"

    
for file in `ls -f ${FOLDER}/*systamtics.root`; do  python genOptimizedTemplates.py --input $file --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1 ;done   
for file in `ls -f ${FOLDER}/*systamtics.root`; do  python genOptimizedTemplates.py --input $file --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1 ;done
for file in `ls -f ${FOLDER}/*systamtics.root`; do  python genOptimizedTemplates.py --input $file --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1 ;done

hadd ${FOLDER}/TTbb_ttbarOther_systamtics_rebinned.root ${FOLDER}/TTbb_ttbarOther*_DNN_Node0*.root
hadd ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics_rebinned.root ${FOLDER}/TTbb_ttbarPlusBBbarMerged*_DNN_Node0*.root
hadd ${FOLDER}/TTbb_ttbarPlusCCbar_systamtics_rebinned.root ${FOLDER}/TTbb_ttbarPlusCCbar*_DNN_Node0*.root

hadd ${FOLDER}/TTIncl_ttbarOther_systamtics_rebinned.root ${FOLDER}/TTIncl_ttbarOther*_DNN_Node0*.root
hadd ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics_rebinned.root ${FOLDER}/TTIncl_ttbarPlusBBbarMerged*_DNN_Node0*.root
hadd ${FOLDER}/TTIncl_ttbarPlusCCbar_systamtics_rebinned.root ${FOLDER}/TTIncl_ttbarPlusCCbar*_DNN_Node0*.root
