#!/bin/bash

FOLDER="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2018/v5/v1/inputs/v4/VR/initMerge/merged/"
OUTFOLDER="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/Run2/v5/v1/inputs/v4/VR/2018/"

REBININFO7="../../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json"
REBININFO8="../../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json"
REBININFO9="../../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json"

python ../genOptimizedTemplates.py --input ${FOLDER}/data.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ST_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/THQ_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/THW_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ttH_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarOther.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ttV.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/VV.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/WJets.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ZJets.root --outFolder ${OUTFOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1

python ../genOptimizedTemplates.py --input ${FOLDER}/data.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ST_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/THQ_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/THW_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ttH_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarOther.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ttV.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/VV.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/WJets.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ZJets.root --outFolder ${OUTFOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1

python ../genOptimizedTemplates.py --input ${FOLDER}/data.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ST_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/THQ_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/THW_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ttH_MergedPDF.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarOther.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ttV.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/VV.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/WJets.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
python ../genOptimizedTemplates.py --input ${FOLDER}/ZJets.root --outFolder ${OUTFOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1




hadd ${OUTFOLDER}/data_rebinned.root ${OUTFOLDER}/data_*Node0*.root
hadd ${OUTFOLDER}/ST_MergedPDF_rebinned.root ${OUTFOLDER}/ST_MergedPDF_*Node0*.root
hadd ${OUTFOLDER}/THQ_MergedPDF_rebinned.root ${OUTFOLDER}/THQ_MergedPDF_*Node0*.root
hadd ${OUTFOLDER}/THW_MergedPDF_rebinned.root ${OUTFOLDER}/THW_MergedPDF_*Node0*.root
hadd ${OUTFOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${OUTFOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_*Node0*.root
hadd ${OUTFOLDER}/ttH_MergedPDF_rebinned.root ${OUTFOLDER}/ttH_MergedPDF_*Node0*.root
hadd ${OUTFOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarOther_rebinned.root ${OUTFOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarOther_*Node0*.root
hadd ${OUTFOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar_rebinned.root ${OUTFOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar_*Node0*.root
hadd ${OUTFOLDER}/ttV_rebinned.root ${OUTFOLDER}/ttV_*Node0*.root
hadd ${OUTFOLDER}/VV_rebinned.root ${OUTFOLDER}/VV_*Node0*.root
hadd ${OUTFOLDER}/WJets_rebinned.root ${OUTFOLDER}/WJets_*Node0*.root
hadd ${OUTFOLDER}/ZJets_rebinned.root ${OUTFOLDER}/ZJets_*Node0*.root

rm -v ${OUTFOLDER}/*Node0*.root
