#!/bin/bash

FOLDER="/t3home/koschwei/scratch/ttH/datacardRepo/datacards_210125/HIG-19-011/cross_checks/FH_UZH/2016/v14p2/"
FILE="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2016/LegacyRun2_2016_v5_v1_crosscheck_v5_TTMerged_wVariationsForRate.root"

CONFIG="LegacyRun2_2016_v3p3_freeFloating.cfg"
TAG="ttFreeFloating"
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable ht30
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable jetsByPt_0_pt
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable jetsByPt_0_btag
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable numJets

CONFIG="LegacyRun2_2016_v3p3.cfg"
TAG="tRateparam"
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable ht30
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable jetsByPt_0_pt
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable jetsByPt_0_btag
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable numJets


