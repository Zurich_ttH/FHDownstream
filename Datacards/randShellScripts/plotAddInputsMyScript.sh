#!/bin/bash

cd ../../Plotting/DNN/

ERA="2017"
SUBFOLDER="lin/private/"


OUT="out_inputVars/v5/${ERA}/SR/v4p1/addInputs/${SUBFOLDER}"
FOLDER="/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/v5/v1/inputs/SR/addInputs/${ERA}/wotH/v4/cards"
mkdir -p ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_H_1_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_H_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_H_2_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_H_2_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_H_3_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_H_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_H_4_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_H_4_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_H_5_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_H_5_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_R_1_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_R_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_R_2_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_R_2_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_R_3_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_R_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_R_4_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_R_4_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_aplanarity_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_aplanarity_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_jets_bsort_eta_0_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_jets_bsort_eta_0_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_jets_bsort_eta_1_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_jets_bsort_eta_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_selJets_DeltaR_jj_max_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_selJets_DeltaR_jj_max_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_selJets_DeltaR_jj_min_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_selJets_DeltaR_jj_min_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_selJets_Mass_DeltaR_jj_H_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_selJets_Mass_DeltaR_jj_H_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_sphericity_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_sphericity_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_H_1_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_H_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_H_2_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_H_2_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_H_3_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_H_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_H_4_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_H_4_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_H_5_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_H_5_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_R_1_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_R_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_R_2_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_R_2_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_R_3_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_R_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_R_4_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_R_4_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_aplanarity_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_aplanarity_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_jets_bsort_eta_0_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_jets_bsort_eta_0_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_jets_bsort_eta_1_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_jets_bsort_eta_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_jets_bsort_pt_1_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_jets_bsort_pt_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_selJets_DeltaR_jj_avg_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_selJets_DeltaR_jj_avg_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_selJets_DeltaR_jj_max_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_selJets_DeltaR_jj_max_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_selJets_DeltaR_jj_min_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_selJets_DeltaR_jj_min_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_sphericity_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_sphericity_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_H_1_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_H_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_H_2_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_H_2_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_H_3_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_H_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_H_4_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_H_4_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_H_5_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_H_5_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_R_1_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_R_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_R_3_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_R_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_R_4_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_R_4_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_aplanarity_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_aplanarity_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_jets_bsort_eta_0_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_jets_bsort_eta_0_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_jets_bsort_eta_1_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_jets_bsort_eta_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_jets_bsort_pt_1_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_jets_bsort_pt_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_jets_pt_6_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_jets_pt_6_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_selJets_DeltaR_jj_avg_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_selJets_DeltaR_jj_avg_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_selJets_DeltaR_jj_max_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_selJets_DeltaR_jj_max_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_sphericity_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_sphericity_new.txt ${OUT}

OUT="out_inputVars/v5/${ERA}/SR/v5/addInputs/${SUBFOLDER}"
FOLDER="/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/v5/v1/inputs/SR/addInputs/${ERA}/wotH/v5/cards"
mkdir -p ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_H_3_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_H_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_H_3_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_H_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_bJets_Mass_jj_max_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_bJets_Mass_jj_max_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_jets_bsort_pt_1_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_jets_bsort_pt_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_H_3_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_H_3_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_bJets_Mass_jj_max_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_bJets_Mass_jj_max_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_jets_bsort_pt_1_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_jets_bsort_pt_1_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_jets_pt_6_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_jets_pt_6_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_selJets_DeltaR_jj_avg_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_selJets_DeltaR_jj_avg_new.txt ${OUT}

OUT="out_inputVars/v5/${ERA}/SR/v4/inputs/${SUBFOLDER}"
FOLDER="/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/v5/v1/inputs/SR/${ERA}/v4/cards"
mkdir -p ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_Detaj_5_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_Detaj_5_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_H_0_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_H_0_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j7_t4_DNNInput_MEM_new_DataTransplanted.root ${FOLDER}/card_fh_j7_t4_DNNInput_MEM_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_Detaj_6_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_Detaj_6_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_H_0_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_H_0_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_MEM_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_MEM_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j8_t4_DNNInput_bJets_Mass_jj_max_new_DataTransplanted.root ${FOLDER}/card_fh_j8_t4_DNNInput_bJets_Mass_jj_max_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_Detaj_7_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_Detaj_7_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_H_0_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_H_0_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_MEM_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_MEM_new.txt ${OUT}
python DNNPrefitPlots.py ${FOLDER}/fitDiagnosticsv2_card_fh_j9_t4_DNNInput_R_2_new_DataTransplanted.root ${FOLDER}/card_fh_j9_t4_DNNInput_R_2_new.txt ${OUT}


cd -
