#!/bin/bash

# FOLDER="/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/v5/v1/inputs/SR/addInputs/2016/wotH/"
# FILE="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2016/LegacyRun2_2016_v5_v1_addInputs_v4_TTMerged_wVariationsForRate.root"
# CONFIG="LegacyRun2_2016_v3p3_wotH.cfg"
# TAG="v4"
# #7J
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_1 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_2 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_3 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_4 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_5 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_1 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_2 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_3 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_4 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_aplanarity --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_sphericity --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_eta_0 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_eta_1 --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_max --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_min --category fh_j7_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_Mass_DeltaR_jj_H --category fh_j7_t4

# #8j
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_1 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_2 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_3 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_4 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_5 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_1 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_2 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_3 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_4 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_eta_0 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_eta_1 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_pt_1 --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_max --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_min --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_avg --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_aplanarity --category fh_j8_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_sphericity --category fh_j8_t4


# #9j
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_1 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_2 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_3 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_4 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_5 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_1 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_3 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_R_4 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_aplanarity --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_sphericity --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_eta_0 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_eta_1 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_pt_1 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_pt_6 --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_max --category fh_j9_t4
# python ../makeDatacards.py --config ../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_avg --category fh_j9_t4



FOLDER="/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/v5/v1/inputs/SR/addInputs/2018/wotH/"
FILE="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2018/LegacyRun2_2018_v5_v1_addInputs_v5_TTMerged_wVariationsForRate.root"
CONFIG="LegacyRun2_2018_v3p3_wotH.cfg"
TAG="v5"

python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_3 --category fh_j7_t4

python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_3 --category fh_j8_t4
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_bJets_Mass_jj_max --category fh_j8_t4
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_pt_1 --category fh_j8_t4

python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_H_3 --category fh_j9_t4
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_bJets_Mass_jj_max --category fh_j9_t4
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_bsort_pt_1 --category fh_j9_t4
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_jets_pt_6 --category fh_j9_t4
python ../makeDatacards.py --config ../../data/datacards/${CONFIG} --outputPath ${FOLDER} --tag ${TAG} --inputFile ${FILE} --cardPrefix card_ --force --skipMissing --variable DNNInput_selJets_DeltaR_jj_avg --category fh_j9_t4
