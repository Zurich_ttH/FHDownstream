#!/bin/bash
FOLDER=$1

REBININFO7="../data/templateBinning/rebin_HiggsPt_60_to_STXS.json"
REBININFO8="../data/templateBinning/rebin_HiggsPt_60_to_STXS.json"
REBININFO9="../data/templateBinning/rebin_HiggsPt_60_to_STXS.json"

VAR="RecoHiggsPt"
EDGELOW="0"
EDGEHIGH="600"
 
PROCS=( ttbarOther ttbarPlusBBbarMerged ttbarPlusCCbar)

for PROC in "${PROCS[@]}"
do
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH} --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}

    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}

    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow ${EDGELOW} --binEdgeHigh ${EDGEHIGH}  --var ${VAR}



    hadd -f ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}_*${VAR}*.root
    hadd -f ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}_*${VAR}*.root
    hadd -f ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}_*${VAR}*.root
    hadd -f ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}_*${VAR}*.root
    hadd -f ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}_*${VAR}*.root
    hadd -f ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}_*${VAR}*.root
    hadd -f ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}_*${VAR}*.root
    hadd -f ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}_*${VAR}*.root


    rm -v ${FOLDER}/*${VAR}_fh*.root

    
done
