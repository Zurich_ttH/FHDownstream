'''
 This script forces the normalization of each MC in VR+CR to be to be equal to systDown (QGL weight down) case.
 It has been made to avoid that QGL weight change the normalization of our MCs.
 Here we assume that QGL weight down corresponds to no QGL correction.
'''

import ROOT
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################

doPreSel = False
folder = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v3/extended/"
#folder = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v3_noDeta/"

fileNameSR = folder+"merged_SRext.root"
fileNameVR = folder+"merged_VRext.root"

fileNameCR = folder+"merged_CRext.root"
fileNameCR2 = folder+"merged_CR2ext.root"

fileNamePresel = folder+"merged_PreSel_newRange2.root"
#fileNamePresel = folder+"PreSel_noCorr/GCdeb9aabd934f/QCD.root"
if doPreSel:
    fileNameSR = fileNamePresel
    fileNameVR = ""
####################################################################################
####  IMPORTANT: replace CMS_ttH_CSVjesPileUpPtEC1 with QGL weight up & down!! #####
####################################################################################

systDown = "CMS_ttHbb_qgWeight_2017Down" #"CMS_ttH_CSVjesPileUpPtEC1Down"
systUp = "CMS_ttHbb_qgWeight_2017Up" #"CMS_ttH_CSVjesPileUpPtEC1Up"

def doFiles(fileNameSR, fileNameVR):
    logging.info("File %s",(fileNameSR))
    logging.info("and %s", fileNameVR)
    fileSR = ROOT.TFile.Open(fileNameSR)
    if not "PreSel" in fileNameSR:
        fileVR = ROOT.TFile.Open(fileNameVR)
    
    samples = set()
    categories = set()
    variables = set()
    scv = set() # set of (sample, category, variable)
    
    ## Get sample, category, and variable sets
    for histo in fileSR.GetListOfKeys():
        (sample,category,variable) = histo.GetTitle().split("__")[:3]
        if variable == "sixjetHT":
            continue
        if "PreSel" in fileNameSR and sample!="data" and "presel" in category:
            samples.add(sample)
            categories.add(category)
            variables.add(variable)
            scv.add((sample,category,variable))
        elif(sample!="data" and not "presel" in category):
            samples.add(sample)
            categories.add(category)
            variables.add(variable)
            #some variables are not in all categories
            scv.add((sample,category,variable))
    
    logging.info("I've found ...")
    logging.info("Samples: %s", samples)
    logging.info("Categories: %s", categories)
    logging.info("Variables: %s", variables)
    raw_input("Press ret to cont")
    ## Compute the correction
    
    corrNominal = {}
    corrUp = {}
    for (sample,category,variable) in scv:
        histoName = "%s__%s__%s"%(sample,category,variable)
        logging.debug(histoName)
        if "PreSel" in fileNameSR:
            yield_nominal = fileSR.Get(histoName).Integral()
            yield_down = fileSR.Get(histoName+"__"+systDown).Integral()
            yield_up = fileSR.Get(histoName+"__"+systUp).Integral()
        else:
            yield_nominal = fileSR.Get(histoName).Integral() + fileVR.Get(histoName).Integral()
            yield_down = fileSR.Get(histoName+"__"+systDown).Integral() + fileVR.Get(histoName+"__"+systDown).Integral()
            yield_up = fileSR.Get(histoName+"__"+systUp).Integral() + fileVR.Get(histoName+"__"+systUp).Integral()
            yield_nominal_SR, yield_nominal_VR = fileSR.Get(histoName).Integral(), fileVR.Get(histoName).Integral()
            yield_down_SR, yield_down_VR = fileSR.Get(histoName+"__"+systDown).Integral(), fileVR.Get(histoName+"__"+systDown).Integral()
            yield_up_SR, yield_up_VR = fileSR.Get(histoName+"__"+systUp).Integral(), fileVR.Get(histoName+"__"+systUp).Integral()

            
            
        logging.debug("yield_up=%s",yield_up)
        logging.debug("yield_down=%s",yield_down)
        logging.debug("yield_nominal=%s",yield_nominal)
        if yield_nominal > 0:
            corrNominal [(sample,category,variable)] = 1. * yield_down / yield_nominal
        else:
            logging.warning("Set corrNominal to 1.0 for %s", histoName)
            corrNominal [(sample,category,variable)] = 1.
        if yield_up > 0:
            corrUp [(sample,category,variable)]      = 1. * yield_down / yield_up
        else:
            logging.warning("Set corrUp to 1.0 for %s", histoName)
            corrUp [(sample,category,variable)]      = 1.
        
        if (sample,category,variable) == ("ttbarOther","fh_j7_t3","mem_FH_4w2h1t_p"):
            logging.info("yield_up=%s",yield_up)
            logging.info("yield_down=%s",yield_down)
            logging.info("yield_nominal=%s",yield_nominal)
            logging.info("SR yield_up=%s",yield_up_SR)
            logging.info("SR yield_down=%s",yield_down_SR)
            logging.info("SR yield_nominal=%s",yield_nominal_SR)
            logging.info("VR yield_up=%s",yield_up_VR)
            logging.info("VR yield_down=%s",yield_down_VR)
            logging.info("VR yield_nominal=%s",yield_nominal_VR)
            
    if not "PreSel" in fileNameSR:
        logging.info("corrNominal= %s",corrNominal [("ttbarOther","fh_j7_t3","mem_FH_4w2h1t_p")])
        logging.info("corrUp= %s",corrUp [("ttbarOther","fh_j7_t3","mem_FH_4w2h1t_p")])
    ## Write the new file
    
    aaa = None
    sampleOld = ""
    for file_ in [fileSR,fileVR] if not "PreSel" in fileNameSR else [fileSR]:
        filePath = file_.GetName()
        newFile = ROOT.TFile(filePath.replace(".root","_new.root"),"recreate")
        for histo in file_.GetListOfKeys():
            histo = histo.ReadObj()
            syst = ""
            words = histo.GetTitle().split("__")
            if len(words)>=4:
                (sample,category,variable,syst) = words[0:4]
            else:
                (sample,category,variable) = words[0:3]
            if "PreSel" in fileNameSR and not "presel" in category:
                continue
            if not "PreSel" in fileNameSR and "presel" in category: #DS exclude presel
                #continue
                pass
            if sampleOld != sample: 
                sampleOld = sample
                logging.info(sample)
            if (sample,category,variable) in corrNominal:
                if syst == systDown:
                    pass
                elif syst == systUp:
                    histo.Scale(corrUp[(sample,category,variable)])
                else:
                    intePre = histo.Integral()
                    histo.Scale(corrNominal[(sample,category,variable)])
                    if len(words)==3 and "mem" in histo.GetName():
                        logging.info("Scaling %s with factor %s (Pre %s | Post %s)", histo.GetName(), corrNominal[(sample,category,variable)], intePre, histo.Integral())
            histo.Write()
        newFile.Close()
        file_.Close()
        print
        logging.info("Done")
        print
        ROOT.gROOT.GetListOfFiles().Remove(newFile)
        ROOT.gROOT.GetListOfFiles().Remove(file_)

doFiles(fileNameSR, fileNameVR)
if not doPreSel:
    doFiles(fileNameCR, fileNameCR2)
#doFiles(fileNamePresel, fileNamePresel)

#CMS_ttH_CSVhfstats1Up
#CMS_ttH_CSVhfstats1Down


#stop__fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttH_CSVjesPileUpPtEC1Down

#set(['ttz', 'ttH_hbb', 'stop', 'ttbarPlusB', 'ttw', 'ttH_nonhbb', 'stop', 'ttbarPlusCCbar', 'wjets', 'ttbarPlus2B', 'ttbarOther', 'qcd', 'ttbarPlusBBbar', 'zjets', 'data'])
#set(['fh_j7_t4', 'fh_j7_t3', 'fh_j8_t3', 'fh_j8_t4', 'fh_j9_t4', 'fh_j9_t3'])
#set(['mem_FH_3w2h2t_p', 'jetsByPt_0_pt', 'mem_FH_4w2h2t_p', 'ht', 'mem_FH_4w2h1t_p'])
