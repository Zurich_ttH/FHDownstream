#!/bin/bash

TAG2016="2016_v5_v1_VR"
TAG2017="2017_v5_v1_VR"
TAG2018="2018_v5_v1_VR"

# REBININFO7="../data/templateBinning/rebin_60_to_20bins.json"
# REBININFO8="../data/templateBinning/rebin_60_to_20bins.json"
# REBININFO9="../data/templateBinning/rebin_60_to_20bins.json"
# POSTFIX="rebinned_even"


# REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins.json"
# REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins.json"
# REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins.json"
# POSTFIX="rebinned"

REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json"
#REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner_forVR.json"
REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json"
REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json"
POSTFIX="rebinnedv3p1"

# REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner_lessleft.json"
# REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner_lessleft.json"
# REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner_lessleft.json"
# POSTFIX="rebinnedv6"



python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j8_t4 --rebinInfo  ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j8_t4 --rebinInfo  ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2018 --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1

hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTMerged_${POSTFIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTMerged_*DNN_Node0*.root 
rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTMerged_*DNN_Node0*.root 

#hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTInc_${POSTFIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTInc_*DNN_Node0*.root 
#rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_${TAG2018}_TTInc_*DNN_Node0*.root 


python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j8_t4 --rebinInfo  ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j8_t4 --rebinInfo  ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2017 --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1

hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTMerged_${POSTFIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTMerged_*DNN_Node0*.root 
rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTMerged_*DNN_Node0*.root 

#hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTInc_${POSTFIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTInc_*DNN_Node0*.root 
#rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_${TAG2017}_TTInc_*DNN_Node0*.root 


python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j8_t4 --rebinInfo  ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j8_t4 --rebinInfo  ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
#python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/2016 --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1

hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTMerged_${POSTFIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTMerged_*DNN_Node0*.root 
rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTMerged_*DNN_Node0*.root

#hadd /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTInc_${POSTFIX}.root /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTInc_*DNN_Node0*.root 
#rm -v /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_${TAG2016}_TTInc_*DNN_Node0*.root 
