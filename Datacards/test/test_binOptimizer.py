# unittest sucks ----  <3 Pytest
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import unittest
import logging
import ROOT 
import math 
logger = logging.getLogger()
logger.level = logging.DEBUG
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)

from TTH.Plotting.Datacards.AnalysisSpecificationClasses import isclose

from binOptimization import getMergedBinsStep1, applyBinning, getMergedBinsStep2, getMergedBinsStep3, getRebinnedHisto, resetBinning, getTotalBinning

class Step1TestCase(unittest.TestCase):
    def test_getMergedBinsStep1_1(self):
        binning = [[0,1], [1,2], [2,3], [3,4], [4,5], [5,6], [6,7], [7,8]]
        content = [0, 0, 1, 3, 5, 2, 0, 0]
        exceptation = [[0,3], [3,4], [4,5], [5,8]]

        self.assertEqual(exceptation, getMergedBinsStep1(binning, content, 0))

    def test_getMergedBinsStep1_1_threshold(self):
        binning = [[0,1], [1,2], [2,3], [3,4], [4,5], [5,6], [6,7], [7,8]]
        content = [0, 0, 1, 3, 5, 2, 0, 0]
        exceptation = [[0,4], [4,5], [5,8]]

        self.assertEqual(exceptation, getMergedBinsStep1(binning, content, 1))

    def test_getMergedBinsStep1_2(self):
        binning = [[0,1], [1,2], [2,3], [3,4], [4,5], [5,6], [6,7], [7,8]]
        content = [0, 0, 1, 3, 5, 2, 0, 1]
        exceptation = [[0,3], [3,4], [4,5], [5,6], [6,8]]

        self.assertEqual(exceptation, getMergedBinsStep1(binning, content, 0))

    def test_getMergedBinsStep1_3(self):
        binning = [[0,1], [1,2], [2,3], [3,4], [4,5], [5,6], [6,7], [7,8]]
        content = [0, 1, 0, 3, 5, 2, 0, 0]
        exceptation = [[0,2], [2,4], [4,5], [5,8]]

        self.assertEqual(exceptation, getMergedBinsStep1(binning, content, 0))

    def test_applyBinning(self):
        binning2Apply = [[0,3], [3,4], [4,5], [5,8]]
        background = [0, 0, 1, 3, 5, 2, 0, 0]
        expectedBackground = [1, 3, 5, 2]

        retContents = applyBinning(binning2Apply, background)

        self.assertEqual(expectedBackground, retContents)

    def test_getMergedBinsStep2_left(self):
        binning = [[0,1], [1,2], [2,3]]
        background = [10, 10, 10]
        # sqrt(0.1) / 10 = 3%
        # sqrt(0.05) / 10 = 2.2%
        # sqrt(0.01) / 10 = 1%
        # sqrt(0.005) / 10 = 0.7%
        # sqrt(0.15) / 20 = 1.9%
        sumw2 = [0.1, 0.05, 0.005]
        signal = [1,1,1] # Does not matter in this case
        
        self.assertEqual([[0,2], [2,3]], getMergedBinsStep2(binning, sumw2, background, signal, 0.025))

    def test_getMergedBinsStep2_right(self):
        binning = [[0,1], [1,2], [2,3], [3,4]]
        background = [10, 10, 10, 10]
        sumw2 = [0.005, 0.05, 0.05, 0.1]
        signal = [1,1,1,1] # Does not matter in this case
        
        self.assertEqual([[0,1],[1,2],[2,4]], getMergedBinsStep2(binning, sumw2, background, signal, 0.025))

    def test_getMergedBinsStep2_bySoverB_1(self):
        binning = [[0,1], [1,2], [2,3]]
        background = [10, 10, 10]
        sumw2 = [0.005, 0.1, 0.005]
        signal = [1,4,5]
        
        self.assertEqual([[0,1],[1,3]], getMergedBinsStep2(binning, sumw2, background, signal, 0.025))

    def test_getMergedBinsStep2_allGood(self):
        binning = [[0,1], [1,2], [2,3]]
        background = [10, 10, 10]
        sumw2 = [0.005, 0.005, 0.005]
        signal = [1,4,5]
        
        self.assertEqual([[0,1],[1,2],[2,3]], getMergedBinsStep2(binning, sumw2, background, signal, 0.025))
        
    def test_getMergedBinsStep2_bySoverB_2(self):
        binning = [[0,1], [1,2], [2,3], [3,4]]
        background = [10, 10, 10, 10]
        sumw2 = [0.005, 0.1, 0.1, 0.005]
        signal = [1,2,4,5]
        
        self.assertEqual([[0,2],[2,4]], getMergedBinsStep2(binning, sumw2, background, signal, 0.025))

        
    def test_getMergedBinsStep3_left(self):
        binning = [[0,1], [1,2], [2,3], [3,4]]
        background = [10, 10, 10, 10]
        signal = [1,1.01,5,8]
        self.assertEqual([[0,2], [2,3], [3,4]], getMergedBinsStep3(binning, background, signal, 0.002))

    def test_getMergedBinsStep3_left_2(self):
        binning = [[0,1], [1,2], [2,3], [3,4]]
        background = [10, 10, 10, 10]
        signal = [1,1.01,1.02,8]
        self.assertEqual([[0,3],  [3,4]], getMergedBinsStep3(binning, background, signal, 0.002))

        
    def test_getMergedBinsStep3_right(self):
        binning = [[0,1], [1,2], [2,3], [3,4]]
        background = [10, 10, 10, 10]
        signal = [8,5,1.01,1]
        self.assertEqual([[0,1], [1,2], [2,4]], getMergedBinsStep3(binning, background, signal, 0.002))


    def test_getMergedBinsStep3_regular(self):
        binning = [[0,1], [1,2], [2,3], [3,4]]
        background = [10, 10, 10, 10]
        signal = [1,2,2.01,5]
        self.assertEqual([[0,1], [1,3], [3,4]], getMergedBinsStep3(binning, background, signal, 0.002))

    def test_getRebinnedHisto(self):
        # Generate some roughtly realistic histogra
        hTest = ROOT.TH1D("hTest", "hTest", 10, 0, 1)
        hTest.Sumw2()
        binContent = [0,0,2,4,8,9,7,3,1,0]
        for ibin in range(hTest.GetNbinsX()):
            for i in range(binContent[ibin]):
                hTest.Fill(hTest.GetBinCenter(ibin+1))
        newwBinning = [[0,3],[3,4],[4,6],[6,7],[7,8],[8,10]]
        expectedContent = [2.0,4.0,17.0,7.0,3.0,1.0]
        expectedSumw2 = [2.0,4.0,17.0,7.0,3.0,1.0] # W/o scaling error is sqrt(N) so sumw2 is N
        expectedError = [math.sqrt(2),math.sqrt(4),math.sqrt(17),math.sqrt(7),math.sqrt(3),math.sqrt(1)]

        rebinnedHisto = getRebinnedHisto(hTest, newwBinning)
        
        self.assertEqual(expectedContent, list(rebinnedHisto)[1:-1])

        rebinnedSumw2 = list(rebinnedHisto.GetSumw2())[1:-1]
        rebinnedError = [rebinnedHisto.GetBinError(i+1) for i in range(len(newwBinning))]
        for i in range(len(newwBinning)):
            #print expectedSumw2[i], rebinnedSumw2[i]
            self.assertTrue(isclose(expectedSumw2[i], rebinnedSumw2[i]))
            #print expectedError[i], rebinnedError[i]
            self.assertTrue(isclose(expectedError[i], rebinnedError[i]))

    def test_getRebinnedHisto_noneEqual(self):
        # Generate some roughtly realistic histogra
        hTest = ROOT.TH1D("hTest", "hTest", 10, 0, 1)
        hTest.Sumw2()
        binContent = [0,0,2,4,8,9,7,3,1,0]
        for ibin in range(hTest.GetNbinsX()):
            for i in range(binContent[ibin]):
                hTest.Fill(hTest.GetBinCenter(ibin+1))
        newwBinning = [[0,3],[3,4],[4,6],[6,7],[7,8],[8,10]]
        expectedContent = [2.0,4.0,17.0,7.0,3.0,1.0]
        expectedSumw2 = [2.0,4.0,17.0,7.0,3.0,1.0] # W/o scaling error is sqrt(N) so sumw2 is N
        expectedError = [math.sqrt(2),math.sqrt(4),math.sqrt(17),math.sqrt(7),math.sqrt(3),math.sqrt(1)]

        rebinnedHisto = getRebinnedHisto(hTest, newwBinning, useEqualSizedBins=False)
        
        self.assertEqual(expectedContent, list(rebinnedHisto)[1:-1])

        rebinnedSumw2 = list(rebinnedHisto.GetSumw2())[1:-1]
        rebinnedError = [rebinnedHisto.GetBinError(i+1) for i in range(len(newwBinning))]
        for i in range(len(newwBinning)):
            #print expectedSumw2[i], rebinnedSumw2[i]
            self.assertTrue(isclose(expectedSumw2[i], rebinnedSumw2[i]))
            #print expectedError[i], rebinnedError[i]
            self.assertTrue(isclose(expectedError[i], rebinnedError[i]))

    def test_mergeRebinnings(self):
        initial = [[0,1], [1,2], [2,3], [3,4], [4,5], [5,6]]
        processed_once = [[0, 2], [2,3], [3,4], [4,5], [5,6]]
        processed_once_reset = [[0,1], [1,2], [2,3], [3,4], [4,5]]
        processed_twice = [[0,2], [2,3], [3,5]]
        processed_twice_reset = [[0,1], [1,2], [2,3]]
        processed_thrice = [[0,1], [1,3]]
        mergedBinning = [[0,3], [3,6]]

        self.assertEqual(processed_once_reset, resetBinning(processed_once))
        self.assertEqual(processed_twice_reset, resetBinning(processed_twice))
        self.assertEqual(mergedBinning, getTotalBinning([initial, processed_once, processed_twice], processed_thrice))

        
if __name__ == '__main__':
    unittest.main()

