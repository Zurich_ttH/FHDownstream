import ROOT
import logging

import sys
import os

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging, makeCanvasOfHistos, saveCanvasListAsPDF, moveOverUnderFlow
from getYield import makeTable

def getYieldCounting(inFile, folder="shapes_prefit", category="fh_presel_counting"):
    rFile = ROOT.TFile.Open(inFile)

    processes = []
    for process in rFile.GetDirectory(folder).GetDirectory(category).GetListOfKeys():
        processes.append(process.GetName())

    logging.info(processes)
    
    yields, errors = {}, {}
    for process in processes:
        if process == "data":
            x, y = ROOT.Double(0), ROOT.Double(0)
            rFile.Get(folder+"/"+category+"/"+process).GetPoint(0, x, y)
            yields[process] = y
            errors[process] = rFile.Get(folder+"/"+category+"/"+process).GetErrorY(0)
        else:
            yields[process] = rFile.Get(folder+"/"+category+"/"+process).GetBinContent(1)
            errors[process] = rFile.Get(folder+"/"+category+"/"+process).GetBinError(1)

    logging.debug("Yields : %s", yields)
    logging.debug("Errors : %s", errors)
            
    return yields, errors

    
def main(infiles, names):
    assert len(infiles) == len(names)

    order = ["qcd", "ttbb", "ttcc", "ttlf", "singlet", "ttV", "V+Jets", "diboson", "bkg", "ttH", "tHq","tHW", "data"]
    merge = {"V+Jets" : ["wjets", "zjets"],
             "ttV" : ['ttbarW', 'ttbarZ'],
             "ttH" : ['ttH_hbb', 'ttH_nonhbb'],
             "bkg" : ['diboson', 'qcd', 'singlet', 'ttbarW', 'ttbarZ', 'ttbb', 'ttcc', 'ttlf', 'wjets', 'zjets'] }


    yields, errors = {}, {}
    for i, file_ in enumerate(infiles):
        yields[names[i]], errors[names[i]] = getYieldCounting(file_)
        for proc in order:
            if proc not in yields[names[i]]:
                yields[names[i]][proc] = sum([yields[names[i]][p] for p in merge[proc]])
                errors[names[i]][proc] = sum([errors[names[i]][p] for p in merge[proc]])


    makeTable(yields, errors, names, order)
    
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        type = str,
        nargs = "+",
        required=True,
        help = "List of Input files",
    )
    argumentparser.add_argument(
        "--names",
        action = "store",
        type = str,
        nargs = "+",
        required=True,
        help = "List of names per inputs",
    )
    args = argumentparser.parse_args()
    ##############################################################################################################
    
    initLogging(args.loglevel, funcLen = 21)

    main(args.inputs, args.names)
    
