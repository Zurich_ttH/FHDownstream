import sys
import os
import logging
import ROOT
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from copy import deepcopy


nonSTXSSel = lambda sysSection : ("ISR" in sysSection or "FSR" in sysSection) and ("ttHUp" in sysSection or "ttHDown" in sysSection)
STXSSel = lambda sysSection : ("ISR" in sysSection or "FSR" in sysSection) and ("lowpt" in sysSection or "highpt" in sysSection)



def makeVariationsRadiation(file2016, file2017, file2018, ttHProc="ttH", STXS=False):
    
    rfile2016 = ROOT.TFile.Open(file2016)

    allKeys = [key.GetName() for key in rfile2016.GetListOfKeys()]

    allProcs = []
    allVars = []
    allCats = []
    radiatioNSysts = []
    for key in allKeys:
        elem = key.split("__")
        if elem[0] not in allProcs:
            allProcs.append(elem[0])
        if elem[1] not in allCats:
            allCats.append(elem[1])
        if elem[2] not in allVars:
            allVars.append(elem[2])
        if len(elem) > 3:
            isRelevantSys = False
            if STXS and STXSSel(elem[3]):
                isRelevantSys = True
            if not STXS and nonSTXSSel(elem[3]):
                isRelevantSys = True
            if isRelevantSys:
                if elem[3] not in radiatioNSysts:
                    radiatioNSysts.append(elem[3])
    logging.info("Found processes: %s", allProcs)
    logging.info("Found variables: %s", allVars)
    logging.info("Found categories: %s", allCats)
    
    relevantProcs = [key for key in allProcs if key.startswith(ttHProc)]
    logging.info("Found relevant processes: %s", relevantProcs)

    logging.info("Radiation syst names: %s", radiatioNSysts)

    histos2016 = {}
    for cat in allCats:
        for proc in relevantProcs:
            for var in allVars:
                histos2016["{}__{}__{}".format(proc, cat, var)] = deepcopy(rfile2016.Get("{}__{}__{}".format(proc, cat, var)))
                for syst in radiatioNSysts:
                    histos2016["{}__{}__{}__{}".format(proc, cat, var, syst)] = deepcopy(rfile2016.Get("{}__{}__{}__{}".format(proc, cat, var, syst)))

    rfile2016.Close()

    rfile2018 = ROOT.TFile.Open(file2018)
    histos2018 = {}
    for cat in allCats:
        for proc in relevantProcs:
            for var in allVars:
                histos2018["{}__{}__{}".format(proc, cat, var)] = deepcopy(rfile2018.Get("{}__{}__{}".format(proc, cat, var)))
                for syst in radiatioNSysts:
                    histos2018["{}__{}__{}__{}".format(proc, cat, var, syst)] = deepcopy(rfile2018.Get("{}__{}__{}__{}".format(proc, cat, var, syst)))

    rfile2018.Close()


    rfile2017 = ROOT.TFile.Open(file2017)
    variations = []
    for cat in allCats:
        for proc in relevantProcs:
            for var in allVars:
                nomName = "{}__{}__{}".format(proc, cat, var)
                nominalHisto = rfile2017.Get(nomName)
                for syst in radiatioNSysts:
                    systName = "{}__{}__{}__{}".format(proc, cat, var, syst)
                    hRelative2016 = histos2016[systName].Clone(histos2016[systName].GetName()+"_rel2016")
                    hRelative2018 = histos2018[systName].Clone(histos2018[systName].GetName()+"_rel2018")

                    hRelative2016.Divide(histos2016[nomName])
                    hRelative2018.Divide(histos2018[nomName])


                    if "ISR" in systName:
                        sysNameOut = systName.replace("ISR", "ISRTransplanted")
                    if "FSR" in systName:
                        sysNameOut = systName.replace("FSR", "FSRTransplanted")

                    hRelative2017 = hRelative2018.Clone(sysNameOut)
                    hRelative2017.Add(hRelative2016)
                    hRelative2017.Scale(0.5)
                    
                    hRelative2017.Multiply(nominalHisto)


                    variations.append(deepcopy(hRelative2017))
                    
                    
    rfile2017.Close()

    outFile = ROOT.TFile(file2017.replace(".root", "_TransplantedRadiation.root"), "RECREATE")
    outFile.cd()
    
    for h in variations:
        logging.info("Writing: %s", h.GetName())
        h.Write()

    outFile.Close()
    
    
if __name__ == "__main__":
    initLogging(20, funcLen = 21)
    
    file2016 = sys.argv[1]
    file2017 = sys.argv[2]
    file2018 = sys.argv[3]
    ttHProc = "ttH" # ttH or TTH_PHT
    
    makeVariationsRadiation(file2016, file2017, file2018, ttHProc)
