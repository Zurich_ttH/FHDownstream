import sys, os
import json
import logging
import time

from copy import deepcopy 

import ROOT

# Stuff from FHDowstram/Plotting
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/"))
from classes.PlotHelpers import initLogging
# Stuff from FHDowstram/Datacards
from binOptimization import getRebinnedHisto

def generateOptimizedTemplates(inputFile, rebinInfo, folder, tag, category, variable, overwriteEdges):
    """ Function for generating the file containing the rebinned templates """
    t0 = time.time()
    
    rFile = ROOT.TFile.Open(inputFile)

    allTemplates = []

    tdiff = time.time()
    ikey = 0
    for key in rFile.GetListOfKeys():
        keyName = key.GetName()
        if not (category in keyName and variable in keyName):
            continue
        if ikey%500 == 0:
            logging.info("Copying key {0} | Total time: {1:8f} | Diff time {2:8f}".format(ikey, time.time()-t0,time.time()-tdiff))
            tdiff = time.time()
            
        allTemplates.append(
           deepcopy(rFile.Get(keyName).Clone(keyName+"___ORIG___")) # Usual deepcopy usage to trick root
        )
        ikey += 1
    
    logging.info("Got %s histograms to rebin ", len(allTemplates))

    rFile.Close()

    newBinning = None
    logging.info("Loading %s", rebinInfo)
    with open(rebinInfo, "r") as i:
        rebinInfoJSON = json.load(i)
        newBinning = rebinInfoJSON["binning_final"]


    if overwriteEdges is not None:
        useEqualSizedBins = False
    else:
        useEqualSizedBins = True
        
    inputFileName = inputFile.split("/")[-1].replace(".root","")
    outFileName = inputFileName+"_"+variable+"_"+category+("" if tag == "" else "_"+tag)+".root"
    logging.info("Will use **%s** as output file name", outFileName)

    outFileName = folder+"/"+outFileName
    outFile = ROOT.TFile(outFileName, "RECREATE")
    outFile.cd()

    logging.debug("Will apply binning: %s", newBinning)
    tdiff = time.time()
    iTemplate = 0
    for template in allTemplates:
        if iTemplate%500 == 0:
            logging.info("Rebinning template {0} | Total time: {1:8f} | Diff time {2:8f}".format(iTemplate, time.time()-t0,time.time()-tdiff))
            tdiff = time.time()
        logging.debug("Processing histo: %s", template)
        tmpHisto = getRebinnedHisto(template,
                                    newBinning,
                                    useEqualSizedBins=useEqualSizedBins,
                                    overwriteEdges=overwriteEdges)
        tmpHisto.SetName(tmpHisto.GetName().replace("___ORIG___", "").replace("___rebinned___", ""))
        tmpHisto.SetTitle(tmpHisto.GetName())
        tmpHisto.Write()

        iTemplate += 1
        
    outFile.Close()

    with open(folder+"/genOptimizedTemplates.out", "w") as o:
        o.write(folder+"/"+outFileName)

    return outFileName
    
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description='Description')
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--input",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--rebinInfo",
        action = "store",
        help = "JSON file containing optimized binning for binOptimization.py",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outFolder",
        action = "store",
        help = "Location for the output file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--cat",
        action = "store",
        help = "Pass category to process",
        type = str,
        required = True
    )    
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Tag for the output file",
        type = str,
        default = ""
    )
    argumentparser.add_argument(
        "--var",
        action = "store",
        help = "Variable to process",
        type = str,
        default="DNN_Node0"
    )
    argumentparser.add_argument(
        "--binEdgeLow",
        action = "store",
        help = "Lower bound of the binning",
        type = float,
        default=None
    )
    argumentparser.add_argument(
        "--binEdgeHigh",
        action = "store",
        help = "Upper bound of the binning",
        type = float,
        default=None
    )
    
    args = argumentparser.parse_args()

    initLogging(args.logging, funcLen = 25)


    if args.binEdgeLow is None and args.binEdgeHigh is None:
        overwriteEdges = None
    elif args.binEdgeLow is not None and args.binEdgeHigh is not None:
        overwriteEdges = (args.binEdgeLow, args.binEdgeHigh)
    else:
        overwriteEdges = None
        logging.warning("Pass --binEdgeLow and --binEdgeHigh. Will ignore this")
        
    generateOptimizedTemplates(args.input, args.rebinInfo, args.outFolder, args.tag, args.cat, args.var, overwriteEdges)
