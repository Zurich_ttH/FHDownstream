from __future__ import print_function, division

import os,sys 
import ROOT
import logging
import json
import copy

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/"))
from classes.PlotHelpers import initLogging

from binOptimization import calcAsimovSignificance, makeStackPlot, getTemplateNames, getFinalTemplatesSR, getFinalTemplateddQCD, applyBinning, getMergedBinsStep1, resetBinning, getTotalBinning

def binOptimizationPlatAndClac(fileName, folder, tag, category, variable, rebinFactor=None, thresholdContent=None,
                               jsonFile=None, forSTXS=False, saveBinning=False, overWriteFile=False):
    rFile = ROOT.TFile.Open(fileName)

    signalTemplates, backgroundTemplates, CRTemplates = getTemplateNames(rFile, category, variable, forSTXS)

    background = getFinalTemplatesSR(rFile, backgroundTemplates, "background")
    ddQCD = getFinalTemplateddQCD(rFile, CRTemplates)
    totalBackground = background.Clone("totalBackground")
    totalBackground.Add(ddQCD)
    signal = getFinalTemplatesSR(rFile, signalTemplates, "signal")

        
    logging.debug("Background %s; Integral = %s", background, background.Integral())
    logging.debug("ddQCD %s; Integral = %s", ddQCD, ddQCD.Integral())
    logging.debug("totalBackground %s; Integral = %s", totalBackground, totalBackground.Integral())
    logging.debug("Signal %s; Integral = %s", signal, signal.Integral())

    
    #Let the Merging begin
    initBinning = [[x, x+1] for x in range(totalBackground.GetNbinsX())]
    background_Content = list(totalBackground)[1:-1]
    signal_Content = list(signal)[1:-1]
    background_SumW2 = list(totalBackground.GetSumw2())[1:-1]

    newBinning = None
    if rebinFactor is not None:
        newBinning = []
        for i in xrange(0, len(initBinning), rebinFactor):
            binGroup = initBinning[i:i+rebinFactor]
            if len(binGroup) != rebinFactor:
                raise RuntimeError("Rebin factor does not lead to even binning")
            
            newBinning.append([binGroup[0][0],binGroup[-1][1]])

    if jsonFile is not None:
        with open(jsonFile,"r") as j:
            data = json.load(j)
            newBinning = data["binning_final"]
            
    logging.debug("New binning: %s",newBinning)
    logging.info("Old binning had %s bins. New binning has %s", len(initBinning), len(newBinning))

    stepsBinning = []
    stepsBinning.append(copy.copy(initBinning))
    
    background_final = applyBinning(newBinning, background_Content)
    signal_final = applyBinning(newBinning, signal_Content)
    # print(signal_final)
    if thresholdContent is not None:
        stepsBinning.append(copy.copy(newBinning))
        newBinning_cleaned = resetBinning(newBinning)
        logging.info("Got a minimum content threshold. Will apply: %s", thresholdContent)
        newBinning = getMergedBinsStep1(newBinning_cleaned, background_final, thresholdContent)
        logging.info("Binning after min. content merge: %s", newBinning)
        background_final = applyBinning(newBinning, background_final)
        signal_final = applyBinning(newBinning, signal_final)
        newBinning_cleaned = resetBinning(newBinning)
        print(signal_final)
    
    asimov_sig = calcAsimovSignificance(signal_final, background_final)
    logging.info("Asimov significance for final binning: %s", asimov_sig)

    # print(stepsBinning)
    # print(newBinning)
    # print(getTotalBinning(stepsBinning,newBinning))
    makeStackPlot(rFile, getTotalBinning(stepsBinning,newBinning),
                  signal, background, totalBackground, ddQCD,
                  tag, folder,
                  sigLabelText = "Asimov sig. : {:.4f}".format(asimov_sig))

    if saveBinning:
        fullOutName = folder+"/RebinInfo_"+tag+".json"
        if os.path.isfile(fullOutName) and not overWriteFile:
            logging.error("There is already a rebin info file with that name. Set --overWriteFile if you don't care!")
        else:
            logging.info("Saving : %s", fullOutName)
            with open(fullOutName, "w") as o:
                json.dump({"binning_final" : getTotalBinning(stepsBinning,newBinning) }, o, separators=(',', ': '))

            
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description='Description')
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--input",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--cat",
        action = "store",
        help = "Pass category to process",
        type = str,
        required = True
    )    
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Tag for output file",
        type = str,
        required = True
    )    
    argumentparser.add_argument(
        "--var",
        action = "store",
        help = "Variable to process",
        type = str,
        default="DNN_Node0"
    )
    argumentparser.add_argument(
        "--folder",
        action = "store",
        help = "Variable to process",
        type = str,
        default="out_binOptimization"
    )
    argumentparser.add_argument(
        "--rebinFactor",
        action = "store",
        help = "Factor for rebining",
        type = int,
        default=None
    )
    argumentparser.add_argument(
        "--thresholdContent",
        action = "store",
        help = "Content threshold. If noting is set this will have no effect",
        type = float,
        default=None
    )
    argumentparser.add_argument(
        "--jsonFile",
        action = "store",
        help = "JSONFile from binOptimizatioon.py",
        type = str,
        default=None
    )
    argumentparser.add_argument(
        "--forSTXS",
        action = "store_true",
        help = "Pass if processing STXS samples"
    )
    argumentparser.add_argument(
        "--saveBinning",
        action = "store_true",
        help = "Pass if processing binning json should be saved"
    )
    argumentparser.add_argument(
        "--overWriteFile",
        action = "store_true",
        help = "Pass if you want to overwrite the file with the binning. Only does somehting if --saveBinning is also passed"
    )
    args = argumentparser.parse_args()

    initLogging(args.logging, funcLen = 25)

    if args.rebinFactor is None and args.jsonFile is None:
        raise RuntimeError("Pass eitehr rebinfactor or jsonFile")
    if  args.rebinFactor is not None and args.jsonFile is not None:
        raise RuntimeError("Pass only one of rebinfactor and jsonFile")
    
    binOptimizationPlatAndClac(args.input,
                               args.folder,
                               args.tag,
                               args.cat,
                               args.var,
                               args.rebinFactor,
                               args.thresholdContent,
                               args.jsonFile,
                               args.forSTXS,
                               args.saveBinning,
                               args.overWriteFile)
