import sys
import os
import logging
from copy import deepcopy
import ROOT

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging


def rescaleTemapltestoNewLumi(inFile, current_lumi, new_lumi):
    logging.info("Opening file: %s", inFile)
    rFile = ROOT.TFile.Open(inFile)

    logging.info("Scaling:")
    logging.info("  Current lumi: %s", current_lumi)
    logging.info("      New lumi: %s", new_lumi)

    SF = new_lumi / current_lumi

    logging.info("            SF: %s", SF)

    scaled_histos = []
    allKeys = [key.GetName() for key in rFile.GetListOfKeys()]
    
    for iTemplate, template in enumerate(allKeys):
        if iTemplate%500 == 0:
            logging.info("Rebinning template {0:10d} / {1}".format(iTemplate, len(allKeys)))

        h = rFile.Get(template)

        if not "data" in template:            
            logging.debug("Scaling template: %s", template)    
            h.Scale(SF)
        else:
            logging.info("Leaving %s alone", template)    
            
        scaled_histos.append(
            deepcopy(h)
        )
            

    outFile = inFile.replace(".root","_scaledto{}.root".format(str(new_lumi).replace(".","p")))
    logging.info("Will use output file: %s", outFile)
    rOut = ROOT.TFile(outFile, "RECREATE")
    rOut.cd()
    for h in scaled_histos:
        h.Write()
    
    rOut.Close()
        
if __name__ == "__main__":
    infile = sys.argv[1]
    current_lumi = float(sys.argv[2])
    new_lumi = float(sys.argv[3])
    
    initLogging(20, funcLen = 21)

    rescaleTemapltestoNewLumi(infile, current_lumi,new_lumi)
    
