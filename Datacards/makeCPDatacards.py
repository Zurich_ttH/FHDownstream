from __future__ import print_function

import sys
import os
import datetime
from glob import glob

from TTH.MEAnalysis.ParHadd import par_hadd

def mergeCTCVCPGCOut(basepath, outpath, skipMerge=False):

    print("Basepath : %s"%basepath)
    print("outpath : %s"%outpath)
    
    samples = os.listdir(basepath)
    prefixes = []
    
    for sample in samples:
        for weight in os.listdir(basepath+"/"+sample):
            filePrefix = "P"+str(weight.split("_")[-1])
            if filePrefix not in prefixes:
                prefixes.append(filePrefix)
            files = glob(basepath+"/"+sample+"/"+weight+"/*.root")
            if not skipMerge:
                par_hadd(outpath + "{}_{}.root".format(filePrefix, sample),
                         files,
                         250, 10, 3)
            
    return samples, prefixes

def generationProcess(prefix, point, outputPath, datacardTag, datacardConfig, skipMerge, postfix):
    if not skipMerge:
        hadd_command = "hadd -f {0}/{1}/{2}.root {0}/{3}_{4}_rebinned{5}.root".format(outputPath, # 0 
                                                                                      datacardTag, # 1
                                                                                      point, # 2
                                                                                      prefix, # 3
                                                                                      point, # 4
                                                                                      postfix, # 5
        )
        print(hadd_command)
        os.system(hadd_command)

    #datacardCommand =  "python {}/src/TTH/FHDownstream/Datacards/makeDatacards.py".format(os.environ["CMSSW_BASE"])
    datacardCommand =  "python makeDatacards.py"
    datacardCommand += " --config {0} --inputFile {1}/{2}/{3}.root".format(datacardConfig,
                                                                           outputPath,
                                                                           datacardTag,
                                                                           point)
    datacardCommand += " --outputPath {}/{}/cards".format(outputPath, datacardTag)
    datacardCommand += " --tag {} --force".format( point )
    print(datacardCommand)

    os.system(datacardCommand)


if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Process the CTCVCP samples'
    )
    argumentparser.add_argument(
        "--outputPath",
        action = "store",
        help = "Output path.",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--tHdatacardConfig",
        action = "store",
        help = "Config file for the datacard",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--ttHdatacardConfig",
        action = "store",
        help = "Config file for the datacard",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--datacardTag",
        action = "store",
        help = "Tag for the datacard",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--tHPrefix",
        action = "store",
        help = "tHPrefix",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--ttHPrefix",
        action = "store",
        help = "ttHPrefix",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--cardPostfix",
        action = "store",
        help = "File postfix",
        type = str,
        default = "",
    )
    argumentparser.add_argument(
        "--skipDatacardGen",
        action = "store_true",
    )
    argumentparser.add_argument(
        "--skipMerge",
        action = "store_true",
    )
    
    args = argumentparser.parse_args()

    
    th_points = ["P1","P2","P4","P5"]
    tth_points = ["P12","P60"]

    mkdir_command = "mkdir {}/{}".format(args.outputPath, args.datacardTag)
    print(mkdir_command)
    os.system(mkdir_command)


    infoLines = ["Created on "+datetime.datetime.now().strftime("%A %d. %B %Y - %H:%M:%S")]
    infoLines += ["$CMSSW_BASE: "+str(os.environ["CMSSW_BASE"])]
    infoLines += ["Command line options:"]
    infoLines += ["python "+" ".join(sys.argv).replace("--","\n  --")]
    with open(args.outputPath+"/"+args.datacardTag+"/makeCPDatacards.txt", "w") as f:
        for line in infoLines:
            f.write(line+"\n")


    for point in th_points:
        print("TH: Will generate %s"%point)
        generationProcess(args.tHPrefix, point, args.outputPath, args.datacardTag, args.tHdatacardConfig, args.skipMerge, args.cardPostfix)

    for point in tth_points:
        print("TTH: Will generate %s"%point)
        generationProcess(args.ttHPrefix, point, args.outputPath, args.datacardTag, args.ttHdatacardConfig, args.skipMerge, args.cardPostfix)
