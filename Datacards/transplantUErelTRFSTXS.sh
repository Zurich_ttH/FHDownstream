#!/bin/bash
FOLDER=$1
FOLDERMERGED=$2
VAR=$3

REBININFO7_0="../data/templateBinning/STXS_7J_BIN0_v3p3_60Bins_innerv2.json"
REBININFO7_1="../data/templateBinning/STXS_7J_BIN1_v3p3_60Bins_inner.json"
REBININFO7_2="../data/templateBinning/STXS_7J_BIN2_v3p3_60Bins_inner.json"
REBININFO7_3="../data/templateBinning/STXS_7J_BIN3_v3p3_60Bins_inner.json"
REBININFO7_4="../data/templateBinning/STXS_7J_BIN4_v3p3_60Bins_innerv2.json"

REBININFO8_0="../data/templateBinning/STXS_8J_BIN0_v3p3_60Bins_inner.json"
REBININFO8_1="../data/templateBinning/STXS_8J_BIN1_v3p3_60Bins_inner.json"
REBININFO8_2="../data/templateBinning/STXS_8J_BIN2_v3p3_60Bins_inner.json"
REBININFO8_3="../data/templateBinning/STXS_8J_BIN3_v3p3_60Bins_inner.json"
REBININFO8_4="../data/templateBinning/STXS_8J_BIN4_v3p3_60Bins_innerv2.json"

REBININFO9_0="../data/templateBinning/STXS_9J_BIN0_v3p3_60Bins_innerv2.json"
REBININFO9_1="../data/templateBinning/STXS_9J_BIN1_v3p3_60Bins_inner.json"
REBININFO9_2="../data/templateBinning/STXS_9J_BIN2_v3p3_60Bins_inner.json"
REBININFO9_3="../data/templateBinning/STXS_9J_BIN3_v3p3_60Bins_inner.json"
REBININFO9_4="../data/templateBinning/STXS_9J_BIN4_v3p3_60Bins_innerv2.json"



python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j7_t4 --rebinInfo ${REBININFO7_0} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j7_t4 --rebinInfo ${REBININFO7_1} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j7_t4 --rebinInfo ${REBININFO7_2} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j7_t4 --rebinInfo ${REBININFO7_3} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j7_t4 --rebinInfo ${REBININFO7_4} --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j8_t4 --rebinInfo ${REBININFO8_0} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j8_t4 --rebinInfo ${REBININFO8_1} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j8_t4 --rebinInfo ${REBININFO8_2} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j8_t4 --rebinInfo ${REBININFO8_3} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j8_t4 --rebinInfo ${REBININFO8_4} --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j9_t4 --rebinInfo ${REBININFO9_0} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j9_t4 --rebinInfo ${REBININFO9_1} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j9_t4 --rebinInfo ${REBININFO9_2} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j9_t4 --rebinInfo ${REBININFO9_3} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j9_t4 --rebinInfo ${REBININFO9_4} --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j7_t4 --rebinInfo ${REBININFO7_0} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j7_t4 --rebinInfo ${REBININFO7_1} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j7_t4 --rebinInfo ${REBININFO7_2} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j7_t4 --rebinInfo ${REBININFO7_3} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j7_t4 --rebinInfo ${REBININFO7_4} --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j8_t4 --rebinInfo ${REBININFO8_0} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j8_t4 --rebinInfo ${REBININFO8_1} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j8_t4 --rebinInfo ${REBININFO8_2} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j8_t4 --rebinInfo ${REBININFO8_3} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j8_t4 --rebinInfo ${REBININFO8_4} --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_0_j9_t4 --rebinInfo ${REBININFO9_0} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_1_j9_t4 --rebinInfo ${REBININFO9_1} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_2_j9_t4 --rebinInfo ${REBININFO9_2} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_3_j9_t4 --rebinInfo ${REBININFO9_3} --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_STXS_4_j9_t4 --rebinInfo ${REBININFO9_4} --binEdgeLow 0 --binEdgeHigh 1

hadd -f ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root 
hadd -f ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root

rm -v ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root
rm -v ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root

python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
       ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ttbarPlusBBbarMerged
