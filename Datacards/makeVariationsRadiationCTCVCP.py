import sys
import os 
from glob import glob
import logging

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from makeVariationsRadiation import makeVariationsRadiation

if __name__ == "__main__":
    initLogging(30, funcLen = 21)
    base2016 = sys.argv[1]
    base2017 = sys.argv[2]
    base2018 = sys.argv[3]

    allttHFiles = [x.split("/")[-1] for x in glob(base2017+"/P*ttH*.root")]

    for file_ in allttHFiles:
        logging.warning("%s / %s / %s / %s /", base2016, base2017, base2018, file_)
        file2016 = base2016+"/"+file_
        file2017 = base2017+"/"+file_
        file2018 = base2018+"/"+file_

        if not os.path.exists(file2016):
            raise IOError("File %s not found"%file2016)
        if not os.path.exists(file2017):
            raise IOError("File %s not found"%file2017)
        if not os.path.exists(file2018):
            raise IOError("File %s not found"%file2018)
        
        makeVariationsRadiation(file2016, file2017, file2018)
