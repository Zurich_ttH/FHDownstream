# Tempalte processing

1. Combined workflow
   1. Run `mergeSamplesWithSameProc.sh`. Pass folder containing merged spare per dataset and output folder for the merged files
   2. Change the `BASEFOLDER` variable in `processTTbar.sh` and `processOtherSamples.sh` to the output folder for the merged files in _Step 1._
   3. Run both scripts
   4. Skip **Step 2. Individual workflow**

2. Individual workflow
   1. Merge the files with the same processes (tt inc, ttbb, st, etc.)
   2. Calcualte PDF weights from PDF Memebr

	  - Use script `calcPDFWeightFromMembers.py`
	  - For ttH, tH, ttbar, ttbb and st files

   3. Merge ttb* processe

	  - Use script `mergeTTtemplates.py`
	  - For ttbar aand ttbb files
	  - Optional: Rename merged temples from `ttbarPlusBBbarMerged` to `ttbarPlusBBbar` with renameBBarMerged.py
	4. Split ttbar samples into the differrent processed with `splitFileIntoProcs.py` so you can choose the ones you want later.

3. hadd the separate files for the processes into one big file with all required templates

	```bash
	export FOLDER=path/to/folder/with/merged/Files
	export TAG=SOME_NAME

	hadd LegacyRun2_${TAG}_TTMerged.root ${FOLDER}/ttH_MergedPDF.root ${FOLDER}/THQ_MergedPDF.root ${FOLDER}/THW_MergedPDF.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarOther.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root ${FOLDER}/VV.root ${FOLDER}/WJets.root ${FOLDER}/ZJets.root ${FOLDER}/data.root ${FOLDER}/ttV.root ${FOLDER}/ST_MergedPDF.root


   	hadd LegacyRun2_${TAG}_TTInc.root ${FOLDER}/ttH_MergedPDF.root ${FOLDER}/THQ_MergedPDF.root ${FOLDER}/THW_MergedPDF.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarOther.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root ${FOLDER}/VV.root ${FOLDER}/WJets.root ${FOLDER}/ZJets.root ${FOLDER}/data.root ${FOLDER}/ttV.root ${FOLDER}/ST_MergedPDF.root

	```


4. Rebin templates if desired
   - Use `genOptimizedTemplates.py`. Use the previously merged template as `--input`, set the category `--cat` and the output folder with `--outFolder`. You need a json file that specifies the new binning, which is set the `--rebinInfo`.
   - For this application the file only need to contain a dict with one key called `"binning_final"` that contains a list. 
   - Each element of the list needs to be a list with two elements: The left and the right edge of the bin.
   - The first element of the first list (in the list) need to be the left edge of the histogram and the second element of the last list (in the list) needs to be the right edge of the histogram. --> Check `data/templateBinning` for examples.

	```bash
	export YEAR=
	export TAG=
	
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_j7_t4 --rebinInfo ../data/templateBinning/j7_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_j8_t4 --rebinInfo ../data/templateBinning/j8_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_j9_t4 --rebinInfo ../data/templateBinning/j9_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1

	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_j7_t4 --rebinInfo ../data/templateBinning/j7_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_j8_t4 --rebinInfo ../data/templateBinning/j8_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTInc.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_j9_t4 --rebinInfo ../data/templateBinning/j9_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
	```
	
	Or for STXS:
	```bash
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_0_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_1_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_2_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_3_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_4_j7_t4 --rebinInfo ../data/templateBinning/STXS_7J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_0_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_1_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_2_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_3_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_4_j8_t4 --rebinInfo ../data/templateBinning/STXS_8J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_0_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN0_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_1_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN1_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_2_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN2_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_3_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN3_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	python genOptimizedTemplates.py --input /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR}/LegacyRun2_${TAG}_TTMerged.root --outFolder /scratch/koschwei/ttH/sparse/LegacyRun2/${YEAR} --cat fh_STXS_4_j9_t4 --rebinInfo ../data/templateBinning/STXS_9J_BIN4_v3p3_60Bins.json --binEdgeLow 0 --binEdgeHigh 1
	```

5. **For 2017 only: ** Generate 2017 ISR/FSR tempaltes for ttH*:
   ```bash
   export TAGNOYEAR=
   
   python makeVariationsRadiation.py \
   /scratch/koschwei/ttH/sparse/LegacyRun2/2016/LegacyRun2_2016_${TAGNOYEAR}_TTMerged_rebinned.root \
   /scratch/koschwei/ttH/sparse/LegacyRun2/2017/LegacyRun2_2017_${TAGNOYEAR}_TTMerged_rebinned.root \
   /scratch/koschwei/ttH/sparse/LegacyRun2/2018/LegacyRun2_2018_${TAGNOYEAR}_TTMerged_rebinned.root \
   ttH
   ```

6. Make datacards:
   - Use script: `makeDatacards.py`
   - Requires a config. See `FHDownstream/data/datacards/`

# Variated samples

1. Run SR and CR templates (with TRF method) --> __STXS only__ : Run also all samples with TRF disabled in order to scale TRF to SR prediction (use `scaleVariatedSamplesSTXS.sh` for the scaling step)
2. Use the script `mergeVariatedSamplesWithSameProc.sh` to merge the exclusive decay samples into one. The script expect the SR and CR files to in different folders passed as first and second argument
3. Use the script `processTTbarVariated.sh` to merge the tt+B* sampels and split into different files per tt+X process.
4. Use the script `renameVariatedSamples.sh` to rename the "nominal" templates to the expected systamtic from. Will also merge the different subprocess file for the different variations into one file per subprocess.
5. Rebin the DNN like the main files (Set $FOLDER to folder with the previously preprocessed files):
   - Use `rebinDNNVariated.sh` for the main samples 
   - Use `rebinDNNVariatedSTXS.sh` for the STXS samples
   
6. For the transplantation of the UE variations form the 5FS templates to the 4FS tempaltes we need also rebinned samples with the nominal ttbb templates for the ttbb and ttIncl samples. Set the second argument (`FOLDERMERGED`) to the output of the `mergeSamplesWithSameProc.sh` of the main templates. 
   - Use `transplantUE.sh` for the main samples
   - Use `transplantUESTXS.sh` for the STXS samples
7. Hadd to UE/HDAMP tempaltes:

   ```bash
   export FOLDER=
   hadd ${FOLDER}/VariatedTTMerged.root \
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics_rebinned_TransplantedUE.root \
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics_rebinned.root \
	   ${FOLDER}/TTIncl_ttbarPlusCCbar_systamtics_rebinned.root \
	   ${FOLDER}/TTIncl_ttbarOther_systamtics_rebinned.root
   ```
   
The resulting files can be merged into the final file used for the datacard creation

## Variated samples -- Relative TRF

1. Run SR and CR templates (with TRF method). Also run the nominal samples with TRF method!
2.  Use the script `mergeVariatedSamplesWithSameProc.sh` to merge the exclusive decay samples into one. The script expect the SR and CR files to in different folders passed as first and second argument
3. Use the script `processTTbarVariated.sh` to merge the tt+B* sa
mpels and split into different files per tt+X process.
4. **If producing DNN templates** : Use `rebinForRelativeTRFTemapltes.sh` to generate the rebinned templates per tt+X process
5. Use `calcRelativeTRFTemplates.sh` to get the final HDAMP and UE templates. Pass the workdir as first argument, merged sparse files for TTMerged and TTInc as second and third argument and the variable as fourth
6. Use `transplantUErelTRF.sh` to generate the ttbb UE tempaltes.
7. Hadd to UE/HDAMP tempaltes:

   ```bash
   export FOLDER=
   export VAR=
   
   hadd ${FOLDER}/VariatedTTMerged.root \
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics_TransplantedUE.root \
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_${VAR}_relTRF_systamtics.root \
	   ${FOLDER}/TTIncl_ttbarPlusCCbar_${VAR}_relTRF_systamtics.root \
	   ${FOLDER}/TTIncl_ttbarOther_${VAR}_relTRF_systamtics.root 
   ```

## Variated samples -- Plain Rate uncertainty
1. Run all variated templates. **The nominal sample is not required**
2.  Use the script `mergeVariatedSamplesWithSameProcRegular.sh` to merge the exclusive decay samples into one. Expects the folder containing the files as first argument and an output folder (which can be the same as the input folder) as second argument.
3. Use the script `processTTbarVariated.sh` to merge the tt+B* 
4. Use the script `renameVariatedSamples.sh` to rename the "nominal" templates to the expected systamtic from. Will also merge the different subprocess file for the different variations into one file per subprocess.
5. **If producing DNN templates** : Use `rebinDNNVariated.sh` (STXS: `rebinDNNVariatedSTXS.sh`) to generate the rebinned templates per tt+X process.
6. Use `mergeVariatedSamplesRegular.sh` next. Pass the folder as arguement but check the setting (VAR and POSTFIX) in the file. **Not necessary for STXS rebinning**
7. Use `transplantUERegular.sh` next. Pass the folder, the TTInc sample and the TTMerged sample as arguments. Keep the internal setting in mind.
8. Hadd to UE/HDAMP tempaltes
   ```bash
   export FOLDER=
   export POSTFIX=
   
   hadd ${FOLDER}/VariatedTTMerged.root \ 
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics_${POSTFIX}_TransplantedUE.root \
	   ${FOLDER}/TTIncl_ttbarPlusCCbar_systamtics_${POSTFIX}.root \
	   ${FOLDER}/TTIncl_ttbarOther_systamtics_${POSTFIX}.root \
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics_${POSTFIX}.root
   ```

### For other variables:
1. Follow steps 1-4 above
2. Use `removeVariable.sh` to remove the DNN_Node0 from the file.
3. Follow steps 7-8 but change to setting in the scripts accordingly.
4. Hadd to UE/HDAMP tempaltes
   ```bash
   export FOLDER=
   export POSTFIX=
   
   hadd ${FOLDER}/VariatedTTMerged_${POSTFIX}.root \ 
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics${POSTFIX}_TransplantedUE.root \
	   ${FOLDER}/TTIncl_ttbarPlusCCbar_systamtics${POSTFIX}.root \
	   ${FOLDER}/TTIncl_ttbarOther_systamtics${POSTFIX}.root \
	   ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics${POSTFIX}.root
   ```
   
# Datacard commands:

Default `combine` commands for SR fits:

```
combine -M FitDiagnostics -m 125 --saveShapes --saveNormalizations --saveWithUncertainties --numToysForShapes 50 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 --setParameterRanges r=-10,10 --setParameters r=1 --redefineSignalPOIs r  -t -1 -n _asimov_sig1_r_${IDENTIFIER} ${WORKSPACE}
```

and for significances:
```
combine  -M Significance --signalForSignificance 0 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 --setParameterRanges r=-10,10 --setParameters r=1  --redefineSignalPOIs r -t -1 -n _asimov_significance_sig1_r_${IDENTIFIER} ${WORKSPACE}
```


For preselection fits:
```
combine -M FitDiagnostics -m 125 --saveShapes --saveNormalizations --saveWithUncertainties --numToysForShapes 50 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 --setParameterRanges r=-50,50 --setParameters r=1 --redefineSignalPOIs r -t -1 -n _asimov_sig1_r_${IDENTIFIER} ${WORKSPACE}

```

or

```
combine -M FitDiagnostics -m 125  --numToysForShapes 50 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0  --setParameterRanges r=-50,50 --setParameters r=1 --redefineSignalPOIs r  -t -1  -n _asimov_sig1_r_${IDENTIFIER} ${WORKSPACE}

```
