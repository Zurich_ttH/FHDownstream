import sys, os
import logging

import ROOT

from copy import deepcopy

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

def splitFileIntoProcs(fullFilePath, fileNamePrefix = None):
    fileName = fullFilePath.split("/")[-1]
    folderName = fullFilePath.replace(fileName, "")
    
    logging.info("Got output file prefix: %s", fileNamePrefix)
    logging.info("Will split file: %s", fileName)
    logging.info("Workfolder: %s", folderName)
    
    rFile = ROOT.TFile.Open(fullFilePath)
    
    allKeys = [x.GetName() for x in rFile.GetListOfKeys()]

    nominals = [x for x in allKeys if len(x.split("__"))==3]

    procs = []
    for template in nominals:
        procs.append(template.split("__")[0])

    procs = list(set(procs))

    logging.info("Found procs: %s", procs)
    procTempaltes = {}
    for proc in procs:
        procTempaltes[proc] = []
        keys = [x for x in allKeys if x.startswith(proc)]
        for key in keys:
            procTempaltes[proc].append(deepcopy(rFile.Get(key)))


    outfiles = []
    for proc in procs:
        if fileNamePrefix is None:
            outName = fullFilePath.replace(".root","_"+proc+".root")
        else:
            outName = "/".join(fullFilePath.split("/")[:-1])+"/"+fileNamePrefix+"_"+proc+".root"
        logging.info("Will use output file %s", outName)
        outfiles.append(outName)
        outFile = ROOT.TFile(outName, "RECREATE")
        outFile.cd()

        for template in procTempaltes[proc]:
            template.Write()

        outFile.Close()

    return " ".join(outfiles)
    
if __name__ == "__main__":
    import argparse
    ############################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        help = "Input file",
        nargs = "+",
        type = str,
        required = True
    )
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    for file_ in args.inputs:
        logging.info("Processing %s", file_)
        splitFileIntoProcs(file_, "TESTNAME")
    
