#!/bin/bash

BASEFOLDERSR=$1
OUTFOLDER=$2

mkdir -p ${OUTFOLDER}

ls ${BASEFOLDERSR}/TTbb_*hdampUP*.root

hadd -f ${OUTFOLDER}/TTbb_hdamp_up.root ${BASEFOLDERSR}/TTbb_*hdampUP*.root
hadd -f ${OUTFOLDER}/TTbb_hdamp_down.root ${BASEFOLDERSR}/TTbb_*hdampDOWN*.root

hadd -f ${OUTFOLDER}/TTIncl_hdamp_up.root ${BASEFOLDERSR}/TTTo*hdampUP*.root
hadd -f ${OUTFOLDER}/TTIncl_hdamp_down.root ${BASEFOLDERSR}/TTTo*hdampDOWN*.root
hadd -f ${OUTFOLDER}/TTIncl_tune_up.root ${BASEFOLDERSR}/TTTo*TuneCP5up*.root
hadd -f ${OUTFOLDER}/TTIncl_tune_down.root ${BASEFOLDERSR}/TTTo*TuneCP5down*.root

