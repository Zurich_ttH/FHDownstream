#!/bin/bash
MAKECARDS=false
RUNCOMBINE=true
RUNPLOTS=false
RUNDIFFNUI=false
RUNBKGFIT=false
RUNASIMOV=true

GENERALSETTINGS=" -M FitDiagnostics  -m 125 --cminDefaultMinimizerStrategy 0 --cminPreScan --saveWithUncertainties --saveNormalizations --saveShapes --saveOverallShapes"
# --saveWithUncertainties "
RSETTINGSREST=" --rMin -5 --rMax 5 "
RSETTINGS3B=" --rMin -15 --rMax 15 "
# --saveShapes --saveWithUncertainties
TOL=" --cminDefaultMinimizerTolerance 0.00001 --setRobustFitTolerance 0.001 --setCrossingTolerance 0.001"
POSTFIX="_v3p3_v1_tol0p00001_robTolp001_crossTolp001"



if [ "${MAKECARDS}" = true ]; then
  ###################################################
  combineCards.py \
      fh_STXS_0_j7_t4_DNN_Node0=fh_STXS_0_j7_t4_DNN_Node0.txt \
      fh_STXS_0_j8_t4_DNN_Node0=fh_STXS_0_j8_t4_DNN_Node0.txt \
      fh_STXS_0_j9_t4_DNN_Node0=fh_STXS_0_j9_t4_DNN_Node0.txt \
     > fh_STXS_0_t4_DNN_Node0.txt


  text2workspace.py fh_STXS_0_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_0_j7_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_0_j7_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_0_j8_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_0_j8_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_0_j9_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_0_j9_t4_DNN_Node0.txt

  ###################################################
  combineCards.py \
      fh_STXS_1_j7_t4_DNN_Node0=fh_STXS_1_j7_t4_DNN_Node0.txt \
      fh_STXS_1_j8_t4_DNN_Node0=fh_STXS_1_j8_t4_DNN_Node0.txt \
      fh_STXS_1_j9_t4_DNN_Node0=fh_STXS_1_j9_t4_DNN_Node0.txt \
     > fh_STXS_1_t4_DNN_Node0.txt


  text2workspace.py fh_STXS_1_t4_DNN_Node0.txt
  
  echo -e "text2workspace.py fh_STXS_1_j7_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_1_j7_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_1_j8_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_1_j8_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_1_j9_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_1_j9_t4_DNN_Node0.txt

  ###################################################
  combineCards.py \
      fh_STXS_2_j7_t4_DNN_Node0=fh_STXS_2_j7_t4_DNN_Node0.txt \
      fh_STXS_2_j8_t4_DNN_Node0=fh_STXS_2_j8_t4_DNN_Node0.txt \
      fh_STXS_2_j9_t4_DNN_Node0=fh_STXS_2_j9_t4_DNN_Node0.txt \
     > fh_STXS_2_t4_DNN_Node0.txt


  text2workspace.py fh_STXS_2_t4_DNN_Node0.txt
  
  echo -e "text2workspace.py fh_STXS_2_j7_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_2_j7_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_2_j8_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_2_j8_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_2_j9_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_2_j9_t4_DNN_Node0.txt

  ###################################################
  combineCards.py \
      fh_STXS_3_j7_t4_DNN_Node0=fh_STXS_3_j7_t4_DNN_Node0.txt \
      fh_STXS_3_j8_t4_DNN_Node0=fh_STXS_3_j8_t4_DNN_Node0.txt \
      fh_STXS_3_j9_t4_DNN_Node0=fh_STXS_3_j9_t4_DNN_Node0.txt \
     > fh_STXS_3_t4_DNN_Node0.txt


  text2workspace.py fh_STXS_3_t4_DNN_Node0.txt
  
  echo -e "text2workspace.py fh_STXS_3_j7_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_3_j7_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_3_j8_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_3_j8_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_3_j9_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_3_j9_t4_DNN_Node0.txt

  ###################################################
  combineCards.py \
      fh_STXS_4_j7_t4_DNN_Node0=fh_STXS_4_j7_t4_DNN_Node0.txt \
      fh_STXS_4_j8_t4_DNN_Node0=fh_STXS_4_j8_t4_DNN_Node0.txt \
      fh_STXS_4_j9_t4_DNN_Node0=fh_STXS_4_j9_t4_DNN_Node0.txt \
     > fh_STXS_4_t4_DNN_Node0.txt


  text2workspace.py fh_STXS_4_t4_DNN_Node0.txt
  
  echo -e "text2workspace.py fh_STXS_4_j7_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_4_j7_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_4_j8_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_4_j8_t4_DNN_Node0.txt
  echo -e "text2workspace.py fh_STXS_4_j9_t4_DNN_Node0.txt"
  text2workspace.py fh_STXS_4_j9_t4_DNN_Node0.txt
  
fi


echo "Finished text2Workspace"
DIFFNUI="/work/koschwei/slc7/ttH/LegacyRun2/combine/CMSSW_10_2_13/src/HiggsAnalysis/CombinedLimit/test/diffNuisances.py"
if [ "${RUNCOMBINE}" = true ]; then
    echo "Running fits"
    if [ "${RUNDATA}" = true ]; then
	echo "NOT IMPLEMENTED"
    fi

    if [ "${RUNASIMOV}" = true ]; then
	##################################################################################################################################################
	echo "=================================================== STXS 0 ==================================================="
	echo "=============================================== RUNNING ASIMOV ==============================================="
	echo "============================================= SIGNAL + BACKGROUND ============================================"
	echo "combine fh_STXS_0_j7_t4_DNN_Node0.root -n _asimov_STXS_0_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_j7_t4${POSTFIX}.out.txt"
	combine fh_STXS_0_j7_t4_DNN_Node0.root -n _asimov_STXS_0_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_j7_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_0_sig_j7_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_0_j8_t4_DNN_Node0.root -n _asimov_STXS_0_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_j8_t4${POSTFIX}.out.txt"
	combine fh_STXS_0_j8_t4_DNN_Node0.root -n _asimov_STXS_0_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_j8_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_0_sig_j8_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_0_j9_t4_DNN_Node0.root -n _asimov_STXS_0_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_j9_t4${POSTFIX}.out.txt"
	combine fh_STXS_0_j9_t4_DNN_Node0.root -n _asimov_STXS_0_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_j9_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_0_sig_j9_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_0_t4_DNN_Node0.root -n _asimov_STXS_0_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_comb${POSTFIX}.out.txt"
	combine fh_STXS_0_t4_DNN_Node0.root -n _asimov_STXS_0_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_0_sig_comb${POSTFIX}.out.txt
	cat combine_asimov_STXS_0_sig_comb${POSTFIX}.out.txt

	if [ "${RUNBKGFIT}" = true ]; then
	    echo "=============================================== BACKGROUND ONLY =============================================="
	    echo "combine fh_STXS_0_j7_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_j7_t4${POSTFIX}.out.txt"
	    combine fh_STXS_0_j7_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_j7_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_0_bkg_j7_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_0_j8_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_j8_t4${POSTFIX}.out.txt"
	    combine fh_STXS_0_j8_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_j8_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_0_bkg_j8_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_0_j9_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_j9_t4${POSTFIX}.out.txt"
	    combine fh_STXS_0_j9_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_j9_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_0_bkg_j9_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_0_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_comb${POSTFIX}.out.txt"
	    combine fh_STXS_0_t4_DNN_Node0.root -n _asimov_STXS_0_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_0_bkg_comb${POSTFIX}.out.txt
	    cat combine_asimov_STXS_0_bkg_comb${POSTFIX}.out.txt
	fi

	##################################################################################################################################################
	echo "=================================================== STXS 1 ==================================================="
	echo "=============================================== RUNNING ASIMOV ==============================================="
	echo "============================================= SIGNAL + BACKGROUND ============================================"
	echo "combine fh_STXS_1_j7_t4_DNN_Node0.root -n _asimov_STXS_1_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_j7_t4${POSTFIX}.out.txt"
	combine fh_STXS_1_j7_t4_DNN_Node0.root -n _asimov_STXS_1_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_j7_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_1_sig_j7_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_1_j8_t4_DNN_Node0.root -n _asimov_STXS_1_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_j8_t4${POSTFIX}.out.txt"
	combine fh_STXS_1_j8_t4_DNN_Node0.root -n _asimov_STXS_1_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_j8_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_1_sig_j8_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_1_j9_t4_DNN_Node0.root -n _asimov_STXS_1_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_j9_t4${POSTFIX}.out.txt"
	combine fh_STXS_1_j9_t4_DNN_Node0.root -n _asimov_STXS_1_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_j9_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_1_sig_j9_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_1_t4_DNN_Node0.root -n _asimov_STXS_1_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_comb${POSTFIX}.out.txt"
	combine fh_STXS_1_t4_DNN_Node0.root -n _asimov_STXS_1_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_1_sig_comb${POSTFIX}.out.txt
	cat combine_asimov_STXS_1_sig_comb${POSTFIX}.out.txt

	if [ "${RUNBKGFIT}" = true ]; then
	    echo "=============================================== BACKGROUND ONLY =============================================="
	    echo "combine fh_STXS_1_j7_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_j7_t4${POSTFIX}.out.txt"
	    combine fh_STXS_1_j7_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_j7_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_1_bkg_j7_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_1_j8_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_j8_t4${POSTFIX}.out.txt"
	    combine fh_STXS_1_j8_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_j8_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_1_bkg_j8_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_1_j9_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_j9_t4${POSTFIX}.out.txt"
	    combine fh_STXS_1_j9_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_j9_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_1_bkg_j9_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_1_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_comb${POSTFIX}.out.txt"
	    combine fh_STXS_1_t4_DNN_Node0.root -n _asimov_STXS_1_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_1_bkg_comb${POSTFIX}.out.txt
	    cat combine_asimov_STXS_1_bkg_comb${POSTFIX}.out.txt
	fi

	##################################################################################################################################################
	echo "=================================================== STXS 2 ==================================================="
	echo "=============================================== RUNNING ASIMOV ==============================================="
	echo "============================================= SIGNAL + BACKGROUND ============================================"
	echo "combine fh_STXS_2_j7_t4_DNN_Node0.root -n _asimov_STXS_2_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_j7_t4${POSTFIX}.out.txt"
	combine fh_STXS_2_j7_t4_DNN_Node0.root -n _asimov_STXS_2_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_j7_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_2_sig_j7_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_2_j8_t4_DNN_Node0.root -n _asimov_STXS_2_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_j8_t4${POSTFIX}.out.txt"
	combine fh_STXS_2_j8_t4_DNN_Node0.root -n _asimov_STXS_2_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_j8_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_2_sig_j8_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_2_j9_t4_DNN_Node0.root -n _asimov_STXS_2_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_j9_t4${POSTFIX}.out.txt"
	combine fh_STXS_2_j9_t4_DNN_Node0.root -n _asimov_STXS_2_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_j9_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_2_sig_j9_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_2_t4_DNN_Node0.root -n _asimov_STXS_2_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_comb${POSTFIX}.out.txt"
	combine fh_STXS_2_t4_DNN_Node0.root -n _asimov_STXS_2_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_2_sig_comb${POSTFIX}.out.txt
	cat combine_asimov_STXS_2_sig_comb${POSTFIX}.out.txt

	if [ "${RUNBKGFIT}" = true ]; then
	    echo "=============================================== BACKGROUND ONLY =============================================="
	    echo "combine fh_STXS_2_j7_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_j7_t4${POSTFIX}.out.txt"
	    combine fh_STXS_2_j7_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_j7_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_2_bkg_j7_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_2_j8_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_j8_t4${POSTFIX}.out.txt"
	    combine fh_STXS_2_j8_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_j8_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_2_bkg_j8_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_2_j9_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_j9_t4${POSTFIX}.out.txt"
	    combine fh_STXS_2_j9_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_j9_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_2_bkg_j9_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_2_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_comb${POSTFIX}.out.txt"
	    combine fh_STXS_2_t4_DNN_Node0.root -n _asimov_STXS_2_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_2_bkg_comb${POSTFIX}.out.txt
	    cat combine_asimov_STXS_2_bkg_comb${POSTFIX}.out.txt
	fi

	##################################################################################################################################################
	echo "=================================================== STXS 3 ==================================================="
	echo "=============================================== RUNNING ASIMOV ==============================================="
	echo "============================================= SIGNAL + BACKGROUND ============================================"
	echo "combine fh_STXS_3_j7_t4_DNN_Node0.root -n _asimov_STXS_3_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_j7_t4${POSTFIX}.out.txt"
	combine fh_STXS_3_j7_t4_DNN_Node0.root -n _asimov_STXS_3_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_j7_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_3_sig_j7_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_3_j8_t4_DNN_Node0.root -n _asimov_STXS_3_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_j8_t4${POSTFIX}.out.txt"
	combine fh_STXS_3_j8_t4_DNN_Node0.root -n _asimov_STXS_3_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_j8_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_3_sig_j8_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_3_j9_t4_DNN_Node0.root -n _asimov_STXS_3_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_j9_t4${POSTFIX}.out.txt"
	combine fh_STXS_3_j9_t4_DNN_Node0.root -n _asimov_STXS_3_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_j9_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_3_sig_j9_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_3_t4_DNN_Node0.root -n _asimov_STXS_3_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_comb${POSTFIX}.out.txt"
	combine fh_STXS_3_t4_DNN_Node0.root -n _asimov_STXS_3_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_3_sig_comb${POSTFIX}.out.txt
	cat combine_asimov_STXS_3_sig_comb${POSTFIX}.out.txt

	if [ "${RUNBKGFIT}" = true ]; then
	    echo "=============================================== BACKGROUND ONLY =============================================="
	    echo "combine fh_STXS_3_j7_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_j7_t4${POSTFIX}.out.txt"
	    combine fh_STXS_3_j7_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_j7_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_3_bkg_j7_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_3_j8_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_j8_t4${POSTFIX}.out.txt"
	    combine fh_STXS_3_j8_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_j8_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_3_bkg_j8_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_3_j9_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_j9_t4${POSTFIX}.out.txt"
	    combine fh_STXS_3_j9_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_j9_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_3_bkg_j9_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_3_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_comb${POSTFIX}.out.txt"
	    combine fh_STXS_3_t4_DNN_Node0.root -n _asimov_STXS_3_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_3_bkg_comb${POSTFIX}.out.txt
	    cat combine_asimov_STXS_3_bkg_comb${POSTFIX}.out.txt
	fi
	##################################################################################################################################################
	echo "=================================================== STXS 4 ==================================================="
	echo "=============================================== RUNNING ASIMOV ==============================================="
	echo "============================================= SIGNAL + BACKGROUND ============================================"
	echo "combine fh_STXS_4_j7_t4_DNN_Node0.root -n _asimov_STXS_4_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_j7_t4${POSTFIX}.out.txt"
	combine fh_STXS_4_j7_t4_DNN_Node0.root -n _asimov_STXS_4_sig_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_j7_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_4_sig_j7_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_4_j8_t4_DNN_Node0.root -n _asimov_STXS_4_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_j8_t4${POSTFIX}.out.txt"
	combine fh_STXS_4_j8_t4_DNN_Node0.root -n _asimov_STXS_4_sig_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_j8_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_4_sig_j8_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_4_j9_t4_DNN_Node0.root -n _asimov_STXS_4_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_j9_t4${POSTFIX}.out.txt"
	combine fh_STXS_4_j9_t4_DNN_Node0.root -n _asimov_STXS_4_sig_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_j9_t4${POSTFIX}.out.txt
	cat combine_asimov_STXS_4_sig_j9_t4${POSTFIX}.out.txt
	echo "combine fh_STXS_4_t4_DNN_Node0.root -n _asimov_STXS_4_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_comb${POSTFIX}.out.txt"
	combine fh_STXS_4_t4_DNN_Node0.root -n _asimov_STXS_4_sig_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 1 &> combine_asimov_STXS_4_sig_comb${POSTFIX}.out.txt
	cat combine_asimov_STXS_4_sig_comb${POSTFIX}.out.txt

	if [ "${RUNBKGFIT}" = true ]; then
	    echo "=============================================== BACKGROUND ONLY =============================================="
	    echo "combine fh_STXS_4_j7_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_j7_t4${POSTFIX}.out.txt"
	    combine fh_STXS_4_j7_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_j7_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_j7_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_4_bkg_j7_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_4_j8_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_j8_t4${POSTFIX}.out.txt"
	    combine fh_STXS_4_j8_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_j8_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_j8_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_4_bkg_j8_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_4_j9_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_j9_t4${POSTFIX}.out.txt"
	    combine fh_STXS_4_j9_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_j9_t4${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_j9_t4${POSTFIX}.out.txt
	    cat combine_asimov_STXS_4_bkg_j9_t4${POSTFIX}.out.txt
	    echo "combine fh_STXS_4_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_comb${POSTFIX}.out.txt"
	    combine fh_STXS_4_t4_DNN_Node0.root -n _asimov_STXS_4_bkg_comb${POSTFIX} ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} -t -1 --expectSignal 0 &> combine_asimov_STXS_4_bkg_comb${POSTFIX}.out.txt
	    cat combine_asimov_STXS_4_bkg_comb${POSTFIX}.out.txt
	fi


	
    fi
fi


if [ "${RUNDIFFNUI}" = true ]; then
    ##################################################################################################################################################
    echo "=================================================== STXS 0 ==================================================="
    echo "============================================== RUNNING DIFFNUI ==============================================="
    echo "============================================= SIGNAL + BACKGROUND ============================================"
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_0_sig_comb${POSTFIX}.txt
	
    echo "=============================================== BACKGROUND ONLY =============================================="
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_0_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_0_bkg_comb${POSTFIX}.txt

    ##################################################################################################################################################
    echo "=================================================== STXS 1 ==================================================="
    echo "============================================== RUNNING DIFFNUI ==============================================="
    echo "============================================= SIGNAL + BACKGROUND ============================================"
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_1_sig_comb${POSTFIX}.txt
	
    echo "=============================================== BACKGROUND ONLY =============================================="
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_1_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_1_bkg_comb${POSTFIX}.txt

    ##################################################################################################################################################
    echo "=================================================== STXS 2 ==================================================="
    echo "============================================== RUNNING DIFFNUI ==============================================="
    echo "============================================= SIGNAL + BACKGROUND ============================================"
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_2_sig_comb${POSTFIX}.txt
	
    echo "=============================================== BACKGROUND ONLY =============================================="
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_2_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_2_bkg_comb${POSTFIX}.txt

    ##################################################################################################################################################
    echo "=================================================== STXS 3 ==================================================="
    echo "============================================== RUNNING DIFFNUI ==============================================="
    echo "============================================= SIGNAL + BACKGROUND ============================================"
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_3_sig_comb${POSTFIX}.txt
	
    echo "=============================================== BACKGROUND ONLY =============================================="
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_3_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_3_bkg_comb${POSTFIX}.txt

    ##################################################################################################################################################
    echo "=================================================== STXS 4 ==================================================="
    echo "============================================== RUNNING DIFFNUI ==============================================="
    echo "============================================= SIGNAL + BACKGROUND ============================================"
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_sig_comb${POSTFIX}.root &> diffNui_asimov_STXS_4_sig_comb${POSTFIX}.txt
	
    echo "=============================================== BACKGROUND ONLY =============================================="
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_7j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_j7_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_7j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_8j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_j8_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_8j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_9j4t${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_j9_t4${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_9j4t${POSTFIX}.txt
    echo "python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_comb${POSTFIX}.txt"
    python ${DIFFNUI} --all fitDiagnostics_asimov_STXS_4_bkg_comb${POSTFIX}.root &> diffNui_asimov_STXS_4_bkg_comb${POSTFIX}.txt
    

fi
