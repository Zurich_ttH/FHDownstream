from glob import glob
from copy import copy
import os
import sys

scriptTemplates = '#!/bin/bash \n'
scriptTemplates += '#  \n'
scriptTemplates += '#SBATCH -p quick  \n'
scriptTemplates += '#SBATCH --account=t3  \n'
scriptTemplates += '#SBATCH --job-name=%%NAMEPREFIX%%_%%PREFIX%%_%%FILEBASE%%  \n'
#scriptTemplates += '#SBATCH --mem=3000M                     # memory 3GB (per job)'
scriptTemplates += '#SBATCH --time 01:00:00 \n'
scriptTemplates += '#SBATCH -o output/%x-%j.out \n'
scriptTemplates += '#SBATCH -e output/%x-%j.err \n'
scriptTemplates += '\n'
scriptTemplates += 'echo HOME: $HOME \n'
scriptTemplates += 'echo PWD: $(pwd) \n'
scriptTemplates += 'echo USER: $USER \n'
scriptTemplates += 'echo SLURM_JOB_ID: $SLURM_JOB_ID \n'
scriptTemplates += 'echo HOSTNAME: $HOSTNAME \n'
scriptTemplates += '\n'
scriptTemplates += 'source %%SCRIPT%% \n'
scriptTemplates += 'EXITCODE=$? \n'
scriptTemplates += 'if [ $EXITCODE == "0" ] \n'
scriptTemplates += 'then \n'
scriptTemplates += '    echo SUCCESS \n'
scriptTemplates += 'else \n'
scriptTemplates += '   echo FAILED \n'
scriptTemplates += 'fi \n'

rMax = "10"
prefix = "v1"

nameprefix = sys.argv[1]
datacardFile = sys.argv[2]

datacardName = datacardFile.split("/")[-1].replace(".root", "")


initCommand = "combineTool.py -M Impacts -m 125.38 --doInitialFit -t -1 --expectSignal 1 --cminDefaultMinimizerStrategy 0 --cminDefaultMinimizerTolerance 1e-2 --cminFallbackAlgo Minuit2,Migrad,0:1e-2 --X-rtd MINIMIZER_analytic --rMin -"+rMax+" --rMax "+rMax+" --robustFit 1"

fullInitCommand = "{} -d {} -n Impacts_{}_{}".format(initCommand, datacardFile, prefix, datacardName)
print(fullInitCommand)
os.system(fullInitCommand)

nuiCommand = "combineTool.py -M Impacts -m 125.38 --doFits -t -1 --expectSignal 1 --cminDefaultMinimizerStrategy 0 --cminDefaultMinimizerTolerance 1e-2 --cminFallbackAlgo Minuit2,Migrad,0:1e-2 --X-rtd MINIMIZER_analytic --rMin -10 --rMax 10 --job-mode script --robustFit 1"
fullnuiCommand = "{} -d {} -n Impacts_{}_{}".format(nuiCommand, datacardFile, prefix, datacardName)
print(fullnuiCommand)
os.system(fullnuiCommand)

os.system("mkdir output")

for file_ in glob("job_combine_task*.sh"):
    baseName = file_.split("/")[-1].replace(".sh","")
    thisScript = copy(scriptTemplates)
    thisScript = thisScript.replace("%%PREFIX%%", prefix)
    thisScript = thisScript.replace("%%FILEBASE%%", baseName)
    thisScript = thisScript.replace("%%NAMEPREFIX%%", nameprefix)
    thisScript = thisScript.replace("%%SCRIPT%%", file_)
    with open("slum_fit_"+prefix+"_"+baseName+".sh", "w") as f:
        f.write(thisScript)


finalCommand1 = "combineTool.py -M Impacts -d "+datacardFile+" -m 125.38  -n Impacts_"+prefix+"_"+datacardName+" -o impacts.json"
print(finalCommand1)
os.system("echo "+finalCommand1+" >> impCommand.txt")
print("plotImpacts.py -i impacts.json -o impacts")
