#!/bin/bash

BASEFOLDERSR=$1
BASEFOLDERCR=$2
OUTFOLDER=$3

mkdir -p ${OUTFOLDER}

hadd ${OUTFOLDER}/TTbb_hdamp_up.root ${BASEFOLDERSR}/TTbb_*hdampUP*.root ${BASEFOLDERCR}/TTbb_*hdampUP*.root
hadd ${OUTFOLDER}/TTbb_hdamp_down.root ${BASEFOLDERSR}/TTbb_*hdampDOWN*.root ${BASEFOLDERCR}/TTbb_*hdampDOWN*.root

hadd ${OUTFOLDER}/TTIncl_hdamp_up.root ${BASEFOLDERSR}/TTTo*hdampUP*.root ${BASEFOLDERCR}/TTTo*hdampUP*.root
hadd ${OUTFOLDER}/TTIncl_hdamp_down.root ${BASEFOLDERSR}/TTTo*hdampDOWN*.root ${BASEFOLDERCR}/TTTo*hdampDOWN*.root
hadd ${OUTFOLDER}/TTIncl_tune_up.root ${BASEFOLDERSR}/TTTo*TuneCP5up*.root ${BASEFOLDERCR}/TTTo*TuneCP5up*.root
hadd ${OUTFOLDER}/TTIncl_tune_down.root ${BASEFOLDERSR}/TTTo*TuneCP5down*.root ${BASEFOLDERCR}/TTTo*TuneCP5down*.root



hadd ${OUTFOLDER}/TTbb_nom.root \
     ${BASEFOLDERSR}/TTbb_4f_TTTo2l2nu_TuneCP5-Powheg-Openloops-Pythia8.root \
     ${BASEFOLDERSR}/TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8.root \
     ${BASEFOLDERSR}/TTbb_4f_TTToSemiLeptonic_TuneCP5-Powheg-Openloops-Pythia8.root \
     ${BASEFOLDERCR}/TTbb_4f_TTTo2l2nu_TuneCP5-Powheg-Openloops-Pythia8.root \
     ${BASEFOLDERCR}/TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8.root \
     ${BASEFOLDERCR}/TTbb_4f_TTToSemiLeptonic_TuneCP5-Powheg-Openloops-Pythia8.root

hadd ${OUTFOLDER}/TTIncl_nom.root \
     ${BASEFOLDERSR}/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root \
     ${BASEFOLDERSR}/TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root \
     ${BASEFOLDERSR}/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root \
     ${BASEFOLDERCR}/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root \
     ${BASEFOLDERCR}/TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root \
     ${BASEFOLDERCR}/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root
