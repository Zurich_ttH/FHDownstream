import ROOT
import sys, os
import logging
from copy import deepcopy
import math 
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

from makeDatacards import replChannel, replVariable, replProcess, replSystematic

def calcPDFWeightFromMembers(
        inputFile, postfix, namePostfix, 
        templateSchemaNominal = "$PROCESS__$CHANNEL__$VARIABLE",
        templateSchemaSystematic = "$PROCESS__$CHANNEL__$VARIABLE__$SYSTEMATIC"):
    logging.info("Arguments:")
    logging.info("    inputFile: %s",inputFile)
    logging.info("    postfix: %s",postfix)
    logging.info("    namePostfix : %s",namePostfix )
    logging.info("    templateSchemaNominal: %s",templateSchemaNominal)
    logging.info("    templateSchemaSystematic : %s",templateSchemaSystematic )
    folder = "/".join(inputFile.split("/")[0:-1])
    logging.info("Operating folder: %s", folder)
    
    rFile = ROOT.TFile.Open(inputFile)

    allKeys = [x.GetName() for x in rFile.GetListOfKeys()]
    #CMS_ttHbb_PDFMember_XXX

    
    nominals = [x for x in allKeys if len(x.split("__"))==3]
    systematics = [x for x in allKeys if len(x.split("__"))==4]
    systematics = [x for x in systematics if "CMS_ttHbb_PDFMember" in x]

    carryOverNames = [x for x in allKeys if x not in systematics]
    
    carryOver = []
    for key in carryOverNames:
        carryOver.append(deepcopy(rFile.Get(key)))

    
    _procs = list(set([x.split("__")[0] for x in nominals]))
    _cats = list(set([x.split("__")[1] for x in nominals]))
    _vars = list(set([x.split("__")[2] for x in nominals]))
    _systs = list(set([x.split("__")[3] for x in systematics]))

    logging.info("Procs: %s", _procs)
    logging.info("Systamtics: %s",  _systs)
    logging.info("number of Systamtics: %s",  len(_systs))

    logging.info("Categories: %s", _cats)
    
    doRMS = False
    if "TTbb" in inputFile:
        logging.warning("Doing RMS")
        doRMS = True    

    
    nominals = []
    newHistos = []
    for proc in _procs:
        logging.info("Processing %s", proc)
        for cat in _cats:
            logging.debug("Processing %s", cat)
            for var in _vars:
                logging.debug("Processing %s", var)
                nominalName = replVariable(
                    replChannel(
                        replProcess(templateSchemaNominal, proc),

                        cat),
                    var)
                
                hNom = rFile.Get(nominalName)
                logging.debug("hNom: %s (%s)", nominalName, hNom)
                try:
                    hNom.GetNbinsX()
                except AttributeError:
                    logging.error("Skipping %s", nominalName )
                    continue
                nominals.append(nominalName)
                res = []
                binVals = []
                for iBin in range(hNom.GetNbinsX()+1):
                    binVals.append(hNom.GetBinContent(iBin))
                    res.append(0)
                    
                for syst in _systs:
                    systName = replSystematic(
                        replVariable(
                            replChannel(
                                replProcess(templateSchemaSystematic, proc),
                                cat),
                            var),
                        syst)
                    logging.debug(systName)
                    thisSyst = rFile.Get(systName)
                    #thisSyst.Scale(1/1.05290660498)#TEMP
                    for iBin in range(thisSyst.GetNbinsX()+1):
                        logging.debug("%s / %s / %s", binVals[iBin], thisSyst.GetBinContent(iBin), pow(binVals[iBin]-thisSyst.GetBinContent(iBin), 2))
                        res[iBin] += pow(binVals[iBin]-thisSyst.GetBinContent(iBin), 2)

                if doRMS:
                    #res = [math.sqrt((1/len(res))*x) for x in res]
                    res = [math.sqrt((1/float(len(res)))*x) for x in res]
                else:
                    res = [math.sqrt(x) for x in res]
                logging.debug("Diff %s", res)
                sysBaseName = replVariable(replChannel(replProcess(templateSchemaSystematic, proc), cat), var)
                thisUpHisto = hNom.Clone(replSystematic(sysBaseName, "CMS_ttHbb_PDFFromMembers"+namePostfix+"Up"))
                thisUpHisto.SetTitle(replSystematic(sysBaseName, "CMS_ttHbb_PDFFromMembers"+namePostfix+"Up"))
                thisDownHisto =  hNom.Clone(replSystematic(sysBaseName, "CMS_ttHbb_PDFFromMembers"+namePostfix+"Down"))
                thisDownHisto.SetTitle(replSystematic(sysBaseName, "CMS_ttHbb_PDFFromMembers"+namePostfix+"Down"))
                for iBin in range(hNom.GetNbinsX()+1):
                    thisUpHisto.SetBinContent(iBin, thisUpHisto.GetBinContent(iBin)+res[iBin])
                    thisDownHisto.SetBinContent(iBin, thisDownHisto.GetBinContent(iBin)-res[iBin])
                newHistos.append(deepcopy(thisUpHisto))
                newHistos.append(deepcopy(thisDownHisto))
                #raw_input("NEXT")
                
    outName = inputFile.split("/")[-1].replace(".root","")+"_"+postfix+".root"
    logging.debug("Will use output file %s", outName)
    outFile = ROOT.TFile(folder+"/"+outName, "RECREATE")
    outFile.cd()

    for template in carryOver:
        template.Write()

    for templateName in newHistos:
        templateName.Write()

    outFile.Close()

    return folder+"/"+outName

if __name__ == "__main__":
    import argparse
    ############################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        help = "Input file",
        nargs = "+",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--folder",
        action = "store",
        help = "input folder",
        type = str,
        default = None,
    )    
    argumentparser.add_argument(
        "--outPostfix",
        action = "store",
        help = "Postfix for output file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--templatePostfix",
        action = "store",
        help = "Postfix for the tempalte name/title",
        type = str,
        nargs = "+",
        default = None,
    )
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    if args.templatePostfix is None:
        templatePostfix = len(args.inputs)*[""]
    else:
        templatePostfix = args.templatePostfix

    assert len(templatePostfix) == len(args.inputs)
    
    for ifile, infile in enumerate(args.inputs):
        if args.folder is not None:
            fullpathInput = args.folder+"/"+infile
        else:
            fullpathInput = infile
        logging.info("Processing %s", fullpathInput)
        calcPDFWeightFromMembers(fullpathInput, args.outPostfix, templatePostfix[ifile])
