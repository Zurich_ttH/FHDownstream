#!/bin/bash

FOLDER=$1

VARS=(DNN_Node0)

for VAR in "${VARS[@]}"
do
    python removeVariable.py --input ${FOLDER}/TTbb_ttbarOther_systamtics.root --var ${VAR}
    python removeVariable.py --input ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics.root --var ${VAR}
    python removeVariable.py --input ${FOLDER}/TTbb_ttbarPlusCCbar_systamtics.root --var ${VAR}
    
    python removeVariable.py --input ${FOLDER}/TTIncl_ttbarOther_systamtics.root --var ${VAR}
    python removeVariable.py --input ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics.root --var ${VAR}
    python removeVariable.py --input ${FOLDER}/TTIncl_ttbarPlusCCbar_systamtics.root --var ${VAR}
done

