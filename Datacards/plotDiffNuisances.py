""" Script for making pull plot split in different categories. Can also be used to make pull plots with b-Only pulls for mu=0 and s+b pulls for mu=1. """
import ROOT

import logging
from collections import OrderedDict

from copy import deepcopy
import sys, os
sys.path.insert(0, "../Plotting/classes/")

from PlotHelpers import saveCanvasListAsPDF


def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )


def getPulls(diffNuisancesOutput, bOnly = False, splusb = False, mlfitFile=False):
    """
    This functions parses the output from the diffNuisances.py script (from the combine repo) with 
    the --all option. See help message for exact command.
    Args:
    =====
    diffNuisancesOutput (str) : filename of piped diffNuisances output
    bOnly/splusb (bool) : Defines if return value

    Returns:
    ========
    Script returns 
    - b-only pulls of bOnly = True and splusb = False
    - s+b pulls of bOnly = False and splusb = True
    - tuple (s+b , b-only) otherwise
    """
    lines = None
    with open(diffNuisancesOutput) as f:
        lines = f.readlines()

    SplusB_pulls = OrderedDict()
    BOnly_pulls = OrderedDict()
    logging.debug("Creating pull dicts for %s", diffNuisancesOutput)
    for l in lines:
        if not mlfitFile:
            lineElem = l.split(" ")
            line = []
            for elem in lineElem:
                if elem not in ["", "*", "!"]:
                    elem = elem.replace(",","")
                    elem = elem.replace("!","")
                    elem = elem.replace("*","")
                    line.append(elem)
            if len(line) != 6:
                continue
            if line[0] == "name":
                continue
            logging.debug("For %s",line[0])
            logging.debug("Found: B-only: %s +- %s",line[1], line[2])
            logging.debug("Found: S+B: %s +- %s",line[3], line[4])
            SplusB_pulls.update({line[0] : (float(line[3]), float(line[4]))})
            BOnly_pulls.update({line[0] : (float(line[1]), float(line[2]))})
        else:
            line = l
            if line.startswith("|"):
                if "parameter" in line:
                    continue
                #print line
                lineElements = line.split("|")
                while "" in lineElements:
                    lineElements.remove("")
                lineName = lineElements[0]
                lineName = lineName.replace(" ","")
                linebOnly = lineElements[1]
                linesplusb = lineElements[2]
                bOnlyValues = linebOnly.split(" ")
                splusbValues = linesplusb.split(" ")
                while "" in bOnlyValues:
                    bOnlyValues.remove("")
                while "" in splusbValues:
                    splusbValues.remove("")
                bOnlyCentral, bOnlyErr = bOnlyValues[0], bOnlyValues[1]
                splusbCentral, splusbErr = splusbValues[0], splusbValues[1]
                if bOnlyValues[1] != bOnlyValues[2]:
                    logging.warning("Unsymmetric nuisance b Only %s. Will use mean", lineName)
                    bOnlyErr = (bOnlyValues[1]+bOnlyValues[2])/2
                if splusbValues[1] != splusbValues[2]:
                    logging.warning("Unsymmetric nuisance s+b %s. Will use mean", lineName)
                    splusbErr = (splusbValues[1]+splusbValues[2])/2
                logging.debug("For %s",lineName)
                logging.debug("Found: B-only: %s +- %s",bOnlyCentral, bOnlyErr)
                logging.debug("Found: S+B: %s +- %s", splusbCentral, splusbErr )
                SplusB_pulls.update({lineName : (float(bOnlyCentral), float(bOnlyErr))})
                BOnly_pulls.update({lineName : (float(splusbCentral), float(splusbErr))})

    if bOnly and not splusb:
        return BOnly_pulls
    elif not bOnly and splusb:
        return SplusB_pulls
    else:
        return SplusB_pulls, BOnly_pulls

def fillPlot(pulls, histo, graph, names, shift = False):
    logging.debug("Filling graph: %s", graph)
    j = 1
    for name in names:
        pull, Xsigma = pulls[name]
        if histo is not None:
            histo.GetXaxis().SetBinLabel(j, name)
        thisShift = 0.6 if shift else 0.4
        graph.SetPoint(j, j-thisShift, pull)
        graph.SetPointError(j, 0.1, Xsigma)
        j+=1

def getMaxError(pulls_SB, pulls_B, names):
    maxError = 0.0
    for name in names:
        pullS, XsigmaS = pulls_SB[name]
        pull, Xsigma = pulls_B[name]
        if max(Xsigma,XsigmaS) > maxError: maxError = max(Xsigma,XsigmaS)
    logging.debug("passing maxerror : %s", maxError)
    return maxError


def makePullPlot(one, negone, histo, gSB, gB, maxerror, isData):
    logging.info("Making canvas")
    thismax = int(maxerror+0.999)
    div = 500 + 2*thismax #100*secondary+primary

    leg = ROOT.TLegend(0.89,0.55,0.96,0.90)
    leg.SetFillStyle(0)
    leg.SetBorderSize(1)
    leg.SetTextSize(0.045)

    #draw histograms
    one.SetLineColor(1)
    one.SetLineStyle(2)
    one.SetLineWidth(1)
    negone.SetLineColor(1)
    negone.SetLineStyle(2)
    negone.SetLineWidth(1)
    
    histo.SetLineColor(1)
    gB.SetLineWidth(3)
    gB.SetLineColor(ROOT.kAzure-3)
    gSB.SetLineWidth(3)
    gSB.SetLineColor(ROOT.kRed-3)
    #gJetB.SetMarkerColor(4)
    #gJetB.SetMarkerStyle(20)
    histo.GetXaxis().SetLabelSize(0.060)
    histo.GetYaxis().SetLabelSize(0.045)
    #hJet.GetYaxis().SetRangeUser(-maxJet,maxJet)
    histo.GetYaxis().SetRangeUser(-2,2)
    #hJet.GetYaxis().SetNdivisions(JetDiv)
    histo.GetYaxis().SetNdivisions(504) #100*secondary+primary

    leg.AddEntry(gB, "B only fit","LE")
    if not isData:
        leg.AddEntry(None, "#mu=0", "")
    #leg.AddEntry(ROOT.TObject(), " ", "")
    leg.AddEntry(gSB, "S+B fit","LE")
    if not isData:
        leg.AddEntry(None, "#mu=1", "")

    c1 = ROOT.TCanvas("c1","",5,30,1580,460)
    #p1 = ROOT.TPad("p1","",0,0,1,1)
    c1.SetBottomMargin(0.27)
    c1.SetLeftMargin(0.03)
    c1.SetRightMargin(0.12)
    #c1.SetTicks()
    histo.Draw()

    one.Draw("SAME")
    negone.Draw("SAME")
    gB.Draw("GP")
    gSB.Draw("GP")
    leg.Draw("same")
    #c1.Update()

    return deepcopy(c1)


def makePlots(pulls_SB, pulls_B, folder, output, isData):
    nbinsShift = 0
    shiftSysts = []
    nbinsJet = 0
    jetSysts = []
    nbinsNorm = 0
    normSysts = []
    nbinsTheory = 0
    theory = []
    for key in pulls_SB:
        if "prop_bin" in key or "_Bin" in key:
            continue
        elif key.find("_j")>0:
            nbinsJet += 1
            jetSysts.append(key)
        elif "UE" in key or "HDAMP" in key or "FSR" in key or "ISR" in key:
            nbinsShift += 1
            shiftSysts.append(key)
        elif key.startswith("pdf_") or key.startswith("QCDscale_") or "lumi" in key or "effTrigger" in key or "_eff_" in key or "FHtrigger" in key or "L1Pre" in key:
            nbinsTheory += 1
            theory.append(key)
        else:
            nbinsNorm += 1
            normSysts.append(key)

    logging.info("Creating histograms and graphs")
    hJet = ROOT.TH1F("hJet","Jet energy correction uncertainties",nbinsJet,0,nbinsJet)
    hNorm = ROOT.TH1F("hNorm","Event weight and normalisation uncertainties I",nbinsNorm,0,nbinsNorm)
    hShift = ROOT.TH1F("hShift","Event weight and normalisation uncertainties II",nbinsShift,0,nbinsShift)
    hTheory = ROOT.TH1F("hTheory","L1Prefiring, Efficiencies, Trigger, lumi and x-sec unc. from PDF and scale variation",nbinsTheory,0,nbinsTheory)
    gJetB = ROOT.TGraphErrors(nbinsJet)
    gNormB = ROOT.TGraphErrors(nbinsNorm)
    gShiftB = ROOT.TGraphErrors(nbinsShift)
    gTheoryB = ROOT.TGraphErrors(nbinsTheory)
    gJetS = ROOT.TGraphErrors(nbinsJet)
    gNormS = ROOT.TGraphErrors(nbinsNorm)
    gShiftS = ROOT.TGraphErrors(nbinsShift)
    gTheoryS = ROOT.TGraphErrors(nbinsTheory)
    one = ROOT.TF1("one","1",0,max(nbinsJet,nbinsNorm))
    negone = ROOT.TF1("negone","-1",0,max(nbinsJet,nbinsNorm))

    logging.info("Filling jet plots")
    fillPlot(pulls_SB, hJet, gJetS, jetSysts, shift = True)
    fillPlot(pulls_B, None, gJetB, jetSysts)

    logging.info("Filling nom plots")
    fillPlot(pulls_SB, hNorm, gNormS, normSysts, shift = True)
    fillPlot(pulls_B, None, gNormB, normSysts)

    logging.info("Filling plots with UE, HDAMP, FSR and ISR")
    fillPlot(pulls_SB, hShift, gShiftS, shiftSysts, shift = True)
    fillPlot(pulls_B, None, gShiftB, shiftSysts)

    logging.info("Filling theory nuic.")
    fillPlot(pulls_SB, hTheory, gTheoryS, theory, shift = True)
    fillPlot(pulls_B, None, gTheoryB, theory)
    
    maxJetErr = getMaxError(pulls_SB, pulls_B, jetSysts)
    maxNormErr = getMaxError(pulls_SB, pulls_B, normSysts)
    maxShiftsErr = getMaxError(pulls_SB, pulls_B, shiftSysts)
    maxTheoryErr = getMaxError(pulls_SB, pulls_B, theory)

    canvasJets = makePullPlot(one, negone, hJet, gJetS, gJetB, maxJetErr, isData)
    canvasNom = makePullPlot(one, negone, hNorm, gNormS, gNormB, maxNormErr, isData)
    canvasShifts = makePullPlot(one, negone, hShift, gShiftS, gShiftB, maxShiftsErr, isData)
    canvasTheory = makePullPlot(one, negone, hTheory, gTheoryS, gTheoryB, maxTheoryErr, isData)
    
    
    saveCanvasListAsPDF(
        [canvasJets, canvasNom, canvasShifts, canvasTheory],
        output,
        folder
    )

def plotDiffNuisances(SBIn, BOnlyIn, outputfolder, outputfile, isData, ismlFitFile):
    ROOT.gStyle.SetOptStat(0)
    ROOT.gROOT.SetBatch(1)
    pulls_SplusB = getPulls(SBIn, splusb = True, mlfitFile = ismlFitFile)
    pulls_BOnly = getPulls(BOnlyIn, bOnly = True, mlfitFile = ismlFitFile)

    makePlots(pulls_SplusB , pulls_BOnly, outputfolder, outputfile, isData)



if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for making split pull plots from diffnuisances output. Run python $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit/test/diffNuisances.py fitDiagnostics.root --all &> pulls.txt',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inSplusB",
        action = "store",
        help = "Input file from the S+B fit",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--inBOnly",
        action = "store",
        help = "Input file from the B-only fit",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        help = "Outputfile name",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outfolder",
        action = "store",
        help = "Output folder",
        type = str,
        default = "out_pulls"
    )
    argumentparser.add_argument(
        "--isData",
        action = "store_true",
        help = "Pass if pulls are plotted for data",
    )
    argumentparser.add_argument(
        "--mlfitFile",
        action = "store_true",
        help = "Pass if using the output stored in the pull plot dir of the shared datacard FW",
    )
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)
    plotDiffNuisances(args.inSplusB, args.inBOnly, args.outfolder, args.output, args.isData, args.mlfitFile)
