import ROOT
import sys
from copy import deepcopy


infile = sys.argv[1]

rFile = ROOT.TFile.Open(infile)

keys = rFile.GetListOfKeys()

proc = "ttbarPlusBBbar"

nomYieldQCD = {}
histos = []
hDict = {}
nomBkgYield = {}
procs = []
variables = []
cats = []
nomYields = {}
for key in keys:
    keyName = key.GetName()
    histos.append(deepcopy(rFile.Get(keyName)))
    if len(keyName.split("__")) == 3:
        nomYields[keyName] = rFile.Get(keyName).Integral()
for histo in histos:
   hName = histo.GetName()
   if proc not in hName:
       continue
   if hName.split("__") > 3:
       nominal = "{0}__{1}__{2}".format(hName.split("__")[0], hName.split("__")[1], hName.split("__")[2])
       print hName,nomYields[nominal]/float(histo.Integral())
        
        
