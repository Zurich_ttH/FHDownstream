import ROOT
import sys
from copy import deepcopy
import math

inFile = sys.argv[1]
outDirPlusPrefix = sys.argv[2]
rFile = ROOT.TFile.Open(inFile)

newLastBin = 1600


histos = []
for key in rFile.GetListOfKeys():
    keyName = key.GetName()
    if not "ht30" in keyName:
        continue
    histos.append(deepcopy(rFile.Get(keyName)))
    
firstHisto = histos[0]

nBinsOrig = firstHisto.GetNbinsX()
firstBin = firstHisto.GetBinLowEdge(1)
lastBinOrig = firstHisto.GetBinLowEdge(nBinsOrig+1)

nBinsNew = None
for xBin in range(nBinsOrig+1):
    if firstHisto.GetBinLowEdge(xBin) == newLastBin:
        nBinsNew = xBin-1

if nBinsNew is None:
    raise RuntimeError("No binning selected")

newHistoBase = ROOT.TH1F("newHistoBase", "newHistoBase", nBinsNew, firstBin, newLastBin)

rOutFile = ROOT.TFile(outDirPlusPrefix+".root", "RECREATE")
rOutFile.cd()
outHistos = []
for histo in histos:
    thisHisto = newHistoBase.Clone(histo.GetName())
    thisHisto.SetTitle(histo.GetName())
    OverFlowContent = 0
    OverFlowError = 0
    for xBin in range(histo.GetNbinsX()+1):
        if xBin < nBinsNew:
            thisHisto.SetBinContent(xBin, histo.GetBinContent(xBin))
            thisHisto.SetBinError(xBin, histo.GetBinError(xBin))
        else:
            OverFlowContent += histo.GetBinContent(xBin)
            OverFlowError += histo.GetBinError(xBin)*histo.GetBinError(xBin)

    #OverFlowError = math.sqrt(histo.GetBinError(nBinsNew)*histo.GetBinError(nBinsNew) + OverFlowError)
    OverFlowError = math.sqrt(OverFlowError)
    #OverFlowContent += histo.GetBinContent(nBinsNew)
    thisHisto.SetBinContent(nBinsNew,OverFlowContent)
    thisHisto.SetBinError(nBinsNew,OverFlowError) 
    thisHisto.Write()
