#!/bin/bash

export BASEFOLDER="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2017/v3p3/v1"
export OUTPUTFOLDER="finalFiles"

echo "hadd ${BASEFOLDER}/${OUTPUTFOLDER}/TTToHadronic_Merged.root ${BASEFOLDER}/TTToHadronic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarOther.root ${BASEFOLDER}/TTToHadronic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${BASEFOLDER}/TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root"
hadd ${BASEFOLDER}/${OUTPUTFOLDER}/TTToHadronic_Merged.root ${BASEFOLDER}/TTToHadronic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarOther.root ${BASEFOLDER}/TTToHadronic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${BASEFOLDER}/TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root

echo "hadd ${BASEFOLDER}/${OUTPUTFOLDER}/TTTo2L2Nu_Merged.root ${BASEFOLDER}/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarOther.root ${BASEFOLDER}/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${BASEFOLDER}/TTbb_4f_TTTo2l2nu_TuneCP5-Powheg-Openloops-Pythia8_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root"
hadd ${BASEFOLDER}/${OUTPUTFOLDER}/TTTo2L2Nu_Merged.root ${BASEFOLDER}/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarOther.root ${BASEFOLDER}/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${BASEFOLDER}/TTbb_4f_TTTo2l2nu_TuneCP5-Powheg-Openloops-Pythia8_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root

echo "hadd ${BASEFOLDER}/${OUTPUTFOLDER}/TTToSemiLeptonic_Merged.root ${BASEFOLDER}/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarOther.root ${BASEFOLDER}/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${BASEFOLDER}/TTbb_4f_TTToSemiLeptonic_TuneCP5-Powheg-Openloops-Pythia8_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root"
hadd ${BASEFOLDER}/${OUTPUTFOLDER}/TTToSemiLeptonic_Merged.root ${BASEFOLDER}/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarOther.root ${BASEFOLDER}/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8_PDFMerged_ttbbMerged_ttbarPlusCCbar.root ${BASEFOLDER}/TTbb_4f_TTToSemiLeptonic_TuneCP5-Powheg-Openloops-Pythia8_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root
