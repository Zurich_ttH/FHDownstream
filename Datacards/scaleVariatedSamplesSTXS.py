import sys

from glob import glob
from copy import deepcopy

import ROOT

def main(baseTRF, baseNom, sampleBase, outFolder="scaled"):
    variations = []
    for file_ in glob(baseTRF+"/"+sampleBase+"*.root"):
#        if "down" in file_.split("/")[-1].lower() or "up" in file_.split("/")[-1].lower():
        variations.append(file_)
        print "Found variation: %s"%file_



    yieldNom = {}
    for file_ in glob(baseNom+"/"+sampleBase+"*.root"):
        yieldNom[file_.split("/")[-1]] = {}
        rNom = ROOT.TFile.Open(file_)
        for key in rNom.GetListOfKeys():
            yieldNom[file_.split("/")[-1]][key.GetName()] = rNom.Get(key.GetName()).Integral()

        rNom.Close()
    
    for file_ in variations:
        scaledHistos = {}
        rVar = ROOT.TFile.Open(file_)
        for key in rVar.GetListOfKeys():
           scaledHistos[key.GetName()] = deepcopy(rVar.Get(key.GetName()))
           if scaledHistos[key.GetName()].Integral() != 0:
               scaledHistos[key.GetName()].Scale( yieldNom[file_.split("/")[-1]][key.GetName()] / scaledHistos[key.GetName()].Integral() )
           else:
               print "Skipping %s"%key.GetName()


        rVar.Close()
        rOut = ROOT.TFile("/".join(file_.split("/")[0:-1]+[outFolder]+[file_.split("/")[-1]]),
                          "RECREATE")
        rOut.cd()
        for key in scaledHistos:
            scaledHistos[key].Write()
        rOut.Close()
    
if __name__ == "__main__":
    baseTRF = sys.argv[1]
    baseNom = sys.argv[2]
    sampleBase = sys.argv[3]


    main(baseTRF, baseNom, sampleBase)
