from __future__ import print_function, division
import sys
import ROOT

def mergeSparseVR(in_file):
    rFile = ROOT.TFile.Open(in_file)

    out_keys = [x.GetName().replace("VR_right","VR") for x in rFile.GetListOfKeys() if "VR_right" in x.GetName()]


    rOut = ROOT.TFile(in_file.replace(".root","_merged.root"), "RECREATE")
    rOut.cd()
    
    for key in out_keys:
        h = rFile.Get(key.replace("VR","VR_right")).Clone(key)
        h.Add(rFile.Get(key.replace("VR","VR_left")))

        h.Write()

        h_CR = rFile.Get(key.replace("VR","CR2_right")).Clone(key.replace("VR","CR2"))
        h_CR.Add(rFile.Get(key.replace("VR","CR2_left")))

        h_CR.Write()
        
if __name__ == "__main__":
    in_file = sys.argv[1]

    print("Processing %s"%in_file)
    
    mergeSparseVR(in_file)
