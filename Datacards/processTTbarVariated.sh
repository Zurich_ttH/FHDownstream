#!/bin/bash
BASEFOLDER=$1

#endChar="&"
endChar=""

echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_hdamp_up.root --mergeOnly --noGluSplitUnc &> TTIncl_hdamp_up.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_hdamp_up.root --mergeOnly --noGluSplitUnc &> TTIncl_hdamp_up.log ${endChar}

echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_hdamp_down.root --mergeOnly --noGluSplitUnc &> TTIncl_hdamp_down.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_hdamp_down.root --mergeOnly --noGluSplitUnc &> TTIncl_hdamp_down.log ${endChar}

echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_tune_up.root --mergeOnly --noGluSplitUnc &> TTIncl_tune_up.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_tune_up.root --mergeOnly --noGluSplitUnc &> TTIncl_tune_up.log ${endChar}

echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_tune_down.root --mergeOnly --noGluSplitUnc &> TTIncl_tune_down.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_tune_down.root --mergeOnly --noGluSplitUnc &> TTIncl_tune_down.log ${endChar}




echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb_hdamp_up.root --mergeOnly --noGluSplitUnc &> TTbb_hdamp_up.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb_hdamp_up.root --mergeOnly --noGluSplitUnc &> TTbb_hdamp_up.log ${endChar}
echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb_hdamp_down.root --mergeOnly --noGluSplitUnc &> TTbb_hdamp_down.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb_hdamp_down.root --mergeOnly --noGluSplitUnc &> TTbb_hdamp_down.log ${endChar}

echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb_nom.root --mergeOnly --noGluSplitUnc &> TTbb_nom.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTbb_nom.root --mergeOnly --noGluSplitUnc &> TTbb_nom.log ${endChar}

echo "python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTInlc_nom.root --mergeOnly --noGluSplitUnc &> TTInlc_nom.log &"
python ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards/processTTbarTemplates.py --inputs ${BASEFOLDER}/TTIncl_nom.root --mergeOnly --noGluSplitUnc &> TTIncl_nom.log ${endChar}
