#!/bin/bash

FOLDER2016="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2016"
FOLDER2017="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2017"
FOLDER2018="/t3home/koschwei/scratch/ttH/sparse/LegacyRun2/2018"
INFILE2016="LegacyRun2_2016_v3p3_v6p2_TTMerged_rebinnedv6"
INFILE2017="LegacyRun2_2017_v3p3_v6p2_TTMerged_rebinnedv6"
INFILE2018="LegacyRun2_2018_v3p3_v6p2_TTMerged_rebinnedv6"

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.018195 --ratedown 0.98213
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.992202 --ratedown 1.007859
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.981587 --ratedown 1.018758
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.995568 --ratedown 1.004452
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.015126 --ratedown 0.985099
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.990742 --ratedown 1.009344

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 0.982771 --ratedown 1.017531
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 1.03613 --ratedown 0.96513
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 0.97392 --ratedown 1.026778
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.98816 --ratedown 1.011982
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.987145 --ratedown 1.013022
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.994634 --ratedown 1.005395

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_SR --processes ttbarOther --name UERate --rateup 1.029397 --ratedown 0.971442
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_SR --processes ttbarOther --name UERate --rateup 1.016452 --ratedown 0.983814
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_SR --processes ttbarOther --name UERate --rateup 1.030903 --ratedown 0.970023
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_CR --processes ttbarOther --name UERate --rateup 1.003156 --ratedown 0.996854
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_CR --processes ttbarOther --name UERate --rateup 0.992611 --ratedown 1.007444
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_CR --processes ttbarOther --name UERate --rateup 1.00865 --ratedown 0.991424


python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.97128 --ratedown 1.029569
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.984666 --ratedown 1.015573
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.972117 --ratedown 1.028683
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.96373 --ratedown 1.037635
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.973652 --ratedown 1.027061
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.968354 --ratedown 1.032681

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.951235 --ratedown 1.051265
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.967206 --ratedown 1.033906
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.936967 --ratedown 1.067274
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.953177 --ratedown 1.049123
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.934593 --ratedown 1.069984
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.930696 --ratedown 1.074464

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.947377 --ratedown 1.055546
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.932157 --ratedown 1.072781
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.934213 --ratedown 1.07042
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j7_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.961011 --ratedown 1.040571
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j8_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.933583 --ratedown 1.071142
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2016}/${INFILE2016}.root --cat fh_j9_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.927728 --ratedown 1.077902

hadd ${FOLDER2016}/${INFILE2016}_UEHDAMPRates.root ${FOLDER2016}/${INFILE2016}_rateAdded*.root
rm -v ${FOLDER2016}/${INFILE2016}_rateAdded*.root 

#####################################################################################################################################################################

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.985205 --ratedown 1.015018
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.984271 --ratedown 1.01598
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.986214 --ratedown 1.013979
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.970253 --ratedown 1.030659
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.986485 --ratedown 1.0137
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.009539 --ratedown 0.990551

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 1.017101 --ratedown 0.983186
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 1.024546 --ratedown 0.976042
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 1.002488 --ratedown 0.997518
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.993555 --ratedown 1.006487
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.993216 --ratedown 1.006831
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.979008 --ratedown 1.021442

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_SR --processes ttbarOther --name UERate --rateup 0.994923 --ratedown 1.005103
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_SR --processes ttbarOther --name UERate --rateup 0.982734 --ratedown 1.017569
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_SR --processes ttbarOther --name UERate --rateup 1.050772 --ratedown 0.951682
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_CR --processes ttbarOther --name UERate --rateup 0.984554 --ratedown 1.015688
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_CR --processes ttbarOther --name UERate --rateup 0.987857 --ratedown 1.012292
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_CR --processes ttbarOther --name UERate --rateup 0.985091 --ratedown 1.015134


python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.972003 --ratedown 1.028803
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.971979 --ratedown 1.028829
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.97073 --ratedown 1.030152
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 1.019276 --ratedown 0.981088
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.977733 --ratedown 1.022774
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.959496 --ratedown 1.042213

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.924431 --ratedown 1.081746
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.945736 --ratedown 1.057378
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.925646 --ratedown 1.080327
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.935714 --ratedown 1.068702
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.941387 --ratedown 1.062262
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.93697 --ratedown 1.06727

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.955982 --ratedown 1.046044
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.94131 --ratedown 1.06235
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.908766 --ratedown 1.100393
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j7_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.963561 --ratedown 1.037817
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j8_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.939347 --ratedown 1.064569
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2017}/${INFILE2017}.root --cat fh_j9_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.926858 --ratedown 1.078914

hadd ${FOLDER2017}/${INFILE2017}_UEHDAMPRates.root ${FOLDER2017}/${INFILE2017}_rateAdded*.root
rm -v ${FOLDER2017}/${INFILE2017}_rateAdded*.root 

#####################################################################################################################################################################


python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.032839 --ratedown 0.968205
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.015681 --ratedown 0.984561
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_SR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.011554 --ratedown 0.988578
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.02239 --ratedown 0.9781
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 0.982337 --ratedown 1.017981
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_CR --processes ttbarPlusBBbarMerged --name UERate --rateup 1.005207 --ratedown 0.99482

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 0.97823 --ratedown 1.022255
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 1.023766 --ratedown 0.976785
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_SR --processes ttbarPlusCCbar --name UERate --rateup 0.981807 --ratedown 1.01853
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.987917 --ratedown 1.012231
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 1.017367 --ratedown 0.98293
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_CR --processes ttbarPlusCCbar --name UERate --rateup 0.982735 --ratedown 1.017568

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_SR --processes ttbarOther --name UERate --rateup 1.019091 --ratedown 0.981267
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_SR --processes ttbarOther --name UERate --rateup 1.029429 --ratedown 0.971412
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_SR --processes ttbarOther --name UERate --rateup 1.035208 --ratedown 0.965989
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_CR --processes ttbarOther --name UERate --rateup 0.995493 --ratedown 1.004527
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_CR --processes ttbarOther --name UERate --rateup 1.014708 --ratedown 0.985506
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_CR --processes ttbarOther --name UERate --rateup 0.99169 --ratedown 1.00838


python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.978469 --ratedown 1.022005
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.976511 --ratedown 1.024054
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_SR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.962244 --ratedown 1.039237
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 1.037536 --ratedown 0.963822
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.973649 --ratedown 1.027065
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_CR --processes ttbarPlusBBbarMerged --name HDAMPRate --rateup 0.975696 --ratedown 1.02491

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.935389 --ratedown 1.069074
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.953608 --ratedown 1.048649
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_SR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.935866 --ratedown 1.068529
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.925971 --ratedown 1.079948
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.929162 --ratedown 1.076239
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_CR --processes ttbarPlusCCbar --name HDAMPRate --rateup 0.931482 --ratedown 1.073557

python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.953271 --ratedown 1.04902
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.957658 --ratedown 1.044214
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_SR --processes ttbarOther --name HDAMPRate --rateup 0.950268 --ratedown 1.052335
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j7_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.957352 --ratedown 1.044548
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j8_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.935283 --ratedown 1.069196
python addRateUncertaintiesAsTemplate.py --input ${FOLDER2018}/${INFILE2018}.root --cat fh_j9_t4_CR --processes ttbarOther --name HDAMPRate --rateup 0.937724 --ratedown 1.066412

hadd ${FOLDER2018}/${INFILE2018}_UEHDAMPRates.root ${FOLDER2018}/${INFILE2018}_rateAdded*.root
rm -v ${FOLDER2018}/${INFILE2018}_rateAdded*.root 
