#!/bin/bash
BASEFOLDER=$1


python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarOther.root --systematic CMS_ttHbb_HDAMP --systematicDir Down 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarPlusBBbarMerged.root --systematic CMS_ttHbb_HDAMP --systematicDir Down 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_hdamp_down_ttbbMerged_ttbarPlusCCbar.root --systematic CMS_ttHbb_HDAMP --systematicDir Down 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarOther.root --systematic CMS_ttHbb_HDAMP --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarPlusBBbarMerged.root --systematic CMS_ttHbb_HDAMP --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_hdamp_up_ttbbMerged_ttbarPlusCCbar.root --systematic CMS_ttHbb_HDAMP --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_tune_down_ttbbMerged_ttbarOther.root --systematic CMS_ttHbb_UE --systematicDir Down 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_tune_down_ttbbMerged_ttbarPlusBBbarMerged.root --systematic CMS_ttHbb_UE --systematicDir Down 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_tune_down_ttbbMerged_ttbarPlusCCbar.root --systematic CMS_ttHbb_UE --systematicDir Down 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_tune_up_ttbbMerged_ttbarOther.root --systematic CMS_ttHbb_UE --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_tune_up_ttbbMerged_ttbarPlusBBbarMerged.root --systematic CMS_ttHbb_UE --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTIncl_tune_up_ttbbMerged_ttbarPlusCCbar.root --systematic CMS_ttHbb_UE --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTbb_hdamp_down_ttbbMerged_ttbarOther.root --systematic CMS_ttHbb_HDAMP --systematicDir Down
python renameVariatedSamples.py --input ${BASEFOLDER}/TTbb_hdamp_down_ttbbMerged_ttbarPlusBBbarMerged.root --systematic CMS_ttHbb_HDAMP --systematicDir Down
python renameVariatedSamples.py --input ${BASEFOLDER}/TTbb_hdamp_down_ttbbMerged_ttbarPlusCCbar.root --systematic CMS_ttHbb_HDAMP --systematicDir Down
python renameVariatedSamples.py --input ${BASEFOLDER}/TTbb_hdamp_up_ttbbMerged_ttbarOther.root --systematic CMS_ttHbb_HDAMP --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTbb_hdamp_up_ttbbMerged_ttbarPlusBBbarMerged.root --systematic CMS_ttHbb_HDAMP --systematicDir Up 
python renameVariatedSamples.py --input ${BASEFOLDER}/TTbb_hdamp_up_ttbbMerged_ttbarPlusCCbar.root --systematic CMS_ttHbb_HDAMP --systematicDir Up 



hadd -f ${BASEFOLDER}/TTbb_ttbarOther_systamtics.root  ${BASEFOLDER}/TTbb_*ttbarOther_renamed.root
hadd -f ${BASEFOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics.root  ${BASEFOLDER}/TTbb_*ttbarPlusBBbarMerged_renamed.root
hadd -f ${BASEFOLDER}/TTbb_ttbarPlusCCbar_systamtics.root  ${BASEFOLDER}/TTbb_*ttbarPlusCCbar_renamed.root

hadd -f ${BASEFOLDER}/TTIncl_ttbarOther_systamtics.root  ${BASEFOLDER}/TTIncl_*ttbarOther_renamed.root
hadd -f ${BASEFOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics.root  ${BASEFOLDER}/TTIncl_*ttbarPlusBBbarMerged_renamed.root
hadd -f ${BASEFOLDER}/TTIncl_ttbarPlusCCbar_systamtics.root  ${BASEFOLDER}/TTIncl_*ttbarPlusCCbar_renamed.root


