#!/bin/bash
FOLDER=$1
FOLDERMERGED=$2

python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ../data/templateBinning/j7_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ../data/templateBinning/j8_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ../data/templateBinning/j9_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1

python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ../data/templateBinning/j7_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ../data/templateBinning/j8_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1
python genOptimizedTemplates.py --input $FOLDERMERGED/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ../data/templateBinning/j9_t4_rebin_v3p3_60bins.json --binEdgeLow 0 --binEdgeHigh 1

hadd ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root 
hadd ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged*_DNN_Node0*.root 
python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics_rebinned.root \
       ${FOLDER}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics_rebinned.root \
       ${FOLDER}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root \
       ttbarPlusBBbarMerged
