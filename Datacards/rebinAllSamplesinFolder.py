import sys
import os
import glob
import logging
# Stuff from FHDowstram/Plotting
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/"))
from classes.PlotHelpers import initLogging

from genOptimizedTemplates import generateOptimizedTemplates



if __name__ == "__main__":
    folder = sys.argv[1]
    initLogging(20, funcLen = 25)

    
    rebins = [("fh_j7_t4","../data/templateBinning/j7_t4_rebin_v3p3_60bins.json"),
              ("fh_j8_t4","../data/templateBinning/j8_t4_rebin_v3p3_60bins.json"),
              ("fh_j9_t4","../data/templateBinning/j9_t4_rebin_v3p3_60bins.json")]

    files = glob.glob(folder+"/*.root")
    logging.info(files)
    for file_ in files:
        logging.info("Processing %s", file_)
        rebinnedFiles = []
        for cat, rebin in rebins:
            rebinnedFiles.append(
                generateOptimizedTemplates(inputFile = file_,
                                           rebinInfo = rebin,
                                           folder = folder,
                                           category = cat,
                                           tag = "",
                                           variable = "DNN_Node0")
            )

        haddCommand = "hadd {} {}".format(file_.replace(".root", "_rebinned.root"), " ".join(rebinnedFiles))
        logging.warning(haddCommand)
        os.system(haddCommand)

        rmCommand = "rm {}".format(" ".join(rebinnedFiles))
        logging.warning(rmCommand)
        os.system(rmCommand)
