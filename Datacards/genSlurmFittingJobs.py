from glob import glob
from copy import copy
import os
import sys

scriptTemplates = '#!/bin/bash \n'
scriptTemplates += '#  \n'
scriptTemplates += '#SBATCH -p standard  \n'
scriptTemplates += '#SBATCH --account=t3  \n'
scriptTemplates += '#SBATCH --job-name=%%NAMEPREFIX%%_%%PREFIX%%_%%FILEBASE%%  \n'
#scriptTemplates += '#SBATCH --mem=3000M                     # memory 3GB (per job)'
scriptTemplates += '#SBATCH --time 12:00:00 \n'
scriptTemplates += '#SBATCH -o output/%x-%j.out \n'
scriptTemplates += '#SBATCH -e output/%x-%j.err \n'
scriptTemplates += '\n'
scriptTemplates += 'echo HOME: $HOME \n'
scriptTemplates += 'echo PWD: $(pwd) \n'
scriptTemplates += 'echo USER: $USER \n'
scriptTemplates += 'echo SLURM_JOB_ID: $SLURM_JOB_ID \n'
scriptTemplates += 'echo HOSTNAME: $HOSTNAME \n'
scriptTemplates += '\n'
scriptTemplates += 'export COMBINECOMMAND="%%COMBINECOMMAND%%" \n'
scriptTemplates += '${COMBINECOMMAND} -n %%PREFIX%%_%%FILEBASE%% %%FILEBASE%%.root &> log_%%FILEBASE%%.txt \n'
scriptTemplates += 'EXITCODE=$? \n'
scriptTemplates += 'if [ $EXITCODE == "0" ] \n'
scriptTemplates += 'then \n'
scriptTemplates += '    echo SUCCESS \n'
scriptTemplates += 'else \n'
scriptTemplates += '   echo FAILED \n'
scriptTemplates += 'fi \n'

asimov= True
nameprefix = sys.argv[1]
fileSel = sys.argv[2]
rMax = "20"
prefix = "v2"
combineCommand = "combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 0.1 --cminDefaultMinimizerStrategy 0  --saveWithUncertainties --saveNormalizations --saveShapes --saveOverallShapes --rMin -"+rMax+" --rMax "+rMax+"  --skipBOnlyFit"

if asimov:
    combineCommand += " -t -1  --expectSignal 1"

os.system("mkdir output")
print "Seleciton: %s"%fileSel
for file_ in glob(fileSel):
    baseName = file_.split("/")[-1].replace(".root","")
    thisScript = copy(scriptTemplates)
    thisScript = thisScript.replace("%%COMBINECOMMAND%%", combineCommand)
    thisScript = thisScript.replace("%%PREFIX%%", prefix)
    thisScript = thisScript.replace("%%FILEBASE%%", baseName)
    thisScript = thisScript.replace("%%NAMEPREFIX%%", nameprefix)

    with open("slum_fit_"+prefix+"_"+baseName+".sh", "w") as f:
        f.write(thisScript)
