import sys, os
import logging
from copy import deepcopy

import ROOT

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

def removeVariable(inputFile, var, postfix):
    folder = "/".join(inputFile.split("/")[0:-1])
    logging.info("Operating folder: %s", folder)

    rFile = ROOT.TFile.Open(inputFile)

    logging.info("File: %s", rFile)
    
    allKeys = [x.GetName() for x in rFile.GetListOfKeys()]

    out_templates = []
    
    for key in allKeys:
        if var not in key:
            logging.debug("Will keep: %s", key)

            out_templates.append(
                deepcopy(
                    rFile.Get(key)
                )
            )

    outName = inputFile.split("/")[-1].replace(".root","")+"_removed_"+var+postfix+".root"
    logging.info("Will use output file %s", outName)
    outFile = ROOT.TFile(folder+"/"+outName, "RECREATE")
    outFile.cd()

    for template in out_templates:
        logging.info("Writing: %s (%s)", template.GetName(), template)
        template.Write()
            
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--input",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--var",
        action = "store",
        help = "Variable",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--postfix",
        action = "store",
        help = "Postfix for the output file",
        type = str,
        default = ""
    )
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    removeVariable(args.input, args.var, args.postfix)
