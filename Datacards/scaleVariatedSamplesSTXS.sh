#!/bin/bash

FOLDERTRF=$1
FOLDERNOM=$2

python scaleVariatedSamplesSTXS.py ${FOLDERTRF} ${FOLDERNOM} TTToHadronic
python scaleVariatedSamplesSTXS.py ${FOLDERTRF} ${FOLDERNOM} TTTo2L2Nu
python scaleVariatedSamplesSTXS.py ${FOLDERTRF} ${FOLDERNOM} TTToSemiLeptonic

python scaleVariatedSamplesSTXS.py ${FOLDERTRF} ${FOLDERNOM} TTbb_4f_TTTo2l2nu
python scaleVariatedSamplesSTXS.py ${FOLDERTRF} ${FOLDERNOM} TTbb_4f_TTToHadronic
python scaleVariatedSamplesSTXS.py ${FOLDERTRF} ${FOLDERNOM} TTbb_4f_TTToSemiLeptonic
