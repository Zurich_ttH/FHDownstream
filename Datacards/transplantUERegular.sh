#!/bin/bash
FOLDER=$1
TTINCFILE=$2
TTBBFILE=$3
POSTFIX=""
#POSTFIX="_rebinned"
#POSTFIX="_removed_DNN_Node0"

python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics${POSTFIX}.root \
       ${TTINCFILE} \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics${POSTFIX}.root \
       ${TTBBFILE} \
       ttbarPlusBBbarMerged
