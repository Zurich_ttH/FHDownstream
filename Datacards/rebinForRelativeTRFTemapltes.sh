#!/bin/bash
FOLDER=$1

# REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins.json"
# REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins.json"
# REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins.json"

REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner.json"
REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner.json"
REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner.json"

# REBININFO7="../data/templateBinning/j7_t4_rebin_v3p3_60bins_inner_lessleft.json"
# REBININFO8="../data/templateBinning/j8_t4_rebin_v3p3_60bins_inner_lessleft.json"
# REBININFO9="../data/templateBinning/j9_t4_rebin_v3p3_60bins_inner_lessleft.json"


PROCS=( ttbarOther ttbarPlusBBbarMerged ttbarPlusCCbar)

for PROC in "${PROCS[@]}"
do
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j7_t4 --rebinInfo ${REBININFO7} --binEdgeLow 0 --binEdgeHigh 1

    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j8_t4 --rebinInfo ${REBININFO8} --binEdgeLow 0 --binEdgeHigh 1

    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1
    python genOptimizedTemplates.py --input ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}.root --outFolder ${FOLDER} --cat fh_j9_t4 --rebinInfo ${REBININFO9} --binEdgeLow 0 --binEdgeHigh 1



    hadd -f ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_hdamp_down_ttbbMerged_${PROC}_*DNN_Node0*.root
    hadd -f ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_hdamp_up_ttbbMerged_${PROC}_*DNN_Node0*.root
    hadd -f ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_nom_ttbbMerged_${PROC}_*DNN_Node0*.root
    hadd -f ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_tune_down_ttbbMerged_${PROC}_*DNN_Node0*.root
    hadd -f ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTIncl_tune_up_ttbbMerged_${PROC}_*DNN_Node0*.root
    hadd -f ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTbb_hdamp_down_ttbbMerged_${PROC}_*DNN_Node0*.root
    hadd -f ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTbb_hdamp_up_ttbbMerged_${PROC}_*DNN_Node0*.root
    hadd -f ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}_rebinned.root ${FOLDER}/TTbb_nom_ttbbMerged_${PROC}_*DNN_Node0*.root


    rm -v ${FOLDER}/*DNN_Node0_fh*.root

    
done
