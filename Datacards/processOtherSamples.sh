#!/bin/bash
BASEFOLDER=$1

echo "python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs ST.root --outPostfix MergedPDF &> ST.log &"
python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs ST.root --outPostfix MergedPDF &> ST.log &
echo "python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THW.root --outPostfix MergedPDF --templatePostfix _tHW &> TH.log &"
python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THW.root --outPostfix MergedPDF --templatePostfix _tHW &> THW.log &
echo "python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THQ.root --outPostfix MergedPDF --templatePostfix _tHW &> TH.log &"
python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs THQ.root --outPostfix MergedPDF --templatePostfix _tHq --logging 10 &> THq.log &
echo "python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs ttH.root --outPostfix MergedPDF &> ttH.log &"
python calcPDFWeightFromMembers.py --folder ${BASEFOLDER} --inputs ttH.root --outPostfix MergedPDF &> ttH.log &
