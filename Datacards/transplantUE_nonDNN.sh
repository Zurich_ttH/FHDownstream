#!/bin/bash
FOLDER=$1
FOLDERINIT=$2

python makeVariationsUE.py \
       ${FOLDER}/TTIncl_ttbarPlusBBbarMerged_systamtics.root \
       ${FOLDERINIT}/TTIncl_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root \
       ${FOLDER}/TTbb_ttbarPlusBBbarMerged_systamtics.root \
       ${FOLDERINIT}/TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root \
       ttbarPlusBBbarMerged \
       DNN_Node0
