import ROOT
import sys, os
from TTH.MEAnalysis.samples_base import getSitePrefix

datatypes = sys.argv[2:]
print datatypes

ofname = sys.argv[1]
tt = ROOT.TChain("tree")
for fi in os.environ["FILE_NAMES"].split():
    print "adding", fi
    fn = getSitePrefix(fi)
    tf = ROOT.TFile.Open(fn)
    if not tf or tf.IsZombie():
        raise Exception("Could not open file: {0}".format(fn))
    tf.Close() 
    tt.AddFile(fn)

firstFile = os.environ["FILE_NAMES"].split()[0].split("/")
isBTagCSV = False
if "BTagCSV" in firstFile:
    isBTagCSV = True
    print "+----------------------------+"
    print "| This file is BTagCSV!!!!!! |"
    print "+----------------------------+"
    

tt.SetBranchStatus("*", False)
if "all" in datatypes:
    tt.SetBranchStatus("*", True)

tt.SetBranchStatus("is_*", True)
tt.SetBranchStatus("numJets*", True)
tt.SetBranchStatus("nB*", True)
tt.SetBranchStatus("n*", True)
tt.SetBranchStatus("ttCls", True)
tt.SetBranchStatus("run", True)
tt.SetBranchStatus("lumi", True)
tt.SetBranchStatus("event", True)
tt.SetBranchStatus("*eight*", True)
tt.SetBranchStatus("*Pileup*", True)
tt.SetBranchStatus("ttCls", True)
tt.SetBranchStatus("btag*Weight*", True)
tt.SetBranchStatus("puWeight*", True)
#tt.SetBranchStatus("changes_jet_category*", True)
tt.SetBranchStatus("passMETFilters", True)
tt.SetBranchStatus("HTXS_Higgs_pt", True)

if "mem" in datatypes:
    tt.SetBranchStatus("mem_*", True)
    tt.SetBranchStatus("nMatch*", True)
    tt.SetBranchStatus("nGen*", True)
    tt.SetBranchStatus("gen*", True)
    tt.SetBranchStatus("Gen*", True)#
    #tt.SetBranchStatus("btag_LR_*", True)
    tt.SetBranchStatus("tth_rho_*", True)

if "kinematics" in datatypes:
    tt.SetBranchStatus("Detaj*", True)
    tt.SetBranchStatus("jets_*", True)
    tt.SetBranchStatus("njets", True)
    tt.SetBranchStatus("leps_pt*", True)
    tt.SetBranchStatus("leps_eta*", True)
    tt.SetBranchStatus("leps_pdgId*", True)
    tt.SetBranchStatus("nleps", True)
    tt.SetBranchStatus("Wmass*", True)
    tt.SetBranchStatus("qg_LR*", True)
    tt.SetBranchStatus("*mass*", True)
    tt.SetBranchStatus("ht*", True)
    tt.SetBranchStatus("nPVs", True)
    tt.SetBranchStatus("qg*", True)
    tt.SetBranchStatus("*gen*", True)
    tt.SetBranchStatus("met_*", True)
    tt.SetBranchStatus("mjjmin*", True)
    tt.SetBranchStatus("min_dr*", True)
    
    
if "leptons" in datatypes:
    tt.SetBranchStatus("leps_*", True)
    tt.SetBranchStatus("nleps", True)
    
#tt.SetBranchStatus("*topCand*", True)
if "trigger" in datatypes:
    tt.SetBranchStatus("HLT_ttH*", True)
    tt.SetBranchStatus("HLT_BIT_HLT_PFHT*", True)
    tt.SetBranchStatus("HLT_BIT_HLT_Iso*", True)
    tt.SetBranchStatus("HLT_BIT_HLT_Ele*", True)
    tt.SetBranchStatus("trigger*", True)
    tt.SetBranchStatus("tr*", True)

if "alltrigger" in datatypes:
    tt.SetBranchStatus("HLT_*", True)

if "noSyst" in datatypes:
    tt.SetBranchStatus("*_Absolute*", False)
    tt.SetBranchStatus("*_Relative*", False)
    tt.SetBranchStatus("*_PileUpData*", False)
    tt.SetBranchStatus("*_PileUpPt*", False)
    tt.SetBranchStatus("*_PileUpData*", False)
    tt.SetBranchStatus("*_PileUpEnvelope*", False)
    tt.SetBranchStatus("*_PileUpMuZero*", False)
    tt.SetBranchStatus("*_SubTotal*", False)
    tt.SetBranchStatus("*_Total*", False)
    tt.SetBranchStatus("*_Time*", False)
    tt.SetBranchStatus("*_FlavorZ*", False)
    tt.SetBranchStatus("*_Fragmentation*", False)
    tt.SetBranchStatus("*_SinglePion*", False)
    tt.SetBranchStatus("*_FlavorP*", False)
    tt.SetBranchStatus("*_FlavorQ*", False)
    tt.SetBranchStatus("*_CorrelationG*", False)
    tt.SetBranchStatus("*_JERDown", False)
    tt.SetBranchStatus("*_JERUp", False)
    tt.SetBranchStatus("*_ScaleHEM*", False)
    

    
of = ROOT.TFile(ofname, "RECREATE")
of.cd()
#tt.CopyTree("(is_sl && ((numJets>=6 && nBCSVM>=2) || (numJets>=4 && nBCSVM>=3) || (numJets==5 && nBCSVM>=2))) || (is_dl && (numJets>=3 && nBCSVM >=2)) || (is_fh && (numJets>=4 && nBCSVM>=3))")
#tt.CopyTree("(is_sl && numJets>=4 && nBCSVM>=2) || (is_dl && numJets>=4 && nBCSVM>=2)")
#tt.CopyTree("(is_sl && numJets>=6)")
#tt.CopyTree("(is_sl || is_dl || is_fh)")


#tt.CopyTree("is_fh && HLT_ttH_FH")

if "YEAR" in os.environ.keys():
    eraID = os.environ["YEAR"]
    print "Using YEAR",eraID,"for skimming"
else:
    eraID = "2018"
    print "DID NOT FIND $YEAR. USING",eraID,"(default)"


if "SKIMPOWER" in os.environ.keys():
    skimpower = os.environ["SKIMPOWER"]
    print "Using skimpower", skimpower
else:
    skimpower = "Medium"
    print "Using default skimpowet:", skimpower

baseSel = "is_fh == 1 && Wmass4b >= 30 && Wmass4b < 250"
if skimpower == "Tight":
    baseSel += " && ht30 > 500 && (nBDeepCSVM >= 2 || nBDeepFlavM >= 2) && numJets>= 6 && jets_pt[5] >= 40"
elif skimpower == "Medium":
    baseSel += " && ht30 > 250 && (nBDeepCSVM >= 1 || nBDeepFlavM >= 1) && numJets>= 5"
elif skimpower == "Loose":
    baseSel += " && ht30 > 250 && numJets>= 5"
else:
    raise RuntimeError("Skimpower %s not supported"%skimpower)

    
if eraID == "2016":
    tt.CopyTree(baseSel)#TODO: ADD TRIGGER
elif eraID == "2017":
    if isBTagCSV:
        tt.CopyTree(baseSel+"&& (HLT_ttH_FH == 1 && HLT_BIT_HLT_PFHT1050 == 0)")
    else:
        tt.CopyTree(baseSel+"&& (HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)")
elif eraID == "2018":
    tt.CopyTree(baseSel)#TODO: ADD TRIGGER

        
#tt.CopyTree("is_fh == 1 && ht30 > 250 && (nBCSVM >= 1 || nBDeepCSVM >= 1 || nBDeepFlavM >= 1) && numJets>= 5")
    
#tt.CopyTree("is_fh == 1 && ht30 > 250 && nBCSVM >= 1 && numJets>= 5")

    

of.Write()
of.Close()
