import sys
import ROOT

def getTriggerEfficiency(year, mediumWP, outName, inputs, tagger = "btagDeepFlavB"):
    nanoEvents = ROOT.TChain("nanoAOD/Events")
    for file_ in inputs:
        print "Adding file: %s"%file_
        nanoEvents.Add(file_)

    print "Entires in chain %s"%nanoEvents.GetEntries()

    hNoTrigger = ROOT.TH1F("hNoTrigger","hNoTrigger",1,1,2)
    hBaseline = ROOT.TH1F("hBaseline","hBaseline",1,1,2)
    h6J1T = ROOT.TH1F("h6J1T","h6J1T",1,1,2)
    h6J2T = ROOT.TH1F("h6J2T","h6J2T",1,1,2)
    h4J3T = ROOT.TH1F("h4J3T","h4J3T",1,1,2)
    hAddTrigger = ROOT.TH1F("hAddTrigger","hAddTrigger",1,1,2)
    h6J1T2T = ROOT.TH1F("h6J1T2T","h6J1T2T",1,1,2)
    h6J1T2T4J3T = ROOT.TH1F("h6J1T2T4J3T","h6J1T2T4J3T",1,1,2)
    hAll = ROOT.TH1F("hAll","hAll",1,1,2)
    h6J1T4J3T = ROOT.TH1F("h6J1T4J3T","h6J1T4J3T",1,1,2)
    h6J2T4J3T = ROOT.TH1F("h6J2T4J3T","h6J2T4J3T",1,1,2)
    h6J1TAdd = ROOT.TH1F("h6J1TAdd","h6J1TAdd",1,1,2)
    h6J2TAdd = ROOT.TH1F("h6J2TAdd","h6J2TAdd",1,1,2)
    h4J3TAdd = ROOT.TH1F("h4J3TAdd","h4J3TAdd",1,1,2)


    baselineSel = "Sum$(Jet_pt * (Jet_pt >= 30 && abs(Jet_eta) < 2.4)) >= 500" # HT
    baselineSel += " && Sum$((Jet_pt >= 30 && abs(Jet_eta) < 2.4)) >= 6" # njets
    baselineSel += " && Jet_pt[5] > 40" # 6th jet pt
    baselineSel += " && Sum$(Jet_pt >= 30 && abs(Jet_eta) < 2.4 && Jet_{} > {}) >= 2".format(tagger, mediumWP) # nBtags
    baselineSel += " &&((Electron_pt >= 15 && abs(Electron_eta) < 2.5) == 0 && (Muon_pt >= 15 && abs(Muon_eta) < 2.4) == 0)" # Lepton veto


    HLT6J1T = None
    HLT6J2T = None
    HLT4J3T = None
    AddTriger = None

    if year == "2016":
        HLT6J1T = "HLT_PFHT450_SixJet40_BTagCSV_p056"
        HLT6J2T = "HLT_PFHT400_SixJet30_DoubleBTagCSV_p056"
        HLT4J3T = "0"
        AddTriger = "HLT_PFJet450"
    if year == "2017":
        HLT6J1T = "HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5"
        HLT6J2T = "HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2"
        HLT4J3T = "HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0"
        AddTriger = "HLT_PFHT1050"
    if year == "2018":
        HLT6J1T = "HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59"
        HLT6J2T = "HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94"
        HLT4J3T = "HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5"
        AddTriger = "HLT_PFHT1050"

    nanoEvents.Project("hNoTrigger", "1", "1")
    nanoEvents.Project("hBaseline", "1", baselineSel)
    nanoEvents.Project("h6J1T", "1", "{} == 1 && {}".format(HLT6J1T, baselineSel))
    nanoEvents.Project("h6J2T", "1", "{} == 1 && {}".format(HLT6J2T, baselineSel))
    nanoEvents.Project("h4J3T", "1", "{} == 1 && {}".format(HLT4J3T, baselineSel))
    nanoEvents.Project("hAddTrigger", "1", "{} == 1 && {}".format(AddTriger, baselineSel))
    nanoEvents.Project("h6J1T2T", "1", "({} == 1 || {} == 1) && {}".format(HLT6J1T, HLT6J2T, baselineSel))
    nanoEvents.Project("h6J1T2T4J3T", "1", "({} == 1 || {} == 1 || {} == 1) && {}".format(HLT6J1T, HLT6J2T, HLT4J3T, baselineSel))
    nanoEvents.Project("hAll", "1", "({} == 1 || {} == 1 || {} == 1 || {} == 1) && {}".format(HLT6J1T, HLT6J2T, HLT4J3T, AddTriger, baselineSel))
    nanoEvents.Project("h6J1T4J3T", "1", "({} == 1 || {} == 1) && {}".format(HLT6J1T, HLT4J3T, baselineSel))
    nanoEvents.Project("h6J2T4J3T", "1", "({} == 1 || {} == 1) && {}".format(HLT6J2T, HLT4J3T, baselineSel))
    nanoEvents.Project("h6J1TAdd", "1", "({} == 1 || {} == 1) && {}".format(HLT6J1T, AddTriger, baselineSel))
    nanoEvents.Project("h6J2TAdd", "1", "({} == 1 || {} == 1) && {}".format(HLT6J2T, AddTriger, baselineSel))
    nanoEvents.Project("h4J3TAdd", "1", "({} == 1 || {} == 1) && {}".format(HLT4J3T, AddTriger, baselineSel))


    rOut = ROOT.TFile(outName, "RECREATE")
    rOut.cd()

    hNoTrigger.Write()
    hBaseline.Write()
    h6J1T.Write()
    h6J2T.Write()
    h4J3T.Write()
    hAddTrigger.Write()
    h6J1T2T.Write()
    h6J1T2T4J3T.Write()
    hAll.Write()
    h6J1T4J3T.Write()
    h6J2T4J3T.Write()
    h6J1TAdd.Write()
    h6J2TAdd.Write()
    h4J3TAdd.Write()
    
    rOut.Close()
    
if __name__ == "__main__":
    year = sys.argv[1]
    mediumWP = sys.argv[2]
    outName = sys.argv[3]
    inputs = sys.argv[4:]

    getTriggerEfficiency(year, mediumWP, outName, inputs)
