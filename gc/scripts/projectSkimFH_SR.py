import ROOT
import sys, os
from TTH.MEAnalysis.samples_base import getSitePrefix

datatypes = sys.argv[2:]
print datatypes

ofname = sys.argv[1]
tt = ROOT.TChain("tree")
for fi in os.environ["FILE_NAMES"].split():
    print "adding", fi
    fn = getSitePrefix(fi)
    tf = ROOT.TFile.Open(fn)
    if not tf or tf.IsZombie():
        raise Exception("Could not open file: {0}".format(fn))
    tf.Close() 
    tt.AddFile(fn)

firstFile = os.environ["FILE_NAMES"].split()[0].split("/")
isBTagCSV = False
if "BTagCSV" in firstFile:
    isBTagCSV = True
    print "+----------------------------+"
    print "| This file is BTagCSV!!!!!! |"
    print "+----------------------------+"
    


of = ROOT.TFile(ofname, "RECREATE")
of.cd()
#tt.CopyTree("(is_sl && ((numJets>=6 && nBCSVM>=2) || (numJets>=4 && nBCSVM>=3) || (numJets==5 && nBCSVM>=2))) || (is_dl && (numJets>=3 && nBCSVM >=2)) || (is_fh && (numJets>=4 && nBCSVM>=3))")
#tt.CopyTree("(is_sl && numJets>=4 && nBCSVM>=2) || (is_dl && numJets>=4 && nBCSVM>=2)")
#tt.CopyTree("(is_sl && numJets>=6)")
#tt.CopyTree("(is_sl || is_dl || is_fh)")


#tt.CopyTree("is_fh && HLT_ttH_FH")

if "YEAR" in os.environ.keys():
    eraID = os.environ["YEAR"]
    print "Using YEAR",eraID,"for skimming"
else:
    eraID = "2018"
    print "DID NOT FIND $YEAR. USING",eraID,"(default)"

baseSel = "passMETFilters == 1 && is_fh == 1 && ht30>500 && jets_pt[5]>40 && nBDeepFlavM >= 4 && ((numJets == 7 && Wmass4b > 60 && Wmass4b < 100) || (numJets == 8 && Wmass4b > 60 && Wmass4b < 100) || (numJets >= 9 && Wmass4b > 72 && Wmass4b < 90))"
    
if eraID == "2016":
    tt.CopyTree(baseSel +"&& (HLT_ttH_FH == 1 || HLT_BIT_HLT_PFJet450 == 1)")#TODO: ADD TRIGGER
elif eraID == "2017":
    if isBTagCSV:
        tt.CopyTree(baseSel+"&& (HLT_ttH_FH == 1 && HLT_BIT_HLT_PFHT1050 == 0)")
    else:
        tt.CopyTree(baseSel+"&& (HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)")
elif eraID == "2018":
    tt.CopyTree(baseSel + "&& (HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)")#TODO: ADD TRIGGER

        
#tt.CopyTree("is_fh == 1 && ht30 > 250 && (nBCSVM >= 1 || nBDeepCSVM >= 1 || nBDeepFlavM >= 1) && numJets>= 5")
    
#tt.CopyTree("is_fh == 1 && ht30 > 250 && nBCSVM >= 1 && numJets>= 5")

    

of.Write()
of.Close()
