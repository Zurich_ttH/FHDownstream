import sys
import ROOT

def getTriggerEfficiency(year, mediumWP, outName, inputs, tagger = "btagDeepFlavB"):
    nanoEvents = ROOT.TChain("nanoAOD/Events")
    for file_ in inputs:
        print "Adding file: %s"%file_
        nanoEvents.Add(file_)

    print "Entires in chain %s"%nanoEvents.GetEntries()

    hNoTrigger = ROOT.TH1F("hNoTrigger","hNoTrigger",1,1,2)
    hTrigger = ROOT.TH1F("hTrigger","hTrigger",1,1,2)
    hLeptonVeto = ROOT.TH1F("hLeptonVeto","hLeptonVeto",1,1,2)
    hnJetCuts = ROOT.TH1F("hnJetCuts","hnJetCuts",1,1,2)
    hsixJetCut = ROOT.TH1F("hsixJetCut","hsixJetCut",1,1,2)
    hHTCut = ROOT.TH1F("hHTCut","hHTCut",1,1,2)
    hBTag = ROOT.TH1F("hBTag","hBTag",1,1,2)


    HTSel = "(Sum$(Jet_pt * (Jet_pt >= 30 && abs(Jet_eta) < 2.4)) >= 500)" # HT
    nJetsSelSel = "(Sum$((Jet_pt >= 30 && abs(Jet_eta) < 2.4)) >= 6)" # njets
    sixJetCutSel = "(Jet_pt[5] > 40)" # 6th jet pt
    BTagSel = " (Sum$(Jet_pt >= 30 && abs(Jet_eta) < 2.4 && Jet_{} > {}) >= 2)".format(tagger, mediumWP) # nBtags
    LeptonSel = "((Electron_pt >= 15 && abs(Electron_eta) < 2.5) == 0 && (Muon_pt >= 15 && abs(Muon_eta) < 2.4) == 0)" # Lepton veto


    HLT6J1T = None
    HLT6J2T = None
    HLT4J3T = None
    AddTriger = None

    if year == "2016":
        HLT6J1T = "HLT_PFHT450_SixJet40_BTagCSV_p056"
        HLT6J2T = "HLT_PFHT400_SixJet30_DoubleBTagCSV_p056"
        HLT4J3T = "0"
        AddTriger = "HLT_PFJet450"
    if year == "2017":
        HLT6J1T = "HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5"
        HLT6J2T = "HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2"
        HLT4J3T = "HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0"
        AddTriger = "HLT_PFHT1050"
    if year == "2018":
        HLT6J1T = "HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59"
        HLT6J2T = "HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94"
        HLT4J3T = "HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5"
        AddTriger = "HLT_PFHT1050"

        

        
    nanoEvents.Project("hNoTrigger", "1", "{}".format(LeptonSel))
    nanoEvents.Project("hTrigger", "1", "({} == 1 || {} == 1 || {} == 1 || {} == 1) && {}".format(HLT6J1T, HLT6J2T, HLT4J3T, AddTriger,
                                                                                                  LeptonSel))

    nanoEvents.Project("hLeptonVeto", "1", "({} == 1 || {} == 1 || {} == 1 || {} == 1) && {}".format(HLT6J1T, HLT6J2T, HLT4J3T,
                                                                                                     AddTriger,
                                                                                                     LeptonSel))

    nanoEvents.Project("hnJetCuts", "1", "({} == 1 || {} == 1 || {} == 1 || {} == 1) && {} && {}".format(HLT6J1T, HLT6J2T, HLT4J3T,
                                                                                                         AddTriger,
                                                                                                         LeptonSel,
                                                                                                         nJetsSelSel))
    
    nanoEvents.Project("hsixJetCut", "1", "({} == 1 || {} == 1 || {} == 1 || {} == 1) && {} && {} && {}".format(HLT6J1T, HLT6J2T, HLT4J3T,
                                                                                                                AddTriger,
                                                                                                                LeptonSel,
                                                                                                                nJetsSelSel,
                                                                                                                sixJetCutSel))
    
    nanoEvents.Project("hHTCut", "1", "({} == 1 || {} == 1 || {} == 1 || {} == 1) && {} && {} && {} && {}".format(HLT6J1T, HLT6J2T, HLT4J3T,
                                                                                                                  AddTriger,
                                                                                                                  LeptonSel,
                                                                                                                  nJetsSelSel,
                                                                                                                  sixJetCutSel,
                                                                                                                  HTSel))

    nanoEvents.Project("hBTag", "1", "({} == 1 || {} == 1 || {} == 1 || {} == 1) && {} && {} && {} && {} && {}".format(HLT6J1T, HLT6J2T, HLT4J3T,
                                                                                                                       AddTriger,
                                                                                                                       LeptonSel,
                                                                                                                       nJetsSelSel,
                                                                                                                       sixJetCutSel,
                                                                                                                       HTSel,
                                                                                                                       BTagSel))


    rOut = ROOT.TFile(outName, "RECREATE")
    rOut.cd()

    hNoTrigger.Write()
    hTrigger.Write()
    hLeptonVeto.Write()
    hnJetCuts.Write()
    hsixJetCut.Write()
    hHTCut.Write()
    hBTag.Write()

    
    rOut.Close()
    
if __name__ == "__main__":
    year = sys.argv[1]
    mediumWP = sys.argv[2]
    outName = sys.argv[3]
    inputs = sys.argv[4:]

    getTriggerEfficiency(year, mediumWP, outName, inputs)
