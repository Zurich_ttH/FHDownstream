#include "TFile.h"
#include "TH3F.h"
#include "CondFormats/BTauObjects/interface/BTagCalibration.h"
#include "CondTools/BTau/interface/BTagCalibrationReader.h"

class bTagSF {
 public:
  bTagSF(std::string SFFile );
  ~bTagSF( );

  float getbTagSF(int systID, int dirID, float pt, float eta, float deepcsv, int flav);
 private:
  BTagCalibration calib; 
  BTagCalibrationReader BCalibReader;
  std::set<std::string> shapeCorr_jes;
};

bTagSF::bTagSF(std::string SFFile){
  calib = BTagCalibration("DeepCSV", SFFile);
  std::vector<std::string> bSystematics = {
					   "up_lf", "down_lf",
					   "up_hf", "down_hf",
					   "up_hfstats1", "down_hfstats1",
					   "up_hfstats2", "down_hfstats2",
					   "up_lfstats1", "down_lfstats1",
					   "up_lfstats2", "down_lfstats2",
					   "up_cferr1", "down_cferr1",
					   "up_cferr2", "down_cferr2",
  };
  shapeCorr_jes = {
		   "up_jesAbsoluteMPFBias", "down_jesAbsoluteMPFBias",
		   "up_jesAbsoluteScale", "down_jesAbsoluteScale",
		   "up_jesAbsoluteStat", "down_jesAbsoluteStat",
		   "up_jesFlavorQCD", "down_jesFlavorQCD",
		   "up_jesFragmentation", "down_jesFragmentation",
		   "up_jesPileUpDataMC", "down_jesPileUpDataMC",
		   "up_jesPileUpPtBB", "down_jesPileUpPtBB",
		   "up_jesPileUpPtEC1", "down_jesPileUpPtEC1",
		   "up_jesPileUpPtEC2", "down_jesPileUpPtEC2",
		   "up_jesPileUpPtHF", "down_jesPileUpPtHF",
		   "up_jesPileUpPtRef", "down_jesPileUpPtRef",
		   "up_jesRelativeBal", "down_jesRelativeBal",
		   "up_jesRelativeFSR", "down_jesRelativeFSR",
		   "up_jesRelativeJEREC1", "down_jesRelativeJEREC1",
		   "up_jesRelativeJEREC2", "down_jesRelativeJEREC2",
		   "up_jesRelativeJERHF", "down_jesRelativeJERHF",
		   "up_jesRelativePtBB", "down_jesRelativePtBB",
		   "up_jesRelativePtEC1", "down_jesRelativePtEC1",
		   "up_jesRelativePtEC2", "down_jesRelativePtEC2",
		   "up_jesRelativePtHF", "down_jesRelativePtHF",
		   "up_jesRelativeStatFSR", "down_jesRelativeStatFSR",
		   "up_jesRelativeStatEC", "down_jesRelativeStatEC",
		   "up_jesRelativeStatHF", "down_jesRelativeStatHF",
		   "up_jesSinglePionECAL", "down_jesSinglePionECAL",
		   "up_jesSinglePionHCAL", "down_jesSinglePionHCAL",
		   "up_jesTimePtEta", "down_jesTimePtEta"
  };
  
  for( auto jesSys : shapeCorr_jes){
    bSystematics.push_back(jesSys);
  }
  
  std::string cSys = "central";
  
  BCalibReader = BTagCalibrationReader(BTagEntry::OP_RESHAPING, cSys, bSystematics);
 
  BCalibReader.load(calib,  BTagEntry::FLAV_B, "iterativefit");
  BCalibReader.load(calib,  BTagEntry::FLAV_C, "iterativefit");
  BCalibReader.load(calib,  BTagEntry::FLAV_UDSG, "iterativefit");
}

bTagSF::~bTagSF(){}

float bTagSF::getbTagSF(int systID, int dirID, float pt, float eta, float deepcsv, int flav){
  std::string systematic = "notSet";
  switch(systID){
  case 1 : systematic = "lf"; break; 
  case 2 : systematic = "hf"; break;
  case 3 : systematic = "hfstats1"; break;
  case 4 : systematic = "hfstats2"; break;
  case 5 : systematic = "lfstats1"; break;
  case 6 : systematic = "lfstats2"; break;
  case 7 : systematic = "cferr1"; break;
  case 8 : systematic = "cferr2"; break;
  case 9 : systematic = "jesAbsoluteMPFBias"; break;
  case 10 : systematic = "jesAbsoluteScale"; break;
  case 11 : systematic = "jesAbsoluteStat"; break;
  case 12 : systematic = "jesFlavorQCD"; break;
  case 13 : systematic = "jesFragmentation"; break;
  case 14 : systematic = "jesPileUpDataMC"; break;
  case 15 : systematic = "jesPileUpPtBB"; break;
  case 16 : systematic = "jesPileUpPtEC1"; break;
  case 17 : systematic = "jesPileUpPtEC2"; break;
  case 18 : systematic = "jesPileUpPtHF"; break;
  case 19 : systematic = "jesPileUpPtRef"; break;
  case 20 : systematic = "jesRelativeBal"; break;
  case 21 : systematic = "jesRelativeFSR"; break;
  case 22 : systematic = "jesRelativeJEREC1"; break;
  case 23 : systematic = "jesRelativeJEREC2"; break;
  case 24 : systematic = "jesRelativeJERHF"; break;
  case 25 : systematic = "jesRelativePtBB"; break;
  case 26 : systematic = "jesRelativePtEC1"; break;
  case 27 : systematic = "jesRelativePtEC2"; break;
  case 28 : systematic = "jesRelativePtHF"; break;
  case 29 : systematic = "jesRelativeStatFSR"; break;
  case 30 : systematic = "jesRelativeStatEC"; break;
  case 31 : systematic = "jesRelativeStatHF"; break;
  case 32 : systematic = "jesSinglePionECAL"; break;
  case 33 : systematic = "jesSinglePionHCAL"; break;
  case 34 : systematic = "jesTimePtEta"; break;
  case 35 : systematic = "central"; break;
  }

  std::string direction = "None";
  if(dirID == 1){ direction = "up_"; }
  else if(dirID == 2){ direction = "down_"; }
  else if(dirID == 0 && systematic == "central"){ direction = ""; }
  else {
    std::cout << "Something is wrong" << std::endl;
    return 1;
  }
  std::string thisSystematic = "";
  thisSystematic.append(direction);
  thisSystematic.append(systematic);
  //std::cout << thisSystematic << " " << systematic << "(" << systID << ") " << direction << "(" << dirID << ")" << std::endl;
  BTagEntry::JetFlavor BTV_flav;
  std::set<std::string> shapeCorr_bFlav({"central", "up_lf", "down_lf", "up_hfstats1", "down_hfstats1", "up_hfstats2", "down_hfstats2"});
  std::set<std::string> shapeCorr_cFlav({"central", "up_cferr1", "down_cferr1",  "up_cferr2", "down_cferr2"});
  std::set<std::string> shapeCorr_udsgFlav({"central", "up_hf", "down_hf", "up_lfstats1", "down_lfstats1", "up_lfstats2", "down_lfstats2"});
  float SF = 1;
  if (abs(flav) == 5 ){ //b-Jets
    BTV_flav = BTagEntry::FLAV_B;
    if (shapeCorr_bFlav.find(thisSystematic) == shapeCorr_bFlav.end() && shapeCorr_jes.find(thisSystematic)  == shapeCorr_jes.end()){
      thisSystematic = "central";
    }
  }
  else if (abs(flav) == 4 ){ //c-Jets
    BTV_flav = BTagEntry::FLAV_C;
    if (shapeCorr_cFlav.find(thisSystematic) == shapeCorr_cFlav.end() && shapeCorr_jes.find(thisSystematic)  == shapeCorr_jes.end()){
      thisSystematic = "central";
    }
  }
  else if (abs(flav) == 0 || abs(flav) == 1 || abs(flav) == 2 || abs(flav) == 3 || abs(flav) == 21 ){ //udsg-Jets
    BTV_flav = BTagEntry::FLAV_UDSG;
    if (shapeCorr_udsgFlav.find(thisSystematic) == shapeCorr_udsgFlav.end() && shapeCorr_jes.find(thisSystematic)  == shapeCorr_jes.end()){
      thisSystematic = "central";
    }
  }
  else {
    std::cout << "Unknown flavour" << std::endl;
    BTV_flav =  BTagEntry::FLAV_UDSG;
    return 1.0;
  }
  
  SF = BCalibReader.eval_auto_bounds(thisSystematic, BTV_flav, abs(eta), pt, deepcsv);
  
  return SF;
}
