import sys
import json


def parseCombineResult(combienOut, infoFile):
    print "Using combine ouput:", combienOut
    print "Using json:", infoFile
    resultLine = None
    with open(combienOut, "r") as cOut:
        lines = cOut.read().split("\n")
        for l in lines:
            print "===", l
            if l.startswith("Best fit r"):
                resultLine = l


    if resultLine is None:
        raise RuntimeError("Could not find result")

    resultLine = resultLine.replace("Best fit r:","").replace("(68% CL)","").split(" ")
    resultLine = [x for x in resultLine if x != ""]
    central = resultLine[0]
    up, down = resultLine[1].split("/")


    central = float(central)
    up = float(up)
    down = float(down)

    data = None
    with open(infoFile, "r") as j:
        data = json.load(j)


    data["Fit_mu"] = central
    data["Fit_err_up"] = up
    data["Fit_err_down"] = down
    with open(infoFile, "w") as j:
        json.dump(data, j, sort_keys=True, indent=4, separators=(',', ': '))

        
if __name__ == "__main__":
    parseCombineResult(sys.argv[1], sys.argv[2])
