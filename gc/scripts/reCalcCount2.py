#!/usr/bin/env python
"""Recalculates the number fo weighted gen events from the nanoAOD tree"""
import ROOT
import sys
from TTH.MEAnalysis.samples_base import getSitePrefix
import logging
import os
import math
import array

from TTH.Plotting.Datacards.MiscClasses import PUWeightProducer

LOG_MODULE_NAME = logging.getLogger(__name__)

def main(filenames, ofname, debug=False, reCalcPUWeight=False, year="2016", fillSysts=True):
    """
    Get the necessary weight from the nanoAOD file and calulates the the number of genevents with
    Sum of 1 * puWeight * sign(genWeight)
    """
    filenames_pref = map(getSitePrefix, filenames)
    if os.path.isfile(ofname):
        LOG_MODULE_NAME.info("opening existing file {0}".format(ofname))
        of = ROOT.TFile(ofname, "UPDATE")
    else:
        LOG_MODULE_NAME.info("creating new file {0}".format(ofname))
        of = ROOT.TFile(ofname, "RECREATE")
    tree_list = ROOT.TList()

    #keep tfiles open for the duration of the merge
    #tfiles = []

    print "FillSysts: %s"%fillSysts

    if reCalcPUWeight:
        if year == "2016":
            pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupData_GoldenJSON_Full2016.root" % os.environ['CMSSW_BASE']
        elif year == "2017":
            pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupHistogram-goldenJSON-13tev-2018-99bins_withVar.root" % os.environ['CMSSW_BASE']
        elif year == "2018":
            pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupHistogram-goldenJSON-13tev-2018-100bins_withVar.root" % os.environ['CMSSW_BASE']
        else:
            raise RuntimeError("Invalid PUweightEra: %s"%PUweightEra)
        puWeight = PUWeightProducer(filenames, pufile_data)

    
    hGen = ROOT.TH1F("hGen","hGen",1,1,2)
    hGenEvtEven = ROOT.TH1F("hGenEvtEven","hGenEvtEven",1,1,2)
    hGenEvtOdd = ROOT.TH1F("hGenEvtOdd","hGenEvtOdd",1,1,2)
    hGenWeighted = ROOT.TH1F("hGenWeighted","hGenWeighted",1,1,2)
    hGenEvtEvenWeighted = ROOT.TH1F("hGenEvtEvenWeighted","hGenEvtEvenWeighted",1,1,2)
    hGenEvtOddWeighted = ROOT.TH1F("hGenEvtOddWeighted","hGenEvtOddWeighted",1,1,2)
    hGenWeightednoPU = ROOT.TH1F("hGenWeightednoPU","hGenWeightednoPU",1,1,2)
    hGenWeightedonlyPU = ROOT.TH1F("hGenWeightedonlyPU","hGenWeightedonlyPU",1,1,2)

    hGenWeightedmuFUp = ROOT.TH1F("hGenWeightedmuFUp","hGenWeightedmuFUp",1,1,2)
    hGenWeightedmuFDown = ROOT.TH1F("hGenWeightedmuFDown","hGenWeightedmuFDown",1,1,2)
    hGenWeightedmuRUp = ROOT.TH1F("hGenWeightedmuRUp","hGenWeightedmuRUp",1,1,2)
    hGenWeightedmuRDown = ROOT.TH1F("hGenWeightedmuRDown","hGenWeightedmuRDown",1,1,2)

    hGenWeightedISRUp = ROOT.TH1F("hGenWeightedISRUp","hGenWeightedISRUp",1,1,2)
    hGenWeightedISRDown = ROOT.TH1F("hGenWeightedISRDown","hGenWeightedISRDown",1,1,2)
    hGenWeightedFSRUp = ROOT.TH1F("hGenWeightedFSRUp","hGenWeightedFSRUp",1,1,2)
    hGenWeightedFSRDown = ROOT.TH1F("hGenWeightedFSRDown","hGenWeightedFSRDown",1,1,2)

    
    hGenWeightedPDFMember = {}
    for i in range(103):
        hGenWeightedPDFMember["hGenWeightedPDFMember"+str(i)] = ROOT.TH1F("hGenWeightedPDFMember"+str(i),"hGenWeightedPDFMember"+str(i),1,1,2)
    
    httbb = ROOT.TH1F("httbb", "httbb", 2,0,2)
    fprocessed = 0
    for infn, lfn in zip(filenames_pref, filenames):
        LOG_MODULE_NAME.info("processing {0}".format(infn))
        tf = ROOT.TFile.Open(infn)
        #tfiles += [tf]

        #run_tree = tf.Get("Runs")
        #if not run_tree:
        #    run_tree = tf.Get("nanoAOD/Runs")
        
        evt_tree = tf.Get("Events")
        if not evt_tree:
            evt_tree = tf.Get("nanoAOD/Events")

        
        if reCalcPUWeight:
            LOG_MODULE_NAME.info("Calculating puweight")
            newPUWeightArray = array.array("f", [-1])
            branch = evt_tree.Branch("reCalcedPUWeight", newPUWeightArray, "reCalcedPUWeight/F")
            for iev in range(evt_tree.GetEntries()):
                if iev%10000 == 0:
                    LOG_MODULE_NAME.info("Event {0:10d}".format(iev))
                evt_tree.GetEvent(iev)
                val , _, _ =  puWeight.getWeight(evt_tree.Pileup_nTrueInt)
                newPUWeightArray[0] = float(val)
                #print newPUWeightArray[0]
                branch.Fill()

        evt_tree.GetEvent(0)
        try:
            nPDF = evt_tree.nLHEPdfWeight
        except AttributeError:
            nPDF = 0
        
        thishGen = hGen.Clone(hGen.GetName()+"_"+str(fprocessed))
        thishGenEvtEven = hGenEvtEven.Clone(hGenEvtEven.GetName()+"_"+str(fprocessed))
        thishGenEvtOdd = hGenEvtOdd.Clone(hGenEvtOdd.GetName()+"_"+str(fprocessed))
        thishGenWeighted = hGenWeighted.Clone(hGenWeighted.GetName()+"_"+str(fprocessed))
        thishGenEvtEvenWeighted = hGenEvtEvenWeighted.Clone(hGenEvtEvenWeighted.GetName()+"_"+str(fprocessed))
        thishGenEvtOddWeighted = hGenEvtOddWeighted.Clone(hGenEvtOddWeighted.GetName()+"_"+str(fprocessed))
        thishGenWeightednoPU = hGenWeightednoPU.Clone(hGenWeightednoPU.GetName()+"_"+str(fprocessed))
        thishGenWeightedonlyPU = hGenWeightedonlyPU.Clone(hGenWeightedonlyPU.GetName()+"_"+str(fprocessed))

        thishGenWeightedmuFUp  = hGenWeightedmuFUp.Clone(hGenWeightedmuFUp.GetName()+"_"+str(fprocessed))
        thishGenWeightedmuFDown  = hGenWeightedmuFDown.Clone(hGenWeightedmuFDown.GetName()+"_"+str(fprocessed))
        thishGenWeightedmuRUp  = hGenWeightedmuRUp.Clone(hGenWeightedmuRUp.GetName()+"_"+str(fprocessed))
        thishGenWeightedmuRDown  = hGenWeightedmuRDown.Clone(hGenWeightedmuRDown.GetName()+"_"+str(fprocessed))

        thishGenWeightedISRUp  = hGenWeightedISRUp.Clone(hGenWeightedISRUp.GetName()+"_"+str(fprocessed))
        thishGenWeightedISRDown  = hGenWeightedISRDown.Clone(hGenWeightedISRDown.GetName()+"_"+str(fprocessed))
        thishGenWeightedFSRUp  = hGenWeightedFSRUp.Clone(hGenWeightedFSRUp.GetName()+"_"+str(fprocessed))
        thishGenWeightedFSRDown  = hGenWeightedFSRDown.Clone(hGenWeightedFSRDown.GetName()+"_"+str(fprocessed))

        
        thishGenWeightedPDFMember = {}
        for i in range(103):
            thishGenWeightedPDFMember["thishGenWeightedPDFMember"+str(i)] = hGenWeightedPDFMember["hGenWeightedPDFMember"+str(i)].Clone(hGenWeightedPDFMember["hGenWeightedPDFMember"+str(i)].GetName()+"_"+str(fprocessed))
        
        thishttbb = httbb.Clone(httbb.GetName()+"_"+str(fprocessed))
        evt_tree.Project("hGen"+"_"+str(fprocessed),"1","1")
        evt_tree.Project("hGenEvtEven"+"_"+str(fprocessed),"1","(event%2==0)")
        evt_tree.Project("hGenEvtOdd"+"_"+str(fprocessed),"1","(event%2==1)")
        if reCalcPUWeight:
            evt_tree.Project("hGenWeighted"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight)")
            evt_tree.Project("hGenEvtEvenWeighted"+"_"+str(fprocessed),"1","(event%2==0) * (reCalcedPUWeight * sign(genWeight))")
            evt_tree.Project("hGenEvtOddWeighted"+"_"+str(fprocessed),"1","(event%2==1) * (reCalcedPUWeight * sign(genWeight))")
            evt_tree.Project("hGenWeightedonlyPU"+"_"+str(fprocessed),"1","reCalcedPUWeight")

            if fillSysts:
                evt_tree.Project("hGenWeightedmuFUp"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * LHEScaleWeight[5]")
                evt_tree.Project("hGenWeightedmuFDown"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * LHEScaleWeight[3]")
                evt_tree.Project("hGenWeightedmuRUp"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * LHEScaleWeight[7]")
                evt_tree.Project("hGenWeightedmuRDown"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * LHEScaleWeight[1]")

                evt_tree.Project("hGenWeightedISRUp"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * PSWeight[2]")
                evt_tree.Project("hGenWeightedISRDown"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * PSWeight[0]")
                evt_tree.Project("hGenWeightedFSRUp"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * PSWeight[3]")
                evt_tree.Project("hGenWeightedFSRDown"+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * PSWeight[1]")

                for i in range(nPDF):
                    evt_tree.Project("hGenWeightedPDFMember"+str(i)+"_"+str(fprocessed),"1","reCalcedPUWeight * sign(genWeight) * LHEPdfWeight["+str(i)+"]")
                
            
        else:
            evt_tree.Project("hGenWeighted"+"_"+str(fprocessed),"1","puWeight * sign(genWeight)")
            evt_tree.Project("hGenEvtEvenWeighted"+"_"+str(fprocessed),"1","(event%2==0) * (puWeight * sign(genWeight))")
            evt_tree.Project("hGenEvtOddWeighted"+"_"+str(fprocessed),"1","(event%2==1) * (puWeight * sign(genWeight))")
            evt_tree.Project("hGenWeightedonlyPU"+"_"+str(fprocessed),"1","puWeight")

            if fillSysts:
                evt_tree.Project("hGenWeightedmuFUp"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * LHEScaleWeight[5]")
                evt_tree.Project("hGenWeightedmuFDown"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * LHEScaleWeight[3]")
                evt_tree.Project("hGenWeightedmuRUp"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * LHEScaleWeight[7]")
                evt_tree.Project("hGenWeightedmuRDown"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * LHEScaleWeight[1]")

                evt_tree.Project("hGenWeightedISRUp"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * PSWeight[2]")
                evt_tree.Project("hGenWeightedISRDown"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * PSWeight[0]")
                evt_tree.Project("hGenWeightedFSRUp"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * PSWeight[3]")
                evt_tree.Project("hGenWeightedFSRDown"+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * PSWeight[1]")

                for i in range(nPDF):
                    # evt_tree.Project("hGenWeightedPDFMember"+str(i)+"_"+str(fprocessed),"1","sign(genWeight) * LHEPdfWeight["+str(i)+"]")
                    evt_tree.Project("hGenWeightedPDFMember"+str(i)+"_"+str(fprocessed),"1","puWeight * sign(genWeight) * LHEPdfWeight["+str(i)+"]")

            
        evt_tree.Project("hGenWeightednoPU"+"_"+str(fprocessed),"1","sign(genWeight)")
        evt_tree.Project("httbb"+"_"+str(fprocessed), "(genTtbarId%100)>=51","1")
        hGen.Add(thishGen)
        hGenEvtEven.Add(thishGenEvtEven)
        hGenEvtOdd.Add(thishGenEvtOdd)
        hGenWeighted.Add(thishGenWeighted)
        hGenEvtEvenWeighted.Add(thishGenEvtEvenWeighted)
        hGenEvtOddWeighted.Add(thishGenEvtOddWeighted)
        hGenWeightednoPU.Add(thishGenWeightednoPU)
        hGenWeightedonlyPU.Add(thishGenWeightedonlyPU)


        if fillSysts:
            hGenWeightedmuFUp.Add(thishGenWeightedmuFUp)
            hGenWeightedmuFDown.Add(thishGenWeightedmuFDown)
            hGenWeightedmuRUp.Add(thishGenWeightedmuRUp)
            hGenWeightedmuRDown.Add(thishGenWeightedmuRDown)
            
            hGenWeightedISRUp.Add(thishGenWeightedISRUp)
            hGenWeightedISRDown.Add(thishGenWeightedISRDown)
            hGenWeightedFSRUp.Add(thishGenWeightedFSRUp)
            hGenWeightedFSRDown.Add(thishGenWeightedFSRDown)
            
            for i in range(103):
                hGenWeightedPDFMember["hGenWeightedPDFMember"+str(i)].Add(thishGenWeightedPDFMember["thishGenWeightedPDFMember"+str(i)])
        
        httbb.Add(thishttbb)
        tf.Close()
        fprocessed += 1
    of.cd()
    #out_tree = ROOT.TTree.MergeTrees(tree_list)    
    LOG_MODULE_NAME.info("saving output")
    #out_tree.Write()
    hGen.Write()
    hGenEvtEven.Write()
    hGenEvtOdd.Write()
    hGenWeighted.Write()
    hGenEvtEvenWeighted.Write()
    hGenEvtOddWeighted.Write()
    hGenWeightednoPU.Write()
    hGenWeightedonlyPU.Write()

    if fillSysts:
        hGenWeightedmuFUp.Write()
        hGenWeightedmuFDown.Write()
        hGenWeightedmuRUp.Write()
        hGenWeightedmuRDown.Write()
        
        hGenWeightedISRUp.Write()
        hGenWeightedISRDown.Write()
        hGenWeightedFSRUp.Write()
        hGenWeightedFSRDown.Write()
        
        for i in range(103):
            hGenWeightedPDFMember["hGenWeightedPDFMember"+str(i)].Write()
    
    httbb.Write()
    if debug:
        LOG_MODULE_NAME.debug("Recalculated Values:")
        LOG_MODULE_NAME.debug("hGen : {0}".format(hGen.GetBinContent(1)))
        LOG_MODULE_NAME.debug("hGenWeighted : {0}".format(hGenWeighted.GetBinContent(1)))
        LOG_MODULE_NAME.debug("hGenWeightednoPU : {0}".format(hGenWeightednoPU.GetBinContent(1)))
        LOG_MODULE_NAME.debug("hGenWeightedonlyPU : {0}".format(hGenWeightedonlyPU.GetBinContent(1)))
        LOG_MODULE_NAME.debug("nanoAOD Values:")
        #countw = 0
        #countgen = 0
        #for ient in range(out_tree.GetEntries()):
        #    out_tree.GetEntry(ient)
        #    countw += out_tree.genEventSumw
        #    countgen += out_tree.genEventCount
        #LOG_MODULE_NAME.debug("genEventCount : {0}".format(countgen))
        #LOG_MODULE_NAME.debug("genEventCount : {0}".format(countw))
    
    
    of.Close()
    return ofname

if __name__ == "__main__":
    loglevel = 20
    logging.basicConfig(stream=sys.stdout, level=loglevel)
    filenames = sys.argv[2:]
    ofname = sys.argv[1]
    logging.info("Files to process: {0}".format(len(filenames)))
    logging.info("Outputfilename: "+ofname)
    debug = False
    if loglevel < 20:
        debug = True

    if "RERUNPUWEIGHT" in os.environ.keys():
        reCalcPUWeight = False if os.environ["RERUNPUWEIGHT"] == "0" else True        
    else:
        reCalcPUWeight = False
        print "No RERUNPUWEIGHT in env."
    print "Using reCalcPUWeight =", reCalcPUWeight
    if "YEAR" in os.environ.keys():
        eraID = os.environ["YEAR"]
        print "Using YEAR",eraID,"for skimming"
    else:
        eraID = "2018"
        print "DID NOT FIND $YEAR. USING",eraID,"(default)"

    print main(filenames, ofname, debug, reCalcPUWeight, eraID)
