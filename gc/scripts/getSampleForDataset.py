#Put SAMPLE=$(python getSampleForDataset.py) in bash script for gc config
import os

dataset = os.environ["DATASETPATH"]

splitDataset = dataset.split("_")
if len(splitDataset) > 1:
    print "%s"%splitDataset[0] # MC: Something like name_X_X_X
if dataset in ["JetHT", "BTagCSV"]:
    print "%s"%dataset # Data: JetHT or BTagCSV
