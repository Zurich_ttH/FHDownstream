import sys
import ROOT

def getTriggerEfficiency(year, mediumWP, outName, inputs, tagger = "btagDeepFlavB"):
    nanoEvents = ROOT.TChain("nanoAOD/Events")
    for file_ in inputs:
        print "Adding file: %s"%file_
        nanoEvents.Add(file_)

    print "Entires in chain %s"%nanoEvents.GetEntries()

    hNoTrigger = ROOT.TH1F("hNoTrigger","hNoTrigger",1,1,2)
    hNoTriggerNoSel = ROOT.TH1F("hNoTriggerNoSel","hNoTriggerNoSel",1,1,2)
   

    hSingleMuTrigger = ROOT.TH1F("hSingleMuTrigger","hSingleMuTrigger",1,1,2)
    h6J1T = ROOT.TH1F("h6J1T","h6J1T",1,1,2)
    h6J2T = ROOT.TH1F("h6J2T","h6J2T",1,1,2)
    h4J3T = ROOT.TH1F("h4J3T","h4J3T",1,1,2)
    hJetTag = ROOT.TH1F("hJetTag","hJetTag",1,1,2)
    hAddTrigger = ROOT.TH1F("hAddTrigger","hAddTrigger",1,1,2)
    hAll = ROOT.TH1F("hAll","hAll",1,1,2)
    
    h6J1TAndSingleMu = ROOT.TH1F("h6J1TAndSingleMu","h6J1TAndSingleMu",1,1,2)
    h6J2TAndSingleMu = ROOT.TH1F("h6J2TAndSingleMu","h6J2TAndSingleMu",1,1,2)
    h4J3TAndSingleMu = ROOT.TH1F("h4J3TAndSingleMu","h4J3TAndSingleMu",1,1,2)
    hJetTagAndSingleMu = ROOT.TH1F("hJetTagAndSingleMu","hJetTagAndSingleMu",1,1,2)
    hAddTriggerAndSingleMu = ROOT.TH1F("hAddTriggerAndSingleMu","hAddTriggerAndSingleMu",1,1,2)
    hAllAndSingleMu = ROOT.TH1F("hAllAndSingleMu","hAllAndSingleMu",1,1,2)

    hSingleMuTriggerNoSel = ROOT.TH1F("hSingleMuTriggerNoSel","hSingleMuTriggerNoSel",1,1,2)
    h6J1TNoSel = ROOT.TH1F("h6J1TNoSel","h6J1TNoSel",1,1,2)
    h6J2TNoSel = ROOT.TH1F("h6J2TNoSel","h6J2TNoSel",1,1,2)
    h4J3TNoSel = ROOT.TH1F("h4J3TNoSel","h4J3TNoSel",1,1,2)
    hJetTagNoSel = ROOT.TH1F("hJetTagNoSel","hJetTagNoSel",1,1,2)
    hAddTriggerNoSel = ROOT.TH1F("hAddTriggerNoSel","hAddTriggerNoSel",1,1,2)
    hAllNoSel = ROOT.TH1F("hAllNoSel","hAllNoSel",1,1,2)
    
    h6J1TAndSingleMuNoSel = ROOT.TH1F("h6J1TAndSingleMuNoSel","h6J1TAndSingleMuNoSel",1,1,2)
    h6J2TAndSingleMuNoSel = ROOT.TH1F("h6J2TAndSingleMuNoSel","h6J2TAndSingleMuNoSel",1,1,2)
    h4J3TAndSingleMuNoSel = ROOT.TH1F("h4J3TAndSingleMuNoSel","h4J3TAndSingleMuNoSel",1,1,2)
    hJetTagAndSingleMuNoSel = ROOT.TH1F("hJetTagAndSingleMuNoSel","hJetTagAndSingleMuNoSel",1,1,2)
    hAddTriggerAndSingleMuNoSel = ROOT.TH1F("hAddTriggerAndSingleMuNoSel","hAddTriggerAndSingleMuNoSel",1,1,2)
    hAllAndSingleMuNoSel = ROOT.TH1F("hAllAndSingleMuNoSel","hAllAndSingleMuNoSel",1,1,2)

    HLT6J1T = None
    HLT6J2T = None
    HLT4J3T = None
    AddTriger = None
    HLTBaseline = None
    muPTCut = None
    
    if year == "2016":
        HLT6J1T = "HLT_PFHT450_SixJet40_BTagCSV_p056"
        HLT6J2T = "HLT_PFHT400_SixJet30_DoubleBTagCSV_p056"
        HLT4J3T = "0"
        AddTriger = "HLT_PFJet450"
        HLTBaseline = "HLT_IsoMu24"
        muPTCut = "26"
    if year == "2017":
        HLT6J1T = "HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5"
        HLT6J2T = "HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2"
        HLT4J3T = "HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0"
        AddTriger = "HLT_PFHT1050"
        HLTBaseline = "HLT_IsoMu27"
        muPTCut = "29"
    if year == "2018":
        HLT6J1T = "HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59"
        HLT6J2T = "HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94"
        HLT4J3T = "HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5"
        AddTriger = "HLT_PFHT1050"
        HLTBaseline = "HLT_IsoMu27"
        muPTCut = "26"
        
    baselineSel  = "Sum$(Jet_pt * (Jet_pt >= 30 && abs(Jet_eta) < 2.4)) >= 500" # HT
    baselineSel += " && Sum$((Jet_pt >= 30 && abs(Jet_eta) < 2.4)) >= 6" # njets
    baselineSel += " && Jet_pt[5] > 40" # 6th jet pt
    baselineSel += " && Sum$(Jet_pt >= 30 && abs(Jet_eta) < 2.4 && Jet_{} > {}) >= 2".format(tagger, mediumWP) # nBtags
    #baselineSel += " &&((Electron_pt >= 15 && abs(Electron_eta) < 2.5) == 0 && (Muon_pt >= 15 && abs(Muon_eta) < 2.4) == 0)" # Lepton veto
    baselineSel += " && (Sum$((Electron_pt >= 15 && abs(Electron_eta) < 2.5)) == 0 && Sum$((Muon_pt >= 15 && Muon_pt < "+muPTCut+" && abs(Muon_eta) < 2.4)) == 0 &&  Sum$((Muon_pt >= "+muPTCut+" && abs(Muon_eta) < 2.4)) == 1)" # veto

    print(baselineSel)

    nanoEvents.Project("hNoTrigger", "1", "{}".format(baselineSel))
    
    nanoEvents.Project("hSingleMuTrigger","1","{} && {} == 1".format(baselineSel, HLTBaseline))
    nanoEvents.Project("h6J1T","1","{} && {} == 1".format(baselineSel, HLT6J1T))
    nanoEvents.Project("h6J2T","1","{} && {} == 1".format(baselineSel, HLT6J2T))
    nanoEvents.Project("h4J3T","1","{} && {} == 1".format(baselineSel, HLT4J3T))
    nanoEvents.Project("hJetTag","1","{} && ({} == 1 || {} == 1 || {} == 1)".format(baselineSel, HLT6J1T, HLT6J2T, HLT4J3T))
    nanoEvents.Project("hAddTrigger","1","{} && {} == 1".format(baselineSel, AddTriger))
    nanoEvents.Project("hAll","1","{} && ({} == 1 || {} == 1 || {} == 1 || {} == 1)".format(baselineSel, HLT6J1T, HLT6J2T, HLT4J3T, AddTriger))

    nanoEvents.Project("h6J1TAndSingleMu","1","{} && ({} == 1 && {} == 1)".format(baselineSel, HLT6J1T, HLTBaseline))
    nanoEvents.Project("h6J2TAndSingleMu","1","{} && ({} == 1 && {} == 1)".format(baselineSel, HLT6J2T, HLTBaseline))
    nanoEvents.Project("h4J3TAndSingleMu","1","{} && ({} == 1 && {} == 1)".format(baselineSel, HLT4J3T, HLTBaseline))
    nanoEvents.Project("hJetTagAndSingleMu","1","{} && (({} == 1 || {} == 1 || {} == 1) && {} == 1)".format(baselineSel, HLT6J1T, HLT6J2T, HLT4J3T, HLTBaseline))
    nanoEvents.Project("hAddTriggerAndSingleMu","1","{} && ({} == 1 && {} == 1)".format(baselineSel, AddTriger, HLTBaseline))
    nanoEvents.Project("hAllAndSingleMu","1","{}&& (({} == 1 || {} == 1 || {} == 1 || {} == 1) && {} == 1)".format(baselineSel,  HLT6J1T, HLT6J2T, HLT4J3T, AddTriger, HLTBaseline))



    nanoEvents.Project("hNoTriggerNoSel", "1", "{}".format("1"))
    
    nanoEvents.Project("hSingleMuTriggerNoSel","1","{} && {} == 1".format("1", HLTBaseline))
    nanoEvents.Project("h6J1TNoSel","1","{} && {} == 1".format("1", HLT6J1T))
    nanoEvents.Project("h6J2TNoSel","1","{} && {} == 1".format("1", HLT6J2T))
    nanoEvents.Project("h4J3TNoSel","1","{} && {} == 1".format("1", HLT4J3T))
    nanoEvents.Project("hJetTagNoSel","1","{} && ({} == 1 || {} == 1 || {} == 1)".format("1", HLT6J1T, HLT6J2T, HLT4J3T))
    nanoEvents.Project("hAddTriggerNoSel","1","{} && {} == 1".format("1", AddTriger))
    nanoEvents.Project("hAllNoSel","1","{} && ({} == 1 || {} == 1 || {} == 1 || {} == 1)".format("1", HLT6J1T, HLT6J2T, HLT4J3T, AddTriger))

    nanoEvents.Project("h6J1TAndSingleMuNoSel","1","{} && ({} == 1 && {} == 1)".format("1", HLT6J1T, HLTBaseline))
    nanoEvents.Project("h6J2TAndSingleMuNoSel","1","{} && ({} == 1 && {} == 1)".format("1", HLT6J2T, HLTBaseline))
    nanoEvents.Project("h4J3TAndSingleMuNoSel","1","{} && ({} == 1 && {} == 1)".format("1", HLT4J3T, HLTBaseline))
    nanoEvents.Project("hJetTagAndSingleMuNoSel","1","{} && (({} == 1 || {} == 1 || {} == 1) && {} == 1)".format("1", HLT6J1T, HLT6J2T, HLT4J3T, HLTBaseline))
    nanoEvents.Project("hAddTriggerAndSingleMuNoSel","1","{} && ({} == 1 && {} == 1)".format("1", AddTriger, HLTBaseline))
    nanoEvents.Project("hAllAndSingleMuNoSel","1","{}&& (({} == 1 || {} == 1 || {} == 1 || {} == 1) && {} == 1)".format("1",  HLT6J1T, HLT6J2T, HLT4J3T, AddTriger, HLTBaseline))


    


    rOut = ROOT.TFile(outName, "RECREATE")
    rOut.cd()


    hNoTrigger.Write()
    hNoTriggerNoSel.Write()

    hSingleMuTrigger.Write()
    h6J1T.Write()
    h6J2T.Write()
    h4J3T.Write()
    hJetTag.Write()
    hAddTrigger.Write()
    hAll.Write()
    
    h6J1TAndSingleMu.Write()
    h6J2TAndSingleMu.Write()
    h4J3TAndSingleMu.Write()
    hJetTagAndSingleMu.Write()
    hAddTriggerAndSingleMu.Write()
    hAllAndSingleMu.Write()

    hSingleMuTriggerNoSel.Write()
    h6J1TNoSel.Write()
    h6J2TNoSel.Write()
    h4J3TNoSel.Write()
    hJetTagNoSel.Write()
    hAddTriggerNoSel.Write()
    hAllNoSel.Write()
    
    h6J1TAndSingleMuNoSel.Write()
    h6J2TAndSingleMuNoSel.Write()
    h4J3TAndSingleMuNoSel.Write()
    hJetTagAndSingleMuNoSel.Write()
    hAddTriggerAndSingleMuNoSel.Write()
    hAllAndSingleMuNoSel.Write()
    
    rOut.Close()
    
if __name__ == "__main__":
    year = sys.argv[1]
    mediumWP = sys.argv[2]
    outName = sys.argv[3]
    inputs = sys.argv[4:]

    getTriggerEfficiency(year, mediumWP, outName, inputs)
