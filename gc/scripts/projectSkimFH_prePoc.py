import ROOT
import sys, os
from TTH.MEAnalysis.samples_base import getSitePrefix

datatypes = sys.argv[2:]
print datatypes

ofname = sys.argv[1]
tt = ROOT.TChain("tree")
for fi in os.environ["FILE_NAMES"].split():
    print "adding", fi
    fn = getSitePrefix(fi)
    tf = ROOT.TFile.Open(fn)
    if not tf or tf.IsZombie():
        raise Exception("Could not open file: {0}".format(fn))
    tf.Close() 
    tt.AddFile(fn)

firstFile = os.environ["FILE_NAMES"].split()[0].split("/")
isBTagCSV = False
if "BTagCSV" in firstFile:
    isBTagCSV = True
    print "+----------------------------+"
    print "| This file is BTagCSV!!!!!! |"
    print "+----------------------------+"
    

tt.SetBranchStatus("*", False)
tt.SetBranchStatus("*", True)
    
of = ROOT.TFile(ofname, "RECREATE")
of.cd()


if "YEAR" in os.environ.keys():
    eraID = os.environ["YEAR"]
    print "Using YEAR",eraID,"for skimming"
else:
    eraID = "2018"
    print "DID NOT FIND $YEAR. USING",eraID,"(default)"

baseSel = "ht30 > 500 && (nBDeepCSVM >= 2 || nBDeepFlavM >= 2) && numJets>= 7"
    
if eraID == "2016":
    tt.CopyTree(baseSel +"&& (HLT_ttH_FH == 1 || HLT_BIT_HLT_PFJet450 == 1)")
elif eraID == "2017":
    if isBTagCSV:
        tt.CopyTree(baseSel+"&& (HLT_ttH_FH == 1 && HLT_BIT_HLT_PFHT1050 == 0)")
    else:
        tt.CopyTree(baseSel+"&& (HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)")
elif eraID == "2018":
    tt.CopyTree(baseSel)#TODO: ADD TRIGGER

    

of.Write()
of.Close()
