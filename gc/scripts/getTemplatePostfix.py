import os
import sys

sampleName = os.environ['DATASETPATH']

if sys.argv[1] == "PDF":
    if sampleName.startswith("TTbb"):
        print "_ttbbNLO"
    elif sampleName.startswith("THQ"):
        print "_tHq"
    elif sampleName.startswith("THW"):
        print "_tHW"
    else:
        print ""
