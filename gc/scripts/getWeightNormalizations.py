#!/usr/bin/env python
""" Script for getting normalization factors for b-tag reweighting  """
import sys
import logging
import os
import math
import imp
from copy import copy
from time import sleep

import numpy as np
LOG_MODULE_NAME = logging.getLogger(__name__)

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/nTupleProcessing"))
from testbTagWeights import BtagWeightAnalyzer
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting"))
from classes.PlotHelpers import moveOverUnderFlow, initLogging

from TTH.Plotting.Datacards.MiscClasses import PUWeightProducer


class Jet(object):
    def __init__(self, pt, eta, bTagValue, hadronFlavour, puID, jetID, systs = [], systPts = []):
        self.pt = pt
        self.eta = eta
        self.bTagValue = bTagValue
        self.hadronFlavour = hadronFlavour
        self.puID = puID
        self.jetID = jetID
        self.systematics = systs
        self.systematicPts = {}
        for isyst, syst in enumerate(systs):
            self.systematicPts[syst] = systPts[isyst]

    def getPt(self, systematic):
        if systematic == "nominal":
            return self.pt
        elif systematic in self.systematicPts:
            return self.systematicPts[systematic]
        else:
            return self.pt

    def passesCut(self, systematic):
        """ Applies the baseline jet selection with tight+LepVeto Jet ID and loose puID """
        return (self.getPt(systematic) > 30 and abs(self.eta) < 2.4 and self.jetID >= 4 and (self.puID >= 4 or self.pt > 50))
        
    def __repr__(self):
        retVal = "pt: %s, eta: %s, btag %s, flav %s, puID %s, jetID %s"%(self.pt, self.eta, self.bTagValue, self.hadronFlavour, self.puID, self.jetID)
        if self.systematics:
            sysString = ""
            for sys in self.systematics:
                sysString += "%s = %s "%(sys, self.systematicPts[sys])
            retVal += "\n"+sysString
        return retVal

def convertToExternalName(systematicName):
    if systematicName == "central":
        return "nominal"
    if systematicName.startswith("up"):
        return systematicName.replace("up_","")+"Up"
    elif systematicName.startswith("down"):
        return systematicName.replace("down_","")+"Down"
    else:
        raise RuntimeError("Can not convert %s"%systematicName)
    
    
def btagNoramlization(filenames, ofname, mem_python_config, debug=False, reCalcPUWeight=False, year="2016"):
    """
    Get Yield with and w/o b-tag reweighting from nanoAOD --> No selection applied 
    """
    import ROOT
    ROOT.TH1.SetDefaultSumw2(True)

    LOG_MODULE_NAME.info("Leading config: %s",mem_python_config )
    meconf = imp.load_source("meconf", mem_python_config)
    from meconf import Conf as python_conf

    csvFile = python_conf.general["BTagSFFiles"]["DeepFlav"]
    allSystematics = python_conf.general["BTagSystematics"]
    bSystematics = python_conf.general["BTagSystematics_bFlav"]
    cSystematics = python_conf.general["BTagSystematics_cFlav"]
    lSystematics = python_conf.general["BTagSystematics_udsgFlav"]
    jesSysts = []
    
    for sdir in ["Up","Down"]:
        for syst in [x for x in allSystematics if x.startswith("jes")]:
            jesSysts.append(syst+sdir)

    runSF = True
    if runSF:
        analyzer = BtagWeightAnalyzer(csvFile, False, allSystematics, bSystematics, cSystematics, lSystematics, "deepjet")
    else:
        analyzer = None

    from TTH.MEAnalysis.samples_base import getSitePrefix
    filenames_pref = map(getSitePrefix, filenames)
    if os.path.isfile(ofname):
        LOG_MODULE_NAME.info("opening existing file {0}".format(ofname))
        of = ROOT.TFile(ofname, "UPDATE")
    else:
        LOG_MODULE_NAME.info("creating new file {0}".format(ofname))
        of = ROOT.TFile(ofname, "RECREATE")
    tree_list = ROOT.TList()

    weightBase = "btagSF_shape"
    weightBaseFW = "btagWeight_shape"

    
    weights = []
    hnames = {}
    for sys in allSystematics:
        if sys == "central":
            dirs = ["nom"]
        else:
            dirs = ["up", "down"]
        for i in dirs:
            weight = "{0}_{1}".format(i, sys)
            weights.append(weight)
            hnames[weight] = "{0}_{1}".format(i, sys)
            #print weight4Draw[weight]
    hnames["central"] = "central"

    EventsWeights = {}
    for weight in weights:
        EventsWeights[weight] = 1
    emptyEventWeights = copy(EventsWeights)


    hNumNJetsBase = ROOT.TH1F("hBaseNJets", "hBaseNJets", 12, 0 ,12)
    hNumAllJetsBase = ROOT.TH1F("hBaseAllJets","hBaseAllJets", 50, 30, 530)
    hNumJetsBase = {}
    hNumJetsBase[0] = ROOT.TH1F("hBaseJets0","hBaseJets0", 50, 30, 530)
    hNumJetsBase[1] = ROOT.TH1F("hBaseJets1","hBaseJets1", 30, 30, 430)
    hNumJetsBase[2] = ROOT.TH1F("hBaseJets2","hBaseJets2", 45, 30, 300)
    hNumJetsBase[3] = ROOT.TH1F("hBaseJets3","hBaseJets3", 38, 30, 220)
    hNumJetsBase[4] = ROOT.TH1F("hBaseJets4","hBaseJets4", 30, 30, 180)
    hNumJetsBase[5] = ROOT.TH1F("hBaseJets5","hBaseJets5", 20, 30, 150)

    hWeightBase = ROOT.TH1F("hWeightBase","hWeightBase",40,0,2)
    
    systsWdirs = ["central"]
    for sdir in ["up", "down"]:
        for syst in allSystematics:
            systsWdirs.append(sdir+"_"+syst)


    hNumNJets = {}
    hDenomNJets = {}

    hNumAllJets = {}
    hDenomAllJets = {}

    hNumJets = {}
    hDenomJets = {}
    for i in range(6):
        hNumJets[i] = {}
        hDenomJets[i] = {}

    hWeight = {}
    hWeightbOnly = {}
        
    for syst in systsWdirs:
        hNumNJets[syst] = hNumNJetsBase.Clone(hNumNJetsBase.GetName().replace("Base","Num")+"_"+hnames[syst])
        hDenomNJets[syst] = hNumNJetsBase.Clone(hNumNJetsBase.GetName().replace("Base","Denom")+"_"+hnames[syst])

        hNumAllJets[syst] = hNumAllJetsBase.Clone(hNumAllJetsBase.GetName().replace("Base","Num")+"_"+hnames[syst])
        hDenomAllJets[syst] = hNumAllJetsBase.Clone(hNumAllJetsBase.GetName().replace("Base","Denom")+"_"+hnames[syst])

        for i in range(6):
            hNumJets[i][syst] = hNumJetsBase[i].Clone(hNumJetsBase[i].GetName().replace("Base","Num")+"_"+hnames[syst])
            hDenomJets[i][syst] = hNumJetsBase[i].Clone(hNumJetsBase[i].GetName().replace("Base","Denom")+"_"+hnames[syst])

        hWeight[syst] = hWeightBase.Clone(hWeightBase.GetName().replace("Base","")+"_all_"+hnames[syst])
        hWeightbOnly[syst] = hWeightBase.Clone(hWeightBase.GetName().replace("Base","")+"_bOnly_"+hnames[syst])
        
            

    """ PUWEIGHT """
    if reCalcPUWeight:
        if year == "2016":
            pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupData_GoldenJSON_Full2016.root" % os.environ['CMSSW_BASE']
        elif year == "2017":
            pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupHistogram-goldenJSON-13tev-2018-99bins_withVar.root" % os.environ['CMSSW_BASE']
        elif year == "2018":
            pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupHistogram-goldenJSON-13tev-2018-100bins_withVar.root" % os.environ['CMSSW_BASE']
        else:
            raise RuntimeError("Invalid PUweightEra: %s"%PUweightEra)
        puWeight = PUWeightProducer(filenames, pufile_data)


        
    iFile = 0
    for infn, lfn in zip(filenames_pref, filenames):
        LOG_MODULE_NAME.info("processing {0}".format(infn))
        tf = ROOT.TFile.Open(infn)
        #tfiles += [tf]

        #run_tree = tf.Get("Runs")
        #if not run_tree:
        #    run_tree = tf.Get("nanoAOD/Runs")

        evt_tree = tf.Get("Events")
        if not evt_tree:
            evt_tree = tf.Get("nanoAOD/Events")

        logging.debug("Creating temp histos for file %s", iFile)
        htmpNumNJets = {}
        htmpDenomNJets = {}

        htmpNumAllJets = {}
        htmpDenomAllJets = {}

        htmpNumJets = {}
        htmpDenomJets = {}
        for i in range(6):
            htmpNumJets[i] = {}
            htmpDenomJets[i] = {}

        htmpWeight = {}
        htmpWeightbOnly = {}

        for syst in systsWdirs:
            htmpNumNJets[syst] = hNumNJetsBase.Clone(hNumNJetsBase.GetName().replace("Base","Num")+"_"+hnames[syst]+"_"+str(iFile))
            htmpDenomNJets[syst] = hNumNJetsBase.Clone(hNumNJetsBase.GetName().replace("Base","Denom")+"_"+hnames[syst]+"_"+str(iFile))

            htmpNumAllJets[syst] = hNumAllJetsBase.Clone(hNumAllJetsBase.GetName().replace("Base","Num")+"_"+hnames[syst]+"_"+str(iFile))
            htmpDenomAllJets[syst] = hNumAllJetsBase.Clone(hNumAllJetsBase.GetName().replace("Base","Denom")+"_"+hnames[syst]+"_"+str(iFile))

            for i in range(6):
                htmpNumJets[i][syst] = hNumJetsBase[i].Clone(hNumJetsBase[i].GetName().replace("Base","Num")+"_"+hnames[syst]+"_"+str(iFile))
                htmpDenomJets[i][syst] = hNumJetsBase[i].Clone(hNumJetsBase[i].GetName().replace("Base","Denom")+"_"+hnames[syst]+"_"+str(iFile))

            htmpWeight[syst] = hWeightBase.Clone(hWeightBase.GetName().replace("Base","")+"_all_"+hnames[syst]+"_"+str(iFile))
            htmpWeightbOnly[syst] = hWeightBase.Clone(hWeightBase.GetName().replace("Base","")+"_bOnly_"+hnames[syst]+"_"+str(iFile))


                
        nEvents = evt_tree.GetEntries()
        logging.info("Tree has %s entries", nEvents)
        lenvar = "nJet"
        brPrefix = "Jet"
        kinVariables = ["pt","eta","hadronFlavour"]
        idVariables = ["puId","jetId"]
        bDiscName = "btagDeepFlavB"
        for iev in range(nEvents):
            if iev%1000 == 0:
                logging.info("Processing event %s/%s",iev, nEvents)
            evt_tree.GetEvent(iev)
            nJets =evt_tree.__getattr__(lenvar)
            jets = []
            for i in range(nJets):
                jets.append( Jet(
                    evt_tree.__getattr__(brPrefix+"_"+kinVariables[0]+"_nom")[i],
                    evt_tree.__getattr__(brPrefix+"_"+kinVariables[1])[i],
                    evt_tree.__getattr__(brPrefix+"_"+bDiscName)[i],
                    evt_tree.__getattr__(brPrefix+"_"+kinVariables[2])[i],
                    evt_tree.__getattr__(brPrefix+"_"+idVariables[0])[i],
                    evt_tree.__getattr__(brPrefix+"_"+idVariables[1])[i],
                    jesSysts,
                    [evt_tree.__getattr__(brPrefix+"_"+kinVariables[0]+"_"+x)[i] for x in jesSysts]
                ))

                
            if runSF:
                evtCorrections = analyzer.clacSF(jets)
            else:
                evtCorrections = {}
                evtCorrections["central"] = 1.0
                for sdir in ["up", "down"]:
                    for syst in allSystematics:
                        thisSyst = sdir+"_"+syst
                        evtCorrections[thisSyst] = 1.0
                    
            if reCalcPUWeight:
                LOG_MODULE_NAME.debug("Calculating puweight")
                thispuWeight, _, _ =  puWeight.getWeight(evt_tree.Pileup_nTrueInt)
            else:
                thispuWeight = evt_tree.puWeight

            genWeight = np.sign(evt_tree.genWeight)
            

            logging.debug("PUWeight = %s | genWeight = %s | central b = %s", thispuWeight, genWeight, evtCorrections["central"])
            logging.debug("nJets = %s", len([x for x in jets if (x.pt > 30 and abs(x.eta) < 2.4)]))
            for sdir in ["up", "down"]:
                for syst in allSystematics:
                    logging.debug("%s = %s", syst, evtCorrections[sdir+"_"+syst])

            logging.debug("Filling histograms")
            for sys in systsWdirs:
                # print "------ Pre ( "+sys+" ) ------"
                # for iJet, jet in enumerate(jets):
                #     print iJet, jet.getPt(convertToExternalName(sys))
                systJets = sorted([x.getPt(convertToExternalName(sys)) for x in jets if x.passesCut(convertToExternalName(sys))], reverse=True)
                # print "------ Post ( "+sys+" ) ------"
                # for iJet, jetpT in enumerate(systJets):
                #     print iJet, jetpT

                htmpNumNJets[sys].Fill(len(systJets), genWeight*thispuWeight)
                htmpDenomNJets[sys].Fill(len(systJets), genWeight*thispuWeight*evtCorrections[sys])

                for iJet, pt in enumerate(systJets):
                    htmpNumAllJets[sys].Fill(pt,genWeight*thispuWeight)
                    htmpDenomAllJets[sys].Fill(pt,genWeight*thispuWeight*evtCorrections[sys])
                    if iJet in htmpNumJets.keys():
                        htmpNumJets[iJet][sys].Fill(pt,genWeight*thispuWeight)
                        htmpDenomJets[iJet][sys].Fill(pt,genWeight*thispuWeight*evtCorrections[sys])

                htmpWeight[sys].Fill(genWeight*thispuWeight)
                htmpWeightbOnly[sys].Fill(genWeight*thispuWeight*evtCorrections[sys])
            
            
        #Add histo from this file to total histos
        for sys in systsWdirs:
            hNumNJets[sys].Add(htmpNumNJets[sys])
            hDenomNJets[sys].Add(htmpDenomNJets[sys])

            hNumAllJets[sys].Add(htmpNumAllJets[sys])
            hDenomAllJets[sys].Add(htmpDenomAllJets[sys])
            for key in htmpNumJets.keys():
                hNumJets[key][sys].Add(htmpNumJets[key][sys])
                hDenomJets[key][sys].Add(htmpDenomJets[key][sys])

            hWeight[sys].Add(htmpWeight[sys])
            hWeightbOnly[sys].Add(htmpWeightbOnly[sys])
        
        
        tf.Close()
        iFile += 1
    of.cd()        
    #out_tree = ROOT.TTree.MergeTrees(tree_list)    
    LOG_MODULE_NAME.info("saving output")
    for sys in systsWdirs:
        logging.info("Moving overflow")
        moveOverUnderFlow(hNumNJets[sys], True, True)
        moveOverUnderFlow(hDenomNJets[sys], True, True)

        moveOverUnderFlow(hNumAllJets[sys], True, True)
        moveOverUnderFlow(hDenomAllJets[sys], True, True)
        for key in hNumJets.keys():
            moveOverUnderFlow(hNumJets[key][sys], True, True)
            moveOverUnderFlow(hDenomJets[key][sys], True, True)

        moveOverUnderFlow(hWeight[sys], True, True)
        moveOverUnderFlow(hWeightbOnly[sys], True, True)
        
        logging.info("Saving %s", hNumNJets[sys].GetName())
        
        hNumNJets[sys].Write()
        hDenomNJets[sys].Write()

        hNumAllJets[sys].Write()
        hDenomAllJets[sys].Write()
        for key in hNumJets.keys():
            hNumJets[key][sys].Write()
            hDenomJets[key][sys].Write()

        hWeight[sys].Write()
        hWeightbOnly[sys].Write()
    of.Close()
    return ofname

if __name__ == "__main__":
    loglevel = 20
    #logging.basicConfig(stream=sys.stdout, level=loglevel)
    initLogging(loglevel, 30)
    filenames = sys.argv[2:]
    ofname = sys.argv[1]
    logging.info("Files to process: {0}".format(len(filenames)))
    logging.info("Outputfilename: "+ofname)
    debug = False
    if loglevel < 20:
        debug = True

    if "ANALYSISCONFIG" in os.environ.keys():
        configPath = os.environ["ANALYSISCONFIG"]
    else:
        raise RuntimeError("$ANALYSISCONFIG not set. Please set! (Need path to python config)")

    if "RERUNPUWEIGHT" in os.environ.keys():
        reCalcPUWeight = False if os.environ["RERUNPUWEIGHT"] == "0" else True        
    else:
        reCalcPUWeight = True
        print "No RERUNPUWEIGHT in env."
    if "YEAR" in os.environ.keys():
        eraID = os.environ["YEAR"]
        logging.info("Using YEAR %s", eraID)
    else:
        eraID = "2018"
        logging.info("DID NOT FIND $YEAR. USING %s (default)", eraID)

    print btagNoramlization(filenames, ofname, configPath, debug, reCalcPUWeight, eraID)
