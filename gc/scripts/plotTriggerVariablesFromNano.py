import sys
import ROOT

def plotTriggerVariablesFromNano(mediumWP, outName, inputs, tagger = "btagDeepFlavB"):

    nanoEvents = ROOT.TChain("nanoAOD/Events")
    for file_ in inputs:
        print "Adding file: %s"%file_
        nanoEvents.Add(file_)

    print "Entires in chain %s"%nanoEvents.GetEntries()

    hHT = ROOT.TH1F("hHT", "hHT", 90, 0, 1800)
    hHTLoose = ROOT.TH1F("hHTLoose", "hHTLoose", 90, 0, 1800)
    hpt = ROOT.TH1F("hpT", "hpT", 47, 30, 500)
    hEta = ROOT.TH1F("hEta", "hEta", 42, -4.2, 4.2)
    hpt0 = ROOT.TH1F("hpT0", "hpT0", 60, 30, 630)
    hpt1 = ROOT.TH1F("hpT1", "hpT1", 50, 30, 530)
    hpt2 = ROOT.TH1F("hpT2", "hpT2", 27, 30, 300)
    hpt3 = ROOT.TH1F("hpT3", "hpT3", 24, 30, 270)
    hpt5 = ROOT.TH1F("hpT5", "hpT5", 26, 30, 160)
    hNJets = ROOT.TH1F("hNJets", "hNJets", 13, 0, 13)
    hNBTags = ROOT.TH1F("hNBTags", "hNBTags", 6, 0, 6)


    nanoEvents.Project("hHTLoose", "Sum$(Jet_pt * (Jet_pt >= 15 && abs(Jet_eta) < 4.7))")
    nanoEvents.Project("hHT", "Sum$(Jet_pt * (Jet_pt >= 30 && abs(Jet_eta) < 2.4))")
    nanoEvents.Project("hNJets", "Sum$(Jet_pt >= 30 && abs(Jet_eta) < 2.4)")
    nanoEvents.Project("hNBTags", "Sum$(Jet_pt >= 30 && abs(Jet_eta) < 2.4 && Jet_{} > {})".format(tagger, mediumWP))
    nanoEvents.Project("hpT", "Jet_pt", "Jet_pt >= 30 && abs(Jet_eta) < 2.4")
    nanoEvents.Project("hEta", "Jet_eta", "Jet_pt >= 30")
    nanoEvents.Project("hpT0", "Jet_pt[0]", "Jet_pt[0] >= 30 && abs(Jet_eta[0]) < 2.4")
    nanoEvents.Project("hpT1", "Jet_pt[1]", "Jet_pt[1] >= 30 && abs(Jet_eta[1]) < 2.4")
    nanoEvents.Project("hpT2", "Jet_pt[2]", "Jet_pt[2] >= 30 && abs(Jet_eta[2]) < 2.4")
    nanoEvents.Project("hpT3", "Jet_pt[3]", "Jet_pt[3] >= 30 && abs(Jet_eta[3]) < 2.4")
    nanoEvents.Project("hpT5", "Jet_pt[5]", "Jet_pt[5] >= 30 && abs(Jet_eta[5]) < 2.4")
    
    simpleLeptonVeto = "((Electron_pt >= 15 && abs(Electron_eta) < 2.5) == 0 && (Muon_pt >= 15 && abs(Muon_eta) < 2.4) == 0)"


    hHT_fh = ROOT.TH1F("hHT_fh", "hHT_fh", 90, 0, 1800)
    hHTLoose_fh = ROOT.TH1F("hHTLoose_fh", "hHTLoose_fh", 90, 0, 1800)
    hpt_fh = ROOT.TH1F("hpT_fh", "hpT_fh", 47, 30, 500)
    hEta_fh = ROOT.TH1F("hEta_fh", "hEta_fh", 42, -4.2, 4.2)
    hpt0_fh = ROOT.TH1F("hpT0_fh", "hpT0_fh", 60, 30, 630)
    hpt1_fh = ROOT.TH1F("hpT1_fh", "hpT1_fh", 50, 30, 530)
    hpt2_fh = ROOT.TH1F("hpT2_fh", "hpT2_fh", 27, 30, 300)
    hpt3_fh = ROOT.TH1F("hpT3_fh", "hpT3_fh", 24, 30, 270)
    hpt5_fh = ROOT.TH1F("hpT5_fh", "hpT5_fh", 26, 30, 160)
    hNJets_fh = ROOT.TH1F("hNJets_fh", "hNJets_fh", 13, 0, 13)
    hNBTags_fh = ROOT.TH1F("hNBTags_fh", "hNBTags_fh", 6, 0, 6)


    nanoEvents.Project("hHTLoose_fh",
                       "Sum$(Jet_pt * (Jet_pt >= 15 && abs(Jet_eta) < 4.7))",
                       simpleLeptonVeto)
    nanoEvents.Project("hHT_fh",
                       "Sum$(Jet_pt * (Jet_pt >= 30 && abs(Jet_eta) < 2.4))",
                       simpleLeptonVeto)
    nanoEvents.Project("hNJets_fh",
                       "Sum$(Jet_pt >= 30 && abs(Jet_eta) < 2.4)",
                       simpleLeptonVeto)
    nanoEvents.Project("hNBTags_fh",
                       "Sum$(Jet_pt >= 30 && abs(Jet_eta) < 2.4 && Jet_{} > {})".format(tagger, mediumWP),
                       simpleLeptonVeto)
    nanoEvents.Project("hpT_fh",
                       "Jet_pt",
                       "Jet_pt >= 30 && abs(Jet_eta) < 2.4 &&"+simpleLeptonVeto)
    nanoEvents.Project("hEta_fh",
                       "Jet_eta",
                       "Jet_pt >= 30 &&"+simpleLeptonVeto)
    nanoEvents.Project("hpT0_fh",
                       "Jet_pt[0]",
                       "Jet_pt[0] >= 30 && abs(Jet_eta[0]) < 2.4 &&"+simpleLeptonVeto)
    nanoEvents.Project("hpT1_fh",
                       "Jet_pt[1]",
                       "Jet_pt[1] >= 30 && abs(Jet_eta[1]) < 2.4 &&"+simpleLeptonVeto)
    nanoEvents.Project("hpT2_fh",
                       "Jet_pt[2]",
                       "Jet_pt[2] >= 30 && abs(Jet_eta[2]) < 2.4 &&"+simpleLeptonVeto)
    nanoEvents.Project("hpT3_fh",
                       "Jet_pt[3]",
                       "Jet_pt[3] >= 30 && abs(Jet_eta[3]) < 2.4 &&"+simpleLeptonVeto)
    nanoEvents.Project("hpT5_fh",
                       "Jet_pt[5]",
                       "Jet_pt[5] >= 30 && abs(Jet_eta[5]) < 2.4 &&"+simpleLeptonVeto)

    
    
    
    rOut = ROOT.TFile(outName, "RECREATE")
    rOut.cd()

    hHT.Write()
    hHTLoose.Write()
    hpt.Write()
    hEta.Write()
    hpt0.Write()
    hpt1.Write()
    hpt2.Write()
    hpt3.Write()
    hpt5.Write()
    hNJets.Write()
    hNBTags.Write()

    hHT_fh.Write()
    hHTLoose_fh.Write()
    hpt_fh.Write()
    hEta_fh.Write()
    hpt0_fh.Write()
    hpt1_fh.Write()
    hpt2_fh.Write()
    hpt3_fh.Write()
    hpt5_fh.Write()
    hNJets_fh.Write()
    hNBTags_fh.Write()


    
    rOut.Close()


    
    
if __name__ == "__main__":
    mediumWP = sys.argv[1]
    outName = sys.argv[2]
    inputs = sys.argv[3:]

    plotTriggerVariablesFromNano(mediumWP, outName, inputs)
