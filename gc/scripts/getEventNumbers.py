from __future__ import print_function

import sys
import os
import time

import logging
import ROOT

from TTH.MEAnalysis.samples_base import getSitePrefix, get_prefix_sample, TRIGGERPATH_MAP
from TTH.Plotting.joosep.sparsinator import createEvent
varConversion = {
    'is_fh' : '',
    'ht30' : '',
    'jetsByPt_5_pt' : '',
    'numJets' : '',
    'nBDeepFlavM' : '',
    'nBDeepFlavL' : '',
    'Wmass' : '',
}


def getEventNumbers(inputFiles, outputName, config, year, sample_name):
    t0 = time.time()
    tdiff = time.time()
    from TTH.Plotting.Datacards.AnalysisSpecificationFromConfig import analysisFromConfig
    analysis = analysisFromConfig(os.environ["CMSSW_BASE"] + "/src/TTH/MEAnalysis/data/" + config)

    sample = analysis.get_sample(sample_name)
    
    print(analysis)    

    useCats = []
    
    for icat, cat in enumerate(analysis.categories):
        if icat == 0:
            useVar = cat.discriminator.name

        if useVar == cat.discriminator.name:
            useCats.append(cat)

        
    tt = ROOT.TChain("tree")
    for fi in inputFiles:
        print("adding", fi)
        fn = getSitePrefix(fi)
        tf = ROOT.TFile.Open(fn)
        if not tf or tf.IsZombie():
            raise Exception("Could not open file: {0}".format(fn))
        tf.Close() 
        tt.AddFile(fn)


    treemodel = getattr(ROOT.TTH_MEAnalysis, "TreeDescriptionFHMCFloat")
    logging.debug("treemodel {0}".format(treemodel))

    matched_processes = [p for p in analysis.processes if p.input_name == sample.name]

    print(matched_processes)

    samdesc = ROOT.TTH_MEAnalysis.SampleDescriptionFH

    events = treemodel(
        tt,
        samdesc(
            samdesc.MC
        ),
        "Run2v1p3",
        year,
    )
    
    iEv = 0
    while events.reader.Next():
        if ttree_postproc:
            exit()
            ttree_postproc.GetEntry(iEv)
            
        nevents += 1
        iEv += 1

        if nevents % 100 == 0:
            logging.info("processed {0} events".format(nevents))

        if not (events.is_sl or events.is_dl or events.is_fh):
            continue


         # event = createEvent(
         #     events,
         #     "nominal",
         #     "mc",
         #     year,
             
             
         # )
    # nEvents = tree.GetEntries()
    # for iev in range(skipevents, nEvents):
    #     if iev%10000 == 0:
    #         logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f} | Time left {2:8f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
    #         tdiff = time.time()
    
if __name__ == "__main__":
    fileNames = os.environ["FILE_NAMES"].split()
    outFileName = sys.argv[1]
    config = sys.argv[2]
    year = sys.argv[3]

    prefix, sample = get_prefix_sample(os.environ["DATASETPATH"])
    

    log_format = ('%(levelname)-8s %(module)-18s %(funcName)-18s %(message)s')
    logging.basicConfig(level=logging.DEBUG, format=log_format)



    
    getEventNumbers(fileNames, outFileName, config, year, sample)
