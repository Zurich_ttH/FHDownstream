#!/bin/bash

source common.sh

cd $GC_SCRATCH

hadd -f out.root $FILE_NAMES

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/addDNN.py --inputFile out.root --ModelBase FHDownstream/data/DNN/Run2Legacy_v1_in25_100Nodes/ --mediumWP 0.2770 --looseWP 0.0494 --addInputVariables
