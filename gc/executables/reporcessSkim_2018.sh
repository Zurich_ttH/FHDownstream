#!/bin/bash

source common.sh

# # #go to work directory
cd $GC_SCRATCH

# export DATASETPATH="TTToHadronic_TuneCP5_13TeV-powheg-pythia8"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2018_AH_v3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3/191114_152051/0000/tree_249.root"
#export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2018_AH_v3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3/191114_152051/0000/tree_173.root"

#xrdcp -f $FILE_NAMES out.root

SAMPLE=$(python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getSampleForDataset.py)

hadd -f out.root $FILE_NAMES

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addTFLoose.py --inputFile out.root --outBranchs TFLoose --mediumWP 0.2770 --looseWP 0.0494 --year 2018 --sample ${SAMPLE} 

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/addDNN.py --inputFile out.root --ModelBase FHDownstream/data/DNN/Run2Legacy_v1_in25_100Nodes/ --mediumWP 0.2770 --looseWP 0.0494 --addInputVariables
