#!/bin/bash

source common.sh
source year_2017.sh

# export DATASETPATH=ttHTobb_M125_13TeV_powheg_pythia8
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/vmikuni/tth/LegacyRun2_2017_AH_v3/ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3/191025_142430/0004/tree_4898.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/vmikuni/tth/LegacyRun2_2017_AH_v3/ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3/191025_142430/0004/tree_4899.root"
# export GC_SCRATCH=/scratch/koschwei
# export YEAR=2016

#go to work directory
cd $GC_SCRATCH

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/projectSkimFH.py out.root kinematics alltrigger leptons mem noSyst
python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/reCalcPUWeightForSkim.py --inputFile out.root --output . --inplace --year ${YEAR} --dataset ${DATASETPATH}
#python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addbTagWeights.py --inputFile out.root --output . --inplace --onlyNominal  --addNorm --sample ${DATASETPATH} --normPath /work/koschwei/slc7/ttH/LegacyRun2/v3p2/CMSSW_10_2_15_patch2/src/TTH/FHDownstream/data/bNorm/2016/LegacyRun2_AH_v3/
# #python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/addDNN.py --inputFile out.root --ModelBase FHDownstream/data/DNN/Run2Baseline_v1_ttHSR_Binary_100Nodes_Dropout0p3/ --outBranchs DNNPred

