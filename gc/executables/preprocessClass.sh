#!/bin/bash

source common.sh

cd $GC_SCRATCH

SAMPLE=$(python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getSampleForDataset.py)

export PYTHONPATH=${PYTHONPATH}:${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/:${CMSSW_BASE}/src/TTH/FHDownstream/utils/
echo ${PYTHONPATH}
echo ${SAMPLE}
python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/flatNprocess.py --config ${CMSSW_BASE}/src/TTH/FHDownstream/data/nTupleProcessing/classification/Run2Baseline_2017_v1.cfg --envInput FILE_NAMES --output ./out.root --sample ${SAMPLE}


