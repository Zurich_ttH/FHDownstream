#!/bin/bash

source common.sh
source year_2017.sh

# export DATASETPATH="TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2017_AH_v3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3/191101_145252/0005/tree_5369.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2017_AH_v3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3/191101_145252/0005/tree_5289.root"

# export DATASETPATH="JetHT"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_1.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_15.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_2.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_3.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_4.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_5.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_6.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/JetHT/LegacyRun2_2017_AH_v3_re_2/191202_083204/0000/tree_7.root"

# export DATASETPATH="BTagCSV"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re/BTagCSV/LegacyRun2_2017_AH_v3_re/191128_082558/0000/tree_19.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re/BTagCSV/LegacyRun2_2017_AH_v3_re/191128_082558/0000/tree_6.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re/BTagCSV/LegacyRun2_2017_AH_v3_re/191128_082558/0000/tree_7.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re/BTagCSV/LegacyRun2_2017_AH_v3_re/191128_082558/0000/tree_8.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re/BTagCSV/LegacyRun2_2017_AH_v3_re/191128_082558/0000/tree_9.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/BTagCSV/LegacyRun2_2017_AH_v3_re_2/191202_085345/0000/tree_1.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/BTagCSV/LegacyRun2_2017_AH_v3_re_2/191202_085345/0000/tree_2.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/BTagCSV/LegacyRun2_2017_AH_v3_re_2/191202_085345/0000/tree_3.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/BTagCSV/LegacyRun2_2017_AH_v3_re_2/191202_085345/0000/tree_4.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3_re_2/BTagCSV/LegacyRun2_2017_AH_v3_re_2/191202_085345/0000/tree_5.root"


cd $GC_SCRATCH

SAMPLE=$(python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getSampleForDataset.py)

export PYTHONPATH=${PYTHONPATH}:${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/:${CMSSW_BASE}/src/TTH/FHDownstream/utils/
echo ${PYTHONPATH}
echo ${SAMPLE}

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/projectSkimFH_preProc.py preProcSkim.root
python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/reCalcPUWeightForSkim.py --inputFile preProcSkim.root --output . --inplace --year ${YEAR} --dataset ${DATASETPATH}

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/flatNprocess.py --config ${CMSSW_BASE}/src/TTH/FHDownstream/data/nTupleProcessing/classification/Run2Legacy_2017_v1.cfg --inputFile preProcSkim.root --output ./out.root --sample ${SAMPLE} 


