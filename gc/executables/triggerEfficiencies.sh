#!/bin/bash

source common.sh

cd $GC_SCRATCH

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getTriggerEfficiency.py ${YEAR} ${MEDIUMWP} out.root ${FILE_NAMES}
