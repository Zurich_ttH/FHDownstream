#!/bin/bash

source common.sh

env

#go to work directory
cd $GC_SCRATCH


echo "========================================================="
echo "========================================================="
echo $SKIP_EVENTS
echo $MAX_EVENTS
echo $FILE_NAMES
echo $DATASETPATH
echo "========================================================="
echo "========================================================="


echo "python ${CMSSW_BASE}/src/TTH/Analyzer/test/transferFunctions/runSkimmerTF.py --sample ${DATASETPATH} --numEvents ${MAX_EVENTS} --skipEvents ${SKIP_EVENTS} --inputFiles ${FILE_NAMES} --autoEra"
python ${CMSSW_BASE}/src/TTH/Analyzer/test/transferFunctions/runSkimmerTF.py --sample ${DATASETPATH} --numEvents ${MAX_EVENTS} --skipEvents ${SKIP_EVENTS} --inputFiles ${FILE_NAMES} --autoEra

mv $GC_SCRATCH/processedNanoAOD.root out.root

echo $OFNAME > output.txt
