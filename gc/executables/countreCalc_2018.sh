#!/bin/bash
set -e
source common.sh
source year_2018.sh

#go to work directory
cd $GC_SCRATCH

#export SCRATCHDIR=/scratch/koschwei/${GC_TASK_ID}_${GC_PARAM}/
#cd SCRATCHDIR

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/reCalcCount2.py out.root $FILE_NAMES

