#!/bin/bash

source common.sh

echo "======== CONSTANTS ========"
echo "       YEAR = ${YEAR}      "
echo "  SKIMPOWER = ${SKIMPOWER} "
#go to work directory
cd $GC_SCRATCH

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/projectSkimFH.py out.root kinematics alltrigger leptons mem noSyst
python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addBTagNorm.py --inputFile out.root --sample ${DATASETPATH} --year ${YEAR}
