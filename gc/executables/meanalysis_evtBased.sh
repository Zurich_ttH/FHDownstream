#!/bin/bash

source common.sh

env

#go to work directory
cd $GC_SCRATCH


echo "========================================================="
echo "========================================================="
echo "========================================================="
echo $SKIP_EVENTS
echo $MAX_EVENTS
echo $FILE_NAMES
echo $DATASETPATH
echo $me_conf
echo $an_conf
echo "========================================================="
export MEMConf=${CMSSW_BASE}/src/TTH/MEAnalysis/python/$me_conf
echo $MEMConf
echo "========================================================="
echo "========================================================="
echo "========================================================="


python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/MEAnalysis_heppy_gc.py ${CMSSW_BASE}/src/TTH/MEAnalysis/data/${an_conf}

mv $GC_SCRATCH/Loop/tree.root out.root

echo $OFNAME > output.txt
