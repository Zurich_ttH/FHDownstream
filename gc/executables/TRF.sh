#!/bin/bash

source common.sh


######################## TESTING
#export DATASETPATH="TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8"
### TESTING 2016:
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2016_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2016_AH_v3p3/200212_144435/0009/tree_9352.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2016_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2016_AH_v3p3/200212_144435/0009/tree_9307.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2016_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2016_AH_v3p3/200212_144435/0009/tree_9271.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2016_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2016_AH_v3p3/200212_144435/0009/tree_9226.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/pbaertsc/tth/LegacyRun2_2016_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2016_AH_v3p3/200212_144435/0009/tree_9190.root"
# export YEAR="2016"
# export FLATCONFIG="data/nTupleProcessing/classification/Run2Legacy_2016_TRF.cfg"
### TESTING 2017
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3p3/200130_111959/0009/tree_9803.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3p3/200130_111959/0009/tree_9064.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3p3/200130_111959/0009/tree_9019.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3p3/200130_111959/0009/tree_9722.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3p3/200130_111959/0009/tree_9641.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3p3/200130_111959/0009/tree_9560.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2017_AH_v3p3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2017_AH_v3p3/200130_111959/0009/tree_9515.root"
# export YEAR="2017"
# export FLATCONFIG="data/nTupleProcessing/classification/Run2Legacy_2017_TRF.cfg"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2018_AH_v3p3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3p3/200211_152734/0005/tree_5784.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2018_AH_v3p3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3p3/200211_152734/0005/tree_5739.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2018_AH_v3p3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3p3/200211_152734/0005/tree_5658.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2018_AH_v3p3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3p3/200211_152734/0005/tree_5577.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2018_AH_v3p3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3p3/200211_152734/0005/tree_5496.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/rdelburg/tth/LegacyRun2_2018_AH_v3p3/TTToHadronic_TuneCP5_13TeV-powheg-pythia8/LegacyRun2_2018_AH_v3p3/200211_152734/0005/tree_5289.root"
# export YEAR="2018"
# export FLATCONFIG="data/nTupleProcessing/classification/Run2Legacy_2018_TRF.cfg"
######################## TESTING


cd $GC_SCRATCH

SAMPLE=$(python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getSampleForDataset.py)

export PYTHONPATH=${PYTHONPATH}:${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/:${CMSSW_BASE}/src/TTH/FHDownstream/utils/

echo "====================================================="
echo "                       SETTING                       "
echo "PYTHONPATH : "${PYTHONPATH}
echo "SAMPLE : " ${SAMPLE}
echo "YEAR : " ${YEAR}
echo "FLATCONFIG : " ${FLATCONFIG}
echo "====================================================="
python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/projectSkimFH_TRF.py preProcSkim.root

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/flatNprocess.py --config ${CMSSW_BASE}/src/TTH/FHDownstream/${FLATCONFIG} --inputFile preProcSkim.root --output ./out.root --sample ${SAMPLE} 


