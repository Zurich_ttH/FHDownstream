#!/bin/bash
source common.sh
cd $GC_SCRATCH
export ANALYSIS_CONFIG=${ANALYSISCONFIGFROMGC}
export SYSTVERSION=Run2v1p3

echo "------------------------------------------------------------"
echo "ANALYSIS_CONFIG = ${ANALYSIS_CONFIG}"
echo "------------------------------------------------------------"

xrdcp /pnfs/psi.ch/cms/trivcat/store/user/koschwei/TTH/data/weights_mergeEventPickles_2017_v5_THQ_ctcvcp_HIncl_M125_TuneCP5_13TeV-madgraph-pythia8.pkl .
xrdcp /pnfs/psi.ch/cms/trivcat/store/user/koschwei/TTH/data/weights_mergeEventPickles_2017_v5_THW_ctcvcp_HIncl_M125_TuneCP5_13TeV-madgraph-pythia8.pkl .

export PICKLEDIR=$(pwd)

echo "------------------------------------------------------------"
echo "PICKLEDIR = ${PICKLEDIR}"
echo "------------------------------------------------------------"

echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ls -l
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

python ${CMSSW_BASE}/src/TTH/Plotting/python/joosep/sparsinator.py
