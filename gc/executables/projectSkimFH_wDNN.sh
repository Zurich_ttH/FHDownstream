#!/bin/bash

source common.sh

echo "======== CONSTANTS ========"
echo "        YEAR = ${YEAR}      "
echo "   SKIMPOWER = ${SKIMPOWER} "
echo "   MEDIUM WP = ${MEDIUMWP}  "
echo "    LOOSE WP = ${LOOSEWP}   "
echo "    Higgs MU = ${HIGGSMU}   "
echo " Higgs Sigma = ${HIGGSSIGMA}"
#go to work directory
cd $GC_SCRATCH



python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/projectSkimFH.py out.root kinematics alltrigger leptons mem noSyst

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addBTagNorm.py --inputFile out.root --sample ${DATASETPATH} --year ${YEAR}

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addTFLoose.py --inputFile out.root --outBranchs TFLoose --mediumWP ${MEDIUMWP} --looseWP ${LOOSEWP} --year ${YEAR} --sample ${DATASETPATH}
python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addTFLoose.py --inputFile out.root --outBranchs TFLooseML --mediumWP ${MEDIUMWP} --looseWP ${LOOSEWP} --year ${YEAR} --sample ${DATASETPATH} --doValidation

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addRecoHiggs.py --inputFile out.root --doLeadingBJets --mu ${HIGGSMU} --sigma ${HIGGSSIGMA} --mediumWP ${MEDIUMWP} --looseWP ${LOOSEWP}

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/addDNN.py --inputFile out.root --ModelBase ${MODELBASE} --outBranchs DNNPred
