#!/bin/bash
set -e
source common.sh
source year_2017.sh

#go to work directory
cd $GC_SCRATCH

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getWeightNormalizations.py out.root $FILE_NAMES
