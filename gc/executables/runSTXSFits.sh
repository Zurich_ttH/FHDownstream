source /cvmfs/cms.cern.ch/cmsset_default.sh

echo "////////////////////////////////////////////////////////////7"
echo "CMSSWCOMBINE : ${CMSSWCOMBINE}"
echo "DATACARDARCHIVE : ${DATACARDARCHIVE}"
echo "SETTINGS : ${SETTINGS}"
echo "PREFIX : ${PREFIX}"
echo "CATEGORY : ${CATEGORY}"
echo "////////////////////////////////////////////////////////////7"

echo "////////////////////////////////////////////////////////////7"
cd ${CMSSWCOMBINE}
pwd
eval `scramv1 runtime -sh`
echo "////////////////////////////////////////////////////////////7"
cd ${GC_SCRATCH}
pwd
cp ${DATACARDARCHIVE} .
echo "////////////////////////////////////////////////////////////7"
ls -la
echo "////////////////////////////////////////////////////////////7"
tar -xzvf *.tar
echo "////////////////////////////////////////////////////////////7"
ls -la
echo "////////////////////////////////////////////////////////////7"
cd cards

text2workspace.py ${CATEGORY}_DNN_Node0.txt

echo "combine ${CATEGORY}_DNN_Node0.root -n _asimov_${CATEGORY}${PREFIX} ${SETTINGS} -t -1 --expectSignal 1 &> combineOutput_${CATEGORY}${PREFIX}.out.txt"
combine ${CATEGORY}_DNN_Node0.root -n _asimov_${CATEGORY}${PREFIX} ${SETTINGS} -t -1 --expectSignal 1 &> combineOutput_${CATEGORY}${PREFIX}.out.txt

tar -czvf fitResults.tar *${CATEGORY}${PREFIX}*
cp fitResults.tar ${GC_SCRATCH}/
echo "////////////////////////////////////////////////////////////7"
cd ${GC_SCRATCH}
less fitResults.tar
echo "////////////////////////////////////////////////////////////7"
