source /cvmfs/cms.cern.ch/cmsset_default.sh

export FULLINPUT=${PNFSPREFIX}${INPUTFILE}

echo "FULLINPUT = ${FULLINPUT}"
echo "CONFIG = ${CMSSW_BASE}/${DATACARDCONFIG}"
echo "Using combine from ${CMSSWCOMBINE}"
echo "THRESHOLD_1 = ${THRESHOLD_1}"
echo "THRESHOLD_2 = ${THRESHOLD_2}"
echo "THRESHOLD_3 = ${THRESHOLD_3}"
echo "CATEGORY = ${CATEGORY}"  
cd ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards

export TAG="Opt_${THRESHOLD_1}_${THRESHOLD_2}_${THRESHOLD_3}"
echo "TAG ) ${TAG}"

export ORIGCMSSW_BASE=${CMSSW_BASE}

pwd
echo "////////////////////////////////////////////////////////////7"
echo "python binOptimization.py --input ${FULLINPUT} --cat ${CATEGORY} --tag Opt_${THRESHOLD_1}_${THRESHOLD_2}_${THRESHOLD_3}  --folder ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID} --step1 ${THRESHOLD_1} --step2 ${THRESHOLD_2} --step3 ${THRESHOLD_3}"
python binOptimization.py --input ${FULLINPUT} --cat ${CATEGORY} --tag Opt_${THRESHOLD_1}_${THRESHOLD_2}_${THRESHOLD_3}  --folder ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID} --step1 ${THRESHOLD_1} --step2 ${THRESHOLD_2} --step3 ${THRESHOLD_3}

echo "////////////////////////////////////////////////////////////7"
ls -l ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}

echo "////////////////////////////////////////////////////////////7"
echo "python genOptimizedTemplates.py --input ${FULLINPUT} --rebinInfo ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}/binningOptimiziaton_${TAG}.json --cat ${CATEGORY} --outFolder ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}"
python genOptimizedTemplates.py --input ${FULLINPUT} --rebinInfo ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}/binningOptimiziaton_${TAG}.json --cat ${CATEGORY} --outFolder ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}

echo "////////////////////////////////////////////////////////////7"
ls -l ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}

echo "////////////////////////////////////////////////////////////7"
echo "python makeDatacards.py --config ${CMSSW_BASE}/${DATACARDCONFIG} --inputFile $(cat ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}/genOptimizedTemplates.out) --outputPath ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID} --tag ${TAG} --variable DNN_Node0 --category ${CATEGORY} --force --skipMissing"
python makeDatacards.py --config ${CMSSW_BASE}/${DATACARDCONFIG} --inputFile $(cat ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}/genOptimizedTemplates.out) --outputPath ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID} --tag ${TAG} --variable DNN_Node0 --category ${CATEGORY} --force --skipMissing

echo "////////////////////////////////////////////////////////////7"
cd ${CMSSWCOMBINE}
pwd
eval `scramv1 runtime -sh`
echo "////////////////////////////////////////////////////////////7"
cd ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}/${TAG}/cards
pwd
ls -l
echo "////////////////////////////////////////////////////////////7"
GENERALSETTINGS=" -M FitDiagnostics  -m 125 --cminDefaultMinimizerStrategy 0 --cminPreScan  --saveNormalizations --saveShapes --saveOverallShapes"
RSETTINGSREST=" --rMin -10 --rMax 10 "
TOL=" --cminDefaultMinimizerTolerance 0.1"


text2workspace.py ${CATEGORY}_DNN_Node0.txt
ls -l
echo "combine ${CATEGORY}_DNN_Node0.root ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST} &> combine.out.txt"
combine ${CATEGORY}_DNN_Node0.root ${TOL} ${GENERALSETTINGS} ${RSETTINGSREST}  -t -1 --expectSignal 1 &> combine.out.txt

cat combine.out.txt

echo "////////////////////////////////////////////////////////////7"

python ${ORIGCMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/parseCombineResult.py combine.out.txt ${GC_SCRATCH}/out_binOptimization_${GC_JOB_ID}/binningOptimiziaton_${TAG}.json

echo "////////////////////////////////////////////////////////////7"
cd ${GC_SCRATCH}/

tar cf optimized.tar out_binOptimization_${GC_JOB_ID}/
echo "////////////////////////////////////////////////////////////7"
less optimized.tar

