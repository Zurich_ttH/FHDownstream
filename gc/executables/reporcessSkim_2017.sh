#!/bin/bash

source common.sh

# #go to work directory
cd $GC_SCRATCH

# export DATASETPATH="JetHT"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/LegacyRun2_2018/v3/GCfcd38ca4b51e/JetHT/job_202_out.root"


SAMPLE=$(python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getSampleForDataset.py)

hadd -f out.root $FILE_NAMES

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/addTFLoose.py --inputFile out.root --outBranchs TFLoose --mediumWP 0.3033 --looseWP 0.0521 --year 2017 --sample ${SAMPLE}

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/addDNN.py --inputFile out.root --ModelBase FHDownstream/data/DNN/Run2Legacy_v1_in25_100Nodes/ --mediumWP 0.3033 --looseWP 0.0521 --addInputVariables 
