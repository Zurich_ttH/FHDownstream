#!/bin/bash

source common.sh
source year_2016.sh

# export DATASETPATH="TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/swertz/tth/LegacyRun2_2016_AH_v3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2016_AH_v3/191028_181919/0000/tree_122.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/swertz/tth/LegacyRun2_2016_AH_v3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8/LegacyRun2_2016_AH_v3/191028_181919/0000/tree_248.root"
#  export DATASETPATH="JetHT"
# export FILE_NAMES="root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_469.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_135.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_168.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_124.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_136.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_595.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_612.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_613.root root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/LegacyRun2_2017_AH_v3/JetHT/LegacyRun2_2017_AH_v3/191025_125903/0000/tree_598.root"

cd $GC_SCRATCH

SAMPLE=$(python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/getSampleForDataset.py)

export PYTHONPATH=${PYTHONPATH}:${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/:${CMSSW_BASE}/src/TTH/FHDownstream/utils/
echo ${PYTHONPATH}
echo ${SAMPLE}

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/projectSkimFH_preProc.py preProcSkim.root
python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/reCalcPUWeightForSkim.py --inputFile preProcSkim.root --output . --inplace --year ${YEAR} --dataset ${DATASETPATH}

python ${CMSSW_BASE}/src/TTH/FHDownstream/nTupleProcessing/classification/flatNprocess.py --config ${CMSSW_BASE}/src/TTH/FHDownstream/data/nTupleProcessing/classification/Run2Legacy_2016_v1.cfg --inputFile preProcSkim.root --output ./out.root --sample ${SAMPLE} 


