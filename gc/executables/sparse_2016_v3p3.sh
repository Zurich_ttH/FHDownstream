#!/bin/bash
source common.sh
cd $GC_SCRATCH
export ANALYSIS_CONFIG=${ANALYSISCONFIGFROMGC}
export SYSTVERSION=Run2v1p3

echo "------------------------------------------------------------"
echo "ANALYSIS_CONFIG = ${ANALYSIS_CONFIG}"
echo "------------------------------------------------------------"

python ${CMSSW_BASE}/src/TTH/Plotting/python/joosep/sparsinator.py
