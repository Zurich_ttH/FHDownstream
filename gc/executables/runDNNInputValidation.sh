source /cvmfs/cms.cern.ch/cmsset_default.sh

export FULLINPUT=${PNFSPREFIX}${INPUTFILE}

echo "FULLINPUT = ${FULLINPUT}"
echo "CONFIG = ${CMSSW_BASE}/${DATACARDCONFIG}"
echo "Using combine from ${CMSSWCOMBINE}"
echo "VARIBALE = ${VARIABLE}"
echo "CATEGORY = ${CATEGORY}"  
cd ${CMSSW_BASE}/src/TTH/FHDownstream/Datacards

pwd

#echo "python makeDatacards.py --config ${DATACARDCONFIG} --outputPath ${GC_SCRATCH} --tag ${DATACARDTAG} --variable ${VARIABLE} --force --skipMissing"
echo "python makeDatacards.py --config ${CMSSW_BASE}/${DATACARDCONFIG} --inputFile ${FULLINPUT} --outputPath ${GC_SCRATCH} --tag ${DATACARDTAG} --variable ${VARIABLE} --category ${CATEGORY} --force --skipMissing --cardPrefix card_"
python makeDatacards.py --config ${CMSSW_BASE}/${DATACARDCONFIG} --inputFile ${FULLINPUT} --outputPath ${GC_SCRATCH} --tag ${DATACARDTAG} --variable ${VARIABLE} --category ${CATEGORY} --force --skipMissing --cardPrefix card_

if [[ $? != 0 ]]
then
    exit 1
fi

export ORIGCMSSW_BASE=${CMSSW_BASE}

echo "////////////////////////////////////////////////////////////7"
cd ${CMSSWCOMBINE}
pwd
eval `scramv1 runtime -sh`
echo "////////////////////////////////////////////////////////////7"
ls -l ${GC_SCRATCH}/
echo "////////////////////////////////////////////////////////////7"
ls -l ${GC_SCRATCH}/${DATACARDTAG}/
echo "////////////////////////////////////////////////////////////7"
cd ${GC_SCRATCH}/${DATACARDTAG}/cards
pwd
ls -l 
echo "////////////////////////////////////////////////////////////7"
#${ORIGCMSSW_BASE}/src/TTH/FHDownstream/Datacards/fitDNNInputs.sh out fh
${ORIGCMSSW_BASE}/src/TTH/FHDownstream/Datacards/fitDNNInputs.sh out fh
echo "////////////////////////////////////////////////////////////7"
cd out
pwd
echo "////////////////////////////////////////////////////////////7"
tar cf ${GC_SCRATCH}/fitOut.tar *
echo "////////////////////////////////////////////////////////////7"
less ${GC_SCRATCH}/fitOut.tar 
