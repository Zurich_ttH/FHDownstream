import json
import pandas as pd
import sys, os
from glob import glob

def summerize(baseFolder):
    if not baseFolder.endswith("/"):
        baseFolder += "/"
        
    dirs = os.listdir(baseFolder)

    relevantInfo = []

    uniqueCats = []
    
    for d in dirs:

        jsobnFile = glob(baseFolder+d+"/*.json")[0]

        fitResults = None
        with open(jsobnFile, "r") as i:
            fitResults = json.load(i)
            relevantInfo.append(
                {
                    "JobID" : d.split("_")[-1],
                    "Threshold step 1" : fitResults["theshold_step1"],
                    "Threshold step 2" : fitResults["theshold_step2"],
                    "Threshold step 3" : fitResults["theshold_step3"],
                    "Category" : fitResults["category"],
                    "Variable" : fitResults["variables"],
                    "Asimov Sig" : fitResults["Asimov_sig"],
                    "Best fit mu" : fitResults["Fit_mu"],
                    "Best fit mu err up" : fitResults["Fit_err_up"]*(-1),
                    "Best fit mu err down" : fitResults["Fit_err_down"],  #Make error pos for sorting
                    "Bist fit mu err symm" : abs(fitResults["Fit_err_down"] + fitResults["Fit_err_up"]),
                    "nBins" : len(fitResults["binning_final"])
                }
            )
            if not fitResults["category"] in uniqueCats:
                uniqueCats.append(fitResults["category"])

    infoDF = pd.DataFrame(relevantInfo)
    
    for cat in uniqueCats:
        print("================================",cat,"================================")
        thisCatDF = infoDF.loc[infoDF["Category"] == cat]
        thisCatDF.set_index("JobID", inplace=True)
        #sortByColumns = ["Threshold step 1","Threshold step 2","Threshold step 3"]
        sortByColumns = ["Best fit mu err up", "Best fit mu err down", "Bist fit mu err symm", "Asimov Sig"]
        order = [True, True, True, False]
        thisCatDF.sort_values(by=sortByColumns, inplace=True, ascending=order)
        
        print(thisCatDF.loc[:, sortByColumns+["Threshold step 1","Threshold step 2","Threshold step 3", "nBins"]])
    
    


if __name__ == "__main__":
    baseFolder = sys.argv[1]
    summerize(baseFolder)
