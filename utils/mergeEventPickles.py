from __future__ import print_function

import sys
import pickle

from glob import glob

def update_progress(progress, barLength=50):
    """
    Draws a progress bar.
    Taken from https://stackoverflow.com/a/15860757

    Args:
      progress (float) : Pass 0 to start and > 1 to stop.
    """
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1:.2f}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()


def mergeEventPickles(baseFolder, tag):
    for datasetFolder in glob(baseFolder+"/*"):
        mergedEventList = []
        dataset = datasetFolder.split("/")[-1]
        print("Merging dataset: %s"%dataset)
        update_progress(0)
        allFiles = glob(datasetFolder+"/*.pkl")
        for ipkl, pkl in enumerate(allFiles):
            mergedEventList += pickle.load( open(pkl, "rb" ) )
            update_progress(ipkl/float(len(allFiles)))
        update_progress(2)
        pickle.dump( mergedEventList , open( "mergeEventPickles_"+tag+"_"+dataset+".pkl", "wb" ) )

if __name__ == "__main__":
    baseFolder = sys.argv[1]
    tag = sys.argv[2]
    mergeEventPickles(baseFolder, tag)
