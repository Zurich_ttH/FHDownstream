from __future__ import print_function
import sys
import datetime
from time import gmtime
from time import strftime

from glob import glob

def getRelevantValues(startdir):
    jobValues = {}
    
    for file_ in glob(startdir+"/*.txt"):
        with open(file_, "r") as f:

            jobNr = file_.split("/")[-1].split("_")[-1].replace(".txt","")
            jobValues[jobNr] = {}
            lines = f.read().split("\n")
            for line in lines:
                if line == "":
                    continue
                type_, val_ = line.split("=")
                if type_ in ["status","raw_status","runtime","attempt","retcode","id"]:
                    jobValues[jobNr][type_] = val_.replace('"','')

    return jobValues
        
def getGCBenchmark(startdir):

    jobValues = getRelevantValues(startdir)

    nSubmitted = 0
    nRunning = 0
    nFinished = 0
    nSuccess = 0
    nUnknown = 0
    nFailed = 0
    nCancelled = 0

    unknown_jobs = []
    cancelled_jobs = []
    failed_jobs = []
    
    times_success = []
    
    time_success_avg = -1
    time_success_max = -1
    time_success_min = -1

    attempts_success = []
    
    attempts_success_avg = -1
    attempts_success_max = -1
    attempts_success_min = -1
    
    
    
    for job in jobValues:
        if jobValues[job]["status"] == "RUNNING":
            nRunning += 1

        elif jobValues[job]["status"] == "SUBMITTED":
            nSubmitted += 1
        elif jobValues[job]["status"] == "FAILED":
            nFailed += 1
            failed_jobs.append(job)
        else:
            nFinished += 1
            if jobValues[job]["status"] == "SUCCESS":
                nSuccess += 1
                times_success.append(int(jobValues[job]["runtime"]))
                attempts_success.append(int(jobValues[job]["attempt"]))
            elif jobValues[job]["status"] == "UNKNOWN":
                nUnknown += 1
                unknown_jobs.append(job)
            elif jobValues[job]["status"] == "CANCELLED":
                nCancelled += 1
                cancelled_jobs.append(job)
            else:
                print("Found job with status", jobValues[job]["status"], " - no: ", job)


    time_success_avg = float(sum(times_success))/len(times_success)
    time_success_max = max(times_success)
    time_success_min = min(times_success)


    time_success_avg = strftime("%H:%M:%S", gmtime(time_success_avg))
    time_success_max = strftime("%H:%M:%S", gmtime(time_success_max))
    time_success_min = strftime("%H:%M:%S", gmtime(time_success_min))


    
    attempts_success_avg = float(sum(attempts_success))/len(attempts_success)
    attempts_success_max = max(attempts_success)
    attempts_success_min = min(attempts_success)
    attempts_success_avg_str = "{:.4f}".format(attempts_success_avg)
    attempts_success_max_str = "{}".format(attempts_success_max)
    attempts_success_min_str = "{}".format(attempts_success_min)

    

    print("============================================== BENCHMARK ==============================================")
    print("SUBMITTED \t n = {}".format(nSubmitted))
    print("-------------------------------------------------------------------------------------------------------")
    print("FAILED    \t n = {}".format(nFailed))
    print("-------------------------------------------------------------------------------------------------------")
    print("RUNNING   \t n = {}".format(nRunning))
    print("-------------------------------------------------------------------------------------------------------")
    print("FINISHED  \t n = {}".format(nFinished))
    print("SUCCESS   \t n = {} \t time \t  avg = {:10} \t max = {:10} \t min = {:10}".format(nSuccess, time_success_avg, time_success_max, time_success_min))
    print("          \t        \t  att \t  avg = {:10} \t max = {:10} \t min = {:10}".format(attempts_success_avg_str, attempts_success_max_str, attempts_success_min_str))
    print("UNKNOWN   \t n = {}".format(nUnknown))
    print("CANCELLED \t n = {}".format(nCancelled))
    if nFailed > 0:
        print("============================================== FAILED ================================================")
        print("  JOBList: "+",".join(failed_jobs))
    if nCancelled > 0:
        print("============================================= CANCELLED ==============================================")
        print("  JOBList: "+",".join(cancelled_jobs))
        
    if nUnknown > 0:
        print("============================================== UNKNOWN ===============================================")
        for job in unknown_jobs:
            print("  {} \t {}".format(job, jobValues[job]["id"]))
        print("  -----------------------------------------------------------------------------------------------------")
        print("  JOBList: "+",".join(unknown_jobs))

if __name__ == "__main__":
    startDir = sys.argv[1]

    getGCBenchmark(startDir)
