import sys, os
import ROOT
from glob import glob
from copy import deepcopy

def calcWeights(path2File, folder):
    print "Processing:",path2File
    rFile = ROOT.TFile(path2File, "READ")
    rFile.cd()
    allDenomHistos = filter(lambda x: x.GetName().startswith("hDenom"), list(rFile.GetListOfKeys()))
    allDenomHistos = [h.GetName() for h in allDenomHistos]
    
    allNumHistos = filter(lambda x: x.GetName().startswith("hNum"), list(rFile.GetListOfKeys()))
    allNumHistos = [h.GetName() for h in allNumHistos]


    allSystematics = [x.replace("hNumNJets_", "") for x in allNumHistos if x.startswith("hNumNJets_")]

    hDenoms = {}
    for name in allDenomHistos:
        if "central" in name:
            thisSyst = "central"
            thisVar = name.split("_central")[0]
        elif "down" in name:
            thisSyst = "down_"+name.split("_down_")[-1]
            thisVar = name.split("_down_")[0]
        elif "up" in name:
            thisSyst = "up_"+name.split("_up_")[-1]
            thisVar = name.split("_up_")[0]
        else:
            raise RuntimeError
        thisVar = thisVar.replace("hDenom", "")
        if thisVar not in hDenoms.keys():
            hDenoms[thisVar] = {}
        hDenoms[thisVar][thisSyst] = rFile.Get(name)

    hNums = {}
    for name in allNumHistos:
        if "central" in name:
            thisSyst = "central"
            thisVar = name.split("_central")[0]
        elif "down" in name:
            thisSyst = "down_"+name.split("_down_")[-1]
            thisVar = name.split("_down_")[0]
        elif "up" in name:
            thisSyst = "up_"+name.split("_up_")[-1]
            thisVar = name.split("_up_")[0]
        else:
            raise RuntimeError
        thisVar = thisVar.replace("hNum", "")
        if thisVar not in hNums.keys():
            hNums[thisVar] = {}
        hNums[thisVar][thisSyst] = rFile.Get(name)


    allVariables = hNums.keys()
    print allVariables
    print "-------------------------"
    print hDenoms.keys()
    infileName = path2File.split("/")[-1]
    outfileName = "{0}/bTagNormWeights_{1}".format(folder, infileName) 
    outFile = ROOT.TFile(outfileName, "RECREATE")
    outFile.cd()
    for var in allVariables:
        for syst in allSystematics:
            hWeight = hNums[var][syst].Clone(hNums[var][syst].GetName().replace("hNum", "hWeight"))
            hWeight.Divide(hDenoms[var][syst])
            hWeight.Write()
    outFile.Close()


def getAllWeights(path, folder = "."):
    if path.endswith("/"):
        path = path[:-1]

    print "Using path:",path
    for file_ in sorted(glob(path+"/*.root")):
        calcWeights(file_, folder)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Script for calcuating the btagNormalization for a given set of file from the getWeightNormalizations GC script")
    parser.add_argument("--inputPath", required=True, help="Path to inputfile for this script. Pass path ending with / and all root files in the folder will be processed", type=str)
    parser.add_argument("--folder", default = ".", help=" Folder where the output will be written to")
    args = parser.parse_args()
    if not os.path.exists(args.folder):
        print "Creating folder: {0}".format(args.folder)
        os.makedirs(args.folder)

    
    getAllWeights(args.inputPath, args.folder)
    
