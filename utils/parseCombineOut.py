import sys
from glob import glob

if __name__ == "__main__":
    folder = sys.argv[1]

    for _file in sorted(glob(folder+"*.out.txt")):
        fileName = _file.split("/")[-1]
        with open(_file, "r") as f:
            lines = f.read()
            lines = lines.split("\n")
            fitResutls = None
            errdown, errup, rVal = None, None, None
            for iline, line in enumerate(lines):
                if line.startswith("Best fit r"):
                    lineElem = line.split(" ")
                    #print lineElem
                    for ielem, elem in enumerate(lineElem):
                        if elem == "r:":
                            #print elem
                            rVal = lineElem[ielem+1]
                        if "/" in elem:
                            errdown, errup = elem.split("/")
                            #print errdown, errup

                    errdown = "{:.2f}".format(float(errdown))
                    errup = "{:.2f}".format(float(errup))
                            
                    fitResutls = "$%s_{%s}^{%s}$"%(rVal,errdown,errup)
                    print fileName, "\t", fitResutls
        
    
