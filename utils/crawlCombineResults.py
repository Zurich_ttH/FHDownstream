import sys
import os

def parseCombineResult(combienOut):
    print "Using combine ouput:", combienOut
    resultLine = None
    with open(combienOut, "r") as cOut:
        lines = cOut.read().split("\n")
        for l in lines:
            print "===", l
            if l.startswith("Best fit r"):
                resultLine = l


    if resultLine is None:
        raise RuntimeError("Could not find result")

    resultLine = resultLine.replace("Best fit r:","").replace("(68% CL)","").split(" ")
    resultLine = [x for x in resultLine if x != ""]
    central = resultLine[0]
    up, down = resultLine[1].split("/")


    central = float(central)
    up = float(up)
    down = float(down)

def parseFitDiagnoticsLog(lines):
    resultLine = None
    for l in lines:
        if l.startswith("Best fit r"):
            resultLine = l

    if resultLine is None:
        raise RuntimeError("Could not find result")

    resultLine = resultLine.replace("Best fit r:","").replace("(68% CL)","").split(" ")
    resultLine = [x for x in resultLine if x != ""]
    central = resultLine[0]
    down, up = resultLine[1].split("/")


    central = float(central)
    up = float(up)
    down = float(down)

    return central, up, down


def parseSignificanceLog(lines):
    resultLine = None
    for l in lines:
        if l.startswith("Significance:"):
            resultLine = l

    if resultLine is None:
        raise RuntimeError("Could not find result")

    resultLine = resultLine.replace("Significance:","").replace(" ","")
    sig = float(resultLine)

    return sig

def main(fpath):

    csvout = []
    
    if not fpath.endswith("/"):
        fpath += "/"


    results = {}
        
    for dirpath, dirnames, filenames in os.walk(fpath):
        #print "+++++++++++++++++++++++++++++"
        logFiles = [x for x in filenames if x.endswith(".log")]
        if logFiles:
            for file_ in logFiles:
                lines = None
                with open(dirpath+"/"+file_) as f:
                    lines = f.read().split("\n")
                if lines is None:
                    continue
                isSignificance = (len([x for x in lines if "method used is Significance" in x]) == 1)
                isFitDiagnostics = (len([x for x in lines if "method used is FitDiagnostics" in x]) == 1)
                if not (isSignificance or isFitDiagnostics):
                    continue
                #print "Will use file: "+dirpath+"/"+file_
                if isSignificance:
                    #print "  isSignificance"
                    sig = parseSignificanceLog(lines)
                    #print "  %s"%sig
                    result = (sig)
                if isFitDiagnostics:
                    #print "  isFitDiagnostics"
                    central, up, down = parseFitDiagnoticsLog(lines)
                    #print "  %s + %s - %s"%(central, up, down)
                    result = (central, up, down)
                    
                if "fh_j7_t4" in file_:
                    category = "7J4T"
                elif "fh_j8_t4" in file_:
                    category = "8J4T"
                elif "fh_j9_t4" in file_:
                    category = "8J4T"
                elif "fh_t4" in file_:
                    category = "4T"
                else:
                    category = ""

                #print "  %s"%category


                year = [x for x in dirpath.split("/") if x == "2016" or x == "2017" or x == "2018"]

                if len(year) == 1:
                    year = year[0]
                else:
                    year = ""
                
                #print "  %s"%year
                
                results[dirpath+"/"+file_] = {"year" : year,
                                              "cat" : category,
                                              "results" : result}


    for file_ in sorted(results.keys()):
        print results[file_]
        
if __name__ == "__main__":
    basefolder = sys.argv[1]

    main(basefolder)
