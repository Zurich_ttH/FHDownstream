import sys
import os
import ConfigParser
import glob
import subprocess


def renameSemplaeNames(config, basefolder):
    thisconfig = ConfigParser.ConfigParser()
    thisconfig.optionxform = str
    thisconfig.read(config)

    samples = []
    prefixes = []
    for section in thisconfig.sections():
        if "pythia8" in section and "TuneCP5" in section:
            samples.append(section)
            if "TuneCP5up" in section:
                prefix = section[0:section.find("TuneCP5up")+len("TuneCP5up")]
            elif "TuneCP5down" in section:
                prefix = section[0:section.find("TuneCP5down")+len("TuneCP5down")]
            else:
                prefix = section.split("TuneCP5")[0]
                
            prefixes.append(prefix)
    print samples

    print "-----------------------------------------------------------"
    print prefixes

    datasetFiles = glob.glob(basefolder+"/*.txt")

    print "-----------------------------------------------------------"
    print datasetFiles
    
    for _file in  datasetFiles:
        filename = _file.split("/")[-1]
        for iprefix, prefix in enumerate(prefixes):
            if filename.lower().startswith(prefix.lower()):
                #print filename.lower(), prefix.lower()
                if ( "up" in filename.lower() and not "up" in prefix.lower() or
                     "down" in filename.lower() and not "down" in prefix.lower()):
                    continue
                #print filename, samples[iprefix], prefix
                datasetname = subprocess.check_output("head -n 1 %s"%_file, shell=True)
                datasetname = datasetname.replace("[","").replace("]","").replace(" ","").replace("\n","")
                print datasetname
                if datasetname != samples[iprefix]:
                    print datasetname," != ",samples[iprefix]
                    sedCommand = 'sed -i "s@\[%s\]@\[%s\]@" %s'%(datasetname, samples[iprefix], _file)
                    print "Will do:"
                    print sedCommand
                    answer = raw_input("Fine? Enter y -- ")
                    if answer.lower() == "y":
                        os.system(sedCommand)
                

                break


if __name__ == "__main__":
    config = sys.argv[1]
    basefolder = sys.argv[2]
    renameSemplaeNames(config, basefolder)

    
