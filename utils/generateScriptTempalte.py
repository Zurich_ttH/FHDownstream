import logging

ARG_TEMPALTE = """
    argumentparser.add_argument(
        "--$ARG",
        action = "store",
        help = "Description of $ARG",
        type = str,
    )"""

FLAG_TEMPALTE = """
    argumentparser.add_argument(
        "--$FLAG",
        action = "store_true",
        help = "Descriotpn of $FLAG", 
    )"""

LOGGING_STANDALONE_TEMPALTE = """
    logging.basicConfig(
        format=('[%(asctime)s] %(funcName)-'+str(25)+'s %(levelname)-8s %(message)s'),
        level=args.logging,
        datefmt="%H:%M:%S"
    )"""

LOGGING_FHDOWNSTREAM = """
    initLogging(args.logging, funcLen = 21)"""

def generateScriptTemplate(target, standalone, args, flags):
    logging.info("Targetting : %s", target)
    tempalte = None
    with open("generateScript.template", "r") as t:
        tempalte = t.read().split("\n")

    funcName = target.split("/")[-1].replace(".py","")

    logging.info("Will use %s as function name",funcName)
    
    outTemplate = []
    for line in tempalte:
        justAddLine = True
        if "$ARGS" in line:
            justAddLine = False
            for arg in args:
                logging.info("Adding arg: %s", arg)
                thisArg = ARG_TEMPALTE.replace("$ARG", arg)
                for argline in thisArg.split("\n"):
                    outTemplate.append(argline)
        if "$FLAGS" in line:
            justAddLine = False
            if not flags:
                pass
            else:
                for flag in flags:
                    logging.info("Adding flag: %s", flag)
                    thisFlag = FLAG_TEMPALTE.replace("$FLAG", arg)
                    for flagline in thisFlag.split("\n"):
                        outTemplate.append(flagline)
        if "$LOGGINGINIT" in line:
            justAddLine = False
            if standalone:
                for thisline in LOGGING_STANDALONE_TEMPALTE.split("\n"):
                    outTemplate.append(thisline)
            else:
                for thisline in LOGGING_FHDOWNSTREAM.split("\n"):
                    outTemplate.append(thisline)
        if "$FUNCCALL" in line:
            justAddLine = False
            thisFuncCall = "{}({})".format(funcName, ",".join(args+flags))            
            outTemplate.append(line.replace("$FUNCCALL", thisFuncCall))
        if "$FUNCNAMEDEF" in line:
            justAddLine = False
            outTemplate.append(
                "def {}({}):".format(funcName, ",".join(args+flags))
            )
        if "$DEPENDENCY" in line:
            justAddLine = False
            if not standalone:
                outTemplate.append('import sys')
                outTemplate.append('import os')
                outTemplate.append('sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/"))')
                outTemplate.append('from classes.PlotHelpers import initLogging')
        if justAddLine:
            outTemplate.append(line)
        
    with open(target, "w") as o:
        for line in outTemplate:
            o.write(line+"\n")
    

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for making the ddQCD parameter in the FH cards explicit instead of using a wildcard'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--target",
        action = "store",
        help = "Target folder/scriptName.py",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--args",
        action = "store",
        help = "List of arguments",
        nargs = "+",
        default = ["arg1"],
        type = str,
    )
    argumentparser.add_argument(
        "--flags",
        action = "store",
        help = "List of flags",
        nargs = "+",
        default = [],
        type = str,
    )
    argumentparser.add_argument(
        "--standAlone",
        action = "store_true",
        help = "If set, no usual FHDownstream dependencies will be included",
    )
    args = argumentparser.parse_args()
    ##############################################################################################################
    ##############################################################################################################
    logging.basicConfig(
        format=('[%(asctime)s] %(funcName)-'+str(25)+'s %(levelname)-8s %(message)s'),
        level=args.logging,
        datefmt="%H:%M:%S"
    )

    generateScriptTemplate(args.target, args.standAlone, args.args, args.flags)
    
