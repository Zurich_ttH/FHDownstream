import sys, os
from glob import glob
import logging

import ROOT

sys.path.insert(0, os.path.abspath('../Plotting/'))
from classes.PlotHelpers import saveCanvasListAsPDF, initLogging, makeCanvasOfHistos, getColorStyleList
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def compatebTagNormFiles(rootDir, outputName, outputFolder, yRange, plotVars = None, plotSamples = None):
    """
    Compare b-tag normalization weights between samples.
    """

    logging.info("Glob: %s", rootDir+"/*.root")
    print plotSamples
    weightFiles = glob(rootDir+"/*.root")
    rFilesIn = {}
    samples = []
    for inFile in weightFiles:
        if not "bTagNormWeights_" in inFile:
            logging.warning("Skipping file %s", inFile)
            continue
        fileName = inFile.split("/")[-1].split(".root")[0]
        _sampleHandle = fileName.replace("bTagNormWeights_","").split("_")
        sampleHandle = _sampleHandle[0]+"_"+_sampleHandle[1]
        if plotSamples is not None:
            skipThisSample = True
            for s in plotSamples:
                if s in sampleHandle:
                    skipThisSample = False
                    break
            if skipThisSample:
                continue
        logging.info("Opening: %s", inFile)
        rFilesIn[sampleHandle] = ROOT.TFile.Open(inFile)
        samples.append(sampleHandle)
        
    print rFilesIn

    lineColors, lineStyles = getColorStyleList(nColors = len(weightFiles))

    #Get all histos in files. It is expected all of the files have the same ones
    canvases = []
    allKeys = rFilesIn[rFilesIn.keys()[0]].GetListOfKeys()
    fistName = allKeys[0]
    for ikey, key in enumerate(allKeys):
        if ikey%10 == 0 and ikey != 0:
            logging.info("Processing key %s/%s", ikey, len(rFilesIn[rFilesIn.keys()[0]].GetListOfKeys()))
        keyName = key.GetName()
        if plotVars is not None:
            skipThisKey = True
            for var in plotVars:
                if var in keyName:
                    skipThisKey = False
                    break
            if skipThisKey:
                continue
        logging.debug("Getting histos for %s", keyName)
        histos = []
        skipThisPlot = False
        for iSample, sample in enumerate(samples):
            try:
                thisHisto=rFilesIn[sample].Get(keyName)
                thisHisto.IsZombie()
            except ReferenceError:
                logging.error("Skipping %s in %s",keyName, sample)
                skipThisPlot = True
                break
            thisHisto.SetLineColor(lineColors[iSample])
            thisHisto.SetLineStyle(lineStyles[iSample])
            thisHisto.GetYaxis().SetRangeUser(yRange[0], yRange[1])
            histos.append(thisHisto)

        if skipThisPlot:
            continue
        canvases.append(
            makeCanvasOfHistos(
                name = keyName,
                Varname = keyName,
                histoList = histos,
                legendText = samples,
                yRange = yRange,
                legendSize = (0.15, 0.15, 0.7, 0.35)
                #legendColumns = 3,
            )
        )


    saveCanvasListAsPDF(canvases, outputName, outputFolder)
    


if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Script for comparing bTagNorm files between samples'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--inputFolder",
        action = "store",
        type = str,
        required = True,
        help = "Sets folder that will be used to find .root files",
    )
    argumentparser.add_argument(
        "--outName",
        action = "store",
        type = str,
        required = True,
        help = "Output file name",
    )
    
    argumentparser.add_argument(
        "--outFolder",
        action = "store",
        type = str,
        required = True,
        help = "Output folder. Will be created!",
    )    
    argumentparser.add_argument(
        "--range",
        action = "store",
        nargs = 2,
        default = [0.84, 1.02],
        type = float,
        help = "Sets the y-range of the plot",
    )
    argumentparser.add_argument(
        "--vars",
        action = "store",
        nargs = "+",
        default = None,
        help = "Selects a subset of variables",
    )
    argumentparser.add_argument(
        "--samples",
        action = "store",
        nargs = "+",
        default = None,
        help = "Selects a subset of variables",
    )
    args = argumentparser.parse_args()
        
    initLogging(args.loglevel)
    
    compatebTagNormFiles(
        rootDir=args.inputFolder,
        outputName=args.outName,
        outputFolder=args.outFolder,
        yRange=args.range,
        plotVars = args.vars,
        plotSamples = args.samples
    )
