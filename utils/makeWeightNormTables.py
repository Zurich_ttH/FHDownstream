import sys, os
import glob
import json
import logging

import ROOT

sys.path.insert(0, os.path.abspath('../Plotting/'))
from classes.PlotHelpers import initLogging

def calcWeightFactors(inFile, hBase="hGenWeightednoPU"):
    rFile = ROOT.TFile.Open(inFile)

    baseHisto = rFile.Get(hBase)

    hmuFUp = rFile.Get("")
    
    hmuFUp = rFile.Get("hGenWeightedmuFUp")
    hmuFDown = rFile.Get("hGenWeightedmuFDown")
    hmuRUp = rFile.Get("hGenWeightedmuRUp")
    hmuRDown = rFile.Get("hGenWeightedmuRDown")

    hISRUp = rFile.Get("hGenWeightedISRUp")
    hISRDown = rFile.Get("hGenWeightedISRDown")
    hFSRUp = rFile.Get("hGenWeightedFSRUp")
    hFSRDown = rFile.Get("hGenWeightedFSRDown")

    
    hPDFMembers = []
    nMemberMax = 0
    for i in range(103):
        thisHisto = rFile.Get("hGenWeightedPDFMember"+str(i))
        if thisHisto.GetBinContent(1) == 0:
            logging.warning("Only considering %s PDF Members", i)
            break
        
        hPDFMembers.append(
            thisHisto
        )

    retDict = {"muF_Up" : baseHisto.GetBinContent(1)/hmuFUp.GetBinContent(1),
               "muF_Down" : baseHisto.GetBinContent(1)/hmuFDown.GetBinContent(1),
               "muR_Up" : baseHisto.GetBinContent(1)/hmuRUp.GetBinContent(1),
               "muR_Down" : baseHisto.GetBinContent(1)/hmuRDown.GetBinContent(1),
               "ISR_Up" : baseHisto.GetBinContent(1)/hISRUp.GetBinContent(1),
               "ISR_Down" : baseHisto.GetBinContent(1)/hISRDown.GetBinContent(1),
               "FSR_Up" : baseHisto.GetBinContent(1)/hFSRUp.GetBinContent(1),
               "FSR_Down" : baseHisto.GetBinContent(1)/hFSRDown.GetBinContent(1)}
    for i in range(len(hPDFMembers)):
        retDict["PDFMember_"+str(i)] = baseHisto.GetBinContent(1)/hPDFMembers[i].GetBinContent(1)

    return retDict
        
    
    

if __name__ == "__main__":
    import argparse
    ############################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inFolder",
        action = "store",
        help = "Folder with the input files",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outName",
        action = "store",
        help = "Name of the output json file",
        type = str,
        required = True
    )
    args = argumentparser.parse_args()
    initLogging(args.logging, funcLen = 21)

    allFactors = {}
    for file_ in glob.glob(args.inFolder+"/*.root"):
        sample = file_.split("/")[-1].split(".root")[0]
        if not ("tH" in sample or "TT" in sample or "ST" in sample or "TH" in sample):
            continue
        logging.info("Processing file %s", file_)
        logging.info("  Sample: %s", sample)
        allFactors[sample] = calcWeightFactors(file_)

    with open("./"+args.outName+".json", "w") as f:
        json.dump(allFactors, f, separators=(',', ': '), sort_keys=True, indent=4,)
    
