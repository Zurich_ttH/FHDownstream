from __future__ import print_function, division

import sys
import os
import logging
import pickle
sys.path.insert(0, os.path.abspath('../Plotting/'))
from classes.PlotHelpers import initLogging

from mergeEventPickles import update_progress

import ROOT


def genWeightsBranchNames(floating=True, grid=True):
    branchNames = []

    if floating:
        for i in range(9):
            branchNames.append(
                "weight_CPfloat_"+str(i+1)
            )

    if grid:
        for i in range(600):
            branchNames.append(
                "weight_CPgrid_"+str(i+1)
            )

    return branchNames


def genWeightPickle(inputFile, eventPickle, treeName="Events", drawProgressBar=False, floating=True,
                    grid=False, batch=False, ignoreMissingEvents=False):
    if (not floating) and (not grid):
        raise RuntimeError("Either floating or grid has to be passed")
    
    if ignoreMissingEvents:
        logging.warning("Will ignore missing events in input pickle")
        if not batch:
            raw_input("Press return if you want to continue.")

    
    logging.info("Loading: %s", eventPickle)
    events = pickle.load( open(eventPickle, "rb" ) )

    nEvents_pickle = len(events)
    logging.info("Events loaded: %s", nEvents_pickle)

    rFile = ROOT.TFile.Open(inputFile)
    logging.info("Opened file: %s", rFile)
    
    tree = rFile.Get(treeName)
    logging.info("Loaded tree : %s (tree name %s)", tree, treeName)

    nEvents = tree.GetEntries()
    logging.info("Tree has %s entries", nEvents)

    if batch:
        drawProgressBar = False

    if drawProgressBar:
        update_progress(0)

    tree.BuildIndex("event")

    theBranches = genWeightsBranchNames(floating, grid)

    theWeights = {}

    nMissing = 0
    missing = []
    
    for i, eventIds in enumerate(events):
        run, lumi, event = eventIds
        logging.debug("%s | Run %s | Lumi %s | Evt  %s", i, run, lumi, event)
        if drawProgressBar:
            update_progress(i/nEvents_pickle)

        if batch:
            if i%50000 == 0:
                logging.info("Processing event %s/%s", i, nEvents_pickle)

        status = tree.GetEntryWithIndex(event)

        if status == -1:
            if ignoreMissingEvents:
                if batch:
                    logging.error("Found a missing event but will ignore it")
                nMissing += 1
                missing.append(eventIds)
                continue
            else:
                raise RuntimeError("There is some event missing in the input pickle")

        if event in theWeights.keys():
            raise RuntimeError("Already processed this event: %s", event)
        
        theWeights[event] = {}
        if floating: 
            theWeights[event]["floating"] = {}
        if grid:
            theWeights[event]["grid"] = {}
        
        for branch in theBranches:
            thisWeight = tree.__getattr__(branch)

            logging.debug("Weight %s = %s", branch, thisWeight)
            
            if "CPfloat" in branch:
                theWeights[event]["floating"][int(branch.replace("weight_CPfloat_",""))] = thisWeight
            if "CPgrid" in branch:
                theWeights[event]["grid"][int(branch.replace("weight_CPgrid_",""))] = thisWeight
        
    if drawProgressBar:
        update_progress(2)


    if ignoreMissingEvents:
        logging.warning("Missing events: %s ")
        with open("missing_"+eventPickle+".txt","w") as fMiss:
            for m in missing:
                fMiss.write(",".join([str(x) for x in m])+"\n")

        
    outName = "weights_"+eventPickle
    logging.info("Output file: %s", outName)
    pickle.dump(theWeights, open( outName, "wb" ))
        

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for generating the Weight pickle for the tH and ttH CP samples'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        choices=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input root file with the weight (in nanoAOD style)",
        type = str,
        required=True
    )
    argumentparser.add_argument(
        "--eventPickle",
        action = "store",
        help = "Pickle with the event/lumi/run number of the file",
        type = str,
        required=True
    )
    argumentparser.add_argument(
        "--floating",
        action = "store_true",
        help = "If passed, the floating weights will be included", 
    )
    argumentparser.add_argument(
        "--grid",
        action = "store_true",
        help = "If passed, the grid weights will be included", 
    )
    argumentparser.add_argument(
        "--batch",
        action = "store_true",
        help = "If passed, the progress bar will not be shown (usefull when piping to file)", 
    )
    argumentparser.add_argument(
        "--ignoreMissingEvents",
        action = "store_true",
        help = "If passed, missing events in the input pickle will be ignored", 
    )
    args = argumentparser.parse_args()
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging, funcLen = 21)

    genWeightPickle(args.inputFile, args.eventPickle, drawProgressBar = True if args.logging > 10 else False, floating=args.floating, grid=args.grid, batch=args.batch, ignoreMissingEvents=args.ignoreMissingEvents)
