from __future__ import print_function
import glob

import ROOT

def findNanoFileWithEvent(filename, event, treeName="nanoAOD/Events"):
    rFile = ROOT.TFile.Open(filename)
    events = rFile.Get(treeName)
    events.BuildIndex("event")

    events.GetEntryWithIndex(event)
    if events.event == event:
        print("Found %s in file %s"%(event,filename))


if __name__ == "__main__":
    findEvents = 32672938
    for f in glob.glob("/t3home/koschwei/scratch/ttH/testing_200129/WJet800/*.root"):
        findNanoFileWithEvent(f, findEvents)
