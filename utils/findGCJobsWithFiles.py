import sys
import os

def findGCJobsWithFiles(baseFolder, fileSignatures):
    jobsWithFile = []
    for idir_, dir_ in enumerate(os.listdir(baseFolder)):
        if idir_%250 == 0:
            print "Status:: {:5d} / {:5d}".format(idir_, len(os.listdir(baseFolder)))
        with open(baseFolder+"/"+dir_+"/gc.stdout", "r") as f:
            lines = f.read().split("/n")
            for line in lines:
                if "export FILE_NAMES" in line:
                    for fileSignature in fileSignatures:
                        if fileSignature in line:
                            jobsWithFile.append(dir_)
            
    print jobsWithFile
    

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for adding DNN from a lookup table to tree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--basePath",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--fileSignatures",
        action = "store",
        help = "List of file path signatures",
        type = str,
        nargs = "+",
        required = True,
    )
    args = argumentparser.parse_args()
    findGCJobsWithFiles(args.basePath,
                        args.fileSignatures)
