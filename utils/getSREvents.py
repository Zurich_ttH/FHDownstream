import ROOT


#fileList = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p3/CMSSW_9_4_9/src/TTH/FHDownstream/gc/datasets/2016_Vini/JetHT.txt"
fileList = "JetHT.txt"
fileList = "ttHTobb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8.txt"
midFix = "ttH_"
fileList_ = []
with open(fileList, "r") as f:
    lines = f.read()
    for line in lines.split("\n"):
        elem = line.split(" = ")
        if elem[0].startswith("root"):
            fileList_.append(elem[0])
            #print elem[0]

print "File "+str(len(fileList_))
tree = ROOT.TChain("tree")
for ifile, f in enumerate(fileList_):
    if ifile%50 == 0 and ifile > 1:
        print "Adding file "+str(ifile)
        #break
    tree.Add(f)

baseSelection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFJet450_v == 1) && is_fh && ht>500 && jets_pt[5]>40 && numJets >= 7 && nBCSVM >= 3 && wmass>60 && wmass<100"
print "Pre skim"
tree = tree.CopyTree(baseSelection)
print "Post skim"

nEvents = tree.GetEntries()
print "Number of events "+str(nEvents)
nSR = {}
nSR["7j3tSR"] = 0
nSR["8j3tSR"] = 0
nSR["9j3tSR"] = 0
nSR["7j4tSR"] = 0
nSR["8j4tSR"] = 0
nSR["9j4tSR"] = 0
evtLists = {}
evtLists["7j3tSR"] = []
evtLists["8j3tSR"] = []
evtLists["9j3tSR"] = []
evtLists["7j4tSR"] = []
evtLists["8j4tSR"] = []
evtLists["9j4tSR"] = []
evtLists["7j3tVR"] = []
evtLists["8j3tVR"] = []
evtLists["9j3tVR"] = []
evtLists["7j4tVR"] = []
evtLists["8j4tVR"] = []
evtLists["9j4tVR"] = []

for iev in range(0, nEvents):
    tree.GetEvent(iev)
    cat = "None"
    if tree.numJets == 7:
        if tree.nBCSVM == 3:
            cat = "7j3t"
        elif tree.nBCSVM >= 4:
            cat = "7j4t"
        else:
            pass

        if "4t" in cat:
            if tree.qg_LR_4b_flavour_3q_0q >= 0.5:
                cat += "SR"
            else:
                cat += "VR"
        if "3t" in cat:
            if tree.qg_LR_3b_flavour_4q_0q >= 0.5:
                cat += "SR"
            else:
                cat += "VR"
    elif tree.numJets == 8:
        if tree.nBCSVM == 3:
            cat = "8j3t"
        elif tree.nBCSVM >= 4:
            cat = "8j4t"
        else:
            pass

        if "4t" in cat:
            if tree.qg_LR_4b_flavour_4q_0q >= 0.5:
                cat += "SR"
            else:
                cat += "VR"
        if "3t" in cat:
            if tree.qg_LR_3b_flavour_5q_0q >= 0.5:
                cat += "SR"
            else:
                cat += "VR"
    elif tree.numJets >= 9 and tree.wmass <= 92 and tree.wmass >= 70:
        if tree.nBCSVM == 3:
            cat = "9j3t"
        elif tree.nBCSVM >= 4:
            cat = "9j4t"
        else:
            pass

        if "4t" in cat:
            if tree.qg_LR_4b_flavour_5q_0q >= 0.5:
                cat += "SR"
            else:
                cat += "VR"
        if "3t" in cat:
            if tree.qg_LR_3b_flavour_5q_0q >= 0.5:
                cat += "SR"
            else:
                cat += "VR"
    else:
        pass



    if "SR" in cat:
        nSR[cat] += 1
    if cat != "None":
        print "Evt: {0} | Cat: {1}".format(tree.evt, cat)        

        if cat == "7j3tSR":
            evtLists["7j3tSR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "8j3tSR":
            evtLists["8j3tSR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "9j3tSR":
            evtLists["9j3tSR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "7j4tSR":
            evtLists["7j4tSR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "8j4tSR":
            evtLists["8j4tSR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "9j4tSR":
            evtLists["9j4tSR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "7j3tVR":
            evtLists["7j3tVR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "8j3tVR":
            evtLists["8j3tVR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "9j3tVR":
            evtLists["9j3tVR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "7j4tVR":
            evtLists["7j4tVR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "8j4tVR":
            evtLists["8j4tVR"].append("{0} {1}".format(tree.evt, tree.run))
        if cat == "9j4tVR":
            evtLists["9j4tVR"].append("{0} {1}".format(tree.evt, tree.run))
print nSR

for cat in ["7j3tSR","8j3tSR","9j3tSR","7j4tSR","8j4tSR","9j4tSR","7j3tVR","8j3tVR","9j3tVR","7j4tVR","8j4tVR","9j4tVR"]:
    with open("Evt_"+midFix+cat+".txt", "w") as f:
        print "Writing "+str(f)
        for evt in evtLists[cat]:
            f.write(str(evt)+"\n")

        
