# DNN Workflow

## 1 Training and initial evaluation
The framework for training and evaluating DNNs (with Keras/TF) is implemented in the [TF4ttHFH framwork](https://github.com/kschweiger/TF4ttHFH) 

### 1.1 Preprocessing
The framework for training is written to be completely independent on ROOT (and CMSSW). Therefore the initial preprocessing of the nTuples (created with the tthbb13 FW) is happening within the CMSSW environment.   
The relevant script is located in `FHDownstream/nTupleProcessing/classification/flatNprocess.py`. It will flatten to tree and calculate additional variables, that are not in the nTuple. The script is initialized with a config. An example can can found here: `FHDownstream/data/nTupleProcessing/classification/Run2Dev_v1.cfg`
The proprocessing step already applied a cut selecting only the SR/CR/TR + validation regions to save space.

#### 1.1.1 Additional variables
The processor for calculating additional variables are located in `FHDownstream/nTupleProcessing/classification/processModules.py`. Additinal processes can be implemented after the example of these processors. This module will also be used in the **sparsinator** and the scirpt for adding DNN predictions to skims in order to ensure consitent behaviour.

#### 1.1.2 Running the preprocessor
Use the batch system configuration `FHDownstream/gc/confs/preprocessClass_Data.conf` and `FHDownstream/gc/confs/preprocessClass_MC.conf`.

### 1.2 Training
The next steps are done within the TF4ttHFH framework. **All given path are relative to the TF4ttHFH root directory**

#### 1.2.1 Get file lists
The converter runs from txt file containing paths to all input root files. Use `utils/makeFileList.py` to generate them. It is expected that the output files from the proprocessor are located on the scratch space with a folder per sample. 

```bash
# /path/to/output/folder/ will be created
python utils/makeFileList.py /path/to/scratch/containing/sampleFolders/  /path/to/output/folder/
```

#### 1.2.2 Conversion to h5 files

Use the `convertTree.py` (or the shell script `runAllSamples.sh`) to convert all the samples.
The scripts run off configuration files. Check the folder `data/converter/` for example configurations. The idea is that a config defines

- which input filelist to use,
- how to call the output file,
- a baseline selection and
- multiple categories with additional selection cuts.

#### 1.2.3. The actual training
All training paramters are set by configuration files. So to start a training use

```bash
python train_dnn.py --config data/training/dnn/someConfig.cfg
```

Check the [README](https://github.com/kschweiger/TF4ttHFH/blob/master/README.md), the [example config with all options](https://github.com/kschweiger/TF4ttHFH/blob/master/data/dnn_example.cfg) or [DNN training folder](https://github.com/kschweiger/TF4ttHFH/tree/master/data/dnn/training)  for more informantions.

After a sucessfull training, relevant files (model, weights, transformations etc.) will be written to the folder defined in the config. Furthermore the used config will be copied there to assure no information is removed and we can do some book keeping on the hyperparamter.  

For DNN binary as well as categorical training can be implemented by changing the configuration file. Depending on this different metric will be written als plots to the output folder. In both cases metric over epoch plots will be created. Binary classification will also include ROC and discriminator plots out of the box

#### 1.2.4 Evaluation
For further evaluation (e.g. in other regions, or comparisons ot other classifiers in the input file) of the trained model use the `eval_dnn.py` script. (Unsurprisingly at this point) The script is also configuratble with .cfg files. 
Check the configs present in [DNN evalutation folder](https://github.com/kschweiger/TF4ttHFH/tree/master/data/dnn/evaluation).    
Note: here it is important that the config takes the folder to the trained model (the one created after training) as input. It will then load data from different files in this folder. 


## 2 Using the data in the tthbb13 Framework

We are back in the tthbb13 framework. **All given path are relativ to `$CMSSW_BASE/src/TTH`**

The "offical" models are placed in `FHDownsteam/data/DNN`, each with a separate folder. If you want to run on new ones, please make sure that it contains the model as well as all `.txt` and `.json` files since they are required in the evaluation.

### 2.1 Adding DNN predictions to skim

_Disclaimer: Technically this is skim with appended scripts for adding the prediction but it runs on the batch and skimming is really fast_

The script `FHDownstream/nTupleProcessing/classification/addDNN.py` uses the modules from `FHDownstream/nTupleProcessing/classification/processModules.py` to recalculate the input variables on and event-by-event basis, transfrom them _(according to the transformation file in the DNN training output directory)_
and evaluate the model. The prediction of the node set with the `--signalNode` agrument is saved to a branch called like the passed `--branchNames` arguement.

Currently the script args are set up so that there is a base folder for a DNN version and subcategories called `7J`, `8J` and `9J`.

To run over all samples you the GC config: `FHDownstream/gc/confs/projectSkimFH_wDNN.conf`

### 2.2 Running the sparsinator (template production) with DNN prediction

Setting if and what classifier is run in the sparsinator is set in the tthbb13 configuration file `MEAnalysis/data/default.cfg` (or child configs like `MEAnalysis/data/config_FH.cfg`) by setting the `addDNNPrediction` option in the `sparsinator` section to `True`. The sparinator will then try to load the  models from the folder defined in the `fhDNNModels` section of the config. There is also a option for only running on half of ttHbb with `evt%2 == 1` called `runEvenTTH`. Please also update the btagging WP in the sparsinator section. They are required for the variable calculation.

In the sparsinator all model will be loaded. The prediction is calculated in the `createEvent` function after event selection (as they are defined in the `cut` option of the enabled processes). Up to three ouput nodes will be saved in the event as attributes `DNNPred_Node0`, `DNNPred_Node1` and `DNNPred_Node2`. Which should be used as discriminator can be set in the `discriminator` option of an enabled process in the config.     
It is also setup to add all input variables to the event's attributes with the following naming schema `DNNInput_*`. Since the histogram filling requires some hardcoding new variables ahve to be added the `FUNCTION_TABLE` dict in `Plotting/python/Datacards/AnalysisSpecificationClasses.py`. After that the can be added to the templates (with full systematics) to the `control_variables`of an enabled process in the config.

As usual, run the sparsinator on the grid e.g. with `FHDownstream/gc/confs/sparse_SR.cfg` and `FHDownstream/gc/confs/sparse_CR.cfg`. Since we do not use the QGLR anymore with the current analysis stragy, the `VR` and `CR2` are not required to run the analysis. They are only needed if we want to produce VR plots with the full systematic model.

#### General postprocessing steps.

After this the regular template postprocessing steps are required:

- Merge the output files per region into one root file (usually called `merged_SR.root` or `merged_CR.root`)
- Run `FHDownstream/Datacards/Add_ddQCDv2.py` (with a new configuration file according to the newly created templates) to get the ddQCD prediction
- Use `Plotting/python/Datacards/LaunchAll_FH.py` to generate datacards.


## 3 Validation

### 3.1 Base validation with systematic uncertainty only

This requires a skim with added DNN branch (see [Section 2.1](#21-adding-dnn-predictions-to-skim)). The validation plots (or SR blinded or unblinded) for any given branch of the tree can be create using `FHDownsteam/Plotting/QCD_estimate_v2.py`. It operates based on config files like `FHDownstream/data/QCD_estimate_2017_DNN.cfg`. The variables and categories can be set with the arguments `--vars` and `--cats`, respectively. The script also supports replotting histograms projected in a previous run by passing `--rerunFile` (requires `--tag` to be same as before).

### 3.2 Input variables from sparsinator

As described in [Section 2.2](#22-running-the-sparsinator-template-production-with-dnn-prediction), the input variables (present in the current iteration of the DNN) are added to the event and corresonding histograms will be filled if enabled in the config.     
In the current version of `FHDownstream/Datacards/Add_ddQCDv2.py` and `Plotting/python/Datacards/LaunchAll_FH.py` on the DNN output is processed. Add the input variables to the config for `Add_ddQCDv2.py` so the ddQCD shape is generated. Then `LaunchAll_FH.py` can also generate datacards for these variables (might need to remove some `if` or `continue` statement). Check `Plotting/python/Datacards/LaunchAll_FH_VR.py` (untested in current version. I think it only changes some names and input but is otherwise identical to `LaunchAll_FH.py`). Running combine on these datacards will generate the variables distributions with the full systematic model. It should be enough to look at the prefit distributions since we are not really interesten in fitting the the actual data. For combine setup follow e.g. [ttH reference](https://gitlab.cern.ch/ttH/reference/blob/LegacyRun2/definitions/Legacy2018.md).

Check the script `FHDownstream/runAll.sh` for an example script of running a bunch of combine jobs with a script. But running something like

```bash
text2workspace.py ttH_hbb_13TeV_2017_fh_8j_4t.txt

combine ttH_hbb_13TeV_2017_fh_8j_4t.root -n _asimov_sig_8j4t_repoSettings_Tolp1  --cminDefaultMinimizerTolerance 0.1  -M FitDiagnostics  -m 125 --cminDefaultMinimizerStrategy 0 --cminPreScan  --saveNormalizations --saveShapes --saveOverallShapes  --rMin -5 --rMax 5  -t -1 --expectSignal 1
```
should be a good start.

Exeact procedure form this point has to be determined. E.g. what criteria we used to quantify if a variable has good enough agreement.
