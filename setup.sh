git submodule init
git submodule update

# This is the place for setting up various subfunctionalities
ln -s $CMSSW_BASE/src/TTH/FHDownstream/utils/crabTools.py $CMSSW_BASE/src/TTH/MEAnalysis/crab_nano/crabTools.py
# Link GC go.py file to gc directory
ln -s /swshare/grid-control/go.py $CMSSW_BASE/src/TTH/FHDownstream/gc/go.py 
# Create script for setting the environment on the batch system
cd gc
source makeEnv.sh
cd -
