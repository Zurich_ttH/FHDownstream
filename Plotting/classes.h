#include "TFile.h"
#include "TH3F.h"

class TriggerSF3D {
 public:
  TriggerSF3D( );
  ~TriggerSF3D( );

  float getTriggerWeight( float ht, float jet_pt, int nB );
  
 private:
  TH3F *h_bin_0;
};


  //PUBLIC
  TriggerSF3D::TriggerSF3D( ){

    const char * histoInputFile = "/work/koschwei/tth/2018Data/update2018/v2/CMSSW_10_2_15/src/TTH/FHDownstream/Plotting/TriggerPlots_ttH_AH_v5/sf3d_out_3DSF_JEC32_looseCut_All_CSV.root";
    //const char * histoInputFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/CMSSW_9_4_9/src/TTH/FHDownstream/Plotting/TriggerPlots_ttH_AH_newSkim_v2p1/sf3d_out_3DSF_Final_looseCut_All_CSV_updated.root";
    //const char * histoInputFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/CMSSW_9_4_9/src/TTH/Plotting/Daniel/TriggerPlots_ttH_AH_v2/sf3d_out_3DSF_FullDataset_tightCut_wPuGen_ht30.root";
    TFile *sfFile = new TFile(histoInputFile, "READ");

    //h_bin_0 = (TH3F*)sfFile->Get("h3SF_tot_mod");
    h_bin_0 = (TH3F*)sfFile->Get("h3SF_tot");
    
  }

  TriggerSF3D::~TriggerSF3D( ){

  }

  float TriggerSF3D::getTriggerWeight( float ht, float jet_pt,  int nB){

    float SF = 1;
    //if (nB == 0){
    //  nB = 1;
    //}
    int bin = h_bin_0->FindBin(ht, jet_pt, nB);
    SF = h_bin_0->GetBinContent(bin);

    if (SF == 0){
      SF = 1;
    }
    return SF;
  }


class bTagNormalization {
 public:
  bTagNormalization( string folder, string tag, string combined );
  ~bTagNormalization( );

  float getbTagNormalization( string Sample, int nJets, float ht );
  float getMaxHT( TH2F* hSF, float passHT);
  
  
 private:
  TFile *rSF;
  TH2F *H_ttbb_SL;
  TH2F *H_ttbb_inc_SL;
  TH2F *H_ttbb_DL;
  TH2F *H_ttbb_inc_DL;
  TH2F *H_ttbb_FH;
  TH2F *H_ttbb_inc_FH;
  TH2F *H_ttcc_SL;
  TH2F *H_ttcc_DL;
  TH2F *H_ttcc_FH;
  TH2F *H_ttlf_SL;
  TH2F *H_ttlf_DL;
  TH2F *H_ttlf_FH;
  TH2F *H_ttHbb;
  TH2F *H_ttHnonbb;
  TH2F *H_QCD200;
  TH2F *H_QCD300;
  TH2F *H_QCD500;
  TH2F *H_QCD700;
  TH2F *H_QCD1000;
  TH2F *H_QCD1500;
  TH2F *H_QCD2000;
  TH2F *H_st_s_lep;
  TH2F *H_st_s_had;
  TH2F *H_st_t;
  TH2F *H_stbar_t;
  TH2F *H_st_tW;
  TH2F *H_stbar_tW;
  TH2F *H_tHq;
  TH2F *H_tHW;
  TH2F *H_ttZ;
  TH2F *H_ttW;
  
};


//PUBLIC
bTagNormalization::bTagNormalization(string folder, string tag, string combined){
  string name = folder+"/Histos_"+tag+".root";

  std::cout << "Loading file: " << name << std::endl;

  
  TFile *rSF = new TFile(name.c_str() , "READ");
  if(combined == "True"){
    std::cout << "================ USING COMBINED SFs ================" << std::endl;
    H_ttbb_SL = (TH2F*)rSF->Get("hSF_TTbb_Combined");
    H_ttbb_DL = (TH2F*)rSF->Get("hSF_TTbb_Combined");
    H_ttbb_FH = (TH2F*)rSF->Get("hSF_TTbb_Combined");
  
    H_ttbb_inc_SL = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttbb");
    H_ttbb_inc_DL = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttbb");
    H_ttbb_inc_FH = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttbb");
    
    H_ttcc_SL = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttcc");
    H_ttcc_DL = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttcc");
    H_ttcc_FH = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttcc");
    
    H_ttlf_SL = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttlf");
    H_ttlf_DL = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttlf");
    H_ttlf_FH = (TH2F*)rSF->Get("hSF_TTIncl_Combined__ttlf");
    
    H_ttHbb = (TH2F*)rSF->Get("hSF_ttH_Combined");
    H_ttHnonbb = (TH2F*)rSF->Get("hSF_ttH_Combined");
  }
  else {
    H_ttbb_SL = (TH2F*)rSF->Get("hSF_TTbb_4f_TTToSemiLeptonic_TuneCP5-Powheg-Openloops-Pythia8");
    H_ttbb_DL = (TH2F*)rSF->Get("hSF_TTbb_4f_TTTo2l2nu_TuneCP5-Powheg-Openloops-Pythia8");
    H_ttbb_FH = (TH2F*)rSF->Get("hSF_TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8");
  
    H_ttbb_inc_SL = (TH2F*)rSF->Get("hSF_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8__ttbb");
    H_ttbb_inc_DL = (TH2F*)rSF->Get("hSF_TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8__ttbb");
    H_ttbb_inc_FH = (TH2F*)rSF->Get("hSF_TTToHadronic_TuneCP5_13TeV-powheg-pythia8__ttbb");
    
    H_ttcc_SL = (TH2F*)rSF->Get("hSF_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8__ttcc");
    H_ttcc_DL = (TH2F*)rSF->Get("hSF_TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8__ttcc");
    H_ttcc_FH = (TH2F*)rSF->Get("hSF_TTToHadronic_TuneCP5_13TeV-powheg-pythia8__ttcc");
    
    H_ttlf_SL = (TH2F*)rSF->Get("hSF_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8__ttlf");
    H_ttlf_DL = (TH2F*)rSF->Get("hSF_TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8__ttlf");
    H_ttlf_FH = (TH2F*)rSF->Get("hSF_TTToHadronic_TuneCP5_13TeV-powheg-pythia8__ttlf");

    H_ttHbb = (TH2F*)rSF->Get("hSF_ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8");
    H_ttHnonbb = (TH2F*)rSF->Get("hSF_ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8");
  }
  H_QCD200  = (TH2F*)rSF->Get("hSF_QCD_HT200to300_TuneCP5_13TeV-madgraph-pythia8");
  H_QCD300  = (TH2F*)rSF->Get("hSF_QCD_HT300to500_TuneCP5_13TeV-madgraph-pythia8");
  H_QCD500  = (TH2F*)rSF->Get("hSF_QCD_HT500to700_TuneCP5_13TeV-madgraph-pythia8");
  H_QCD700  = (TH2F*)rSF->Get("hSF_QCD_HT700to1000_TuneCP5_13TeV-madgraph-pythia8");
  H_QCD1000  = (TH2F*)rSF->Get("hSF_QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8");
  H_QCD1500  = (TH2F*)rSF->Get("hSF_QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8");
  H_QCD2000  = (TH2F*)rSF->Get("hSF_QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8");
  H_st_s_lep  = (TH2F*)rSF->Get("hSF_ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8");
  H_st_s_had  = (TH2F*)rSF->Get("hSF_ST_s-channel_4f_hadronicDecays_TuneCP5_13TeV-amcatnlo-pythia8");
  H_st_t  = (TH2F*)rSF->Get("hSF_ST_t-channel_top_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8");
  H_stbar_t  = (TH2F*)rSF->Get("hSF_ST_t-channel_antitop_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8");
  H_st_tW  = (TH2F*)rSF->Get("hSF_ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8");
  H_stbar_tW  = (TH2F*)rSF->Get("hSF_ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8");
  H_tHq  = (TH2F*)rSF->Get("hSF_THQ_ctcvcp_HIncl_M125_TuneCP5_13TeV-madgraph-pythia8");
  H_tHW  = (TH2F*)rSF->Get("hSF_THW_ctcvcp_HIncl_M125_TuneCP5_13TeV-madgraph-pythia8");
  H_ttZ  = (TH2F*)rSF->Get("hSF_TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8");
  H_ttW  = (TH2F*)rSF->Get("hSF_TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8");
}

bTagNormalization::~bTagNormalization(){ };

float bTagNormalization::getbTagNormalization( string Sample, int nJets, float ht ){

  int bin = -1;
  float SF = -1;

  if (nJets >= 10){
    nJets = 9;
  }

  //std::cout << "ht = " << ht << std::endl;
  if (Sample == "ttbb_SL"){
    SF = H_ttbb_SL->GetBinContent(H_ttbb_SL->FindBin(nJets, getMaxHT(H_ttbb_SL, ht)));
  }
  else if (Sample == "ttbb_inc_SL"){
    SF = H_ttbb_inc_SL->GetBinContent(H_ttbb_inc_SL->FindBin(nJets, getMaxHT(H_ttbb_inc_SL, ht)));
  }
  else if (Sample == "ttbb_DL"){
    SF = H_ttbb_DL->GetBinContent(H_ttbb_DL->FindBin(nJets, getMaxHT(H_ttbb_DL, ht)));
  }
  else if (Sample == "ttbb_inc_DL"){
    SF = H_ttbb_inc_DL->GetBinContent(H_ttbb_inc_DL->FindBin(nJets, getMaxHT(H_ttbb_inc_DL, ht)));
  }
  else if (Sample == "ttbb_FH"){
    SF = H_ttbb_FH->GetBinContent(H_ttbb_FH->FindBin(nJets, getMaxHT(H_ttbb_FH, ht)));
  }
  else if (Sample == "ttbb_inc_FH"){
    SF = H_ttbb_inc_FH->GetBinContent(H_ttbb_inc_FH->FindBin(nJets, getMaxHT(H_ttbb_inc_FH, ht)));
  }
  else if (Sample == "ttcc_SL"){
    SF = H_ttcc_SL->GetBinContent(H_ttcc_SL->FindBin(nJets, getMaxHT(H_ttcc_SL, ht)));
  }
  else if (Sample == "ttcc_DL"){
    SF = H_ttcc_DL->GetBinContent(H_ttcc_DL->FindBin(nJets, getMaxHT(H_ttcc_DL, ht)));
  }
  else if (Sample == "ttcc_FH"){
    SF = H_ttcc_FH->GetBinContent(H_ttcc_FH->FindBin(nJets, getMaxHT(H_ttcc_FH, ht)));
  }
  else if (Sample == "ttlf_SL"){
    SF = H_ttlf_SL->GetBinContent(H_ttlf_SL->FindBin(nJets, getMaxHT(H_ttlf_SL, ht)));
  }
  else if (Sample == "ttlf_DL"){
    SF = H_ttlf_DL->GetBinContent(H_ttlf_DL->FindBin(nJets, getMaxHT(H_ttlf_DL, ht)));
  }
  else if (Sample == "ttlf_FH"){
    SF = H_ttlf_FH->GetBinContent(H_ttlf_FH->FindBin(nJets, getMaxHT(H_ttlf_FH, ht)));
  }
  else if (Sample == "ttHbb"){
    SF = H_ttHbb->GetBinContent(H_ttHbb->FindBin(nJets, getMaxHT(H_ttHbb, ht)));
  }
  else if (Sample == "ttHnonbb"){
    SF = H_ttHnonbb->GetBinContent(H_ttHnonbb->FindBin(nJets, getMaxHT(H_ttHnonbb, ht)));
  }
  else if (Sample == "QCD200") {
    SF = H_QCD200->GetBinContent(H_QCD200->FindBin(nJets, getMaxHT(H_QCD200, ht)));
  }
  else if (Sample == "QCD300") {
    SF = H_QCD300->GetBinContent(H_QCD300->FindBin(nJets, getMaxHT(H_QCD300, ht)));
  }
  else if (Sample == "QCD500") {
    SF = H_QCD500->GetBinContent(H_QCD500->FindBin(nJets, getMaxHT(H_QCD500, ht)));
  }
  else if (Sample == "QCD700") {
    SF = H_QCD700->GetBinContent(H_QCD700->FindBin(nJets, getMaxHT(H_QCD700, ht)));
  }
  else if (Sample == "QCD1000") {
    SF = H_QCD1000->GetBinContent(H_QCD1000->FindBin(nJets, getMaxHT(H_QCD1000, ht)));
  }
  else if (Sample == "QCD1500") {
    SF = H_QCD1500->GetBinContent(H_QCD1500->FindBin(nJets, getMaxHT(H_QCD1500, ht)));
  }
  else if (Sample == "QCD2000") {
    SF = H_QCD2000->GetBinContent(H_QCD2000->FindBin(nJets, getMaxHT(H_QCD2000, ht)));
  }
  else if (Sample == "st_s_lep") {
    SF = H_st_s_lep->GetBinContent(H_st_s_lep->FindBin(nJets, getMaxHT(H_st_s_lep, ht)));
  }
  else if (Sample == "st_s_had") {
    SF = H_st_s_had->GetBinContent(H_st_s_had->FindBin(nJets, getMaxHT(H_st_s_had, ht)));
  }
  else if (Sample == "st_t") {
    SF = H_st_t->GetBinContent(H_st_t->FindBin(nJets, getMaxHT(H_st_t, ht)));
  }
  else if (Sample == "stbar_t") {
    SF = H_stbar_t->GetBinContent(H_stbar_t->FindBin(nJets, getMaxHT(H_stbar_t, ht)));
  }
  else if (Sample == "st_tW") {
    SF = H_st_tW->GetBinContent(H_st_tW->FindBin(nJets, getMaxHT(H_st_tW, ht)));
  }
  else if (Sample == "stbar_tW") {
    SF = H_stbar_tW->GetBinContent(H_stbar_tW->FindBin(nJets, getMaxHT(H_stbar_tW, ht)));
  }
  else if (Sample == "tHq") {
    SF = H_tHq->GetBinContent(H_tHq->FindBin(nJets, getMaxHT(H_tHq, ht)));
  }
  else if (Sample == "tHW") {
    SF = H_tHW->GetBinContent(H_tHW->FindBin(nJets, getMaxHT(H_tHW, ht)));
  }
  else if (Sample == "ttZ") {
    SF = H_ttZ->GetBinContent(H_ttZ->FindBin(nJets, getMaxHT(H_ttZ, ht)));
  }
  else if (Sample == "ttW") {
    SF = H_ttW->GetBinContent(H_ttW->FindBin(nJets, getMaxHT(H_ttW, ht)));
  }
  
  else {
    std::cout << "SOMETHING IS WRONG IN THE BTAG WEIGHT NAME -- "<< Sample << std::endl;
    SF = 1;
  }
  return SF;   
  }

float bTagNormalization::getMaxHT(TH2F* hSF, float passHT){
  if (passHT > hSF->GetYaxis()->GetXmax()){
    return hSF->GetYaxis()->GetXmax();
  }
  else {
    return passHT;
  }
}




class TriggerSFMulitEra {
 public:
  TriggerSFMulitEra( string era, string tagger );
  ~TriggerSFMulitEra( );

  float getTriggerWeight( float ht, float jet_pt, int nB );
  
 private:
  TH3F *h_bin_0;
};


//PUBLIC
TriggerSFMulitEra::TriggerSFMulitEra( string era, string tagger){
  const char * histoInputFile = "/path/to/file.root";
  const char * hName;
  if (era == "2018"){
    if (tagger == "DeepCSV"){
      histoInputFile =  "/work/koschwei/tth/2018Data/update2018/v2/CMSSW_10_2_15/src/TTH/FHDownstream/Plotting/TriggerPlots_ttH_AH_v5/sf3d_out_3DSF_JEC32_looseCut_All_CSV.root";
    }
    else {
      histoInputFile =  "/work/koschwei/tth/2018Data/update2018/v2/CMSSW_10_2_15/src/TTH/FHDownstream/Plotting/TriggerPlots_LegacyRun2/2018/DeepFlav/mod_sf3d_out_3DSF_v1_2018_DeepFlav_tightCut.root";
      hName = "h3SF_tot_mod";
    }
  }
  else if (era == "2017"){
    if (tagger == "DeepCSV"){
      histoInputFile =  "/work/koschwei/tth/2018Data/update2018/v2/CMSSW_10_2_15/src/TTH/FHDownstream/Plotting/TriggerPlots_ttH_AH_v5/sf3d_out_3DSF_JEC32_looseCut_All_CSV.root";
    }
    else {
      histoInputFile =  "/work/koschwei/tth/2018Data/update2018/v2/CMSSW_10_2_15/src/TTH/FHDownstream/Plotting/TriggerPlots_LegacyRun2/2017/DeepFlav/mod_sf3d_out_3DSF_v1_2017_DeepFlav_tightCut.root";
      hName = "h3SF_tot_mod";
    }
  }
  else {
    if (tagger == "DeepCSV"){
      histoInputFile =  "/work/koschwei/tth/2018Data/update2018/v2/CMSSW_10_2_15/src/TTH/FHDownstream/Plotting/TriggerPlots_ttH_AH_v5/sf3d_out_3DSF_JEC32_looseCut_All_CSV.root";
    }
    else {
      histoInputFile =  "/work/koschwei/tth/2018Data/update2018/v2/CMSSW_10_2_15/src/TTH/FHDownstream/Plotting/TriggerPlots_LegacyRun2/2016/DeepFlav/mod_sf3d_out_3DSF_v1_2016_DeepFlav_tightCut.root";
      hName = "h3SF_tot_mod";
    }
  }

  
  TFile *sfFile = new TFile(histoInputFile, "READ");

  cout << histoInputFile << endl;
  
  //h_bin_0 = (TH3F*)sfFile->Get("h3SF_tot_mod");
  h_bin_0 = (TH3F*)sfFile->Get(hName);
  
}

TriggerSFMulitEra::~TriggerSFMulitEra( ){
  
}

float TriggerSFMulitEra::getTriggerWeight( float ht, float jet_pt,  int nB){
  
  float SF = 1;
  //if (nB == 0){
  //  nB = 1;
  //}
  int bin = h_bin_0->FindBin(ht, jet_pt, nB);
  SF = h_bin_0->GetBinContent(bin);
  
  if (SF == 0){
    SF = 1;
  }
  return SF;
}
