import logging
from copy import deepcopy
import ROOT

from classes.ratios import RatioPlot
from classes.roc import ROC
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def getFitDiagnosticsHistos(filename, postfit = True):
    rfile = ROOT.TFile.Open(filename)

    FitDiagnosticsChannels = {
        "j7t3" : "ttH_hbb_13TeV_2017_fh_7j3b_MEM",
        "j7t4" : "ttH_hbb_13TeV_2017_fh_7j4b_MEM",
        "j8t3" : "ttH_hbb_13TeV_2017_fh_8j3b_MEM",
        "j8t4" : "ttH_hbb_13TeV_2017_fh_8j4b_MEM",
        "j9t3" : "ttH_hbb_13TeV_2017_fh_9j3b_MEM",
        "j9t4" : "ttH_hbb_13TeV_2017_fh_9j4b_MEM"
    }
    histos = {"j7t3" : {},
              "j7t4" : {},
              "j8t3" : {},
              "j8t4" : {},
              "j9t3" : {},
              "j9t4" : {}}
    if postfit:
        topfolder = "shapes_fit_s"
    else:
        topfolder = "shapes_prefit"

    #GetProcesses:
    processes = []
    for process in rfile.GetDirectory(topfolder+"/ttH_hbb_13TeV_2017_fh_7j3b_MEM/").GetListOfKeys():
        processes.append(process.GetName())
    print processes

    for cat in histos:
        logging.info("Getting histograms for %s", cat)
        for process in processes:
            logging.debug("Getting histogram for %s/%s", cat, process)
            histos[cat][process] = deepcopy(rfile.Get(topfolder+"/"+FitDiagnosticsChannels[cat]+"/"+process))

    # for cat in histos:
    #     for process in processes:
    #         histos[cat][process].SetSum
    return histos


def getComparison(histos1, histos2, process, postfit):
    logging.info("Making plot for %s", process)
    for cat in histos1.keys():
        logging.info("Processing category %s", cat)

        output = RatioPlot(name = "Comp_{0}_{1}".format(cat, process))
        output.ratioRange =  (0.98,3.3)
        output.useDefaultColors = True
        #output.legendSize = (0.6, 0.3, 0.9, 0.77)
        #output.yTitleOffset = 0.99
        #output.yTitleOffsetRatio = 0.45
        output.addLabel("Post-fit" if postfit else "Pre-fit", 0.16, 0.955, 1.2)
        output.addLabel(Helper.CatLabels[cat], 0.16, 0.87, 1.2)
        output.addLabel(Helper.legnames[process], 0.7, 0.955, 1.2)

        output.yTitle = "Events / Category"
        output.passHistos([histos1[cat][process], histos2[cat][process]])
        thisCanvas = output.drawPlot(["w/ #Delta#eta_{J} cut", "w/o #Delta#eta_{J} cut"],
                                     "MEM Disciminant"                                    
        )
        

        saveCanvasListAsPDF([thisCanvas], "DetaJ_cutComp_{0}_fitDiagnostics{1}".format(cat, process),".")


def getMultiComp(histos_1, histos_2, processes, legend, postfit):
    colors = []
    for proc in processes:
        colors.append(Helper.colors[proc])
    for cat in histos_1.keys():
        logging.info("Processing category %s", cat)
        allHistos = []
        allLegends = []
        for iproc, proc in enumerate(processes):
            thisH_1 = histos_1[cat][proc]
            thisH_2 = histos_2[cat][proc]
            thisH_1.SetLineColor(colors[iproc])
            thisH_2.SetLineColor(colors[iproc])
            thisH_2.SetLineStyle(2)
            allHistos.append(thisH_1)
            allHistos.append(thisH_2)
            allLegends.append("{0} - {1}".format(legend[0], proc))
            allLegends.append("{0} - {1}".format(legend[1], proc))
            
        output = RatioPlot(name = "MultiComp_{0}".format(cat))
        output.ratioRange =  (0.51,1.49)
        output.legendSize = (0.6, 0.5, 0.9, 0.9)
        #output.yTitleOffset = 0.99
        #output.yTitleOffsetRatio = 0.45
        #output.invertRatio = True
        output.addLabel("Post-fit" if postfit else "Pre-fit", 0.16, 0.955, 1.2)
        output.addLabel(Helper.CatLabels[cat], 0.16, 0.87, 1.2)

        output.yTitle = "Events / Category"
        output.passHistos(allHistos, normalize = True)
        thisCanvas = output.drawPlot(allLegends,
                                     "MEM Disciminant",
                                     groupRatios = True
                                     
        )
        

        saveCanvasListAsPDF([thisCanvas], "MultiComp_{0}_fitDiagnostics".format(cat),".")




if __name__ == "__main__":
    fitDiagnostics_1 = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p3/Datacards/sparsev5_final_v2_unblind_V5_L1Pre/cards/fitDiagnostics_asimov_sig_fh_repoSettings_Tolp1.root"
    fitDiagnostics_2 = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p3/Datacards/sparsev5_final_v3_noDeta_unblind_V5_L1Pre/cards/fitDiagnostics_asimov_sig_fh_repoSettings_Tolp1.root"
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--postfit",
        action = "store_true",
        help = "",
    )
    argumentparser.add_argument(
        "--singleComp",
        action = "store_true",
        help = "",
    )
    argumentparser.add_argument(
        "--multiComp",
        action = "store_true",
        help = "",
    )
    argumentparser.add_argument(
        "--processes",
        action = "store",
        type = str,
        nargs = "+",
        required = True,
        help = "Process to be compared",
    )
    argumentparser.add_argument(
        "--legendNames",
        action = "store",
        type = str,
        nargs = "+",
        help = "Names for legend",
    )
    args = argumentparser.parse_args()
    ##############################################################################################################
    initLogging(args.loglevel)
    logging.info("Starting script")
    histos_1 = getFitDiagnosticsHistos(fitDiagnostics_1, postfit = args.postfit)
    histos_2 = getFitDiagnosticsHistos(fitDiagnostics_2, postfit = args.postfit)


    
    if args.singleComp:
        logging.info("RUnning single comp")
        for proc in args.processes:
            getComparison(histos_1, histos_2, proc, args.postfit)

    if args.multiComp:
        assert len(args.legendNames) == 2
        logging.info("RUnning multi comp")
        getMultiComp(histos_1, histos_2, args.processes, args.legendNames, args.postfit)
    logging.info("Exiting script")
