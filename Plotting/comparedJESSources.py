import logging

import ROOT

from copy import copy

import classes.ratios as rb
from classes.PlotHelpers import saveCanvasListAsPDF, initLogging

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def compareHistos(inputFiles, outputName, outputFolder = ".", nameFilters = None, legendElements = None, invertUpAndDown = False):
    if len(inputFiles) != 2 and invertUpAndDown:
        raise NotImplementedError
    
    rFiles = {}
    keys = {}
    for inputFile in inputFiles:
        rFiles[inputFile] = ROOT.TFile.Open(inputFile)
        keys[inputFile] = []
        for key in rFiles[inputFile].GetListOfKeys():
            keys[inputFile].append(key.GetName())

    legendElements_passed = copy(legendElements)
            
    legendElements_ = []
    if legendElements is None:
        for f in inputFiles:
            legendElements_.append(f.split("/")[-1].split(".")[0])
    else:
        legendElements_ = legendElements
    legendElements = legendElements_
    logging.debug("legendElements %s", legendElements)


    pairs = []
    # Find plot pairs
    for hName in keys[inputFiles[0]]:
        if not all(hName in keys[inputFile] for inputFile in inputFiles):
            logging.debug("%s not in all input files. Skipping", hName)
            continue
        if nameFilters is not None:
            if not all(nameFilter in hName for nameFilter in nameFilters):
                logging.debug("Skipping %s because not passing name filter")
                continue

        if invertUpAndDown:
            if hName.endswith("Up"):
                pairs.append( [hName, hName.replace("Up", "Down")] )
            else:
                pairs.append( [hName, hName.replace("Down", "Up")] )
        else:
            pairs.append( len(inputFiles)*[hName] )
        

    allCanvases = []
    
    logging.info("Starting loop for plots")
    for histoNames in pairs:
        logging.debug("Creating histo list for ratio for %s", histoNames)
        hList = []
        for iName,hName in enumerate(histoNames):
            hList.append(rFiles[inputFiles[iName]].Get(hName))
        logging.debug("hList: %s", hList)            
        thisRatio = rb.RatioPlot("CompareHistos_{0}".format(hName))
        thisRatio.useDefaultColors = True
        thisRatio.ratioRelUncertaintySelf = True
        if invertUpAndDown:
            if "Up" in histoNames[0]:
                labelText = "{} is Up and {} is Down".format(legendElements_passed[0], legendElements_passed[1])
            if "Down" in histoNames[0]:
                labelText = "{} is Down and {} is up".format(legendElements_passed[0], legendElements_passed[1])
            thisRatio.addLabel(labelText, 0.2, 0.83)
        
        thisRatio.ratioRange = (0.95, 1.05)
        thisRatio.passHistos(hList)
        allCanvases.append(thisRatio.drawPlot(legendElements, plotTitle = hName))

    logging.info("Get %s canvases", len(allCanvases))
    logging.info("Saving output")
    saveCanvasListAsPDF(allCanvases, outputName, outputFolder)
        

def compareJESSources(inputFiles, outputName, sources, outputFolder = "." , legendElements = None, invertUpAndDown = False):
    sourcePairs = [sources[i:i + 2] for i in range(0, len(sources), 2)]  

    rFiles = {}
    keys = {}
    for inputFile in inputFiles:
        rFiles[inputFile] = ROOT.TFile.Open(inputFile)
        keys[inputFile] = []
        for key in rFiles[inputFile].GetListOfKeys():
            keys[inputFile].append(key.GetName())

    
    for i, sources in enumerate(sourcePairs):
        sourceA, sourceB = sources
        relevantKeysA = sorted([key for key in keys[inputFiles[0]] if sourceA in key])
        relevantKeysB = sorted([key for key in keys[inputFiles[1]] if sourceB in key])

        checkKeysA = []
        for key in relevantKeysA:
            if key.replace(sourceA, sourceB) in relevantKeysB:
                checkKeysA.append(key)
            else:
                logging.info("Could not find corresponding histos for: %s", key)

        logging.info("Will plot %s tempaltes", len(checkKeysA))

        pairs = []
        for key in checkKeysA:
            if invertUpAndDown:
                if key.endswith("Up"):
                    pairs.append( [key, key.replace(sourceA, sourceB).replace("Up","Down")] )
                else:
                    pairs.append( [key, key.replace(sourceA, sourceB).replace("Down","Up")] )
            else:
                pairs.append( [key, key.replace(sourceA, sourceB)] )

        allCanvases = []
    
        logging.info("Starting loop for plots")
        for histoNames in pairs:
            logging.debug("Creating histo list for ratio for %s", histoNames)
            hList = []
            for iName,hName in enumerate(histoNames):
                hList.append(rFiles[inputFiles[iName]].Get(hName))
            logging.debug("hList: %s", hList)            
            thisRatio = rb.RatioPlot("CompareHistos_{0}".format(hName))
            thisRatio.useDefaultColors = True
            thisRatio.ratioRelUncertaintySelf = True
            if invertUpAndDown:
                if "Up" in histoNames[0]:
                    labelText = "{} is Up and {} is Down".format(legendElements[0], legendElements[1])
                if "Down" in histoNames[0]:
                    labelText = "{} is Down and {} is up".format(legendElements[0], legendElements[1])
                thisRatio.addLabel(labelText, 0.2, 0.83)

            thisRatio.ratioRange = (0.95, 1.05)
            thisRatio.passHistos(hList)
            allCanvases.append(thisRatio.drawPlot(legendElements, plotTitle = hName))

        logging.info("Get %s canvases", len(allCanvases))
        logging.info("Saving output")
        saveCanvasListAsPDF(allCanvases, outputName+"__"+sourceA+"_"+sourceB, outputFolder)

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        help = "Input files (At least 2 would be great",
        type = str,
        nargs="+",
        required = True
    )
    argumentparser.add_argument(
        "--compareSources",
        action = "store",
        help = "Pass multiple of 2",
        type = str,
        nargs="+",
        required = True
    )
    argumentparser.add_argument(
        "--inputsLegend",
        action = "store",
        help = "Optional legend text for plots per input file. If not set, filename will be used",
        type = str,
        nargs="+",
        default = None
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        help = "File name of the output file (will append .pdf)",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outputfolder",
        action = "store",
        help = "File name of the output file (will append .pdf)",
        type = str,
        default = "."
    )
    argumentparser.add_argument(
        "--nameFilters",
        action = "store",
        help = "String that is used to filter histograms. Technically it will look for this substring in the histoname",
        type = str,
        nargs = "+",
        default = None
    )

    argumentparser.add_argument(
        "--invertUpAndDown",
        action = "store_true",
        help = "Please only pass if there are Up/Down variations and 2 inputs. If passed the script will try to flow Down and up of the second sample",
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging)

    if len(args.inputs) != 2:
        raise RuntimeError("Pass exactly 2 input files")

    if len(args.compareSources)%2 != 0:
        raise Exception("Pass a multiple of 2 for --compareSources")
    
    if args.inputsLegend is not None:
        if len(args.inputs) != len(args.inputsLegend):
            raise Exception("If --inputsLegend is set it requires to have the same number of elements as inputs")
    
    #compareHistos(args.inputs, args.output, args.outputfolder, args.nameFilters, args.inputsLegend, args.invertUpAndDown)
    compareJESSources(args.inputs, args.output, args.compareSources, args.outputfolder, args.inputsLegend, args.invertUpAndDown)
    logging.info("Exiting")
