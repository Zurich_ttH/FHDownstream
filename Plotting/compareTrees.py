import sys
import os

from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, checkNcreateFolder, project
import ROOT
import logging
from copy import deepcopy

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)


def main(inputFiles, legendElements, treeName, folder, outputName, ignoreBranches, selectBranches):
    logging.info("============= GOT =============")
    logging.info(" inputFiles : %s", inputFiles)
    logging.info(" legendElements : %s", legendElements)
    logging.info(" treeName : %s", treeName)
    logging.info(" folder : %s", folder)
    logging.info(" outputName : %s", outputName)
    logging.info(" ignoreBranches : %s", ignoreBranches)
    logging.info(" selectBranches : %s", selectBranches)
    logging.info("===============================")
    rFiles = []
    for i, inputFile in enumerate(inputFiles):
        rFiles.append(ROOT.TFile.Open(inputFile))
        logging.info("Got input file: %s",rFiles[i])

    trees = []
    for i, rFile in enumerate(rFiles):
        trees.append(rFile.Get(treeName))
        logging.info("Got tree: %s", trees[i])

    branches = []
    for i, tree, in enumerate(trees):
        if i == 0:
            for x in tree.GetListOfBranches():
                bName = x.GetName()
                if selectBranches:
                    if bName not in selectBranches:
                        logging.debug("Selecting  %s", bName)
                        continue
                else:
                    if bName in ignoreBranches:
                        logging.debug("Skipping %s", bName)
                        continue
                branches.append(bName)
            logging.debug("Initial brnaches: %s", branches)
        else:
            theseBranches = []
            for x in tree.GetListOfBranches():
                bName = x.GetName()
                if selectBranches:
                    if bName not in selectBranches:
                        continue
                else:
                    if bName in ignoreBranches:
                        continue
                theseBranches.append(bName)

            if theseBranches != branches:
                raise RuntimeError("Tree %s has different branches to first tree"%tree)

    if selectBranches:
        for b in selectBranches:
            if b not in branches:
                logging.warning("Could not find %s in tree", b)


    for branch in branches:
        canvases = []
        thisBranchHistos = []

        nBins = -1
        edgeL = -1
        edgeR = -1
        
        for i, tree in enumerate(trees):
            drawcommand = "{0}>>h{0}_temp".format(branch)
            logging.debug(drawcommand)
            tree.Draw(drawcommand)
            nBins_ = ROOT.gDirectory.Get("h{0}_{1}".format(branch, "temp")).GetNbinsX()
            edgeL_ = int(ROOT.gDirectory.Get("h{0}_{1}".format(branch, "temp")).GetBinLowEdge(1))
            edgeR_ = int(ROOT.gDirectory.Get("h{0}_{1}".format(branch, "temp")).GetBinLowEdge(nBins_+1))
            if i == 0:
                nBins = nBins_
                edgeL = edgeL_
                edgeR = edgeR_
                logging.debug("i = 0: nBins = %s", nBins)
                logging.debug("i = 0: edgeL = %s", edgeL)
                logging.debug("i = 0: edgeR = %s", edgeR)
                
            else:
                if edgeR_-edgeL_ < edgeR-edgeL:
                    nBins = nBins_
                    edgeL = edgeL_
                    edgeR = edgeR_
                    logging.debug("Replacing binnign:")
                    logging.debug("i = %s: nBins = %s", i, nBins)
                    logging.debug("i = %s: edgeL = %s", i, edgeL)
                    logging.debug("i = %s: edgeR = %s", i, edgeR)

        
        for i, tree in enumerate(trees):
            if i == 0:
                hBase = ROOT.TH1F("h_{0}_base".format(branch), "h_{0}_base".format(branch), nBins, edgeL, edgeR)
                for j in range(len(trees)):
                    thisBranchHistos.append(
                        hBase.Clone("h_{0}_{1}".format(branch, j))
                    )
                
            project(tree, "h_{0}_{1}".format(branch, i), branch, "1", "1", "Tree : %s"%i)
            
                

        # for h in thisBranchHistos:
        #     h.SetMarkerStyle(1)
        logging.debug(thisBranchHistos)
        thisRatio = RatioPlot(branch)
        thisRatio.passHistos(deepcopy(thisBranchHistos), normalize=True)
        thisRatio.legendSize = (0.5,0.7,0.9,0.9)
        thisRatio.useDefaultColors = True
        canvases.append(
            deepcopy(
                thisRatio.drawPlot(
                legendElements,
                xTitle = branch,
                floatingMax=  1.35,
                )
            )
        )
        
        
        saveCanvasListAsPDF(canvases, outputName+"_"+branch, folder)
    


if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description="Comapre trees with same brnaches")
    argumentparser.add_argument("--logging", help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages", type=int,default=20)
    argumentparser.add_argument("--folder", help="Ouptut folder", type=str,default=".")
    argumentparser.add_argument("--treeName", help="Tree name", type=str, required=True)
    argumentparser.add_argument("--outName", help="Outut file name", type=str, required=True)
    argumentparser.add_argument("--inputs", nargs="+", help="Input files", type=str,required=True)
    argumentparser.add_argument("--leg", nargs ="+", help="Categories w/o TRF", type=str,default=None)
    argumentparser.add_argument("--ignore", nargs ="+", help="igore", type=str,default=None)
    argumentparser.add_argument("--select", nargs ="+", help="igore", type=str,default=None)
    
    args = argumentparser.parse_args()
    
    initLogging(args.logging, "25")

    checkNcreateFolder(args.folder)

    if args.leg is not None:
        assert len(args.leg) == len(args.inputs)
        leg2Pass = args.leg
    else:
        leg2Pass = []
        for inFile in args.inputs:
            leg2Pass.append(os.path.basename(inFile))

    if args.ignore is None:
        ignoreBranches = []
    else:
        ignoreBranches = args.ignore


    if args.select is None:
        selectBranches = []
    else:
        selectBranches = args.select
            
    logging.debug("Got Legend: %s", leg2Pass)

    main(args.inputs, leg2Pass, args.treeName, args.folder, args.outName, ignoreBranches, selectBranches)
    
