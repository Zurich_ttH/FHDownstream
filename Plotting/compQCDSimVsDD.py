import logging
import ROOT
import sys
from glob import glob
from copy import deepcopy

ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetErrorX(0) #turns of horizontal error bars (but also for bkg)
ROOT.gROOT.SetBatch(1)


from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, moveOverUnderFlow, project, makeStackPlotCanvas, getColorStyleList
from classes.PLotDrawers import makeCanvasOfHistosBase
from classes.ratios import RatioPlot

cat_nice_names = {
    "fh_j7_t4" : "FH VR (7 jets, #geq4 b tags)",
    "fh_j8_t4" : "FH VR (8 jets, #geq4 b tags)",
    "fh_j9_t4" : "FH VR (#geq9 jets, #geq4 b tags)"
}


def compareQCDSimVsDD(inputFolder, outFolder, cats=["fh_j7_t4","fh_j8_t4","fh_j9_t4"], variables=["DNN_Node0"], niceNames=["ANN output score"], isVR=True):
    ddQCDSimFiles = [("singlet", "ST_MergedPDF.root"),
                     ("ttbarOther", "TTIncl_PDFMerged_ttbbMerged_ttbarOther.root"),
                     ("ttbarPlusCCbar", "TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar.root"),
                     ("ttbarPlusBBbarMerged", "TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root"),
                     ("diboson", "VV.root"),
                     ("wjets", "WJets.root"),
                     ("zjets", "ZJets.root")]
    ddQCDDataFile = ("data", "data.root")

    QCDFile = ("qcd", "QCD.root")



    if "/2016/" in inputFolder:
        lumi = "35.9"
    elif "/2017/" in inputFolder:
        lumi = "41.5"
    elif "/2018/" in inputFolder:
        lumi = "59.7"
    else:
        lumi = "137"

    assert len(variables) == len(niceNames)
        
    for ivar, var in enumerate(variables):
        for cat in cats:
            catSR = cat+"_VR" if isVR else cat+"_SR"
            catCR = cat+"_CR2"if isVR else cat+"_CR"
            
            logging.debug("SR : %s || CR : %s", catSR, catCR)
            
            rFileData = ROOT.TFile.Open(inputFolder+"/"+ddQCDDataFile[1])

            ddQCD =  rFileData.Get("{}__{}__{}".format(ddQCDDataFile[0], catCR, var)).Clone("ddQCD")

            logging.debug("Int ddQCD: %s",ddQCD.Integral())
            
            for proc, file_ in ddQCDSimFiles:
                rFileProcCR = ROOT.TFile.Open(inputFolder+"/"+file_)
                logging.debug("%s - %s", proc,rFileProcCR )
                thisHisto = rFileProcCR.Get("{}__{}__{}".format(proc, catCR, var))
                ddQCD.Add(thisHisto, -1)

                logging.debug("Substracting %s --> Int ddQCD: %s",proc, ddQCD.Integral())

            rFileQCD =  ROOT.TFile.Open(inputFolder+"/"+QCDFile[1])
            simQCD = rFileQCD.Get("{}__{}__{}".format(QCDFile[0], catSR, var)).Clone("simQCD")


            simQCD.Rebin(3)
            ddQCD.Rebin(3)

            simQCD.SetLineWidth(2)
            ddQCD.SetLineWidth(2)

            simQCD.SetLineColor(ROOT.kBlack)
            ddQCD.SetLineColor(ROOT.kGreen+2)

           

            # simQCD.GetXaxis().SetTickLength(0)
            # ddQCD.GetXaxis().SetTickLength(0)
            
            thisRatio =  RatioPlot("QCDSimVsDD_{}_{}".format(cat, var))
            thisRatio.passHistos([simQCD, ddQCD], normalize=True)
            thisRatio.thisLumi = lumi
            thisRatio.ratioText = "Ratio"
            thisRatio.ratioRange = (0,4)
            thisRatio.thisCMSLabel = "Private work (Simulation)"
            thisRatio.moveCMS = -0.02
            thisRatio.legendSize = (0.6,0.6,0.9,0.8)

            thisRatio.addLabel(
                cat_nice_names[cat],
                0.12, 0.95
            )
            
            canvas = thisRatio.drawPlot(
                ["QCD Simulation", "QCD data-driven"],
                niceNames[ivar],   drawCMS = False,
                floatingMax = 1.3,
            )

            saveCanvasListAsPDF([canvas], "QCDSimVsDD_{}_{}".format(cat, var), outFolder)
            

if __name__ == "__main__":
    initLogging(10)
    
    inputFolder = sys.argv[1]
    outputFolder = sys.argv[2]
    compareQCDSimVsDD(inputFolder, outputFolder)
    
    
    

    
