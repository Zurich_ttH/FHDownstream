import sys
import os

from glob import glob

def get_file_dict_and_set(folder):
    ret_dict = {}
    for f in glob(folder+"/*.root"):
        ret_dict[f.split("/")[-1]] = f 

    return ret_dict, set(ret_dict.keys())

if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument("--folders", action = "store", nargs="+", type = str, required = True)
    argumentparser.add_argument("--names", action = "store", nargs="+", type = str,required = True)
    argumentparser.add_argument("--output", action = "store",type = str,required = True)
    args = argumentparser.parse_args()

    assert len(args.folders) == len(args.names)

    file_dicts = []
    file_sets = []

    for folder in args.folders:
        this_dict, this_set = get_file_dict_and_set(folder)
        file_dicts.append(this_dict)
        file_sets.append(this_set)

    intersection = list(file_sets[0].intersection(*file_sets[1:]))

    for name in args.names:    
        os.system("mkdir {}/{}".format(args.output,name))
    
    for f in intersection:
        for i, name in enumerate(args.names):
            os.system("cp -v {} {}/{}".format(file_dicts[i][f], args.output,name))
        
