from __future__ import print_function, division

import os
import sys
import logging
from copy import deepcopy
from glob import glob
import time

import numpy as np
from array import array

import ROOT

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, checkNcreateFolder, project
from classes.utilities import update_progress

def generateMEMTree(inputFolder, treeName, outFolder, outFileName, year = 2016, drawProgress = False, addUnc=False):
    inTree = ROOT.TChain(treeName)
    nFilesAdd = 0
    for file_ in glob(inputFolder+"/*.root"):
        logging.debug("Adding file %s", file_)
        inTree.Add(file_)
        nFilesAdd += 1

    logging.info("Number of file added to chain: %s",nFilesAdd)

    outFile = outFolder + "/" + outFileName + ".root"
    logging.info("Will write output to %s", outFile)
    outputFile = ROOT.TFile(outFile, "RECREATE")
    outputFile.cd()
    outTree = ROOT.TTree(treeName,treeName);
    logging.info("Created output file %s and tree %s", outFile, treeName)        

    if addUnc:
        systSources = [
            "JER",
            "FlavorQCD",
            "RelativeBal",
            "HF",
            "BBEC1",
            "EC2",
            "Absolute",
            "AbsoluteYear",
            "HFYear",
            "EC2Year",
            "RelativeSampleYear",
            "BBEC1Year",
            "ScaleHEM1516",
            "Total"
        ]
    else:
        systSources = []
        
    sdirs = ["Up", "Down"]
    
    
    # Set branches in the output file
    evt_array = array("i",[0])
    evt_branch = outTree.Branch("event", evt_array, "event/I")

    cat_array = array("i",[4])
    cat_branch = outTree.Branch("cat", cat_array, "cat/I")

    mem_array = array("f", [-99.0])
    mem_branch = outTree.Branch("mem", mem_array, "mem/F")
    mem_ttbb_array = array("f", [-99.0])
    mem_ttbb_branch = outTree.Branch("mem_ttbb", mem_ttbb_array, "mem_ttbb/F")
    mem_tth_array = array("f", [-99.0])
    mem_tth_branch = outTree.Branch("mem_tth", mem_tth_array, "mem_tth/F")

    syst_mem_arrays = {}
    syst_mem_branches = {}
    syst_mem_ttbb_arrays = {}
    syst_mem_ttbb_branches = {}
    syst_mem_tth_arrays = {}
    syst_mem_tth_branches = {}

    uncNames = []
    for source in systSources:
        for sdir in sdirs:
            name = "%s%s"%(source, sdir)
            uncNames.append(name)
            syst_mem_arrays[name] = array("f", [-99.0])
            syst_mem_branches[name] = outTree.Branch("mem_"+name, syst_mem_arrays[name], "mem/F")
            syst_mem_ttbb_arrays[name] = array("f", [-99.0])
            syst_mem_ttbb_branches[name] = outTree.Branch("mem_ttbb_"+name, syst_mem_ttbb_arrays[name], "mem_ttbb_"+name+"/F")
            syst_mem_tth_arrays[name] = array("f", [-99.0])
            syst_mem_tth_branches[name] = outTree.Branch("mem_tth_"+name, syst_mem_tth_arrays[name], "mem_tth_"+name+"/F")

        
    
    nEvents = inTree.GetEntries()

    logging.info("Tree has %s entries \n", nEvents)


    
    
    if drawProgress:
        update_progress(0)
    
    for iev in range(nEvents):
        inTree.GetEvent(iev)

        logging.debug("Processingevent : %s (Event = %s)", iev, inTree.event)
        
        if drawProgress:
            update_progress(iev/nEvents)
        
        # Set the category
        is_7j = False
        is_8j = False
        is_9j = False
        is_WMassCenter = False
        if inTree.numJets == 7:
            is_7j = True
            if inTree.Wmass4b > 60 and inTree.Wmass4b <= 100:
                is_WMassCenter = True
        if inTree.numJets == 8:
            is_8j = True
            if inTree.Wmass4b > 60 and inTree.Wmass4b <= 100:
                is_WMassCenter = True
        if inTree.numJets >= 9:
            is_9j = True
            if inTree.Wmass4b > 70 and inTree.Wmass4b <= 92:
                is_WMassCenter = True

        is_4t = False
        if inTree.nBDeepFlavM >= 4:
            is_4t = True
            
        if (is_7j or is_8j or is_9j) and is_4t and is_WMassCenter:
            if year == 2016:
                trigSel = inTree.HLT_ttH_FH == 1 or inTree.HLT_BIT_HLT_PFJet450 == 1
            else:
                trigSel = inTree.HLT_ttH_FH == 1 or inTree.HLT_BIT_HLT_PFHT1050 == 1

            genSel = inTree.passMETFilters == 1 and inTree.is_fh == 1 and  inTree.ht30>500 and inTree.jets_pt[5]>40

            if not (trigSel and genSel):
                logging.debug("Event failed . Trigger selection or general Selection")
                continue
            
        else:
            logging.debug("Event failed : nJets selection or nBtagSelection or WMass Center selection")
            continue

        
        
        if is_9j:
            cat_array[0] = 2
            mem_ttbb_array[0] = inTree.mem_ttbb_FH_4w2h2t_p
            mem_tth_array[0] = inTree.mem_tth_FH_4w2h2t_p

            for name in uncNames:
                syst_mem_ttbb_arrays[name][0] = inTree.__getattr__("mem_ttbb_FH_4w2h2t_"+name+"_p")
                syst_mem_tth_arrays[name][0] = inTree.__getattr__("mem_tth_FH_4w2h2t_"+name+"_p")
            
        elif is_8j:
            cat_array[0] = 1
            mem_ttbb_array[0] = inTree.mem_ttbb_FH_3w2h2t_p
            mem_tth_array[0] = inTree.mem_tth_FH_3w2h2t_p

            for name in uncNames:
                syst_mem_ttbb_arrays[name][0] = inTree.__getattr__("mem_ttbb_FH_3w2h2t_"+name+"_p")
                syst_mem_tth_arrays[name][0] = inTree.__getattr__("mem_tth_FH_3w2h2t_"+name+"_p")

            
        elif is_7j:
            cat_array[0] = 0
            mem_ttbb_array[0] = inTree.mem_ttbb_FH_3w2h2t_p
            mem_tth_array[0] = inTree.mem_tth_FH_3w2h2t_p

            for name in uncNames:
                syst_mem_ttbb_arrays[name][0] = inTree.__getattr__("mem_ttbb_FH_3w2h2t_"+name+"_p")
                syst_mem_tth_arrays[name][0] = inTree.__getattr__("mem_tth_FH_3w2h2t_"+name+"_p")

            
        else:
            raise RuntimeError("At this point all event should fall in 7j, 8j or 9j category")
        
        logging.debug("nJets %s | nBtags %s", inTree.numJets, inTree.nBDeepFlavM)

        try:
            mem_array[0] = mem_tth_array[0] / ( mem_tth_array[0] + 0.065 * mem_ttbb_array[0] )
            for name in uncNames:
                syst_mem_arrays[name][0] = syst_mem_tth_arrays[name][0] / ( syst_mem_tth_arrays[name][0] + 0.065 * syst_mem_ttbb_arrays[name][0]  )
        except ZeroDivisionError:
            continue

        logging.debug("=== MEM ===")
        logging.debug("  ttbb = %s", mem_ttbb_array[0])
        logging.debug("  ttH = %s", mem_tth_array[0])
        logging.debug("  MEM = %s", mem_array[0])
        evt_array[0] = inTree.event

        outTree.Fill()

    if drawProgress:
        update_progress(2)

    outputFile.Write()

    
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for generating the Weight pickle for the tH and ttH CP samples'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        choices=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--inFolder",
        action = "store",
        help = "Input root file with the weight (in nanoAOD style)",
        type = str,
        required=True
    )
    argumentparser.add_argument(
        "--outFolder",
        action = "store",
        help = "Pickle with the event/lumi/run number of the file",
        type = str,
        required=True
    )
    argumentparser.add_argument(
        "--outName",
        action = "store",
        help = "Pickle with the event/lumi/run number of the file",
        type = str,
        required=True
    )
    argumentparser.add_argument(
        "--year",
        action = "store",
        help = "2016, 2017 or 2018",
        type = int,
        required=True,
        choices=[2016,2017,2018],
    )
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name if the input tree",
        type = str,
        default="tree",
    )
    argumentparser.add_argument(
        "--batch",
        action = "store_true",
        help = "If passed, the progress bar will not be shown (usefull when piping to file)", 
    )
    argumentparser.add_argument(
        "--addUnc",
        action = "store_true",
        help = "", 
    )


    
    args = argumentparser.parse_args()
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging, funcLen = 21)


    generateMEMTree(args.inFolder,
                    args.treeName,
                    args.outFolder,
                    args.outName,
                    args.year,
                    True if (args.logging > 10 and args.batch is False) else False,
                    args.addUnc
    )
