import logging
import ROOT

from classes.PlotHelpers import makeCanvasOfHistos, makeCanvas2DwCorrelation, moveOverUnderFlow, saveCanvasListAsPDF, initLogging, project

initLogging(20)

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

ROOT.gROOT.LoadMacro("classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("functions.h") #inlcude functions that can be used in TTree::Draw


#rFile = ROOT.TFile.Open("root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root")
rFile = ROOT.TFile.Open("root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8.root")
logging.info("Opened file %s", rFile)
tree = rFile.Get("tree")

eventSelection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && nBDeepCSVM>=2  && passMETFilters == 1"
eventWeight = "puWeight * (sign(genWeight)) *  bTagSF * bTagSF_norm  * get_TriggerSF3D(ht30, jets_pt[5], nBCSVM) * qgWeight"
jetSelectionq = "jets_btagDeepCSV >= 0 && jets_partonFlavour < 20"
jetSelectiong = "jets_btagDeepCSV >= 0 && jets_partonFlavour >= 20"

hBase = ROOT.TH1F("hBase","hBase",40,0,1)
hBase2D = ROOT.TH2F("hBase2D", "hBase2D", 20, 0.15 , 0.5, 40, 0 , 1)
hBase2DEvent = ROOT.TH2F("hBase2DEvent", "hBase2DEvent", 6, 0 , 6, 40, 0 , 1)

hAllq = hBase.Clone("hAllq")
hAllg = hBase.Clone("hAllg")
hAll = hBase.Clone("hAll")

hLooseq = hBase.Clone("hLooseq")
hLooseg = hBase.Clone("hLooseg")
hLoose = hBase.Clone("hLoose")

hLTMediumq = hBase.Clone("hLTMediumq")
hLTMediumg = hBase.Clone("hLTMediumg")
hLTMedium = hBase.Clone("hLTMedium")


hMediumq = hBase.Clone("hMediumq")
hMediumg = hBase.Clone("hMediumg")
hMedium = hBase.Clone("hMedium")

h2DQGLvLTags = hBase2D.Clone("h2DQGLvLTags")
h2DQGLvLTagsQ = hBase2D.Clone("h2DQGLvLTagsQ")
h2DQGLvLTagsG = hBase2D.Clone("h2DQGLvLTagsG")

h2DQGLRvsLoose = hBase2DEvent.Clone("h2DQGLRvsLoose")

hAllq.SetLineColor(ROOT.kBlue)
hLooseq.SetLineColor(ROOT.kBlue)
hLTMediumq.SetLineColor(ROOT.kBlue)
hMediumq.SetLineColor(ROOT.kBlue)

hAllg.SetLineColor(ROOT.kGreen-5)
hLooseg.SetLineColor(ROOT.kGreen-5)
hLTMediumg.SetLineColor(ROOT.kGreen-5)
hMediumg.SetLineColor(ROOT.kGreen-5)

hLoose.SetLineColor(ROOT.kRed)
hMedium.SetLineColor(ROOT.kRed)

logging.info("Projecting...")


# project(tree, "hAllq" ,"jets_qgl", "({0} && {1})".format(eventSelection, jetSelectionq), eventWeight)
# project(tree, "hAllg" ,"jets_qgl", "({0} && {1})".format(eventSelection, jetSelectiong), eventWeight)
project(tree, "hLooseq" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941)".format(eventSelection, jetSelectionq), eventWeight)
project(tree, "hLooseg" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941)".format(eventSelection, jetSelectiong), eventWeight)
project(tree, "hLTMediumq" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV < 0.4941 && jets_btagDeepCSV >= 0)".format(eventSelection, jetSelectionq), eventWeight)
project(tree, "hLTMediumg" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV < 0.4941 && jets_btagDeepCSV >= 0)".format(eventSelection, jetSelectiong), eventWeight)

# project(tree, "hMediumq" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.4941 && jets_btagDeepCSV < 1 && jets_DeepCSVrank >= 2)".format(eventSelection, jetSelectionq), eventWeight)
# project(tree, "hMediumg" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.4941 && jets_btagDeepCSV < 1 && jets_DeepCSVrank >= 2)".format(eventSelection, jetSelectiong), eventWeight)


# project(tree, "hAll" ,"jets_qgl", "({0} && jets_btagDeepCSV >= 0 )".format(eventSelection), eventWeight)
project(tree, "hLoose" ,"jets_qgl", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941)".format(eventSelection), eventWeight)
project(tree, "hLTMedium" ,"jets_qgl", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0 && jets_btagDeepCSV < 0.4941)".format(eventSelection), eventWeight)
# project(tree, "hMedium" ,"jets_qgl", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.4941 && jets_btagDeepCSV < 1 && jets_DeepCSVrank >= 2)".format(eventSelection), eventWeight)

# project(tree, "h2DQGLvLTags", "jets_qgl:jets_btagDeepCSV", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941)".format(eventSelection), eventWeight)

# project(tree, "h2DQGLvLTagsQ", "jets_qgl:jets_btagDeepCSV", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941 && jets_partonFlavour < 20)".format(eventSelection), eventWeight)

# project(tree, "h2DQGLvLTagsG", "jets_qgl:jets_btagDeepCSV", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941 && jets_partonFlavour >= 20)".format(eventSelection), eventWeight)

# project(tree, "h2DQGLRvsLoose", "qg_LR_4b_flavour_5q_0q:nBDeepCSVL", "({0})".format(eventSelection), eventWeight)

nJetsLoose_q = tree.Draw("jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941) * ({2})".format(eventSelection, jetSelectionq, eventWeight))
nJets_q = tree.Draw("jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0 && jets_btagDeepCSV < 0.4941) * ({2})".format(eventSelection, jetSelectionq, eventWeight))
nJetsLoose_g = tree.Draw("jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941) * ({2})".format(eventSelection, jetSelectiong, eventWeight))
nJets_g = tree.Draw("jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0 && jets_btagDeepCSV < 0.4941) * ({2})".format(eventSelection, jetSelectiong, eventWeight))

logging.info("========================================")
logging.info("Fraction of all jets < MWP that pass LWP")
logging.info("Quark Jets: %s/%s = %s", nJetsLoose_q, nJets_q, nJetsLoose_q/float(nJets_q))
logging.info("Gluon Jets: %s/%s = %s", nJetsLoose_g, nJets_g, nJetsLoose_g/float(nJets_g))
logging.info("========================================")

# tree.Project("hAllq" ,"jets_qgl", "({0} && {1}) * ({2})".format(eventSelection, jetSelectionq, eventWeight))
# tree.Project("hAllg" ,"jets_qgl", "({0} && {1}) * ({2})".format(eventSelection, jetSelectiong, eventWeight))
# tree.Project("hLooseq" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941) * ({2})".format(eventSelection, jetSelectionq, eventWeight))
# tree.Project("hLooseg" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941) * ({2})".format(eventSelection, jetSelectiong, eventWeight))
# tree.Project("hMediumq" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.4941 && jets_btagDeepCSV < 1 && jets_DeepCSVrank >= 2) * ({2})".format(eventSelection, jetSelectionq, eventWeight))
# tree.Project("hMediumg" ,"jets_qgl", "({0} && {1} && jets_btagDeepCSV >= 0.4941 && jets_btagDeepCSV < 1 && jets_DeepCSVrank >= 2) * ({2})".format(eventSelection, jetSelectiong, eventWeight))


# tree.Project("hAll" ,"jets_qgl", "({0} && jets_btagDeepCSV >= 0 ) * ({1})".format(eventSelection, eventWeight))
4# tree.Project("hLoose" ,"jets_qgl", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941) * ({1})".format(eventSelection, eventWeight))
# tree.Project("hMedium" ,"jets_qgl", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.4941 && jets_btagDeepCSV < 1 && jets_DeepCSVrank >= 2) * ({1})".format(eventSelection, eventWeight))

# tree.Project("h2DQGLvLTags", "jets_qgl:jets_btagDeepCSV", "({0} && jets_btagDeepCSV >= 0  && jets_btagDeepCSV >= 0.1522 && jets_btagDeepCSV < 0.4941)".format(eventSelection, eventWeight))



plots = []
# plots.append(makeCanvasOfHistos(
#     name = "qgAll",
#     Varname = "QGL value",
#     histoList = [hAllq, hAllg],
#     legendText = ["Quark Jets", "Gluon Jets"],
#     normalized = True)
# )

plots.append(makeCanvasOfHistos(
    name = "qgLoose",
    Varname = "QGL value",
    histoList = [hLooseq, hLooseg],
    legendText = ["Quark Jets", "Gluon Jets"],
    normalized = True)
)

plots.append(makeCanvasOfHistos(
    name = "qgLTMedium",
    Varname = "QGL value",
    histoList = [hLTMediumq, hLTMediumg],
    legendText = ["Quark Jets", "Gluon Jets"],
    normalized = True)
)



# plots.append(makeCanvasOfHistos(
#     name = "qgMedium",
#     Varname = "QGL value",
#     histoList = [hMediumq, hMediumg],
#     legendText = ["Quark Jets", "Gluon Jets"],
#     normalized = True)
# )

# hMediumg.SetLineColor(ROOT.kBlack)
# hMediumq.SetLineColor(ROOT.kBlack)
# hMedium.SetLineColor(ROOT.kBlack)


# plots.append(makeCanvasOfHistos(
#     name = "compq",
#     Varname = "QGL value",
#     histoList = [hLooseq, hMediumq],
#     legendText = ["Loose Quark Jets", "Add. Medium Quark Jets"],
#     normalized = True)
# )

# plots.append(makeCanvasOfHistos(
#     name = "compg",
#     Varname = "QGL value",
#     histoList = [hLooseg, hMediumg],
#     legendText = ["Loose Gluon Jets", "Add. Medium Gluon Jets"],
#     normalized = True)
# )


# plots.append(makeCanvasOfHistos(
#     name = "comp",
#     Varname = "QGL value",
#     histoList = [hLoose, hMedium],
#     legendText = ["Loose Jets", "Add. Medium Jets"],
#     normalized = True)
# )

#######################################################################
############################ 2D Plots #################################
#######################################################################
# plots.append(makeCanvas2DwCorrelation(
#     histo=h2DQGLvLTags,
#     width=600,
#     height=540)
# )
# plots.append(makeCanvas2DwCorrelation(
#     histo=h2DQGLvLTagsG,
#     width=600,
#     height=540)
# )
# plots.append(makeCanvas2DwCorrelation(
#     histo=h2DQGLvLTagsQ,
#     width=600,
#     height=540)
# )
# plots.append(makeCanvas2DwCorrelation(
#     histo=h2DQGLRvsLoose,
#     width=600,
#     height=540)
# )

saveCanvasListAsPDF(plots, "QGJetCheck",".")
