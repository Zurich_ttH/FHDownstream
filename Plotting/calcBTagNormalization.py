from __future__ import print_function

import ROOT
import sys, os
#import os.path
import logging
from copy import deepcopy
from array import array

from classes.PlotHelpers import initLogging, project, makeCanvas2D, saveCanvasListAsPDF, moveOverflow2D, moveOverUnderFlow
from classes.ratios import RatioPlot
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/lib/ConfigReader'))
from configreader import AutoConfigReader

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def makeBaseHisto(name, title, xbinning, ybinning):
    """
    Function for creating the 3D base histogram
    """
    logging.debug("Making 2Dbase histogram: %s", name)
    h2 = ROOT.TH2F(name, title,
                   len(xbinning)-1, array("d",xbinning),
                   len(ybinning)-1, array("d",ybinning),
     )
    h2.Sumw2()
    return h2

def makeBaseHisto1D(name, title, xbinning):
    """
    Function for creating the 3D base histogram
    """
    logging.debug("Making 1Dbase histogram: %s", name)
    h1 = ROOT.TH1F(name, title,
                   len(xbinning)-1, array("d",xbinning),
    )
    h1.Sumw2()
    return h1


def getConfig(config):
    config = AutoConfigReader(config, toplevelSection="General")
    if isinstance(config.weights ,list):
        config.weights = ",".join(config.weights)
    logging.info("Will process %s procs", len(config.procs))
    logging.info("Selection: %s",config.selection)
    logging.info("Base weight: %s", config.weights)
    logging.info("Add weight: %s", config.bTagWeights)
    logging.debug("Processes: %s", config.procs)

    return config

def loadTriggerMacro(config):
    # Load Trigger SF macro
    ROOT.gROOT.LoadMacro("classes.h") # You might have to chnage the path tp the file in classes.h of Error in <TFile::TFile>: file appears
    ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D = new TriggerSFMulitEra("{0}", "{1}")'.format(config.era, "DeepFlav"))
    ROOT.gROOT.LoadMacro("functions.h")

def loadbTagNorm(tag, folder, inclSFClosure):
    passSFFlag = "False"
    if inclSFClosure:
        passSFFlag = "True"
    ROOT.gInterpreter.ProcessLine('bTagNormalization* internalbTagNormalization = new bTagNormalization("{}", "{}", "{}")'.format(folder, tag, passSFFlag))
    ROOT.gROOT.LoadMacro("functions_btagNorm.h")
    
def getProcessInfo(config, proc, forceDefaultBinning=False):
    procInfo = getattr(config, proc)        
    
    var1, var2 = config.dim1, config.dim2
    binning1 = config.binning1
    if "binning1" in procInfo and not forceDefaultBinning:
        binning1 = procInfo["binning1"]
    binning2 = config.binning2
    if "binning2" in procInfo and not forceDefaultBinning:
        binning2 = procInfo["binning2"]
    
    binning1 = [float(x) for x in binning1]
    binning2 = [float(x) for x in binning2]
    
    logging.debug("Binning variable %s : %s", var1, binning1)
    logging.debug("Binning variable %s : %s", var2, binning2)
    
    sampleName = procInfo["file"].replace(".root","")
    postfix = "" if not "procPostfix" in procInfo else "__"+procInfo["procPostfix"]
        
    procSel = "1" if not "selection" in  procInfo else procInfo["selection"]
    procWei = "1" if not "weight" in  procInfo else procInfo["weight"]

    return [var1, var2], [binning1, binning2], sampleName, postfix, procSel, procWei

def generateCombinedttbarSF(config, tag, folder):
    logging.info("Start caclualting combined ttbar SF")

    config = getConfig(config)

    fHistos = "{}/Histos_{}.root".format(folder, tag)
    
    if not os.path.isfile(fHistos):
        raise RuntimeError("File for this tag/folder not present. Was looking for %s"%fHistos)

    logging.info("Creating backup")
    postFix = ""
    iTries = 1
    while os.path.isfile("{0}.bak{1}".format(fHistos, postFix)):
        postFix = "."+str(iTries)
        iTries += 1
    logging.debug("Backup file name: %s", "{0}.bak{1}".format(fHistos, postFix))
    os.system("cp {0} {0}.bak{1}".format(fHistos, postFix))

    rFile = ROOT.TFile(fHistos, "UPDATE")

    allKeys = [x.GetName() for x in rFile.GetListOfKeys()]
    ttInclusive = [x.replace("hBase_","") for x in allKeys if x.startswith("hBase_TTTo")]
    ttbb = [x.replace("hBase_","") for x in allKeys if x.startswith("hBase_TTbb")]
    ttH = [x.replace("hBase_","") for x in allKeys if x.startswith("hBase_ttH")]
    logging.debug("Found ttbar inclusive histos: %s", ttInclusive)
    logging.debug("Found ttbbb histos: %s", ttbb)
    logging.debug("Found ttbH histos: %s", ttH)
    canvases, canvases_base, canvases_weighted = [], [], []
    
    logging.info("Processing ttbb")
    hBase_ttbbComb = None
    hWeighted_ttbbComb = None
    for iproc, proc in enumerate(ttbb):
        if hBase_ttbbComb is None:
            logging.info("Initilizing with %s", "hBase_"+proc)
            hBase_ttbbComb = rFile.Get("hBase_"+proc).Clone("hBase_TTbb_Combined")
            hWeighted_ttbbComb = rFile.Get("hWeighted_"+proc).Clone("hWeighted_TTbb_Combined")
        else:
            logging.info("Adding %s", "hBase_"+proc)
            hBase_ttbbComb.Add(rFile.Get("hBase_"+proc))
            hWeighted_ttbbComb.Add(rFile.Get("hWeighted_"+proc))

    hSF_ttbbComb = hBase_ttbbComb.Clone("hSF_TTbb_Combined")
    hSF_ttbbComb.Divide(hWeighted_ttbbComb)

    hSF_ttbbComb.Write()
    hBase_ttbbComb.Write()
    hWeighted_ttbbComb.Write()

    plotSF(config, "TTbb", hBase_ttbbComb, hWeighted_ttbbComb, hSF_ttbbComb, canvases, canvases_base, canvases_weighted)

    logging.info("Processing ttH")
    hBase_ttHComb = None
    hWeighted_ttHComb = None
    for iproc, proc in enumerate(ttH):
        if hBase_ttHComb is None:
            logging.info("Initilizing with %s", "hBase_"+proc)
            hBase_ttHComb = rFile.Get("hBase_"+proc).Clone("hBase_ttH_Combined")
            hWeighted_ttHComb = rFile.Get("hWeighted_"+proc).Clone("hWeighted_ttH_Combined")
        else:
            logging.info("Adding %s", "hBase_"+proc)
            hBase_ttHComb.Add(rFile.Get("hBase_"+proc))
            hWeighted_ttHComb.Add(rFile.Get("hWeighted_"+proc))

    hSF_ttHComb = hBase_ttHComb.Clone("hSF_ttH_Combined")
    hSF_ttHComb.Divide(hWeighted_ttHComb)

    hSF_ttHComb.Write()
    hBase_ttHComb.Write()
    hWeighted_ttHComb.Write()

    plotSF(config, "ttH", hBase_ttHComb, hWeighted_ttHComb, hSF_ttHComb, canvases, canvases_base, canvases_weighted)
    
    
    hBases = {}
    hWeighted = {}
    hSF = {}
    for subproc in list(set([x.split("__")[1] for x in ttInclusive])):
        logging.info("Processing ttbar incl. subproc: %s"%subproc)
        hBases[subproc] = None
        hWeighted[subproc] = None
        for proc in list(set([x.split("__")[0] for x in ttInclusive])):
            logging.info("Processing proc %s", proc)
            if hBases[subproc] is None:
                logging.info("Initializing with %s", "hBase_"+proc+"__"+subproc)
                hBases[subproc] = rFile.Get("hBase_"+proc+"__"+subproc).Clone("hBase_TTIncl_Combined__"+subproc)
                hWeighted[subproc] = rFile.Get("hWeighted_"+proc+"__"+subproc).Clone("hWeighted_TTIncl_Combined__"+subproc)
            else:
                logging.info("Adding %s", "hBase_"+proc+"__"+subproc)
                hBases[subproc].Add(rFile.Get("hBase_"+proc+"__"+subproc))
                hWeighted[subproc].Add(rFile.Get("hWeighted_"+proc+"__"+subproc))
        hSF[subproc] = hBases[subproc].Clone("hSF_TTIncl_Combined__"+subproc)
        hSF[subproc].Divide(hWeighted[subproc])

        hBases[subproc].Write()
        hWeighted[subproc].Write()
        hSF[subproc].Write()

        plotSF(config, "TTIncl__"+subproc, hBases[subproc], hWeighted[subproc], hSF[subproc], canvases, canvases_base, canvases_weighted)
        
    rFile.Close()
    
    saveCanvasListAsPDF(canvases, "h2DSF_ttComb_"+tag, folder)
    saveCanvasListAsPDF(canvases_base, "h2DBase_ttComb_"+tag, folder)
    saveCanvasListAsPDF(canvases_weighted, "h2DWeighted_ttComb_"+tag, folder)


def calcBTagNormalization(config, tag, folder):
    logging.info("Start calculating normalization")

    config = getConfig(config)
    
    loadTriggerMacro(config)

    canvases = []
    canvases_base = []
    canvases_weighted = []

    histos = []

    rFiles = {}
    trees = {}
    logging.info("Creating FIles and Trees")
    for proc in config.procs:
        logging.debug("Opening file for %s", proc)
        procInfo = getattr(config, proc)        
        fullFileName = procInfo["folder"]+"/"+procInfo["file"]
        logging.debug("Filename: %s", fullFileName)
        rFiles[proc] = ROOT.TFile.Open(fullFileName)
        trees[proc] = rFiles[proc].Get(config.tree)
        
    logging.info("Processing sampels")
    for proc in config.procs:
        logging.info("Processing %s", proc)

        variables, binnings, sampleName, postfix, procSel, procWei = getProcessInfo(config, proc)
        var1, var2 = variables
        binning1, binning2 = binnings
        hNameBase = "hBase_"+sampleName+postfix
        hNameWeighted= "hWeighted_"+sampleName+postfix

        hBase = makeBaseHisto(hNameBase, hNameBase, binning1, binning2)
        hWeighted = makeBaseHisto(hNameWeighted, hNameWeighted, binning1, binning2)
    
        logging.debug("Got Tree: %s", trees[proc])

        project(trees[proc],
                hNameBase, "%s:%s"%(var2,var1),
                "%s && %s"%(config.selection, procSel),
                "%s * %s"%(config.weights, procWei),
                proc + " (Base)")
        project(trees[proc],
                hNameWeighted,
                "%s:%s"%(var2,var1),
                "%s && %s"%(config.selection, procSel),
                "%s * %s * %s"%(config.weights, procWei, config.bTagWeights),
                proc + " (Weighted)")


        moveOverflow2D(hBase)
        moveOverflow2D(hWeighted)

        hSF = hBase.Clone("hSF_"+sampleName+postfix)
        hSF.Divide(hWeighted)
        hSF.GetZaxis().SetRangeUser(0,1)        

        plotSF(config, proc, hBase, hWeighted, hSF, canvases, canvases_base, canvases_weighted)

        histos += [ deepcopy(hBase),
                   deepcopy(hWeighted),
                   deepcopy(hSF)]

    saveCanvasListAsPDF(canvases, "h2DSF_"+tag, folder)
    saveCanvasListAsPDF(canvases_base, "h2DBase_"+tag, folder)
    saveCanvasListAsPDF(canvases_weighted, "h2DWeighted_"+tag, folder)
    
    outFileName = folder+"/Histos_"+tag+".root"
    logging.info("Writing histograms to %s", outFileName)
    rOutFile = ROOT.TFile(outFileName, "RECREATE")
    rOutFile.cd()
    for h in histos:
        h.Write()

    rOutFile.Close()
    
    logging.info("Finished")

def plotSF(config, proc, hBase, hWeighted, hSF, canvases, canvases_base, canvases_weighted):
    hBase.SetTitle("")
    hBase.GetXaxis().SetTitle(config.name1)
    hBase.GetYaxis().SetTitle(config.name2)
    hWeighted.SetTitle("")
    hWeighted.GetXaxis().SetTitle(config.name1)
    hWeighted.GetYaxis().SetTitle(config.name2)
    hSF.SetTitle("")
    hSF.GetXaxis().SetTitle(config.name1)
    hSF.GetYaxis().SetTitle(config.name2)
    hSF.GetZaxis().SetRangeUser(0,1)

    canvases.append(
        makeCanvas2D(
            hSF,
            addOption="TEXTE",
            labelText="Sample: "+proc,
            labelpos = (0.27,0.95)
        )
    )

    canvases_base.append(
        makeCanvas2D(
            hBase,
            addOption="TEXTE",
            labelText="Sample: "+proc+" w/o b-tag weights",
            labelpos = (0.27,0.95)
        )
    )
    canvases_weighted.append(
        makeCanvas2D(
            hWeighted,
            addOption="TEXTE",
            labelText="Sample: "+proc+" w/ b-tag weights",
            labelpos = (0.27,0.95)
        )
    )


def makeHistosForVariable(config, trees, proc, procWeight, procSel, variable, binning, histoPostFix,
                          addSelection = "1", doClosure=False, closureWeight="1", applybTagSelection=False):
    hNameBase = "hBase_"+histoPostFix
    hNameWeighted= "hWeighted_"+histoPostFix
    hClosure = None
    
    hBase = makeBaseHisto1D(hNameBase, hNameBase, binning)
    hWeighted = makeBaseHisto1D(hNameWeighted, hNameWeighted, binning)

    logging.debug("Got Tree: %s [%s]", trees[proc], proc)

    project(trees[proc],
            hNameBase,
            variable,
            "%s && %s && %s && %s"%(config.selection, procSel, addSelection, config.bTagSelection if applybTagSelection else "1"),
            "%s * %s"%(config.weights, procWeight),
            proc + " " + variable + " (Base)")
    project(trees[proc],
            hNameWeighted,
            variable,
            "%s && %s && %s && %s"%(config.selection, procSel, addSelection, config.bTagSelection if applybTagSelection else "1"),
            "%s * %s * %s"%(config.weights, procWeight, config.bTagWeights),
            proc + " " + variable + " (Weighted)")

    if doClosure:
        hNameClosure = "hClosure_"+histoPostFix
        hClosure = makeBaseHisto1D(hNameClosure, hNameClosure, binning)
        project(trees[proc],
                hNameClosure,
                variable,
                "%s && %s && %s && %s"%(config.selection, procSel, addSelection, config.bTagSelection if applybTagSelection else "1"),
                "%s * %s * %s * %s"%(config.weights, procWeight, config.bTagWeights, closureWeight),
                proc + " " + variable + " (Closure)")

    return hBase, hWeighted, hClosure

    
def makePlotForVariable(config, trees, proc, procWeight, procSel, variable, binning, xAxisName, histoPostFix,
                        addSelection = "1", doClosure=False, closureWeight="1", applybTagSelection=False, procName=None):
    


    if isinstance(proc, str):
        hBase, hWeighted, hClosure = makeHistosForVariable(config, trees, proc, procWeight, procSel, variable, binning,
                                                               histoPostFix,
                                                               addSelection, doClosure, closureWeight, applybTagSelection)
        ratioName = proc + " " + variable
            
    elif isinstance(proc, list):
        if procName is None:
            raise RuntimeError("Please pass the procname variable if plotting multiple processes")

        ratioName = procName + " " + variable
            
        for ip,p in enumerate(proc):
            logging.debug("%s | %s | %s | %s | %s | %s ",ip, p, procWeight[ip], procSel[ip], binning[ip], closureWeight[ip], )
            if ip == 0:
                hBase, hWeighted, hClosure = makeHistosForVariable(config, trees, p, procWeight[ip], procSel[ip], variable, binning[ip],
                                                                   histoPostFix+"_"+procName,
                                                                   addSelection,
                                                                   doClosure,
                                                                   closureWeight[ip],
                                                                   applybTagSelection)
                
            else:
                hBase_, hWeighted_, hClosure_ = makeHistosForVariable(config, trees, p, procWeight[ip], procSel[ip], variable, binning[ip],
                                                                      histoPostFix+"_"+procName+"_"+str(ip),
                                                                      addSelection, doClosure, closureWeight[ip], applybTagSelection)

                hBase.Add(hBase_)
                hWeighted.Add(hWeighted_)
                if doClosure:
                    hClosure.Add(hClosure_)

    if doClosure:
        hClosure.SetLineColor(ROOT.kRed)
        hClosure.SetLineWidth(2)
        moveOverUnderFlow(hClosure)



    hBase.SetLineColor(ROOT.kBlack)
    hBase.SetLineWidth(2)
    hWeighted.SetLineColor(ROOT.kBlue)
    hWeighted.SetLineWidth(2)

    moveOverUnderFlow(hBase)
    moveOverUnderFlow(hWeighted)

    histos = [hBase, hWeighted]
    legendText = ["Before Reweighting", "After Reweighting"]
    if doClosure:
        histos.append(hClosure)
        legendText.append("Closure")


        
    thisRatio = RatioPlot(ratioName)
    if config.era == "2018":
        thisRatio.thisLumi = "59.7"
    if config.era == "2016":
        thisRatio.thisLumi = "35.9"
    thisRatio.passHistos(histos)
    if procName is None:
        thisRatio.addLabel("Process: %s"%proc, xStart= 0.30, yStart=0.87)
    else:
        thisRatio.addLabel("Process: %s"%procName, xStart= 0.30, yStart=0.87)
    if addSelection != "1" and applybTagSelection:
        thisRatio.addLabel("Add. Sel.: %s"%addSelection, xStart= 0.30, yStart=0.82)
        thisRatio.addLabel("Applied: %s"%config.bTagSelection, xStart= 0.30, yStart=0.77)
    elif applybTagSelection:
        thisRatio.addLabel("Applied: %s"%config.bTagSelection, xStart= 0.30, yStart=0.82)
    thisRatio.legendSize = (0.6,0.55,0.9,0.75)
    return thisRatio.drawPlot(legendText,
                              xAxisName,
                              drawCMS = True)

    

def plotVariables(config, tag, folder, doClosure=False, plot2per1=False, applybTagSelection=False, plotaddVars=False, forceDefaultBinning=False, inclSFClosure=False, plotMergedTTbar=True):
    logging.info("Start making plots")

    config = getConfig(config)

    loadTriggerMacro(config)

    if doClosure:
        loadbTagNorm(tag, folder, inclSFClosure)
        
    histos = []

    rFiles = {}
    trees = {}
    logging.info("Creating FIles and Trees")
    for proc in config.procs:
        logging.debug("Opening file for %s", proc)
        procInfo = getattr(config, proc)        
        fullFileName = procInfo["folder"]+"/"+procInfo["file"]
        logging.debug("Filename: %s", fullFileName)
        rFiles[proc] = ROOT.TFile.Open(fullFileName)
        trees[proc] = rFiles[proc].Get(config.tree)
        
    logging.info("Processing sampels")

    if applybTagSelection:
        logging.warning("Applying b-tagging selection: %s", config.bTagSelection)

    xNames = [config.name1,config.name2]

    _, binnings, _, _, _, _ = getProcessInfo(config, config.procs[0], forceDefaultBinning)

    
    addVariableInfo = {}
    nAddVars = 0
    if plotaddVars:
        if plot2per1:
            raise NotImplementedError
        addVariableInfo = getattr(config, "additionalVariables")
        for addVar in addVariableInfo:
            addVariableInfo[addVar]["name"] = addVariableInfo[addVar]["name"][0].replace("__"," ")
            addVariableInfo[addVar]["binning"] = [float(x) for x in addVariableInfo[addVar]["binning"]]
            addVariableInfo[addVar]["variable"] = addVariableInfo[addVar]["variable"][0]
            nAddVars += 1
            
    if plot2per1:
        canvases = [[] for x in range(len(binnings[0])-1)]
    else:
        canvases = [[] for x in range(2+nAddVars)]
                    
    for iproc, proc in enumerate(config.procs):
        logging.info("Processing %s", proc)

        if doClosure:            
            closureWeight = "get_bTagNormalization({}, numJets, ht30)".format(iproc+1)
            logging.warning("Closure weight: %s (for %s)", closureWeight, proc)
        else:
            closureWeight = "1"
        variables, binnings, sampleName, postfix, procSel, procWei = getProcessInfo(config, proc, forceDefaultBinning)            

        if plot2per1:
            for _ibin, _bin in enumerate(binnings[0]):
                if _ibin == len(binnings[0])-1:
                    continue
                elif _ibin == len(binnings[0])-2:
                    thisBinSel = "{0} >= {1}".format(variables[0], _bin)
                else:
                    thisBinSel = "{0} >= {1} && {0} < {2}".format(variables[0], _bin, binnings[0][_ibin+1])

                logging.debug("Using binSelection %s", thisBinSel)

                canvases[_ibin].append(
                    makePlotForVariable(config, trees, proc, procWei, procSel,
                                        variables[1], binnings[1], xNames[1],
                                        histoPostFix=sampleName+postfix+variables[1]+str(_ibin),
                                        addSelection=thisBinSel,
                                        doClosure = doClosure, closureWeight=closureWeight,
                                        applybTagSelection = applybTagSelection
                    )
                )

        else:
            ivar = 0
            for var in variables:
                logging.debug("Processing variable %s", var)

                canvases[ivar].append(
                    makePlotForVariable(config, trees, proc, procWei, procSel,
                                        var, binnings[ivar], xNames[ivar],
                                        histoPostFix = sampleName+postfix+var,
                                        doClosure = doClosure, closureWeight=closureWeight,
                                        applybTagSelection = applybTagSelection
                    )
                )
                ivar += 1
            for addVar in addVariableInfo.keys():
                logging.info("Processing additional variable %s", addVar)
                canvases[ivar].append(
                    makePlotForVariable(config, trees, proc, procWei, procSel,
                                        addVariableInfo[addVar]["variable"],
                                        addVariableInfo[addVar]["binning"],
                                        addVariableInfo[addVar]["name"],
                                        histoPostFix = sampleName+postfix+addVariableInfo[addVar]["variable"],
                                        doClosure = doClosure, closureWeight=closureWeight,
                                        applybTagSelection = applybTagSelection
                    )
                )
                ivar += 1

    
    if plotMergedTTbar:

        mergedprocs = {}
        mergedprocs["ttbb"] = [x for x in config.procs if (x.startswith("ttbb") and not "inc" in x)]
        mergedprocs["ttbb_inc"] = [x for x in config.procs if x.startswith("ttbb_inc")]
        mergedprocs["ttcc"] = [x for x in config.procs if x.startswith("ttcc")]
        mergedprocs["ttlf"] = [x for x in config.procs if x.startswith("ttlf")]
        mergedprocs["ttH"] = [x for x in config.procs if x.startswith("ttH")]
        
        for mergeproc in mergedprocs:
            allBinnings = []
            procSels = []
            procWeis = []
            sampleNames = []
            postfixes = []
            closureWeights = []
            for p in mergedprocs[mergeproc]:
                if doClosure:            
                    closureWeight = "get_bTagNormalization({}, numJets, ht30)".format(config.procs.index(p)+1)
                    logging.warning("Closure weight: %s (for %s)", closureWeight, p)
                else:
                    closureWeight = "1"
                    
                variables, binnings, sampleName, _, procSel, procWei = getProcessInfo(config, p, forceDefaultBinning)
                allBinnings.append(binnings)
                procSels.append(procSel)
                procWeis.append(procWei)
                sampleNames.append(sampleName)
                closureWeights.append(closureWeight)

            logging.info("Processing merged proc %s", mergeproc)
            ivar = 0
            for ivar, var in enumerate(variables):
                logging.debug("Processing variable %s", var)
                
                canvases[ivar].append(
                    makePlotForVariable(config, trees,
                                        mergedprocs[mergeproc],
                                        procWeis,
                                        procSels,
                                        var,
                                        [x[ivar] for x in allBinnings],
                                        xNames[ivar],
                                        histoPostFix = mergeproc+"_forMerge_"+var,
                                        doClosure = doClosure, closureWeight=closureWeights,
                                        applybTagSelection = applybTagSelection,
                                        procName = mergeproc,
                    )
                )
                ivar += 1
            for addVar in addVariableInfo.keys():
                logging.info("Processing additional variable %s", addVar)
                canvases[ivar].append(
                    makePlotForVariable(config, trees,
                                        mergedprocs[mergeproc],
                                        procWeis,
                                        procSels,
                                        addVariableInfo[addVar]["variable"],
                                        len(procWeis)*[addVariableInfo[addVar]["binning"]],
                                        addVariableInfo[addVar]["name"],
                                        histoPostFix = mergeproc+"_forMerge_"+addVariableInfo[addVar]["variable"],
                                        doClosure = doClosure, closureWeight=closureWeights,
                                        applybTagSelection = applybTagSelection,
                                        procName = mergeproc,
                    )
                )
                ivar += 1
                
    if plot2per1:
        for icanvas, canvasList in enumerate(canvases):
            print(canvasList)
            saveCanvasListAsPDF(canvasList, "hPerBinVar1_"+"_bin-"+str(icanvas)+"_"+config.dim2+"_"+tag+("closure" if doClosure else "")+("_btag" if applybTagSelection else ""), folder)
    else:
        saveCanvasListAsPDF(canvases[0], "hVariables_"+config.dim1+"_"+tag+("_closure" if doClosure else "")+("_btag" if applybTagSelection else ""), folder)
        saveCanvasListAsPDF(canvases[1], "hVariables_"+config.dim2+"_"+tag+("_closure" if doClosure else "")+("_btag" if applybTagSelection else ""), folder)
        for ivar, addVar in enumerate(addVariableInfo.keys()):
            saveCanvasListAsPDF(canvases[2+ivar], "hVariables_"+addVariableInfo[addVar]["variable"]+"_"+tag+("_closure" if doClosure else "")+("_btag" if applybTagSelection else ""), folder)
            
            
if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description='Description')
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--tag",
        action = "store", required = True,
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--folder",
        action = "store", required = False, default="out_BTagNorm",
        help = "Set the folder name used for the output",
    )
    argumentparser.add_argument(
        "--config",
        action = "store", required=True,
        help = "Config",
    )
    argumentparser.add_argument(
        "--plotVars",
        action = "store_true",
        help = "Plost the ratio between weighted and unweighted varibales for all input variables",
    )
    argumentparser.add_argument(
        "--plotClosure",
        action = "store_true",
        help = "Plost the closure test",
    )
    argumentparser.add_argument(
        "--inclSFClosure",
        action = "store_true",
        help = "If passed, the inclusive SF will be used. Make sure to run with --generateCombinedttbarSF before",
    )
    argumentparser.add_argument(
        "--plot2PerBin1",
        action = "store_true",
        help = "Plost variable 2 per bin in variable 1",
    )
    argumentparser.add_argument(
        "--applybTag",
        action = "store_true",
        help = "Plost variable 2 per bin in variable 1",
    )
    argumentparser.add_argument(
        "--calcNorm",
        action = "store_true",
        help = "Calc the normalization",
    )
    argumentparser.add_argument(
        "--plotaddVars",
        action = "store_true",
        help = "Script will try to plot variables listed in the additonalVariables section of the config",
    )
    argumentparser.add_argument(
        "--forceDefaultBinning",
        action = "store_true",
        help = "If passed, all samples with use the default binning ",
    )
    argumentparser.add_argument(
        "--generateCombinedttbarSF",
        action = "store_true",
        help = "If passed, the combined ttbar SF will be generate form the root file",
    )

    args = argumentparser.parse_args()

    initLogging(args.logging, funcLen = 25)

    if args.plotVars or args.plotClosure:
        
        plotVariables(args.config, args.tag, args.folder, args.plotClosure, args.plot2PerBin1, args.applybTag, args.plotaddVars, args.forceDefaultBinning, args.inclSFClosure)
    
    if args.calcNorm:
        calcBTagNormalization(args.config, args.tag, args.folder)

    
    if args.generateCombinedttbarSF:
        generateCombinedttbarSF(args.config, args.tag, args.folder)
        
