#!/usr/bin/env python
import os
import cPickle as pickle
import TTH.MEAnalysis.TFClasses as TFClasses
import sys
import ROOT

sys.modules["TFClasses"] = TFClasses

genE = 50 #target gen jet pT [GeV]
names = ["Legacy_v3", "Legacy_v4"]
transferFunctionsPickles = [
    "/work/koschwei/tth/LegacyRun2/v3master/CMSSW_10_2_15_patch2/src/TTH/MEAnalysis/data/transfer_functions_2018_deepFlavour_ttHbb_v3.pickle",
    "/work/koschwei/tth/LegacyRun2/v3master/CMSSW_10_2_15_patch2/src/TTH/MEAnalysis/data/transfer_functions_2018_deepFlavour_ttHbb_v4.pickle",
]

tf_formulas = {}
for name, transferFunctionsPickle in zip(names, transferFunctionsPickles):
    print "Loading TF_matrix for:",name

    pi_file = open(transferFunctionsPickle, 'rb')
    tf_matrix = pickle.load(pi_file)

    func = {}

    for fl in ["b", "l"]:
        func[fl] = {}
        if fl == "l" and "higgs" in transferFunctionsPickle:
            continue
        for bin in [0, 1]:
            func[fl][bin] = {}
            for i in range(1,5):
                #func[fl][bin][i] = tf_matrix[fl][bin].AcrossBinFuncs[i] 
                func[fl][bin][i] = ROOT.TF1(name + "_" + fl + "_" + str(bin) + "_" + str(i), tf_matrix[fl][bin].AcrossBinFuncs[i].str, 0, 300)
                for idx, p in enumerate(tf_matrix[fl][bin].AcrossBinFuncs[i].par_values):
                    func[fl][bin][i].SetParameter(idx, p) 
                #func[fl][bin][i] = tf_matrix[fl][bin].Print_content() 


    pi_file.close()
    tf_formulas[name] = func

print tf_formulas

for fl in ["b", "l"]:
    for bin in [0, 1]:
        for par in range(1,5):
            print "flavour:", fl, "bin:", bin, "par:", par
            #tf_formula[fl][bin].Draw("L")
            for name in tf_formulas.keys():
                if fl == "l" and "Higgs" in name:
                    continue
                #tf_formulas[name][fl][bin][par]
                #tf_formulas[name][fl][bin][par].SetRange(0,2*genE)
                print "\n***",name,"***"
                #for i in range(tf_formulas[name][fl][bin].GetNpar()):
                #    print tf_formulas[name][fl][bin].GetParameter(i)

                c1 = ROOT.TCanvas("c1","c1",700,600)
                c1.SetGrid()

                tf_formulas[names[0]][fl][bin][par].SetTitle("{} quarks, eta bin {}".format(fl,bin))
                tf_formulas[names[0]][fl][bin][par].GetHistogram().GetXaxis().SetTitle("reco p_{T} [GeV]")
                tf_formulas[names[0]][fl][bin][par].Draw("L")
                tf_formulas[names[1]][fl][bin][par].SetLineColor(ROOT.kBlue)
                tf_formulas[names[1]][fl][bin][par].Draw("LSAME")
                try:
                    tf_formulas[names[2]][fl][bin][par].SetLineColor(ROOT.kGreen)
                    tf_formulas[names[2]][fl][bin][par].Draw("LSAME")
                except:
                    pass

                leg = ROOT.TLegend(0.65,0.7,0.85,0.88)
                leg.SetBorderSize(0)
                leg.AddEntry(tf_formulas[names[0]][fl][bin][par], names[0], "l")
                leg.AddEntry(tf_formulas[names[1]][fl][bin][par], names[1], "l")
                try:
                    leg.AddEntry(tf_formulas[names[2]][fl][bin][par], names[2], "l")
                except:
                    pass
                leg.Draw()

                c1.SaveAs("tf_plots/ABF_TF_{0}GeV_{1}_eta{2}_par{3}.pdf".format( genE, fl, bin, par ))

