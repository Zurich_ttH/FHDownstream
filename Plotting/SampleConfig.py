import sys 
import logging
import os
sys.path.insert(0, os.path.abspath('../utils/'))
from ConfigReader import ConfigReaderBase


class SampleConfig(ConfigReaderBase):
    """
    Container for sample information

    Class is child of ConfigReaderBase.

    Args:
      pathtoConfig (str) : Path to config file
      lumi (float) : Luminosity the simulation samples are scaled to

    Attributes:
       samples (dict) : Dict of Sample objects
       outputSamples (dict) : Map linking Unique sampleNames to samples with this name. The idea here is,
                              that not all samples will appear as separate histogram in the plot but some of 
                              them will be merged. E.g. QCD consists of multiple subsamples (HT binned) but only
                              the sum on all of them should be plotted. So outputSamples would contain a pair with
                              key QCD and a list of QCD samples ["QCD300", "QCD500", ...]
    """
    def __init__(self, pathtoConfig, lumi):
        super(SampleConfig, self).__init__(pathtoConfig)

        self.samples = {}
        
        for section in self.readConfig.sections():
            if section == "General":
                continue
            self.samples[section] = Sample(
                name = section, #General identifier
                sampleName = self.readConfig.get(section, "sampleName"), # Used for defining which samples habe to be merged
                displayName = self.setOptionWithDefault(section, "displayName", self.readConfig.get(section, "sampleName")), #Used for legend
                path = self.readConfig.get(section, "path"), #Absolute path to skim 
                xsec = self.setOptionWithDefault(section, "xsec", 1.0, getterType = "float"), #xsec in picobarn
                nGen = self.setOptionWithDefault(section, "nGen", 1.0, getterType = "float"), #Number of weighted gen events
                color = self.readConfig.get(section, "color"), #Color - Currently can be set in ROOT style e.g. ROOT.kRed
                lumi = float(lumi),
                isData = self.readConfig.getboolean(section, "isData"),
                addScale = self.setOptionWithDefault(section, "addScale", 1.0, getterType = "float"),
                addWeight = self.setOptionWithDefault(section, "addWeight", "1"),
                addSelection = self.setOptionWithDefault(section, "addSelection", "1"),
                )

        # Define map for which samples have to be merged in the ouput plot
        self.outputSamples = {}
        for sample in self.samples:
            if self.samples[sample].sampleName in self.outputSamples.keys():
                self.outputSamples[self.samples[sample].sampleName].append(self.samples[sample].name)
            else:
                self.outputSamples[self.samples[sample].sampleName] = [self.samples[sample].name]

        orderFromConf = self.getList(self.readConfig.get("General", "order"))
        toAddSamples = []
        for key in self.outputSamples.keys():
            if key not in orderFromConf:
                if key == "data":
                    continue
                logging.warning("Sample %s not in order from config. Adding to the beginning", key)
                toAddSamples.append(key) 
        self.sampleOrder = toAddSamples+orderFromConf
        logging.info("Sample order: %s", self.sampleOrder)
        
class Sample(object):
    """
    Container for individual samples.

    Args:
      name (str) : Internal unique name of the samples
      sampleName (str): Internal name that is mainly used to identify multiple samples that should be merged later
      displayName (str) : Nice name that will be used in the legend
      path (str) : Path to root file
      xsec (float) : Cross section fo the sample on pb-1
      nGen (float) : Number of (weighted) generated events in the sample
      color (str) : Color of the sampel. Will be converted to root parsable indentifier by self.colorConverter
      lumi (float) : Luminosity used for scaling MC samples
      isData (bool) : Flag if the sample is data. if True the scaleing will set to 1
      addScale (float) : Additional per-sample SF. Not needed in normal operation!

    Attributies : 
      All the above
      SF (float) : Scale factor for the sample to scale to a given luminosity 
    """
    def __init__(self, name, sampleName, displayName, path, xsec, nGen, color, lumi, isData=False, addScale = 1.0, addWeight = "1", addSelection="1"):
        logging.debug("Initializing sample %s", name)
        self.name = name
        self.sampleName = sampleName
        self.displayName = displayName
        self.path = path
        self.xsec = xsec
        self.nGen = nGen
        self.color = self.colorConverter(color)
        self.lumi = lumi
        self.isData = isData
        self.addScale = addScale
        self.addWeight = addWeight
        self.addSelection = addSelection
        
        if not self.isData:
            self.SF = (1000*lumi*xsec*self.addScale)/nGen
            logging.debug("Sample MC - Setting SF to %s (incl add scale %s)", self.SF , self.addScale)
        else:
            self.SF = 1
            logging.debug("Sample isData - Setting SF to 1")

    @staticmethod
    def colorConverter(colorString):
        conversiondict = {  "kWhite" : 0,   "kBlack" : 1,   "kGray" : 920,  "kRed" : 632,  "kGreen" : 416,
                            "kBlue" : 600, "kYellow" : 400, "kMagenta" : 616,  "kCyan" : 432,  "kOrange" : 800,
                            "kSpring" : 820, "kTeal" : 840, "kAzure" :  860, "kViolet" : 880,  "kPink" : 900}
        if colorString.startswith("k"):
            if "+" in colorString:
                colorBase, colorAdd = colorString.split("+")
                colorAdd = int(colorAdd)
            elif "-" in colorString:
                colorBase, colorAdd = colorString.split("-")
                colorAdd = -1*int(colorAdd)
            else:
                colorBase, colorAdd = colorString, 0

            return conversiondict[colorBase]+colorAdd

        
