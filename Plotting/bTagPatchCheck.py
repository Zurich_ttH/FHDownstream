from __future__ import print_function, division

from copy import deepcopy
import logging
import itertools
from glob import glob
import math
import numpy as np

import ROOT

from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, moveOverUnderFlow, project, makeStackPlotCanvas
from classes.ratios import RatioPlot

ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetErrorX(0) #turns of horizontal error bars (but also for bkg)
ROOT.gROOT.SetBatch(1)


nioceNames = {
    "ht30" : "HT [GeV]",
    "DNN_Node0" : "ANN output",
    "jets_pt_all" : "p_{T} of all jets [GeV]",
    "jets_eta_all" : "#eta of all jets",
    "jets_btag_all" : "DeepFlav value of all jets",
    "jetsByPt_0_pt" : "Leading jet p_{T} [GeV]",
    "jetsByPt_0_btag" : "Leading jet DeepFlav value",
    "fh_j9_t4_SR" : "FH (#geq 9 jets, #geq 4 tags)",
    "fh_j8_t4_SR" : "FH (8 jets, #geq 4 tags)",
    "fh_j7_t4_SR" : "FH (7 jets, #geq 4 tags)"
    }


def mergeHistos(fileNames, histos):
    mergedHistos = {}
    for ifile, fileName in enumerate(fileNames):
        for key in istos:
            if ifile == 0:
                mergedHistos[key] = histos[fileName][key].Clone()
            else:
                mergedHistos[key].Add(histos[fileName][key].Clone())

    return mergedHistos
    
def mergeSystematics(histos, proc, keys, sysDir):
    logging.info("Merging systeamtics: %s", keys)
    outHisto = None
    binContents = []
    for ikey, key in enumerate(keys):
        if ikey == 0:
            outHisto = histos[proc][key].Clone()
            systName = key.split("__")[3]
            outHisto.SetName(outHisto.GetName().replace(systName,"CMS_btag"+sysDir))
            for i in range(outHisto.GetNbinsX()+1):
                binContents.append([outHisto.GetBinContent(i)])
                outHisto.SetBinContent(i, 0)
                outHisto.SetBinError(i, 0)
        else:
            for i in range(outHisto.GetNbinsX()+1):
                binContents[i].append(histos[proc][key].GetBinContent(i))

    # RMS of each bin
    finalBinContent = []
    for i, binVals in enumerate(binContents):
        logging.debug("Contents: %s", binVals)
        finalBinContent.append(
            math.sqrt((1/len(binVals))*sum([x*x for x in binVals]))
        )
        logging.debug("BinContent = %s", finalBinContent[i])
    # Fill content in outHisto
    for i, binVal in enumerate(finalBinContent):
        outHisto.SetBinContent(i, binVal)
    # return histo
    return outHisto


def mergeSystematicsAuto(histos, proc, keys, nomKey, SUM=True, RMS=False, SQUARDSUM=False):
    logging.debug("Args:")
    logging.debug("  histos    : %s", histos)
    logging.debug("  proc      : %s", proc)
    logging.debug("  keys      : %s", keys)
    logging.debug("  nomKey    : %s", nomKey)
    logging.debug("  RMS       : %s", RMS)
    logging.debug("  SUM       : %s", SUM)
    logging.debug("  SQUARDSUM : %s", SQUARDSUM)
    if RMS and SUM:
        raise RuntimeError("Only RMS or SUM should be set to true")
    if SQUARDSUM and SUM:
        raise RuntimeError("Only SQUARDSUM or SUM should be set to true")
    if SQUARDSUM and RMS:
        raise RuntimeError("Only SQUARDSUM or RMS should be set to true")
    
    if (not SQUARDSUM) and (not RMS) and (not SUM):
        raise RuntimeError("Set either RMS, SUM or SQUARDSUM to true")

    if RMS:
        logging.info("Will use RMS")        
    if SUM:
        logging.info("Will use SUM")
        
    
    cleanKeys = list(set([key.replace("Up","").replace("Down","") for key  in keys]))


    nomBinVals = []
    nomHisto = list(histos[proc][nomKey])
    logging.debug("NomHisto Bin values: %s", nomHisto)

    nomHistoNeg = [-x for x in nomHisto]

    upAllDiffs = None
    downAllDiffs = None
    for ikey, key in enumerate(cleanKeys):
        logging.debug("Processing systeamtic: %s", key)
        upHisto_ = list(histos[proc][key+"Up"])
        downHisto_ = list(histos[proc][key+"Down"])
        logging.debug("upHisto_ Bin values: %s", upHisto_)
        logging.debug("downHisto_ Bin values: %s", downHisto_)

        upRelDir = np.sign(sum(upHisto_)-sum(nomHisto))
        downRelDir = np.sign(sum(downHisto_)-sum(nomHisto))

        if upRelDir >= 0 and downRelDir < 0: 
            upHisto = upHisto_
            downHisto = downHisto_
        elif upRelDir < 0 and downRelDir >= 0: 
            upHisto = downHisto_
            downHisto = upHisto_
        elif upRelDir == 0 and downRelDir >= 0:
            upHisto = downHisto_
            downHisto = upHisto_
        elif upRelDir >= 0 and downRelDir >= 0:
            upHisto = upHisto_
            downHisto = downHisto_
        else:
            raise RuntimeError("Here is something weird- upRelDir %s and downRelDir %s",upRelDir, downRelDir)

        upDiffs = []
        downDiffs = []
        for i, nomVal in enumerate(nomHisto):
            upDiffs.append(upHisto[i] - nomVal)
            downDiffs.append(nomVal - downHisto[i])

        logging.debug("upDiffs Bin values: %s", upDiffs)
        logging.debug("downDiffs Bin values: %s", downDiffs)

        if ikey == 0:
            upAllDiffs = [[x] for x in upDiffs]
            downAllDiffs = [[x] for x in downDiffs]
        else:
            for i in range(len(upDiffs)):
                upAllDiffs[i].append(upDiffs[i])
                downAllDiffs[i].append(downDiffs[i])

    upAllDiffs_ = []
    downAllDiffs_ = []
    for i in range(len(upAllDiffs)):
        upAllDiffs_.append(
            [abs(x) for x in upAllDiffs[i]]
        )
        downAllDiffs_.append(
            [abs(x) for x in downAllDiffs[i]]
        )

    upAllDiffs = upAllDiffs_
    downAllDiffs = downAllDiffs_

    logging.debug("all UP diffs = %s", upAllDiffs )
    logging.debug("all DOWN diffs = %s", downAllDiffs )

    
    upFinalDiff = None
    downFinalDiff = None
    if SUM:
        upFinalDiff = [sum(x) for x in upAllDiffs]
        downFinalDiff = [sum(x) for x in downAllDiffs]
    elif RMS:
        upFinalDiff = []
        downFinalDiff = []
        for i in range(len(upAllDiffs)):
            upFinalDiff.append(
                math.sqrt((1/len(upAllDiffs[i]))*sum([x*x for x in upAllDiffs[i]]))
            )
            downFinalDiff.append(
                math.sqrt((1/len(upAllDiffs[i]))*sum([x*x for x in downAllDiffs[i]]))
            )
    elif SQUARDSUM:
        upFinalDiff = []
        downFinalDiff = []
        for i in range(len(upAllDiffs)):
            upFinalDiff.append(
                math.sqrt(sum([x*x for x in upAllDiffs[i]]))
            )
            downFinalDiff.append(
                math.sqrt(sum([x*x for x in downAllDiffs[i]]))
            )

    logging.debug("upFinalDiff : %s", upFinalDiff)
    logging.debug("downFinalDiff : %s", downFinalDiff)

    upHisto = histos[proc][nomKey].Clone(histos[proc][nomKey].GetName()+"__CMS_btagUp")
    downHisto = histos[proc][nomKey].Clone(histos[proc][nomKey].GetName()+"__CMS_btagDown")

    upHisto.SetLineWidth(2)
    downHisto.SetLineWidth(2)
    
    for i in range(len(upFinalDiff)):
        upHisto.SetBinContent(i, upHisto.GetBinContent(i)+upFinalDiff[i])
        #logging.info("%s | %s - %s = %s", i, downHisto.GetBinContent(i),downFinalDiff[i], downHisto.GetBinContent(i)-downFinalDiff[i])
        downHisto.SetBinContent(i, downHisto.GetBinContent(i)-downFinalDiff[i])

    return upHisto,downHisto
    
    
def bTagPatchCheck(inFolder, outputFolder, tag, variables = ["ht30"], cats = ["fh_j9_t4_SR","fh_j8_t4_SR","fh_j7_t4_SR"],
                   SUM=True, RMS=False, SQUARDSUM=False):

    if RMS and SQUARDSUM:
        raise RuntimeError("Only RMS *****OR***** SQUARDSUM are supported")
    
    relevantKeysSysts_single = [
        "btagNorm_off",
        "CMS_btag_PatchDown",
        "CMS_btag_PatchUp",
    ]
    relevantKeysSysts_toMerge = [
        "CMS_btag_cferr1Down",
        "CMS_btag_cferr1Up",
        "CMS_btag_cferr2Down",
        "CMS_btag_cferr2Up",
        "CMS_btag_hfDown",
        "CMS_btag_hfUp",
        "CMS_btag_hfstats1Down",
        "CMS_btag_hfstats1Up",
        "CMS_btag_hfstats2Down",
        "CMS_btag_hfstats2Up",
        "CMS_btag_lfDown",
        "CMS_btag_lfUp",
        "CMS_btag_lfstats1Down",
        "CMS_btag_lfstats1Up",
        "CMS_btag_lfstats2Down",
        "CMS_btag_lfstats2Up",
    ]

    datasetFiles = {
        # "ttlf" : "TTIncl_PDFMerged_ttbbMerged_ttbarOther.root",
        # "ttcc" : "TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar.root",
        # "ttbb" : "TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged.root",
        # "ttH" : "ttH_MergedPDF.root"

        "ttlf" : "TTIncl_PDFMerged_ttbbMerged_ttbarOther_rebinned.root",
        "ttcc" : "TTIncl_PDFMerged_ttbbMerged_ttbarPlusCCbar_rebinned.root",
        "ttbb" : "TTbb_PDFMerged_ttbbMerged_ttbarPlusBBbarMerged_rebinned.root",
        "ttH" : "ttH_MergedPDF_rebinned.root"
    }
    
    histos = {}
    for file_ in datasetFiles:
        histos[file_] = {}
        rFile = ROOT.TFile.Open(inFolder+"/"+datasetFiles[file_])
        allKeys = [key.GetName() for key in rFile.GetListOfKeys()]
        for key in allKeys:
            if len(key.split("__")) == 3:
                logging.debug("Adding key %s (nominal)", key)
                histos[file_][key] = deepcopy( rFile.Get(key) )

            else:
                syst = key.split("__")[3]
                if syst in relevantKeysSysts_single+relevantKeysSysts_toMerge:
                    logging.debug("Adding key %s for dataset %s", key, file_)
                    histos[file_][key] = deepcopy( rFile.Get(key) )
                    histos[file_][key].SetLineWidth(2)

                    
    for cat in cats:
        logging.info("Processing category %s", cat)
        for var in variables:
            logging.info("Processing variables %s", var)
            for dataset in datasetFiles:
                logging.info("Processing process %s", dataset)
                relevantKeys = [key for key in histos[dataset] if (cat in key and var in key)]
                logging.debug("Relevant keys: %s", relevantKeys)
                nomKey = [key for key in relevantKeys if len(key.split("__")) == 3]
                assert len(nomKey) == 1
                nomKey = nomKey[0]

                thisRelevantKeysSysts_single = []
                for syst in relevantKeysSysts_single:
                    thisSysKey = [key for key in relevantKeys if syst in key]
                    assert len(thisSysKey) == 1
                    thisRelevantKeysSysts_single.append(thisSysKey[0])

                thisRelevantKeysSysts_toMerge = []
                for syst in relevantKeysSysts_toMerge:
                    thisSysKey = [key for key in relevantKeys if syst in key]
                    assert len(thisSysKey) == 1
                    thisRelevantKeysSysts_toMerge.append(thisSysKey[0])

                
                # upSystKeys = [key for key in thisRelevantKeysSysts_toMerge if "Up" in key]
                # downSystKeys = [key for key in thisRelevantKeysSysts_toMerge if "Down" in key]
                
                # assert len(upSystKeys) == len(downSystKeys)

                # h_bTagUp = deepcopy(mergeSystematics(histos, dataset, upSystKeys, "Up"))
                # logging.debug("BTag UP: %s", h_bTagUp)
                # h_bTagDown = deepcopy(mergeSystematics(histos, dataset, downSystKeys, "Down"))
                # logging.debug("BTag Down: %s", h_bTagDown)

                h_btagUp, h_btagDown = mergeSystematicsAuto(histos, dataset, thisRelevantKeysSysts_toMerge, nomKey, SUM, RMS, SQUARDSUM)


                logging.debug("btag Up: %s", list(h_btagUp))
                logging.debug("btag Down: %s", list(h_btagDown))
                
                thisRatio = RatioPlot("ratio_"+cat+"_"+var+"_"+dataset)
                thisRatio.defaultColors = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed, ROOT.kRed, ROOT.kYellow+1,ROOT.kYellow+1]
                thisRatio.ratioText = "#frac{Variation}{Nominal}"
                thisRatio.thisCMSLabel = "Simulation"
                thisRatio.legendSize = (0.5,0.6,0.9,0.8)
                thisRatio.useDefaultColors = True
                thisRatio.passHistos(
                    [histos[dataset][nomKey]] +  # Nominal Histo
                    [histos[dataset][key] for key in thisRelevantKeysSysts_single] + # Nominal w/o b-tag patch, b-tag patch sys Up, b-tag patch sys down
                    [h_btagUp, # b-tag Up Sum
                     h_btagDown] # b-tag Down Sum
                )
                thisRatio.addLabel(nioceNames[cat] if cat in nioceNames.keys() else cat,
                                   0.12, 0.95)
                thisRatio.addLabel(dataset, 0.45, 0.95)
                thisCanvas = thisRatio.drawPlot(
                    ["Nominal", "Nominal w/o b-tag patch", "Stat unc. of b-tag patch", None, "b-tag uncertainties", None],
                    xTitle = nioceNames[var] if var in nioceNames.keys() else var,
                    noErr = True,
                    drawCMS = True, 
                    noErrRatio = True,
                    floatingMax = 1.15 if not ("DNN" in var or "eta" in var or "btag" in var) else 1.75,
                )

                if SUM:
                    postfix = "SUM"
                elif RMS:
                    postfix = "RMS"
                elif SQUARDSUM:
                    postfix = "SQUARDSUM"
                else:
                    raise RuntimeError("This should not happen.")


                saveCanvasListAsPDF([thisCanvas], "{}_{}_{}_{}_{}".format(tag, cat, var, dataset, postfix),outputFolder)    

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--inFolder",
        action = "store",
        help = "Path to input files",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Tag for the output files",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outFolder",
        action = "store",
        help = "output folder",
        type = str,
        default = "out_btagPatchCheck"
    )
    argumentparser.add_argument(
        "--vars",
        action = "store",
        help = "Variables",
        nargs = "+",
        type = str,
        default = ["ht30"]
    )
    argumentparser.add_argument(
        "--cats",
        action = "store",
        help = "Variables",
        nargs = "+",
        type = str,
        default = ["fh_j9_t4_SR","fh_j8_t4_SR","fh_j7_t4_SR"]
    )
    argumentparser.add_argument(
        "--RMS",
        action = "store_true",
        help = "IF passed the b-tag uncertainty source envelope will be the RMS instead of the sum",
    )    
    argumentparser.add_argument(
        "--SQUARDSUM",
        action = "store_true",
        help = "IF passed the b-tag uncertainty source envelope will be the SQUARDSUM instead of the sum",
    )    
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    bTagPatchCheck(args.inFolder, args.outFolder, args.tag, args.vars, args.cats,
                   SUM=(not (args.RMS or args.SQUARDSUM) ),
                   RMS=args.RMS, SQUARDSUM=args.SQUARDSUM)


    
