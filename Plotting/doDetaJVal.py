import ROOT

import ROOT, copy
import Helper
import logging
from classes.PlotHelpers import makeCanvasOfHistos, makeCanvas2DwCorrelation, moveOverUnderFlow, saveCanvasListAsPDF, initLogging, project


initLogging(20)

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)


inFile = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"
datasetName = "TTbar_fh"
jets = ["7j","8j","9j"] #run all 6 categories
bjets = ["3b","4b"]
allCuts = Helper.get_cuts(DeltaEta = False)
preSel = Helper.preselection
eventWeight = Helper.get_weights(triggerType = "tree")
qgWeights = Helper.get_QGNormaliziation()

rFile = ROOT.TFile.Open(inFile)
tree = rFile.Get("tree")


hBase7J = ROOT.TH2F("hBase7J", "hBase7J", 40,0.5, 3.5, 100, 50, 150)
hBase8J = ROOT.TH2F("hBase8J", "hBase8J", 40,0.5, 4, 100, 50, 150)
hBase9J = ROOT.TH2F("hBase9J", "hBase9J", 40,0.5, 4, 100, 50, 150)

thesePlots = []
for jc in jets:
    logging.info("Processing jet cat: %s", jc)
    jetCut = allCuts[jc+"noW"]
    if jc == "7j":
        thisBase = hBase7J
        thisVar = "Detaj[5]"
        thisBase.GetXaxis().SetTitle("#Delta#etaJ_{6}")
    elif jc == "8j":
        thisBase = hBase8J
        thisVar = "Detaj[6]"
        thisBase.GetXaxis().SetTitle("#Delta#etaJ_{7}")
    else:
        thisBase = hBase9J
        thisVar = "Detaj[7]"
        thisBase.GetXaxis().SetTitle("#Delta#etaJ_{8}")
    thisBase.GetYaxis().SetTitle("M_{qq}")
    thisBase.SetTitle("")
        
    thisHisto = thisBase.Clone("h2D"+jc)
    
    project(tree, "h2D"+jc, "Wmass:"+thisVar, "{0} && {1}".format(preSel, jetCut), eventWeight.replace("qgWeight","1"))
    
    thesePlots.append(
        makeCanvas2DwCorrelation(thisHisto, labelText = jc, labelpos = (0.8, 0.95))
    )
    

        
saveCanvasListAsPDF(thesePlots, "CorrDetaWMass",".")
