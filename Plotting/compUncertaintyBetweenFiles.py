import sys
import os
import logging
import ROOT

from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, moveOverUnderFlow, project, makeStackPlotCanvas
from classes.ratios import RatioPlot
from classes.PLotDrawers import getFitResultPlot, makeCanvasOfHistosWithLines
from classes.fitHelpers import getCHiSquare

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

niceNames = {
    "fh_j9_t4_SR" : "FH (#geq 9 jets, #geq 4 tags)",
    "fh_j8_t4_SR" : "FH (8 jets, #geq 4 tags)",
    "fh_j7_t4_SR" : "FH (7 jets, #geq 4 tags)",
    "fh_j9_t4_CR" : "FH CR (#geq 9 jets, #geq 4 tags)",
    "fh_j8_t4_CR" : "FH CR (8 jets, #geq 4 tags)",
    "fh_j7_t4_CR" : "FH CR (7 jets, #geq 4 tags)",
    "DNN_Node0" : "ANN output",
    "fh_STXS_0_j7_t4_SR" : "fh_STXS_0_j7_t4_SR" ,
    "fh_STXS_1_j7_t4_SR" : "fh_STXS_1_j7_t4_SR" ,
    "fh_STXS_2_j7_t4_SR" : "fh_STXS_2_j7_t4_SR" ,
    "fh_STXS_3_j7_t4_SR" : "fh_STXS_3_j7_t4_SR" ,
    "fh_STXS_4_j7_t4_SR" : "fh_STXS_4_j7_t4_SR" ,
    "fh_STXS_0_j8_t4_SR" : "fh_STXS_0_j8_t4_SR" ,
    "fh_STXS_1_j8_t4_SR" : "fh_STXS_1_j8_t4_SR" ,
    "fh_STXS_2_j8_t4_SR" : "fh_STXS_2_j8_t4_SR" ,
    "fh_STXS_3_j8_t4_SR" : "fh_STXS_3_j8_t4_SR" ,
    "fh_STXS_4_j8_t4_SR" : "fh_STXS_4_j8_t4_SR" ,
    "fh_STXS_0_j9_t4_SR" : "fh_STXS_0_j9_t4_SR" ,
    "fh_STXS_1_j9_t4_SR" : "fh_STXS_1_j9_t4_SR" ,
    "fh_STXS_2_j9_t4_SR" : "fh_STXS_2_j9_t4_SR" ,
    "fh_STXS_3_j9_t4_SR" : "fh_STXS_3_j9_t4_SR" ,
    "fh_STXS_4_j9_t4_SR" : "fh_STXS_4_j9_t4_SR" ,
    "fh_STXS_0_j7_t4_CR" : "fh_STXS_0_j7_t4_CR" ,
    "fh_STXS_1_j7_t4_CR" : "fh_STXS_1_j7_t4_CR" ,
    "fh_STXS_2_j7_t4_CR" : "fh_STXS_2_j7_t4_CR" ,
    "fh_STXS_3_j7_t4_CR" : "fh_STXS_3_j7_t4_CR" ,
    "fh_STXS_4_j7_t4_CR" : "fh_STXS_4_j7_t4_CR" ,
    "fh_STXS_0_j8_t4_CR" : "fh_STXS_0_j8_t4_CR" ,
    "fh_STXS_1_j8_t4_CR" : "fh_STXS_1_j8_t4_CR" ,
    "fh_STXS_2_j8_t4_CR" : "fh_STXS_2_j8_t4_CR" ,
    "fh_STXS_3_j8_t4_CR" : "fh_STXS_3_j8_t4_CR" ,
    "fh_STXS_4_j8_t4_CR" : "fh_STXS_4_j8_t4_CR" ,
    "fh_STXS_0_j9_t4_CR" : "fh_STXS_0_j9_t4_CR" ,
    "fh_STXS_1_j9_t4_CR" : "fh_STXS_1_j9_t4_CR" ,
    "fh_STXS_2_j9_t4_CR" : "fh_STXS_2_j9_t4_CR" ,
    "fh_STXS_3_j9_t4_CR" : "fh_STXS_3_j9_t4_CR" ,
    "fh_STXS_4_j9_t4_CR" : "fh_STXS_4_j9_t4_CR" ,
}


def compUncertaintyBetweenFiles(inputFiles, inputNames, systematic, year, procs, cats, var, outputFolder, tag,
                                splitDirections=False, signalProc="ttH_hbb", colors=[ROOT.kRed, ROOT.kGreen+1, ROOT.kBlue, ROOT.kYellow+1],
                                addUncertainty=None, addUncertaintyName=None, calcAgreementWithRate=False, narrowRatio=False,
                                useErrForAgreement=True, addSignal=True):
    logging.debug("Got Argumnents:")
    logging.debug("  inputFiles            : %s",inputFiles)
    logging.debug("  inputNames            : %s",inputNames)
    logging.debug("  systematic            : %s",systematic)
    logging.debug("  year                  : %s",year)
    logging.debug("  procs                 : %s",procs)
    logging.debug("  cats                  : %s",cats)
    logging.debug("  var                   : %s",var)
    logging.debug("  outputFolder          : %s",outputFolder)
    logging.debug("  signalProc            : %s",signalProc)
    logging.debug("  colors                : %s",colors)
    logging.debug("  addUncertainty        : %s",addUncertainty)
    logging.debug("  addUncertaintyName    : %s",addUncertaintyName)
    logging.debug("  calcAgreementWithRate : %s",calcAgreementWithRate)
    
    assert len(colors) >= len(inputFiles)

    if (addUncertainty is None and addUncertaintyName is not None) or (addUncertainty is not None and addUncertaintyName is None):
        logging.error("Either pass something for addUncertainty and addUncertaintyName or none. Will igonre it")
        addUncertainty=None
        addUncertaintyName=None
    
    fileNames = []
    rFiles = {}
    names = {}
    
    for i  in range(len(inputFiles)):
        thisFileName = inputFiles[i].split("/")[-1].replace(".root","")
        fileNames.append(thisFileName)
        
        rFiles[thisFileName] = ROOT.TFile.Open(inputFiles[i])
        names[thisFileName] = inputNames[i]

        logging.info("Opended file: %s", rFiles[thisFileName])

    for proc in procs:
        logging.info("Prcoessing process: %s", proc)
        canvases = []
        for cat in cats:
            logging.info("Processing cat: %s", cat)

            nomName = "{}__{}__{}".format(proc, cat, var)
            nomNameSignal = "{}__{}__{}".format(signalProc, cat, var)
            sysBaseName = "{}__{}__{}__{}".format(proc, cat, var, systematic)

            logging.debug("nom: %s", nomName)
            logging.debug("signal: %s", nomNameSignal)
            logging.debug("systematic base: %s", sysBaseName)

            nominalHisto = None
            signalHisto = None
            systematicPairs = []
            addHistoUp = None
            addHistoDown = None

            legendTexts = ["Nominal"]

            agreementWithRateLabelText = []

            if not "CR" in cat:
                legendTexts.append("Signal (scaled to nom)")
            if addUncertaintyName is not None:
                legendTexts.append(addUncertaintyName)
                legendTexts.append(None)

            for i, file_ in enumerate(fileNames):

                if i == 0:
                    nominalHisto = rFiles[file_].Get(nomName)
                    nominalHisto.SetLineColor(ROOT.kBlack)
                    nominalHisto.SetLineWidth(2)
                    if addSignal:
                        signalHisto = rFiles[file_].Get(nomNameSignal)
                        signalHisto.SetLineColor(ROOT.kCyan+1)
                        signalHisto.SetLineWidth(2)

                        signalHisto.Scale((1/signalHisto.Integral())*nominalHisto.Integral())

                    if addUncertainty is not None:
                        addHistoUp = rFiles[file_].Get(nomName+"__"+addUncertainty+"Up")
                        addHistoDown = rFiles[file_].Get(nomName+"__"+addUncertainty+"Down")

                        addHistoUp.SetLineWidth(2)
                        addHistoDown.SetLineWidth(2)


                        if not useErrForAgreement:
                            for ib in range(len(list(addHistoUp))):
                                addHistoUp.SetBinError(ib, 0)
                                addHistoDown.SetBinError(ib, 0)

                        addHistoUp.SetLineColor(ROOT.kGray+2)
                        addHistoDown.SetLineColor(ROOT.kGray+2)
                        if splitDirections:
                            addHistoDown.SetLineStyle(2)

                thisUpHisto = rFiles[file_].Get(sysBaseName+"Up")
                thisDownHisto = rFiles[file_].Get(sysBaseName+"Down")

                thisUpHisto.SetLineColor(colors[i])
                thisDownHisto.SetLineColor(colors[i])

                thisUpHisto.SetLineWidth(2)
                thisDownHisto.SetLineWidth(2)
                if splitDirections:
                    if i%2==0:
                        thisDownHisto.SetLineStyle(2)
                    else:
                        thisDownHisto.SetLineStyle(3)


                if calcAgreementWithRate and addUncertaintyName is not None and addUncertainty is not None:
                    # if not agreementWithRateLabelText:
                    #     agreementWithRateLabelText.append(
                    #         ""
                    #     )

                    pval_up, chi2val_up, ndof_up, modFlag_up = getCHiSquare(thisUpHisto, addHistoUp, "WW")
                    pval_down, chi2val_down, ndof_down, modFlag_down = getCHiSquare(thisDownHisto, addHistoDown, "WW")
                    KSprop_up = thisUpHisto.KolmogorovTest(addHistoUp)
                    KSprop_down = thisDownHisto.KolmogorovTest(addHistoDown)
                    KSMax_up = thisUpHisto.KolmogorovTest(addHistoUp, "M")
                    KSMax_down = thisDownHisto.KolmogorovTest(addHistoDown, "M")
                    logging.info("== %s == ", names[file_])
                    logging.info("===================================")
                    logging.info("==============  UP  ===============")
                    logging.info("Chi2: %s",chi2val_up)
                    logging.info("P-value: %s",pval_up)
                    logging.info("KS: %s",KSprop_up)
                    logging.info("KS Max distance: %s",KSMax_up)
                    logging.info("===================================")
                    logging.info("============== DOWN ===============")
                    logging.info("Chi2: %s",chi2val_down)
                    logging.info("P-value: %s",pval_down)
                    logging.info("KS: %s",KSprop_down)
                    logging.info("KS Max distance: %s",KSMax_down)
                    logging.info("===================================")
                    agreementWithRateLabelText += [
                        names[file_],
                        "#chi^2/dof = {0:0.3f} | p-val = {1:0.2f} | KS = {2:0.2f} | Up".format(chi2val_up, pval_up, KSprop_up),
                        "#chi^2/dof = {0:0.3f} | p-val = {1:0.2f} | KS = {2:0.2f} | Down".format(chi2val_down, pval_down, KSprop_down),
                    ]


                systematicPairs += [thisUpHisto, thisDownHisto]

                legendTexts += [names[file_], None]


            logging.debug("legendTexts: %s", legendTexts)


            allHistos = [nominalHisto]
            if not "CR" in cat and addSignal:
                allHistos.append(signalHisto)
            if addHistoUp is not None and addHistoDown is not None:
                allHistos.append(addHistoUp)
                allHistos.append(addHistoDown)
            allHistos += systematicPairs


            thisRatio = RatioPlot(cat)
            thisRatio.addLabel(niceNames[cat], 0.15, 0.96)
            thisRatio.legendSize = (0.15, 0.6, 0.45, 0.9)
            thisRatio.ratioRange = (0.6,1.4)
            thisRatio.ratioText = "Ratio to Nom."
            thisRatio.ratioLineWidth = 2
            if narrowRatio:
                thisRatio.ratioRange = (0.75,1.25)
            if year == "2016":
                thisRatio.thisLumi = "35.9"
            elif year == "2018":
                thisRatio.thisLumi = "59.7"
            else:
                pass

            thisRatio.passHistos(allHistos)

            if calcAgreementWithRate and addUncertaintyName is not None and addUncertainty is not None:
                fitRes = ROOT.TLegend(0.37,0.06,0.75,0.3)
                fitRes.SetFillStyle(0)
                fitRes.SetBorderSize(0)
                fitRes.SetTextSize(0.03)

                for line in agreementWithRateLabelText:
                    fitRes.AddEntry(None,line ,"")

                thisRatio.labels.append(fitRes)

            canvases.append(thisRatio.drawPlot(legendText = legendTexts,
                                               xTitle = niceNames[var],
                                               drawCMS = True,
                                               noErr = False,
                                               noErrRatio = True))


        saveCanvasListAsPDF(canvases, "UncComp{}_{}_{}_{}_{}".format("_"+tag if tag != "" else tag, systematic, proc, var, year), outputFolder)
             
            

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Comparison of systamtic impleemntaitons',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFiles",
        action = "store",
        help = "Name of the input sparse files",
        nargs="+",
        type=str,
        required = True
    )
    argumentparser.add_argument(
        "--inputNames",
        action = "store",
        help = "Name of the input sparse files",
        nargs="+",
        type=str,
        required = True
    )
    argumentparser.add_argument(
        "--systematic",
        action = "store",
        help = "Name of the systematic",
        required= True,
    )
    argumentparser.add_argument(
        "--proc",
        action = "store",
        nargs="+",
        help = "Process",
        required=True
    )
    argumentparser.add_argument(
        "--cats",
        action = "store",
        help = "Process",
        nargs="+",
        default=["fh_j9_t4_SR","fh_j8_t4_SR","fh_j7_t4_SR","fh_j9_t4_CR","fh_j8_t4_CR","fh_j7_t4_CR"]
    )
    argumentparser.add_argument(
        "--var",
        action = "store",
        help = "Years to be run",
        default = "DNN_Node0"
    )
    argumentparser.add_argument(
        "--year",
        action = "store",
        help = "Years to be run",
        default = "2018"
    )
    argumentparser.add_argument(
        "--outputFolder",
        action = "store",
        help = "Name of the output folder",
        type=str,
        default = "out_compUnc"
    )
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Tag",
        default = ""
    )
    argumentparser.add_argument(
        "--splitDirections",
        action = "store_true",
        help = "If passed, the the down variation will be dashed.",
    )
    argumentparser.add_argument(
        "--addUncertainty",
        action = "store",
        help = "Specifiy uncertainty to add from the first file",
        default = None,
    )
    argumentparser.add_argument(
        "--addUncertaintyName",
        action = "store",
        help = "Legend name for the systeamtic passed with --addUncertainty",
        default = None,
    )
    argumentparser.add_argument(
        "--calcAgreementWithRate",
        action = "store_true",
        help = "Pass arguement to calculate the chi^2 wrt to the rate. Hence only works if --addUncertainty and --addUncertaintyName is passed.",
    )
    argumentparser.add_argument(
        "--narrowRatio",
        action = "store_true",
        help = "Pass arguement to switch to a narrower ratio y axis",
    )
    argumentparser.add_argument(
        "--useErrForAgreement",
        action = "store_true",
        help = "Pass arguement to switch to a narrower ratio y axis",
    )
    argumentparser.add_argument(
        "--skipSignal",
        action = "store_true",
        help = "Pass arguement to switch to a narrower ratio y axis",
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging, 30)

    assert len(args.inputFiles) == len(args.inputNames)

    
    compUncertaintyBetweenFiles(inputFiles = args.inputFiles,
                                inputNames = args.inputNames,
                                systematic = args.systematic,
                                year = args.year,
                                procs = args.proc,
                                cats = args.cats,
                                var = args.var,
                                outputFolder = args.outputFolder,
                                tag = args.tag,
                                splitDirections = args.splitDirections,
                                addUncertainty = args.addUncertainty,
                                addUncertaintyName = args.addUncertaintyName,
                                calcAgreementWithRate = args.calcAgreementWithRate,
                                narrowRatio = args.narrowRatio,
                                useErrForAgreement = args.useErrForAgreement,
                                addSignal = (not args.skipSignal))
