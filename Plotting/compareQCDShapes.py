import ROOT
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging, checkNcreateFolder, moveOverUnderFlow
from classes.ratios import RatioPlot
import copy

ROOT.gStyle.SetOptStat(0)

def getInfo(year):
    files_2016 = {
        "QCD200" : "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3/QCD_HT200to300_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
        "QCD300" : "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3/QCD_HT300to500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
        "QCD500" : "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3/QCD_HT500to700_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
        "QCD700" : "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3/QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
        "QCD1000" : "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3/QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
        "QCD1500" : "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3/QCD_HT1500to2000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
        "QCD2000" : "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3/QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
    }

    files_2017 = {
        "QCD200" : "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3/QCD_HT200to300_TuneCP5_13TeV-madgraph-pythia8.root",
        "QCD300" : "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3/QCD_HT300to500_TuneCP5_13TeV-madgraph-pythia8.root",
        "QCD500" : "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3/QCD_HT500to700_TuneCP5_13TeV-madgraph-pythia8.root",
        "QCD700" : "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3/QCD_HT700to1000_TuneCP5_13TeV-madgraph-pythia8.root",
        "QCD1000" : "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3/QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8.root",
        "QCD1500" : "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3/QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8.root",
        "QCD2000" : "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3/QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8.root",
    }

    files_2018 = {
        "QCD200" : "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3/QCD_HT200to300_TuneCP5_13TeV-madgraphMLM-pythia8.root",
        "QCD300" : "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3/QCD_HT300to500_TuneCP5_13TeV-madgraphMLM-pythia8.root",
        "QCD500" : "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3/QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8.root",
        "QCD700" : "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3/QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8.root",
        "QCD1000" : "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8.root",
        "QCD1500" : "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8.root",
        "QCD2000" : "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8.root",
    }

    nGen_2016 = {
        "QCD200" : 18117488.0,
        "QCD300" : 16941994.0,
        "QCD500" : 18560666.0,
        "QCD700" : 15475317.0,
        "QCD1000" : 4843592.0,
        "QCD1500" : 3970835.5,
        "QCD2000" : 1991172.875,
    }


    nGen_2017 = {
        "QCD200" : 57363712.0,
        "QCD300" : 60038160.0,
        "QCD500" : 52551504.0,
        "QCD700" : 47428792.0,
        "QCD1000" : 16466719.0,
        "QCD1500" : 6490822.0,
        "QCD2000" : 5825510.0,
    }


    nGen_2018 = {
        "QCD200" : 54250576.0,
        "QCD300" : 54692544.0,
        "QCD500" : 55054320.0,
        "QCD700" : 48035312.0,
        "QCD1000" : 15407460.0,
        "QCD1500" : 10797606.0,
        "QCD2000" : 4038546.0,
    }

    xsec_2016 = {
        "QCD200" : 1759000,
        "QCD300" : 323400,
        "QCD500" : 30010,
        "QCD700" : 6361,
        "QCD1000" : 1094,
        "QCD1500" : 99.31,
        "QCD2000" : 20.20,
    }

    xsec_2017 = {
        "QCD200" : 1556000,
        "QCD300" : 323600,
        "QCD500" : 29990,
        "QCD700" : 6351,
        "QCD1000" : 1094,
        "QCD1500" : 99.01,
        "QCD2000" : 20.23,
    }

    if year == "2016":
        return files_2016, nGen_2016, xsec_2016
    elif year == "2017":
        return files_2017, nGen_2017, xsec_2017
    elif year == "2018":
        return files_2018, nGen_2018, xsec_2017
    else:
        raise KeyError

def main():
    ROOT.gROOT.LoadMacro("classes.h") # You might have to chnage the path tp the file in classes.h of Error in <TFile::TFile>: file appears
    ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D_2016 = new TriggerSFMulitEra("{0}", "{1}")'.format("2016", "DeepFlav"))
    ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D_2017 = new TriggerSFMulitEra("{0}", "{1}")'.format("2017", "DeepFlav"))
    ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D_2018 = new TriggerSFMulitEra("{0}", "{1}")'.format("2018", "DeepFlav"))
    ROOT.gROOT.LoadMacro("functions2.h")
    
    hBase = ROOT.TH1F("h", "h",7, 6, 13)
    var = "numJets"
    
    referenceYear = "2016"
    compareYear = "2018"

    ref_Files, ref_nGen, ref_xsec = getInfo(referenceYear)
    comp_Files, comp_nGen, comp_xsec = getInfo(compareYear)

    assert ref_Files.keys() == comp_Files.keys()

    ref_trigger = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFJet450 == 1)"
    comp_trigger = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)"
    selection = "numJets >=6 && ht30>500 && jets_pt[5]>40 && nBDeepFlavM>=2 && passMETFilters == 1 && is_fh == 1 && Wmass4b >= 30 && Wmass4b < 250"
    ref_weight = "(sign(genWeight)) * btagDeepFlavWeight_shape"
    comp_weight = "(sign(genWeight)) * btagDeepFlavWeight_shape"
    if compareYear == "2018":
        comp_weight = comp_weight + " * puWeight"      
    else:
        comp_weight = comp_weight + " * weightPURecalc"
    if referenceYear == "2018":
        ref_weight = ref_weight + " * puWeight"        
    else:
        ref_weight = ref_weight + " * weightPURecalc"


    if referenceYear == "2018":
        ref_weight = ref_weight + " *  get_TriggerSF3D_2018(ht30, jets_pt[5], nBDeepFlavM)"
    elif referenceYear == "2017":
        ref_weight = ref_weight + " * get_TriggerSF3D_2017(ht30, jets_pt[5], nBDeepFlavM)"
    else:
        ref_weight = ref_weight + " * get_TriggerSF3D_2016(ht30, jets_pt[5], nBDeepFlavM)"

    if compareYear == "2018":
        comp_weight = comp_weight + " *  get_TriggerSF3D_2018(ht30, jets_pt[5], nBDeepFlavM)"
    elif compareYear == "2017":
        comp_weight = comp_weight + " * get_TriggerSF3D_2017(ht30, jets_pt[5], nBDeepFlavM)"
    else:
        comp_weight = comp_weight + " * get_TriggerSF3D_2016(ht30, jets_pt[5], nBDeepFlavM)"

        
    if referenceYear == "2016":
        ref_trigger = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFJet450 == 1)"
    else:
        ref_trigger = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)"
    if compareYear == "2016":
        comp_trigger = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFJet450 == 1)"
    else:
        comp_trigger = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)"
        
    ref_histos = {}
    comp_histos = {}
    for sample in ref_Files:
        ref_rFile = ROOT.TFile.Open(ref_Files[sample])
        comp_rFile = ROOT.TFile.Open(comp_Files[sample])
        ref_tree =  ref_rFile.Get("tree")
        comp_tree =  comp_rFile.Get("tree")
        
        this_h_ref = hBase.Clone("h_"+"ref_"+sample)
        this_h_comp = hBase.Clone("h_"+"comp_"+sample)

        ref_tree.Project("h_"+"ref_"+sample, var, "("+ref_trigger+" && "+selection+") * ("+ref_weight+")")
        comp_tree.Project("h_"+"comp_"+sample, var, "("+comp_trigger+" && "+selection+") * ("+comp_weight+")")

        ref_histos[sample] = copy.deepcopy(this_h_ref)
        comp_histos[sample] = copy.deepcopy(this_h_comp)

    ref_merged_histo = None
    comp_merged_histo = None    
    for isample, sample in enumerate(ref_Files):
        if isample == 0:
            ref_merged_histo = ref_histos[sample].Clone("h_ref")
            comp_merged_histo = comp_histos[sample].Clone("h_comp")
        else:
            ref_merged_histo.Add(ref_histos[sample])
            comp_merged_histo.Add(comp_histos[sample])

    moveOverUnderFlow(ref_merged_histo)
    ref_merged_histo.Scale(1/ref_merged_histo.Integral())
    moveOverUnderFlow(comp_merged_histo)
    comp_merged_histo.Scale(1/comp_merged_histo.Integral())
    
    ratio = ref_merged_histo.Clone("h_ratio")
    ratio.Divide(comp_merged_histo)

    bins = ["== 6", "== 7", "== 8", "== 9", "== 10", "== 11", ">= 12"]

    print "======================================================="
    print "======================================================="
    print "========================= C++ ========================="
    for i in range(1,ratio.GetNbinsX()+1):
        if i == 1:
            print "if (nJets ",bins[i-1],"){ return ",ratio.GetBinContent(i),";} //+-",ratio.GetBinError(i)
        else:
            print "else if (nJets ",bins[i-1],"){ return ",ratio.GetBinContent(i),";} //+-",ratio.GetBinError(i)

    print "else { return 1.0; }"


    print "========================= py ========================="
    for i in range(1,ratio.GetNbinsX()+1):
        if i == 1:
            print "if nJets ",bins[i-1],":"
        else:
            print "elif  nJets ",bins[i-1],":"
        print "    return",ratio.GetBinContent(i),"# +-",ratio.GetBinError(i)

    print "else:"
    print "    return 1.0"
    print "======================================================="
    print "======================================================="
    
    ref_merged_histo.SetLineColor(ROOT.kRed)
    ref_merged_histo.SetLineWidth(2)
    comp_merged_histo.SetLineColor(ROOT.kBlue)
    comp_merged_histo.SetLineWidth(2)

    thisRatio = RatioPlot("hComp")
    thisRatio.passHistos([ref_merged_histo, comp_merged_histo], normalize=True)
    thisCanvas = thisRatio.drawPlot(
        legendText = [referenceYear, compareYear],
    )
    saveCanvasListAsPDF([thisCanvas], "CompareQCDShapes_"+referenceYear+"-vs-"+compareYear, ".")

    
if __name__ == "__main__":
    main()
