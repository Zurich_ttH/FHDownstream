from __future__ import print_function

import copy
import time
import logging
import os
import numpy as np
import itertools

import ROOT

from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging, checkNcreateFolder, moveOverUnderFlow, makeCanvas2D
from classes.ratios import RatioPlot
from classes.PLotDrawers import getFitResultPlot

from TTH.Plotting.Datacards.MiscClasses import TriggerSFCalculator

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

class Jet(object):
    def __init__(self, pt, mass, eta, phi, bTagValue, bTagIndex, hadronFlavour, matchFlag):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.mass = mass
        self.bTagValue = bTagValue
        self.hadronFlavour = hadronFlavour
        self.matchFlag = matchFlag
        self.bTagIndex = bTagIndex
        
        self.lv = ROOT.TLorentzVector()
        self.lv.SetPtEtaPhiM(self.pt, self.eta, self.phi, self.mass)

    def __repr__(self):
        return "pt = {0:.2f} | mass = {1:.2f} |eta = {2:.2f} | phi = {3:.2f} | bTagVal = {4:.2f} | hadFlav = {5} | matchFlag = {6}".format(self.pt,
                                                                                                                                           self.mass,
                                                                                                                                           self.eta,
                                                                                                                                           self.phi,
                                                                                                                                           self.bTagValue,
                                                                                                                                           self.hadronFlavour,
                                                                                                                                           self.matchFlag)
        
def getInfo(year):
    if year == "2016":
        return (
            "/t3home/koschwei/scratch/ttH/skims/2016/LegacyRun2_AH_v3p3/loose/ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root",
            9747590.0,
            0.2934045,
            35.9,
            0.3093,
            0.0614
        )
    elif year == "2017":
        return (
            "/t3home/koschwei/scratch/ttH/skims/2017/LegacyRun2_AH_v3p3/tight/ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root",
            7745976.0,
            0.2934045,
            41.5,
            0.3033,
            0.0521
        )
    elif year == "2018":
        return (
            "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRun2_AH_v3p3/tight/ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root",
            9261470.0,
            0.2934045,
            59.7,
            0.2770,
            0.0494
        )


def getHistos(processYears):
    t0 = time.time()


    lenvar = "njets"
    brPrefix = "jets"
    kinVariables = ["pt","eta","phi"]
    flagVariables = []
    bDiscName = "btagDeepFlav"        

    hBase = ROOT.TH1F("hBase", "hBase", 50, 50, 175)

    hIndexBase = ROOT.TH1F("hIndexBase", "hIndexBase", 7, 0, 7)

    h2DBase = ROOT.TH2F("h2DBase","h2DBase",50, 50, 175, 7, 0, 7)
    
    hSep = []
    hSepIndex = []
    hSep2D = []
    
    hTotal = hBase.Clone("hMass")
    hTotal.SetTitle("hMass")

    hIndexTotal = hIndexBase.Clone("hIndexTotal")
    hIndexTotal.SetTitle("hIndexTotal")
    
    total2DHisto = h2DBase.Clone("h2D")
    total2DHisto.SetTitle("h2D")
    
    for year in processYears:
        tYearStart = time.time()
        
        logging.info("Processing year: %s", year)
        inFile, nGen, xsec, lumi, mediumWP, looseWP = getInfo(year)
        logging.debug("Get Infos: %s / %s / %s / %s", inFile, nGen, xsec, lumi)
        rFile = ROOT.TFile.Open(inFile)
        tree = rFile.Get("tree")
        tdiff = time.time()
        nEvents = tree.GetEntries()
        thisMassHisto = hBase.Clone("hMass_%s"%year)
        thisMassHisto.SetTitle("hMass_%s"%year)
        logging.info("Created histo : %s", thisMassHisto)

        thisIndexHisto = hIndexBase.Clone("hIndex_%s"%year)
        thisIndexHisto.SetTitle("hIndex_%s"%year)
        logging.info("Created histo : %s", thisIndexHisto)

        this2DHisto = h2DBase.Clone("h2D_%s"%year)
        this2DHisto.SetTitle("h2D_%s"%year)
        
        triggerWeightFile = os.environ["CMSSW_BASE"]+"/src/TTH/MEAnalysis/data/"
        if year == "2016":
            triggerWeightFile += "TSF2016_FH_v2_DeepFlav_tightCut_updated.root"
        elif year == "2017":
            triggerWeightFile += "TSF2017_FH_v2_DeepFlav_tightCut_updated.root"
        else:
            triggerWeightFile += "TSF2018_FH_v2_DeepFlav_tightCut_updated.root"

        logging.info("Using Trigger SF file: %s", triggerWeightFile)
            
        triggerSF = TriggerSFCalculator(triggerWeightFile)
        
        logging.info("Will process %s events", nEvents)
        for iEv in range(nEvents):
            if iEv%250000== 0 and iEv != 0:
                logging.info("Event {0:10d} | Total time: {1:15.2f} | Diff time {2:15.2f}".format(iEv, time.time()-t0,time.time()-tdiff))
                tdiff = time.time()
                
            tree.GetEvent(iEv)
            nJets =tree.__getattr__(lenvar)

            if not (nJets >= 6 and tree.nBDeepFlavM >= 2 and tree.jets_pt[5]>40 and
                    tree.passMETFilters and tree.Wmass4b >= 30  and tree.Wmass4b and
                    tree.is_fh == 1 and tree.ht30 >= 500):
                continue
            
            jets = []
            for i in range(nJets):
                jets.append( Jet(
                    tree.__getattr__(brPrefix+"_pt")[i],
                    tree.__getattr__(brPrefix+"_mass")[i],
                    tree.__getattr__(brPrefix+"_eta")[i],
                    tree.__getattr__(brPrefix+"_phi")[i],
                    tree.__getattr__(brPrefix+"_btagDeepFlav")[i],
                    tree.__getattr__(brPrefix+"_DeepFlavindex")[i],
                    tree.__getattr__(brPrefix+"_hadronFlavour")[i],
                    tree.__getattr__(brPrefix+"_matchFlag")[i],
                ))

            higgsJets = []
            higgsIndices = []
            for ijet, jet in enumerate(jets):
                if jet.matchFlag == 2:
                    higgsJets.append(jet)
                    higgsIndices.append(jet.bTagIndex)
                
            logging.debug("Event %s - Found %s higgs Jets", iEv, len(higgsJets))
            for jet in higgsJets:
                logging.debug(jet)

            w_pu = tree.puWeight
            w_gen = tree.genWeight
            w_Btag = tree.btagDeepFlavWeight_shape
            w_trig = triggerSF.getSF(tree.ht30, tree.jets_pt[5], tree.nBDeepFlavM)

            logging.debug("Weights: PU = %s | GEN = %s | BTAG = %s | TRIG = %s", w_pu, w_gen, w_Btag, w_trig)

            w_event = w_pu * np.sign(w_gen) * w_Btag * w_trig

            logging.debug("Event weight = %s", w_event)

            if len(higgsJets) == 2:
                for idx in higgsIndices:
                    thisIndexHisto.Fill(idx, w_event)
            
            if len(higgsJets) == 2:
                higgs = higgsJets[0].lv + higgsJets[1].lv
                logging.debug("Higgs: pt = %.2f | mass = %.2f | eta = %.2f | phi = %.2f", higgs.Pt(), higgs.M(), higgs.Eta(), higgs.Phi())
                thisMassHisto.Fill(higgs.M(), w_event)
                for idx in higgsIndices:
                    this2DHisto.Fill(higgs.M(), idx, w_event)

                
        thisMassHisto.Scale((1000*lumi*xsec)/nGen)
        thisIndexHisto.Scale((1000*lumi*xsec)/nGen)
        this2DHisto.Scale((1000*lumi*xsec)/nGen)
        hTotal.Add(thisMassHisto)
        total2DHisto.Add(this2DHisto)
        hIndexTotal.Add(thisIndexHisto)
        hSep.append(copy.deepcopy(thisMassHisto))
        hSepIndex.append(copy.deepcopy(thisIndexHisto))            
        hSep2D.append(copy.deepcopy(this2DHisto))
        logging.info("Time to process year %s: %15.2f", year, time.time()-tYearStart)


        
        
    return hTotal, hSep, hIndexTotal, hSepIndex, total2DHisto, hSep2D
    
def main(processYears, outputFolder, outputName, rerunFile,
         fitStartVals_A, fitStartVals_B, fitStartVals_C,
         fitStartVals_A_years = None, fitStartVals_B_years = None, fitStartVals_C_years = None,
         evalFit = False, doFit = False):
    checkNcreateFolder(outputFolder)
    outPath = outputFolder+"/"+outputName+".root"
    
    if not rerunFile:
        hTotal, hSep, hIndexTotal, hSepIndex, total2DHisto, hSep2D = getHistos(processYears)
        
        logging.info("Writing output file: %s", outPath)
        outFile = ROOT.TFile(outPath, "RECREATE")
        outFile.cd()
        hTotal.Write()
        hIndexTotal.Write()
        total2DHisto.Write()
        for h in hSep+hSepIndex+hSep2D:
            h.Write()
        outFile.Close()

    else:
        if not os.path.isfile(outPath):
            raise RuntimeError("Could not find %s", outPath)
        inFile = ROOT.TFile.Open(outPath)
        hTotal = None
        hSep = []
        for key in inFile.GetListOfKeys():
            keyName = key.GetName()
            if keyName == "hMass":
                #print("getting ", keyName, "...", inFile.Get("hMass"))
                hTotal = copy.deepcopy(inFile.Get("hMass"))
            elif keyName.startswith("hMass"):
                #print("getting ", keyName)
                hSep.append(copy.deepcopy(inFile.Get(keyName)))

        inFile.Close()
        logging.info("Loaded %s", hTotal)
        logging.info("Loaded %s", hSep)



    #TODO: Add fitting part
    if doFit:
        logging.info("Doing the fit")
        fitResutls = {"A" : {}, "B" : {}, "C" : {}}
        
        totalLumi = 0.0
        yearColors = [ROOT.kRed, ROOT.kGreen+2,ROOT.kCyan]
        for year in processYears:
            logging.debug("Processing year %s", year)
            _,_,_,lumi,_,_ = getInfo(year)
            totalLumi += lumi

        canvases = []
        # A *exp(-0.5*((x- B )/ C )^2)
        fitRangeMin = 90
        fitRangeMax = 155

        fitFunction = ROOT.TF1("fit_gauss", "gaus",fitRangeMin,fitRangeMax)
        ###########################################
        # Setup parameters:
        fitFunction.SetParameter(0, fitStartVals_A)
        fitFunction.SetParName(0, "A")
        # A/[0] Limits?
        fitFunction.SetParameter(1, fitStartVals_B)
        fitFunction.SetParName(1, "B")
        # B/[1] Limits?
        fitFunction.SetParameter(2, fitStartVals_C)
        fitFunction.SetParName(2, "C")
        # C/[2] Limits?
        ###########################################
        print("-----------",fitFunction.GetNumberFreeParameters())
        hTotal.Fit("fit_gauss", "R")

        fitResutls["A"]["Total"] = fitFunction.GetParameter(0)
        fitResutls["B"]["Total"] = fitFunction.GetParameter(1)
        fitResutls["C"]["Total"] = fitFunction.GetParameter(2)

        
        saveCanvasListAsPDF([getFitResultPlot("Total", "m_{H} [GeV]",
                                              hTotal, "Total",
                                              fitFunction, "Fit",
                                              histoStyle = (ROOT.kBlue, 2),
                                              functionStyle = (ROOT.kRed, 2),
                                              listFitValues=True,
                                              drawCMS = True,
                                              CMSText="Simulation",
                                              Lumi="{:2.1f}".format(totalLumi))],
                            outputName,
                            outputFolder
        )

        for iyear, year in enumerate(processYears):
            thisFitFunction = ROOT.TF1("fit_gauss_"+year, "gaus",fitRangeMin,fitRangeMax)
            ###########################################
            # Setup parameters:
            thisFitFunction.SetParameter(0, fitStartVals_A)
            thisFitFunction.SetParName(0, "A")
            # A/[0] Limits?
            thisFitFunction.SetParameter(1, fitStartVals_B)
            thisFitFunction.SetParName(1, "B")
            # B/[1] Limits?
            thisFitFunction.SetParameter(2, fitStartVals_C)
            thisFitFunction.SetParName(2, "C")
            # C/[2] Limits?
            ###########################################
            hSep[iyear].Fit("fit_gauss_"+year, "R")

            fitResutls["A"][year] = thisFitFunction.GetParameter(0)
            fitResutls["B"][year] = thisFitFunction.GetParameter(1)
            fitResutls["C"][year] = thisFitFunction.GetParameter(2)

            
            _,_,_,lumi,_,_ = getInfo(year)
            saveCanvasListAsPDF([getFitResultPlot(year, "m_{H} [GeV]",
                                                  hSep[iyear], year,
                                                  thisFitFunction, "Fit",
                                                  histoStyle = (ROOT.kBlue, 2),
                                                  functionStyle = (ROOT.kRed, 2),
                                                  listFitValues=True,
                                                  drawCMS = True,
                                                  CMSText="Simulation",
                                                  replaceParNames = ["A", "#mu", "#sigma"],
                                                  Lumi="{:2.1f}".format(lumi))],
                                outputName+"_"+year,
                                outputFolder
            )



        saveCanvasListAsPDF([makeCanvasOfHistos("YearComp",
                                                "m_{H} [GeV]",
                                                [hTotal]+hSep,
                                                legendText=["Total"]+processYears,
                                                normalized=True,
                                                colorList=[ROOT.kBlue]+yearColors[0:len(processYears)],
                                                addCMS=True,
                                                simulation=True,
                                                lumi="{:2.1f}".format(totalLumi))],
                             outputName+"_yearComp",
                             outputFolder)

    if evalFit and doFit:
        logging.info("Evaluating the fit")
        evaluateFit(processYears, fitResutls["B"], fitResutls["C"], outputName, outputFolder)



def evaluateFit(processYears, Bs, Cs, outfilePrefix, folder):
    t0 = time.time()
    lenvar = "njets"
    brPrefix = "jets"
    kinVariables = ["pt","eta","phi"]
    flagVariables = []
    bDiscName = "btagDeepFlav"    

    hBase = ROOT.TH1F("hBase", "hBase", 40, 80, 160)
    hIndexBase = ROOT.TH1F("hIndexBase", "hIndexBase", 7, 0, 7)
    h2DBase = ROOT.TH2F("h2DBase","h2DBase", 40, 80, 160, 40, 80, 160)
    h2DBase.GetXaxis().SetTitle("m_{bb,H}")
    h2DBase.GetYaxis().SetTitle("m_{H}")
    h2DBase.SetTitle("")
    
    for year in processYears:
        tYearStart = time.time()
        
        logging.info("Processing year: %s", year)
        inFile, nGen, xsec, lumi, mediumWP, looseWP = getInfo(year)

        rFile = ROOT.TFile.Open(inFile)
        tree = rFile.Get("tree")
        tdiff = time.time()
        nEvents = tree.GetEntries()

        triggerWeightFile = os.environ["CMSSW_BASE"]+"/src/TTH/MEAnalysis/data/"
        if year == "2016":
            triggerWeightFile += "TSF2016_FH_v2_DeepFlav_tightCut_updated.root"
        elif year == "2017":
            triggerWeightFile += "TSF2017_FH_v2_DeepFlav_tightCut_updated.root"
        else:
            triggerWeightFile += "TSF2018_FH_v2_DeepFlav_tightCut_updated.root"


        logging.info("Using Trigger SF file: %s", triggerWeightFile)
            
        triggerSF = TriggerSFCalculator(triggerWeightFile)

        thisHiggsHisto = hBase.Clone("hHiggsMass_%s"%year)
        
        thisMassHisto = hBase.Clone("hMass_%s"%year)
        thisMassHisto_SR = hBase.Clone("hMass_SR_%s"%year)
        thisMassHisto_CR = hBase.Clone("hMass_CR_%s"%year)
        this4MassHisto = hBase.Clone("h4Mass_%s"%year)
        this4MassHisto_SR = hBase.Clone("h4Mass_SR_%s"%year)
        this4MassHisto_CR = hBase.Clone("h4Mass_CR_%s"%year)

        thisMassHisto_hMatched = hBase.Clone("hMass_hMatched_%s"%year)
        thisMassHisto_hMatched_SR = hBase.Clone("hMass_hMatched_SR_%s"%year)
        thisMassHisto_hMatched_CR = hBase.Clone("hMass_hMatched_CR_%s"%year)
        this4MassHisto_hMatched = hBase.Clone("h4Mass_hMatched_%s"%year)
        this4MassHisto_hMatched_SR = hBase.Clone("h4Mass_hMatched_SR_%s"%year)
        this4MassHisto_hMatched_CR = hBase.Clone("h4Mass_hMatched_CR_%s"%year)

        thisMassHisto_hMatchedNot = hBase.Clone("hMass_hMatchedNot_%s"%year)
        thisMassHisto_hMatchedNot_SR = hBase.Clone("hMass_hMatchedNot_SR_%s"%year)
        thisMassHisto_hMatchedNot_CR = hBase.Clone("hMass_hMatchedNot_CR_%s"%year)
        this4MassHisto_hMatchedNot = hBase.Clone("h4Mass_hMatchedNot_%s"%year)
        this4MassHisto_hMatchedNot_SR = hBase.Clone("h4Mass_hMatchedNot_SR_%s"%year)
        this4MassHisto_hMatchedNot_CR = hBase.Clone("h4Mass_hMatchedNot_CR_%s"%year)
        
        
        thisIndexHisto = hIndexBase.Clone("hIndex_%s"%year)
        thisIndexHisto_SR = hIndexBase.Clone("hIndex_SR_%s"%year)
        thisIndexHisto_CR = hIndexBase.Clone("hIndex_CR_%s"%year)
        this4IndexHisto = hIndexBase.Clone("h4Index_%s"%year)
        this4IndexHisto_SR = hIndexBase.Clone("h4Index_SR_%s"%year)
        this4IndexHisto_CR = hIndexBase.Clone("h4Index_CR_%s"%year)

        # thisMassHisto_All = hBase.Clone("hMass_All_%s"%year)
        # thisMassHisto_All_SR = hBase.Clone("hMass_All_SR_%s"%year)
        # thisMassHisto_All_CR = hBase.Clone("hMass_All_CR_%s"%year)
        
        thisHiggsMass2D = h2DBase.Clone("hHiggsMass2D_%s"%year)
        thisHiggsMass2D_SR = h2DBase.Clone("hHiggsMass2D_SR_%s"%year)
        thisHiggsMass2D_CR = h2DBase.Clone("hHiggsMass2D_CR_%s"%year)

        this4HiggsMass2D = h2DBase.Clone("h4HiggsMass2D_%s"%year)
        this4HiggsMass2D_SR = h2DBase.Clone("h4HiggsMass2D_SR_%s"%year)
        this4HiggsMass2D_CR = h2DBase.Clone("h4HiggsMass2D_CR_%s"%year)
        
        # thisHiggsMass2D_All = h2DBase.Clone("hHiggsMass2D_All_%s"%year)
        # thisHiggsMass2D_All_SR = h2DBase.Clone("hHiggsMass2D_All_SR_%s"%year)
        # thisHiggsMass2D_All_CR = h2DBase.Clone("hHiggsMass2D_All_CR_%s"%year)


        
        logging.info("Will process %s events", nEvents)
        for iEv in range(nEvents):
            if iEv%200000 == 0 and iEv != 0:
                logging.info("Event {0:10d} | Total time: {1:15.2f} | Diff time {2:15.2f}".format(iEv, time.time()-t0,time.time()-tdiff))
                tdiff = time.time()
                break
                
            tree.GetEvent(iEv)
            nJets =tree.__getattr__(lenvar)


            if not (nJets >= 6 and tree.jets_pt[5]>40 and
                    (  tree.nBDeepFlavM >= 4 or
                      (tree.nBDeepFlavM == 3 and tree.nBDeepFlavL >= 4) ) and 
                    tree.passMETFilters and tree.Wmass4b >= 30  and tree.Wmass4b and
                    tree.is_fh == 1 and tree.ht30 >= 500):
                continue

            if tree.nBDeepFlavM >= 4:
                isSR = True
            else:
                isSR = False
                
            jets = []
            for i in range(nJets):
                jets.append( Jet(
                    tree.__getattr__(brPrefix+"_pt")[i],
                    tree.__getattr__(brPrefix+"_mass")[i],
                    tree.__getattr__(brPrefix+"_eta")[i],
                    tree.__getattr__(brPrefix+"_phi")[i],
                    tree.__getattr__(brPrefix+"_btagDeepFlav")[i],
                    tree.__getattr__(brPrefix+"_DeepFlavindex")[i],
                    tree.__getattr__(brPrefix+"_hadronFlavour")[i],
                    tree.__getattr__(brPrefix+"_matchFlag")[i],
                ))


            w_pu = tree.puWeight
            w_gen = tree.genWeight
            w_Btag = tree.btagDeepFlavWeight_shape
            w_trig = triggerSF.getSF(tree.ht30, tree.jets_pt[5], tree.nBDeepFlavM)

            logging.debug("Weights: PU = %s | GEN = %s | BTAG = %s | TRIG = %s", w_pu, w_gen, w_Btag, w_trig)

            w_event = w_pu * np.sign(w_gen) * w_Btag * w_trig

            logging.debug("Event weight = %s", w_event)

            bJets = []
            for jet in jets:
                if jet.bTagValue >= mediumWP:
                    bJets.append(jet)
            
            bJets = sorted(bJets, key=lambda jet: jet.bTagValue, reverse=True)
            
            if len(bJets) < 4:
                addbJets = []
                for jet in jets:
                    if jet.bTagValue < mediumWP and jet.bTagValue >= looseWP:
                        addbJets.append(jet)

                addbJets = sorted(addbJets, key=lambda jet: jet.bTagValue, reverse=True)
                bJets += addbJets
            
            combs = itertools.combinations(range(len(bJets)),2)
            bestMass = 0.0
            minimumComb = None
            minimumScore = 999999999.9
            for comb in combs:
                score = pow((bJets[comb[0]].lv+bJets[comb[1]].lv).M() - Bs[year], 2)/pow(Cs[year],2)
                if score < minimumScore:
                    minimumScore = score
                    minimumComb = comb
                    bestMass = (bJets[comb[0]].lv+bJets[comb[1]].lv).M()


            #####
            higgsMass = None
            higgsJets = []
            for ijet, jet in enumerate(jets):
                if jet.matchFlag == 2:
                    higgsJets.append(jet)

            if len(higgsJets) == 2:
                higgs = higgsJets[0].lv + higgsJets[1].lv
                thisHiggsHisto.Fill(higgs.M(), w_event)
                higgsMass = higgs.M()

                
            thisMassHisto.Fill(bestMass, w_event)
            if len(higgsJets) == 2:
                thisMassHisto_hMatched.Fill(bestMass, w_event)
                thisHiggsMass2D.Fill(bestMass, higgsMass, w_event)
            else:
                thisMassHisto_hMatchedNot.Fill(bestMass, w_event)
            thisIndexHisto.Fill(minimumComb[0], w_event)
            thisIndexHisto.Fill(minimumComb[1], w_event)

            if isSR:
                thisMassHisto_SR.Fill(bestMass, w_event)
                thisIndexHisto_SR.Fill(minimumComb[0], w_event)
                thisIndexHisto_SR.Fill(minimumComb[1], w_event)
                if len(higgsJets) == 2:
                    thisMassHisto_hMatched_SR.Fill(bestMass, w_event)
                    thisHiggsMass2D_SR.Fill(bestMass, higgsMass, w_event)
                else:
                    thisMassHisto_hMatchedNot_SR.Fill(bestMass, w_event)
            else:
                thisMassHisto_CR.Fill(bestMass, w_event)
                thisIndexHisto_CR.Fill(minimumComb[0], w_event)
                thisIndexHisto_CR.Fill(minimumComb[1], w_event)
                if len(higgsJets) == 2:
                    thisMassHisto_hMatched_CR.Fill(bestMass, w_event)
                    thisHiggsMass2D_CR.Fill(bestMass, higgsMass, w_event)
                else:
                    thisMassHisto_hMatchedNot_CR.Fill(bestMass, w_event)
                

                    
            leading4bJets = bJets[0:4]
            combs = itertools.combinations(range(len(leading4bJets)),2)
            minimumComb4b = None
            minimumScore4b = 999999999.9
            for comb in combs:
                score = pow((leading4bJets[comb[0]].lv+leading4bJets[comb[1]].lv).M() - Bs[year], 2)/pow(Cs[year],2)
                if score < minimumScore4b:
                    minimumScore4b = score
                    minimumComb4b = comb
                    bestMass = (leading4bJets[comb[0]].lv+leading4bJets[comb[1]].lv).M()

            this4MassHisto.Fill(bestMass, w_event)
            if len(higgsJets) == 2:
                this4MassHisto_hMatched.Fill(bestMass, w_event)
                this4HiggsMass2D.Fill(bestMass, higgsMass, w_event)
            else:
                this4MassHisto_hMatchedNot.Fill(bestMass, w_event)
            this4IndexHisto.Fill(minimumComb[0], w_event)
            this4IndexHisto.Fill(minimumComb[1], w_event)
            if isSR:
                this4MassHisto_SR.Fill(bestMass, w_event)
                this4IndexHisto_SR.Fill(minimumComb[0], w_event)
                this4IndexHisto_SR.Fill(minimumComb[1], w_event)
                if len(higgsJets) == 2:
                    this4MassHisto_hMatched_SR.Fill(bestMass, w_event)
                    this4HiggsMass2D_SR.Fill(bestMass, higgsMass, w_event)
                else:
                    this4MassHisto_hMatchedNot_SR.Fill(bestMass, w_event)
            else:
                this4MassHisto_CR.Fill(bestMass, w_event)
                this4IndexHisto_CR.Fill(minimumComb[0], w_event)
                this4IndexHisto_CR.Fill(minimumComb[1], w_event)
                if len(higgsJets) == 2:
                    this4MassHisto_hMatched_CR.Fill(bestMass, w_event)
                    this4HiggsMass2D_CR.Fill(bestMass, higgsMass, w_event)
                else:
                    this4MassHisto_hMatchedNot_CR.Fill(bestMass, w_event)


            # combs = itertools.combinations(range(len(jets)),2)
            # minimumComb4b = None
            # minimumScore4b = 999999999.9
            # for comb in combs:
            #     score = pow((jets[comb[0]].lv+jets[comb[1]].lv).M() - Bs[year], 2)/pow(Cs[year],2)
            #     if score < minimumScore4b:
            #         minimumScore4b = score
            #         minimumComb4b = comb
            #         bestMass = (jets[comb[0]].lv+jets[comb[1]].lv).M()


                    
            # thisMassHisto_All.Fill(bestMass, w_event)
            # if len(higgsJets) == 2:
            #     thisHiggsMass2D_All.Fill(bestMass, higgsMass, w_event)
            # if isSR:
            #     thisMassHisto_All_SR.Fill(bestMass, w_event)
            #     if len(higgsJets) == 2:
            #         thisHiggsMass2D_All_SR.Fill(bestMass, higgsMass, w_event)
            # else:
            #     thisMassHisto_All_CR.Fill(bestMass, w_event)
            #     if len(higgsJets) == 2:
            #         thisHiggsMass2D_All_CR.Fill(bestMass, higgsMass, w_event)
                    
        canvases = []
        canvases.append(
            makeCanvasOfHistos(
                "higgsMass_%s"%year,
                "m_{bb, H} [GeV]",
                [thisHiggsHisto, thisMassHisto, thisMassHisto_SR, thisMassHisto_CR],
                legendText = ["Dijet matched to H", "SR+CR", "SR", "CR"],
                legendSize = (0.16,0.6,0.30,0.9), 
                colorList = [ROOT.kGreen+2, ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
                showHistoProperties = True,
            )
        )
        canvases.append(
            makeCanvasOfHistos(
                "higgs4Mass_%s"%year,
                "m_{bb, H} [GeV]",
                [thisHiggsHisto, this4MassHisto, this4MassHisto_SR, this4MassHisto_CR],
                legendText = ["Dijet matched to H", "SR+CR", "SR", "CR"],
                legendSize = (0.16,0.6,0.30,0.9),
                colorList = [ROOT.kGreen+2, ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
                addLabel = "4 leading b-jets",
                labelpos = (0.27,0.965),
                showHistoProperties = True,
            )
        )
        # canvases.append(
        #     makeCanvasOfHistos(
        #         "higgsMass_AllJets_%s"%year,
        #         "m_{bb, H} [GeV]",
        #         [thisHiggsHisto, thisMassHisto_All, thisMassHisto_All_SR, thisMassHisto_All_CR],
        #         legendText = ["Dijet matched to H", "SR+CR", "SR", "CR"],
        #         legendSize = (0.16,0.6,0.30,0.9), 
        #         colorList = [ROOT.kGreen+2, ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
        #         normalized = True,
        #         addCMS = True,
        #         simulation = True,
        #         lumi="{:2.1f}".format(lumi),
        #         addLabel = "All jets",
        #         labelpos = (0.27,0.965),
        #         showHistoProperties = True,
        #     )
        # )
        canvases.append(
            makeCanvasOfHistos(
                "higgsMass_onlyMatched_%s"%year,
                "m_{bb, H} [GeV]",
                [thisHiggsHisto, thisMassHisto_hMatched, thisMassHisto_hMatched_SR, thisMassHisto_hMatched_CR],
                legendText = ["Dijet matched to H", "SR+CR", "SR", "CR"],
                legendSize = (0.16,0.6,0.30,0.9), 
                colorList = [ROOT.kGreen+2, ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
                addLabel = "Events with matched H",
                labelpos = (0.12,0.965),
                showHistoProperties = True,
            )
        )
        canvases.append(
            makeCanvasOfHistos(
                "higgs4Mass_onlyMatched_%s"%year,
                "m_{bb, H} [GeV]",
                [thisHiggsHisto, this4MassHisto_hMatched, this4MassHisto_hMatched_SR, this4MassHisto_hMatched_CR],
                legendText = ["Dijet matched to H", "SR+CR", "SR", "CR"],
                legendSize = (0.16,0.6,0.30,0.9),
                colorList = [ROOT.kGreen+2, ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
                addLabel = "4 leading b-jets, Events with matched H",
                labelpos = (0.12,0.965),
                showHistoProperties = True,
            )
        )
        canvases.append(
            makeCanvasOfHistos(
                "higgsMass_notMatched_%s"%year,
                "m_{bb, H} [GeV]",
                [thisMassHisto_hMatchedNot, thisMassHisto_hMatchedNot_SR, thisMassHisto_hMatchedNot_CR],
                legendText = ["SR+CR", "SR", "CR"],
                legendSize = (0.16,0.6,0.30,0.9), 
                colorList = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
                addLabel = "Events w/o matched H",
                labelpos = (0.12,0.965),
                showHistoProperties = True,
            )
        )
        canvases.append(
            makeCanvasOfHistos(
                "higgs4Mass_notMatched_%s"%year,
                "m_{bb, H} [GeV]",
                [this4MassHisto_hMatchedNot, this4MassHisto_hMatchedNot_SR, this4MassHisto_hMatchedNot_CR],
                legendText = ["SR+CR", "SR", "CR"],
                legendSize = (0.16,0.6,0.30,0.9),
                colorList = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
                addLabel = "4 leading b-jets, Events w/o matched H",
                labelpos = (0.12,0.965),
                showHistoProperties = True,
            )
        )
        canvases.append(
            makeCanvasOfHistos(
                "higgsIndex_%s"%year,
                "Jet index",
                [thisIndexHisto, thisIndexHisto_SR, thisIndexHisto_CR],
                legendText = ["SR+CR", "SR", "CR"],
                legendSize = (0.16,0.65,0.30,0.9),
                colorList = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
            )
        )
        canvases.append(
            makeCanvasOfHistos(
                "higgs4Index_%s"%year,
                "Jet index",
                [this4IndexHisto, this4IndexHisto_SR, this4IndexHisto_CR],
                legendText = ["SR+CR", "SR", "CR"],
                legendSize = (0.16,0.65,0.30,0.9),
                colorList = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue],
                normalized = True,
                addCMS = True,
                simulation = True,
                lumi="{:2.1f}".format(lumi),
                addLabel = "4 leading b-jets",
                labelpos = (0.27,0.965)
            )
        )
        saveCanvasListAsPDF(canvases, outfilePrefix+"_fitEval_"+year, folder)
        canvases = []
        canvases.append(
            makeCanvas2D(
                thisHiggsMass2D,
                width=600, height=540,
                addCMS = True,
                lumi="{:2.1f}".format(lumi),
                CMSLabel = "Simulation",
                leftMargin=0.12,
                corrLabel=True, xstart=0.5,
            )
        )
        canvases.append(
            makeCanvas2D(
                thisHiggsMass2D_SR,
                width=600, height=540,
                addCMS = True,
                lumi="{:2.1f}".format(lumi),
                labelText = "SR Events",
                labelpos = (0.14, 0.85),
                CMSLabel = "Simulation",
                leftMargin=0.12,
                corrLabel=True, xstart=0.5,
            )
        )
        canvases.append(
            makeCanvas2D(
                thisHiggsMass2D_CR,
                width=600, height=540,
                addCMS = True,
                lumi="{:2.1f}".format(lumi),
                labelText = "CR Events",
                labelpos = (0.14, 0.85),
                CMSLabel = "Simulation",
                leftMargin=0.12,
                corrLabel=True, xstart=0.5,
            )
        )
        ###################################
        canvases.append(
            makeCanvas2D(
                this4HiggsMass2D,
                width=600, height=540,
                addCMS = True,
                labelText = "4 leading b-jets",
                labelpos = (0.14, 0.85),
                lumi="{:2.1f}".format(lumi),
                CMSLabel = "Simulation",
                leftMargin=0.12,
                corrLabel=True, xstart=0.5,
            )
        )
        canvases.append(
            makeCanvas2D(
                this4HiggsMass2D_SR,
                width=600, height=540,
                addCMS = True,
                lumi="{:2.1f}".format(lumi),
                labelText = "SR Events, 4 leading b-jets",
                labelpos = (0.14, 0.85),
                CMSLabel = "Simulation",
                leftMargin=0.12,
                corrLabel=True, xstart=0.5,
            )
        )
        canvases.append(
            makeCanvas2D(
                this4HiggsMass2D_CR,
                width=600, height=540,
                addCMS = True,
                lumi="{:2.1f}".format(lumi),
                labelText = "CR Events, 4 leading b-jets",
                labelpos = (0.14, 0.85),
                CMSLabel = "Simulation",
                leftMargin=0.12,
                corrLabel=True, xstart=0.5,
            )
        )
        #####################################
        # canvases.append(
        #     makeCanvas2D(
        #         thisHiggsMass2D_All,
        #         width=600, height=540,
        #         addCMS = True,
        #         labelText = "All jets",
        #         labelpos = (0.14, 0.85),
        #         lumi="{:2.1f}".format(lumi),
        #         CMSLabel = "Work in Progress",
        #         leftMargin=0.12,
        #         corrLabel=True, xstart=0.5,
        #     )
        # )
        # canvases.append(
        #     makeCanvas2D(
        #         thisHiggsMass2D_All_SR,
        #         width=600, height=540,
        #         addCMS = True,
        #         lumi="{:2.1f}".format(lumi),
        #         labelText = "SR Events, All jets",
        #         labelpos = (0.14, 0.85),
        #         CMSLabel = "Work in Progress",
        #         leftMargin=0.12,
        #         corrLabel=True, xstart=0.5,
        #     )
        # )
        # canvases.append(
        #     makeCanvas2D(
        #         thisHiggsMass2D_All_CR,
        #         width=600, height=540,
        #         addCMS = True,
        #         lumi="{:2.1f}".format(lumi),
        #         labelText = "CR Events, All jets",
        #         labelpos = (0.14, 0.85),
        #         CMSLabel = "Work in Progress",
        #         leftMargin=0.12,
        #         corrLabel=True, xstart=0.5,
        #     )
        # )
        
        saveCanvasListAsPDF(canvases, outfilePrefix+"_fitEval_2DPlots_"+year, folder)
        
                
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for calculating the values for the chi2 reconstuction of bb pairs to Higgs mass. Will fit  A*exp(-0.5*((x-B)/C)^2)',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--years",
        action = "store",
        help = "Years to be run",
        nargs = "+",
        default = ["2016"]
    )
    argumentparser.add_argument(
        "--outputName",
        action = "store",
        help = "Name of the output ROOT file",
        type=str,
        required = True
    )
    argumentparser.add_argument(
        "--outputFolder",
        action = "store",
        help = "Name of the output folder",
        type=str,
        default = "out_HiggsMass"
    )
    argumentparser.add_argument(
        "--reRunFile",
        action = "store_true",
        help = "If passed, script will attempt to rerun from a present file in **--outputFolder** with name **--outputName**.root",
    )
    argumentparser.add_argument(
        "--gaussAs",
        action = "store",
        help = "Start values for the fit",
        nargs = "+",
        type = float,
        default = [1, 1]
    )
    argumentparser.add_argument(
        "--gaussBs",
        action = "store",
        help = "Start values for the fit",
        nargs = "+",
        type = float,
        default = [1, 1]
    )
    argumentparser.add_argument(
        "--gaussCs",
        action = "store",
        help = "Start values for the fit",
        nargs = "+",
        type = float,
        default = [1, 1]
    )
    argumentparser.add_argument(
        "--doFit",
        action = "store_true",
        help = "If passed, the fit will be done",
    )
    argumentparser.add_argument(
        "--evalFit",
        action = "store_true",
        help = "If passed, the fit will be applied and plotted (also pass doFit)",
    )

    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging)

    print(args.gaussBs)
    
    startValsFit_A_Total = args.gaussAs[0]
    startValsFit_B_Total = args.gaussBs[0]
    startValsFit_C_Total = args.gaussCs[0]
    
    
    main(args.years, args.outputFolder, args.outputName, args.reRunFile,
         startValsFit_A_Total, startValsFit_B_Total, startValsFit_C_Total,
         evalFit = args.evalFit, doFit = args.doFit)
