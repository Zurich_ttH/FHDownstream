from copy import deepcopy
import logging
import itertools


import ROOT
from QCD_estimate_v2 import Config
from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, moveOverUnderFlow, project, makeStackPlotCanvas


def getHistograms(config, inputs, baseHisto, variable, category, eventSelection, weights, ID, blind=False):
    eventWeightData, eventWeightMC = weights

    histos = {}
    for proc in config.allProcs:
        histos[proc] = baseHisto.Clone(baseHisto.GetName().replace("baseHisto", proc+"_"+ID))
        if proc == "data":
            thisWeight = eventWeightData
            if blind:
                logging.warning("Blinding SR in data")
                if category == "9j4t":
                    eventSelection_ = eventSelection + "&& (Wmass4b<70 || Wmass4b>92)" 
                else:
                    eventSelection_ = eventSelection + "&& (Wmass4b<60 || Wmass4b>100)"
            else:
                eventSelection_ = eventSelection
        else:
            thisWeight = eventWeightMC
            eventSelection_ = eventSelection
        project(inputs["tree"][proc], histos[proc].GetName(), variable, eventSelection_, thisWeight, "{0} {1}".format(proc,ID))
        logging.debug("Integral of projected histogram: %s", histos[proc].Integral())
        moveOverUnderFlow(histos[proc], True, True)
    
    return histos 

def mergeHistos(config, histos, output):
    if output not in ["tt","minor","signals", "qcd"]:
        raise RuntimeError
    if output == "tt":
        toMerge = config.tt
    elif output == "minor":
        toMerge = config.minor
    elif output == "qcd":
        toMerge = config.qcd
    else:
        toMerge = config.signals
        
    logging.debug("Merging %s",toMerge)
    for iproc, proc in enumerate(toMerge):
        if iproc == 0:
            histos[output] = histos[proc].Clone(histos[proc].GetName().replace(proc, output))
        else:
            histos[output].Add(histos[proc])

def setStyle(histos):
    for histo in histos:
        if histo == "data":
            histos[histo].SetLineColor(ROOT.kBlack)
        elif histo == "tt":
            histos[histo].SetLineColor(ROOT.kRed)
        elif histo == "minor":
            histos[histo].SetLineColor(ROOT.kCyan)
        elif histo == "qcd":
            histos[histo].SetLineColor(ROOT.kGreen+2)
        elif histo == "signals":
            histos[histo].SetLineColor(ROOT.kBlue)
        else:
            pass

def getAllHistos(config, inputs, category, variable, prefix = None, blind=False):
    regionCut = []
    SRCut = config.regionCuts[category]["VRSelection"]
    CRCut = config.regionCuts[category]["CR2Selection"]
    baseline = config.BaselineSel

    selectionCR= "{0} && {1} && {2}".format(baseline, config.catCut[category], CRCut)
    selectionSR= "{0} && {1} && {2}".format(baseline, config.catCut[category], SRCut)
    
    
    if True:
        selectionSR = selectionSR.replace("((Wmass4b>30 && Wmass4b<60) || (Wmass4b>100 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")
        selectionCR = selectionCR.replace("((Wmass4b>30 && Wmass4b<60) || (Wmass4b>100 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")
        selectionSR = selectionSR.replace("((Wmass4b>30 && Wmass4b<70) || (Wmass4b>92 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")
        selectionCR = selectionCR.replace("((Wmass4b>30 && Wmass4b<70) || (Wmass4b>92 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")
        
    eventWeightMC = "{0}*{1}".format( config.weight, config.bCorr["MC"])
    eventWeightData = "{0}*{1}".format("1", config.bCorr["Data"])

    if prefix is None:
        hName_base = "{0}_baseHisto".format(category)
    else:
        hName_base = "{0}_{1}_baseHisto".format(prefix, category)
    baseHisto = ROOT.TH1F(hName_base, hName_base, 60, 0, 300)

    histosSR = getHistograms(config, inputs, baseHisto, variable, category, selectionSR, (eventWeightData, eventWeightMC), "SR", blind=blind)
    histosCR = getHistograms(config, inputs, baseHisto, variable, category, selectionCR, (eventWeightData, eventWeightMC), "CR")



    mergeHistos(config, histosSR, "tt")
    mergeHistos(config, histosCR, "tt")
    mergeHistos(config, histosSR, "minor")
    mergeHistos(config, histosCR, "minor")
    # mergeHistos(config, histosSR, "qcd")
    # mergeHistos(config, histosCR, "qcd")
    mergeHistos(config, histosSR, "signals")
    mergeHistos(config, histosCR, "signals")

    setStyle(histosSR)
    setStyle(histosCR)

    return histosSR, histosCR
    
def getSidebandPlots(config, inputs, category, outdir, blind=True):
    histosSR, histosCR = getAllHistos(config, inputs, category, "Wmass4b", blind=blind)
    
    for key in ["tt","minor","signals","data"]:
        if key == "data":
            name = "Data"
        elif key == "tt":
            name = "t#bar{t}"
        elif key == "minor":
            name = "Minor"
        elif key == "signals":
            name = "t#bar{t}H"
        elif key == "qcd":
            name = "Multijet (MC)"
        else:
            pass
        thisCanvasSR = makeCanvasOfHistos("SR_"+category+"_"+key,
                                          "M_{qq}",
                                          [histosSR[key]],
                                          lineWidth = 2,
                                          addLabel = "SR/VR {0} - {1} - {2}".format(name, category, int(round(histosSR[key].Integral()))),
                                          labelpos=(0.12,0.96))
        saveCanvasListAsPDF([thisCanvasSR], "SR_{0}_{1}".format(category, key),outdir)
        thisCanvasCR = makeCanvasOfHistos("CR_"+category+"_"+key,
                                          "M_{qq}",
                                          [histosCR[key]],
                                          lineWidth = 2,
                                          addLabel = "CR/CR2 {0} - {1} - {2}".format(name, category, int(round(histosCR[key].Integral()))),
                                          labelpos=(0.12,0.96))
        saveCanvasListAsPDF([thisCanvasCR], "CR_{0}_{1}".format(category, key),outdir)


    # histosSR =[histosSR["tt"], histosSR["minor"], histosSR["signals"], histosSR["qcd"], histosSR["data"]]
    # histosCR =[histosCR["tt"], histosCR["minor"], histosCR["signals"], histosCR["qcd"], histosCR["data"]]
    # legend = ["t#bar{t}", "Minor Bkg", "t#bar{t}H", "Multijet (MC)", "Data"]
    histosSR =[histosSR["tt"], histosSR["minor"], histosSR["signals"], histosSR["data"]]
    histosCR =[histosCR["tt"], histosCR["minor"], histosCR["signals"], histosCR["data"]]
    legend = ["t#bar{t}", "Minor Bkg", "t#bar{t}H", "Data"]
    
    SRCanvas = makeCanvasOfHistos("SR_"+category,
                                  "#M_{qq}",
                                  histosSR,
                                  legendText = legend,
                                  normalized = True,
                                  addIntLegend = True,
                                  lineWidth = 2,
                                  legendSize = (0.7,0.6,0.9,0.9),
                                  addLabel = "SR/VR - {0}".format(category),
                                  labelpos=(0.12,0.96))
    saveCanvasListAsPDF([SRCanvas], "ShapeComp_SR_{0}".format(category),outdir)
    CRCanvas = makeCanvasOfHistos("CR_"+category,
                                  "M_{qq}",
                                  histosCR,
                                  legendText = legend,
                                  normalized = True,
                                  addIntLegend = True,
                                  lineWidth = 2,
                                  legendSize = (0.7,0.6,0.9,0.9),
                                  addLabel = "CR/CR2 - {0}".format(category),
                                  labelpos=(0.12,0.96))
    saveCanvasListAsPDF([CRCanvas], "ShapeComp_cR_{0}".format(category),outdir)


def getSidebandCatCompPlots(config, inputs, categories, outdir):
    histosSR_4b, histosCR_4b = {}, {}
    catLegend = []
    catColors = []
    allColors = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed]
    for icat, cat in enumerate(categories):
        logging.info("Getting Wmass4b histograms for cat %s", cat)
        histosSR_4b[cat], histosCR_4b[cat] = getAllHistos(config, inputs, cat, "Wmass4b", "MW4b")
        if cat == "7j4t":
            catLegend.append("7 Jets, #geq 4 tags")
        elif cat == "8j4t":
            catLegend.append("8 Jets, #geq 4 tags")
        elif cat == "9j4t":
            catLegend.append("#geq 9 Jets, #geq 4 tags")
        else:
            catLegend.append(cat)
        catColors.append(allColors[icat])
    for key in ["tt","minor","signals","data"]:
        if key == "data":
            name = "Data"
        elif key == "tt":
            name = "t#bar{t}"
        elif key == "minor":
            name = "Minor"
        elif key == "signals":
            name = "t#bar{t}H"
        else:
            pass

        thisHistoListCR = []
        thisHistoListSR = []
        for cat in categories:
            thisHistoListCR.append(histosCR_4b[cat][key])
            thisHistoListSR.append(histosSR_4b[cat][key])

        thisCanvasCR = makeCanvasOfHistos("CR_catComp_"+key,
                                          "M_{W}",
                                          thisHistoListCR,
                                          legendText = catLegend,
                                          legendSize =  (0.5,0.6,0.9,0.9),
                                          colorList = catColors,
                                          lineWidth = 2,
                                          addLabel = "CR/CR2 {0}".format(name),
                                          normalized = True,
                                          labelpos=(0.12,0.96))
        saveCanvasListAsPDF([thisCanvasCR], "CatComp_cR_{0}".format(key),outdir)
        thisCanvasSR = makeCanvasOfHistos("SR_catComp_"+key,
                                          "M_{W}",
                                          thisHistoListSR,
                                          legendText = catLegend,
                                          legendSize =  (0.5,0.6,0.9,0.9),
                                          colorList = catColors,
                                          lineWidth = 2,
                                          addLabel = "SR/VR {0}".format(name),
                                          normalized = True,
                                          labelpos=(0.12,0.96))
        saveCanvasListAsPDF([thisCanvasSR], "CatComp_SR_{0}".format(key),outdir)
   
    
def compareWmass(config, inputs, category, outdir):
    logging.info("Getting Wmass histograms")
    histosSR, histosCR = getAllHistos(config, inputs, category, "Wmass")
    logging.info("Getting Wmass4b histograms")
    histosSR_4b, histosCR_4b = getAllHistos(config, inputs, category, "Wmass4b", "MW4b")
    for key in ["tt","minor","signals","data"]:
        if key == "data":
            name = "Data"
        elif key == "tt":
            name = "t#bar{t}"
        elif key == "minor":
            name = "Minor"
        elif key == "signals":
            name = "t#bar{t}H"
        else:
            pass
        # thisCanvasSR = makeCanvasOfHistos("SR_"+category+"_"+key,
        #                                   "M_{W}",
        #                                   [histosSR[key], histosSR_4b[key]],
        #                                   legendText = ["Old definition M_{W}", "M_{W} excl. 4 leading b-jets"],
        #                                   legendSize =  (0.5,0.6,0.9,0.9),
        #                                   colorList = [ROOT.kOrange, ROOT.kRed],
        #                                   lineWidth = 2,
        #                                   addLabel = "SR/VR {0} - {1}".format(name, category),
        #                                   labelpos=(0.12,0.96))
        # saveCanvasListAsPDF([thisCanvasSR], "SR_{0}_{1}".format(category, key),outdir+"/WmassComp")
        thisCanvasCR = makeCanvasOfHistos("CR_"+category+"_"+key,
                                          "M_{W}",
                                          [histosCR[key], histosCR_4b[key]],
                                          legendText = ["Old definition M_{W}", "M_{W} excl. 4 leading b-jets"],
                                          legendSize =  (0.5,0.6,0.9,0.9),
                                          colorList = [ROOT.kViolet, ROOT.kBlue],
                                          lineWidth = 2,
                                          addLabel = "CR/CR2 {0} - {1}".format(name, category),
                                          labelpos=(0.12,0.96))
        saveCanvasListAsPDF([thisCanvasCR], "CR_{0}_{1}".format(category, key),outdir+"/WmassComp")
        thisCanvasAll = makeCanvasOfHistos("CR_"+category+"_"+key,
                                           "M_{W}",
                                           [histosSR_4b[key], histosCR[key], histosCR_4b[key]],
                                           legendText = ["SR/VR: M_{W} excl. 4 leading b-jets",
                                                         "CR/CR2: Old definition M_{W}", "CR/CR2: M_{W} excl. 4 leading b-jets"],
                                           legendSize =  (0.5,0.6,0.9,0.9),
                                           colorList = [ ROOT.kRed, ROOT.kViolet, ROOT.kBlue],
                                           lineWidth = 2,
                                           addLabel = "{0} - {1}".format(name, category),
                                           labelpos=(0.12,0.96))
        saveCanvasListAsPDF([thisCanvasAll], "All_{0}_{1}".format(category, key),outdir+"/WmassComp")
        thisCanvasAll = makeCanvasOfHistos("CR_"+category+"_"+key,
                                           "M_{W}",
                                           [histosSR_4b[key], histosCR[key], histosCR_4b[key]],
                                           legendText = ["SR/VR: M_{W} excl. 4 leading b-jets",
                                                         "CR/CR2: Old definition M_{W}", "CR/CR2: M_{W} excl. 4 leading b-jets"],
                                           legendSize =  (0.5,0.6,0.9,0.9),
                                           colorList = [ROOT.kRed, ROOT.kViolet, ROOT.kBlue],
                                           lineWidth = 2,
                                           normalized = True,
                                           addLabel = "{0} - {1}".format(name, category),
                                           labelpos=(0.12,0.96))
        saveCanvasListAsPDF([thisCanvasAll], "All_norm_{0}_{1}".format(category, key),outdir+"/WmassComp")


def getCorrelation(config, inputs, category, set1, set2, outdir):
    combinations = list(itertools.product(set1, set2))
    logging.info("Will generate correlation plots for %s", combinations)
    
    
    
    
        
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "config file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outDir",
        action = "store",
        help = "categories to be run",
        type = str,
        default = "out_SidebandPlots"
    )
    argumentparser.add_argument(
        "--cats",
        action = "store",
        help = "categories to be run",
        type = str,
        nargs="+",
        default = ["7j4t", "8j4t", "9j4t"]
    )
    argumentparser.add_argument(
        "--fullRangeComp",
        action = "store_true",
        help = "Full range comp w/ Wmass4b",
    )
    argumentparser.add_argument(
        "--WmassComp",
        action = "store_true",
        help = "Comparison between Wmass and Wmass4b",
    )
    argumentparser.add_argument(
        "--crossCatComp",
        action = "store_true",
        help = "Comparison  shapes Wmass4b in the different categories passes",
    )
    argumentparser.add_argument(
        "--blind",
        action = "store_true",
        help = "Blinde SR of data histogram",
    )
    argumentparser.add_argument(
        "--correlation",
        action = "store_true",
        help = "PLot correlation between all combination of corrSet1 and corrSet2",
    )
    argumentparser.add_argument(
        "--corrSet1",
        action = "store",
        help = "First set of variables considered for corraltion",
        type = str,
        nargs="+",
        default = ["Wmass4b"],
    )
    argumentparser.add_argument(
        "--corrSet2",
        action = "store",
        help = "First set of variables considered for corraltion",
        type = str,
        nargs="+",
        default = ["4thBJet_bVal"],
    )
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    thisConfig = Config(args.config)

    logging.info("Getting trees:")
    fileObj = { "rFile" : {},
                "tree" : {}
    }
    for process in thisConfig.allProcs:
        logging.debug("Processing process: %s", process)
        fileObj["rFile"][process] = ROOT.TFile.Open(thisConfig.files[process])
        fileObj["tree"][process] = fileObj["rFile"][process].Get("tree")
        logging.debug("Got tree at %s", fileObj["tree"][process])

    ROOT.gROOT.LoadMacro("classes.h")
    ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
    ROOT.gROOT.LoadMacro("functions.h")
    if thisConfig.loadbCorrMacro:
        ROOT.gROOT.LoadMacro(thisConfig.bCorrMarco)

    if args.fullRangeComp:
        logging.info("Making sideband plots comparing the full range.")
        for cat in args.cats:
            logging.info("Processing %s", cat)
            getSidebandPlots(thisConfig, fileObj, cat, args.outDir)

    if args.WmassComp:
        logging.info("Camparing Wmass and Wmass4b")
        for cat in args.cats:
            logging.info("Processing %s", cat)
            compareWmass(thisConfig, fileObj, cat, args.outDir)

    if args.crossCatComp:
        logging.info("Making category comparison")
        getSidebandCatCompPlots(thisConfig, fileObj, args.cats, args.outDir)

    if args.correlation:
        for var in args.corrSet1+args.corrSet2:
            #if var not in thisConfig.
            pass
        logging.info("Making correlation plots")
        for cat in args.cats:
            logging.info("Processing cat %s", cat)
            getCorrelation(thisConfig, fileObj, cat, args.corrSet1, args.corrSet2, args.outDir)
