# Based on original implementation by V. Mikuni
import ROOT
import sys
import numpy as np
import array
import  tdrstyle
import CMS_lumi
from Plotting_cfg import *
tdrstyle.setTDRStyle()
# from within CMSSW:
ROOT.gSystem.Load('libCondFormatsBTauObjects') 
ROOT.gSystem.Load('libCondToolsBTau') 
ROOT.gROOT.SetBatch(True)
#ROOT.TGaxis.SetMaxDigits(2)
histFillColor =['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#a65628','#f781bf','#999999','#ffff33']


def singlePlot(h,legtext=[],flav='l',same=True,method = '', wp = 'tight',dire = ''):
    ROOT.gStyle.SetLegendFont(42)
    if same:
        c = ROOT.TCanvas('mycv'+h[0].GetName(),'mycv'+h[0].GetName(),5,30,540,600)
        SetupCanvas(c, 1,0)
        legend = ROOT.TLegend(x0_l-0.1,y0_l+0.1,x1_l-0.1, y1_l)
        legend.SetTextSize(4)
    else:
        c = ROOT.TCanvas('mycv'+h.GetName(),'mycv'+h.GetName(),5,30,600,600)
        SetupCanvas(c, 1,0)
        c.SetRightMargin(0.15)
        h.GetXaxis().SetMoreLogLabels(True)
        h.GetXaxis().SetTitle("p_{T} [GeV]")
        h.GetYaxis().SetTitle("#eta")

        if 'iterative2' in h.GetName():
            h.GetYaxis().SetTitle("Discriminator")
        h.GetYaxis().SetNdivisions(6,5,0)
        h.GetYaxis().SetTitleOffset(1)
        h.GetYaxis().SetTitleSize(0.2)
        h.Draw("colz")
        
    if same:
        for ihist in range(len(h)):
            
            #h[ihist].Scale(1.0/h[ihist].Integral())
            h[ihist].SetLineColor(ROOT.TColor.GetColor(histFillColor[ihist]))
            h[ihist].SetMarkerColor(ROOT.TColor.GetColor(histFillColor[ihist]))
            h[ihist].SetLineWidth(3)
            #h[ihist].SetMaximum(max(h[len(h)-1].GetMaximum()*0.5,0))
            if 'unc' in method:
                maxh = 0.1
                if flav == 'l':
                    maxh = 0.4
                if flav == 'c':
                    maxh = 0.2
                h[ihist].GetYaxis().SetRangeUser(0.01,maxh)
            else:
                h[ihist].GetYaxis().SetRangeUser(0.9,1.15)

            #h[ihist].SetMinimum(0.0)                                                                                                                                                                                                                                             

            yAxis = h[ihist].GetYaxis()
            xAxis = h[ihist].GetXaxis()
            yAxis.SetNdivisions(6,5,0)
            yAxis.SetTitleOffset(1.2)
            yAxis.SetTitleSize(0.05)
            yAxis.SetTitle('Unc. '+flav + ' [%]')
            xAxis.SetTitle('p_{T} [GeV]')
            xAxis.SetTitleSize(0.05)
            xAxis.SetMoreLogLabels(True)
            

            c.cd()
            
            legend.AddEntry(h[ihist],legtext[ihist],'p')
            h[ihist].Draw("psamex0E")


        #if len(h) == 2:
        #    rp = ROOT.TRatioPlot(h[0],h[1])
        #    rp.Draw("histsamex0")
        #    
        #    rp.GetLowerRefYaxis().SetTitle('ratio')
        #    rp.GetUpperRefYaxis().SetTitle('SF_{'+flav+'}')
        #    rp.GetUpperRefYaxis().SetTitleOffset(0.95)
        #    rp.GetLowerRefYaxis().SetTitleOffset(0.75)
        #    rp.GetLowerRefYaxis().SetLabelSize(0.03)
        #    rp.GetLowerRefXaxis().SetTitleSize(0.01)
        #    rp.GetLowerRefYaxis().SetRangeUser(0.7,1.3)
        #    rp.GetLowYaxis().SetNdivisions(2,0,0) #was 402                                                                                                                                                                                                                        
        #    rp.GetLowerRefXaxis().SetLabelSize(0.03)
        #    rp.GetLowerRefXaxis().SetTitleOffset(0.55)
        #    rp.GetLowerRefXaxis().SetTitleSize(0.06)
            
            
        legend.SetTextSize(0.03)
        legend.Draw('samex0')

    CMS_lumi.CMS_lumi(c, iPeriod, iPos)

    c.Update()
    c.Modified()
    c.RedrawAxis()

    if not os.path.exists("Plots/btagging/{0}".format(dire)):
        os.makedirs("Plots/btagging/{0}".format(dire))
    else:
        print "WARNING: directory already exists. Will overwrite existing files..."
    if 'unc' in method:
        c.SaveAs("Plots/btagging/{0}/SF{1}_{2}_unc.pdf".format(dire,flav,wp))
    else:
        c.SaveAs("Plots/btagging/{0}/SF{1}_{2}.pdf".format(dire,flav,wp))




pt_bin = array.array('f',[20,30,50,70,100,140,200,300,600,1000])
eta_bin = array.array('f',[0,0.8,1.6,2.4])
DeepCSV_bin = array.array('f',[0,0.0761,0.18635,0.2547,0.3231,0.3915,0.4599,0.5247,0.5859,0.6471,0.7083,0.7695,0.82105,0.863,0.905,0.947,0.989,1.1])
CSV_bin = array.array('f',[0,0.1356,0.4069,0.5731,0.6342,0.6954,0.7566,0.8178,0.85895,0.88,0.901,0.943,0.95915,0.97045,0.98175,0.99305,1.00435])
DeepJet_bin = array.array('f',[0,0.08,0.2,0.28,0.36,0.44,0.52,0.6,0.68,0.76,0.82,0.86,0.9,0.935,0.965,0.995,1.1])
directions = ['up','down']
flav_translate = {
    'b': 0,
    'c': 1,
    'l': 2,
    }
wp_translate = {
    'loose': 0,
    'medium': 1,
    'tight': 2,
    'iterativefit': 3,
}



sys_list = [
    # '_hf',
    # '_cferr1',
    # '_cferr2',
    # '_lf',
    # '_lfstats2',
    # '_lfstats1',
    # '_hfstats2',
    # '_hfstats1',

    "_jesAbsoluteStat",
    "_jesRelativeStatHF",
    "_jesAbsoluteMPFBias",
    "_jesFragmentation",
    #"_jesAbsoluteSample",
    "_jesPileUpPtRef",
    "_jesSinglePionECAL",
    "_jesRelativePtBB",
    "_jesRelativeFSR",
    "_jesRelativeJEREC1",
    "_jesPileUpPtEC1",
    "_jesRelativeStatEC",
    "_jesPileUpDataMC",
    "_jesRelativeJERHF",
    "_jesRelativeStatFSR",
    "_jesAbsoluteScale",
    "_jesFlavorQCD",
    "_jesRelativePtHF",
    "_jesTimePtEta",
    "_jesRelativePtEC1",
    "_jesRelativeJEREC2",
    "_jesSinglePionHCAL",
    "_jesRelativePtEC2",
    "_jesPileUpPtHF",
#    "_jes",
    "_jesRelativeBal",
    "_jesPileUpPtBB",
    "_jesPileUpPtEC2",
    
    ]


hists = []


def load_btagSF(weight_file,name,method,wp):
    v_sys = getattr(ROOT, 'vector<string>')()
    v_sys.push_back('central')
    for di in directions:
        if method != "iterativefit":
            v_sys.push_back(di)
        else:
            for sys in sys_list:
                v_sys.push_back(di+sys)

    calib = ROOT.BTagCalibration(name, weight_file)

    reader = ROOT.BTagCalibrationReader(
        wp,              # 0 is for loose op, 1: medium, 2: tight, 3: discr. reshaping
        'central',
        v_sys,          # vector of other sys. types
        )
    
    reader.load(
        calib, 
        0,          # 0 is for b flavour, 1: FLAV_C, 2: FLAV_UDSG 
        method      # measurement type
        )
    
    reader.load(
        calib, 
        1,          # 0 is for b flavour, 1: FLAV_C, 2: FLAV_UDSG 
        method      # measurement type
        )
    
    reader.load(
        calib, 
        2,          # 0 is for b flavour, 1: FLAV_C, 2: FLAV_UDSG 
        method      # measurement type
        )


    
    return reader


def btagSF(jet_pt,jet_eta,flav,jet_csv,reader):
    dic={}


    # making a std::vector<std::string>> in python is a bit awkward, 
    # but works with root (needed to load other sys types):
 
            

    # make a reader instance and load the sf data
    
    
    # in your event loop



    central = reader.eval_auto_bounds(
        'central',      # systematic (here also 'up'/'down' possible)
        flav,              # jet flavor
        abs(jet_eta),            # absolute value of eta
        abs(jet_pt),
        # pt
        jet_csv
        )

    
    dic['central'] = central

    for di in directions:
        if method != 'iterativefit':
           sf = reader.eval_auto_bounds(
               di,      # systematic (here also 'up'/'down' possible)
               flav,              # jet flavor
               abs(jet_eta),            # absolute value of eta
               abs(jet_pt),                 # pt
               jet_csv,
               )
           dic[di] = sf if sf !=0 else central
        else:
            for sys in sys_list:   
                # in your event loop
                sf = reader.eval_auto_bounds(
                    di+sys,      # systematic (here also 'up'/'down' possible)
                    flav,              # jet flavor
                    abs(jet_eta),            # absolute value of eta
                    abs(jet_pt),                 # pt
                    jet_csv,
                    )

                dic[di+sys] = sf if sf !=0 else central
            
    return dic



if __name__=='__main__':

    plots = []
    legs = []
    #flav = 'b'
    #flav = 'c'
    flav = 'l'
    #wp = 'loose'
    wp = 'medium'
    #wp = 'tight'
    method = 'comb' #For QCD + ttbar methods
    # method = 'incl' #For light jets
    # method = 'mujets' #For QCD with muons only
    #method = 'iterativefit'
    #wp = 'iterativefit'
    dire = 'all'
    if len(sys.argv) > 1:
        flav = sys.argv[1]
        method = sys.argv[2]
        wp = sys.argv[3]
        dire = sys.argv[4]
        print 'readng options from commandline'

    
    if 'Boosted' in dire:
        weight_files = {            
            'subjet CSVv2_2016':['csv/subjet_CSVv2_Moriond17_B_H.csv',ROOT.TH1D('subjet CSVv2_2016','subjet CSVv2_2016',len(pt_bin)-1,pt_bin)],
            'subjet_DeepCSV_2017':['csv/subjet_DeepCSV_94XSF_V4_B_F.csv',ROOT.TH1D('subjet_DeepCSV_2017','subjet_DeepCSV_2017',len(pt_bin)-1,pt_bin)],
            }
    elif '2016' in dire:
        weight_files = {
            
            'CSVv2_2016':['csv/CSVv2_Moriond17_B_H.csv',ROOT.TH1D('CSVv2_2016','CSVv2_2016',len(pt_bin)-1,pt_bin)],
            'DeepCSV_2016_Legacy':['csv/DeepCSV_2016LegacySF_V1.csv',ROOT.TH1D('DeepCSV_2016_Legacy','DeepCSV_2016_Legacy',len(pt_bin)-1,pt_bin)],
            
            'DeepCSV_2016':['csv/DeepCSV_Moriond17_B_H.csv',ROOT.TH1D('DeepCSV_2016','DeepCSV_2016',len(pt_bin)-1,pt_bin)],
            
            
            'DeepJet_2016_Legacy':['csv/DeepJet_2016LegacySF_V1.csv',ROOT.TH1D('DeepJet_2016','DeepJet_2016',len(pt_bin)-1,pt_bin)],
            }

    elif '2017' in dire:

        weight_files = {
            'DeepCSV_2017':['csv/DeepCSV_94XSF_V4_B_F.csv',ROOT.TH1D('DeepCSV_2017','DeepCSV_2017',len(pt_bin)-1,pt_bin)],
            'DeepJet_2017':['csv/DeepFlavour_94XSF_V2_B_F.csv',ROOT.TH1D('DeepJet_2017','DeepJet_2017',len(pt_bin)-1,pt_bin)],
            }

    elif '2018' in dire:

        weight_files = {
            'DeepCSV_2018':['csv/DeepCSV_102XSF_V1.csv', ROOT.TH1D('DeepCSV_2018','DeepCSV_2018',len(pt_bin)-1,pt_bin)],

            'DeepJet_2018':['csv/DeepJet_102XSF_V1.csv',ROOT.TH1D('DeepJet_2018','DeepJet_2018',len(pt_bin)-1,pt_bin)],
            
            }

    elif 'all' in dire:
        weight_files = {
            # 'DeepJet_ttH_2018':[ '/t3home/koschwei/work/tth/reference/scale_factors/btag/2018/sfs_deepjet_2018.csv',ROOT.TH1D('DeepJet_ttH_2018','DeepJet_ttH_2018',len(pt_bin)-1,pt_bin)],
            # 'DeepCSV_ttH_2018':[ '/t3home/koschwei/work/tth/reference/scale_factors/btag/2018/sfs_deepcsv_2018.csv', ROOT.TH1D('DeepCSV_ttH_2018','DeepCSV_ttH_2018',len(pt_bin)-1,pt_bin)],
            'DeepJet_ttH_2017':[ '/t3home/koschwei/work/tth/reference/scale_factors/btag/2017/sfs_deepjet_2017.csv',ROOT.TH1D('DeepJet_ttH_2017','DeepJet_ttH_2017',len(pt_bin)-1,pt_bin)],
            'DeepCSV_ttH_2017':[ '/t3home/koschwei/work/tth/reference/scale_factors/btag/2017/sfs_deepcsv_2017.csv', ROOT.TH1D('DeepCSV_ttH_2017','DeepCSV_ttH_2017',len(pt_bin)-1,pt_bin)],
            'DeepJet_ttH_2016':[ '/t3home/koschwei/work/tth/reference/scale_factors/btag/2016/sfs_deepjet_2016.csv',ROOT.TH1D('DeepJet_ttH_2016','DeepJet_ttH_2016',len(pt_bin)-1,pt_bin)],
            'DeepCSV_ttH_2016':[ '/t3home/koschwei/work/tth/reference/scale_factors/btag/2016/sfs_deepcsv_2016.csv', ROOT.TH1D('DeepCSV_ttH_2016','DeepCSV_ttH_2016',len(pt_bin)-1,pt_bin)],
#            'DeepJet_2018':[ 'DeepJet_102XSF_V1.csv',ROOT.TH1D('DeepJet_2018','DeepJet_2018',len(pt_bin)-1,pt_bin)],
#            'DeepCSV_2018':[ 'DeepCSV_102XSF_V1.csv', ROOT.TH1D('DeepCSV_2018','DeepCSV_2018',len(pt_bin)-1,pt_bin)],
            
            # 'DeepCSV_2018':['csv/DeepCSV_102XSF_V1.csv', ROOT.TH1D('DeepCSV_2018','DeepCSV_2018',len(pt_bin)-1,pt_bin)],
            # 'CSVv2_2016':['csv/CSVv2_Moriond17_B_H.csv',ROOT.TH1D('CSVv2_2016','CSVv2_2016',len(pt_bin)-1,pt_bin)],
            # 'DeepCSV_2016_Legacy':['csv/DeepCSV_2016LegacySF_V1.csv',ROOT.TH1D('DeepCSV_2016_Legacy','DeepCSV_2016_Legacy',len(pt_bin)-1,pt_bin)],
            # 'DeepCSV_2017':['csv/DeepCSV_94XSF_V4_B_F.csv',ROOT.TH1D('DeepCSV_2017','DeepCSV_2017',len(pt_bin)-1,pt_bin)],
            # #'DeepCSV_2016':['csv/DeepCSV_Moriond17_B_H.csv',ROOT.TH1D('DeepCSV_2016','DeepCSV_2016',len(pt_bin)-1,pt_bin)],
            # 'DeepJet_2017':['csv/DeepFlavour_94XSF_V2_B_F.csv',ROOT.TH1D('DeepJet_2017','DeepJet_2017',len(pt_bin)-1,pt_bin)],
            # 'DeepJet_2018':['csv/DeepJet_102XSF_V1.csv',ROOT.TH1D('DeepJet_2018','DeepJet_2018',len(pt_bin)-1,pt_bin)],
            # 'DeepJet_2016_Legacy':['csv/DeepJet_2016LegacySF_V1.csv',ROOT.TH1D('DeepJet_2016','DeepJet_2016',len(pt_bin)-1,pt_bin)],
    }
    else:
        print 'Cannot find the input files'
        sys.exit()



    do_unc =  True
    plot = ROOT.TH2D("iterativ","iterativ",len(pt_bin)-1,pt_bin,len(eta_bin)-1,eta_bin)
    for f in weight_files:
        print f
        plot = ROOT.TH2D("iterativ"+f,"iterativ"+f,len(pt_bin)-1,pt_bin,len(eta_bin)-1,eta_bin)
        if 'DeepCSV' in f:
            csv_bin = DeepCSV_bin
        elif 'CSVv2' in f:
            csv_bin = CSV_bin
        elif 'DeepJet' in f:
            csv_bin = DeepJet_bin
        else: print("I dont recognize tagger: ", f)

        plot2 = ROOT.TH2D("iterative2"+f,"iterative2"+f,len(pt_bin)-1,pt_bin,len(csv_bin)-1,csv_bin)
        reader = load_btagSF(weight_files[f][0],f,method,wp_translate[wp])
        for bin, val  in enumerate(pt_bin):
            if method == 'iterativefit':
                for bin_eta, val_eta  in enumerate(eta_bin):
                    pt = val +1e-5 #Small shift to keep in the correct bin
                    eta = val_eta +1e-5
                    d = btagSF(pt,eta,flav_translate[flav],0.8,reader)
                    total_sys = 0
                    if d['central'] == 0: continue
                    for sys in sys_list:
                        total_sys +=(d['up'+sys] - d['central'])**2
                    total_sys = (total_sys**0.5)/d['central']
                    plot.SetBinContent(bin+1,bin_eta+1,total_sys)
                for bin_csv, val_csv in enumerate(csv_bin):
                    pt = val +1e-5
                    csv = val_csv +1e-5
                    d = btagSF(pt,0.1,flav_translate[flav],csv,reader)
                    total_sys = 0
                    if d['central'] == 0: continue
                    for sys in sys_list:
                        total_sys +=(d['up'+sys] - d['central'])**2
                        # if bin+1 == 1 and bin_eta+1 == 4:
                        #     print total_sys, sys
                    total_sys = (total_sys**0.5)/d['central']
                    # if total_sys > 0.7:
                    #     print bin+1,bin_eta+1,total_sys
                    biny = plot2.GetYaxis().FindBin(csv)
                    plot2.SetBinContent(bin+1,bin_csv+1,total_sys)

            else:
                pt = val +1e-5
                d = btagSF(pt,1.2,flav_translate[flav],0.1,reader)
                #print d

                if d['central'] == 0: continue
                if do_unc:
                    weight_files[f][1].SetBinContent(bin+1,abs(d['up'] - d['central'])/d['central'])
                    weight_files[f][1].SetBinError(bin+1,0)
                
                else:
                    weight_files[f][1].SetBinContent(bin+1,d['central'])
                    weight_files[f][1].SetBinError(bin+1,abs(d['up'] - d['central']))

        plots.append(weight_files[f][1])
        legs.append(f.replace('_'," "))
        if 'iterativefit' in method:
            singlePlot(plot,flav=flav, same = False,wp = "{0}_{1}".format(wp,f),dire=dire)
            singlePlot(plot2,flav=flav, same = False,wp = "{0}2_{1}".format(wp,f),dire=dire)

    if method != 'iterativefit' :
        if do_unc:
            singlePlot(plots,legs,flav,method = 'unc',wp=wp,dire=dire)
        else:
            singlePlot(plots,legs,flav, wp = wp,dire=dire)
