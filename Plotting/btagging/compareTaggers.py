import logging

import ROOT

import os
import sys
sys.path.insert(0, os.path.abspath('..'))
from classes.PlotHelpers import project, initLogging


def main():

    lumi = 54.9

    basepath = "/t3home/koschwei/scratch/ttH/skims/2018/LegacyRunII_v1"

    files = {
        "JetHT" : "JetHT.root",
        "ttFH" : "TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root",
        "ttSL" : "TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root",
        "ttDL" : "TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root",
        "ttHbb" : "ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root"
    }

    nGen = {
        "JetHT" : 1,
        "ttFH" : 120589264,
        "ttSL" : 85394144,
        "ttDL" : 62658272,
        "ttHbb" : 7565864.5
    }

    xsec = {
        "JetHT" : 1,
        "ttFH" : 377.96,
        "ttSL" : 365.46,
        "ttDL" : 88.34,
        "ttHbb" : 0.2934045 
    }

    groups = {
        "data" : ["JetHT"],
        "tt" : ["ttFH","ttSL","ttDL"],
        "ttH" : ["ttHbb"]
    }

    selection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && passMETFilters == 1 && njets >= 6 "
    #selection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && passMETFilters == 1 && njets >= 7 && ((njets >= 9 && Wmass4b > 70 && Wmass4b < 92) || ((njets == 7 || njets == 8) && Wmass4b > 60 && Wmass4b < 100 ))"
    #selection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && passMETFilters == 1 && njets == 7 && ((njets >= 9 && Wmass4b > 70 && Wmass4b < 92) || ((njets == 7 || njets == 8) && Wmass4b > 60 && Wmass4b < 100 ))"
    #selection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && passMETFilters == 1 && njets == 8 && ((njets >= 9 && Wmass4b > 70 && Wmass4b < 92) || ((njets == 7 || njets == 8) && Wmass4b > 60 && Wmass4b < 100 ))"
    #selection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && passMETFilters == 1 && njets >= 9 && ((njets >= 9 && Wmass4b > 70 && Wmass4b < 92) || ((njets == 7 || njets == 8) && Wmass4b > 60 && Wmass4b < 100 ))"
    selectionCSV = "nBDeepCSVM>=4 "
    selectionFlav = "nBDeepFlavM>=4 "
    selectionCSV = "nBDeepCSVM>=2 "
    selectionFlav = "nBDeepFlavM>=2 "
    weight = "puWeight * (sign(genWeight))"
    weightCSV = "btagDeepCSVWeight_shape"
    weightFlav = "btagDeepFlavWeight_shape"

    hBase = ROOT.TH1F("hBase","hBase",1,0,1)

    histos = {}
    rFiles = {}
    trees = {}

    runAlgos = ["DeepCSV", "DeepFlav"]
    
    for algo in runAlgos:
        histos[algo] = {}
        logging.info("Processing algorithm: %s", algo)
        for proc in files:
            if not proc in rFiles:
                rFiles[proc] = ROOT.TFile.Open(basepath+"/"+files[proc])
                trees[proc] = rFiles[proc].Get("tree")
            thisHName = proc+"_"+algo
            histos[algo][proc] = hBase.Clone(thisHName)

            

            if algo == "DeepCSV":
                thisSelection = "{0} && {1}".format(selection, selectionCSV)
                thisWeight = "{0} * {1}".format(weight, weightCSV)
            else:
                thisSelection = "{0} && {1}".format(selection, selectionFlav)
                thisWeight = "{0} * {1}".format(weight, weightFlav)
            if proc == "JetHT":
                thisWeight = "1"

                
            project(trees[proc], thisHName, "0", thisSelection, thisWeight, "{0} - {1}".format(proc, algo))

            if proc != "JetHT":
                sf = (lumi*1000*xsec[proc])/nGen[proc]
                logging.debug("Scaling %s with %s", thisHName, sf)
                histos[algo][proc].Scale(sf)
            
        for group in groups:
            for iElem, elem in enumerate(groups[group]):
                logging.debug("Adding %s to group %s", elem, group)
                if iElem == 0:
                    histos[algo][group] = histos[algo][elem].Clone(histos[algo][elem].GetName()+"_merged")
                else:
                    histos[algo][group].Add(histos[algo][elem])
        print histos[algo]



    print histos
    logging.info("%s \t %s \t %s","Process", runAlgos[0], runAlgos[1])
    for group in groups:
        logging.info("%s \t %s \t %s",group, histos[runAlgos[0]][group].Integral(), histos[runAlgos[1]][group].Integral())


if __name__ == "__main__":
    loglevel = 20
    initLogging(loglevel)
    main()
