# Implementation by V. Mikuni
import CMS_lumi
import ROOT as rt
import array
import pandas as pd
import numpy as np
import  tdrstyle
import os

rt.gROOT.SetBatch(False)
tdrstyle.setTDRStyle()

destination = '../plots'
root_folder = '../root'

histFillColor =['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#a65628','#f781bf','#999999','#ffff33']
histLineColor = rt.kBlack
markerSize  = 1.0
#CMS Style
iPeriod = 0
iPos = 11
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "35.9 fb^{-1} (13 TeV)" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)
if( iPos==0 ): CMS_lumi.relPosX = 0.12
H_ref = 600;
W_ref = 540;
x1_l = 0.95
y1_l = 0.90
dx_l = 0.30
dy_l = 0.28
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l
ar_l = dy_l/dx_l
W = W_ref
H  = H_ref
# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref







def SetupBox(box_,yy_,fill = rt.kBlack):

    box_.SetLineStyle( rt.kSolid )
    box_.SetLineWidth( 1 )
    box_.SetLineColor( rt.kBlack )
    box_.SetFillColor(fill)






def SetupCanvas(c,logx,logy):
    c.SetFillColor(0)
    c.SetBorderMode(0)
    c.SetFrameFillStyle(0)
    c.SetFrameBorderMode(0)
    c.SetLeftMargin( L/W_ref)
    c.SetRightMargin( R/W_ref)
    c.SetTopMargin( T/H )
    c.SetBottomMargin( B/H )
    c.SetTickx(0)
    c.SetTicky(0)
    if logx: c.SetLogx()
    if logy: c.SetLogy()


def AddOverflow(h):
    b0 = h.GetBinContent(0)
    e0 = h.GetBinError(0)
    nb = h.GetNbinsX()
    bn = h.GetBinContent(nb + 1)
    en = h.GetBinError(nb + 1)

    h.SetBinContent(0, 0)
    h.SetBinContent(nb+1, 0)
    h.SetBinError(0, 0)
    h.SetBinError(nb+1, 0)

    h.SetBinContent(1, h.GetBinContent(1) + b0)
    h.SetBinError(1, (h.GetBinError(1)**2 + e0**2)**0.5 )

    h.SetBinContent(nb, h.GetBinContent(nb) + bn)
    h.SetBinError(nb, (h.GetBinError(nb)**2 + en**2)**0.5 )

def AddOverflow2D(h):
    nbx = h.GetNbinsX()
    nby = h.GetNbinsY()
    for ix in range(nbx):
        b0x=h.GetBinContent(ix,0)
        e0x=h.GetBinError(ix,0)
        h.SetBinContent(ix,0, 0)
        h.SetBinError(ix,0, 0)
        h.SetBinContent(ix,1, h.GetBinContent(ix,1) + b0x)
        h.SetBinError(ix,1, (h.GetBinError(ix,1)**2 + e0x**2)**0.5 )


        bx = h.GetBinContent(ix,nby+1)
        ex = h.GetBinContent(ix,nby+1)
        h.SetBinContent(ix,nby+1, 0)
        h.SetBinError(ix,nby+1, 0)
        h.SetBinContent(ix,nby, h.GetBinContent(ix,nby) + bx)
        h.SetBinError(ix,nby, (h.GetBinError(ix,nby)**2 + ex**2)**0.5 )

    for iy in range(nby):
        b0y=h.GetBinContent(0,iy+1)
        e0y=h.GetBinError(0,iy+1)
        h.SetBinContent(0,iy+1,0)
        h.SetBinError(0,iy+1,0)
        h.SetBinContent(1,iy+1, h.GetBinContent(1,iy+1) + b0y)
        h.SetBinError(1,iy+1, (h.GetBinError(1,iy+1)**2 + e0y**2)**0.5 )


        by = h.GetBinContent(nbx+1,iy+1)
        ey = h.GetBinContent(nbx+1,iy+1)
        h.SetBinContent(nbx+1,ix+1, 0)
        h.SetBinError(nbx+1,iy+1, 0)
        h.SetBinContent(nbx,iy+1, h.GetBinContent(nbx,iy+1) + by)
        h.SetBinError(nbx,iy+1, (h.GetBinError(nbx,iy+1)**2 + ey**2)**0.5 )


