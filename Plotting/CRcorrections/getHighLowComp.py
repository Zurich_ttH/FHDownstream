import sys
import os
import logging 
sys.path.insert(0, os.path.abspath('..'))
from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF, initLogging

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)


def getHighLowComp(inputFile, output, outFolder, isMC=True, lumi="59.7"):
    logging.info("Opening")

    rFile = ROOT.TFile.Open(inputFile)

    hHigh = rFile.Get("3DHisto_high")
    hLow = rFile.Get("3DHisto_low")

    
    for name, axis in [("Jet p_{T}", 0),("Jet #eta", 1),("#Delta R_{min}", 2)]:
        thisHistoHigh = None
        thisHistoLow = None
        if axis == 0:
            thisHistoHigh = hHigh.ProjectionX()
            thisHistoLow = hLow.ProjectionX()
            floatingMax = 1.15
        elif axis == 1:
            thisHistoHigh = hHigh.ProjectionY()
            thisHistoLow = hLow.ProjectionY()
            floatingMax = 1.5
        else:
            thisHistoHigh = hHigh.ProjectionZ()
            thisHistoLow = hLow.ProjectionZ()
            floatingMax = 1.35

        thisHistoHigh.Rebin(2)
        thisHistoLow.Rebin(2)
            
        thisHistoHigh.SetLineWidth(2)
        thisHistoLow.SetLineWidth(2)
            
        radiationRatio = RatioPlot("HighVsLow"+str(axis))
        radiationRatio.passHistos([thisHistoHigh, thisHistoLow], normalize=True)
        radiationRatio.ratioText = "#frac{High b-tag}{Low b-tag}"
        radiationRatio.thisCMSLabel = "Simulation"
        radiationRatio.thisLumi = lumi
        radiationRatio.CMSscale = (1.2, 1.2)
        radiationRatio.moveCMSTop = 0.085
        radiationRatio.yTitleOffset = 0.85
        #radiationRatio.ratioRange = (0.6,1.4)
        radiationRatio.ratioLineWidth = 2
        radiationRatio.CMSLeft = True
        radiationRatio.CMSOneLine = True
        radiationRatio.useDefaultColors = True
        radiationRatio.defaultColors = [ROOT.kBlue, ROOT.kRed]
        radiationRatio.legendSize = (0.3,0.6,0.9,0.9)

        canvas = radiationRatio.drawPlot(
            ["Medium tagged b-jets", "Loose but not medium tagged b-jets"],
            xTitle = name,
            floatingMax = floatingMax,
            drawCMS = True
        )
        
        saveCanvasListAsPDF([canvas], output+"_axis"+str(axis), outFolder)

        
if __name__ == "__main__":
    import argparse
    ##############################################################################################
    ##############################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file (expecting output .root file from getCorrection.py",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--out",
        action = "store",
        help = "Output file name",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outFolder",
        action = "store",
        help = "Output folder",
        type = str,
        default = "."
    )
    args = argumentparser.parse_args()
    initLogging(args.logging, 24)

    getHighLowComp(args.inputFile, args.out, args.outFolder)
    
