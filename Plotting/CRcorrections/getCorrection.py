"""
getCorrection.py

Script for calculating the CR correction from skimmed nTuples

Procedure:
- Define functions that are fitted
- Project kinematic distributions (pT. eta, DeltaR) from the tree into THNF
- For the correction calucaltion 1D ratios of the current variable is reuqired
- Get the correction: 
  0.) Get the ratios from the ND histogram
  1.) Fit the function for a given variabe to its ratio histogram
  2.) Add the fit result to a ND function (x,y,z,..)
  3.) Apply this function to the ND histogram (or a clone)
  4.) Start with 1. but using the corrected ND histogram
- Make control plots
- Write correction function to C++ file as ROOT macro

2019, K. Schweiger
"""
from __future__ import division

import ROOT
import sys, os
from shutil import copyfile
import logging
from copy import deepcopy
import itertools
import time
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

sys.path.insert(0, os.path.abspath('..'))
from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging, project, makeCanvas2D, set_palette

ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize");

class FitInfo(object):
    """
    Class for storing the setting for the fit of a given variable passed in the configuration file
    """
    def __init__(self, name, varName, niceName, nBins, minBin, maxBin, fit_string, fit_function, initVals, options, ratioMax, ratioMin, silent=False):
        self.name = name
        self.varName = varName
        self.niceName = niceName
        self.nBins = int(nBins)
        self.minBin = float(minBin)
        self.maxBin = float(maxBin)
        self.fitOptions = options
        self.fit_string_raw = fit_string
        self.fit_string_function = fit_function
        self.ratioMax = ratioMax
        self.ratioMin = ratioMin
        self.initMC = None
        self.initData = None
        initVals = initVals.split("\n")
        if fit_string.startswith("pol"):
            nPol = int(fit_string.replace("pol",""))
            self.nVals = nPol + 1
            if self.fit_string_raw.startswith("pol"):
                for i in range(self.nVals):
                    if i == 0:
                        self.fit_string_raw = "{0}"
                    else:
                        self.fit_string_raw += " + {"+str(i)+"}*pow(x,"+str(i)+")"
                self.fit_string_raw = "(" + self.fit_string_raw  + ")"
            for line in initVals:
                self.setinitVals(line)

        else:
            for line in initVals:
                self.setinitVals(line)
            self.nVals = len(self.initMC)

        _initMC = []
        for val in self.initMC:
            _initMC.append(float(val))
        self.initMC = _initMC

        _initData = []
        for val in self.initData:
            _initData.append(float(val))
        self.initData = _initData

        assert len(self.initMC) == len(self.initData)
        assert len(self.initData) == self.nVals

        if not silent:
            logging.debug("---- FitInfo for %s ----",  self.name)
            logging.debug("Variable name : %s", self.varName)
            logging.debug("Variable axis name : %s", self.niceName)
            logging.debug("Binning: %s, %s, %s", self.nBins, self.minBin, self.maxBin)
            logging.debug("Fit String: %s", self.fit_string_raw)
            logging.debug("Fit Function (for the fit): %s", self.fit_string_function)
            logging.debug("Funtion definitions: nValues = %s", self.nVals)
            logging.debug("Fit options: %s", self.fitOptions)
            logging.debug("(MC) init Vals: %s", self.initMC)
            logging.debug("(Data) init Vals: %s", self.initData)
        
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            #Init values are not considered here on purpose!
            for attr in ["name", "varName", "nBins","minBin","maxBin",
                         "fit_string_raw","fit_string_function","nVals"]:
                if getattr(self, attr, None) != getattr(other, attr, None):
                    return False
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        else:
            return NotImplemented
        

    def setinitVals(self, line):
        if line == "":
            return False
        name, val = line.split(" : ")
        if not "auto" in val:
            if name == "data":
                self.initData = val.replace(" ", "").split(",")
            elif name == "MC":
                self.initMC = val.replace(" ", "").split(",")
            else:
                raise KeyError("Unsupported key for initval in config")
        else:
            if name == "data":
                self.initData = self.nVals*[1]
            elif name == "MC":
                 self.initMC = self.nVals*[1]
            else:
                raise KeyError("Unsupported key for initval in config")
        return True

class Config(object):
    """
    Class for storing reading and string the information for the passed configuration file
    """
    def __init__(self, pathtoConfig, reEta=False, silent=False):
        logging.info("Loading config %s", pathtoConfig)
        import ConfigParser
        thisconfig = ConfigParser.ConfigParser()
        thisconfig.optionxform = str #Use this so the section names keep Uppercase letters
        thisconfig.read(pathtoConfig)
        self.path = pathtoConfig
        #### BASE INFORMATION
        self.outputpath = thisconfig.get("General", "output")
        if not self.outputpath.endswith("/"):
            self.outputpath = self.outputpath+"/"
        self.lumi = thisconfig.getfloat("General", "lumi")
        self.era = thisconfig.getint("General", "era")
        self.trigger = thisconfig.get("General", "trigger")
        self.loadMacros = thisconfig.getboolean("General", "loadMacros")
        self.baseSelection = thisconfig.get("General", "basesel")
        self.EventWeight = thisconfig.get("General", "weight")
        self.modWeight = thisconfig.get("General", "moduleWeight")
        self.jetBranchBase = thisconfig.get("General", "jetBranchBase")
        self.bTagBranch = thisconfig.get("General", "bTagBranch")
        self.variables = self.getList(thisconfig.get("General", "variables"))
        self.reEtaCorrection = reEta
        if reEta:
            self.outputpath = self.outputpath+"reEta/"
        self.workingPoints = {}


        if thisconfig.has_option("General", "sidebandVal"):
            self.doSidebandValidation = thisconfig.getboolean("General", "sidebandVal")
            self.sidebandSplit = float(thisconfig.get("General", "sidebandSplit"))
        else:
            self.doSidebandValidation = False
            self.sidebandSplit = -1.0
            
        if not silent:
            logging.debug("============= CONFIG =============")
            logging.debug("Lumi : %s", self.lumi)
            logging.debug("Trigger  : %s", self.trigger)
            logging.debug("Selection  : %s", self.baseSelection)
            logging.debug("Weight  : %s", self.EventWeight)
            logging.debug("Jet base branch name : %s", self.jetBranchBase )
            logging.debug("Btag branch name : %s", self.bTagBranch )
            logging.debug("Flags : loadMacros : %s - reEta : %s - sideband : %s", self.loadMacros, self.reEtaCorrection, self.doSidebandValidation)
        allWPS = self.readMulitlineOption(thisconfig.get("General", "WorkingPoints"), "Single")
        for wp in allWPS:
            self.workingPoints[wp] = allWPS[wp]
            
        self.workingPoints["maximum"] = 1.0
        if self.doSidebandValidation:
            logging.warning("Will do Sideband validation")
            self.workingPoints["maximum"] = self.workingPoints["medium"]
            self.workingPoints["medium"] = self.sidebandSplit
            
        logging.debug("WP: %s",  self.workingPoints)
        basepath = thisconfig.get("General", "basepath")
        if not basepath.endswith("/"):
            basepath = basepath + "/"
        
        self.files = {}
        self.files["data"] = basepath + thisconfig.get("General", "data")
        self.xsec = {"data" : 1}
        self.nGen = {"data" : 1}
        bkgFiles = self.readMulitlineOption(thisconfig.get("General", "background"), "Single")
        bkgXsec = self.readMulitlineOption(thisconfig.get("General", "xsec"), "Single")
        bkgnGen = self.readMulitlineOption(thisconfig.get("General", "nGen"), "Single")
        
        if not (set(bkgFiles.keys()) == set(bkgXsec.keys()) and set(bkgXsec.keys()) == set(bkgnGen.keys())):
            raise KeyError("Check config! background, xsec and nGen do not have the same elements!")

        self.backgrounds = bkgFiles.keys()
        if not silent:
            logging.debug("Found %s as backgrounds", self.backgrounds)
            logging.debug("Background: %s", self.backgrounds )
        for bkg in bkgFiles:
            self.files[bkg] = basepath + bkgFiles[bkg]
            self.xsec[bkg] = float(bkgXsec[bkg])
            self.nGen[bkg] = float(bkgnGen[bkg])
            logging.debug("%s : %s, %s, %s", bkg, self.xsec[bkg], self.nGen[bkg], self.files[bkg])


        if not os.path.isdir(self.outputpath):
            logging.warning("Creating dir %s", self.outputpath)
            os.makedirs(self.outputpath)
        #### FIT INFORMATION
        self.fitOrder = self.getList(thisconfig.get("General", "reEtaOrder")) if reEta else self.getList(thisconfig.get("General", "fitOrder"))
        if not silent:
            logging.debug("Fit Order: %s",self.fitOrder)
        self.fits = {}
        for section in thisconfig.sections():
            if not "Variables_" in section:
                continue
            thisVariable = section.split("_")[1]
            rMax, rMin = (thisconfig.get(section, "ratioRange").replace(" ","")).split(",")
            #varName, nBins, minBin, maxBin, fit_string, fit_function, initVals
            self.fits[thisVariable] = FitInfo( thisVariable,
                                               thisconfig.get(section, "varName"),
                                               thisconfig.get(section, "niceName"),
                                               thisconfig.get(section, "nBins"),
                                               thisconfig.get(section, "min"),
                                               thisconfig.get(section, "max"),
                                               thisconfig.get(section, "fitStr"),
                                               thisconfig.get(section, "fitFunc"),
                                               thisconfig.get(section, "initVal"),
                                               thisconfig.get(section, "options"),
                                               float(rMax), float(rMin),
                                               silent = silent)
        

    def __eq__(self, other):
        """ 
        Implementation of equal for config objet.
        IMPORTANT: This equality obly checks if the setting for the projection are equal!
        """
        if isinstance(other, self.__class__):
            for fit in self.fits:
                if self.fits[fit] != other.fits[fit]:
                    return False
            if self.files.keys() != other.files.keys():
                return False
            for proc in self.files:
                if self.files[proc] != other.files[proc]:
                    return False
                if self.xsec[proc] != other.xsec[proc]:
                    return False
                if self.nGen[proc] != other.nGen[proc]:
                    return False
            for attr in ["baseSelection", "EventWeight", "modWeight",
                         "jetBranchBase", "bTagBranch", "variables",
                         "workingPoints", "lumi", "trigger"]:
                if getattr(self, attr, None) != getattr(other, attr, None):
                    return False
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        else:
            return NotImplemented
        
    @staticmethod
    def readMulitlineOption(option, optionType):
        ret = {}
        for elem in option.split("\n"):
            if elem == "":
                continue
            if optionType == "Single":
                name, value = elem.split(" : ")
            elif optionType == "List":
                name, value = elem.split(" ")
                value = self.getList(value)
            else:
                raise RuntimeError
            logging.info("Found: %s = %s", name, value)
            ret[name] = value

        return ret

    @staticmethod
    def getList(value):
        value = value.replace(" ", "") 
        return value.split(",")

def getProjection(histo3D, axis, name = "", normalized=True):
    """
    Get the 1D projection of a 3D histogram
    Args:
      histo3D (TH3F)
      axis (int) : 0:X, 1:Y, 2:Z
      normalized (bool) : If true distribution will be normalized
    Returns:
      hProjection (TH1F) : Projection on *axis* of *histo3D*
    """
    if axis == 0:
        hProjection = histo3D.ProjectionX(histo3D.GetName()+"_X_"+name, 0, -1,0,-1,"e")
        logmsg = "X"
    elif axis == 1:
        hProjection = histo3D.ProjectionY(histo3D.GetName()+"_Y_"+name, 0, -1,0,-1,"e")
        logmsg = "Y"
    elif axis == 2:
        hProjection = histo3D.ProjectionZ(histo3D.GetName()+"_Z_"+name, 0, -1,0,-1,"e")
        logmsg = "Z"
    else:
        raise RuntimeError("Passed axis %s is out of bound"%axis)

    logging.debug("Project %s on axis %s", histo3D, logmsg)
    if normalized:
        hProjection.Scale(1.0/hProjection.Integral())

    return deepcopy(hProjection)

def getRatio(histo3D_num, histo3D_denom, axis, name="", normalized=True, yMin = 0.6, yMax = 1.2):
    """
    Get the ratio between passed histos of the proection on axis 
    Args:
8      histo3D_num (TH3F) : Hisogram used for the numerator
      histo3D_denom (TH3F) : Hisogram used for the denominator
      name (str) : Identifier used in the histo name
      axis (int) : 0:X, 1:Y, 2:Z
    
    Returns:
      ratio (TH1F) : Ratio of projection
    """
    if axis == 0:
        hProjection_num = histo3D_num.ProjectionX(histo3D_num.GetName()+"_X_"+name, 0, -1,0,-1,"e")
        hProjection_denom = histo3D_denom.ProjectionX(histo3D_denom.GetName()+"_X_"+name, 0, -1,0,-1,"e")
        logmsg = "X"
    elif axis == 1:
        hProjection_num = histo3D_num.ProjectionY(histo3D_num.GetName()+"_Y_"+name, 0, -1,0,-1,"e")
        hProjection_denom = histo3D_denom.ProjectionY(histo3D_denom.GetName()+"_Y_"+name, 0, -1,0,-1,"e")
        logmsg = "Y"
    elif axis == 2:
        hProjection_num = histo3D_num.ProjectionZ(histo3D_num.GetName()+"_Z_"+name, 0, -1,0,-1,"e")
        hProjection_denom = histo3D_denom.ProjectionZ(histo3D_denom.GetName()+"_Z_"+name, 0, -1,0,-1,"e")
        logmsg = "Z"
    else:
        raise RuntimeError("Passed axis %s is out of bound"%axis)
    logging.debug("Numerator: Project %s on axis %s", histo3D_num, logmsg)
    logging.debug("Denominator: Project %s on axis %s", histo3D_denom, logmsg)
    if normalized:
        hProjection_num.Scale(1/hProjection_num.Integral())
        hProjection_denom.Scale(1/hProjection_denom.Integral())

    ratio = hProjection_num.Clone("ratio_%s_%s"%(logmsg, name))
    ratio.Divide(hProjection_denom)

    ratio.GetYaxis().SetRangeUser(yMin, yMax)
    
    return deepcopy(ratio)

def plotVariables1D(config, variables, histoHigh, histoLow, prefix, isData):
    """ Function for making the comarison of high and low distributins (before fit) """
    for ivar, var in enumerate(variables):
        logging.debug("Making plots for %s with axis %s", var, ivar)
        hProj_High = getProjection(histoHigh, ivar)
        hProj_Low = getProjection(histoLow, ivar)
        thisRatio = RatioPlot("PreCorrection_%s"%var)
        thisRatio.legendSize = (0.6, 0.6, 0.8, 0.8)
        thisRatio.passHistos([hProj_High, hProj_Low])
        thisRatio.addLabel("Data" if isData else "Simulation", 0.15,0.85, 1.2)
        thisRatio.useDefaultColors = True
        thisRatio.CMSLeft = True
        thisRatio.moveCMSTop = 0.084
        thisRatio.labelScaleX = 0.97
        thisRatio.CMSOneLine = True
        thisRatio.CMSscale = (1.0, 1.2)
        thisRatio.thisLumi = "{0:.1f}".format(config.lumi)
        canvas = thisRatio.drawPlot(["High b-tag region", "Low b-tag region"],
                                    xTitle = config.fits[var].niceName,
                                    drawCMS = True)
        saveCanvasListAsPDF([canvas], "%s_%s"%(prefix, var), config.outputpath)

def plotPostFitRatios(config, variable, ivar, pairs, legend, fitFunc, confIntervall, prefix, isData):
    """
    Function for plotting the 1D distributions after fitting
    Args:
      config (Config) : Config object
      variable (str) : Variable to be plotted
      ivar (int) : 0,1,2 --> Required for projection
      pairs (list) : List of tuples of TH3F
      prefix (str) : Prefix for the output files
    """
    assert len(pairs) == len(legend)
    allPlots = []
    fitFunc.SetLineColor(ROOT.kRed)
    confIntervall.SetFillColor(ROOT.kRed-9)
    for histoNum, histoDenom in pairs:
        allPlots.append(getRatio(histoNum, histoDenom, ivar, prefix+"_"+variable))
    for i in range(len(allPlots)):
        allPlots[i].GetYaxis().SetLabelSize(allPlots[i].GetYaxis().GetLabelSize()*1.2)
    thisRatio = RatioPlot("PostCorrectionRatios_%s"%variable)
    thisRatio.legendSize = (0.2, 0.1, 0.6, 0.3)
    thisRatio.passHistos(allPlots)
    thisRatio.useDefaultColors = True
    thisRatio.drawHorisontalLine = True
    thisRatio.invertRatioOrder = True
    thisRatio.drawAddHistosBackground = True
    thisRatio.addLabel("Data" if isData else "Simulation", 0.15,0.85, 1.2)
    thisRatio.CMSLeft = True
    thisRatio.moveCMSTop = 0.084
    thisRatio.labelScaleX = 0.97
    thisRatio.CMSOneLine = True
    thisRatio.CMSscale = (1.0, 1.2)
    thisRatio.thisLumi = "{0:.1f}".format(config.lumi)
    thisRatio.ratioText = "Diff. to final fit"
    thisRatio.yTitle = "Ratio"
    thisRatio.customRange = (config.fits[variable].ratioMax, config.fits[variable].ratioMin)
    thisRatio.defaultColors = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed,
                               ROOT.kGreen-2, ROOT.kCyan, ROOT.kMagenta]
    #thisRatio.ratioReference = makeReferneceRatioHisto(allPlots[0])
    canvas = thisRatio.drawPlot(legend,
                                xTitle = config.fits[variable].niceName,
                                addHisto = [(confIntervall,"e3"), fitFunc],
                                drawCMS = True)
    saveCanvasListAsPDF([canvas], "%s_%s"%(prefix, variable), config.outputpath)

def plotPostFitDistributions(config, variable, ivar, histos, legend, prefix, isData):
    """ Function for making the comparison ratios plots of high, low and all fitted distributions """
    thisRatio = RatioPlot("PostCorrectionDistributions_%s"%variable)
    thisRatio.legendSize = (0.6, 0.6, 0.8, 0.8)
    _histos = []
    for histo in histos:
        _histos.append(getProjection(histo, ivar, "postfitdit"+variable))
    thisRatio.passHistos(_histos)
    thisRatio.useDefaultColors = True
    if "eta" in variable:
        thisRatio.legendSize = (0.3,0.1,0.7,0.3)
    else:
        thisRatio.legendSize = (0.5,0.6,0.9,0.8)
    thisRatio.CMSLeft = True
    thisRatio.moveCMSTop = 0.084
    thisRatio.labelScaleX = 0.97
    thisRatio.addLabel("Data" if isData else "Simulation", 0.15,0.85, 1.2)
    thisRatio.CMSOneLine = True
    thisRatio.CMSscale = (1.0, 1.2)
    thisRatio.thisLumi = "{0:.1f}".format(config.lumi)
    thisRatio.ratioText = "#splitline{Diff. to high}{b-tag region}"
    thisRatio.defaultColors = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed,
                               ROOT.kGreen-2, ROOT.kAzure+5, ROOT.kMagenta]
    canvas = thisRatio.drawPlot(legend,
                                xTitle = config.fits[variable].niceName,
                                drawCMS = True)
    saveCanvasListAsPDF([canvas], "%s_%s"%(prefix, variable), config.outputpath)
    
    
def makeReferneceRatioHisto(histo, refVal = 1.0):
    """ Function for making a reference histo (line at refVal) """
    refHisto = histo.Clone(histo.GetName()+"reference")
    for ibin in range(refHisto.GetNbinsX()+1):
        refHisto.SetBinContent(ibin, refVal)
        refHisto.SetBinError(ibin, 0)
        #print ibin, refHisto.GetBinContent(ibin)
    return deepcopy(refHisto)

def make2DRatioProjections(config, histo3DHigh, histo3DLow, varAxis, prefix, title, isData):
    """ Function for making the 2D ratio comparison plots for each combination of axis """
    combination = list(itertools.combinations(["x","y","z"], 2))
    combinationName = []
    ROOT.gStyle.SetPalette(105) # See https://root.cern.ch/doc/master/classTColor.html#aea9a467ba2ab61a20f86d9587805e2cc
    #Get the variable combination with variable names for output
    for comb in combination:
        combName = []
        combNameNice = []
        for elem in comb:
            combName.append(varAxis[elem])
            combNameNice.append(config.fits[varAxis[elem]].niceName)
        thisOutnamePostfix = "".join(combName)
        thisProjection = "".join(comb)

        histo2DHigh = histo3DHigh.Project3D(thisProjection)
        histo2DHigh.Scale(1/histo2DHigh.Integral())
        histo2DLow = histo3DLow.Project3D(thisProjection)
        histo2DLow.Scale(1/histo2DLow.Integral())

        ratio2D = histo2DHigh.Clone(prefix+"_"+thisProjection)
        ratio2D.Divide(histo2DLow)
        ratio2D.SetMaximum(1.5)
        ratio2D.SetMinimum(0.5)
        ratio2D.SetTitle(title+" - "+" vs. ".join(combNameNice))
        ratio2D.GetXaxis().SetTitle(combNameNice[1])
        ratio2D.GetXaxis().SetTitleOffset(1.0)
        ratio2D.GetYaxis().SetTitle(combNameNice[0])
        ratio2D.GetYaxis().SetTitleOffset(1.4)
        
        saveCanvasListAsPDF([makeCanvas2D(ratio2D,
                                          addCMS=True,
                                          labelText = "Data" if isData else "Simulation",
                                          labelpos = (0.11,0.05),
                                          lumi = "{0:.1f}".format(config.lumi))],
                            "%s_%s"%(prefix, thisOutnamePostfix),
                            config.outputpath)

def convertToFunction(config, functionStr, funcName, outputFormat):
    """ Function for creating the cpp or py output from the fit function  """
    assert outputFormat in ["Cpp","py"]
    
    functionStrFinal = functionStr.replace("x",config.variables[0])
    functionStrFinal = functionStrFinal.replace("y",config.variables[1])
    functionStrFinal = functionStrFinal.replace("z",config.variables[2])

    lines = []
    outFileName = funcName+"."
    if outputFormat == "Cpp":
        lines.append("#include<algorithm>")
        lines.append("")
        lines.append("float {0}(float {1}, float {2}, float {3}, float btag)".format(funcName,
                                                                                     config.variables[0],
                                                                                     config.variables[1],
                                                                                     config.variables[2])+"{")
        lines.append("if(btag<{0} || btag>{1}) return 1;".format(config.workingPoints["loose"],
                                                                 config.workingPoints["medium"]))
        lines.append("else return {0}".format(functionStrFinal))
        lines.append("}")
        outFileName += "h"
    else:
        functionStrFinal.replace("erf","math.erf")
        lines.append("import math")
        lines.append("def {0}({1}, {2}, {3}, btag):".format(funcName,
                                                            config.variables[0],
                                                            config.variables[1],
                                                            config.variables[2]))
        lines.append("    if btag < {0} or btag > {1}:".format(config.workingPoints["loose"],
                                                               config.workingPoints["medium"]))
        lines.append("        return 1")
        lines.append("    else:")
        lines.append("        return {0}".format(functionStrFinal))
        outFileName += "py"

    logging.info("Writing file %s/%s",config.outputpath, outFileName)
    with open(config.outputpath+"/"+outFileName, "w") as f:
        f.write("\n".join(lines))
    
def getHistosFromTree(config, tree, histo3DHigh, histo3DLow, weight, procs, scaleHistos=False):
    """ 
    Function for prejection the 3D histograms from the passed tree to a histogram derived from 
    the base histos histo3DHigh, histo3DLow.
    """
    hprocHigh = {}
    hprocLow = {}
    if config.loadMacros:
        weight = weight + "*" + config.modWeight
    for proc in procs:
        if proc == "data":
            weight = "1"
        hprocHigh[proc] = histo3DHigh.Clone(histo3DHigh.GetName()+"_"+proc)
        hprocLow[proc] = histo3DLow.Clone(histo3DLow.GetName()+"_"+proc)
        project(
            tree[proc],
            hprocHigh[proc].GetName(),
            "{0}:{1}:{2}".format(config.fits[config.variables[2]].varName,
                                 config.fits[config.variables[1]].varName,
                                 config.fits[config.variables[0]].varName),
            "{5} && {0} && {1}_{2} > {3} &&  {1}_{2} <= {4}".format(
                config.baseSelection,
                config.jetBranchBase,
                config.bTagBranch,
                config.workingPoints["medium"],
                config.workingPoints["maximum"], # 1.0 in regular operation
                config.trigger),
            weight,
            "(proc: {0} - Heigh)".format(proc)
        )
        project(
            tree[proc],
            hprocLow[proc].GetName(),
            "{0}:{1}:{2}".format(config.fits[config.variables[2]].varName,
                                 config.fits[config.variables[1]].varName,
                                 config.fits[config.variables[0]].varName),
            "{5} && {0} && {1}_{2} > {3} &&  {1}_{2} <= {4}".format(
                config.baseSelection,
                config.jetBranchBase,
                config.bTagBranch,
                config.workingPoints["loose"],
                config.workingPoints["medium"],
                config.trigger),
            weight,
            "(proc: {0} - Low)".format(proc)
        )

        procScale = 1.0
        if scaleHistos:
            procScale = config.xsec[proc]*config.lumi/config.nGen[proc]
            hprocHigh[proc].Scale(procScale)
            hprocLow[proc].Scale(procScale)
        logging.debug("--- Projection info - %s ---", proc)
        logging.debug("Integral (high) = %s", hprocHigh[proc].Integral())
        logging.debug("Integral (low)  = %s", hprocLow[proc].Integral())
        logging.debug("Scale = %s", procScale)

    return deepcopy(hprocHigh), deepcopy(hprocLow)
        
def getCorrection(config, runData, reRunFiles, skipChecks):
    logging.debug("Starting to get Correction")
    logging.debug("Getting Fit functions")
    fitFuncs = {}
    fitConf = {}
    basehisto = {}
    #histoHigh = {} #Delete?
    #histoLow = {} #Delete?
    hisots3DHigh = {}
    hisots3DLow = {}
    for function in config.fitOrder:
        logging.info("Setting fit funtion with: %s", config.fits[function].fit_string_function)
        fitFuncs[function] = ROOT.TF1("func_"+function,
                                      config.fits[function].fit_string_function,
                                      config.fits[function].minBin,
                                      config.fits[function].maxBin)
        logging.debug("Object: %s", fitFuncs[function])
        allParams = config.fits[function].initData if runData else config.fits[function].initMC
        for iparam, param in enumerate(allParams):
            logging.debug("Setting %s as parameter %s for function %s", param, iparam, function)
            fitFuncs[function].SetParameter(iparam, param)
            # if "eta" in function:
            #     fitFuncs[function].SetParLimits(iparam, -2, 2)
        #Setup of each of the histograms that will be used after the fit to store the confidence band
        fitConf[function] = ROOT.TH1F("confFit_"+function,
                                     "fit with 0.95 conf.band",
                                     config.fits[function].nBins,
                                     config.fits[function].minBin,
                                     config.fits[function].maxBin)

    histo3DBase = ROOT.TH3F("3DHisto","3DHisto",
                            config.fits[config.variables[0]].nBins, config.fits[config.variables[0]].minBin, config.fits[config.variables[0]].maxBin,
                            config.fits[config.variables[1]].nBins, config.fits[config.variables[1]].minBin, config.fits[config.variables[1]].maxBin,
                            config.fits[config.variables[2]].nBins, config.fits[config.variables[2]].minBin, config.fits[config.variables[2]].maxBin)
    histo3DBase.Sumw2()

    #This is needed in oder to connect the x,y,z to the 0,1,2 (convenience in loops) and to the variables set in the "variables" option the config
    axisVars = {"x" : config.variables[0], "y" : config.variables[1], "z" : config.variables[2]}
    varAxes = {config.variables[0] : "x", config.variables[1] : "y", config.variables[2] : "z"}
    axisCode = {0 : "x", 1 : "y", 2 : "z"}
    codeAxis = {"x" : 0, "y" : 1, "z" : 2}
    
    if runData:
        outname =  config.outputpath+"/projections_data.root"
        fileTag = "_data"
    else:
        outname =  config.outputpath+"/projections_mc.root"
        fileTag = "_mc"
        
        
    if reRunFiles:
        histo3DHigh = histo3DBase.Clone(histo3DBase.GetName()+"_high")
        histo3DLow = histo3DBase.Clone(histo3DBase.GetName()+"_low")
        if config.loadMacros:
            ROOT.gROOT.LoadMacro("../classes.h")
            ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D = new TriggerSFMulitEra("{0}", "{1}")'.format(config.era, "DeepFlav"))
            ROOT.gROOT.LoadMacro("../functions.h")

        
        logging.info("Reading tree")
        rFiles = {}
        tree = {}
        if runData:
            logging.debug("File data at %s",config.files["data"])
            rFiles["data"] = ROOT.TFile.Open(config.files["data"])
            tree["data"] = rFiles["data"].Get("tree")
        else:
            for proc in config.backgrounds:
                logging.debug("File %s at %s",proc, config.files[proc])
                rFiles[proc] = ROOT.TFile.Open(config.files[proc])
                tree[proc] = rFiles[proc].Get("tree")

                
        logging.info("Starting projection")
        if runData:
            hprocHigh, hprocLow = getHistosFromTree(config, tree, histo3DHigh,
                                                    histo3DLow, "1", ["data"])
        else:
            hprocHigh, hprocLow = getHistosFromTree(config, tree, histo3DHigh, histo3DLow,
                                                    config.EventWeight, config.backgrounds, True)

        for proc in hprocHigh:
            logging.info("Adding %s (proc %s) to fit histograms %s", proc, hprocHigh[proc], histo3DHigh)
            histo3DHigh.Add(hprocHigh[proc])
            logging.info("Adding %s (proc %s) to fit histograms %s", proc, hprocLow[proc], histo3DLow)
            histo3DLow.Add(hprocLow[proc])

        logging.debug("Integral High sum = %s", histo3DHigh.Integral())
        logging.debug("Integral Low sum = %s", histo3DLow.Integral())
        
        logging.info("Saving projection hisos")
        logging.debug("Output file: %s", outname)
        outFile = ROOT.TFile(outname, "RECREATE")
        outFile.cd()
        histo3DHigh.Write()
        histo3DLow.Write()
        outFile.Close()
        copyfile(config.path,config.outputpath+"/config.cfg")
                
                    
    else:
        logging.info("Loading existing histograms")
        if os.path.exists(outname) and os.path.exists(config.outputpath+"/config.cfg"):
            if skipChecks:
                check = True
            else:
                check = (config == Config(config.outputpath+"/config.cfg", silent=True))
            if check:
                inFile = ROOT.TFile.Open(outname)
                inFile.cd()
                histo3DHigh = inFile.Get(histo3DBase.GetName()+"_high")
                histo3DLow = inFile.Get(histo3DBase.GetName()+"_low")
            else:
                logging.error("Config changes %s", config.outputpath+"/config.cfg")
                return False
        else:
            logging.error("No config or output root file in %s", config.outputpath)        
            return False

    #####################################################################################   
    logging.info("Preparing initial plots")
    plotVariables1D(config, config.variables, histo3DHigh, histo3DLow, "plot_init"+fileTag, runData)
    #####################################################################################   
    logging.info("Starting fit")
    #This variable will be used in the loop to calculate the ratio. After each fit this will be updated with
    #a rescaled version that will be used in the next fit
    nextDenominator = histo3DLow
    
    fittedFunctionStrs = {}
    fittedFunctions = {}
    functions3D = {}
    for ivar, variable in enumerate(config.fitOrder):
        if variable.endswith("2"):
            fitVar = variable.replace("2","")
            logging.warning("Replacing 2 in %s", variable)
        else:
            fitVar = variable
        fittedFunctionStrs[variable] = "0*{0}".format(varAxes[fitVar])
        logging.debug("Initialized fittedFunctionStrs[{0}] with {1}".format(variable, fittedFunctionStrs[variable]))

    fitRes = {}
    denominators = {}
    for ivar, variable in enumerate(config.fitOrder):
        if variable.endswith("2"):
            fitVar = variable.replace("2","")
            logging.warning("Replacing 2 in variable, variable")
        else:
            fitVar = variable
        logging.info("Fitting %s", fitVar)
        thisAxis = codeAxis[varAxes[fitVar]] #= Axis number needed for some functions
        ratio = getRatio(histo3DHigh, nextDenominator, thisAxis, fitVar)
        fitRes[variable] = ratio.Fit(fitFuncs[variable], config.fits[variable].fitOptions)
        ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(fitConf[variable]) #Get the confidence band
        logging.info("Fit status: %s", fitRes[variable].CovMatrixStatus())
        #Get String representation of the fit function and massage it in order to apply it later
        fittedFunctionStrs[variable] = str(fitFuncs[variable].GetExpFormula("P")).replace("x", varAxes[fitVar])
        fittedFunctionStrs[variable] = fittedFunctionStrs[variable].replace("+-","-")
        fittedFunctionStrs[variable] = "("+fittedFunctionStrs[variable]+")"
        logging.debug("Fit resutl: %s",fittedFunctionStrs[variable])
        fullFunction = ""
        for _ivar, _var in enumerate(config.fitOrder):
            if _ivar != 0:
                if fittedFunctionStrs[_var] in ["0*x", "0*y", "0*z"]:
                    comb = " + " #If no fit was done for the variable, add 0*var to the 3D fit function
                else:
                    comb = " * " #If a fit was done for the variable, mulitply it to previous fits 
            else:
                comb = ""
            fullFunction += comb + fittedFunctionStrs[_var]
        logging.debug("fullFunction: %s",fullFunction)
        #Apply current fit and previous fits to the low region 3D histogram and reference it in nexDenominator so
        #reweighted histogram will be used in the next step of the fit
        functions3D[variable] = ROOT.TF3("function3D_"+variable, fullFunction,
                                         config.fits[config.variables[0]].minBin, config.fits[config.variables[0]].maxBin,
                                         config.fits[config.variables[1]].minBin, config.fits[config.variables[1]].maxBin,
                                         config.fits[config.variables[2]].minBin, config.fits[config.variables[2]].maxBin)
        denominators[variable] = histo3DLow.Clone(histo3DLow.GetName()+"_corr"+variable)
        denominators[variable].Multiply(functions3D[variable], 1)
        nextDenominator = denominators[variable]
        
    #Final Plots
    for variable in config.fitOrder:
        if variable.endswith("2"):
            fitVar = variable.replace("2","")
            logging.warning("Replacing 2 in %s", variable)
        else:
            fitVar = variable
        #config, variable, ivar, pairs, legend, prefix
        thisPairs = [(histo3DHigh, histo3DLow)]
        thisHistos = [histo3DHigh, histo3DLow]
        thisLegend = ["No correction"]
        thisLegendSep = ["High b-tag region", "Low b-tag region"]
        for var in config.fitOrder:
            thisPairs.append((histo3DHigh, denominators[var]))
            thisHistos.append(denominators[var])
            thisLegend.append("Additionally corrected for %s"%var)
            thisLegendSep.append("Low b-tag region + %s"%var)
        plotPostFitRatios(config, variable, codeAxis[varAxes[fitVar]],
                          thisPairs, thisLegend, fitFuncs[variable],
                          fitConf[variable], "PostFitRatio_comparison"+fileTag, runData)
        plotPostFitDistributions(config, variable, codeAxis[varAxes[fitVar]],
                                 thisHistos, thisLegendSep, "PostFitDist_comparison"+fileTag, runData)

    make2DRatioProjections(config, histo3DHigh, histo3DLow, axisVars, "ratio2D_prefit"+fileTag, "High/Low region prefit", runData)
    make2DRatioProjections(config, histo3DHigh, denominators[config.fitOrder[-1]], axisVars, "ratio2D_postfit"+fileTag, "High/Low region postfit", runData)

    #Write functions as c++ and pyhton function to files
    convertToFunction(config, fullFunction, "btagCorrection"+fileTag, "Cpp")
    convertToFunction(config, fullFunction, "btagCorrection"+fileTag, "py")
    
    return True

if __name__ == "__main__":
    import argparse
    ##############################################################################################
    ##############################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "config file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--runData",
        action = "store_true",
        help = "Will run data if passed",
    )
    #This option is intended to skip the checks when rerunning from previously saved histograms and config
    #Can be used when testing and the config need to be modified. Obviously this may cause the code to crash!
    argumentparser.add_argument(
        "--skipCheck",
        action = "store_true",
        help = "Will skip the checks when using reRunFiles",
    )
    argumentparser.add_argument(
        "--reRunFiles",
        action = "store_false",
        help = "If passed the script will try to run form previously saved files",
    )
    argumentparser.add_argument(
        "--reEtaCorr",
        action = "store_true",
        help = "If passed the will execute the fits given in the reEtaOrder variable",
    )
    args = argumentparser.parse_args()
    initLogging(args.logging, 24)

    cfg = Config(args.config, reEta=args.reEtaCorr)

    if args.skipCheck:
        logging.warning("******************************************************")
        logging.warning("*** DANGERZONE!! *************************************")
        logging.warning("******************** DANGERZONE!! ********************")
        logging.warning("************************************* DANGERZONE!! ***")
        logging.warning("******************************************************")
        time.sleep(2)
        
    getCorrection(cfg, args.runData, args.reRunFiles, args.skipCheck)
    
