import logging
import os
import sys
sys.path.insert(0, os.path.abspath('..'))
from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging, project

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

ROOT.gROOT.LoadMacro("../CRcorrections/btagCorrections.h+")
ROOT.gROOT.LoadMacro("../classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("../functions.h") #inlcude functions that can be used in TTree::Draw

initLogging(20)

jetCorr = "btagCorrMC(jets_pt,jets_eta,jets_dRmin,jets_btagDeepCSV)"
jetSel = "jets_btagFlag == 1 && jets_btagDeepCSV >= 0.15522 && jets_btagDeepCSV < 0.494100"
eventWeight = "puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*qgWeight*sign(genWeight)*bTagSF * bTagSF_norm"
eventSelection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && nBDeepCSVM>=2  && passMETFilters == 1"

useFile = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"
rFile = ROOT.TFile.Open(useFile)
tree = rFile.Get("tree")

qCut = "jets_partonFlavour < 20"
gCut = "jets_partonFlavour >= 20"

hBase_pt = ROOT.TH1F("hBase_pt","hBase_pt",40,30,400)
hBase_eta = ROOT.TH1F("hBase_eta","hBase_eta",40,-2.4,2.4)


hpt_q_noCorr = hBase_pt.Clone("hpt_q_noCorr")
hpt_q = hBase_pt.Clone("hpt_q")
hpt_g_noCorr = hBase_pt.Clone("hpt_g_noCorr")
hpt_g = hBase_pt.Clone("hpt_g")

heta_q_noCorr = hBase_eta.Clone("heta_q_noCorr")
heta_q = hBase_eta.Clone("heta_q")
heta_g_noCorr = hBase_eta.Clone("heta_g_noCorr")
heta_g = hBase_eta.Clone("heta_g")

logging.info("Projection pt")
project(tree, "hpt_q_noCorr", "jets_pt", "({0} && {1} && {2})".format(eventSelection, qCut, jetSel), " ({0})".format(eventWeight))
project(tree, "hpt_g_noCorr", "jets_pt", "({0} && {1} && {2})".format(eventSelection, gCut, jetSel), " ({0})".format(eventWeight))

project(tree, "hpt_q", "jets_pt", "({0} && {1} && {2})".format(eventSelection, qCut, jetSel), " ({0} * {1})".format(eventWeight, jetCorr))
project(tree, "hpt_g", "jets_pt", "({0} && {1} && {2})".format(eventSelection, gCut, jetSel), " ({0} * {1})".format(eventWeight, jetCorr))

logging.info("Projection eta")
project(tree, "heta_q_noCorr", "jets_eta", "({0} && {1} && {2})".format(eventSelection, qCut, jetSel), " ({0})".format(eventWeight))
project(tree, "heta_g_noCorr", "jets_eta", "({0} && {1} && {2})".format(eventSelection, gCut, jetSel), " ({0})".format(eventWeight))

project(tree, "heta_q", "jets_eta", "({0} && {1} && {2})".format(eventSelection, qCut, jetSel), " ({0} * {1})".format(eventWeight, jetCorr))
project(tree, "heta_g", "jets_eta", "({0} && {1} && {2})".format(eventSelection, gCut, jetSel), " ({0} * {1})".format(eventWeight, jetCorr))
 
hpt_q_noCorr.SetLineColor(ROOT.kBlue)
hpt_q.SetLineColor(ROOT.kBlue)
hpt_g_noCorr.SetLineColor(ROOT.kGreen+2)
hpt_g.SetLineColor(ROOT.kGreen+2)
hpt_q.SetLineStyle(2)
hpt_g.SetLineStyle(2)

heta_q_noCorr.SetLineColor(ROOT.kBlue)
heta_q.SetLineColor(ROOT.kBlue)
heta_g_noCorr.SetLineColor(ROOT.kGreen+2)
heta_g.SetLineColor(ROOT.kGreen+2)
heta_q.SetLineStyle(2)
heta_g.SetLineStyle(2)

combinations = []
combinations.append([hpt_q_noCorr, hpt_q, hpt_g_noCorr, hpt_g])
combinations.append([heta_q_noCorr, heta_q,heta_g_noCorr, heta_g])

thisCanvas = []
for icomb, comb in enumerate(combinations):
    output = RatioPlot(name = "qgCRCorr_{0}".format(icomb))
    output.ratioRange =  (0.81,1.21)
    output.legendSize = (0.6, 0.5, 0.9, 0.9)
    #output.yTitleOffset = 0.99
    #output.yTitleOffsetRatio = 0.45
    #output.invertRatio = True

    output.passHistos(comb, normalize = True)
    thisCanvas.append(
        output.drawPlot(
            ["q Jets w/o Correction", "q Jets w/ Correction", "g Jets w/o Correction", "g Jets w/ Correction"],
            groupRatios = True
        )
    )



saveCanvasListAsPDF(thisCanvas, "qgCRCorr".format(comb),".")

