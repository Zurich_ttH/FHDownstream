
#include<algorithm>

/*
//Version report 28.11
float btagCorrMC(float pt, float eta, float minDeltaRb, float csv, float deepcsv){
    if(csv<0.5803 || deepcsv>0.494100) return 1;
    else return ((0.91895-0.12471*minDeltaRb+0.345628*pow(minDeltaRb,2)-0.273356*pow(minDeltaRb,3)+0.12466*pow(minDeltaRb,4)-0.0308728*pow(minDeltaRb,5)+0.00292968*pow(minDeltaRb,6)) )*((1.13021-3.99673e-05*eta-0.671792*pow(eta,2)+0.0309211*pow(eta,3)+1.85965*pow(eta,4)-0.0746467*pow(eta,5)-2.14558*pow(eta,6)+0.0740179*pow(eta,7)+1.27664*pow(eta,8)-0.0367835*pow(eta,9)-0.431042*pow(eta,10)+0.00969775*pow(eta,11)+0.0830416*pow(eta,12)-0.00128892*pow(eta,13)-0.00849295*pow(eta,14)+6.77158e-05*pow(eta,15)+0.000357544*pow(eta,16)) )*( (88.5491-0.00651562*pt)*erf((pt+946.968)*0.00159961)-85.228);
}


float btagCorrData(float pt, float eta, float minDeltaRb, float csv, float deepcsv){
    if(csv<0.5803 || deepcsv>0.494100) return 1;
    else return ((1.74723-1.16884*minDeltaRb+0.804481*pow(minDeltaRb,2)-0.330966*pow(minDeltaRb,3)+0.076699*pow(minDeltaRb,4)-0.0091964*pow(minDeltaRb,5)+0.000440055*pow(minDeltaRb,6)) )*((1.23887-0.0388026*eta-0.311978*pow(eta,2)+0.0704975*pow(eta,3)+0.697944*pow(eta,4)-0.0850864*pow(eta,5)-1.04068*pow(eta,6)+0.0561745*pow(eta,7)+0.710762*pow(eta,8)-0.0193156*pow(eta,9)-0.255264*pow(eta,10)+0.00341323*pow(eta,11)+0.0502968*pow(eta,12)-0.000284288*pow(eta,13)-0.0051623*pow(eta,14)+8.14576e-06*pow(eta,15)+0.000216219*pow(eta,16)) )*( (87.7891-0.00095643*pt)*erf((pt+196.758)*0.00889709)-86.6073);
}
*/


/*
float btagCorrMC(float pt, float eta, float minDeltaRb, float csv){
    if(csv<0.152200 || csv>0.494100) return 1;
    else return ((1.02652-0.645229*minDeltaRb+1.1428*pow(minDeltaRb,2)-0.832954*pow(minDeltaRb,3)+0.324089*pow(minDeltaRb,4)-0.0657613*pow(minDeltaRb,5)+0.00530721*pow(minDeltaRb,6)) )*((1.14287-0.0167724*eta-0.789964*pow(eta,2)+0.0676437*pow(eta,3)+2.22545*pow(eta,4)-0.127092*pow(eta,5)-2.65598*pow(eta,6)+0.0981576*pow(eta,7)+1.5851*pow(eta,8)-0.0377779*pow(eta,9)-0.522788*pow(eta,10)+0.0077821*pow(eta,11)+0.0970342*pow(eta,12)-0.000827478*pow(eta,13)-0.00950998*pow(eta,14)+3.58634e-05*pow(eta,15)+0.000383278*pow(eta,16)) )*( (87.4958-0.000805343*pt)*erf((pt+401.666)*0.00476907)-86.3024);
}

float btagCorrData(float pt, float eta, float minDeltaRb, float csv){
    if(csv<0.152200 || csv>0.494100) return 1;
    else return ((1.74723-1.16884*minDeltaRb+0.804481*pow(minDeltaRb,2)-0.330966*pow(minDeltaRb,3)+0.076699*pow(minDeltaRb,4)-0.0091964*pow(minDeltaRb,5)+0.000440055*pow(minDeltaRb,6)) )*((1.23887-0.0388026*eta-0.311978*pow(eta,2)+0.0704975*pow(eta,3)+0.697944*pow(eta,4)-0.0850864*pow(eta,5)-1.04068*pow(eta,6)+0.0561745*pow(eta,7)+0.710762*pow(eta,8)-0.0193156*pow(eta,9)-0.255264*pow(eta,10)+0.00341323*pow(eta,11)+0.0502968*pow(eta,12)-0.000284288*pow(eta,13)-0.0051623*pow(eta,14)+8.14576e-06*pow(eta,15)+0.000216219*pow(eta,16)) )*( (87.7891-0.00095643*pt)*erf((pt+196.758)*0.00889709)-86.6073);
}
*/



//Version 4.12
//With CSVL !!!
/*
float btagCorrData(float pt, float eta, float minDeltaRb, float csv, float deepcsv){
    if(csv<0.5803 || deepcsv>0.494100) return 1;
    else return ((1.51803-0.385023*minDeltaRb-0.178127*pow(minDeltaRb,2)+0.266985*pow(minDeltaRb,3)-0.113518*pow(minDeltaRb,4)+0.0212439*pow(minDeltaRb,5)-0.00149893*pow(minDeltaRb,6)) )*((1.26013-0.029722*eta-0.346969*pow(eta,2)+0.0361391*pow(eta,3)+0.431346*pow(eta,4)+0.00772564*pow(eta,5)-0.389397*pow(eta,6)-0.0292272*pow(eta,7)+0.199235*pow(eta,8)+0.0179127*pow(eta,9)-0.0632467*pow(eta,10)-0.00502566*pow(eta,11)+0.0122279*pow(eta,12)+0.000681049*pow(eta,13)-0.00130102*pow(eta,14)-3.6027e-05*pow(eta,15)+5.79286e-05*pow(eta,16)) )*( (0.396384+0.0122355*pt-5.73007e-05*pow(pt,2)+7.44356e-08*pow(pt,3)));
}
float btagCorrMC(float pt, float eta, float minDeltaRb, float csv, float deepcsv){
    if(csv<0.5803 || deepcsv>0.494100) return 1;
    else return ((0.869087+0.0101912*minDeltaRb+0.187191*pow(minDeltaRb,2)-0.166506*pow(minDeltaRb,3)+0.0843631*pow(minDeltaRb,4)-0.0233231*pow(minDeltaRb,5)+0.00239514*pow(minDeltaRb,6)) )*((1.13389-0.0139823*eta-0.683503*pow(eta,2)+0.0964712*pow(eta,3)+1.87257*pow(eta,4)-0.180826*pow(eta,5)-2.16238*pow(eta,6)+0.153577*pow(eta,7)+1.28752*pow(eta,8)-0.0676664*pow(eta,9)-0.433559*pow(eta,10)+0.0161006*pow(eta,11)+0.0830099*pow(eta,12)-0.00195947*pow(eta,13)-0.00841226*pow(eta,14)+9.54823e-05*pow(eta,15)+0.000350113*pow(eta,16)) )*( (0.531402+0.00816115*pt-2.70361e-05*pow(pt,2)+1.35317e-08*pow(pt,3)));
}
*/

//Version with err function and CSVL from 5.12

/* float btagCorrMC(float pt, float eta, float minDeltaRb, float csv, float deepcsv){ */
/*     if(csv<0.5803 || deepcsv>0.494100) return 1; */
/*     else return ((0.870582+0.004423*minDeltaRb+0.194338*pow(minDeltaRb,2)-0.171104*pow(minDeltaRb,3)+0.0859999*pow(minDeltaRb,4)-0.023611*pow(minDeltaRb,5)+0.00241477*pow(minDeltaRb,6)) )*((1.13385-0.0139358*eta-0.683553*pow(eta,2)+0.096284*pow(eta,3)+1.87285*pow(eta,4)-0.180455*pow(eta,5)-2.16267*pow(eta,6)+0.153249*pow(eta,7)+1.28763*pow(eta,8)-0.0675193*pow(eta,9)-0.433568*pow(eta,10)+0.0160659*pow(eta,11)+0.0830061*pow(eta,12)-0.00195536*pow(eta,13)-0.00841133*pow(eta,14)+9.52914e-05*pow(eta,15)+0.000350053*pow(eta,16)) )*( (69.9348-0.0144096*pt)*erf((pt+1164.39)*0.00097714)-61.8816); */
/* } */

/* float btagCorrData(float pt, float eta, float minDeltaRb, float csv, float deepcsv){ */
/*     if(csv<0.5803 || deepcsv>0.494100) return 1; */
/*     else return ((1.54653-0.496133*minDeltaRb-0.0198612*pow(minDeltaRb,2)+0.157397*pow(minDeltaRb,3)-0.0740446*pow(minDeltaRb,4)+0.0141647*pow(minDeltaRb,5)-0.00100016*pow(minDeltaRb,6)) )*((1.26302-0.0263034*eta-0.394556*pow(eta,2)+0.0116724*pow(eta,3)+0.606915*pow(eta,4)+0.0563538*pow(eta,5)-0.640216*pow(eta,6)-0.0714555*pow(eta,7)+0.375873*pow(eta,8)+0.0366518*pow(eta,9)-0.130831*pow(eta,10)-0.0094651*pow(eta,11)+0.0265445*pow(eta,12)+0.00121532*pow(eta,13)-0.00287871*pow(eta,14)-6.1691e-05*pow(eta,15)+0.000128488*pow(eta,16)) )*( (87.8417-0.00322322*pt)*erf((pt+527.534)*0.00316723)-85.9563); */
/* } */


//Version for v5 nTuples w/ DeepCSVL
/* float btagCorrData(float pt, float eta, float minDeltaRb, float deepcsv){ */
/*     if(deepcsv<0.152200 || deepcsv>0.494100) return 1; */
/*     else return ((1.75263-1.23286*minDeltaRb+0.913096*pow(minDeltaRb,2)-0.415197*pow(minDeltaRb,3)+0.110554*pow(minDeltaRb,4)-0.0158206*pow(minDeltaRb,5)+0.000936585*pow(minDeltaRb,6)) )*((1.25002-0.044203*eta-0.376406*pow(eta,2)+0.0860005*pow(eta,3)+0.902795*pow(eta,4)-0.10661*pow(eta,5)-1.32192*pow(eta,6)+0.0750335*pow(eta,7)+0.906572*pow(eta,8)-0.0287567*pow(eta,9)-0.330148*pow(eta,10)+0.00596648*pow(eta,11)+0.0662199*pow(eta,12)-0.000631275*pow(eta,13)-0.00692669*pow(eta,14)+2.67146e-05*pow(eta,15)+0.000295627*pow(eta,16)) )*( (69.9934-0.00167531*pt)*erf((pt+435.762)*0.00412807)-68.6257); */
/* } */

/* float btagCorrMC(float pt, float eta, float minDeltaRb, float deepcsv){ */
/*     if(deepcsv<0.152200 || deepcsv>0.494100) return 1; */
/*     else return ((1.03467-0.594251*minDeltaRb+0.951541*pow(minDeltaRb,2)-0.620015*pow(minDeltaRb,3)+0.219102*pow(minDeltaRb,4)-0.0419744*pow(minDeltaRb,5)+0.0032999*pow(minDeltaRb,6)) )*((1.14688+0.000368449*eta-0.515495*pow(eta,2)+0.028538*pow(eta,3)+1.38679*pow(eta,4)-0.0893096*pow(eta,5)-1.74438*pow(eta,6)+0.076598*pow(eta,7)+1.09927*pow(eta,8)-0.0295195*pow(eta,9)-0.382607*pow(eta,10)+0.00573673*pow(eta,11)+0.0748339*pow(eta,12)-0.000545574*pow(eta,13)-0.00771059*pow(eta,14)+1.99688e-05*pow(eta,15)+0.000325712*pow(eta,16)) )*( (68.6027-0.00152172*pt)*erf((pt+603.466)*0.00303209)-67.221); */
/* } */

//Version for LegacyRun2 baseline 2017
float btagCorrData(float pt, float eta, float deltaR, float btag){
  if(btag<0.0521 || btag>0.3033) return 1;
  else return ((72.9568-0.000819915*pt)*erf((pt+456.358)*0.00373867)-71.5227) * ((1.20941-0.0346415*eta-0.174228*pow(eta,2)+0.0335203*pow(eta,3)+0.531918*pow(eta,4)+0.00478598*pow(eta,5)-1.02221*pow(eta,6)-0.0251576*pow(eta,7)+0.771976*pow(eta,8)+0.0146118*pow(eta,9)-0.292859*pow(eta,10)-0.00366089*pow(eta,11)+0.0597066*pow(eta,12)+0.0004267*pow(eta,13)-0.00626131*pow(eta,14)-1.88726e-05*pow(eta,15)+0.000265562*pow(eta,16))) * ((2.28762-3.0494*deltaR+3.20492*pow(deltaR,2)-1.82931*pow(deltaR,3)+0.567788*pow(deltaR,4)-0.0900277*pow(deltaR,5)+0.00569906*pow(deltaR,6)));
}

float btagCorrMC(float pt, float eta, float deltaR, float btag){
  if(btag<0.0521 || btag>0.3033) return 1;
  else return ((68.7432-0.00120197*pt)*erf((pt+754.634)*0.00224034)-67.0785) * ((1.14082-0.0173488*eta-0.423724*pow(eta,2)+0.0596071*pow(eta,3)+1.04064*pow(eta,4)-0.100114*pow(eta,5)-1.26057*pow(eta,6)+0.0697006*pow(eta,7)+0.750044*pow(eta,8)-0.0237413*pow(eta,9)-0.24279*pow(eta,10)+0.00419268*pow(eta,11)+0.0437692*pow(eta,12)-0.000365492*pow(eta,13)-0.00413599*pow(eta,14)+1.21992e-05*pow(eta,15)+0.000159825*pow(eta,16))) * ((1.24283-1.29185*deltaR+1.75649*pow(deltaR,2)-1.013*pow(deltaR,3)+0.297171*pow(deltaR,4)-0.0449827*pow(deltaR,5)+0.00280014*pow(deltaR,6)));
}
