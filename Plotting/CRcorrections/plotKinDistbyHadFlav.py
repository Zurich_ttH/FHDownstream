import sys
import os
import logging 

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

sys.path.insert(0, os.path.abspath('..'))
from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging, project, makeCanvas2D, set_palette

initLogging(20, 24)

triggerSel = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)"
#basellineSel = "ht30>500 && jets_pt[5]>40 && nBDeepFlavM>=2 &&  Wmass4b>=30 && Wmass4b<250 && (jets_btagDeepCSV < MaxIf$(jets_btagDeepCSV, jets_btagDeepCSV < Max$(jets_btagDeepCSV)))"
basellineSel = "1"
weight = "btagDeepFlavWeight_shape*sign(genWeight) * puWeight"
lumi = "59.7"

inputFile = "~/scratch/ttH/skims/2018/LegacyRun2_AH_v3p3/loose/TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root"
output = "HadFlav_bVsl_2018_ttFH_wWeight_noSel_loose"


rFile = ROOT.TFile.Open(inputFile)
tree = rFile.Get("tree")

hpt = ROOT.TH1F("hpt", "hpt", 70, 30, 500)
heta = ROOT.TH1F("heta", "heta", 84, -2.4, 2.4)
hDeltaR = ROOT.TH1F("hDeltaR", "hDeltaR", 55, 0.5, 4.5)


hptb = hpt.Clone(hpt.GetName()+"b")
hptl = hpt.Clone(hpt.GetName()+"l")

hetab = heta.Clone(heta.GetName()+"b")
hetal = heta.Clone(heta.GetName()+"l")

hDeltaRb = hDeltaR.Clone(hDeltaR.GetName()+"b")
hDeltaRl = hDeltaR.Clone(hDeltaR.GetName()+"l")

project(tree, "hptb", "jets_pt", "abs(jets_hadronFlavour) == 5 && {} && {}".format(triggerSel, basellineSel), weight)
project(tree, "hptl", "jets_pt", "abs(jets_hadronFlavour) != 5 && {} && {}".format(triggerSel, basellineSel), weight)

project(tree, "hetab", "jets_eta", "abs(jets_hadronFlavour) == 5 && {} && {}".format(triggerSel, basellineSel), weight)
project(tree, "hetal", "jets_eta", "abs(jets_hadronFlavour) != 5 && {} && {}".format(triggerSel, basellineSel), weight)

project(tree, "hDeltaRb", "jets_dRmin", "abs(jets_hadronFlavour) == 5 && {} && {}".format(triggerSel, basellineSel), weight)
project(tree, "hDeltaRl", "jets_dRmin", "abs(jets_hadronFlavour) != 5 && {} && {}".format(triggerSel, basellineSel), weight)

hptb.Rebin(2)
hptl.Rebin(2)

hetab.Rebin(2)
hetal.Rebin(2)

hDeltaRb.Rebin(2)
hDeltaRl.Rebin(2)

hptb.SetLineWidth(2)
hptl.SetLineWidth(2)

hetab.SetLineWidth(2)
hetal.SetLineWidth(2)

hDeltaRb.SetLineWidth(2)
hDeltaRl.SetLineWidth(2)



ptRatio = RatioPlot("HadFlav_bVsl_pt")
ptRatio.passHistos([hptb, hptl], normalize=True)
ptRatio.ratioText = "#frac{Flav == 5}{Flav != 5}"
ptRatio.thisCMSLabel = "Simulation"
ptRatio.thisLumi = lumi
ptRatio.CMSscale = (1.2, 1.2)
ptRatio.moveCMSTop = 0.085
ptRatio.yTitleOffset = 0.85
ptRatio.ratioLineWidth = 2
ptRatio.CMSLeft = True
ptRatio.CMSOneLine = True
ptRatio.useDefaultColors = True
ptRatio.defaultColors = [ROOT.kBlue, ROOT.kRed]
saveCanvasListAsPDF([ ptRatio.drawPlot( ["b Jets", "non-b Jets"],
                                        xTitle = "Jet p_{T}",
                                        drawCMS = True)],
                    output+"_pt",
                    ".")

etaRatio = RatioPlot("HadFlav_bVsl_eta")
etaRatio.passHistos([hetab, hetal], normalize=True)
etaRatio.ratioText = "#frac{Flav == 5}{Flav != 5}"
etaRatio.thisCMSLabel = "Simulation"
etaRatio.thisLumi = lumi
etaRatio.CMSscale = (1.2, 1.2)
etaRatio.moveCMSTop = 0.085
etaRatio.yTitleOffset = 0.85
etaRatio.ratioLineWidth = 2
etaRatio.CMSLeft = True
etaRatio.CMSOneLine = True
etaRatio.useDefaultColors = True
etaRatio.defaultColors = [ROOT.kBlue, ROOT.kRed]
saveCanvasListAsPDF([ etaRatio.drawPlot( ["b Jets", "non-b Jets"],
                                        xTitle = "Jet #eta",
                                        drawCMS = True)],
                    output+"_eta",
                    ".")

dRRatio = RatioPlot("HadFlav_bVsl_dR")
dRRatio.passHistos([hDeltaRb, hDeltaRl], normalize=True)
dRRatio.ratioText = "#frac{Flav == 5}{Flav != 5}"
dRRatio.thisCMSLabel = "Simulation"
dRRatio.thisLumi = lumi
dRRatio.CMSscale = (1.2, 1.2)
dRRatio.moveCMSTop = 0.085
dRRatio.yTitleOffset = 0.85
dRRatio.ratioLineWidth = 2
dRRatio.CMSLeft = True
dRRatio.CMSOneLine = True
dRRatio.useDefaultColors = True
dRRatio.defaultColors = [ROOT.kBlue, ROOT.kRed]
saveCanvasListAsPDF([ dRRatio.drawPlot( ["b Jets", "non-b Jets"],
                                        xTitle = "#Delta R_{min}",
                                        drawCMS = True)],
                    output+"_dR",
                    ".")
