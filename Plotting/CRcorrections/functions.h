#include<algorithm>
#include<vector>

/*
float btagCorrection(float pt, float eta, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return (1.35799+0.0633946*pow(eta,1)-0.662034*pow(eta,2)-0.170081*pow(eta,3)+1.86503*pow(eta,4)+0.320001*pow(eta,5)-2.80448*pow(eta,6)-0.450643*pow(eta,7)+1.8724*pow(eta,8)+0.354873*pow(eta,9)-0.579587*pow(eta,10)-0.15747*pow(eta,11)+0.0487991*pow(eta,12)+0.0407596*pow(eta,13)+0.0191975*pow(eta,14)-0.00611599*pow(eta,15)-0.00607227*pow(eta,16)+0.000492948*pow(eta,17)+0.000682286*pow(eta,18)-1.64945e-05*pow(eta,19)-2.82119e-05*pow(eta,20) )*( (87.5385-0.00126445*pt)*erf((pt+82.9596)*0.0168733)-86.3329);
}

float ptCorrection(float pt, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return ( (87.5385-0.00126445*pt)*erf((pt+82.9596)*0.0168733)-86.3329);
}

float etaCorrection(float eta, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    //else return (1.35799+0.063395*pow(eta,1)-0.66203*pow(eta,2)-0.17008*pow(eta,3)+1.8650*pow(eta,4)+0.3200*pow(eta,5)-2.8045*pow(eta,6)-0.4506*pow(eta,7)+1.872*pow(eta,8)+0.3549*pow(eta,9)-0.5796*pow(eta,10)-0.1575*pow(eta,11)+0.0488*pow(eta,12)+0.04076*pow(eta,13)+0.0192*pow(eta,14)-0.006116*pow(eta,15)-0.00607*pow(eta,16)+0.000493*pow(eta,17)+0.0006823*pow(eta,18)-1.649e-05*pow(eta,19)-2.822e-05*pow(eta,20) ); //rounded
    //else return (1.35799+0.0633945*pow(eta,1)-0.662033*pow(eta,2)-0.170079*pow(eta,3)+1.86502*pow(eta,4)+0.319992*pow(eta,5)-2.80446*pow(eta,6)-0.450628*pow(eta,7)+1.87237*pow(eta,8)+0.35486*pow(eta,9)-0.579565*pow(eta,10)-0.157463*pow(eta,11)+0.0487891*pow(eta,12)+0.0407574*pow(eta,13)+0.0192003*pow(eta,14)-0.00611558*pow(eta,15)-0.00607273*pow(eta,16)+0.000492908*pow(eta,17)+0.000682327*pow(eta,18)-1.64929e-05*pow(eta,19)-1.50375e-09*pow(eta,20) ); //done with correlations
    else return (1.35799+0.0633946*pow(eta,1)-0.662034*pow(eta,2)-0.170081*pow(eta,3)+1.86503*pow(eta,4)+0.320001*pow(eta,5)-2.80448*pow(eta,6)-0.450643*pow(eta,7)+1.8724*pow(eta,8)+0.354873*pow(eta,9)-0.579587*pow(eta,10)-0.15747*pow(eta,11)+0.0487991*pow(eta,12)+0.0407596*pow(eta,13)+0.0191975*pow(eta,14)-0.00611599*pow(eta,15)-0.00607227*pow(eta,16)+0.000492948*pow(eta,17)+0.000682286*pow(eta,18)-1.64945e-05*pow(eta,19)-2.82119e-05*pow(eta,20) );
}

float btagCorrectionUp(float pt, float eta, float csv){ //pt from correlations, no change for eta
    if(csv<0.542600 || csv>0.848400) return 1;
    else return (1.35799+0.0633946*pow(eta,1)-0.662034*pow(eta,2)-0.170081*pow(eta,3)+1.86503*pow(eta,4)+0.320001*pow(eta,5)-2.80448*pow(eta,6)-0.450643*pow(eta,7)+1.8724*pow(eta,8)+0.354873*pow(eta,9)-0.579587*pow(eta,10)-0.15747*pow(eta,11)+0.0487991*pow(eta,12)+0.0407596*pow(eta,13)+0.0191975*pow(eta,14)-0.00611599*pow(eta,15)-0.00607227*pow(eta,16)+0.000492948*pow(eta,17)+0.000682286*pow(eta,18)-1.64945e-05*pow(eta,19)-2.82119e-05*pow(eta,20) )*( (87.5386-0.00124861*pt)*erf((pt+83.9012)*0.0170016)-86.3328);
}

float btagCorrectionDown(float pt, float eta, float csv){ //pt from correlations, no change for eta
    if(csv<0.542600 || csv>0.848400) return 1;
    else return (1.35799+0.0633946*pow(eta,1)-0.662034*pow(eta,2)-0.170081*pow(eta,3)+1.86503*pow(eta,4)+0.320001*pow(eta,5)-2.80448*pow(eta,6)-0.450643*pow(eta,7)+1.8724*pow(eta,8)+0.354873*pow(eta,9)-0.579587*pow(eta,10)-0.15747*pow(eta,11)+0.0487991*pow(eta,12)+0.0407596*pow(eta,13)+0.0191975*pow(eta,14)-0.00611599*pow(eta,15)-0.00607227*pow(eta,16)+0.000492948*pow(eta,17)+0.000682286*pow(eta,18)-1.64945e-05*pow(eta,19)-2.82119e-05*pow(eta,20) )*( (87.5385-0.00128139*pt)*erf((pt+82.0892)*0.0167622)-86.3329);
}


float ptCorrectionUp(float pt, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return ( (87.5386-0.00124861*pt)*erf((pt+83.9012)*0.0170016)-86.3328); //done with correlations
    //else return ( (87.5398-0.00128149*pt)*erf((pt+84.1958)*0.0170566)-86.3344);
}

float etaCorrectionUp(float eta, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return (1.36084+0.0634002*pow(eta,1)-0.658768*pow(eta,2)-0.170134*pow(eta,3)+1.84424*pow(eta,4)+0.320151*pow(eta,5)-2.76599*pow(eta,6)-0.450841*pow(eta,7)+1.85552*pow(eta,8)+0.355017*pow(eta,9)-0.566501*pow(eta,10)-0.157532*pow(eta,11)+0.0486765*pow(eta,12)+0.0407759*pow(eta,13)+0.0198382*pow(eta,14)-0.00611855*pow(eta,15)-0.00599648*pow(eta,16)+0.000493167*pow(eta,17)+0.000686901*pow(eta,18)-1.65024e-05*pow(eta,19)+4.49882e-07*pow(eta,20) );  //done with correlations
    //else return (1.36092+0.0763610*pow(eta,1)-0.708734*pow(eta,2)-0.263524*pow(eta,3)+2.07398*pow(eta,4)+0.560302*pow(eta,5)-3.21087*pow(eta,6)-0.752263*pow(eta,7)+2.2947*pow(eta,8)+0.567570*pow(eta,9)-0.838867*pow(eta,10)-0.24751*pow(eta,11)+0.1474161*pow(eta,12)+0.0641155*pow(eta,13)+0.0426794*pow(eta,14)-0.00975017*pow(eta,15)-0.00947765*pow(eta,16)+0.000804178*pow(eta,17)+0.000957203*pow(eta,18)-2.77677e-05*pow(eta,19)-3.76813e-05*pow(eta,20) );
}


float ptCorrectionDown(float pt, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return ( (87.5385-0.00128139*pt)*erf((pt+82.0892)*0.0167622)-86.3329); //done with correlations
    //else return ( (87.5372-0.00124741*pt)*erf((pt+81.7234)*0.0166900)-86.3314);
}

float etaCorrectionDown(float eta, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return (1.35514+0.0633898*pow(eta,1)-0.665611*pow(eta,2)-0.170041*pow(eta,3)+1.88789*pow(eta,4)+0.319904*pow(eta,5)-2.84816*pow(eta,6)-0.450537*pow(eta,7)+1.8957*pow(eta,8)+0.354812*pow(eta,9)-0.59712*pow(eta,10)-0.15745*pow(eta,11)+0.0507591*pow(eta,12)+0.0407558*pow(eta,13)+0.0180955*pow(eta,14)-0.0061156*pow(eta,15)-0.00607923*pow(eta,16)+0.00049293*pow(eta,17)+0.000672068*pow(eta,18)-1.64943e-05*pow(eta,19)-2.58533e-07*pow(eta,20) ); //done with correlations
    //else return (1.35506+0.0504282*pow(eta,1)-0.615334*pow(eta,2)-0.076638*pow(eta,3)+1.65607*pow(eta,4)+0.079700*pow(eta,5)-2.39809*pow(eta,6)-0.149023*pow(eta,7)+1.4501*pow(eta,8)+0.142176*pow(eta,9)-0.320307*pow(eta,10)-0.06743*pow(eta,11)-0.0498179*pow(eta,12)+0.0174037*pow(eta,13)-0.0042844*pow(eta,14)-0.00248181*pow(eta,15)-0.00266689*pow(eta,16)+0.000181718*pow(eta,17)+0.000407369*pow(eta,18)-5.22130e-06*pow(eta,19)-1.87425e-05*pow(eta,20) );
}

float btagCorrectionSilvio(float pt, float eta, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return (1.31948+0.0132484*pow(eta,1)-0.415898*pow(eta,2)+0.133008*pow(eta,3)+0.873669*pow(eta,4)-0.417787*pow(eta,5)-1.01018*pow(eta,6)+0.4665*pow(eta,7)+0.142038*pow(eta,8)-0.297122*pow(eta,9)+0.40692*pow(eta,10)+0.122241*pow(eta,11)-0.299977*pow(eta,12)-0.0327943*pow(eta,13)+0.0964358*pow(eta,14)+0.00546691*pow(eta,15)-0.01649*pow(eta,16)-0.000508896*pow(eta,17)+0.00146419*pow(eta,18)+2.00858e-05*pow(eta,19)-5.32364e-05*pow(eta,20) )*( (87.5484-0.0014503*pt)*erf((pt+86.9715)*0.0163653)-86.3228);
}

float ptCorrectionSilvio(float pt, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return ( (87.5484-0.0014503*pt)*erf((pt+86.9715)*0.0163653)-86.3228);
}

float etaCorrectionSilvio(float eta, float csv){
    if(csv<0.542600 || csv>0.848400) return 1;
    else return (1.31948+0.0132484*pow(eta,1)-0.415898*pow(eta,2)+0.133008*pow(eta,3)+0.873669*pow(eta,4)-0.417787*pow(eta,5)-1.01018*pow(eta,6)+0.4665*pow(eta,7)+0.142038*pow(eta,8)-0.297122*pow(eta,9)+0.40692*pow(eta,10)+0.122241*pow(eta,11)-0.299977*pow(eta,12)-0.0327943*pow(eta,13)+0.0964358*pow(eta,14)+0.00546691*pow(eta,15)-0.01649*pow(eta,16)-0.000508896*pow(eta,17)+0.00146419*pow(eta,18)+2.00858e-05*pow(eta,19)-5.32364e-05*pow(eta,20) );
}
*/

int getDeepCSVRank(float maxVal, float max2Val, float csv){
  cout << "-----------------------------" << endl;
  cout << "MaxVal = " << maxVal << endl;
  cout << "Max2Val = " << max2Val << endl;
  cout << "csv = " << csv << endl;



  cout << "-----------------------------" << endl;
  return 1;
}
