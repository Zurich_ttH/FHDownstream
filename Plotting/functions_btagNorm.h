#include<algorithm>

float get_bTagNormalization(int SampleID, int nJets, float ht){
  string sampleName = "NotSet";
  switch(SampleID){
  case 1  : sampleName = "ttbb_SL"; break;
  case 2  : sampleName = "ttbb_inc_SL"; break;
  case 3  : sampleName = "ttbb_DL"; break;
  case 4  : sampleName = "ttbb_inc_DL"; break;
  case 5  : sampleName = "ttbb_FH"; break;
  case 6  : sampleName = "ttbb_inc_FH"; break;
  case 7  : sampleName = "ttcc_SL"; break;
  case 8  : sampleName = "ttcc_DL"; break;
  case 9  : sampleName = "ttcc_FH"; break;
  case 10 : sampleName = "ttlf_SL"; break;
  case 11 : sampleName = "ttlf_DL"; break;
  case 12 : sampleName = "ttlf_FH"; break;
  case 13 : sampleName = "ttHbb"; break;
  case 14 : sampleName = "ttHnonbb"; break;
  case 15 : sampleName = "QCD200"; break;
  case 16 : sampleName = "QCD300"; break;
  case 17 : sampleName = "QCD500"; break;
  case 18 : sampleName = "QCD700"; break;
  case 19 : sampleName = "QCD1000"; break;
  case 20 : sampleName = "QCD1500"; break;
  case 21 : sampleName = "QCD2000"; break;
  case 22 : sampleName = "st_s_lep"; break;
  case 23 : sampleName = "st_s_had"; break;
  case 24 : sampleName = "st_t"; break;
  case 25 : sampleName = "stbar_t"; break;
  case 26 : sampleName = "st_tW"; break;
  case 27 : sampleName = "stbar_tW"; break;
  case 28 : sampleName = "tHq"; break;
  case 29 : sampleName = "tHW"; break;
  case 30 : sampleName = "ttZ"; break;
  case 31 : sampleName = "ttW"; break;
    
  }
  return internalbTagNormalization->getbTagNormalization(sampleName, nJets, ht); 
}
