import sys
import ROOT

reference = "hNoTrigger"
baseline = "hSingleMuTrigger"

calcCorrFor = [
    "h6J1T", 
    "h6J2T", 
    "h4J3T", 
    "hJetTag", 
    "hAddTrigger", 
    "hAll"
]


def calcCorrelation(inputFile, noSel=""):
    rFile = ROOT.TFile.Open(inputFile)

    
    nTotal = float(rFile.Get(reference+noSel).GetBinContent(1))
    nBaseline = rFile.Get(baseline+noSel).GetBinContent(1)

    for corr in calcCorrFor:
        nSignal = rFile.Get(corr+noSel).GetBinContent(1)
        print corr+"AndSingleMu"+noSel
        nSignalOrBaseline = rFile.Get(corr+"AndSingleMu"+noSel).GetBinContent(1)

        try: 
            thisCorrelation = ((nBaseline/nTotal) * (nSignal/nTotal)) / ((nSignalOrBaseline/nTotal))
        except ZeroDivisionError:
            thisCorrelation = 0.0

        print corr, ":",  thisCorrelation

if __name__ == "__main__":
    
    inFile = sys.argv[1]

    calcCorrelation(inFile)
