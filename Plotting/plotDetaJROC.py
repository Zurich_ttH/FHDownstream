from copy import deepcopy

import ROOT

from classes.roc import ROC
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################


def main():
    basePath = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p3/CMSSW_9_4_9/src/TTH/FHDownstream/Plotting/HTReweighting/QCD_estimate_plots_v5/v5/"
    files = {
        "7J3T" : basePath+"check_7j_3b_SR_Detaj5_alphaCR_btagCorrDataMC_topPt_signal_pulls.root",
        "7J4T" : basePath+"check_7j_4b_SR_Detaj5_alphaCR_btagCorrDataMC_topPt_signal_pulls.root",
        "8J3T" : basePath+"check_8j_3b_SR_Detaj6_alphaCR_btagCorrDataMC_topPt_signal_pulls.root",
        "8J4T" : basePath+"check_8j_4b_SR_Detaj6_alphaCR_btagCorrDataMC_topPt_signal_pulls.root",
        "9J3T" : basePath+"check_9j_3b_SR_Detaj7_alphaCR_btagCorrDataMC_topPt_signal_pulls.root",
        "9J4T" : basePath+"check_9j_4b_SR_Detaj7_alphaCR_btagCorrDataMC_topPt_signal_pulls.root",
    }

    rFiles = {}
    for fileId in files:
        rFiles[fileId] = ROOT.TFile.Open(files[fileId])

    niceNames = {
        "Detaj[4]" : "#Delta#etaJ_{5}",
        "Detaj[5]" : "#Delta#etaJ_{6}",
        "Detaj[6]" : "#Delta#etaJ_{7}",
        "Detaj[7]" : "#Delta#etaJ_{8}",
        "7J3T" : Helper.CatLabelsSRExt["j7t3"],
        "8J3T" : Helper.CatLabelsSRExt["j8t3"],
        "9J3T" : Helper.CatLabelsSRExt["j9t3"],
        "7J4T" : Helper.CatLabelsSRExt["j7t4"],
        "8J4T" : Helper.CatLabelsSRExt["j8t4"],
        "9J4T" : Helper.CatLabelsSRExt["j9t4"]
    }
        
    for icat, cat in enumerate(files):
        allCanvases = []
        hSignal = rFiles[cat].Get("hSRttH_hbb_ttH_hbb")
        httbar = rFiles[cat].Get("httbarSR")
        hddQCD = rFiles[cat].Get("ddQCD")

        hSignal.UseCurrentStyle() 
        hddQCD.UseCurrentStyle() 
        httbar.UseCurrentStyle() 

        hSignal.GetXaxis().SetTitleOffset(hSignal.GetXaxis().GetTitleOffset()*1.2)
        
        httbar.SetLineColor(ROOT.kRed)
        hddQCD.SetLineColor(Helper.colors["qcd"])

        hSignal.SetFillStyle(0)
        hSignal.GetYaxis().SetTitleSize(hSignal.GetYaxis().GetTitleSize()*1.2)
        hSignal.GetXaxis().SetTitleSize(hSignal.GetXaxis().GetTitleSize()*1.2)
        httbar.SetFillStyle(0)
        hddQCD.SetFillStyle(0)
        hSignal.SetLineStyle(1)
        httbar.SetLineStyle(1)
        hddQCD.SetLineStyle(1)
        
        if "7J" in cat:
            varName = "#Delta#eta_{jets}"
        if "8J" in cat:
            varName = "#Delta#eta_{jets}"
        if "9J" in cat:
            varName = "#Delta#eta_{jets}"
        
        thisROC = ROC(cat,  rocType="sEff-bEff")
        thisROC.passHistos(hSignal, httbar)
        thisROC.passHistos(hSignal, hddQCD)
        thisROC.moveCMS = -0.04
        #thisROC.passHistos(hBackground, hBackground_QCD)
        #thisROC.color[2] = ROOT.kOrange
        print niceNames[cat], cat, niceNames
        print [hSignal, httbar, hddQCD]
        thisROC.addLabel(niceNames[cat], 0.16, 0.87, "Left", scaleText = 2)
        thisROC.addLabel("Variable: "+varName, 0.13, 1-(0.068/2), "Left", scaleText = 2)
        thisROC.thisCMSLabel = "Supplementary"
        allCanvases.append(
            thisROC.drawROC(
                ["t#bar{t}H(bb) vs t#bar{t}+jets", "t#bar{t}H(bb) vs Multijet"],
                invert = True,
                drawCMS = True
            )
        )
        saveCanvasListAsPDF(allCanvases, "ROCs_DetaJ_ddQCD_"+cat,".")
        
        
        
        saveCanvasListAsPDF(
            [makeCanvasOfHistos(
                "comp"+str(cat),
                varName,
                [hSignal, httbar, hddQCD],
                legendText = ["t#bar{t}H(bb)","t#bar{t}+jets","Multijet"],
                normalized = True,
                drawAs = "histo",
                addCMS = True,
                addLabel = niceNames[cat],
                labelpos = (0.13, 1-(0.068/2)),
                # legendColumns = 3,
                # legendSize = (0.15, 0.86, 0.7, 0.9),
                legendSize = (0.16, 0.67, 0.4, 0.9),
                topScale = 1.25,
                supplementary = True
            )],
            "Dist_DetaJ_ddQCD_"+cat,"."
        )


if __name__ == "__main__":
    main()
