#include<algorithm>

float get_TriggerSF3D(float ht, float pt6, int nB){
  // initialize in ROOT with TriggerSF* internalTriggerSF3D = new TriggerSF3D();
  float SF = internalTriggerSF3D->getTriggerWeight(ht,pt6,nB);
  //cout << SF << endl;
  return SF;
}


float QCDnJetWeight(int nJets){
  if (nJets == 6){
    return 0.907373905182;//+- 0.00864550945866
  }
  else if (nJets == 7){
    return 1.0223556757;// +- 0.0111939198432
  }
  else if (nJets == 8){
    return 1.11500406265;// +- 0.0183448039935
  }
  else if (nJets == 9){
    return 1.27151966095;// +- 0.0348287156877
  }
  else if (nJets == 10){
    return 1.49826455116;// +- 0.0717107143588
  }
  else if (nJets >= 11){
    return 1.51720297337;// +- 0.0659095385476
  }
  else {
    return 1.0;
  }
}

float QCDnJetWeight_2017(int nJets){
  if (nJets  == 6 ){ return  1.01393163204 ;} //+- 0.00628860183432
  else if (nJets  == 7 ){ return  0.986823678017 ;} //+- 0.00501960465553
  else if (nJets  == 8 ){ return  0.993375241756 ;} //+- 0.0066555104677
  else if (nJets  == 9 ){ return  0.999115526676 ;} //+- 0.0104611332215
  else if (nJets  == 10 ){ return  1.04123091698 ;} //+- 0.0184941664729
  else if (nJets  == 11 ){ return  1.09126508236 ;} //+- 0.0347214849071
  else if (nJets  >= 12 ){ return  1.14628922939 ;} //+- 0.0575292475352
  else { return 1.0; }
}

float QCDnJetWeight_2018(int nJets){
  if (nJets  == 6 ){ return  0.952937304974 ;} //+- 0.00572742484949
  else if (nJets  == 7 ){ return  0.968972086906 ;} //+- 0.00480268172234
  else if (nJets  == 8 ){ return  1.02770090103 ;} //+- 0.00673455311497
  else if (nJets  == 9 ){ return  1.06833314896 ;} //+- 0.0109936773358
  else if (nJets  == 10 ){ return  1.18727862835 ;} //+- 0.0208512340104
  else if (nJets  == 11 ){ return  1.28438389301 ;} //+- 0.0404773924238
  else if (nJets  >= 12 ){ return  1.37647867203 ;} //+- 0.069883297483
  else { return 1.0; }
}



float HTWeight(float HT){
  if(HT<500.0){ return 1.2043460723; }
  else if(HT<600.0){ return 1.050638257; }
  else if(HT<700.0){ return 1.02494829113; }
  else if(HT<800.0){ return 1.01627005305; }
  else if(HT<900.0){ return 0.957354992911; }
  else if(HT<1000.0){ return 1.02904066291; }
  else if(HT<1100.0){ return 0.931446072616; }
  else if(HT<1200.0){ return 0.883981248122; }
  else if(HT<1300.0){ return 0.881863792477; }
  else if(HT<1400.0){ return 0.85933937345; }
  else { return 0.852647035525; }
}

float HTWeight_2016(float HT){
  if(HT<500.0){ return 1.12185078006; }
  else if(HT<600.0){ return 1.14501273517; }
  else if(HT<700.0){ return 1.06831572748; }
  else if(HT<800.0){ return 1.04715526796; }
  else if(HT<900.0){ return 0.993880722653; }
  else if(HT<1000.0){ return 0.999292718736; }
  else if(HT<1100.0){ return 0.887371655783; }
  else if(HT<1200.0){ return 0.851134433897; }
  else if(HT<1300.0){ return 0.928009453113; }
  else if(HT<1400.0){ return 0.761015802942; }
  else { return 0.765814565707; }
}


float HTWeight_2017(float HT){
  if(HT<500.0){ return 1.30006849802; }
  else if(HT<600.0){ return 1.12310415974; }
  else if(HT<700.0){ return 1.06756151477; }
  else if(HT<800.0){ return 1.04097451186; }
  else if(HT<900.0){ return 0.951372291928; }
  else if(HT<1000.0){ return 1.02489403503; }
  else if(HT<1100.0){ return 0.903107377862; }
  else if(HT<1200.0){ return 0.850280119739; }
  else if(HT<1300.0){ return 0.820038638186; }
  else if(HT<1400.0){ return 0.810100964182; }
  else { return 0.732883430478; }
}


float HTWeight_2018(float HT){
  if(HT<500.0){ return 1.17018367544; }
  else if(HT<600.0){ return 1.09704859734; }
  else if(HT<700.0){ return 1.04896598252; }
  else if(HT<800.0){ return 1.01889523169; }
  else if(HT<900.0){ return 1.00400276884; }
  else if(HT<1000.0){ return 0.944148072132; }
  else if(HT<1100.0){ return 0.958280513206; }
  else if(HT<1200.0){ return 0.902218433282; }
  else if(HT<1300.0){ return 0.824798942119; }
  else if(HT<1400.0){ return 0.874693109106; }
  else { return 0.822004986379; }
}



