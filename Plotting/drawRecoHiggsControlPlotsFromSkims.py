#from __future__ import print_function

import copy
import time
import logging
import os
import numpy as np
import itertools

import ROOT

from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging, checkNcreateFolder, moveOverUnderFlow, makeCanvas2D
from classes.ratios import RatioPlot
from classes.PLotDrawers import getFitResultPlot, makeCanvasOfHistosWithLines

from TTH.Plotting.Datacards.MiscClasses import TriggerSFCalculator

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def main(inputFile, outputName, outputFolder, year, RecoBranch, GenBranch):
    logging.info("Getting file: %s", inputFile)
    rFile = ROOT.TFile.Open(inputFile)
    tree = rFile.Get("tree")

    h1DPt = ROOT.TH1F("h1DPt", "h1DPt", 60, 0, 600)
    h1DPt.GetZaxis().SetTitle("Events / 10 GeV")
    h1DPt.SetTitle("")

    h1DPtGen = h1DPt.Clone("h1DPtGen")
    h1DPtGen.GetXaxis().SetTitle("p_{T}^{H} (Gen) [GeV]")
    
    h2DPt = ROOT.TH2F("h2DPt","h2DPt", 90, 0, 900, 90, 0, 900)
    h2DPt.GetXaxis().SetTitle("p_{T}^{H} (Reco) [GeV]")
    h2DPt.GetXaxis().SetTitleOffset(h2DPt.GetXaxis().GetTitleOffset()*1.1)
    h2DPt.GetYaxis().SetTitle("p_{T}^{H} (Gen) [GeV]")
    h2DPt.GetZaxis().SetTitle("Events / 10 GeV")
    h2DPt.GetZaxis().SetTitleOffset(h2DPt.GetZaxis().GetTitleOffset()*1.5)
    h2DPt.SetTitle("")
    h2DBaseline = h2DPt.Clone("h2DBaseline")
    h2DSR7J = h2DPt.Clone("h2DSR7J")
    h2DSR8J = h2DPt.Clone("h2DSR8J")
    h2DSR9J = h2DPt.Clone("h2DSR9J")

    addSel7J = "numJets == 7 && nBDeepFlavM >= 4 && Wmass4b>60 && Wmass4b<100 "
    addSel8J = "numJets == 8 && nBDeepFlavM >= 4 && Wmass4b>60 && Wmass4b<100 "
    addSel9J = "numJets >= 9 && nBDeepFlavM >= 4 && Wmass4b>70 && Wmass4b<92 "

    if year == "2017":
        baselineSel = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && numJets >=6 && ht30>500 && jets_pt[5]>40 && nBDeepFlavM>=2 && passMETFilters == 1 && is_fh == 1 && Wmass4b >= 30 && Wmass4b < 250"

        lumi = 41.5 
    elif year == "2018":
        baselineSel = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && numJets >=6 && ht30>500 && jets_pt[5]>40 && nBDeepFlavM>=2 && passMETFilters == 1 && is_fh == 1 && Wmass4b >= 30 && Wmass4b < 250"
        lumi = 59.7
    elif year == "2016":
        baselineSel = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFJet450 == 1) && numJets >=6 && ht30>500 && jets_pt[5]>40 && nBDeepFlavM>=2 && passMETFilters == 1 && is_fh == 1 && Wmass4b >= 30 && Wmass4b < 250"
        lumi = 35.9
    else:
        raise NotImplementedError
    
    tree.Project("h2DBaseline", "%s:%s"%(RecoBranch, GenBranch), baselineSel)
    tree.Project("h2DSR7J", "%s:%s"%(RecoBranch, GenBranch), baselineSel+" && "+addSel7J)
    tree.Project("h2DSR8J", "%s:%s"%(RecoBranch, GenBranch), baselineSel+" && "+addSel8J)
    tree.Project("h2DSR9J", "%s:%s"%(RecoBranch, GenBranch), baselineSel+" && "+addSel9J)
    
    
    canvases = []
    canvases.append(
        makeCanvas2D(
            h2DBaseline,
            width=600, height=540,
            addCMS = True,
            lumi="{:2.1f}".format(lumi),
            CMSLabel = "Simulation",
            leftMargin=0.12,
            corrLabel=False, xstart=0.5,
            labelText = "Baseline Region",
            labelpos = (0.37, 0.968),
        )
    )
    canvases.append(
        makeCanvas2D(
            h2DSR7J,
            width=600, height=540,
            addCMS = True,
            lumi="{:2.1f}".format(lumi),
            CMSLabel = "Simulation",
            leftMargin=0.12,
            corrLabel=False, xstart=0.5,
            labelText = "FH (7 jets, #geq 4 btags)",
            labelpos = (0.33, 0.97),
        )
    )
    canvases.append(
        makeCanvas2D(
            h2DSR8J,
            width=600, height=540,
            addCMS = True,
            lumi="{:2.1f}".format(lumi),
            CMSLabel = "Simulation",
            leftMargin=0.12,
            corrLabel=False, xstart=0.5,
            labelText = "FH (8 jets, #geq 4 btags)",
            labelpos = (0.33, 0.97),
        )
    )
    canvases.append(
        makeCanvas2D(
            h2DSR9J,
            width=600, height=540,
            addCMS = True,
            lumi="{:2.1f}".format(lumi),
            CMSLabel = "Simulation",
            leftMargin=0.12,
            corrLabel=False, xstart=0.5,
            labelText = "FH (#geq 9 jets, #geq 4 btags)",
            labelpos = (0.33, 0.97),
        )
    )
    
    saveCanvasListAsPDF(canvases, outputName+"_"+year, outputFolder)

    canvases = []

    GenHistos = [[],[],[]]
    GenHistosLegend = [[],[],[]]
    
    STXS_bin = [(0,60), (60,120), (120,200), (200,300), (300,99999999)]
    categories = [addSel7J, addSel8J, addSel9J]
    for ibin in range(len(STXS_bin)):
        for icat in range(len(categories)):
            this1DPtGen = h1DPtGen.Clone("h1DPtGen_%s_%s"%(ibin, icat))
            tree.Project("h1DPtGen_%s_%s"%(ibin, icat),
                         GenBranch,
                         baselineSel+" && "+categories[icat]+" && %s >= %s && %s < %s"%(RecoBranch,
                                                                                        STXS_bin[ibin][0],
                                                                                        RecoBranch,
                                                                                        STXS_bin[ibin][1])
            )
            moveOverUnderFlow(this1DPtGen)
            GenHistos[icat].append(copy.deepcopy(this1DPtGen))
            GenHistosLegend[icat].append("STXS Bin %s"%ibin)


            if True:
                this1DCountGenAndReco = h1DPtGen.Clone("this1DCountGenAndReco_%s_%s"%(ibin, icat))
                tree.Project("this1DCountGenAndReco_%s_%s"%(ibin, icat),
                             "1",
                             baselineSel+" && "+categories[icat]+" && %s >= %s && %s < %s && %s >= %s && %s < %s "%(RecoBranch,
                                                                                                                    STXS_bin[ibin][0],
                                                                                                                    RecoBranch,
                                                                                                                    STXS_bin[ibin][1],
                                                                                                                    GenBranch,
                                                                                                                    STXS_bin[ibin][0],
                                                                                                                    GenBranch,
                                                                                                                    STXS_bin[ibin][1])
                             )
                this1DCountGen = h1DPtGen.Clone("this1DCountGen_%s_%s"%(ibin, icat))
                tree.Project("this1DCountGen_%s_%s"%(ibin, icat),
                             "1",
                             baselineSel+" && "+categories[icat]+" && %s >= %s && %s < %s"%(GenBranch,
                                                                                            STXS_bin[ibin][0],
                                                                                            GenBranch,
                                                                                            STXS_bin[ibin][1])
                             )
                countCorrect = this1DCountGenAndReco.Integral()
                countTruthAll = this1DCountGen.Integral()
                logging.info("Cat %s ::: Fraction correct assignment Bin %s : %s", icat, ibin, countCorrect/countTruthAll)
            
        
            
    catLabels = ["FH (7 jets, #geq 4 btags)", "FH (8 jets, #geq 4 btags)", "FH (#geq 9 jets, #geq 4 btags)"]
    for icat in range(len(categories)):
        canvases.append(
            makeCanvasOfHistosWithLines(
                "GenPt_%s"%icat,
                this1DPtGen.GetXaxis().GetTitle(),
                GenHistos[icat],
                addLabel = catLabels[icat],
                labelpos = (0.33, 0.97),
                verticalLines = [60, 120, 200, 300],
                lineColors = ROOT.kBlack,
                legendText = GenHistosLegend[icat],
                legendSize = (0.6,0.4,0.9,0.9),
                colorList = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue, ROOT.kGreen+2, ROOT.kCyan]
            )
        )

    saveCanvasListAsPDF(canvases, outputName+"_GenPtForRecoBins_"+"_"+year, outputFolder)
            
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Plotting script for Higgs reco mass control plots form skims',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--outputName",
        action = "store",
        help = "Name of the output ROOT file",
        type=str,
        required = True
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Name of the input ROOT file",
        type=str,
        required = True
    )
    argumentparser.add_argument(
        "--year",
        action = "store",
        help = "Years to be run",
        default = "2017"
    )
    argumentparser.add_argument(
        "--outputFolder",
        action = "store",
        help = "Name of the output folder",
        type=str,
        default = "out_HiggsMass"
    )
    argumentparser.add_argument(
        "--RecoBranch",
        action = "store",
        help = "Branch of the reco higgs pt",
        type=str,
        default = "RecoHiggsPt"
    )
    argumentparser.add_argument(
        "--GenBranch",
        action = "store",
        help = "Branch of the gen higgs pt",
        type=str,
        default = "HTXS_Higgs_pt"
    )
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging)

    main(args.inputFile, args.outputName, args.outputFolder, args.year, args.RecoBranch, args.GenBranch)
