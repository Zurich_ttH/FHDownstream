#!/bin/bash

if [ -z "$ERA" ]
then
    echo "SETTING \$ERA to 2017"
    ERA="2017"
else
    echo "USING ${ERA} as ERA"
fi

echo ${ERA}

if [[ $# -le 1 ]]
then
    echo "Please pass at least two arguments"
    echo ""
    echo "Usage"
    echo "-----"
    echo "Arg 1: Base folder containing the datacards --> If fit is in subfolder pass as 3rd arg"
    echo "Arg 2: Output folder"
    exit 1
else
    DATACARDFOLDER=$1
    OUTPUTFOLDER=$2
    echo "Will use $DATACARDFOLDER as base input directory "
    echo "Will use $OUTPUTFOLDER as output directory "
fi

if [[ $# -ge 3 ]]
then
    FITDIR=$3
    echo "Will use $FITDIR as subdirectory containing fits"
fi


 if [[ -d $OUTPUTFOLDER ]]; then
     echo ""
     echo "$OUTPUTFOLDER already exists. Will overwrite files"
 else
     mkdir $OUTPUTFOLDER
 fi


COLUMNS=$(tput cols)

for FILE in $DATACARDFOLDER/$FITDIR/fitDiagnostics_*.root
do
    echo ""
    seq -s= $COLUMNS|tr -d '[:digit:]'
    echo ""
    
    DATACARDNAME=${FILE##*/}
    DATACARDNAME="${DATACARDNAME//fitDiagnostics_/}"
    DATACARDNAME="${DATACARDNAME//.root/.txt}"

    echo "python DNNPrefitPlots.py $FILE $DATACARDFOLDER/$DATACARDNAME $OUTPUTFOLDER"
    python DNNPrefitPlots.py $FILE $DATACARDFOLDER/$DATACARDNAME $OUTPUTFOLDER

    echo ""
    seq -s= $COLUMNS|tr -d '[:digit:]'
    echo ""

done
