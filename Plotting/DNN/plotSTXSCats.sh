#!/bin/bash
RUNCAT7=true
RUNCAT8=true
RUNCAT9=false

TAG="Testingv1"
CARDFOLDER="/work/koschwei/slc7/ttH/LegacyRun2/combine/Datacards/STXS/v6/STXSTesting/2017/Testingv1/cards"
YEAR="2017"

mkdir -p out_DNNSTXS/${TAG}

if [ "${RUNCAT7}" = true ]; then
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j7_sig1_r_TTH_PTH_0_60.root ${CARDFOLDER}/fh_STXS_j7_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_0_j7_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j7_sig1_r_TTH_PTH_60_120.root ${CARDFOLDER}/fh_STXS_j7_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_1_j7_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j7_sig1_r_TTH_PTH_120_200.root ${CARDFOLDER}/fh_STXS_j7_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_2_j7_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j7_sig1_r_TTH_PTH_200_300.root ${CARDFOLDER}/fh_STXS_j7_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_3_j7_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j7_sig1_r_TTH_PTH_GT300.root ${CARDFOLDER}/fh_STXS_j7_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_4_j7_t4_DNN_Node0
fi

if [ "${RUNCAT8}" = true ]; then
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j8_sig1_r_TTH_PTH_0_60.root ${CARDFOLDER}/fh_STXS_j8_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_0_j8_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j8_sig1_r_TTH_PTH_60_120.root ${CARDFOLDER}/fh_STXS_j8_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_1_j8_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j8_sig1_r_TTH_PTH_120_200.root ${CARDFOLDER}/fh_STXS_j8_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_2_j8_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j8_sig1_r_TTH_PTH_200_300.root ${CARDFOLDER}/fh_STXS_j8_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_3_j8_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j8_sig1_r_TTH_PTH_GT300.root ${CARDFOLDER}/fh_STXS_j8_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_4_j8_t4_DNN_Node0
fi

if [ "${RUNCAT9}" = true ]; then
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j9_sig1_r_TTH_PTH_0_60.root ${CARDFOLDER}/fh_STXS_j9_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_0_j9_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j9_sig1_r_TTH_PTH_60_120.root ${CARDFOLDER}/fh_STXS_j9_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_1_j9_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j9_sig1_r_TTH_PTH_120_200.root ${CARDFOLDER}/fh_STXS_j9_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_2_j9_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j9_sig1_r_TTH_PTH_200_300.root ${CARDFOLDER}/fh_STXS_j9_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_3_j9_t4_DNN_Node0
    python DNNPrefitPlots_STXS.py ${CARDFOLDER}/fitDiagnostics_${TAG}_asimov_j9_sig1_r_TTH_PTH_GT300.root ${CARDFOLDER}/fh_STXS_j9_DNN_Node0.txt ${YEAR} out_DNNSTXS/${TAG} fh_STXS_4_j9_t4_DNN_Node0
fi
