#!/bin/bash
MAKECARDS=true
RUNFIT=true
RUNCAT7=true
RUNCAT8=true
RUNCAT9=true

TAG="Testingv1"


if [ "${MAKECARDS}" = true ]; then

    if [ "${RUNCAT7}" = true ]; then
	echo "Making 7 Jet cards"
	combineCards.py \
	    fh_STXS_0_j7_t4_DNN_Node0=fh_STXS_0_j7_t4_DNN_Node0.txt \
	    fh_STXS_1_j7_t4_DNN_Node0=fh_STXS_1_j7_t4_DNN_Node0.txt \
	    fh_STXS_2_j7_t4_DNN_Node0=fh_STXS_2_j7_t4_DNN_Node0.txt \
	    fh_STXS_3_j7_t4_DNN_Node0=fh_STXS_3_j7_t4_DNN_Node0.txt \
	    fh_STXS_4_j7_t4_DNN_Node0=fh_STXS_4_j7_t4_DNN_Node0.txt \
	    > fh_STXS_j7_DNN_Node0.txt

	text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel --PO verbose  \
			  --PO 'map=.*/(TTH_PTH_0_60):r_TTH_PTH_0_60[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_60_120):r_TTH_PTH_60_120[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_120_200):r_TTH_PTH_120_200[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_200_300):r_TTH_PTH_200_300[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_GT300):r_TTH_PTH_GT300[1,-10,10]' \
			  fh_STXS_j7_DNN_Node0.txt &> text2WS_7J_log.txt

    fi

    if [ "${RUNCAT8}" = true ]; then
	echo "Making 8 Jet cards"
	combineCards.py \
	    fh_STXS_0_j8_t4_DNN_Node0=fh_STXS_0_j8_t4_DNN_Node0.txt \
	    fh_STXS_1_j8_t4_DNN_Node0=fh_STXS_1_j8_t4_DNN_Node0.txt \
	    fh_STXS_2_j8_t4_DNN_Node0=fh_STXS_2_j8_t4_DNN_Node0.txt \
	    fh_STXS_3_j8_t4_DNN_Node0=fh_STXS_3_j8_t4_DNN_Node0.txt \
	    fh_STXS_4_j8_t4_DNN_Node0=fh_STXS_4_j8_t4_DNN_Node0.txt \
	    > fh_STXS_j8_DNN_Node0.txt
	
	text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel --PO verbose  \
			  --PO 'map=.*/(TTH_PTH_0_60):r_TTH_PTH_0_60[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_60_120):r_TTH_PTH_60_120[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_120_200):r_TTH_PTH_120_200[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_200_300):r_TTH_PTH_200_300[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_GT300):r_TTH_PTH_GT300[1,-10,10]' \
			  fh_STXS_j8_DNN_Node0.txt  &> text2WS_8J_log.txt
    fi

    if [ "${RUNCAT9}" = true ]; then
	echo "Making 9 Jet cards"
	combineCards.py \
	    fh_STXS_0_j9_t4_DNN_Node0=fh_STXS_0_j9_t4_DNN_Node0.txt \
	    fh_STXS_1_j9_t4_DNN_Node0=fh_STXS_1_j9_t4_DNN_Node0.txt \
	    fh_STXS_2_j9_t4_DNN_Node0=fh_STXS_2_j9_t4_DNN_Node0.txt \
	    fh_STXS_3_j9_t4_DNN_Node0=fh_STXS_3_j9_t4_DNN_Node0.txt \
	    fh_STXS_4_j9_t4_DNN_Node0=fh_STXS_4_j9_t4_DNN_Node0.txt \
	    > fh_STXS_j9_DNN_Node0.txt
	
	
        text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel --PO verbose  \
			  --PO 'map=.*/(TTH_PTH_0_60):r_TTH_PTH_0_60[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_60_120):r_TTH_PTH_60_120[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_120_200):r_TTH_PTH_120_200[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_200_300):r_TTH_PTH_200_300[1,-10,10]' \
			  --PO 'map=.*/(TTH_PTH_GT300):r_TTH_PTH_GT300[1,-10,10]' \
			  fh_STXS_j9_DNN_Node0.txt &> text2WS_9J_log.txt
    fi    
fi


UNCOPTIONS="--saveNormalizations --saveShapes --saveOverallShapes --saveWithUncertainties"
#UNCOPTIONS="--saveNormalizations --saveShapes --saveOverallShapes "
#UNCOPTIONS=""


if [ "${RUNFIT}" = true ]; then
    if [ "${RUNCAT7}" = true ]; then
	echo "Fitting 7J category"

	echo "  Fitting BIN 0"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_0_60 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_0_60 fh_STXS_j7_DNN_Node0.root &> /dev/null
#         	${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_0_60 fh_STXS_j7_DNN_Node0.root &> out_${TAG}_asimov_j7_sig1_r_TTH_PTH_0_60.txt

	echo "  Fitting BIN 1"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_60_120 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_60_120 fh_STXS_j7_DNN_Node0.root &> /dev/null
#		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_60_120 fh_STXS_j7_DNN_Node0.root &> out_${TAG}_asimov_j7_sig1_r_TTH_PTH_60_120.txt

	echo "  Fitting BIN 2"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_120_200 \
	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_120_200 fh_STXS_j7_DNN_Node0.root &> /dev/null 
#	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_120_200 fh_STXS_j7_DNN_Node0.root &> out_${TAG}_asimov_j7_sig1_r_TTH_PTH_120_200.txt

	echo "  Fitting BIN 3"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_200_300 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_200_300 fh_STXS_j7_DNN_Node0.root &> /dev/null
#	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_200_300 fh_STXS_j7_DNN_Node0.root &> out_${TAG}_asimov_j7_sig1_r_TTH_PTH_200_300.txt

	echo "  Fitting BIN 4"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_GT300 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_GT300 fh_STXS_j7_DNN_Node0.root &> /dev/null
#		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j7_sig1_r_TTH_PTH_GT300 fh_STXS_j7_DNN_Node0.root &> out_${TAG}_asimov_j7_sig1_r_TTH_PTH_GT300.txt
    fi



    if [ "${RUNCAT8}" = true ]; then
	echo "Fitting 8J category"

	echo "  Fitting BIN 0"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_0_60 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_0_60 fh_STXS_j8_DNN_Node0.root &> /dev/null
#         	${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_0_60 fh_STXS_j8_DNN_Node0.root &> out_${TAG}_asimov_j8_sig1_r_TTH_PTH_0_60.txt

	echo "  Fitting BIN 1"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_60_120 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_60_120 fh_STXS_j8_DNN_Node0.root &> /dev/null
#		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_60_120 fh_STXS_j8_DNN_Node0.root &> out_${TAG}_asimov_j8_sig1_r_TTH_PTH_60_120.txt

	echo "  Fitting BIN 2"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_120_200 \
	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_120_200 fh_STXS_j8_DNN_Node0.root &> /dev/null 
#	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_120_200 fh_STXS_j8_DNN_Node0.root &> out_${TAG}_asimov_j8_sig1_r_TTH_PTH_120_200.txt

	echo "  Fitting BIN 3"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_200_300 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_200_300 fh_STXS_j8_DNN_Node0.root &> /dev/null
#	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_200_300 fh_STXS_j8_DNN_Node0.root &> out_${TAG}_asimov_j8_sig1_r_TTH_PTH_200_300.txt

	echo "  Fitting BIN 4"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_GT300 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_GT300 fh_STXS_j8_DNN_Node0.root &> /dev/null
#		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j8_sig1_r_TTH_PTH_GT300 fh_STXS_j8_DNN_Node0.root &> out_${TAG}_asimov_j8_sig1_r_TTH_PTH_GT300.txt
    fi

        if [ "${RUNCAT9}" = true ]; then
	echo "Fitting 9J category"

	echo "  Fitting BIN 0"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_0_60 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_0_60 fh_STXS_j9_DNN_Node0.root &> /dev/null
#         	${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_0_60 fh_STXS_j9_DNN_Node0.root &> out_${TAG}_asimov_j9_sig1_r_TTH_PTH_0_60.txt

	echo "  Fitting BIN 1"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_60_120 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_60_120 fh_STXS_j9_DNN_Node0.root &> /dev/null
#		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_60_120 fh_STXS_j9_DNN_Node0.root &> out_${TAG}_asimov_j9_sig1_r_TTH_PTH_60_120.txt

	echo "  Fitting BIN 2"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_120_200 \
	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_120_200 fh_STXS_j9_DNN_Node0.root &> /dev/null 
#	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_120_200 fh_STXS_j9_DNN_Node0.root &> out_${TAG}_asimov_j9_sig1_r_TTH_PTH_120_200.txt

	echo "  Fitting BIN 3"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_200_300 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_200_300 fh_STXS_j9_DNN_Node0.root &> /dev/null
#	        ${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_200_300 fh_STXS_j9_DNN_Node0.root &> out_${TAG}_asimov_j9_sig1_r_TTH_PTH_200_300.txt

	echo "  Fitting BIN 4"
	combine -M FitDiagnostics -m 125 --cminDefaultMinimizerTolerance 1e-2 --cminDefaultMinimizerStrategy 0 \
		--setParameterRanges r_TTH_PTH_0_60=-10,10:r_TTH_PTH_60_120=-10,10:r_TTH_PTH_120_200=-10,10:r_TTH_PTH_200_300=-10,10:r_TTH_PTH_GT300=-10,10:r=-10,10 \
		--setParameters r_TTH_PTH_0_60=1,r_TTH_PTH_60_120=1,r_TTH_PTH_120_200=1,r_TTH_PTH_200_300=1,r_TTH_PTH_GT300=1,r=1 \
		--redefineSignalPOIs r_TTH_PTH_GT300 \
		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_GT300 fh_STXS_j9_DNN_Node0.root &> /dev/null
#		${UNCOPTIONS} -t -1 -n _${TAG}_asimov_j9_sig1_r_TTH_PTH_GT300 fh_STXS_j9_DNN_Node0.root &> out_${TAG}_asimov_j9_sig1_r_TTH_PTH_GT300.txt
    fi
fi
