from copy import deepcopy
import logging

import ROOT
from QCD_estimate_v2 import Config
from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, moveOverUnderFlow, project, makeStackPlotCanvas




def getAllHistos(config, baseHisto, tree, variable, dependancyCuts, selection, weight, category, sample, prefix):
    hName_base = baseHisto.GetName()
    aBunchOfColor = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen+2, ROOT.kCyan, ROOT.kMagenta, ROOT.kSpring, ROOT.kAzure, ROOT.kViolet+5, ROOT.kYellow-6, ROOT.kPink, ROOT.kBlue-5]
    dependentHistos = []
    dependentHistos_stack = []
    histoColors = []
    legendText = []
    for icut, cut in enumerate(dependancyCuts):
        thisHisto = baseHisto.Clone(hName_base.replace("baseHisto","depCut_"+str(icut)+"_"+prefix))
        project(tree, thisHisto.GetName(), config.varSettings[variable]["treeExpr"], "{0} && {1}".format(selection, cut), weight, "{0} - {1} - {2}".format(category, sample, prefix))
        logging.debug("Integral of projected histogram: %s", thisHisto.Integral())
        dependentHistos.append(thisHisto)
        dependentHistos_stack.append(thisHisto.Clone(thisHisto.GetName()+"_stack"))
        histoColors.append(aBunchOfColor[icut])
        niceCut = cut.replace("<=", "#leq")
        niceCut = niceCut.replace("&&", "and")
        legendText.append(niceCut)

    return dependentHistos, dependentHistos_stack, histoColors, legendText
        



def plotStackDependancy(config, fileObj, sample, category, variable, dependency, dependency_values, region, inclusiveDependencys, outDir):
    tree = fileObj["tree"][sample]
    #Generate dependency cuts:
    dependancyCuts = []
    if not inclusiveDependencys:
        for iValue, value in enumerate(dependency_values):
            if iValue == 0:
                thisCut = "({0} <= {1})".format(dependency, value)
            elif iValue == len(dependency_values):
                thisCut = "({0} > {1})".format(dependency, value)
            else:
                thisCut = "({0} <= {1} && {0} > {2})".format(dependency, value, dependency_values[iValue-1])
            logging.debug("Adding dependency cut: %s", thisCut)
            dependancyCuts.append(thisCut)
    else:
        for iValue, value in enumerate(dependency_values):
            thisCut = "({0} <= {1})".format(dependency, value)
            logging.debug("Adding dependency cut: %s", thisCut)
            dependancyCuts.append(thisCut)
        hisCut = "({0} > {1})".format(dependency, value)
        logging.debug("Adding dependency cut: %s", thisCut)
        dependancyCuts.append(thisCut)

    thisCut = "({0} > {1})".format(dependency, dependency_values[-1])
    logging.debug("Adding dependency cut: %s", thisCut)
    dependancyCuts.append(thisCut)

    regionCut = []
    if region == "VR":
        SRCut = config.regionCuts[category]["VRSelection"]
        CRCut = config.regionCuts[category]["CR2Selection"]
    else:
        SRCut = config.regionCuts[category]["SRSelection"]
        CRCut = config.regionCuts[category]["CRSelection"]

    baseline = config.BaselineSel

    selectionSR= "{0} && {1} && {2}".format(baseline, config.catCut[cat], SRCut)
    selectionCR= "{0} && {1} && {2}".format(baseline, config.catCut[cat], CRCut)

    if True:
        selectionSR = selectionSR.replace("((Wmass4b>30 && Wmass4b<60) || (Wmass4b>100 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")
        selectionCR = selectionCR.replace("((Wmass4b>30 && Wmass4b<60) || (Wmass4b>100 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")
        selectionSR = selectionSR.replace("((Wmass4b>30 && Wmass4b<70) || (Wmass4b>92 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")
        selectionCR = selectionCR.replace("((Wmass4b>30 && Wmass4b<70) || (Wmass4b>92 && Wmass4b<250))", "(Wmass4b<100 || Wmass4b>80)")


    
    
    eventWeightMC = "{0}*{1}".format( config.weight, config.bCorr["MC"])
    eventWeightData = "{0}*{1}".format("1", config.bCorr["Data"])
    
    if sample == "data":
        thisWeight = eventWeightData
        thisWeightSR = "1"
    else:
        thisWeight = eventWeightMC
        thisWeightSR = config.weight

    hName_base = "{0}_{1}_{2}_baseHisto".format(sample, category, variable)
    
    nBins = config.varSettings[variable]["nBins"]
    if var == "ht":
        nBins *= 2
    binLow = config.varSettings[variable]["minBin"]
    binHigh = config.varSettings[variable]["maxBin"]
    
    baseHisto = ROOT.TH1F(hName_base, hName_base, nBins, binLow, binHigh)
    baseHisto.GetXaxis().SetTitle(config.varSettings[variable]["xtitle"])
    baseHisto.GetYaxis().SetTitle(config.varSettings[variable]["ytitle"])
    baseHisto.Sumw2()
    logging.debug("Created main histogram with name %s, %s|%s|%s", hName_base, nBins, binLow, binHigh)

    dependentHistos_CR, dependentHistos_stack_CR, histoColors, legendText = getAllHistos(config, baseHisto, tree, variable, dependancyCuts, selectionCR, thisWeight, category, sample, "CR")
    
    dependentHistos_SR, dependentHistos_stack_SR, histoColors, legendText = getAllHistos(config, baseHisto, tree, variable, dependancyCuts, selectionSR, thisWeightSR, category, sample, "SR")
    
    
        
    stackName = hName_base.replace("baseHisto","stack")
    dependentHistos_SR[1].SetLineWidth(2)
    dependentHistos_CR[1].SetLineWidth(2)


    for iCut in range(len(dependancyCuts)):
        cut = dependancyCuts[iCut]
        niceCut = cut.replace("<=", "#leq")
        niceCut = niceCut.replace("&&", "and")
        canvas = makeCanvasOfHistos(
            hName_base+"_comp_cut_"+str(iCut),
            config.varSettings[variable]["xtitle"],
            [dependentHistos_SR[iCut], dependentHistos_CR[iCut]],
            legendText = ["SR/VR", "CR/CR2"],
            addIntLegend = True,
            legendSize=(0.6,0.6,0.9,0.8),
            normalized = True,
            lineWidth = 2,
            colorList = [ROOT.kRed, ROOT.kBlue],
            addLabel = niceCut,
            labelpos = (0.3, 0.87)
        )
        saveCanvasListAsPDF([canvas], "Dependency_comp_perCut_{0}_{1}_{2}_{3}_{4}".format(dependency, cat, sample, variable, iCut), "{0}/{1}/{2}/{3}/".format(outDir, sample, variable, cat))
    
    thisCanvas = makeStackPlotCanvas(stackName+"SR", dependentHistos_stack_SR, histoColors, legendText, (0.4,0.7,0.9,0.9))
    saveCanvasListAsPDF([thisCanvas], "Dependency_SR_{0}_{1}_{2}_{3}".format(dependency, cat, sample, variable), outDir)
    thisCanvas = makeStackPlotCanvas(stackName+"CR", dependentHistos_stack_CR, histoColors, legendText, (0.4,0.7,0.9,0.9))
    saveCanvasListAsPDF([thisCanvas], "Dependency_CR_{0}_{1}_{2}_{3}".format(dependency, cat, sample, variable), outDir)
    anotherCanvas = makeCanvasOfHistos(hName_base+"_comp_SR", variable, dependentHistos_SR, legendText = legendText , legendSize=(0.4,0.7,0.9,0.9), normalized = True, colorList = histoColors)
    saveCanvasListAsPDF([anotherCanvas], "Dependency_comp_SR_{0}_{1}_{2}_{3}".format(dependency, cat, sample, variable), outDir)
    anotherCanvas = makeCanvasOfHistos(hName_base+"_comp_CR", variable, dependentHistos_CR, legendText = legendText , legendSize=(0.4,0.7,0.9,0.9), normalized = True, colorList = histoColors)
    saveCanvasListAsPDF([anotherCanvas], "Dependency_comp_CR_{0}_{1}_{2}_{3}".format(dependency, cat, sample, variable), outDir)
    
    

    

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "config file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--vars",
        action = "store",
        help = "variables to be run",
        type = str,
        nargs="+",
        default = ["ht", "mjjmin"]
    )
    argumentparser.add_argument(
        "--depVar",
        action = "store",
        help = "variables to be run",
        type = str,
        default = "Wmass4b"
    )
    argumentparser.add_argument(
        "--depVals",
        action = "store",
        help = "variables to be run",
        type = str,
        nargs="+",
        default = ["30", "45", "60", "100","120","150","200","250"]
        #default = ["60", "100", "110", "125", "140", "155", "190", "230"]
    )
    argumentparser.add_argument(
        "--inclDep",
        action = "store_true",
        help = "If set, the dependency cuts will be used as inclusive cuts",
    )
    argumentparser.add_argument(
        "--stackDependancy",
        action = "store_true",
        help = "If set, Plots will be drawn with pulls instead of ratio",
    )
    argumentparser.add_argument(
        "--samples",
        action = "store",
        help = "categories to be run",
        type = str,
        nargs="+",
        default = ["ttH_hbb", "TTbar_fh", "data"]
    )
    argumentparser.add_argument(
        "--region",
        action = "store",
        help = "categories to be run",
        type = str,
        default = "VR"
    )
    argumentparser.add_argument(
        "--cats",
        action = "store",
        help = "categories to be run",
        type = str,
        nargs="+",
        default = ["7j4t", "8j4t", "9j4t"]
    )
    argumentparser.add_argument(
        "--outDir",
        action = "store",
        help = "categories to be run",
        type = str,
        default = "out_scanDependency"
    )


    
    
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    thisConfig = Config(args.config)

    logging.info("Getting trees:")
    fileObj = { "rFile" : {},
                "tree" : {}
    }
    for process in thisConfig.allProcs:
        logging.debug("Processing process: %s", process)
        fileObj["rFile"][process] = ROOT.TFile.Open(thisConfig.files[process])
        fileObj["tree"][process] = fileObj["rFile"][process].Get("tree")
        logging.debug("Got tree at %s", fileObj["tree"][process])

    ROOT.gROOT.LoadMacro("classes.h")
    ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
    ROOT.gROOT.LoadMacro("functions.h")
    if thisConfig.loadbCorrMacro:
        ROOT.gROOT.LoadMacro(thisConfig.bCorrMarco)
        
    for sample in args.samples:
        if sample not in thisConfig.allProcs:
            raise RuntimeError("Sample %s not in thisConfig"%(sample))

    for var in args.vars:
        if var not in thisConfig.variables:
            raise RuntimeError("Variable %s not defined in thisConfig"%var)
    
    if args.stackDependancy:
        for sample in args.samples:
            for cat in args.cats:
                for var in args.vars:
                    plotStackDependancy(thisConfig, fileObj, sample, cat, var, args.depVar, args.depVals, args.region, args.inclDep, args.outDir)
