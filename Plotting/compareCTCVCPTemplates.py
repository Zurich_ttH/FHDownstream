import logging
import ROOT
import sys
from glob import glob
from copy import deepcopy

ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetErrorX(0) #turns of horizontal error bars (but also for bkg)
ROOT.gROOT.SetBatch(1)


from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, moveOverUnderFlow, project, makeStackPlotCanvas, getColorStyleList
from classes.PLotDrawers import makeCanvasOfHistosBase

def compareCTCVCPTemplates(basePath, variable, folder):
    
    filesPerDataset = {}
    
    for _file in glob(basePath+"/*.root"):
        fileName = _file.split("/")[-1]
        sample = "_".join(fileName.split("_")[1:]).replace(".root","")
        if sample not in filesPerDataset.keys():
            filesPerDataset[sample] = []
        filesPerDataset[sample].append(_file)

    logging.info("Found %s samples -- %s", len(filesPerDataset.keys()), filesPerDataset.keys())
    
    for sample in filesPerDataset:
        logging.info("Processing %s", sample)

        #First get the nominal template names:
        rFile = ROOT.TFile.Open(filesPerDataset[sample][0])
        allKeys = [x.GetName() for x in rFile.GetListOfKeys()]
        nomKeys = filter(lambda x: len(x.split("__")) == 3 and x.endswith(variable) , allKeys)
        logging.info("Nominal keys: %s",nomKeys)
        procs = list(set([x.split("__")[0] for x in nomKeys]))
        logging.info("Processes in file: %s", procs)
        cats = list(set([x.split("__")[1] for x in nomKeys]))
        logging.info("Categories in file: %s", cats)
        rFile.Close()

        histos = {}
        labels = []
        for proc in procs:
            histos[proc] = {}
            for cat in cats:
                histos[proc][cat] = []

        for rF in filesPerDataset[sample]:
            rFile = ROOT.TFile.Open(rF)
            labels.append(rF.split("/")[-1].split("_")[0])
            for proc in procs:
                for cat in cats:
                    histoName = "{}__{}__{}".format(proc, cat, variable)
                    logging.debug("Histo name : %s", histoName)
                    histos[proc][cat].append(deepcopy(rFile.Get(histoName)))


        #Now we do the plotting
        canvases = []
        for proc in procs:
            for cat in cats:
                canvases.append(
                    makeCanvasOfHistosBase(
                        "{}__{}".format(proc, cat),
                        variable,
                        histos[proc][cat],
                        600, 540,
                        None,
                        False,
                        2,
                        range(len(filesPerDataset[sample])),
                        "HISTOE",
                        None,
                        False,
                        "{} {}".format(proc,cat),
                        None,
                        1,
                        False,
                        False,
                        "41.5",
                        None,
                        False
                    )
                )

        saveCanvasListAsPDF(canvases, "CTCVCP_comp_"+sample, folder)
    


if __name__ == "__main__":
    initLogging(10)
    
    basePath = sys.argv[1]
    variable = "DNN_Node0"
    folder = "out_CTCVCPComp"
    compareCTCVCPTemplates(basePath, variable, folder)
