
"""
Script which merges the different histograms
"""

import Helper
import ROOT

colors = [ROOT.kBlue, ROOT.kGreen+2, ROOT.kRed]

#First, open the file where the histograms are contained
file_A=ROOT.TFile.Open("TriggerPlots_LegacyRun2/2018/DeepFlav/2Dhistis_eff_v1_2018_DeepFlav_tightCut_2018RunX_closure.root")
file_B=ROOT.TFile.Open("TriggerPlots_LegacyRun2/2018/DeepFlav/2Dhistis_eff_v1_2018_DeepFlav_tightCut_2018RunX_closure.root")
file_C=ROOT.TFile.Open("TriggerPlots_LegacyRun2/2018/DeepFlav/2Dhistis_eff_v1_2018_DeepFlav_tightCut_2018RunY_closure.root")
file_D=ROOT.TFile.Open("TriggerPlots_LegacyRun2/2018/DeepFlav/2Dhistis_eff_v1_2018_DeepFlav_tightCut_2018RunZ_closure.root")
outprefix = "TriggerPlots_LegacyRun2/2018/DeepFlav/"

# file_A=ROOT.TFile.Open("2Dhistis_eff_CSV_tightCutRunA_All_CSV_closure.root")
# file_B=ROOT.TFile.Open("2Dhistis_eff_CSV_tightCutRunB_All_CSV_closure.root")
# file_C=ROOT.TFile.Open("2Dhistis_eff_CSV_tightCutRunC_All_CSV_closure.root")
# file_D=ROOT.TFile.Open("2Dhistis_eff_CSV_tightCutRunD_All_CSV_closure.root")


# file_A=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunA_All_CSV.root")
# file_B=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunB_All_CSV.root")
# file_C=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunC_All_CSV.root")
# file_D=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunD_All_CSV.root")


# file_A=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunA_All_CSV_closure.root")
# file_B=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunB_All_CSV_closure.root")
# file_C=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunC_All_CSV_closure.root")
# file_D=ROOT.TFile.Open("2Dhistis_eff_Flav_tightCutRunD_All_CSV_closure.root")


# file_A=ROOT.TFile.Open("2Dhistis_eff_X_tightCutRunX_All_CSV.root")
# file_B=ROOT.TFile.Open("2Dhistis_eff_Y_tightCutRunY_All_CSV.root")
# file_C=ROOT.TFile.Open("2Dhistis_eff_Z_tightCutRunZ_All_CSV.root")

# file_A=ROOT.TFile.Open("2Dhistis_eff_X_tightCutRunX_All_CSV_closure.root")
# file_B=ROOT.TFile.Open("2Dhistis_eff_Y_tightCutRunY_All_CSV_closure.root")
# file_C=ROOT.TFile.Open("2Dhistis_eff_Z_tightCutRunZ_All_CSV_closure.root")






files=[file_B , file_C  , file_D]
variables=["ht" , "pt" , "nb"]

for var in variables:
    lumi = 59.7
    box = Helper.create_paves(lumi, "DataWiP", CMSposX=0.155, CMSposY=0.84,
                          prelimPosX=0.15, prelimPosY=0.79,
                          lumiPosX=0.977, lumiPosY=0.91, alignRight=False,
                          CMSsize=0.075*.75, prelimSize=0.057*.75, lumiSize=0.060*.75)

    leg = ROOT.TLegend(0.2,0.15,0.85,0.35)
    leg.SetFillStyle(1001)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.05)
    c1 =ROOT.TCanvas("c1","sub data", 5, 30, 640, 580);
    c1.SetTopMargin(0.08*.75)
    c1.SetRightMargin(0.04)
    c1.SetLeftMargin(0.11)
    c1.SetBottomMargin(0.12)
    c1.SetTicks()

    line = ROOT.TF1("line","1",0.0,9000.0)
    line.SetLineColor(12) #Grey
    line.SetLineWidth(1)
    line.SetLineStyle(2) #Dashe

    for ifile, f in enumerate(files):
        if "RunX" in f.GetName():
            legName="Data: 315252 #leq Run < 315974"
        if "RunY" in f.GetName():
            legName="Data: 315974 #leq Run < 317509"
        if "RunZ" in f.GetName():
            legName="Data: Run #geq 317509"

        hist=f.Get("hData_eff_"+var)
        hist.SetLineColor(colors[ifile])
        hist.SetMarkerColor(colors[ifile])
        leg.AddEntry(hist , legName , "PLE")
        hist.Draw("same")
    #Plot MC only once
    MC=file_A.Get("hMC_eff_"+var)
    MC.SetLineColor(ROOT.kBlack)
    MC.SetMarkerColor(ROOT.kBlack)
    leg.AddEntry(MC, "MC" , "PLE")
    MC.Draw("same")

    leg.Draw("same")
    box["lumi"].Draw()
    box["CMS"].Draw()
    box["label"].Draw()
    line.Draw("same")
    c1.Print(outprefix+"RunComp_"+var+".pdf")
