"""
Script for making data/mc plots from skims (Update 2018 data). Run e.g. with

python plotDataMC.py --config ../data/plotDataMC/testPlots_bis.cfg --output test/datamc --loglevel 10
"""
import sys
import os
import logging
#from QCD_estimate_v2 import Config
import QCD_estimate_v2
from collections import namedtuple
from copy import deepcopy
import math



import ROOT



sys.path.insert(0, os.path.abspath('../utils/'))
from ConfigReader import ConfigReaderBase
from SampleConfig import SampleConfig

from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging, checkNcreateFolder, moveOverUnderFlow

class PlottingConfig(ConfigReaderBase):
    """
    Interface for reading the plot config, defining plot details, samples and general settings

    Class is child of ConfigReaderBase. As base class ConfigReaderBase only takes care of reading the .cfg file
    (using the python configparser module) and implements convenience functions when reading comma separated lists
    from the config (ConfigReaderBase.readMulitlineOption) or multiline options (ConfigReaderBase.readMulitlineOption).
    Also implements a function reading form the config with default values in case the option is not 
    present (ConfigReaderBase.setOptionWithDefault).

    Args:
      pathtoConfig (str) : Path to plot config (setting variables, bins, etc.)
      sampleConfigPath (str) : Path to sample config (setting root files, nGen, xsec etc.)

    Attributes;
      lumi (float) : Luminosity the MC samples get scaled to in fb-1
      sampleConf (str) : Path to sample configuration file
      baseSelection (str) : Base event selection --> Use strings compatible with TTree::Draw o.s.
      baseWeight (str) : Base event weight --> Use strings compatible with TTree::Draw o.s.
      scaleQCD (bool) : If true the total QCD samples will be scaled so the integral matches the data yield (Not needed for now!)
      allSamples (SampleConfig) : SampleConfig object (see description there for more information)
      plots (dict) : Dict containing the necessary information for the plot in a namedtuple object
    """
    def __init__(self, pathtoConfig, sampleConfigPath):
        super(PlottingConfig, self).__init__(pathtoConfig) #This just executes the __init__ of the class(es) it inherits from

        self.lumi = self.readConfig.getfloat("General", "lumi")
        self.sampleConf = sampleConfigPath
        self.baseSelection = self.readConfig.get("General", "selection")
        self.baseWeight = self.readConfig.get("General", "weight")

        #We need to scale the weighted sum of the QCD to data in preselection
        self.scaleQCD = self.setOptionWithDefault("General", "scaleQCD", True, getterType = "bool")

        self.allSamples = SampleConfig(self.sampleConf, self.lumi)

        self.legendDict = {}
        for sample in self.allSamples.samples:
            if self.allSamples.samples[sample].sampleName not in self.legendDict.keys():
                self.legendDict[self.allSamples.samples[sample].sampleName] = self.allSamples.samples[sample].displayName

        self.blind = self.setOptionWithDefault("General", "blind", False, getterType = "bool")
        self.logscale = self.setOptionWithDefault("General", "logScale", False, getterType = "bool")
        self.ratioRange = self.setOptionWithDefault("General", "ratioRange", [0.2, 1.8], getterType = "List")
        self.ignoreAddSampleWeight = self.setOptionWithDefault("General", "ignoreAddSampleWeight", False, getterType = "bool")
        self.plots = {}
        """
        Plot information is saved in a namedTuple python object
        Attributes:
          variable (str) : name of the variable to be plotted --> Must be valid branch name in the ROOT::TTree
          niceName (str) : Name that will be displayed on the x-axis
          formula (str) : 
          nBins (int) : Number of bins
          binRange (tuple) : Tuple containing (xBinMin, xBinMax) of the plot 
          selection (str) : Addition selection set on the plot --> Use strings compatible with TTree::Draw o.s.
          weight (str) : Additonal weight set for this plot --> Use strings compatible with TTree::Draw o.s.
          yAxis (str) : Test that will be displayed on the y-Axis

        Note on selection and weight: Selection set for the plots will be appended with **AND** to the base selection set
        before. This is done already on config level so only one selection needs to be called.  Same for plot-based weight
        but will be mulitplied.
        """
        plotTuple = namedtuple("plotTuple", ["variable",
                                             "niceName",
                                             "formula",
                                             "nBins",
                                             "binRange",
                                             "selection",
                                             "weight",
                                             "yAxis",
                                             "logScale",
                                             "ratioRange",
                                             "ignoreAddSampleWeight"])
        for section in self.readConfig.sections():
            #The config only contzrains the General section and section for plots. So we can just skip the General section to get all plots
            if section == "General": 
                continue
            formula = section
            if self.readConfig.has_option(section, "formula"):
                formula = self.readConfig.get(section, "formula")

            self.plots[section] = plotTuple(
                variable = section,
                formula = formula,
                niceName = self.setOptionWithDefault(section, "niceName", section),
                nBins = self.readConfig.getint(section, "nBins"),
                binRange = (self.readConfig.getfloat(section, "binsLower"),self.readConfig.getfloat(section, "binsUpper")),
                selection = "({0} && {1})".format(self.baseSelection, self.setOptionWithDefault(section, "selection", "1")),
                weight = "({0} * {1})".format(self.baseWeight, self.setOptionWithDefault(section, "weight", "1")),
                yAxis = self.setOptionWithDefault(section, "yAxis", "Events"),
                logScale = self.setOptionWithDefault(section, "logScale", self.logscale, getterType = "bool"),
                ratioRange = self.setOptionWithDefault(section, "ratioRange", self.ratioRange, getterType = "List"),
                ignoreAddSampleWeight = self.setOptionWithDefault("General", "ignoreAddSampleWeight", self.ignoreAddSampleWeight, getterType = "bool"),
            )

        logging.debug("------------ Config ------------")
        logging.debug("lumi = %s", self.lumi)
        logging.debug("sampleConf = %s", self.sampleConf )
        logging.debug("baseSelection = %s", self.baseSelection)
        logging.debug("baseWeight = %s", self.baseWeight)
        logging.debug("Scale QCD : %s", self.scaleQCD)
        logging.debug("  Init %s sample", len(self.allSamples.samples.keys()))
        for sample in self.allSamples.samples.keys():
            logging.debug("  Sample : %s", sample)
            logging.debug("    sampleName = %s", self.allSamples.samples[sample].sampleName)
            logging.debug("    displayName = %s", self.allSamples.samples[sample].displayName)
            logging.debug("    path = %s", self.allSamples.samples[sample].path)
            logging.debug("    xsec = %s", self.allSamples.samples[sample].xsec)
            logging.debug("    nGen = %s", self.allSamples.samples[sample].nGen)
            logging.debug("    color = %s", self.allSamples.samples[sample].color)
            logging.debug("    isData = %s", self.allSamples.samples[sample].isData)
            logging.debug("    SF = %s", self.allSamples.samples[sample].SF)
            logging.debug("    addScale = %s", self.allSamples.samples[sample].addScale)
            logging.debug("    addWeight = %s", self.allSamples.samples[sample].addWeight)
            logging.debug("    addSelection = %s", self.allSamples.samples[sample].addSelection)
        logging.debug("  Output Samples %s", self.allSamples.outputSamples)
        logging.debug("Initialized %s plots", len(self.plots.keys()))
        for plot in self.plots:
            logging.debug("  Plot %s", plot)
            logging.debug("    var %s", self.plots[plot].variable)
            logging.debug("    niceName %s", self.plots[plot].niceName)
            logging.debug("    nBins %s", self.plots[plot].nBins)
            logging.debug("    binRange %s", self.plots[plot].binRange)
            logging.debug("    selection %s", self.plots[plot].selection)
            logging.debug("    weight %s", self.plots[plot].weight)
            logging.debug("    logScale %s", self.plots[plot].logScale)
            logging.debug("    ratioRange %s", self.plots[plot].ratioRange)
            logging.debug("    ignoreAddSampleWeight %s", self.plots[plot].ignoreAddSampleWeight)
            


def main(config, outputPath, tagger4Trigger, era, signalName = "ttH", yieldTable = False, yieldPerBin = False):
    checkNcreateFolder(outputPath)

    # Load Trigger SF macro
    ROOT.gROOT.LoadMacro("classes.h") # You might have to chnage the path tp the file in classes.h of Error in <TFile::TFile>: file appears
    ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D = new TriggerSFMulitEra("{0}", "{1}")'.format(era, tagger4Trigger))
    ROOT.gROOT.LoadMacro("functions.h")


    #############################################################################################################################
    ##################################################### TODO ##################################################################
    #############################################################################################################################
    ## 1 Loop over all individual samples (set in the config) and
    ##   - Open Skim and get tree
    ##   -> Opening TFiles and TTrees outside to actual loop and saving them in dict usually leads to less problems with ROOT
    ## 2 Loop over all variables (config.plots.keys())
    ##   2.1 Loop over all samples and for each sample
    ##       - get histograms with TTree::Project(hName, Variable, Selection, Weight) --> See classes.PlotHelpers.project()
    ##       - weight histogran according to sample lumi*xsec/nGen (see config.allSamples.samples[sample].SF variable)
    ##   2.2 Sum histos of samples that are set to end as as one singel histo in the plots (see config.allSamples.outputSamples)
    ##   2.3 Set style of hisograms
    ##       - For Data: Color, Marker (21)
    ##       - For MC: Fill color, Fill style (3001 for solid)
    ##       - Axis Title
    ##   2.4 Create TStack and add all MC samples to it
    ##   2.5 Pass Histograms to classes.ratios.Ratio and use it to draw the plots
    ## 3 Save plots
    #############################################################################################################################
    #############################################################################################################################
    #############################################################################################################################

    files={}
    tree={}
    for sample in config.allSamples.samples:
        #Get the trees from the files
        if sample == "data" and config.blind:
            logging.warning("Skipping data because blinded is activated")
            continue
        files[sample]=ROOT.TFile.Open(config.allSamples.samples[sample].path)
        tree[sample]=files[sample].Get("tree")


    #Loop over all the variables
    for var in config.plots:
        #Create base histogram
        logging.info("Processing variable %s", var)
        hName=config.plots[var].variable
        nBins=config.plots[var].nBins
        xBinMin, xBinMax = config.plots[var].binRange
        hBase=ROOT.TH1F(hName, hName, nBins, xBinMin, xBinMax)

        #We fill this histogram with all the samples
        histos4Samples={}

        logging.info("Projecting Histograms")
        for sample in config.allSamples.samples:
            histos4Samples[sample]=hBase.Clone(hBase.GetName()+sample)#Clone histogram
            #We exclude data from weighting
            if sample == "data" and config.blind:
                logging.warning("Skipping data because blinded is activated")
                continue
            if sample == "data":
                thisSelection = "({0} && {1})".format(config.plots[var].selection, config.allSamples.samples[sample].addSelection)
            else:
                if config.plots[var].ignoreAddSampleWeight:
                    if config.allSamples.samples[sample].addWeight != "1":
                        logging.warning("Ignoring additional sample %s of %s", config.allSamples.samples[sample].addWeight, sample)
                    thisSelection = "({0} && {1}) * ({2})".format(config.plots[var].selection,
                                                                  config.allSamples.samples[sample].addSelection,
                                                                  config.plots[var].weight)
                else:
                    thisSelection = "({0} && {1}) * ({2} * {3})".format(config.plots[var].selection,
                                                                        config.allSamples.samples[sample].addSelection,
                                                                        config.plots[var].weight,
                                                                        config.allSamples.samples[sample].addWeight)
            tree[sample].Project(hBase.GetName()+sample , config.plots[var].formula , thisSelection)
            logging.debug("%s | %s | %s",sample, histos4Samples[sample].Integral(), config.allSamples.samples[sample].SF)

            #Overflow
            moveOverUnderFlow(histos4Samples[sample])

            #We still have to scale with the cross section
            logging.debug("Scaling %s with cross section: %s", sample, config.allSamples.samples[sample].SF)
            histos4Samples[sample].Scale(config.allSamples.samples[sample].SF)

        if config.blind:
            logging.warning("Will replace data with the sum of all simulated samples")
            mcSamples = [x for x in histos4Samples.keys() if "data" not in x]
            for isample, sample in enumerate(mcSamples):
                if isample == 0:
                    logging.debug("Replaced data with %s as blinded data", sample)
                    histos4Samples["data"] = histos4Samples[sample].Clone("BlindedData")
                else:
                    logging.debug("Added %s to blinded data", sample)
                    histos4Samples["data"].Add(histos4Samples[sample])


        # Now that we have all the histograms we need to merge some of them.
        logging.debug("Creating merged histograms")
        mergedSamples = {}
        for iName, name in enumerate(config.allSamples.outputSamples):
            logging.info("Merging sample group  %s", name)
            for iSample, sample in enumerate(config.allSamples.outputSamples[name]):
                if iSample == 0:
                    mergedSamples[name] = histos4Samples[sample].Clone(hBase.GetName()+"_"+name)
                else:
                    mergedSamples[name].Add(histos4Samples[sample])

        #Now we only have to handle the merged ones.
        #Set the style of the histos accordingly (MC filled, Data not filled but markers)
        logging.debug("Setting style for merged histograms")
        for iName, name in enumerate(config.allSamples.outputSamples):
            logging.info("Merging sample group  %s", name)
            for iSample, sample in enumerate(config.allSamples.outputSamples[name]):
                if name=="data":
                    mergedSamples[name].SetMarkerStyle(20)
                    mergedSamples[name].SetMarkerSize(1)
                    mergedSamples[name].SetMarkerColor(ROOT.kBlack)
                    mergedSamples[name].SetLineColor(ROOT.kBlack)
                else:
                    mergedSamples[name].SetFillColor(config.allSamples.samples[sample].color)
                    mergedSamples[name].SetFillStyle(1001)
                    mergedSamples[name].SetLineColor(ROOT.kBlack)


        #Also create a total sum of all going into the Stack since we need it for the ratio plot
        #Clone the first histogram in the stack
        for iSample, sample in enumerate(mergedSamples):
            if "data" in mergedSamples[sample].GetName():
                continue
            if iSample == 0:
                stackSum=mergedSamples[sample].Clone(mergedSamples[sample].GetName().replace(sample, "stackSum"))
            else:
                stackSum.Add(mergedSamples[sample])


        if config.scaleQCD:
            #Scaling of the QCD from the data
            #First, we get the difference between stackSum and the data (let's call it f)
            f=mergedSamples["data"].Clone(mergedSamples["data"].GetName()) #Clone histogram
            f.Add(stackSum,-1) #Substract histograms
            #Now we multiply the QCD by a constant C such that Int(QCD_final)=Int(QCD_initial)+Int(f)
            #It holds that C=1+Int(f)/Int(QCD_initial)
            mergedSamples["QCD"].Scale(1+f.Integral()/mergedSamples["QCD"].Integral())
            logging.warning("Scaling QCD by factor %s", 1+f.Integral()/mergedSamples["QCD"].Integral())

            #Since we have just scaled the histogram, we now have to compute the background sum again so that the ratio agrees with the histogram.
            for iSample, sample in enumerate(mergedSamples):
                if "data" in mergedSamples[sample].GetName():
                    continue
                if iSample == 0:
                    stackSum=mergedSamples[sample].Clone(mergedSamples[sample].GetName().replace(sample, "stackSum"))
                else:
                    stackSum.Add(mergedSamples[sample])


        if yieldTable:
            table = []
            if yieldPerBin:
                for iSample, sample in enumerate(mergedSamples):
                    line = [sample]
                    for i in range(1,stackSum.GetNbinsX()+1):
                        line.append("{:.2f}".format(mergedSamples[sample].GetBinContent(i)))
                    table.append(line)
            else:
                for iSample, sample in enumerate(mergedSamples):
                    line = [sample, "{:.2f}".format(mergedSamples[sample].Integral())]
                    table.append(line)

            with open(outputPath+"/dataMC_yieldTable_"+var+("_perBin" if yieldPerBin else "")+".txt", "w") as f:
                for line in table:
                    f.write("\t".join(line)+"\n")
            

        stack=ROOT.THStack("stackPlot","stackPlot")
        for iName, name in enumerate(config.allSamples.sampleOrder):
            #we do not want the data in the stack, only MC
            logging.debug("Adding %s to the stack", name)
            if name!="data":
                  stack.Add(mergedSamples[name])



        #After creating the stack we only need to use to classes.ratios.RatioPlot object to get the plots.
        ratioPlot=RatioPlot("plot")
        ratioPlot.passHistos([mergedSamples["data"] , stack ])
        ratioPlot.legendSize = (0.6, 0.55, 0.90, 0.90)
        ratioPlot.invertRatio = True
        ratioPlot.legColumns = 2
        ratioPlot.scaleLegenText = 0.9
        ratioPlot.CMSLeft = True
        ratioPlot.CMSOneLine = True
        ratioPlot.moveCMSTop = 0.08
        ratioPlot.CMSscale = (1.0, 1.2)
        ratioPlot.thisLumi = str(config.lumi)
        ratioPlot.ratioRange = tuple([float(x) for x in config.plots[var].ratioRange])
        #Errorband
        errorband = stackSum.Clone(stackSum.GetName().replace("stackSum", "errorband"))
        errorband.SetLineColor(ROOT.kBlack)
        errorband.SetFillColor(ROOT.kBlack)
        errorband.SetFillStyle(3645)

        #Set the color of the ratioplot markers to black
        stackSum.SetLineColor(ROOT.kBlack)
        stackSum.SetFillColor(ROOT.kBlack)

        # a = mergedSamples["data"].Clone("a")
        # a.Divide(stackSum)
        # for i in range(1, a.GetNbinsX()+1):
        #     print "data", i, mergedSamples["data"].GetBinError(i)
        #     print "backsum", i, stackSum.GetBinError(i)
        #     print "ratio", i, a.GetBinError(i)


            
        #Legend
        legendObjwText = []
        for hist in mergedSamples:
            if "data" in hist:
                continue
            legendObjwText.append((deepcopy(mergedSamples[hist]), config.legendDict[hist]))
            
            
            # #We exclude the data since it's been already included
            # if "tt" in mergedSamples[hist].GetName():
            #     if signalName in mergedSamples[hist].GetName():
            #         legendObjwText.append((deepcopy(mergedSamples[hist]), signalName))
            #     elif "ttV" in mergedSamples[hist].GetName():
            #         legendObjwText.append((deepcopy(mergedSamples[hist]), "ttV"))
            #     else:
            #         legendObjwText.append((deepcopy(mergedSamples[hist]), "tt"))
            # if "QCD" in mergedSamples[hist].GetName():
            #     legendObjwText.append((deepcopy(mergedSamples[hist]), "QCD"))

        legendObjwText.append((deepcopy(errorband), "Uncertainty"))

        #Draw plot and save canvas in pdf
        
        thisCanvas=ratioPlot.drawPlot(None ,xTitle=config.plots[var].niceName, isDataMCwStack = True, stacksum=stackSum, errorband=errorband , histolegend=legendObjwText, logscale=config.plots[var].logScale, drawCMS=True)
        saveCanvasListAsPDF([thisCanvas], "dataMCPlots_"+var ,outputPath)
        # if config.plots[var].variable=="nBDeepCSVM":
        #     print(bkgsum.GetBinContent(3))


if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        type = str,
        required=True,
        help = "Plotting config",
    )
    argumentparser.add_argument(
        "--yieldTable",
        action = "store_true",
        help = "If passed a yield table will be displayed",
    )
    argumentparser.add_argument(
        "--perBin",
        action = "store_true",
        help = "Yield will be saved per bin. Only effective if --yieldTable is passed",
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        type = str,
        required=True,
        help = "Output path",
    )
    argumentparser.add_argument(
        "--sampleConfig",
        action = "store",
        type = str,
        default = "../data/plotDataMC/sample_bis.cfg",
        help = "samplec config",
    )
    argumentparser.add_argument(
        "--era",
        action = "store",
        required = False,
        default = "2016",
        choices=["2016","2017","2018"],
        type = str,
        help = "Set era used for trigger SF",
    )
    argumentparser.add_argument(
        "--tagger",
        action = "store",
        required = False,
        default="DeepFlav",
        choices=["CSV","DeepCSV", "DeepFlav"],
        type = str,
        help = "Set the tagger used for trigger SF",
    )

    args = argumentparser.parse_args()
    ##############################################################################################################

    initLogging(args.loglevel)

    config = PlottingConfig(args.config, args.sampleConfig)

    main(config, args.output, args.tagger, args.era, yieldTable = args.yieldTable, yieldPerBin = args.perBin)
