"""
Note that in order to change from deepCSV to deepFlav (or the other way around), all the correspondent strings throughout
the code must be changed manually (Update 2018 data)
"""

#!/usr/bin/env python
import ROOT
from array import array
import Helper
from copy import deepcopy
import os
import GetEfficiencyTH3F
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(levelname)-8s %(funcName)-20s %(message)s')
logging.basicConfig(
    filename='debug.log',
    format=log_format,
    level=logging.INFO,
)

formatter = logging.Formatter(log_format, datefmt="%H:%M:%S")
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
logging.getLogger().addHandler(handler)
#############################################################
#############################################################

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

if True:
    ROOT.gErrorIgnoreLevel = ROOT.kWarning# kPrint, kInfo, kWarning, kError, kBreak, kSysError, kFatal;


def moveOverUnderFlow(histo, moveOverFlow=True, moveUnderFlow=False):
    """
    Function for moving the overflow and (or) underflow bin to the first/last bin
    """
    nBins = histo.GetNbinsX()
    if moveUnderFlow:
        underflow = histo.GetBinContent(0)
        fistBinContent = histo.GetBinContent(1)
        histo.SetBinContent(1, fistBinContent+underflow)
        histo.SetBinContent(0, 0)
    if moveOverFlow:
        overflow = histo.GetBinContent(nBins+1)
        lastBinContent = histo.GetBinContent(nBins)
        histo.SetBinContent(nBins, lastBinContent+overflow)
        histo.SetBinContent(nBins+1, 0)


def makeBaseHisto(name, title, bins, Htype):
    """
    Function for making the base histograms that are used to initialize the plots

    Args:
        name (str) : Name of the histogram (internal ROOT name)
        title (str) : Histogram title --> Will be used as x-axis Title
        bins (list) : List of bin edges. Including Lower and upper edge of histo
        Htype (str) : Either Data or MC --> Used for setting style

    Returns:
        ROOT.TH1F Object
    """
    logging.debug("Making base histo for %s", name)
    htmp = ROOT.TH1F(name, title, len(bins)-1, array("d",bins))
    htmp.Sumw2()
    return htmp

def setStyle(histo, Htype):
    if Htype == "Data":
        #Colors for each run A-D
        color = ROOT.kBlack #Black, A
        #color = ROOT.kBlue #Blue, B
        #color = ROOT.TColor.GetColor(1,0.76,0.0275) #Yellow, C
        #color = ROOT.TColor.GetColor(0.398,0.786,0.214) # Green, D
        #color=ROOT.kGreen
        marker = 20
    elif Htype == "MC":
        color = ROOT.kRed
        marker = 23
    else:
        raise KeyError("Htype can be Data or MC. Your choice: {0}".format(hType))
    histo.SetLineColor(color)
    histo.SetLineWidth(2)
    histo.SetMarkerStyle(marker)
    histo.SetMarkerColor(color)
    histo.SetMarkerSize(0.8)

def make3DBaseHisto(name, title, xbinning, ybinning, zbinning):
    """
    Function for creating the 3D base histogram
    """
    logging.debug("Making 3D base histogram")
    h3 = ROOT.TH3F(name, title,
                   len(xbinning)-1, array("d",xbinning),
                   len(ybinning)-1, array("d",ybinning),
                   len(zbinning)-1, array("d",zbinning),
    )
    h3.Sumw2()
    return h3

def makeHistos(baseHistoVars, prefix, SFHisto = False):
    """
    Function to initialize all the histograms need.

    Args:
        baseHistosVars (list) : List of tuples with base TH1F and variable name (var, th1f)
        prefix (str) : will be used for Cloning and histoname
    Return:
        histoDict (dict) : Dictionary containing the total, triggered and efficiency histograms for each variable
    """
    if SFHisto:
        histoDict = { "ScaleFactor" : {}  }
    else:
        histoDict = { "Total" : {},
                      "Triggered" : {},
                      "Efficiency" : {},
        }
    for variable, histo in baseHistoVars:
        logging.debug("Cloning histograms for variable %s with new names %s", variable,"{0}_*_{1}".format(prefix, variable) )
        if SFHisto:
            histoDict["ScaleFactor"][variable] = histo.Clone("{0}_sf_{1}".format(prefix, variable))
        else:
            histoDict["Total"][variable] = histo.Clone("{0}_tot_{1}".format(prefix, variable))
            histoDict["Triggered"][variable] = histo.Clone("{0}_trig_{1}".format(prefix, variable))
            histoDict["Efficiency"][variable] = histo.Clone("{0}_eff_{1}".format(prefix, variable))
    return histoDict

def make3DHistos(base3DHisto, prefix, binning = None, SFHisto = False):
    """
    Function to initialize all 3d histograms. Will create binned histograms for specified in args

    Args:
        3dBaseHisto (ROOT.TH3F) : base 3D histogram
        prefix (str) : Will be used for cloning
        binName (str) : var name of the binning
        binning (list) : List of bin edges. Including Lower and upper edge

    Returns:
        histoDict (dict)
    """
    if SFHisto:
        histoDict = { "ScaleFactor" : {}  }
    else:
        histoDict = { "Total" : {},
                      "Triggered" : {},
                      "Efficiency" : {},
        }
    if binning is None:
        if SFHisto:
            histoDict["ScaleFactor"]["0"] = base3DHisto.Clone("{0}_tot".format(prefix))
        else:
            histoDict["Total"]["0"] = base3DHisto.Clone("{0}_tot".format(prefix))
            histoDict["Triggered"]["0"] = base3DHisto.Clone("{0}_trig".format(prefix))
            histoDict["Efficiency"]["0"] = base3DHisto.Clone("{0}_eff".format(prefix))
    else:
        logging.info("Initializing binned 3D histograms --> Will create %s histos", len(binning)-1)
        for ibin in range(len(binning)-1):
            if SFHisto:
                histoDict["ScaleFactor"][str(ibin)] = base3DHisto.Clone("{0}_sf_{1}".format(prefix, ibin))
            else:
                histoDict["Total"][str(ibin)] = base3DHisto.Clone("{0}_tot_{1}".format(prefix, ibin))
                histoDict["Triggered"][str(ibin)] = base3DHisto.Clone("{0}_trig_{1}".format(prefix, ibin))
                histoDict["Efficiency"][str(ibin)] = base3DHisto.Clone("{0}_eff_{1}".format(prefix, ibin))

    return histoDict


def FillDataHistos(tree, histos, treeVars, baseSelection, triggerSelection, dataSelection = "1"):
    """
    Funciton for filling the Data histograms

    Args:
       tree (ROOT.TTree) : Tree for projection
       histos (dict) : Dict with histos (see makeHistos function)
       treeVars (dict) : Dict with strings used for projection (corresonding to var names in makeHistos function)
       *Selection (str) : Selection for the projection
    """
    logging.info("Start filling data histos")
    if list(set(histos["Total"].keys())) != list(set(treeVars.keys())):
        raise RuntimeError("Keys of histos and treeVars do not match! Please check. histos %s - treeVars %s"%(histos["Total"].keys(), treeVars.keys()))

    logging.debug("Filling Total histos")
    for var in histos["Total"]:
        logging.debug("Projection Total for %s",var)
        totSel = tree.Project(
            histos["Total"][var].GetName(),
            treeVars[var],
            "({0} && {1} && {2})".format(baseSelection, "1", dataSelection)
        )
        logging.debug("Selection %s total data events", totSel)
        moveOverUnderFlow(histos["Total"][var])
    logging.debug("Filling Triggerd histos")
    for var in histos["Triggered"]:
        logging.debug("Projection Triggerd for %s",var)
        trigSel = tree.Project(
            histos["Triggered"][var].GetName(),
            treeVars[var],
            "({0} && {1} && {2})".format(baseSelection, triggerSelection, dataSelection)
        )
        logging.debug("Selection %s triggered data events", trigSel)
        moveOverUnderFlow(histos["Triggered"][var])
    logging.debug("Filling Efficienies")
    for ivar, var in enumerate(histos["Efficiency"]):
        if ivar == 0:
            logging.info("Integral triggerd: %s | integral Total: %s",histos["Triggered"][var].Integral(), histos["Total"][var].Integral())
        histos["Efficiency"][var].Divide(histos["Triggered"][var], histos["Total"][var], 1, 1, "cl=0.683 b(1,1) mode")

    for plottype in histos:
        for var in histos[plottype]:
            setStyle(histos[plottype][var], "Data")



def FillData3DHistos(tree, histos, xyzTreeVars, binTreeVar = None, binning = None, baseSelection = "1", triggerSelection = "1", dataSelection = "1", useEffFunc = False):
    """
    Function for filling the 3D histograms for data

    Args:
       tree (ROOT.TTree) : Tree for projection
       histos (dict) : Dict with histos (see make3DHistos function)
       xyzTreeVars (tuple) : the 3 variable that will be used for projection
       binTreeVar (str) : Variable for the binning of the histograms
       binning (list) : bin edges
       *Selection (str) : Selection for the projection
    """
    if not isinstance(xyzTreeVars, tuple) or not len(xyzTreeVars) == 3:
        raise RuntimeError("xyzTreeVars is required to be a type with 3 str but is {0}".format(xyzTreeVars))
    logging.info("Starting 3D projection of Data")
    if binTreeVar is None or binning is None:
        binloop = [("0" , "1")]
    else:
        binloop = []
        for ibin in range(len(binning)-1):
            binloop.append( (str(ibin) , "{0} >= {1} && {0} < {2}".format(binTreeVar, binning[ibin], binning[ibin+1])) )
    for ibin, binSel in binloop:
        logging.info("Projection 3D histo with sel: %s",binSel)
        nprojTot = tree.Project(
            histos["Total"][ibin].GetName(),
            "{0}:{1}:{2}".format(xyzTreeVars[2], xyzTreeVars[1], xyzTreeVars[0]),
            "({0} && {1} && {2} && {3})".format(baseSelection, "1", dataSelection, binSel),
        )
        trigSel = "({0} && {1} && {2} && {3})".format(baseSelection, triggerSelection, dataSelection, binSel)
        logging.debug("TriggerSel: %s", trigSel)
        logging.debug("Will project to %s",histos["Triggered"][ibin].GetName())
        logging.debug("Variable: %s","{0}:{1}:{2}".format(xyzTreeVars[2], xyzTreeVars[1], xyzTreeVars[0]))
        nprojTrig = tree.Project(
            histos["Triggered"][ibin].GetName(),
            "{0}:{1}:{2}".format(xyzTreeVars[2], xyzTreeVars[1], xyzTreeVars[0]),
            trigSel,
        )
        logging.debug("Projection yielded %s total events and %s triggered events", nprojTot, nprojTrig)
        if useEffFunc:
            logging.warning("Will calculate efficiency using GetEfficiencyTH3F module")
            name = histos["Efficiency"][ibin].GetName()
            histos["Efficiency"][ibin] = GetEfficiencyTH3F.GetEfficiencyTHNF(histos["Triggered"][ibin], histos["Total"][ibin])
            histos["Efficiency"][ibin].SetName(name)
        else:
            histos["Efficiency"][ibin].Divide(histos["Triggered"][ibin], histos["Total"][ibin],1,1,"cl=0.683 b(1,1) mode")

def FillMCHistos(trees, inhistos, outHistos, treeVars, baseSelection, triggerSelection, weight, tagger="DeepCSV",closure = False):
    """
    Function for filling the MC histos. This will also sum all the trees and put them into the outHistos

    Args:
       trees (list(ROOT.TTree)) : List of Tree for projection
       inhistos (list(dict)) : List Dict with histos (see makeHistos function)
       treeVars (dict) : Dict with strings used for projection (corresonding to var names in makeHistos function)
       *Selection (str) : Selection for the projection
       weight (list(str)) : weight expression for the projection
    """
    logging.info("Starting MC projection")
    for histos in inhistos:
        if list(set(histos["Total"].keys())) != list(set(treeVars.keys())):
            raise RuntimeError("Keys of histos and treeVars do not match! Please check. histos %s - treeVars %s"%(histos["Total"].keys(), treeVars.keys()))
    if len(inhistos) != len(trees):
        raise AssertionError("Trees and inhisto list have different length")
    if closure:
        logging.warning("Will apply Trigger weight for closure test")
        closureWeight = "get_TriggerSF3D(ht30, jets_pt[5], nB{0}M)".format(tagger)
        #closureWeight = "get_TriggerSF3D(ht30, jets_pt[5], nBDeepFlavM)"
    else:
        closureWeight = "1"
    for ihistos, histos in enumerate(inhistos):
        logging.info("Processing MC sample %s", ihistos)
        for var in histos["Total"]:
            logging.debug("Projection Total for %s",var)
            totSel = trees[ihistos].Project(histos["Total"][var].GetName(),treeVars[var],"({0}) * ({1})".format(baseSelection, weight[ihistos]))
            logging.debug("Selection %s total mc events", totSel)
            logging.debug("Integral: %s",histos["Total"][var].Integral())
            moveOverUnderFlow(histos["Total"][var])
            logging.debug("Triggerd sel: ({0}) * ({1})".format(baseSelection, weight[ihistos]))
        for var in histos["Triggered"]:
            logging.debug("Projection Triggerd for %s",var)
            trigSel = trees[ihistos].Project(
                histos["Triggered"][var].GetName(),
                treeVars[var],
                "({0} && {1}) * ({2} * {3})".format(baseSelection, triggerSelection, weight[ihistos], closureWeight)
            )
            logging.debug("Selection %s triggered mc events", trigSel)
            logging.debug("Integral: %s",histos["Triggered"][var].Integral())
            moveOverUnderFlow(histos["Triggered"][var])
            logging.debug("Triggerd sel: ({0} && {1}) * ({2})".format(baseSelection, triggerSelection, weight[ihistos]))
    logging.info("Making sum of MC")
    for histos in inhistos:
        for var in histos["Total"]:
            outHistos["Total"][var].Add( histos["Total"][var] )
        for var in histos["Triggered"]:
            outHistos["Triggered"][var].Add( histos["Triggered"][var] )
    logging.debug("Total MC Sum integral: %s",outHistos["Total"][outHistos["Total"].keys()[0]].Integral())
    logging.debug("Triggered MC Sum integral: %s",outHistos["Triggered"][outHistos["Total"].keys()[0]].Integral())
    logging.info("Calculating efficiency")
    for ivar, var in enumerate(outHistos["Efficiency"]):
        if ivar == 0:
            logging.info("Integral triggerd: %s | integral Total: %s",outHistos["Triggered"][var].Integral(), outHistos["Total"][var].Integral())
        outHistos["Efficiency"][var].Divide(outHistos["Triggered"][var], outHistos["Total"][var], 1, 1, "cl=0.683 b(1,1) mode")

    for plottype in outHistos:
        for var in outHistos[plottype]:
            setStyle(outHistos[plottype][var], "MC")

def FillMC3DHistos(trees, inhistos, outhistos, xyzTreeVars, binTreeVar = None, binning = None, baseSelection = "1", triggerSelection = "1", weights = ["1"], tagger="DeepCSV", useEffFunc = False, closure = False):
    """
    Function for filling the 3D histograms for data

    Args:
       tree (ROOT.TTree) : Tree for projection
       inhistos (list(dict)) : List Dict with histos (see make3DHistos function)
       xyzTreeVars (tuple) : the 3 variable that will be used for projection
       binTreeVar (str) : Variable for the binning of the histograms
       binning (list) : bin edges
       *Selection (str) : Selection for the projection
    """
    logging.debug("Tagger = %s", tagger)
    if not isinstance(xyzTreeVars, tuple) or not len(xyzTreeVars) == 3:
        raise RuntimeError("xyzTreeVars is required to be a type with 3 str but is {0}".format(xyzTreeVars))
    if closure:
        logging.warning("Will apply Trigger weight for closure test")
        closureWeight = "get_TriggerSF3D(ht30, jets_pt[5], nB{0}M)".format(tagger)
        #closureWeight = "get_TriggerSF3D(ht30, jets_pt[5], nBDeepFlavM)"
    else:
        closureWeight = "1"
    logging.info("Starting 3D projection of MC")
    if binTreeVar is None or binning is None:
        binloop = [("0" , "1")]
    else:
        binloop = []
        for ibin in range(len(binning)-1):
            binloop.append( (str(ibin) , "{0} >= {1} && {0} < {2}".format(binTreeVar, binning[ibin], binning[ibin+1])) )
    for ihistos, histos in enumerate(inhistos):
        logging.debug("Processing MC sample %s", ihistos)
        for ibin, binSel in binloop:
            logging.info("Projection 3D histo with sel: %s",binSel)
            nprojTot = trees[ihistos].Project(
                histos["Total"][ibin].GetName(),
                "{0}:{1}:{2}".format(xyzTreeVars[2], xyzTreeVars[1], xyzTreeVars[0]),
                "({0} && {1} && {2}) * ({3})".format(baseSelection, "1", binSel, weights[ihistos]),
            )
            nprojTrig = trees[ihistos].Project(
                histos["Triggered"][ibin].GetName(),
                "{0}:{1}:{2}".format(xyzTreeVars[2], xyzTreeVars[1], xyzTreeVars[0]),
                "({0} && {1} && {2}) * ({3} * {4})".format(baseSelection, triggerSelection, binSel, weights[ihistos], closureWeight),
            )
            histos["Efficiency"][ibin].Divide(histos["Triggered"][ibin], histos["Total"][ibin],1,1,"cl=0.683 b(1,1) mode")
            logging.debug("Projection yielded %s total events and %s triggered events", nprojTot, nprojTrig)
    for ihistos, histos in enumerate(inhistos):
        for ibin, binSel in binloop:
            outhistos["Total"][ibin].Add(histos["Total"][ibin])
            outhistos["Triggered"][ibin].Add(histos["Triggered"][ibin])
    for ibin, binSel in binloop:
        if useEffFunc:
            logging.warning("Will calculate efficiency using GetEfficiencyTH3F module")
            name = outhistos["Efficiency"][ibin].GetName()
            outhistos["Efficiency"][ibin] = GetEfficiencyTH3F.GetEfficiencyTHNF(outhistos["Triggered"][ibin], outhistos["Total"][ibin])
            outhistos["Efficiency"][ibin].SetName(name)
        else:
            outhistos["Efficiency"][ibin].Divide(outhistos["Triggered"][ibin], outhistos["Total"][ibin],1,1,"cl=0.683 b(1,1) mode")


def Make3DScaleFactors(dataHistos, mcHistos, sfHistos):
    """
    Function to make the 3SFs by dividing MC and Data efficiencies
    """
    bins = dataHistos["Efficiency"].keys()
    for b in bins:
        sfHistos["ScaleFactor"][str(b)].Divide(dataHistos["Efficiency"][b], mcHistos["Efficiency"][b], 1, 1)

def setEfficiencyStyle(histos, titles, xmin = 0.4, xmax = 1.1): #0.55
    """
    Function for setting the style of the efficiency plots:

    Args:
       histos (dict) : Dictonary with vars as keys and efficiency histograms
       titles (dict) : Dictonary with vars as keys and histograms titles
       xmin, xmax (float) : y range of efficiency plot
    """
    logging.debug("Setting Efficiency style")
    if list(set(histos["Efficiency"].keys())) != list(set(titles.keys())):
        raise RuntimeError("Keys of histos and titles do not match! Please check")

    for var in histos["Efficiency"]:
        histos["Efficiency"][var].SetTitle(titles[var])
        histos["Efficiency"][var].GetYaxis().SetTitleSize(0.069*0.69)
        histos["Efficiency"][var].GetYaxis().SetTitleOffset(0.98)
        histos["Efficiency"][var].GetYaxis().SetLabelSize(0.060*0.75)
        histos["Efficiency"][var].GetXaxis().SetTitleSize(0.069*0.75)
        histos["Efficiency"][var].GetXaxis().SetLabelSize(0.06*0.75)
        histos["Efficiency"][var].GetXaxis().SetDecimals(1)
        histos["Efficiency"][var].GetYaxis().SetDecimals(1)
        histos["Efficiency"][var].GetXaxis().SetNdivisions(805)
        histos["Efficiency"][var].GetYaxis().SetNdivisions(505)
        histos["Efficiency"][var].GetYaxis().SetRangeUser(xmin,xmax)

def makePlots(dataHistos, mcHistos, plottype = "Efficiency", normalized = False, lumi = 1):
    """
    Function for making the plots. From sets of passed histograms
    Args:  
      dataHistos (dict) : Dictionary containing "Triggered" "Total" and "Efficiency" histograms for data
      mcHistos (dict) : Dictionary containing "Triggered" "Total" and "Efficiency" histograms for MC
      plottype (str) : Selects with hsitograms are plotted
      normalized (bool) : If set to true the plots will be normalized to integal = 1
      lumi (float) : Value for lumi label in plot
    Returns:
      allCanvas (list) : List of canvases
    """
    box = Helper.create_paves(lumi, "DataWiP", CMSposX=0.155, CMSposY=0.84,
                          prelimPosX=0.15, prelimPosY=0.79,
                          lumiPosX=0.977, lumiPosY=0.91, alignRight=False,
                          CMSsize=0.075*.75, prelimSize=0.057*.75, lumiSize=0.060*.75)

    logging.info("Starting to plot plottype %s", plottype)
    leg = ROOT.TLegend(0.7,0.3,0.85,0.5)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.05)
    onekey = dataHistos[plottype].keys()[0]
    leg.AddEntry(mcHistos[plottype][onekey], "t#bar{t} MC", "PLE")
    leg.AddEntry(dataHistos[plottype][onekey], "data", "PLE")


    line = ROOT.TF1("line","1",0.0,9000.0)
    line.SetLineColor(12) #Grey
    line.SetLineWidth(1)
    line.SetLineStyle(2) #Dashe

    allCanvas = []
    for var in dataHistos[plottype]:
        logging.debug("Drawing %s for %s", plottype, var)
        canvas = ROOT.TCanvas("c"+plottype+"_"+var,"c"+plottype+"_"+var, 5, 30, 640, 580)
        canvas.SetTopMargin(0.08*.75)
        canvas.SetRightMargin(0.04)
        canvas.SetLeftMargin(0.11)
        canvas.SetBottomMargin(0.12)
        canvas.SetTicks()
        canvas.cd()
        if normalized:
            dataHistos[plottype][var].GetYaxis().SetTitle("Normalized Units")
            dataHistos[plottype][var].DrawNormalized("PE")
            mcHistos[plottype][var].DrawNormalized("PESAME")
        else:
            dataHistos[plottype][var].Draw("PE")
            mcHistos[plottype][var].Draw("PESAME")
        leg.Draw("SAME")
        box["lumi"].Draw()
        box["CMS"].Draw()
        box["label"].Draw()
        canvas.Update()
        if plottype == "Efficiency":
            logging.info("Drawing line")
            line.Draw("SAME")

        canvas.Update()
        allCanvas.append(deepcopy(canvas))

    return allCanvas


def saveCanvasListAsPDF(listofCanvases, outputfilename, foldername):
    logging.info("Writing outputfile %s.pdf",outputfilename)
    for icanvas, canves in enumerate(listofCanvases):
        if icanvas == 0:
            canves.Print(foldername+"/"+outputfilename+".pdf(")
        elif icanvas == len(listofCanvases)-1:
            canves.Print(foldername+"/"+outputfilename+".pdf)")
        else:
            canves.Print(foldername+"/"+outputfilename+".pdf")

def saveHistosAsROOT(histosList, outfileName, foldername):
    logging.info("Writing outputfile %s.root",outfileName)
    out = ROOT.TFile(foldername+"/"+outfileName+".root", "RECREATE")
    out.cd()
    for obj in histosList:
        for plottype in obj.keys():
            for plot in obj[plottype].keys():
                obj[plottype][plot].Write()
    out.Close()

def make3DSFs(tag, tdata, mcInfo, scaleMCto, mcWeight, baseSel, trigger, triggerMC, datacuts, tagger="DeepCSV", era ="2018", foldername = ".", offlinecuts = True, closureTest = False):
    """
    Main script generating the efficiency plots and SF for a strategy based on 3 offline variables

    Base workflow:
    - Get 1d distributions for all variables, for data and MC, for numerator and denominator
    - Get the 3D histogrmas from the skimmed files for data and all passed MC (MC samples will be merged) for data and MC, for numerator and denominator
    - Generate efficiency plots for 1D histograms and save them (as pdf and ROOT file)
    - Calculate efficiency histograms from the 3D histogram
    - Divide 3D histos of Data and MC efficiencies to get the SF and save all of them (as ROOT file)
    --> Use editSFFile.py to edit single bins to get more even SF if statistics are low (usually only required in high b-tag bin)

    Args:
      tag (str) : Will be used as prefix for the output plot
      tdata (ROOT:TTree) : Tree from the data skim
      mcInfo (dict - SampleName : (ROOT.TTree, xsecWeight)) : MC trees and xsec weight for each considered MC samples
      scaleMCto (float) : Lumi * 1000 (1000 since xsec in pb)
      mcWeights (str) : Weight for the MC sample
      baseSel (str) : Base selection applied to all plots <-> Is denominator selection (or Total as it is called in the functions)
      trigger (str) : Trigger selection for the measurmeent <-> Added seletion the get the numerator
      triggerMC (str) : Same as trigger but for CM samples. Necessary because tigger can be different in Data and MC (usually only names)
      datacuts (str) : Selection only applied in Data. Usually used to select certain run periods
      foldername (str) : Output folder
      offlineCits (bool) : See same variable in TriggerSF()
      closureTest (bool) : See same variable in TriggerSF()
      tagger (str) : Tagger used for the measurment (only DeepCSV and DeepFlav are expected)
      era (str) : Era for the measrument (only 2016, 2017 or 2018 are expected)
    """
    logging.info("Making Base histos")
    logging.debug("Tagger = %s", tagger)
    treeVars = {
        #"ht" : "Sum$(jets_pt*(jets_pt>=40))",
        "ht" : "ht30",
        "pt" : "jets_pt[5]",
        "pt4" : "jets_pt[3]",
        "numJets" : "numJets",
        "nb" : "nB{0}M".format(tagger),
        #"ht4" : "Sum$(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3])",
        #"ht6" : "Sum$(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])",
        #"ht40" : "Sum$(jets_pt * (jets_pt > 40 && abs(jets_eta) < 2.4))",
        #"nb" : "nBDeepFlavM"
    }

    titles = {
        "ht" : ";#it{H}_{T} (GeV);Efficiency",
        "pt" : ";6th leading jet p_{T} (GeV);Efficiency",
        "pt4" : ";4th leading jet p_{T} (GeV);Efficiency",
        "numJets" : ";Number of Jets;Efficiency",
        #"nb" : ";Number of medium DeepFlav b tags;Efficiency",
        "nb" : ";Number of medium {0} b tags;Efficiency".format(tagger),
        #"ht4" : ";4 jet #it{H}_{T} (GeV);Efficiency",
        #"ht6" : ";6 jet #it{H}_{T} (GeV);Efficiency",
        #"ht40" : ";#it{H}_{T, p_{T} > 40} (GeV);Efficiency",
    }

    if offlinecuts:
        binning = {
            "ht" :  [500,550,600,700,800,1000,1500,2500],
            #"ht" :  [500,550,600,700,800,1000,1500,2000,2500],
            "pt" :  [40,45,50,60,70,120,200],
            "pt4" : [40,60,100,300],
            "numJets" : [6,7,8,9,11],
            "nb" : [2,3,4,8],
            #"ht4" :  [270,300,330,360,400,450,500,550,650,800,1000,1400],
            #"ht6" :  [300, 330, 360, 400,450,500,550,650,800,1000,1400],
            #"ht40" :  [400, 450, 500,550,600,700,800,1000,1500,2000],
        }

    else:
        binning = {
            "ht" :  [450,500,550,600,700,800,1000,1500,2500],
            #"ht" :  [450,500,550,600,700,800,1000,1500,2000,2500],
            "pt" :  [35,40,45,50,60,70,120,200],
            "pt4" : [40,60,100,300],
            "numJets" : [6,7,8,9,11],
            "nb" : [1,2,3,4,8],
        }

    BaseDataHistos = [
        ("ht", makeBaseHisto("hd_ht", ";HT[GeV];", binning["ht"], "Data")),
        ("pt", makeBaseHisto("hd_pt", ";6jpT[Gev];", binning["pt"], "Data")),
        ("pt4", makeBaseHisto("hd_pt4", ";4jpT[GeV];", binning["pt4"], "Data")),
        ("numJets", makeBaseHisto("hd_numJets", ";numJets;", binning["numJets"], "Data")),
        ("nb", makeBaseHisto("hd_nb", ";{0}M;".format(tagger), binning["nb"], "Data")),
        #("nb", makeBaseHisto("hd_nb", ";FlavCSVM;", binning["nb"], "Data")),
        #("ht4", makeBaseHisto("hd_ht4", ";4jHT[GeV];", binning["ht4"], "Data")),
        #("ht6", makeBaseHisto("hd_ht6", ";6jHT[GeV];", binning["ht6"], "Data")),
        #("ht40", makeBaseHisto("hd_ht40", ";HT40[GeV];", binning["ht40"], "Data")),
    ]
    BaseMCHistos = [
        ("ht", makeBaseHisto("hm_ht", ";HT[GeV];", binning["ht"], "MC")),
        ("pt", makeBaseHisto("hm_pt", ";6jpT[Gev];", binning["pt"], "MC")),
        ("pt4", makeBaseHisto("hm_pt4", ";4jpT[GeV];", binning["pt4"], "MC")),
        ("numJets", makeBaseHisto("hm_numJets", ";numJets;", binning["numJets"], "MC")),
        ("nb", makeBaseHisto("hm_nb", ";{0}M;".format(tagger), binning["nb"], "MC")),
        #("nb", makeBaseHisto("hm_nb", ";DeepFlavM;", binning["nb"], "MC")),
        #("ht4", makeBaseHisto("hm_ht4", ";4jHT[GeV];", binning["ht4"], "MC")),
        #("ht6", makeBaseHisto("hm_ht6", ";6jHT[GeV];", binning["ht6"], "MC")),
        #("ht40", makeBaseHisto("hm_ht40", ";HT40[GeV];", binning["ht40"], "MC")),
    ]

    Base3D = make3DBaseHisto(
        "h3", "h3",
        xbinning = binning["ht"],
        ybinning = binning["pt"],
        zbinning = binning["nb"]
    )

    dataHistos = makeHistos(BaseDataHistos, "hData")
    mcSLHistos = {}
    for key in mcInfo:
        mcSLHistos[key] = makeHistos(BaseDataHistos, "hMC_"+key)
    mcHistos = makeHistos(BaseDataHistos, "hMC")

    data3DHistos = make3DHistos(Base3D, "h3Data")
    mcSL3DHistos= {}
    for key in mcInfo:
        mcSL3DHistos[key] = make3DHistos(Base3D, "h3MC_"+key)
    mc3DHistos = make3DHistos(Base3D, "h3MC")
    sf3DHistos = make3DHistos(Base3D, "h3SF", SFHisto = True)

    logging.info("Starting histogram filling")
    logging.info("Processing 2D histograms")
    hmcList = []
    h3mcList = []
    tmcList = []
    wmcList = []
    for key in mcInfo:
        hmcList.append(mcSLHistos[key])
        h3mcList.append(mcSL3DHistos[key])
        tmcList.append(mcInfo[key][0])
        wmcList.append(str(scaleMCto * mcInfo[key][1])+" * "+mcWeight)

    logging.info("MC samples:"+str(mcInfo.keys()))
    FillMCHistos(tmcList, hmcList, mcHistos, treeVars, baseSel, triggerMC, wmcList, tagger, closureTest)
    FillDataHistos(tdata, dataHistos, treeVars, baseSel, trigger, "1"+datacuts)

    logging.info("Processing 3D histograms")
    FillData3DHistos(tdata, data3DHistos, (treeVars["ht"], treeVars["pt"], treeVars["nb"]), None, None, baseSel, trigger, "1"+datacuts)
    FillMC3DHistos(tmcList, h3mcList, mc3DHistos, (treeVars["ht"], treeVars["pt"], treeVars["nb"]),  None, None, baseSel, triggerMC, wmcList, closure=closureTest, tagger=tagger)
    Make3DScaleFactors(data3DHistos, mc3DHistos, sf3DHistos)

    setEfficiencyStyle(dataHistos, titles)
    setEfficiencyStyle(mcHistos, titles)

    finalCEffs = makePlots(dataHistos, mcHistos, lumi = (scaleMCto/1000.0) )
    finalCTotals = makePlots(dataHistos, mcHistos,"Total", normalized = True, lumi = (scaleMCto/1000.0))
    finalCTriggers = makePlots(dataHistos, mcHistos,"Triggered", normalized = True, lumi = (scaleMCto/1000.0))

    saveCanvasListAsPDF(finalCEffs, "eff_out_3DSF_"+tag, foldername)
    saveCanvasListAsPDF(finalCTotals, "tot_out_3DSF_"+tag, foldername)
    saveCanvasListAsPDF(finalCTriggers, "trig_out_3DSF_"+tag, foldername)

    saveHistosAsROOT([data3DHistos, mc3DHistos, sf3DHistos], "sf3d_out_3DSF_"+tag, foldername)
    saveHistosAsROOT([mcHistos, dataHistos], "2Dhistis_eff_"+tag, foldername)

def TriggerSF(tag, outFolder, offlinecuts, tagger="DeepCSV", closureTest = False, era="2018", effType="SF"):
    """
    Base function for running Trigger efficiency script for certain variables. These efficiency curves will be used to calculate 
    the trigger SF by taking the difference between MC and Data.
    Since we measure the efficiencies for the ttH FH trigger configuration wrt to the IsuMu_27 single muon trigger, the SingleMu 
    dataset is usedef fro data. Since the main bkg contribution comes form ttbar we only consider ttbar as MC for the calculation.

    ###########################################################################################
    #                 _____                                 _                 _               #
    #                |_   _|                               | |               | |              #
    #                  | |   _ __ ___   _ __    ___   _ __ | |_  __ _  _ __  | |_             #
    #                  | |  | '_ ` _ \ | '_ \  / _ \ | '__|| __|/ _` || '_ \ | __|            #
    #                 _| |_ | | | | | || |_) || (_) || |   | |_| (_| || | | || |_             #
    #                |_____||_| |_| |_|| .__/  \___/ |_|    \__|\__,_||_| |_| \__|            #
    #                                  | |                                                    #
    #                                  |_|                                                    #
    #                                                                                         #
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
    #!! Currently datasets, selection, weights, Triggers, run ranges, nGen, sec  and output !!#
    #!! folder are hardcoded in this function. Please check all setting before running      !!#
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
    #                                                                                         #
    #*****************************************************************************************#
    #** Actual offline variables for the calculation, binnings, titles etc are hardcoded in **#
    #** make3DSFs(). Also ceck this when creating a new measurment                          **#
    #*****************************************************************************************#
    #                                                                                         #
    #=========================================================================================#
    #== Has dependencies on Helper.py - Plotting stuff                                      ==#
    #=========================================================================================#
    #                                                                                         #
    ###########################################################################################

    Args:
      tag (str) : Will be used as c
      offlinecuts (bool) : If True is passed, looser offline cut will be used -> Basically add one bin below usual FH preselection
      closureTest (bool) : If True SF will be loaded form the usual c++ module for on-the-fly calculation of the Trigger SF in TTree::Draw
                           type plotting scripts. Only use this after running the default calculation and updating classes.h and funcitons.h
      tagger (str) : Tagger used for the measurment (only DeepCSV and DeepFlav are expected)
      era (str) : Era for the measrument (only 2016, 2017 or 2018 are expected)

    """

    runcuts = ["2018RunX"]
    #runcuts=["RunB" , "RunC" , "RunCPre" , "RunCNoPre" , "RunD" , "RunE" , "RunF"]
    #runcuts=["RunG" , "RunH" , "RunI"]
    #runcuts=["RunTest"]
    #runcuts = ["RunB","RunD","RunE","RunF"]
    #runcuts = ["RunB","RunCNoPre","RunD","RunE","RunF"]
    #runcuts = ["RunB","RunCNoPre","RunD"]
    #runcuts = ["RunCNoPre"]
    #runcuts = ["RunB"]
    #runcuts = ["RunA"]
    #runcuts = ["RunC"]
    #runcuts = ["RunD"]

    #runcuts =["RunX"]
    #runcuts =["RunY"]
    #runcuts =["RunZ"]

    
    RunCuts = { "2017RunB": " (run>= 297020 && run<= 299329)",
                "2017RunC": " (run>= 299337 && run<= 302029)",
                "2017RunCPre": " (run>= 299337 && run<= 300999)",
                "2017RunCNoPre": " (run>= 301000 && run<= 302029)",
                "2017RunD": " (run>= 302030 && run<= 303434)",
                "2017RunE": " (run>= 303435 && run<= 304826)",
                "2017RunF": " (run>= 304911 && run<= 306462)",
                "2018RunA": " (run>= 315252 && run<= 316995)",
                "2018RunB": " (run>= 316998 && run<= 319312)",
                "2018RunC": " (run>= 319313 && run<= 320393)",
                "2018RunD": "(run>= 320394 && run<= 325273)",
                #Periods where trigger in data changes
                "2018RunX": "(run>= 315252 && run<315974)",
                "2018RunY": "(run>= 315974 && run<317509)",
                "2018RunZ": "(run>= 317509)",
    }

    tag+="_"+era+"_"+tagger
    
    tag += "_"+("tightCut" if offlinecuts else "looseCut")
    if runcuts:
        #tag += "Run"
        for irun, run in enumerate(runcuts):
            if irun == 0:
                #tag += run[len("Run"):]
                tag += "_"+run
            else:
                tag += "-"+run
                #tag += "-"+run[len("Run"):]

    """
    #Old Binning
    if offlinecuts:
        htbins = [500,550,600,650,700,800,1000,1500,2000,2500]
        ptbins = [40,45,50,55,60,70,120,200]
        pt4bins = [40,50,60,80,100,120,200,300]
        nbbins = [2,3,4,8]
    else:
        htbins = [450,500,550,600,650,700,800,1000,1500,2000]
        ptbins = [35,40,45,50,55,60,70,120,200]
        pt4bins = [35,40,50,60,80,100,120,200,300]
        nbbins = [1,2,3,4,8]
    """

    if era == "2018" or era == "2017":
        if effType == "SF":
            baseSel = "is_sl && abs(leps_pdgId[0]) == 13 && (HLT_BIT_HLT_IsoMu27) && passMETFilters == 1"
            trigger = "(HLT_ttH_FH || HLT_BIT_HLT_PFHT1050)"
            # trigger = "(HLT_BIT_HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5)"
            # trigger = "(HLT_BIT_HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 || HLT_BIT_HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2)"
            # trigger = "(HLT_BIT_HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5 || HLT_BIT_HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5 || HLT_BIT_HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59)"
        else:
            btaggingEff = "All" # 1T, 2T, 3T, All
            baseSel = "passMETFilters == 1"
            controlTrigger_1T = "(HLT_BIT_HLT_PFHT430_SixPFJet40==1 || HLT_BIT_HLT_PFHT450_SixPFJet36==1 )"
            trigger_1T = "(HLT_BIT_HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5==1  || HLT_BIT_HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5==1  || HLT_BIT_HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59==1 )"
            controlTrigger_2T = "(HLT_BIT_HLT_PFHT380_SixPFJet32 || HLT_BIT_HLT_PFHT400_SixPFJet32)"
            trigger_2T = "(HLT_BIT_HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2 || HLT_BIT_HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94)"
            controlTrigger_3T = "HLT_BIT_HLT_PFHT330PT30_QuadPFJet_75_60_45_40"
            trigger_3T = "HLT_BIT_HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5"
            if btaggingEff == "1T":
                control = controlTrigger_1T
                trigger = trigger_1T
            elif btaggingEff == "2T":
                control = controlTrigger_2T
                trigger = trigger_2T
            elif btaggingEff == "3T":
                control = controlTrigger_3T
                trigger = trigger_3T
            else:
                control = "({0} || {1} || {2})".format(controlTrigger_1T, controlTrigger_2T, controlTrigger_3T)
                trigger = "({0} || {1} || {2})".format(trigger_1T, trigger_2T, trigger_3T)
            baseSel = baseSel + "&&" + control

            
    else:
        baseSel = "is_sl && abs(leps_pdgId[0]) == 13 && (HLT_BIT_HLT_IsoMu24) && passMETFilters == 1"
        trigger = "(HLT_ttH_FH || HLT_BIT_HLT_PFJet450)"
    triggerMC = trigger

    
    #tag += "_All_CSV"
    #tag += "_all_wPuGenB"
    foldername = outFolder
    if effType != "SF":
        foldername += "/bTagEff"
    
    logging.warning("Using tagger %s", tagger)
    
    if offlinecuts:
        logging.warning("Will use tight offline cuts")
        baseSel += "&&(ht30>500 && jets_pt[5]> 40 && nB{0}M>=2 && njets>=6)".format(tagger)


    else:
        logging.warning("Will use loose offline cuts")
        baseSel += "&&(ht30>450 && jets_pt[5]> 35 && nB{0}M>=1 && njets>=6)".format(tagger)

    datacuts = ""
    if runcuts:
        datacuts = "&&(0"
        for run in runcuts:
            datacuts += "||"+RunCuts[run]
            logging.info("Adding run %s to datasel", run)
        datacuts += ")"

    logging.info("baseSel: %s", baseSel)
    logging.debug("datacuts: %s", datacuts)

    if era == "2018":
        if effType == "SF":
            fdata = ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_Trigger_v1/SingleMuon.root")

            mcFiles = {
                "DL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_Trigger_v1/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root"),
                "SL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_Trigger_v1/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root"),
                "AH" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_Trigger_v1/TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root")
            }
        else:
            fdata = ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_v1/JetHT.root")

            mcFiles = {
                "DL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_Trigger_v1/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root"),
                "SL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_Trigger_v1/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root"),
                "AH" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2018/LegacyRunII_Trigger_v1/TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root")
            }
            
        scaleMCto = 1000*59.74

        if effType == "SF":
            nGen = {
                "DL" : 130768000,
                "SL" : 99085000,
                "AH" : 130768000
            }
        else:
            nGen = {
                "DL" : 62553428.0,
                "SL" : 98275640.0,
                "AH" : 129702336.0
            }

            
    elif era == "2017":
        fdata = ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2017/LegacyRunII_Trigger_v1/SingleMuon.root")

        mcFiles = {
            "DL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2017/LegacyRunII_Trigger_v1/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root"),
            "SL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2017/LegacyRunII_Trigger_v1/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"),
            "AH" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2017/LegacyRunII_Trigger_v1/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root")
        }

        scaleMCto = 1000*41.53

        nGen = {
            "DL" : 67536704,
            "SL" : 109318352,
            "AH" : 129458696
        }
        
    else:
        fdata = ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2016/LegacyRunII_Trigger_v1p1/SingleMuon.root")

        mcFiles = {
            "DL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2016/LegacyRunII_Trigger_v1p1/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root"),
            "SL" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2016/LegacyRunII_Trigger_v1p1/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"),
            "AH" : ROOT.TFile.Open("/scratch/koschwei/ttH/skims/2016/LegacyRunII_Trigger_v1p1/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root")
        }

        scaleMCto = 1000*35.92

        nGen = {
            "DL" : 67145424,
            "SL" : 104807376,
            "AH" : 67251464
        }

    tdata = fdata.Get("tree")
    mcInfo = { # (tree, xsec SF)
        "DL" : (mcFiles["DL"].Get("tree"), 88.34/float(nGen["DL"])),
        "SL" : (mcFiles["SL"].Get("tree"), 365.46/float(nGen["SL"])),
        "AH" : (mcFiles["AH"].Get("tree"), 377.96/float(nGen["AH"])),
        }
    if closureTest:
        tag += "_closure"

    foldername = foldername+"/"+era+"/"+tagger
    if not os.path.exists(foldername):
        logging.warning("Creating folder: {0}".format(foldername))
        os.makedirs(foldername)

    mcWeight = "puWeight * (sign(genWeight))"# * btag{0}Weight_shape".format(tagger)

    logging.info("Will use **%s** as filename postfix",tag)
    make3DSFs(tag, tdata, mcInfo, scaleMCto, mcWeight, baseSel, trigger, triggerMC, datacuts, tagger, era, foldername, offlinecuts, closureTest)


if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--tag",
        action = "store",
        required = True,
        type = str,
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--looseOfflinecuts",
        action = "store_false",
        help = "If enabled, the loose offline cuts for SF measurement will be used",
    )
    argumentparser.add_argument(
        "--closure",
        action = "store_true",
        help = "If enabled, Trigger SF will be applied. See functions.h and classes.h",
    )
    argumentparser.add_argument(
        "--era",
        action = "store",
        required = False,
        default = "2018",
        choices=["2016","2017","2018"],
        type = str,
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--folder",
        action = "store",
        required = False,
        default="TriggerPlots_LegacyRun2",
        type = str,
        help = "Set the folder name used for the output",
    )
    argumentparser.add_argument(
        "--tagger",
        action = "store",
        required = False,
        default="DeepCSV",
        choices=["CSV","DeepCSV", "DeepFlav"],
        type = str,
        help = "Set the tagger used for the output",
    )
    argumentparser.add_argument(
        "--effType",
        action = "store",
        required = False,
        default="SF",
        choices=["SF","Tagging"],
        type = str,
        help = "Set the efficiency type. SF will use the standard strategy for SF with IsoMu and SingleMu dataset. Tagging will measure the b.tagging efficiency in JetHT",
    )

    args = argumentparser.parse_args()
    
    ROOT.gROOT.LoadMacro("classes.h")
    ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D = new TriggerSFMulitEra("{0}", "{1}")'.format(args.era, args.tagger))
    ROOT.gROOT.LoadMacro("functions.h") #inlcude functions that can be used in TTree::Draw

    if args.effType == "Tagging" and args.era != "2018":
        raise RuntimeError("Tagging only supported in 2018. Aborting.")

    TriggerSF(tag = args.tag, outFolder=args.folder, offlinecuts = args.looseOfflinecuts, tagger=args.tagger, closureTest = args.closure, era=args.era, effType=args.effType)
