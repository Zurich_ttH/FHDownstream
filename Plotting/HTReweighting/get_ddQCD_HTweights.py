#!/usr/bin/env python
import ROOT, os, sys
sys.path.insert(0, os.path.abspath('..'))
from classes.PlotHelpers import moveOverUnderFlow
forCpp = True

#file_4b = "/work/koschwei/slc7/ttH/LegacyRun2/v3p2/CMSSW_10_2_15_patch2/src/TTH/FHDownstream/Plotting/QCDEst/LegacyRun2_2016_AH_v3/CombinedDNN/QCDEstimate_ge7j4t_6jHT_VR.root"
#file_4b = "/work/koschwei/slc7/ttH/LegacyRun2/v3p2/CMSSW_10_2_15_patch2/src/TTH/FHDownstream/Plotting/QCDEst/LegacyRun2_2017_AH_v3/CombinedDNN/QCDEstimate_ge7j4t_6jHT_VR.root"
file_4b = "/work/koschwei/slc7/ttH/LegacyRun2/v3p2/CMSSW_10_2_15_patch2/src/TTH/FHDownstream/Plotting/QCDEst/LegacyRun2_2018_AH_v3/CombinedDNN/QCDEstimate_ge7j4t_6jHT_VR.root"
#check_ge7j_4b_VR_6jHT_R_alphaCR_btagCorrDataMC_kBest_signal.root"

f4b = ROOT.TFile.Open(file_4b)
ddQCD4b = f4b.Get("SR_ge7j4t_6jHT_MultiJet")
data4b = f4b.Get("SR_ge7j4t_6jHT_data")
ttbar4b = f4b.Get("SR_ge7j4t_6jHT_tt")
bkg4b = f4b.Get("SR_ge7j4t_6jHT_minor")

moveOverUnderFlow(ddQCD4b, True, True)
moveOverUnderFlow(data4b, True, True)
moveOverUnderFlow(ttbar4b, True, True)
moveOverUnderFlow(bkg4b, True, True)


for i in range(ddQCD4b.GetNbinsX()+2):
    nqcd4b = ddQCD4b.GetBinContent(i)
    ndata4b = data4b.GetBinContent(i)-ttbar4b.GetBinContent(i)-bkg4b.GetBinContent(i)
    edge = ddQCD4b.GetBinLowEdge(i+1)
    if nqcd4b <= 0.000001:
        ndata4b = 0
        nqcd4b = 1
    if forCpp:
        print "if(HT<%s){ return %s; }"%(edge, (ndata4b/nqcd4b))
    else:
        print "elif HT<{0}: return {1:0.4f}".format(edge, (ndata4b/nqcd4b))
    
