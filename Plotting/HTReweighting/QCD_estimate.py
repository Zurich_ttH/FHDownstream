#!/usr/bin/env python
from time import sleep
from scipy import stats
import os
import sys
sys.path.insert(0, os.path.abspath('..'))
import ROOT, copy
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetErrorX(0) #turns of horizontal error bars (but also for bkg)
ROOT.gROOT.SetBatch(1)
import numpy as np
import Helper

saveplots = True
savehistos = True
saveCRRoot = True
lumi = Helper.luminosity #fb-1

jets = ["7j","8j","9j"] #run all 6 categories
bjets = ["3b","4b"]
jets = ["7j"] ##temp
bjets = ["3b"] ##temp
regions = ["SR","CR","VR","CR2"] #do not run x regions in same session as non-x regions. Should be ok now.
#regions = ["SRy","CRy","VRy","CR2y"] #do not run x regions in same session as non-x regions. Should be ok now.
ROOT.gROOT.LoadMacro("../classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("../functions.h") #inlcude functions that can be used in TTree::Draw

#Correction for scripts in CRCorrections folder
if "x" in regions[0]:
    ROOT.gROOT.LoadMacro("../CRcorrections/bMLCorrections.h+")
else:
    ROOT.gROOT.LoadMacro("../CRcorrections/btagCorrections.h+")

#ROOT.gROOT.LoadMacro("functions/HTreweight.h+")
#ROOT.gROOT.LoadMacro("functions/triggerWeight.h+")
# ROOT.gROOT.LoadMacro("puWeight/functions.h+") #puWt added to "extra" skims


idMap = Helper.sampleIDMap


memscale = False
memcorrect = False
CRcorrect = True
rewtTopPt = False
HTreweight = False #temp
scalebins = [1,10]
corrfac = [0.92,0.80] #3b,4b
doDetaJCut = True

validation =False #VR=True, SR=False
useMC = False
method = "alphaCR" #"ABCD" or "alphaCR" or "alphaVR"
unblind = True
blindcut = ""#"&& mem_FH_4w2h2t_p<0.3" #"&& mem_FH_3w2h2t_p<0.3" # "&& mem_FH_4w2h2t_p<0.3" "3w2h2t" "4w2h1t"

drawPulls = True
incSignal = True

#wt = "*puWeight*btagWeightCSV*triggerWeight*qgWeight*sign(genWeight)"

#if useMC:
#    raise NotImplementedError

if "x" in regions[0]:
    bCorrData = "*exp(Sum$(log(bMLCorrData(jets_pt,jets_eta,jets_dRmin,jets_btagDeepCSV))*jets_btagFlag))"
    bCorrMC = "*exp(Sum$(log(bMLCorrMC(jets_pt,jets_eta,jets_dRmin,jets_btagDeepCSV))*jets_btagFlag))"
else:
    bCorrData = "*exp(Sum$(log(btagCorrData(jets_pt,jets_eta,jets_dRmin, jets_btagDeepCSV))*jets_btagFlag))"
    #bCorrData = "*exp(Sum$(log(btagCorrData(jets_pt,jets_eta,jets_dRmin,jets_btagCSV,jets_btagDeepCSV))*jets_btagFlag))"
    bCorrMC = "*exp(Sum$(log(btagCorrMC(jets_pt,jets_eta,jets_dRmin,jets_btagDeepCSV))*jets_btagFlag))"
    #bCorrMC = "*exp(Sum$(log(btagCorrMC(jets_pt,jets_eta,jets_dRmin,jets_btagCSV,jets_btagDeepCSV))*jets_btagFlag))"
    #bCorrData = "*exp(Sum$(log(btagCorrDataUp(jets_pt,jets_eta,jets_dRmin,jets_btagCSV))*jets_btagFlag))" #TEMP!!
    #bCorrMC = "*exp(Sum$(log(btagCorrMCUp(jets_pt,jets_eta,jets_dRmin,jets_btagCSV))*jets_btagFlag))"

qgWeightNorm = Helper.get_QGNormaliziation(deltaEta = doDetaJCut)
minorBkg = True
backgrounds = ["ww","wz","zz","st_t","stbar_t","st_s","st_tw","stbar_tw","ttw_wqq","ttz_zqq","WjetstoQQ400","WJetsToQQ600","WJetsToQQ800","ZJetsToQQ400","ZJetsToQQ600","ZJetsToQQ800"]
if incSignal:
    backgrounds.append("ttH_hbb")
    backgrounds.append("ttH_nonhbb")
qcd = ["QCD300","QCD500","QCD700","QCD1000","QCD1500","QCD2000"]
#qcd = ["QCD500","QCD700","QCD1000","QCD1500","QCD2000"] #temp
#qcd = ["TTbar_inc"] ##temp

presel = Helper.preselection
#presel = "HLT_ttH_FH&&ht>500" #need 6j40 because of systematics
cuts = Helper.get_cuts(DeltaEta = doDetaJCut)
datasets = Helper.datasets
print datasets
print datasets["data"]
xsecs = Helper.xsecs
#qgWtFac = Helper.qgWtFac
#topPTrewt = Helper.topPTrewt
nGen = Helper.nGen



wt = "*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*qgWeight*sign(genWeight)*bTagSF * bTagSF_norm"
if rewtTopPt:
    wtTop = wt+"*"+topPTrewt+"*(jets_pt[0]/jets_pt[0])"
else:
    wtTop = wt

version = "v5"

variables = [("jets_pt[0]","(40,0,800)"), ("jets_pt[1]","(40,0,600)"), ("jets_pt[2]","(40,0,400)"),
             ("jets_pt[3]","(40,0,300)"), ("jets_pt[4]","(40,0,200)"), ("jets_pt[5]","(40,0,120)"),
             ("jets_qgl[0]","(40,0,1)"), ("jets_qgl[1]","(40,0,1)"), ("jets_qgl[2]","(40,0,1)"),
             ("jets_qgl[3]","(40,0,1)"), ("jets_qgl[4]","(40,0,1)"), ("jets_qgl[5]","(40,0,1)"),
             ("jets_eta[0]","(40,-2.5,2.5)"), ("ht","(40,0,2000)"), ("jets_phi[0]","(40,-3.2,3.2)"),
             ("qg_LR_4b_flavour_5q_0q","(40,0,1)"), ("qg_LR_3b_flavour_5q_0q","(40,0,1)"),
             ("mem_ratio","(10,0,1)"), ("mem_tth","(10,-120,0)"), ("mem_ttbb","(20,-120,0)") ]

variables = [("jets_pt[0]","(40,0,800)"), ("jets_pt[5]","(40,0,120)"), 
             ("jets_eta[0]","(40,-2.5,2.5)"), ("jets_eta[5]","(40,-2.5,2.5)"),
             ("jets_phi[0]","(40,-3.2,3.2)"), ("jets_phi[5]","(40,-3.2,3.2)"),
             ("jets_qgl[0]","(40,0,1)"), ("jets_qgl[5]","(40,0,1)"),
             ("jets_btagCSV[0]","(40,0,1)"), ("jets_btagCSV[5]","(40,0,1)"),
             ("qg_LR_4b_flavour_5q_0q","(40,0,1)"), ("qg_LR_3b_flavour_5q_0q","(40,0,1)"),
             ("nBCSVM","(10,0,10)") ]

variables = [("6jHT","(11,400,1500)"),("ht","(16,400,2000)"),("jets_pt[0]","(11,50,600)"),
             ("jets_pt[5]","(12,0,120)"), ("jets_eta[0]","(20,-2.5,2.5)"),
             ("jets_eta[5]","(20,-2.5,2.5)"), ("jets_qgl[0]","(20,0,1)"),
             ("jets_qgl[5]","(20,0,1)"), ("jets_btagCSV[0]","(20,0,1)"), 
             ("jets_btagCSV[5]","(20,0,1)"), ("mem_ratio","(10,0,1)"),
             ("mem_tth","(20,-30,-10)"), ("mem_ttbb","(20,-30,-10)"),
             ("qg_LR_4b_flavour_5q_0q","(20,0,1)"),
             ("mass_drpair_btag","(20,30,300)"),("min_dr_btag","(20,0,4)"),
             ("mass_drpair_untag","(20,20,200)"),("min_dr_untag","(20,0,3.5)"),
             ("mjjmin","(20,0,200)"),("centralitymass","(20,0,1)"),
             #("hmass","(20,0,400)"),("wmass","(20,0,300)"),
             ("3rdBjet_pt","(20,30,300)"),("3rdBjet_eta","(20,-2.4,2.4)"),
             ("3rdBjet_dRmin","(20,0,5)"),("3rdBjet_dRave","(20,0,5)")]
#variables = [("4thBjet_pt","(20,30,300)"),("4thBjet_eta","(20,-2.4,2.4)"),
#             ("4thBjet_dRmin","(20,0,5)"),("4thBjet_dRave","(20,0,5)")]
#variables = [("mem_ratio","(10,0,1)")] #[("mem_tth","(10,-130,-10)"), ("mem_ttbb","(20,-30,-10)")]
variables = [
    ("3rdBjet_pt","(20,30,300)"),
    ("3rdBjet_eta","(20,-2.4,2.4)"),
    ("3rdBjet_dRmin","(20,0,5)"),
    ("6jHT","(11,400,1500)"),
    ("ht30","(15,500,2000)"),
    ("jets_pt[0]","(11,50,600)"),
    ("jets_eta[0]","(40,-2.5,2.5)"),
    ("jets_qgl[0]","(20,0,1)"),
    ("mass_drpair_btag","(20,30,300)"),
    ("min_dr_btag","(20,0,4)"),
    ("mjjmin","(12,20,80)"),
    ("centralitymass","(20,0,1)"),
    ("mass_drpair_untag","(20,20,200)"),
    ("qg_LR_3b_flavour_5q_0q","(20,0,1)"),
    ("mem_ratio","(10,0,1)"),
]
#variables = [ ("6jHT","(11,400,1500)") ]
#variables = [ ("mem_ratio","(10,0,1)"), ("6jHT","(11,400,1500)") ]
variables = [ ("mem_ratio","(10,0,1)") ]
variables = [
    #("Detaj[5]","(45,0,4.5)"),
    #("Detaj[6]","(45,0,4.5)"),
    ("Detaj[7]","(45,0,4.5)" )
]

if bjets[0] == "3b":
    variables = [ ("nBDeepCSVL","(5,3,8)") ]
else:
    variables = [ ("nBDeepCSVL","(4,4,8)") ]

#variables = [ ("6jHT","(11,400,1500)") ]
#variables = [("jets_pt[0]","(11,50,600)") ]
#variables = [("mass_drpair_btag","(9,30,300)"),("min_dr_btag","(9,0.3,3)")]
#variables = [("jets_pt[0]","(40,0,800)"), ("jets_qgl[0]","(40,0,1)"), ("jets_eta[0]","(40,-2.5,2.5)"),
#             ("jets_phi[0]","(40,-3.2,3.2)"), ("ht","(40,0,2000)"), ("mem_ratio","(10,0,1)")]
#variables = [("3rdBjet_eta","(20,-2.4,2.4)")]

#legend position [x0, y0, x1, y1]
#lp = [0.22,0.64,0.47,0.89] #for mem_ratio
#lp = [0.62,0.64,0.87,0.89] #for mbb, etc.

vars_dict = {"qg_LR_3b_flavour_5q_0q": ["QGLR (3b)","Events / 0.05 units"],
             "ht30": ["#it{H}_{T} (GeV)","Events / 100 GeV"],
             "6jHT": ["6 jet #it{H}_{T} (GeV)","Events / 100 GeV"],
             "jets_pt[0]" : ["Leading jet #it{p}_{T} (GeV)","Events / 50 GeV"],
             "jets_eta[0]" : ["Leading jet #eta","Events / 0.25 units"],
             "jets_qgl[0]" : ["Leading jet QGL","Events / 0.05 units"],
             #"numJets": ["Number of jets","Events / bin"],
             #"nBCSVM": ["Number of b-tags (CSVM)","Events / bin"],
             "nBDeepCSVL": ["Number of loose b-tags (DeepCSVL)","Events / bin"],
             "mem_ratio": ["MEM discriminant","Events / 0.1 units"],
             "jets_qgl[0]": ["Highest jet QGL","Events / 0.05 units"],
             "qg_LR_4b_flavour_5q_0q": ["QGLR (4b)","Events / 0.05 units"],
             "mass_drpair_btag": ["Mass bb(#DeltaR_{min}) (GeV)","Events / 30 GeV"],
             "mjjmin": ["Min jj mass (GeV)","Events / 5 GeV"],
             "min_dr_btag": ["#DeltaR(bb)_{min}","Events / 0.3 units"],
             "Detaj[5]" : ["#Delta#etaJ_{6}", "Events / 0.1 units"],
             "Detaj[6]" : ["#Delta#etaJ_{7}", "Events / 0.1 units"],
             "Detaj[7]" : ["#Delta#etaJ_{8}", "Events / 0.1 units"],
             
}
print "Using",variables
paperVars = ["jets_pt[0]"]

label_dict = {'7j3b': "7 jets, 3 b tags",
              '8j3b': "8 jets, 3 b tags",
              '9j3b': "#geq9 jets, 3 b tags",
              '7j4b': "7 jets, #geq4 b tags",
              '8j4b': "8 jets, #geq4 b tags",
              '9j4b': "#geq9 jets, #geq4 b tags",
              'ge7j3b': "#geq7 jets, 3 b tags",
              'ge7j4b': "#geq7 jets, #geq4 b tags"}

colors = Helper.colors

#folder = "/scratch/dsalerno/tth/80x_M17/"
folder = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/"
data = folder+version+"/data.root"
TT = { "SL" : folder+version+"/bSF/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8",
       "DL" : folder+version+"/bSF/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8",
       "FH" : folder+version+"/bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8"}
qcdfolder = folder+version+"/bSF/"

helperConversion = {"SL" : "TTbar_sl", "DL" : "TTbar_dl", "FH" : "TTbar_fh"}

print version
destination = "./QCD_estimate_plots_v5/"+version
if saveplots:
    if not os.path.exists(destination):
        os.makedirs(destination)
    else:
        print "WARNING: directory already exists. Will overwrite existing files..."

qcdxsec = {"QCD_HT300to500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8": 351300.0,
           "QCD_HT500to700_TuneCUETP8M1_13TeV-madgraphMLM-pythia8": 31630.0,
           "QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8": 6802.0,
           "QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8": 1206.0,
           "QCD_HT1500to2000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8": 120.4,
           "QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8": 25.25}

# open files
if minorBkg:
    fbkg = {}
    tbkg = {}
    bkg_scalefac = {}
    for k in backgrounds:
        print qcdfolder+datasets[k]+".root"
        fbkg[k] = ROOT.TFile.Open(qcdfolder+datasets[k]+".root")
        tbkg[k] = fbkg[k].Get("tree")
        bkg_scalefac[k] = 1000.0 * lumi * xsecs[k] / nGen[k]

if useMC:
    fqcd = {}
    tqcd = {}
    #nqcd = {}
    qcd_scalefac = {}
    for k in qcd:
        print qcdfolder+datasets[k]+".root"
        fqcd[k] = ROOT.TFile.Open(qcdfolder+datasets[k]+".root")#qcd[k])
        tqcd[k] = fqcd[k].Get("tree")
        qcd_scalefac[k] = 1000.0 * lumi * xsecs[k] / nGen[k]

fTT = {}
tTT = {}
TT_scalefac = {}
print "Opening tt:"
for k in TT:
    print TT[k]+".root"
    fTT[k] = ROOT.TFile.Open(TT[k]+".root")
    print "Opening tree"
    tTT[k] = fTT[k].Get("tree")
    TT_scalefac[k] =  1000.0 * lumi * xsecs[helperConversion[k]] / nGen[helperConversion[k]]

print "Opening data:"
fdata = ROOT.TFile.Open(data)
tdata = fdata.Get("tree")


print "loop over categories"
for jc in jets:
    for bc in bjets:
        print "------------------------","TTbar_sl",jc,bc.replace("b","t"),"CR"
        print qgWeightNorm
        print "------------------------",qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["CR"]
        if "3b" in bc:# and jc=="7j":
            htrw = "*HTWeight(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5], 3)"
        elif "4b" in bc:
            htrw = "*HTWeight(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5], 4)"
        btagcorrData = bCorrData + (htrw if HTreweight else "*(1.0+0*jets_pt[0])")
        btagcorrMC = bCorrMC + (htrw if HTreweight else "*(1.0+0*jets_pt[0])")

        for varname, binning in variables:

            variable = varname
            if (jc=="9j" and bc=="4b"):
                memtth = "mem_tth_FH_4w2h2t_p"
                memttbb = "mem_ttbb_FH_4w2h2t_p"
                kfac = "0.065"
            if ( (jc=="7j" or jc=="8j") and bc=="4b"):
                memtth = "mem_tth_FH_3w2h2t_p"
                memttbb = "mem_ttbb_FH_3w2h2t_p"
                kfac = "0.065"
            if (bc=="3b"):
                memtth = "mem_tth_FH_4w2h1t_p"
                memttbb = "mem_ttbb_FH_4w2h1t_p"
                kfac = "0.080"

            if "mem" in variable:
                if "ratio" in variable:
                    variable = "{0}/({0}+{2}*{1})".format(memtth, memttbb, kfac)
                elif "tth" in variable:
                    variable = "log10({0})".format(memtth)
                    if (bc=="4b"): binning = "(20, -30, -10)"
                elif "ttbb" in variable:
                    variable = "log10({0})".format(memttbb)
                    if (bc=="4b"): binning = "(20, -30, -10)"
                else:
                    print variable, "not found. skip"
                    continue

            if variable=="6jHT":
                variable = "jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5]"

            tag = ""  #"_memcut" #"_mem_all"
            extracut = "" # "" for no cut "&&cut" for cut

            CMSposX = 0.18
            CMSposY = 0.80
            prelimPosX = 0.29
            prelimPosY = 0.80
            if variable=="centralitymass":
                #variable = "ht/invmass"
                lp = [0.22,0.41,0.47,0.80]
            elif "mem_tt" in varname:
                lp = [0.22,0.64,0.47,0.89]
            elif "jets_pt" in varname:
                lp = [0.58,0.41,0.87,0.80]
                #CMSposX = 0.1
                #CMSposY = 0.92
                #prelimPosX = 0.15
                #prelimPosY = 0.19
            else:
                lp = [0.58,0.41,0.87,0.80]

            if "3rdBjet" in variable:
                variable = variable.replace("3rdBjet","jets")
                variable += "[jets_DeepCSVindex[2]]"
            elif "4thBjet" in variable:
                variable = variable.replace("4thBjet","jets")
                variable += "[jets_DeepCSVindex[3]]"
            # if variable=="3rdBjet_pt":
            #     variable = "jets_pt[jets_DeepCSVindex[2]]"
            #     #extracut += "&&jets_CSVrank==2"
            # elif variable=="3rdBjet_dRmin":
            #     variable = "jets_dRmin[jets_DeepCSVindex[2]]"
            # elif variable=="3rdBjet_dRave":
            #     variable = "jets_dRave[jets_DeepCSVindex[2]]"
            # elif variable=="3rdBjet_dRmax":
            #     variable = "jets_dRmax[jets_DeepCSVindex[2]]"
                 
            if "alpha" in method and memscale:
                normcut = "&& {0}/({0}+0.02*{1})>0.1".format(memtth, memttbb)
                if memcorrect and drawPulls:
                    tag = "_memCorr"
                elif memcorrect:
                    tag = "_memCorrR"
                elif drawPulls:
                    tag = "_memScale"
                else:
                    tag = "_memScaleR"
            else:
                normcut = ""
                if not drawPulls:
                    tag = "_R"
            tag += "_"+method+("_btagCorrDataMC_topPt" if CRcorrect else "") #temp was _btagCorr
            #"_bCorrddQCDdRSep" "_bCorrDataMCdRSep" "_bCorrDataMCdRave" bCorrddQCDdRpresel bCorrDataMCdRpresel
            tag += "_HTrewt" if HTreweight else ""
            tag += "_signal" if incSignal else ""
            tag += "_pulls" if drawPulls else ""
            var = varname.translate(None,"[]")

            cut = presel
            cutSR = presel+" && "+cuts[jc]+" && "+cuts[bc+regions[0]]+extracut
            cutCR = (presel+" && "+cuts[jc]+" && "+cuts[bc+regions[1]]+extracut).replace(" ","")
            cutVR = presel+" && "+cuts[jc]+" && "+cuts[bc+regions[2]]+extracut
            cutCR2 = (presel+" && "+cuts[jc]+" && "+cuts[bc+regions[3]]+extracut).replace(" ","") 

            cat = jc+"_"+bc+("_"+regions[0].split("R")[1] if regions[0].split("R")[1] else "")
            if method=="alphaCR":
                SR = "VR" if validation else "SR"
                CR = "CR2" if validation else "CR"
            elif method=="alphaVR":
                SR = "CR" if validation else "SR"
                CR = "CR2" if validation else "VR"
            elif method=="ABCD":
                SR = "SR"
                CR = "CR"
                #VR = "VR"
                #CR2 = "CR2"
                
            if useMC:
                SR += "mc"
                CR += "mc"
            print "plotregion",cat+"_"+var          

            c2 = ROOT.TCanvas("c2","",5,30,640,580)
            p2 = ROOT.TPad("p2","",0,0,1,1)
            p2.SetTopMargin(0.08)
            p2.SetRightMargin(0.05)
            p2.Draw()
            p2.cd()

            leg2 = ROOT.TLegend(lp[0],lp[1],lp[2],lp[3])
            leg2.SetFillStyle(0)
            leg2.SetBorderSize(0)
            leg2.SetTextSize(0.03)

            if CRcorrect:
                tdata.Draw(variable+">>hdataCR"+binning,"("+cutCR+")"+btagcorrData)
            else:            
                tdata.Draw(variable+">>hdataCR"+binning,cutCR)
            ydataCRnorm = tdata.Draw("",cutCR+normcut)
            hdataCR = ROOT.gDirectory.Get("hdataCR")
            hdataCR.SetLineColor(1)
            hdataCR.SetMarkerColor(1)
            hdataCR.SetMarkerStyle(20)
            hdataCR.SetMarkerSize(1)
            hdataCR.Sumw2()
            ydataCR = hdataCR.Integral() #DS
            ydataCRscale = hdataCR.Integral(scalebins[0],scalebins[1])

            if CRcorrect:
                tdata.Draw(variable+">>hdataCR2"+binning,"("+cutCR2+")"+btagcorrData)
            else:
                tdata.Draw(variable+">>hdataCR2"+binning,cutCR2)
            hdataCR2 = ROOT.gDirectory.Get("hdataCR2")
            hdataCR2.SetLineColor(1)
            hdataCR2.SetMarkerColor(1)
            hdataCR2.SetMarkerStyle(20)
            hdataCR2.SetMarkerSize(1)
            hdataCR2.Sumw2()
            ydataCR2 = hdataCR2.Integral()
            ydataCR2scale = hdataCR2.Integral(scalebins[0],scalebins[1])

            print "---------------------------",cutVR
            tdata.Draw(variable+">>hdataVR"+binning,cutVR)
            hdataVR = ROOT.gDirectory.Get("hdataVR")
            hdataVR.SetLineColor(1)
            hdataVR.SetMarkerColor(1)
            hdataVR.SetMarkerStyle(20)
            hdataVR.SetMarkerSize(1)
            hdataVR.Sumw2()
            ydataVR = hdataVR.Integral()
            ydataVRscale = hdataVR.Integral(scalebins[0],scalebins[1])

            qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["CR"]
            
            if CRcorrect:
                print "Draw:", variable+">>httbarCR"+binning 
                print "Cut sl:", "("+cutCR+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0)
                print "Cut dl:", "("+cutCR+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_dl"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0)
                print "Cut fh:", "("+cutCR+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_fh"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0)
                tTT["SL"].Draw(variable+">>httbar_SL_CR"+binning,"("+cutCR+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0))
                tTT["DL"].Draw(variable+">>httbar_DL_CR"+binning,"("+cutCR+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_dl"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0))
                tTT["FH"].Draw(variable+">>httbar_FH_CR"+binning,"("+cutCR+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_fh"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0))
            else:
                tTT["SL"].Draw(variable+">>httbar_SL_CR"+binning,"("+cutCR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0)) #DS
                tTT["DL"].Draw(variable+">>httbar_DL_CR"+binning,"("+cutCR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_dl"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0)) #DS
                tTT["FH"].Draw(variable+">>httbar_FH_CR"+binning,"("+cutCR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_fh"][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wtTop else 1.0)) #DS
            tTT["SL"].Draw(variable+">>httbar_SL_CRnorm"+binning,"("+cutCR+normcut+")"+wt) #DS
            tTT["DL"].Draw(variable+">>httbar_DL_CRnorm"+binning,"("+cutCR+normcut+")"+wt) #DS
            tTT["FH"].Draw(variable+">>httbar_FH_CRnorm"+binning,"("+cutCR+normcut+")"+wt) #DS
            httbar_SL_CR = ROOT.gDirectory.Get("httbar_SL_CR")
            httbar_DL_CR = ROOT.gDirectory.Get("httbar_DL_CR")
            httbar_FH_CR = ROOT.gDirectory.Get("httbar_FH_CR")
            httbar_SL_CR.Sumw2()
            httbar_DL_CR.Sumw2()
            httbar_FH_CR.Sumw2()
            httbar_SL_CRnorm = ROOT.gDirectory.Get("httbar_SL_CRnorm") #DS
            httbar_DL_CRnorm = ROOT.gDirectory.Get("httbar_DL_CRnorm") #DS
            httbar_FH_CRnorm = ROOT.gDirectory.Get("httbar_FH_CRnorm") #DS
            httbar_SL_CR.Scale(TT_scalefac["SL"])
            httbar_DL_CR.Scale(TT_scalefac["DL"])
            httbar_FH_CR.Scale(TT_scalefac["FH"])
            httbar_SL_CRnorm.Scale(TT_scalefac["SL"]) #DS
            httbar_DL_CRnorm.Scale(TT_scalefac["DL"]) #DS
            httbar_FH_CRnorm.Scale(TT_scalefac["FH"]) #DS

            httbarCR = httbar_SL_CR.Clone("httbarCR")
            httbarCR.Add(httbar_DL_CR)
            httbarCR.Add(httbar_FH_CR)
            httbarCRnorm =  httbar_SL_CRnorm.Clone("httbarCRnorm")
            httbarCRnorm.Add(httbar_DL_CRnorm)
            httbarCRnorm.Add(httbar_FH_CRnorm)

            
            yttbarCR = httbarCR.Integral() #DS
            yttbarCRnorm = httbarCRnorm.Integral() #DS
            httbarCR.SetFillColor(2)
            httbarCR.SetLineWidth(0)
            yttbarCRscale = httbarCR.Integral(scalebins[0],scalebins[1])

            if CRcorrect:
                tTT["SL"].Draw(variable+">>httbar_SL_CR2"+binning,"("+cutCR2+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_sl"]))
                tTT["DL"].Draw(variable+">>httbar_DL_CR2"+binning,"("+cutCR2+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_dl"][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_dl"]))
                tTT["FH"].Draw(variable+">>httbar_FH_CR2"+binning,"("+cutCR2+")"+wtTop+btagcorrMC+"*{0}".format(qgWeightNorm["TTbar_fh"][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_fh"]))
            else:
                tTT["SL"].Draw(variable+">>httbar_SL_CR2"+binning,"("+cutCR2+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_sl"])) #DS
                tTT["DL"].Draw(variable+">>httbar_DL_CR2"+binning,"("+cutCR2+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_dl"][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_dl"])) #DS
                tTT["FH"].Draw(variable+">>httbar_FH_CR2"+binning,"("+cutCR2+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_fh"][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_fh"])) #DS
                
            httbar_SL_CR2 = ROOT.gDirectory.Get("httbar_SL_CR2")
            httbar_DL_CR2 = ROOT.gDirectory.Get("httbar_DL_CR2")
            httbar_FH_CR2 = ROOT.gDirectory.Get("httbar_FH_CR2")
            httbar_SL_CR2.Sumw2()
            httbar_DL_CR2.Sumw2()
            httbar_FH_CR2.Sumw2()
            httbar_SL_CR2.Scale(TT_scalefac["SL"])
            httbar_DL_CR2.Scale(TT_scalefac["DL"])
            httbar_FH_CR2.Scale(TT_scalefac["FH"])

            httbarCR2 = httbar_SL_CR2.Clone("httbarCR2")
            httbarCR2.Add(httbar_DL_CR2)
            httbarCR2.Add(httbar_FH_CR2)
            
            yttbarCR2 = httbarCR2.Integral()
            httbarCR2.SetFillColor(2)
            httbarCR2.SetLineWidth(0)
            yttbarCR2scale = httbarCR2.Integral(scalebins[0],scalebins[1])

            tTT["SL"].Draw(variable+">>httbar_SL_VR"+binning,"("+cutVR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["VR"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_sl"])) #DS
            tTT["DL"].Draw(variable+">>httbar_DL_VR"+binning,"("+cutVR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_dl"][jc][bc.replace("b","t")]["VR"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_dl"])) #DS
            tTT["FH"].Draw(variable+">>httbar_FH_VR"+binning,"("+cutVR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_fh"][jc][bc.replace("b","t")]["VR"] if "qgWeight" in wtTop else 1.0, idMap["TTbar_fh"])) #DS
            httbar_SL_VR = ROOT.gDirectory.Get("httbar_SL_VR")
            httbar_DL_VR = ROOT.gDirectory.Get("httbar_DL_VR")
            httbar_FH_VR = ROOT.gDirectory.Get("httbar_FH_VR")
            httbar_SL_VR.Sumw2()
            httbar_DL_VR.Sumw2()
            httbar_FH_VR.Sumw2()
            httbar_SL_VR.Scale(TT_scalefac["SL"])
            httbar_DL_VR.Scale(TT_scalefac["DL"])
            httbar_FH_VR.Scale(TT_scalefac["FH"])

            #HIER ADDEN
            httbarVR = httbar_SL_VR.Clone("httbarVR")
            httbarVR.Add(httbar_DL_VR)
            httbarVR.Add(httbar_FH_VR)
            
            yttbarVR = httbarVR.Integral() #DS
            httbarVR.SetFillColor(2)
            httbarVR.SetLineWidth(0)
            yttbarVRscale = httbarVR.Integral(scalebins[0],scalebins[1])

            bins = int(binning.split("(")[1].split(",")[0])
            low = float(binning.split(",")[1])
            high = float(binning.split(",")[2].split(")")[0])
            
            ybkgCR = 0.0
            ybkgCR2 = 0.0
            ybkgVR = 0.0
            if minorBkg:
                hbkgCRsam = {}
                hbkgCR = ROOT.TH1F("hbkgCR","",bins,low,high)
                for k in backgrounds:
                    if "ttH" in k: continue
                    print "doing", k
                    if CRcorrect:
                        tbkg[k].Draw(variable+">>hCR"+k+binning,"("+cutCR+")"+wt+btagcorrMC+"*{0}".format(qgWeightNorm[k][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wt else 1.0))
                    else:
                        tbkg[k].Draw(variable+">>hCR"+k+binning,"("+cutCR+")"+wt+"*{0}".format(qgWeightNorm[k][jc][bc.replace("b","t")]["CR"] if "qgWeight" in wt else 1.0))
                    hbkgCRsam[k] = ROOT.gDirectory.Get("hCR"+k)
                    ybkgCR += hbkgCRsam[k].Integral()*bkg_scalefac[k]
                    hbkgCR.Add(hbkgCRsam[k],bkg_scalefac[k])
                ybkgCRscale = hbkgCR.Integral(scalebins[0],scalebins[1])
                hbkgCR.SetFillColor(ROOT.kAzure+8)
                hbkgCR.SetLineWidth(0)
                    
                hbkgCR2sam = {}
                hbkgCR2 = ROOT.TH1F("hbkgCR2","",bins,low,high)
                for k in backgrounds:
                    if "ttH" in k: continue
                    if CRcorrect:
                        tbkg[k].Draw(variable+">>hCR2"+k+binning,"("+cutCR2+")"+wt+btagcorrMC+"*{0}".format(qgWeightNorm[k][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wt else 1.0))
                    else:
                        tbkg[k].Draw(variable+">>hCR2"+k+binning,"("+cutCR2+")"+wt+"*{0}".format(qgWeightNorm[k][jc][bc.replace("b","t")]["CR2"] if "qgWeight" in wt else 1.0))
                    hbkgCR2sam[k] = ROOT.gDirectory.Get("hCR2"+k)
                    ybkgCR2 += hbkgCR2sam[k].Integral()*bkg_scalefac[k]
                    hbkgCR2.Add(hbkgCR2sam[k],bkg_scalefac[k])
                ybkgCR2scale = hbkgCR2.Integral(scalebins[0],scalebins[1])
                hbkgCR2.SetFillColor(ROOT.kAzure+8)
                hbkgCR2.SetLineWidth(0)

                hbkgVRsam = {}
                hbkgVR = ROOT.TH1F("hbkgVR","",bins,low,high)
                for k in backgrounds:
                    if "ttH" in k and "x" in regions[0]: continue
                    tbkg[k].Draw(variable+">>hVR"+k+binning,"("+cutVR+")"+wt+"*{0}".format(qgWeightNorm[k][jc][bc.replace("b","t")]["VR"] if "qgWeight" in wt else 1.0))
                    hbkgVRsam[k] = ROOT.gDirectory.Get("hVR"+k)
                    ybkgVR += hbkgVRsam[k].Integral()*bkg_scalefac[k]
                    hbkgVR.Add(hbkgVRsam[k],bkg_scalefac[k])
                ybkgVRscale = hbkgVR.Integral(scalebins[0],scalebins[1])
                hbkgVR.SetFillColor(ROOT.kAzure+8)
                hbkgVR.SetLineWidth(0)

            if useMC:
                yqcdCR = 0
                hqcdCRbin = {}
                hqcdCR = ROOT.TH1F("hqcdCR","",bins,low,high)
                for k in qcd:
                    if CRcorrect:
                        tqcd[k].Draw(variable+">>hCR"+k+binning,"("+cutCR+")"+wt+btagcorrMC)
                    else:
                        tqcd[k].Draw(variable+">>hCR"+k+binning,"("+cutCR+")"+wt)
                    hqcdCRbin[k] = ROOT.gDirectory.Get("hCR"+k)
                    hqcdCRbin[k].Sumw2()
                    yqcdCR += hqcdCRbin[k].Integral()*qcd_scalefac[k] #DS
                    hqcdCR.Add(hqcdCRbin[k],qcd_scalefac[k])
                norm = (ydataCR-yttbarCR-ybkgCR2)/yqcdCR
                print "CR qcd normalisation: {0:0.2f}".format(norm)
                hqcdCR.Scale(norm)
                yqcdCRscale = hqcdCR.Integral(scalebins[0],scalebins[1])

                yqcdCR2 = 0
                hqcdCR2bin = {}
                hqcdCR2 = ROOT.TH1F("hqcdCR2","",bins,low,high)
                for k in qcd:
                    if CRcorrect:
                        tqcd[k].Draw(variable+">>hCR2"+k+binning,"("+cutCR2+")"+wt+btagcorrMC)
                    else:                    
                        tqcd[k].Draw(variable+">>hCR2"+k+binning,"("+cutCR2+")"+wt)
                    hqcdCR2bin[k] = ROOT.gDirectory.Get("hCR2"+k)
                    hqcdCR2bin[k].Sumw2()
                    yqcdCR2 += hqcdCR2bin[k].Integral()*qcd_scalefac[k] #DS
                    hqcdCR2.Add(hqcdCR2bin[k],qcd_scalefac[k])
                norm = (ydataCR2-yttbarCR2-ybkgCR2)/yqcdCR2
                print "CR2 qcd normalisation: {0:0.2f}".format(norm)
                hqcdCR2.Scale(norm)
                yqcdCR2scale = hqcdCR2.Integral(scalebins[0],scalebins[1])

                yqcdVR = 0
                hqcdVRbin = {}
                hqcdVR = ROOT.TH1F("hqcdVR","",bins,low,high)
                for k in qcd:
                    tqcd[k].Draw(variable+">>hVR"+k+binning,"("+cutVR+")"+wt)#*qcd_scalefac[k]
                    hqcdVRbin[k] = ROOT.gDirectory.Get("hVR"+k)
                    hqcdVRbin[k].Sumw2()
                    yqcdVR += hqcdVRbin[k].Integral()*qcd_scalefac[k] #DS
                    hqcdVR.Add(hqcdVRbin[k],qcd_scalefac[k])
                norm = (ydataVR-yttbarVR-ybkgVR)/yqcdVR
                print "VR qcd normalisation: {0:0.2f}".format(norm)
                hqcdVR.Scale(norm)
                yqcdVRscale = hqcdVR.Integral(scalebins[0],scalebins[1])
            else:
                yqcdCR = ydataCR - yttbarCR - ybkgCR
                yqcdCRnorm = ydataCRnorm - yttbarCRnorm
                print "yqcdCR/CRnorm", yqcdCR,yqcdCRnorm
                if minorBkg:
                    hqcdCR = hdataCR - httbarCR - hbkgCR
                    hqcdCR2 = hdataCR2 - httbarCR2 - hbkgCR2
                    hqcdVR = hdataVR - httbarVR - hbkgVR
                else:
                    hqcdCR = hdataCR - httbarCR
                    hqcdCR2 = hdataCR2 - httbarCR2
                    hqcdVR = hdataVR - httbarVR
                yqcdCR2 = ydataCR2 - yttbarCR2 - ybkgCR2
                yqcdVR = ydataVR - yttbarVR - ybkgVR
                yqcdCRscale = ydataCRscale - yttbarCRscale - (ybkgCRscale if minorBkg else 0)
                yqcdCR2scale = ydataCR2scale - yttbarCR2scale - (ybkgCR2scale if minorBkg else 0)
                yqcdVRscale = ydataVRscale - yttbarVRscale - (ybkgVRscale if minorBkg else 0)

            hqcdCR.SetTitle( CR+" "+cat+";"+var+";Events" )
            hqcdCR.SetFillColor(colors["ddQCD"])
            hqcdCR.SetLineWidth(0)

            hqcdCR2.SetFillColor(colors["ddQCD"])
            hqcdCR2.SetLineWidth(0)

            hqcdVR.SetFillColor(colors["ddQCD"])
            hqcdVR.SetLineWidth(0)

            hstackCR = ROOT.THStack ("hstackCR", CR+" "+cat+";"+var+";Events")
            if validation and "alpha" in method:
                hstackCR.Add(hqcdCR2)
                hstackCR.Add(httbarCR2)
                if minorBkg:
                    hstackCR.Add(hbkgCR2)
                hstackCR.Draw("hist")
                hdataCR2.Draw("pesame")
                leg2.AddEntry(hqcdCR2, "data-t#bar{t} "+"({0:0.0f})".format(hqcdCR2.Integral()), "F")
                leg2.AddEntry(httbarCR2, "t#bar{t} "+"({0:0.0f})".format(httbarCR2.Integral()), "F")
                if minorBkg:
                    leg2.AddEntry(hbkgCR2, "minor bkgs. "+"({0:0.0f})".format(hbkgCR2.Integral()), "F")
                leg2.AddEntry(hdataCR2, "data ({0:0.0f})".format(hdataCR2.Integral()), "PLE")
            elif method=="alphaVR":
                hstackCR.Add(hqcdVR)
                hstackCR.Add(httbarVR)
                if minorBkg:
                    hstackCR.Add(hbkgVR)
                hstackCR.Draw("hist")
                hdataVR.Draw("pesame")
                leg2.AddEntry(hqcdVR, "data-t#bar{t} "+"({0:0.0f})".format(hqcdVR.Integral()), "F")
                leg2.AddEntry(httbarVR, "t#bar{t} "+"({0:0.0f})".format(httbarVR.Integral()), "F")
                if minorBkg:
                    leg2.AddEntry(hbkgVR, "minor bkgs. "+("+ ttH " if incSignal else "")+"({0:0.0f})".format(hbkgVR.Integral()), "F")
                leg2.AddEntry(hdataVR, "data ({0:0.0f})".format(hdataVR.Integral()), "PLE")
            else:
                hstackCR.Add(hqcdCR)
                hstackCR.Add(httbarCR)
                if minorBkg:
                    hstackCR.Add(hbkgCR)
                hstackCR.Draw("hist")
                hdataCR.Draw("pesame")
                leg2.AddEntry(hqcdCR, "data-t#bar{t} "+"({0:0.0f})".format(hqcdCR.Integral()), "F")
                leg2.AddEntry(httbarCR, "t#bar{t} "+"({0:0.0f})".format(httbarCR.Integral()), "F")
                if minorBkg:
                    leg2.AddEntry(hbkgCR, "minor bkgs. "+"({0:0.0f})".format(hbkgCR.Integral()), "F")
                leg2.AddEntry(hdataCR, "data ({0:0.0f})".format(hdataCR.Integral()), "PLE")
            leg2.Draw()
            p2.Update()

            if saveplots: c2.SaveAs(destination+"/stack_"+cat+"_"+CR+"_"+var+tag+".pdf")

            c3 = ROOT.TCanvas("c3","",5,30,640,580)
            p3 = ROOT.TPad("p3","",0,0,1,1)
            p3.SetTopMargin(0.08)
            p3.SetRightMargin(0.05)
            p3.Draw()
            p3.cd()

            leg3 = ROOT.TLegend(lp[0],lp[1],lp[2],lp[3])
            leg3.SetFillStyle(0)
            leg3.SetBorderSize(0)
            leg3.SetTextSize(0.03)

            tdata.Draw(variable+">>hdataSR"+binning,cutSR+(blindcut if unblind else ""))
            ydataSRnorm = tdata.Draw("",cutSR+normcut)
            hdataSR = ROOT.gDirectory.Get("hdataSR")
            hdataSR.SetLineColor(1)
            hdataSR.SetMarkerColor(1)
            hdataSR.SetMarkerStyle(20)
            hdataSR.SetMarkerSize(1)
            hdataSR.Sumw2()
            ydataSR = hdataSR.Integral()
            ydataSRscale = hdataSR.Integral(scalebins[0],scalebins[1])

            tTT["SL"].Draw(variable+">>httbar_SL_SR"+binning,"("+cutSR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_sl"][jc][bc.replace("b","t")]["SR"] if "qgWeight" in wtTop else 1.0)) #DS
            tTT["DL"].Draw(variable+">>httbar_DL_SR"+binning,"("+cutSR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_dl"][jc][bc.replace("b","t")]["SR"] if "qgWeight" in wtTop else 1.0)) #DS
            tTT["FH"].Draw(variable+">>httbar_FH_SR"+binning,"("+cutSR+")"+wtTop+"*{0}".format(qgWeightNorm["TTbar_fh"][jc][bc.replace("b","t")]["SR"] if "qgWeight" in wtTop else 1.0)) #DS
            tTT["SL"].Draw(variable+">>httbar_SL_SRnorm"+binning,"("+cutSR+normcut+")"+wt) #DS
            tTT["DL"].Draw(variable+">>httbar_DL_SRnorm"+binning,"("+cutSR+normcut+")"+wt) #DS
            tTT["FH"].Draw(variable+">>httbar_FH_SRnorm"+binning,"("+cutSR+normcut+")"+wt) #DS
            httbar_SL_SR = ROOT.gDirectory.Get("httbar_SL_SR")
            httbar_DL_SR = ROOT.gDirectory.Get("httbar_DL_SR")
            httbar_FH_SR = ROOT.gDirectory.Get("httbar_FH_SR")
            httbar_SL_SRnorm = ROOT.gDirectory.Get("httbar_SL_SRnorm") #DS
            httbar_DL_SRnorm = ROOT.gDirectory.Get("httbar_DL_SRnorm") #DS
            httbar_FH_SRnorm = ROOT.gDirectory.Get("httbar_FH_SRnorm") #DS
            httbar_SL_SR.Sumw2()
            httbar_DL_SR.Sumw2()
            httbar_FH_SR.Sumw2()
            httbar_SL_SR.Scale(TT_scalefac["SL"])
            httbar_DL_SR.Scale(TT_scalefac["DL"])
            httbar_FH_SR.Scale(TT_scalefac["FH"])
            httbar_SL_SRnorm.Scale(TT_scalefac["SL"]) #DS
            httbar_DL_SRnorm.Scale(TT_scalefac["DL"]) #DS
            httbar_FH_SRnorm.Scale(TT_scalefac["FH"]) #DS

            httbarSR = httbar_SL_SR.Clone("httbarSR")
            httbarSR.Add(httbar_DL_SR)
            httbarSR.Add(httbar_FH_SR)

            yttbarSR = httbarSR.Integral() #DS
            #yttbarSRnorm = httbarSRnorm.Integral() #DS
            httbarSR.SetFillColor(2)
            httbarSR.SetLineWidth(0)
            yttbarSRscale = httbarSR.Integral(scalebins[0],scalebins[1])

            ybkgSR = 0.0
            histos2Save = {}
            if minorBkg:
                hbkgSRsam = {}
                hbkgSR = ROOT.TH1F("hbkgSR","",bins,low,high)
                for k in backgrounds:
                    tbkg[k].Draw(variable+">>hSR"+k+binning,"("+cutSR+")"+wt+"*{0}".format(qgWeightNorm[k][jc][bc.replace("b","t")]["SR"] if "qgWeight" in wt else 1.0))
                    hbkgSRsam[k] = ROOT.gDirectory.Get("hSR"+k)
                    histos2Save[k] = ROOT.gDirectory.Get("hSR"+k).Clone()
                    ybkgSR += hbkgSRsam[k].Integral()*bkg_scalefac[k]
                    hbkgSR.Add(hbkgSRsam[k],bkg_scalefac[k])
                ybkgSRscale = hbkgSR.Integral(scalebins[0],scalebins[1])
                hbkgSR.SetFillColor(ROOT.kAzure+8)
                hbkgSR.SetLineWidth(0)

            if useMC:
                yqcdSR = 0
                hqcdSRbin = {}
                hqcdSR = ROOT.TH1F("hqcdSR","",bins,low,high)
                for k in qcd:
                    tqcd[k].Draw(variable+">>hSR"+k+binning,"("+cutSR+")"+wt)#*qcd_scalefac[k]
                    hqcdSRbin[k] = ROOT.gDirectory.Get("hSR"+k)
                    hqcdSRbin[k].Sumw2()
                    yqcdSR += hqcdSRbin[k].Integral()*qcd_scalefac[k] #DS
                    hqcdSR.Add(hqcdSRbin[k],qcd_scalefac[k])
                norm = (ydataSR-yttbarSR)/yqcdSR
                print "SR qcd normalisation: {0:0.2f}".format(norm)
                hqcdSR.Scale(norm)
                yqcdSRscale = hqcdSR.Integral(scalebins[0],scalebins[1])
            else: #usedata
                yqcdSR = ydataSR - yttbarSR - ybkgSR
                #yqcdSRnorm = ydataSRnorm - yttbarSRnorm
                #print "yqcdSR/SRnorm", yqcdSR,yqcdSRnorm
                if minorBkg:
                    hqcdSR = hdataSR - httbarSR - hbkgSR
                else:
                    hqcdSR = hdataSR - httbarSR
                yqcdSRscale = ydataSRscale - yttbarSRscale - (ybkgSRscale if minorBkg else 0)
                print "yqcdSR/SRscale", yqcdSR,yqcdSRscale

            hqcdSR.SetTitle(SR+" "+cat+";"+var+";Events")
            hqcdSR.SetFillColor(colors["ddQCD"])
            hqcdSR.SetLineWidth(0)
            # hqcdSR.SetLineColor(9)
            # hqcdSR.SetMarkerColor(9)
            # hqcdSR.SetMarkerStyle(20)
            # hqcdSR.SetMarkerSize(1)
            # hqcdSR.Draw("hist")
            # p3.Update()

            hstackSR = ROOT.THStack ("hstackSR",SR+" "+cat+";"+var+";Events")
            if validation and method=="alphaVR":
                 hstackSR.Add(hqcdCR)
                 hstackSR.Add(httbarCR)
                 if minorBkg:
                    hstackSR.Add(hbkgCR)
                 hstackSR.Draw("hist")
                 hdataCR.Draw("pesame")
                 leg3.AddEntry(hqcdCR, "data-t#bar{t} "+"({0:0.0f})".format(hqcdCR.Integral()), "F")
                 leg3.AddEntry(httbarCR, "t#bar{t} "+"({0:0.0f})".format(httbarCR.Integral()), "F")
                 if minorBkg:
                     leg3.AddEntry(hbkgCR, "minor bkgs. "+"({0:0.0f})".format(hbkgCR.Integral()), "F")
                 leg3.AddEntry(hdataCR, "data ({0:0.0f})".format(hdataCR.Integral()), "PLE")
            elif validation and method=="alphaCR":
                hstackSR.Add(hqcdVR)
                hstackSR.Add(httbarVR)
                if minorBkg:
                    hstackSR.Add(hbkgVR)
                hstackSR.Draw("hist")
                hdataVR.Draw("pesame")
                leg3.AddEntry(hqcdVR, "data-t#bar{t} "+"({0:0.0f})".format(hqcdVR.Integral()), "F")
                leg3.AddEntry(httbarVR, "t#bar{t} "+"({0:0.0f})".format(httbarVR.Integral()), "F")
                if minorBkg:
                     leg3.AddEntry(hbkgVR, "minor bkgs. "+("+ ttH " if incSignal else "")+"({0:0.0f})".format(hbkgVR.Integral()), "F")
                leg3.AddEntry(hdataVR, "data ({0:0.0f})".format(hdataVR.Integral()), "PLE")
            else:
                hstackSR.Add(hqcdSR)
                hstackSR.Add(httbarSR)
                if minorBkg:
                    hstackSR.Add(hbkgSR)
                hstackSR.Draw("hist")
                if unblind:
                    hdataSR.Draw("pesame")
                leg3.AddEntry(hqcdSR, "data-t#bar{t} "+"({0:0.0f})".format(hqcdSR.Integral()), "F")
                leg3.AddEntry(httbarSR, "t#bar{t} "+"({0:0.0f})".format(httbarSR.Integral()), "F")
                if minorBkg:
                     leg3.AddEntry(hbkgSR, "minor bkgs. "+("+ ttH " if incSignal else "")+"({0:0.0f})".format(hbkgSR.Integral()), "F")
                if unblind:
                    leg3.AddEntry(hdataSR, "data ({0:0.0f})".format(hdataSR.Integral()), "PLE")
            leg3.Draw()
            p3.Update()

            if saveplots: c3.SaveAs(destination+"/stack_"+cat+"_"+SR+"_"+var+tag+".pdf")

            ########## closure test #########

            c4 = ROOT.TCanvas("c4","",5,30,600,540)
            p4 = ROOT.TPad("p4","",0,0.26,1,1)
            p4.SetTopMargin(0.08)
            p4.SetRightMargin(0.04)
            p4.SetLeftMargin(0.15)
            p4.SetBottomMargin(0.025)
            p4.SetTicks()
            p4.Draw()
            p4.cd()

            leg4 = ROOT.TLegend(lp[0],lp[1]-0.08-0.06,lp[2],lp[3]) #extra for chi2, extra for p-value
            leg4.SetFillStyle(0)
            leg4.SetBorderSize(0)
            leg4.SetTextSize(0.05)

            if validation and method=="alphaVR":
                hqcd_check = hqcdCR2.Clone()
                alpha = hqcdCR.Integral()/hqcdCR2.Integral() if useMC else (yqcdCRscale/yqcdCR2scale if memscale else yqcdCR/yqcdCR2)
                datamax = hdataCR.GetMaximum()
                print "taking QCD from CR2 to CR"
            elif validation and method=="alphaCR":
                hqcd_check = hqcdCR2.Clone()
                alpha = hqcdVR.Integral()/hqcdCR2.Integral() if useMC else (yqcdVRscale/yqcdCR2scale if memscale else yqcdVR/yqcdCR2)
                datamax = hdataVR.GetMaximum()
                print "taking QCD from CR2 to VR"
            elif method=="alphaVR":
                hqcd_check = hqcdVR.Clone()
                alpha = hqcdSR.Integral()/hqcdVR.Integral() if useMC else (yqcdSRscale/yqcdVRscale if memscale else yqcdSR/yqcdVR)
                datamax = hdataSR.GetMaximum()
                print "taking QCD from VR to SR"
            else:
                hqcd_check = hqcdCR.Clone()
                alpha = hqcdSR.Integral()/hqcdCR.Integral() if useMC else (yqcdSRscale/yqcdCRscale if memscale else yqcdSR/yqcdCR)
                datamax = hdataSR.GetMaximum()
                print "taking QCD from CR to SR"

            if "alpha" in method:
                hqcd_check.Scale(alpha)
                print "alpha ratio is: {0:0.2f}".format( alpha )
                if memscale and memcorrect:
                    if bc=="3b":
                        bincont = corrfac[0]*hqcd_check.GetBinContent(1)
                        binerr = corrfac[0]*hqcd_check.GetBinError(1)
                    elif bc=="4b":
                        bincont = corrfac[1]*hqcd_check.GetBinContent(1)
                        binerr = corrfac[1]*hqcd_check.GetBinError(1)
                    hqcd_check.SetBinContent(1,bincont)
                    hqcd_check.SetBinError(1,binerr)
            else:
                for i in range(hqcdCR.GetXaxis().GetNbins()+2):
                    cr = hqcdCR.GetBinContent(i)
                    cr2 = hqcdCR2.GetBinContent(i)
                    vr = hqcdVR.GetBinContent(i)
                    sr = cr*vr/cr2 if cr2>0 else 0.0

                    err_cr = hqcdCR.GetBinError(i)
                    err_cr2 = hqcdCR2.GetBinError(i)
                    err_vr = hqcdVR.GetBinError(i)
                    err_sr = sr * ( ( (err_vr/vr)**2 if vr>0 else 0 ) +
                                    ( (err_cr/cr)**2 if cr>0 else 0 ) +
                                    ( (err_cr2/cr2)**2 if cr2>0 else 0 ) )**0.5

                    hqcd_check.SetBinContent(i, sr)
                    hqcd_check.SetBinError(i, err_sr)
                if useMC:
                    hqcd_check.Scale( hqcdSR.Integral()/hqcd_check.Integral() )

            herr_check = hqcd_check.Clone()
            hstack_check = ROOT.THStack ("hstack_check",SR+" "+cat+" check;"+var+";Events")
            hstack_check.Add(hqcd_check)
            if not useMC:
                if SR=="SR":
                    herr_check.Add(httbarSR)
                    hstack_check.Add(httbarSR)
                    if minorBkg:
                        herr_check.Add(hbkgSR)
                        hstack_check.Add(hbkgSR)
                elif SR=="CR":
                    herr_check.Add(httbarCR)
                    hstack_check.Add(httbarCR)
                    if minorBkg:
                        herr_check.Add(hbkgCR)
                        hstack_check.Add(hbkgCR)
                elif SR=="VR":
                    herr_check.Add(httbarVR)
                    hstack_check.Add(httbarVR)
                    if minorBkg:
                        herr_check.Add(hbkgVR)
                        hstack_check.Add(hbkgVR)
            herr_check.SetFillColor( ROOT.kBlack )
            herr_check.SetMarkerStyle(0)
            herr_check.SetFillStyle( 3354 )

            herr_check.SetMaximum( max(herr_check.GetMaximum(),datamax)*1.2 )
            herr_check.SetMinimum( 0 )  
            #herr_check.SetTitle( SR+" "+cat+" check;"+var+";Events" )
            if varname in vars_dict:
                herr_check.SetTitle(";;"+vars_dict[varname][1])
            else:
                herr_check.SetTitle(";;Events / bin")
            #herr_check.GetYaxis().SetTitleFont(62)
            herr_check.GetYaxis().SetTitleSize(0.069)
            #herr_check.GetYaxis().SetLabelFont(62)
            herr_check.GetYaxis().SetLabelSize(0.060)
            herr_check.GetYaxis().SetTitleOffset(1.2)
            herr_check.GetXaxis().SetTitleSize(0)
            herr_check.GetXaxis().SetLabelSize(0)

            herr_check.Draw("e2")
            hstack_check.Draw("histsame")
            hdata = 0 #"data" or qcd for points and ratio plot
            if not useMC:
                if SR=="SR" and unblind:
                    hdata = hdataSR.Clone()
                elif SR=="VR":
                    hdata = hdataVR.Clone()
                elif SR=="CR":
                    hdata = hdataCR.Clone()
            else:
                if SR=="SRmc":
                    hdata = hqcdSR.Clone()
                elif SR=="VRmc":
                    hdata = hqcdVR.Clone()
                elif SR=="CRmc":
                    hdata = hqcdCR.Clone()

            if not hdata:
                hdata = herr_check.Clone()
                hdata.SetLineColor(1)
                hdata.SetMarkerColor(1)
                hdata.SetMarkerStyle(20)
                hdata.SetMarkerSize(1)
            if useMC:
                hdata.SetLineWidth(1)
                hdata.SetLineColor(9)
                hdata.SetMarkerColor(9)
                hdata.SetMarkerStyle(20)
                hdata.SetMarkerSize(1)

            setex2 = ROOT.TExec("setex2","gStyle->SetErrorX(0.5)")
            setex2.Draw()
            herr_check.Draw("e2same")
            setex1 = ROOT.TExec("setex2","gStyle->SetErrorX(0)")
            if useMC or validation or unblind:
                setex1.Draw()
                hdata.Draw("pesame")
            herr_check.Draw("sameaxis") #redraws the axes
            if useMC:
                leg4.AddEntry(hqcd_check, "MC QCD "+"({0:0.0f})".format(hqcd_check.Integral()), "F")
            else:
                leg4.AddEntry(hqcd_check, "Multijet "+"({0:0.0f})".format(hqcd_check.Integral()), "F")
                if SR=="SR":
                    leg4.AddEntry(httbarSR, "t#bar{t} "+"({0:0.0f})".format(httbarSR.Integral()), "F")
                    if minorBkg:
                        leg4.AddEntry(hbkgSR, "Minor bkgs"+("+t#bar{t}H " if incSignal else "")+"({0:0.0f})".format(hbkgSR.Integral()), "F")
                elif SR=="VR":
                    leg4.AddEntry(httbarVR, "t#bar{t} "+"({0:0.0f})".format(httbarVR.Integral()), "F")
                    if minorBkg:
                        leg4.AddEntry(hbkgVR, "Minor bkgs"+("+t#bar{t}H " if incSignal else "")+"({0:0.0f})".format(hbkgVR.Integral()), "F")
                elif SR=="CR":
                    leg4.AddEntry(httbarCR, "t#bar{t} "+"({0:0.0f})".format(httbarCR.Integral()), "F")
                    if minorBkg:
                        leg4.AddEntry(hbkgCR, "Minor bkgs "+"({0:0.0f})".format(hbkgCR.Integral()), "F")
            
            if useMC:
                if SR=="SRmc":
                    leg4.AddEntry(hdata, "SR QCD ({0:0.0f})".format(hdata.Integral()), "PE")
                elif SR=="VRmc":
                    leg4.AddEntry(hdata, "VR QCD ({0:0.0f})".format(hdata.Integral()), "PE")
                elif SR=="CRmc":
                    leg4.AddEntry(hdata, "CR QCD ({0:0.0f})".format(hdata.Integral()), "PE")
            else:
                if SR=="SR" and unblind:
                    leg4.AddEntry(hdataSR, "Data ({0:0.0f})".format(hdataSR.Integral()), "PE")
                elif SR=="VR":
                    leg4.AddEntry(hdataVR, "Data ({0:0.0f})".format(hdataVR.Integral()), "PE")
                elif SR=="CR":
                    leg4.AddEntry(hdataCR, "Data ({0:0.0f})".format(hdataCR.Integral()), "PE")

            leg4.Draw()
            p4.Update()

            c4.cd()
            p4r = ROOT.TPad("p4","",0,0,1,0.26)
            p4r.SetRightMargin(0.04)
            p4r.SetLeftMargin(0.15)
            p4r.SetTopMargin(0.04)
            p4r.SetBottomMargin(0.42)
            p4r.SetTicks()
            p4r.Draw()
            p4r.cd()

            xmin = float(binning.split(",")[1])
            xmax = float(binning.split(",")[2].split(")")[0])
            if drawPulls:
                line = "0"
            else:
                line = "1"
            one = ROOT.TF1("one",line,xmin,xmax)
            one.SetLineColor(1)
            one.SetLineStyle(2)
            one.SetLineWidth(1)

            nxbins = herr_check.GetNbinsX()
            herr = herr_check.Clone()
            herr.SetFillColor( 16 )
            herr.SetFillStyle( 1001 )

            #residuals = [0.0]*(nxbins+2)
            #rootChi2 = 0
            chi2 = 0.0
            if hdata:
                hratio = hdata.Clone()
            else:
                hdata = hqcd_check.Clone()
                hratio = hdata.Clone()
            for b in range(nxbins):
                nbkg = herr_check.GetBinContent(b+1)
                ebkg = herr_check.GetBinError(b+1)
                ndata = hdata.GetBinContent(b+1)
                edata = hdata.GetBinError(b+1)
                #rootChi2 = hqcd_check.Chi2Test(hqcdSR,"CHI2/NDF" ,residuals)
                
                r = ndata / nbkg if nbkg>0 else 0
                rerr = edata / nbkg if nbkg>0 else 0
                p = (ndata-nbkg)/(edata**2+ebkg**2)**0.5 if (edata+ebkg)>0 else 0
                perr = 1.0
                chi2 += p*p
                
                if drawPulls:
                    hratio.SetBinContent(b+1, p)
                    hratio.SetBinError(b+1,perr)
                else:
                    hratio.SetBinContent(b+1, r)
                    hratio.SetBinError(b+1,rerr)
                    
                herr.SetBinContent(b+1, float(line))
                herr.SetBinError(b+1, ebkg/nbkg*1.0 if nbkg>0 else 0 )

            pval = 1.0 - stats.chi2.cdf(chi2, nxbins-1)
            chi2 /= (1.0*(nxbins-1))

            if drawPulls:
                hmax = int(hratio.GetMaximum()+0.99999)
                if hmax>100:
                    hmax = 1
                hmin = int(hratio.GetMinimum()-0.99999)
                if hmin==0:
                    hmin = -1
                hratio.GetYaxis().SetRangeUser(hmin,hmax)
            else:
                # hmax = int(hratio.GetMaximum()*10+0.99)*0.1
                # if hmax>100:
                #     hmax = 1
                # hmin = int(hratio.GetMinimum()*10-0.99)*0.1
                # if hmin<0:
                #     hmin = 0
                # hratio.GetYaxis().SetRangeUser(hmin,hmax)
                if useMC:
                    hratio.GetYaxis().SetRangeUser(0.5,1.5)
                elif bc=="3b":
                    hratio.GetYaxis().SetRangeUser(0.7,1.3)
                else:
                    hratio.GetYaxis().SetRangeUser(0.5,1.5)

            hratio.SetTitle("")
            if varname in vars_dict:
                hratio.GetXaxis().SetTitle( vars_dict[varname][0] )
            else:
                hratio.GetXaxis().SetTitle( varname )
            hratio.GetXaxis().SetTitleSize(0.196)
            hratio.GetXaxis().SetLabelSize(0.171)
            hratio.GetXaxis().SetTickLength(0.09)
            #hratio.GetXaxis().SetDecimals(1)
            hratio.GetYaxis().SetTitleSize(0.196)
            hratio.GetYaxis().SetLabelSize(0.171)
            hratio.GetYaxis().SetTitleOffset(0.40)
            if drawPulls:
                #hratio.GetYaxis().SetTitle(" (data-N)/#sigma_{tot}")
                #hratio.GetYaxis().SetTitle("  pull (#sigma_{tot})")
                hratio.GetYaxis().SetTitle("pull  ")
                divisions = (hmax-hmin)*100+1
                hratio.GetYaxis().SetNdivisions(divisions)
            else:
                #hratio.GetYaxis().SetTitle("ratio    ")
                hratio.GetYaxis().SetTitle("      Data/Bkg")
                hratio.GetYaxis().SetNdivisions(202)

            hratio.Draw("pe")
            if not drawPulls:
                setex2.Draw() #turn on horizontal error bars
                herr.Draw("e2same")
            one.Draw("SAME")
            setex1.Draw() #turn off horizontal error bars
            hratio.Draw("PEsame")
            hratio.Draw("PE0,X0same") #draw off scale error bars
            hratio.Draw("sameaxis") #redraws the axes
            #hratio.Draw("pesame")
            p4r.Update()

            leg4.AddEntry( ROOT.TObject(), "#chi^{2}/dof = "+"{0:0.1f}".format(chi2), "")
            leg4.AddEntry( ROOT.TObject(), "p-value = {0:0.3f}".format(pval), "")
            p4.cd()
            leg4.Draw()

            #draw category
            text = ROOT.TLatex()
            text.SetTextSize(0.056)
            text.SetTextAlign(11) #31=right align
            text.SetTextFont(42)
            text.DrawLatex(low+(high-low)*0.62,
                           herr_check.GetMaximum()*0.9,
                           label_dict[jc+bc])

            if varname in paperVars:
                label = "Data"
            else:
                label = "DataSupp"
            box = Helper.create_paves(lumi, label, CMSposX=CMSposX, CMSposY=CMSposY, 
                          prelimPosX=prelimPosX, prelimPosY=prelimPosY,
                          lumiPosX=0.977, lumiPosY=0.88, alignRight=False,
                          CMSsize=0.075, prelimSize=0.057, lumiSize=0.060)
            box["lumi"].Draw()
            box["CMS"].Draw()
            box["label"].Draw()
            p4.Update()

            if saveplots: c4.SaveAs(destination+"/check_"+cat+"_"+SR+"_"+var+tag+".pdf")
            if savehistos:
                ofile = ROOT.TFile(destination+"/check_"+cat+"_"+SR+"_"+var+tag+".root","UPDATE")
                ofile.cd()
                hqcd_check.Write("ddQCD", ROOT.TObject.kOverwrite)
                herr_check.Write("total", ROOT.TObject.kOverwrite)
                hstack_check.Write("", ROOT.TObject.kOverwrite)
                if not useMC:
                    if SR=="SR":
                        httbarSR.Write("", ROOT.TObject.kOverwrite)
                        if unblind:
                            hdataSR.Write("", ROOT.TObject.kOverwrite)
                        if minorBkg:
                            hbkgSR.Write("", ROOT.TObject.kOverwrite)
                            for k in backgrounds:
                                print k
                                histos2Save[k].SetName(histos2Save[k].GetName()+"_"+k)
                                histos2Save[k].Write("", ROOT.TObject.kOverwrite)
                    elif SR=="CR":
                        httbarCR.Write("", ROOT.TObject.kOverwrite)
                        hdataCR.Write("", ROOT.TObject.kOverwrite)
                        if minorBkg:
                            hbkgCR.Write("", ROOT.TObject.kOverwrite)
                    elif SR=="VR":
                        httbarVR.Write("", ROOT.TObject.kOverwrite)
                        hdataVR.Write("", ROOT.TObject.kOverwrite)
                        if minorBkg:
                            hbkgVR.Write("", ROOT.TObject.kOverwrite)
                else:
                    hdata.Write("", ROOT.TObject.kOverwrite)
                ofile.Close()

            if saveCRRoot:
                ofile = ROOT.TFile(destination+"/Histos_"+cat+"_"+var+tag+".root","UPDATE")
                
                hqcdCR2.Write("", ROOT.TObject.kOverwrite)
                httbarCR2.Write("", ROOT.TObject.kOverwrite)
                hdataCR2.Write("", ROOT.TObject.kOverwrite)
                hqcdCR.Write("", ROOT.TObject.kOverwrite)
                httbarCR.Write("", ROOT.TObject.kOverwrite)
                hdataCR.Write("", ROOT.TObject.kOverwrite)
                
                hqcdVR.Write("", ROOT.TObject.kOverwrite)
                httbarVR.Write("", ROOT.TObject.kOverwrite)
                hdataVR.Write("", ROOT.TObject.kOverwrite)
                hqcdSR.Write("", ROOT.TObject.kOverwrite)
                httbarSR.Write("", ROOT.TObject.kOverwrite)
                hdataSR.Write("", ROOT.TObject.kOverwrite)

                
                ofile.Close()

            
            
            print "Region data ttbar qcd"
            print "CR",ydataCR,yttbarCR,yqcdCR
            print "SR",ydataSR,yttbarSR,yqcdSR
            print "CR2",ydataCR2,yttbarCR2,yqcdCR2
            print "VR",ydataVR,yttbarVR,yqcdVR
            print "---------------------------------"
            for ibin in range(hdataVR.GetNbinsX()+2):
                print "VR bin {0} : {1}".format(ibin,hdataVR.GetBinContent(ibin) )
            print "---------------------------------"
            print "chi_sq.", chi2
            print "p-value", pval
            #print "rootChi2", rootChi2

            #b = raw_input('done '+varname+': press Enter for next variable, q to quit ')
            #if b=="q": quit()
            sleep(1)

        #end loop over variables
        if not saveplots and not savehistos:
            q = raw_input('done: press Enter for next category, q to quit ')
            if q=="q": quit()

#end loop over categories
#raw_input('done: press Enter to quit')
sleep(5)
