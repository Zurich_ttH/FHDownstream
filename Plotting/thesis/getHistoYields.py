import ROOT
import sys
import glob
from collections import OrderedDict

def yields(file_):
    rFile = ROOT.TFile.Open(file_)

    for key in rFile.GetListOfKeys():
        keyName = key.GetName()
        print "  {:15} \t {:10.3f}".format( keyName, rFile.Get(keyName).Integral())
        
def processAllFromFolder(basePath, wildcard="*.root"):
    for f in glob.glob(basePath+"/"+wildcard):
        print(f)
        yields(f)

if __name__ == "__main__":
    baseFolder = sys.argv[1]

    processAllFromFolder(baseFolder)
