from __future__ import print_function, division

import sys, os
from math import sqrt
import logging
from copy import copy, deepcopy

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging, makeCanvasOfHistos, saveCanvasListAsPDF
from classes.PLotDrawers import makeCanvasOfHistosCustomLeg
from classes.ratios import RatioPlot


def main(inputFile, tag, proc, variables, names, lumi):
    rFile = ROOT.TFile.Open(inputFile)
    logging.info("File: %s", inputFile)
    allKeys = [key.GetName() for key in rFile.GetListOfKeys()]

    allProcs = []
    allVars = []
    allCats = []
    for key in allKeys:
        elem = key.split("__")
        if elem[0] not in allProcs:
            if elem[0] == proc:
                allProcs.append(elem[0])
        if elem[1] not in allCats:
            allCats.append(elem[1])
        if elem[2] not in allVars:
            if elem[2] in variables:
                allVars.append(elem[2])

    logging.info("Found processes: %s", allProcs)
    logging.info("Found variables: %s", allVars)
    logging.info("Found categories: %s", allCats)
    
    for cat in allCats:
        logging.info("Processing categories: %s", cat)
        for proc in allProcs:
            if proc == "data":
                logging.warning("Skipping data")
                continue
            logging.info("Processing process: %s", proc)
            canvases = []
            for ivar, var in enumerate(allVars):
                logging.debug("Processing variable: %s", var)
                nominalHistoName = "{}__{}__{}".format(proc, cat, var)
                logging.debug("Nominal Histo Name: %s", nominalHistoName)
                if nominalHistoName not in allKeys:
                    logging.warning("Skipping %s", nominalHistoName)
                    continue
                nomHisto = rFile.Get(nominalHistoName)
                logging.debug("Nominal histogram: %s", nomHisto)
                systHistos = [key for key in allKeys if key.startswith(nominalHistoName) and key is not nominalHistoName]


                # Radiuation plot:
                ISRUpName =  "{}__{}__{}__{}".format(proc, cat, var, "CMS_ttHbb_ISRUp")
                ISRDownName =  "{}__{}__{}__{}".format(proc, cat, var, "CMS_ttHbb_ISRDown")
                FSRUpName =  "{}__{}__{}__{}".format(proc, cat, var, "CMS_ttHbb_FSRUp")
                FSRDownName =  "{}__{}__{}__{}".format(proc, cat, var, "CMS_ttHbb_FSRDown")

                radiationRatio = RatioPlot("Radiation_{}_{}_{}".format(cat, proc, var))
                radiationRatio.passHistos(
                    [nomHisto,
                     rFile.Get(ISRUpName),
                     rFile.Get(ISRDownName),
                     rFile.Get(FSRUpName),
                     rFile.Get(FSRDownName)],
                    normalize=True,
                )
                radiationRatio.ratioText = "#frac{Nominal}{Systematic}"
                radiationRatio.thisCMSLabel = "Simulation"
                radiationRatio.thisLumi = lumi
                radiationRatio.moveCMSTop = 0.085
                radiationRatio.ratioRange = (0.6,1.4)
                radiationRatio.CMSLeft = True
                radiationRatio.CMSOneLine = True
                radiationRatio.useDefaultColors = True
                radiationRatio.defaultColors = [ROOT.kBlack, ROOT.kRed, ROOT.kMagenta+1, ROOT.kBlue, ROOT.kCyan+1]
                radiationRatio.legendSize = (0.6,0.6,0.9,0.9)
                canvases.append(
                    radiationRatio.drawPlot(
                        ["t#bar{t} Nominal",
                         "t#bar{t} ISR Up","t#bar{t} ISR Down",
                         "t#bar{t} FSR Up","t#bar{t} FSR Down"],
                        xTitle = names[ivar],
                        logscale = False,
                        noErr = True,
                        drawCMS = True
                    )
                )
            
            saveCanvasListAsPDF(canvases, "{}_{}_radiation".format(cat, proc), "out_thesis_unc")
            break
    
if __name__ == "__main__":
    initLogging(20, funcLen = 21)
    
    
    inFile = sys.argv[1]
    tag = sys.argv[2]

    variables = ["numJets", "ht30"]
    names = ["Number of jets", "HT [GeV]"]
    proc = "ttbarPlusBBbarMerged"
    lumi = "137 fb^{-1}"
    
    main(inFile, tag, proc, variables, names, lumi)
