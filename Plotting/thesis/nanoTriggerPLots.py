from __future__ import print_function, division

import sys, os
from math import sqrt
import logging
from copy import copy, deepcopy

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging, makeCanvasOfHistos, saveCanvasListAsPDF, moveOverUnderFlow
from classes.PLotDrawers import makeCanvasOfHistosCustomLeg
from classes.ratios import RatioPlot

xTitles = {"hHT".lower() : "HT [GeV]",
           "hHTLoose".lower() : "HT [GeV]",
           "hpt".lower() : "p_{T} of all jets [GeV]",
           "hEta".lower() : "#eta of all jets ",
           "hpt0".lower() : "p_{T} of leading jet [GeV]",
           "hpt1".lower() : "p_{T} of second leading jet [GeV]",
           "hpt2".lower() : "p_{T} of 3rd leading jet [GeV]",
           "hpt3".lower() : "p_{T} of 4th leading jet [GeV]",
           "hpt5".lower() : "p_{T} of 6th leading jets [GeV]",
           "hNJets".lower() : "Number of jets",
           "hNBTags".lower()  : "Number of medium b-tagged jets"}

topScale = {"hHT".lower() : 1.1,
            "hHTLoose".lower() : 1.1,
            "hpt".lower() : 1.1,
            "hEta".lower() : 1.4 ,
            "hpt0".lower() : 1.1,
            "hpt1".lower() : 1.1,
            "hpt2".lower() : 1.1,
            "hpt3".lower() : 1.1,
            "hpt5".lower() : 1.1,
            "hNJets".lower() : 1.2,
            "hNBTags".lower()  : 1.4}


def nanoTriggerPlots(inputs, names, outputname, folder, lumiLabel):
    assert len(inputs) == len(names)

    # First get 
    histos = {}
    histos_fh = {}

    histoChecks = None
    for i, input_ in enumerate(inputs):
        logging.info("Getting histors of file: %s",input_ )
        histos[names[i]] = {}
        histos_fh[names[i]] = {}
        rFile = ROOT.TFile.Open(input_)
        histoNames = []
        histoNames_fh = []
        for key in rFile.GetListOfKeys():
            if key.GetName().endswith("_fh"):
                logging.debug("Adding %s to fh histos", key.GetName())
                histos_fh[names[i]][key.GetName().replace("_fh", "")] = deepcopy(rFile.Get(key.GetName()))
                histoNames_fh.append(key.GetName().replace("_fh", ""))
            else:
                logging.debug("Adding %s to regular histos", key.GetName())
                histos[names[i]][key.GetName()] = deepcopy(rFile.Get(key.GetName()))
                histoNames.append(key.GetName())

        if not set(histoNames) == set(histoNames_fh):
            raise RuntimeError
        if histoChecks is None:
            histoChecks = histoNames
        else:
            if not set(histoNames) == set(histoChecks):
                raise RuntimeError

    #Move overflow
    logging.info("Moving overflow")
    for name in names:
        for histo in histoChecks:
            moveOverUnderFlow(histos[name][histo])
            moveOverUnderFlow(histos_fh[name][histo])

            
    # Now do all vs fh plots
    logging.info("Doing regular vs. FH plots")
    for name in names:
        for histo in histoChecks:
            canvases = []
            canvases.append(
                makeCanvasOfHistosCustomLeg(
                    "reg_vs_fh_{}_{}".format(name, histo),
                    xTitles[histo.lower()],
                    [histos[name][histo], histos_fh[name][histo]],
                    [(histos[name][histo], name), (histos_fh[name][histo], name+" (w/ lepton veto)")],
                    normalized = True,
                    lineWidth = 2,
                    addCMS = True,
                    colorList = [ROOT.kBlue, ROOT.kGreen+2],
                    simulation=True,
                    legendSize = (0.55, 0.7, 0.87, 0.85),
                    topScale = topScale[histo.lower()],
                    lumi = lumiLabel
                    
                )
            )
            saveCanvasListAsPDF(canvases, "reg_vs_fh_"+name+"_"+histo+"_"+outputname, folder)
        
    # Now let do the plots where samples are compared
    logging.info("Will do samples comparison plots")
    for id_, allHistos in [("All", histos), ("FH", histos_fh)]:
        for histo in histoChecks:
            canvases = []
            theseHistos = []
            theseLegendObjects = []
            for name in names:
                theseHistos.append(allHistos[name][histo])
                theseLegendObjects.append((allHistos[name][histo], name))
            canvases.append(
                makeCanvasOfHistosCustomLeg(
                    "sampelsComp_{}_{}".format(id_, histo),
                    xTitles[histo.lower()],
                    theseHistos,
                    theseLegendObjects,
                    normalized = True,
                    lineWidth = 2,
                    addCMS = True,
                    colorList = [ROOT.kBlue, ROOT.kGreen+2],
                    simulation=True,
                    legendSize = (0.7, 0.65, 0.9, 0.80),
                    topScale = topScale[histo.lower()],
                    lumi = lumiLabel
                    
                )
            )
            saveCanvasListAsPDF(canvases, "sampelsComp_"+id_+"_"+histo+"_"+outputname, folder)

            
            
        
        
    
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        type = str,
        required=True,
        help = "Output path",
    )
    argumentparser.add_argument(
        "--folder",
        action = "store",
        type = str,
        default = "out_nanoTriggerPlots",
        help = "Output folder",
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        type = str,
        nargs = "+",
        required=True,
        help = "List of Input files",
    )
    argumentparser.add_argument(
        "--names",
        action = "store",
        type = str,
        nargs = "+",
        required=True,
        help = "List of names per inputs",
    )
    argumentparser.add_argument(
        "--lumi",
        action = "store",
        type = str,
        default = "137.1",
        help = "Lumi label",
    )
    args = argumentparser.parse_args()
    ##############################################################################################################
    
    initLogging(args.loglevel, funcLen = 21)

    nanoTriggerPlots(args.inputs, args.names,  args.output, args.folder, args.lumi)
