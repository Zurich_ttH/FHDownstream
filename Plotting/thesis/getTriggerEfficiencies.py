import ROOT
import sys
from collections import OrderedDict

histoDict = OrderedDict ([("AllEvents", "hNoTrigger"), 
                          ("Baseline", "hBaseline"), 
                          ("6J1T", "h6J1T"), 
                          ("6J2T", "h6J2T"), 
                          ("4J3T", "h4J3T"),
                          ("AllDedicated", "h6J1T2T4J3T"), 
                          ("Add", "hAddTrigger"), 
                          ("All", "hAll"),
                          ("6J", "h6J1T2T"),
                          ("6J1TorAdd" , "h6J1TAdd"), 
                          ("6J2TorAdd", "h6J2TAdd"), 
                          ("6J1Tor4J3T", "h6J1T4J3T"), 
                          ("6J2Tor4J3T", "h6J2T4J3T"), 
                          ("4J3TorAdd", "h4J3TAdd")])

def getTriggerEfficiencies(inputFile):
    print "Processing %s"%inputFile

    rFile = ROOT.TFile.Open(inputFile)
    rFile.cd()
    
    efficiencies = {}
    for histo in histoDict.keys():
        if histo == "AllEvents" or histo == "Baseline":
            continue
        efficiencies[histo] = rFile.Get(histoDict[histo]).GetBinContent(1) / rFile.Get(histoDict["Baseline"]).GetBinContent(1)
        print "{:15} \t {:10.3f}".format( histo, efficiencies[histo])
        
    return  efficiencies[histo]

if __name__ == "__main__":
    basepath = sys.argv[1]
    files = sys.argv[2:]


    for file_ in files:
        getTriggerEfficiencies(basepath+"/"+file_)
