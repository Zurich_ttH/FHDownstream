from __future__ import print_function, division

import sys, os
from math import sqrt
import logging
from copy import copy, deepcopy

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+'/src/TTH/FHDownstream/Plotting/'))
from classes.PlotHelpers import initLogging

def scaleSample(postfix, inputPath, nGen, xsec, lumi):
    rFile = ROOT.TFile.Open(inputPath)
    allKeys = [key.GetName() for key in rFile.GetListOfKeys()]

    SF = (xsec * 1000 * lumi) / nGen
    logging.info("Scaleing with %s", (xsec * 1000 * lumi) / nGen)
    
    histos = []
    for key in allKeys:
        thisHisto = deepcopy(rFile.Get(key))
        thisHisto.Scale(SF )
        histos.append(thisHisto)
    rFile.Close()

    outFile = ROOT.TFile(inputPath.replace(".root", postfix+".root"), "RECREATE")
    outFile.cd()

    for h in histos:
        h.Write()

    outFile.Close()
    

def scaelMultiSamples(base, outPostFix, inputs, nGen, xsec, lumi):
    assert len(inputs) == len(nGen)
    assert len(inputs) == len(xsec)
    
    for i, input_ in enumerate(inputs):
        logging.info("Processing %s", input_)
        scaleSample(outPostFix, base+"/"+input_, nGen[i], xsec[i], lumi)
        

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--base",
        action = "store",
        type = str,
        required=True,
        help = "Base of the input path",
    )
    argumentparser.add_argument(
        "--outPostFix",
        action = "store",
        type = str,
        default = "scaled",
        help = "Postfix for the output file",
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        type = str,
        nargs = "+",
        required=True,
        help = "List of Input files",
    )
    argumentparser.add_argument(
        "--nGen",
        action = "store",
        type = float,
        nargs = "+",
        required=True,
        help = "List ngen Values",
    )
    argumentparser.add_argument(
        "--xsec",
        action = "store",
        type = float,
        nargs = "+",
        required=True,
        help = "List ngen Values",
    )
    
    argumentparser.add_argument(
        "--lumi",
        action = "store",
        type = float,
        required=True,
        help = "Lumi",
    )
    
    args = argumentparser.parse_args()
    ##############################################################################################################
    
    initLogging(args.loglevel, funcLen = 21)


    
    scaelMultiSamples(args.base, args.outPostFix, args.inputs, args.nGen, args.xsec, args.lumi)
