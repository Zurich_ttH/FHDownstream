#include<algorithm>

float get_TriggerSF3D_2016(float ht, float pt6, int nB){
  // initialize in ROOT with TriggerSF* internalTriggerSF3D = new TriggerSF3D();
  float SF = internalTriggerSF3D_2016->getTriggerWeight(ht,pt6,nB);
  //cout << SF << endl;
  return SF;
}
float get_TriggerSF3D_2017(float ht, float pt6, int nB){
  // initialize in ROOT with TriggerSF* internalTriggerSF3D = new TriggerSF3D();
  float SF = internalTriggerSF3D_2017->getTriggerWeight(ht,pt6,nB);
  //cout << SF << endl;
  return SF;
}
float get_TriggerSF3D_2018(float ht, float pt6, int nB){
  // initialize in ROOT with TriggerSF* internalTriggerSF3D = new TriggerSF3D();
  float SF = internalTriggerSF3D_2018->getTriggerWeight(ht,pt6,nB);
  //cout << SF << endl;
  return SF;
}
