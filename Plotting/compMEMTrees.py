from __future__ import print_function, division
import sys
import os

from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, checkNcreateFolder, project
import ROOT
import logging
from copy import deepcopy

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

import itertools

from bTagPatchCheck import mergeSystematicsAuto


def compMEMTrees(inputFiles, legendElements, treeName, folder, outputName, ratioLabel, checkEvents=False, addJEC=None):
    logging.info("Got intput files: %s", inputFiles)

    trees = {}
    rFiles = {}
    for f in inputFiles:
        rFiles[f] = ROOT.TFile.Open(f)
        trees[f] = rFiles[f].Get(treeName)
        
    if checkEvents:
        logging.warning("Will check the files for the same events. This might take long and use a lot of Memory. Be careful")
        events = []
        for if_, file_ in enumerate(inputFiles):
            events.append([])
            nEvents = trees[f].GetEntries()
            for iev in range(nEvents):
                trees[f].GetEvent(iev)
                events[if_].append(trees[f].event)

        perms = list(itertools.permutations(range(len(inputFiles))))

        all_differences = []
        all_differences_lens = []
        for perm in perms:
            fist_set = set(events[perm[0]])
            diffs = [fist_set.difference(set(events[i])) for i in perm[1:]]
            all_differences.append(
               diffs
            )
            all_differences_lens.append(
                [len(list(x)) for x in diffs]
            )



        allGood = True
        for perm_diffs in all_differences_lens:
            for elem in perm_diffs:
                if elem != 0:
                    allGood = False
                    break

        if not allGood:
            logging.error("Somethign is not goood")
            exit()

    n_bins, left_edge, right_edge = 20,0,1

    hBase = ROOT.TH1F("hBase", "hBase", n_bins, left_edge, right_edge)
    hBase.SetLineWidth(2)
    
    histos_7J = []
    histos_8J = []
    histos_9J = []

    
    colors = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen-2, ROOT.kOrange, ROOT.kCyan]
    #colors = [ROOT.kRed, ROOT.kBlue, ROOT.kMagenta-1, ROOT.kOrange, ROOT.kGreen-2]
    
    for if_, f_ in enumerate(inputFiles):
        this_h_7J = hBase.Clone("h7J_"+str(if_))
        this_h_9J = hBase.Clone("h9J_"+str(if_))
        this_h_8J = hBase.Clone("h8J_"+str(if_))

        project(trees[f_], "h7J_"+str(if_), "mem", "cat==0", "1", "7J Tree: %s"%if_)
        project(trees[f_], "h8J_"+str(if_), "mem", "cat==1", "1", "8J Tree: %s"%if_)
        project(trees[f_], "h9J_"+str(if_), "mem", "cat==2", "1", "9J Tree: %s"%if_)

        this_h_7J.SetLineColor(colors[if_])
        this_h_9J.SetLineColor(colors[if_])
        this_h_8J.SetLineColor(colors[if_])

        
        histos_7J.append(deepcopy(this_h_7J))
        histos_8J.append(deepcopy(this_h_8J))
        histos_9J.append(deepcopy(this_h_9J))

        
        logging.info("Bin values: %s", f_)
        logging.info("  7J = %s",list(this_h_7J))
        logging.info("  8J = %s",list(this_h_8J))
        logging.info("  9J = %s",list(this_h_9J))

    band_7J = histos_7J[0].Clone("Errorband_7J")
    band_8J = histos_8J[0].Clone("Errorband_8J")
    band_9J = histos_9J[0].Clone("Errorband_9J")

    band_7J.SetLineColor(ROOT.kBlack)
    band_7J.SetFillColor(ROOT.kBlack)
    band_7J.SetFillStyle(3345)

    band_8J.SetLineColor(ROOT.kBlack)
    band_8J.SetFillColor(ROOT.kBlack)
    band_8J.SetFillStyle(3345)

    band_9J.SetLineColor(ROOT.kBlack)
    band_9J.SetFillColor(ROOT.kBlack)
    band_9J.SetFillStyle(3345)


    if addJEC:
        systSources = [
#            "JER",
            "FlavorQCD",
            "RelativeBal",
            "HF",
            "BBEC1",
            "EC2",
            "Absolute",
            "AbsoluteYear",
            "HFYear",
            "EC2Year",
            "RelativeSampleYear",
            "BBEC1Year",
            "ScaleHEM1516",
#            "Total"
        ]
   
        sdirs = ["Up", "Down"]

        histos_7J_JEC = {"ttHbb" : {"nom" : histos_7J[0]}}
        histos_8J_JEC = {"ttHbb" : {"nom" : histos_8J[0]}}
        histos_9J_JEC = {"ttHbb" : {"nom" : histos_9J[0]}}

        
        allkeys = []
        for source in systSources:
            for sdir in sdirs:
                allkeys.append(source+sdir)
                histos_7J_JEC["ttHbb"][source+sdir] = hBase.Clone("h7J_"+source+sdir)
                histos_8J_JEC["ttHbb"][source+sdir] = hBase.Clone("h8J_"+source+sdir)
                histos_9J_JEC["ttHbb"][source+sdir] = hBase.Clone("h9J_"+source+sdir)

                project(trees[inputFiles[0]], "h7J_"+source+sdir, "mem_"+source+sdir, "cat==0", "1", "7J"+source+sdir)
                project(trees[inputFiles[0]], "h8J_"+source+sdir, "mem_"+source+sdir, "cat==1", "1", "8J"+source+sdir)
                project(trees[inputFiles[0]], "h9J_"+source+sdir, "mem_"+source+sdir, "cat==2", "1", "9J"+source+sdir)

                
        h_7J_JEC_Up, h_7J_JEC_Down = mergeSystematicsAuto(histos_7J_JEC, "ttHbb", allkeys, "nom", SUM=False, RMS=False, SQUARDSUM=True)
        h_8J_JEC_Up, h_8J_JEC_Down = mergeSystematicsAuto(histos_8J_JEC, "ttHbb", allkeys, "nom", SUM=False, RMS=False, SQUARDSUM=True)
        h_9J_JEC_Up, h_9J_JEC_Down = mergeSystematicsAuto(histos_9J_JEC, "ttHbb", allkeys, "nom", SUM=False, RMS=False, SQUARDSUM=True)
                
        h_7J_JEC_Up.SetLineColor(ROOT.kOrange)
        h_9J_JEC_Up.SetLineColor(ROOT.kOrange)
        h_8J_JEC_Up.SetLineColor(ROOT.kOrange)

        h_7J_JEC_Down.SetLineColor(ROOT.kOrange)
        h_9J_JEC_Down.SetLineColor(ROOT.kOrange)
        h_8J_JEC_Down.SetLineColor(ROOT.kOrange)

        h_7J_JEC_Up.SetLineWidth(2)
        h_9J_JEC_Up.SetLineWidth(2)
        h_8J_JEC_Up.SetLineWidth(2)

        h_7J_JEC_Down.SetLineWidth(2)
        h_9J_JEC_Down.SetLineWidth(2)
        h_8J_JEC_Down.SetLineWidth(2)

        histos_7J.append(deepcopy(h_7J_JEC_Up))
        histos_8J.append(deepcopy(h_8J_JEC_Up))
        histos_9J.append(deepcopy(h_9J_JEC_Up))

        histos_7J.append(deepcopy(h_7J_JEC_Down))
        histos_8J.append(deepcopy(h_8J_JEC_Down))
        histos_9J.append(deepcopy(h_9J_JEC_Down))

        legendElements += ["JEC", None]
        
    canvases = []
        
    thisRatio = RatioPlot("7J")
    thisRatio.passHistos(histos_7J , normalize=True)
    thisRatio.ratioText = ratioLabel
    thisRatio.legendSize = (0.5,0.63,0.9,0.9)
    #thisRatio.ratioLineWidth = 2
    thisRatio.errorbandUncertaintyLabel = "Stat. Unc."
    #thisRatio.useDefaultColors = True
    canvases.append(
        deepcopy(
            thisRatio.drawPlot(
                legendElements,
                xTitle = "MEM (7J)",
                floatingMax=  1.35,
                errorband = band_7J,
                noErrRatio = True,
                noErr = True,
            )
        )
    )

    thisRatio = RatioPlot("8J")
    thisRatio.passHistos(histos_8J , normalize=True)
    thisRatio.ratioText = ratioLabel
    thisRatio.legendSize = (0.5,0.66,0.9,0.9)
    #thisRatio.ratioLineWidth = 2
    thisRatio.errorbandUncertaintyLabel = "Stat. Unc."
    #thisRatio.useDefaultColors = True
    canvases.append(
        deepcopy(
            thisRatio.drawPlot(
                legendElements,
                xTitle = "MEM (8J)",
                floatingMax=  1.35,
                errorband = band_8J,
                noErrRatio = True,
                noErr = True,
            )
        )
    )
    
    thisRatio = RatioPlot("9J")
    thisRatio.passHistos(histos_9J , normalize=True)
    thisRatio.ratioText = ratioLabel
    thisRatio.legendSize = (0.5,0.66,0.9,0.9)
    thisRatio.errorbandUncertaintyLabel = "Stat. Unc."
    #thisRatio.ratioLineWidth = 2
    #thisRatio.useDefaultColors = True
    canvases.append(
        deepcopy(
            thisRatio.drawPlot(
                legendElements,
                xTitle = "MEM (9J)",
                floatingMax=  1.35,
                errorband = band_9J,
                noErrRatio = True,
                noErr = True,
            )
        )
    )
    
    
    saveCanvasListAsPDF(canvases, outputName, folder)


if __name__ == "__main__":
    import argparse
    argumentparser = argparse.ArgumentParser(description="Comapre trees with same brnaches")
    argumentparser.add_argument("--logging", help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages", type=int,default=20)
    argumentparser.add_argument("--folder", help="Ouptut folder", type=str,default=".")
    argumentparser.add_argument("--treeName", help="Tree name", type=str, default="tree")
    argumentparser.add_argument("--outName", help="Outut file name", type=str, required=True)
    argumentparser.add_argument("--inputs", nargs="+", help="Input files", type=str,required=True)
    argumentparser.add_argument("--leg", nargs ="+", help="legend entries", type=str,required=True)
    argumentparser.add_argument("--checkEvents", action = "store_true",help = "Pass to check if the files have the same events")
    argumentparser.add_argument("--addJECComp", action = "store_true", help = "Pass a JEC name to be added as comparison")
    argumentparser.add_argument("--ratioLabel", help="Label for the ratio plot", type=str, default="Ratio to Regular")
    
    args = argumentparser.parse_args()
    
    initLogging(args.logging, "25")

    checkNcreateFolder(args.folder)

    compMEMTrees(args.inputs,
                 args.leg,
                 args.treeName,
                 args.folder,
                 args.outName,
                 args.ratioLabel,
                 args.checkEvents,
                 args.addJECComp)

    
