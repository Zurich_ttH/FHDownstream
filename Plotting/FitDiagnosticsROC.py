import logging
from copy import deepcopy
import ROOT

from classes.ratios import RatioPlot
from classes.roc import ROC
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

ROOT.gROOT.LoadMacro("classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("functions.h") #inlcude functions that can be used in TTree::Draw

def getFitDiagnosticsHistos(filename, postfit = True):
    rfile = ROOT.TFile.Open(filename)

    FitDiagnosticsChannels = {
        "j7t3" : "ttH_hbb_13TeV_2017_fh_7j3b_MEM",
        "j7t4" : "ttH_hbb_13TeV_2017_fh_7j4b_MEM",
        "j8t3" : "ttH_hbb_13TeV_2017_fh_8j3b_MEM",
        "j8t4" : "ttH_hbb_13TeV_2017_fh_8j4b_MEM",
        "j9t3" : "ttH_hbb_13TeV_2017_fh_9j3b_MEM",
        "j9t4" : "ttH_hbb_13TeV_2017_fh_9j4b_MEM"
    }
    histos = {"j7t3" : {},
              "j7t4" : {},
              "j8t3" : {},
              "j8t4" : {},
              "j9t3" : {},
              "j9t4" : {}}
    if postfit:
        topfolder = "shapes_fit_s"
    else:
        topfolder = "shapes_prefit"

    #GetProcesses:
    processes = []
    for process in rfile.GetDirectory(topfolder+"/ttH_hbb_13TeV_2017_fh_7j3b_MEM/").GetListOfKeys():
        processes.append(process.GetName())
    print processes

    for cat in histos:
        logging.info("Getting histograms for %s", cat)
        for process in processes:
            logging.debug("Getting histogram for %s/%s", cat, process)
            histos[cat][process] = deepcopy(rfile.Get(topfolder+"/"+FitDiagnosticsChannels[cat]+"/"+process))

    # for cat in histos:
    #     for process in processes:
    #         histos[cat][process].SetSum
    return histos

def removeErrors(histo):
    for ibin in range(histo.GetNbinsX()):
        histo.SetBinError(ibin, 0)

def makeMEMPerformance(histos, outPostfix = ""):
    distributions = ["ttH_hbb", "ttbarOther", "ttbarPlusCCBar", "ttbarPlusBBbar", "ttbarPlus2B", "ttbarPlusB", "ddQCD"]
    logging.info("Making normalized distributions")

    markers = {
        "ttH_hbb" : 1,
        "ttbarOther" : 33,  
        "ttbarPlusCCBar" : 23,
        "ttbarPlusBBbar" : 20,
        "ttbarPlus2B" : 22,
        "ttbarPlusB" : 29,
        "ddQCD" : 1
    }
    style = {
        "ttH_hbb" : "histo",
        "ttbarOther" : "P",
        "ttbarPlusCCBar" : "P",
        "ttbarPlusBBbar" : "P",
        "ttbarPlus2B" : "P",
        "ttbarPlusB" : "P",
        "ddQCD" : "histo"
    }

    lineStyle = {
        "ttH_hbb" : 2,
        "ttbarOther" : 1,
        "ttbarPlusCCBar" : 1,
        "ttbarPlusBBbar" : 1,
        "ttbarPlus2B" : 1,
        "ttbarPlusB" : 1,
        "ddQCD" : 1,
    }
    
    for cat in histos.keys():
        allCanvases = []
        catHistos = []
        legend = []
        styleList = []
        for process in histos[cat].keys():
            if process not in distributions: continue
            thisHisto = histos[cat][process]
            thisHisto.SetMarkerStyle(markers[process])
            thisHisto.SetMarkerSize(2)
            thisHisto.SetMarkerColor(Helper.colors[process])
            thisHisto.SetLineColor(Helper.colors[process])
            thisHisto.SetLineStyle(lineStyle[process])
            thisHisto.GetYaxis().SetTitleOffset(thisHisto.GetYaxis().GetTitleOffset() * 1.2)
            catHistos.append(deepcopy(thisHisto))#
            styleList.append(style[process])
            legend.append(Helper.legnames[process])
        print catHistos
        for histo in catHistos:
            removeErrors(histo)
        thisTopScale = 1.1
        if cat == "j8t4":
            thisTopScale = 1.3
        allCanvases.append(
            makeCanvasOfHistos(
                cat,
                "MEM Discriminant",
                catHistos,
                lineWidth = 2,
                legendSize = (0.4, 0.5, 0.9, 0.9),
                legendText = legend,
                normalized = True,
                drawAs = "P",
                addCMS = True,
                drawAsList = styleList,
                addLabel = Helper.CatLabels[cat],
                labelpos = (0.12,0.96),
                topScale = thisTopScale,
                supplementary = True,
            )
        )

        saveCanvasListAsPDF(allCanvases, "MEM_comp_{0}_fitDiagnostics{1}".format(cat, outPostfix),".")

    
    distributions = ["ttbarOther", "ttbarPlusCCBar", "ttbarPlusBBbar", "ttbarPlus2B", "ttbarPlusB", "ddQCD"]
    logging.info("Getting ROCs")
    for cat in histos.keys():
        ROCcanvases = []
        catHistos = []
        legend = []
        styleList = []
        thisROC = ROC("ROC_"+cat)
        for process in histos[cat].keys():
            if process not in distributions: continue
            thisHisto = histos[cat][process]
            signalHisto =  histos[cat]["ttH_hbb"]

            thisHisto.SetLineColor(Helper.colors[process])            
            thisROC.passHistos(signalHisto, thisHisto)
            # thisHisto.SetMarkerStyle(markers[process])
            # thisHisto.SetMarkerSize(2)
            # thisHisto.SetMarkerColor(Helper.colors[process])

            # thisHisto.SetLineStyle(lineStyle[process])
            # thisHisto.GetYaxis().SetTitleOffset(thisHisto.GetYaxis().GetTitleOffset() * 1.2)
            # catHistos.append(deepcopy(thisHisto))#
            # styleList.append(style[process])
            legend.append(Helper.legnames[process])
        thisROC.addLabel(Helper.CatLabels[cat], 0.17, 0.88, "Left")
        thisROC.CMSscale = (1.0, 1.0)
        thisROC.moveCMS = -0.07
        thisROC.legendSize = (0.14, 0.5, 0.45, 0.84)
        thisROC.thisCMSLabel = "Supplementary"
        ROCcanvases.append(
           thisROC.drawROC(
               legend,
               drawCMS = True
           ) 
        )
        saveCanvasListAsPDF(ROCcanvases, "MEM_ROC_{0}_fitDiagnostics{1}".format(cat, outPostfix),".")

def makeYield(histos, doPlot, doTable, ttHscale = 1, outPostfix = "", postfit = False):
    stackOrder = Helper.stackOrderDatacard
    signals = Helper.signalsDatacard

    yieldHisto = ROOT.TH1F("yieldHisto", "yieldHisto", len(histos.keys()), 0, len(histos.keys()))

    yieldOrder = ["j7t3", "j8t3", "j9t3", "j7t4", "j8t4", "j9t4"]
    yieldOrderLabels = ["7J3B", "8J3B", "9J3B", "7J4B", "8J4B", "9J4B"]
    
    yields = {}
    logging.info("Getting yields per category")
    for icat, cat in enumerate(yieldOrder):
        for process in histos[cat].keys():
            if process.startswith("total_") and not (process.endswith("background") or process.endswith("signal")): continue
            if not isinstance(histos[cat][process], ROOT.TH1F): continue
            
            if process not in yields.keys():
                yields[process] = yieldHisto.Clone(yieldHisto.GetName()+"_"+process)

            error = ROOT.Double(0)
            thisYield = histos[cat][process].IntegralAndError(0,histos[cat][process].GetNbinsX()+1, error)
            yields[process].SetBinContent(icat+1, thisYield)
            yields[process].SetBinError(icat+1, error)
            logging.debug("Setting %s +- %s for %s/%s", thisYield, error, cat, process)

    #Data is saved as graph (with asymmetric erros). So we have to transver it to a histogram to
    #use the IntegralAndError function. Because histograms can not have asymmetric errors we conservatively
    #use the larger of the two sides for the yield calculation
    for icat, cat in enumerate(yieldOrder):
        for process in histos[cat].keys():
            if not isinstance(histos[cat][process], ROOT.TGraphAsymmErrors): continue
            thisGraph =  histos[cat][process]
            binContents = {}
            #Get content from the graph
            for i in range(thisGraph.GetN()):
                x,y = ROOT.Double(0),ROOT.Double(0)
                thisGraph.GetPoint(i, x, y)
                binContents[i] = (y,max(thisGraph.GetErrorYlow(i), thisGraph.GetErrorYhigh(i)) )
            #Clone a histogram that is always present to get the binning and reset it (just for good measure)
            thisHisto = histos[cat]["total_background"].Clone(thisGraph.GetName())
            for i in range(thisHisto.GetNbinsX()+2):
                thisHisto.SetBinContent(i, 0)
                thisHisto.SetBinError(i, 0)
            #Bin content and error from the graph in the histogram
            bins = range(1, thisHisto.GetNbinsX()+1)
            assert len(bins) == len(binContents.keys())
            logging.debug("Setting data histogram")
            for iBin in bins:
                content, error = binContents[iBin-1]
                logging.debug("Setting bin %s to %s +- %s", iBin, content, error)
                thisHisto.SetBinContent(iBin, content)
                thisHisto.SetBinError(iBin, error)
            error = ROOT.Double(0)
            #Now we can finally calculate the yield
            thisYield = thisHisto.IntegralAndError(0,thisHisto.GetNbinsX()+1, error)
            if process not in yields.keys():
                yields[process] = yieldHisto.Clone(yieldHisto.GetName()+"_"+process)

            yields[process].SetBinContent(icat+1, thisYield)
            yields[process].SetBinError(icat+1, error)
            logging.debug("Setting %s +- %s for %s/%s", thisYield, error, cat, process)

    #Now merge processes:
    for process in stackOrder:
        if len(stackOrder[process]) > 1:
            for iproc, proc in enumerate(stackOrder[process]):
                if iproc == 0:
                    logging.info("Creating histo for %s from %s",process, proc)
                    yields[process] = yields[proc].Clone(yields[proc].GetName().replace(proc, process))
                    logging.debug("Created: %s", yields[process])
                else:
                    yields[process].Add(yields[proc])
                    logging.debug("Adding %s to %s", proc, process)

    if doPlot:
        makeYieldPlots(yields, stackOrder, yieldOrder, yieldOrderLabels, ttHscale, outPostfix = outPostfix, postfit = postfit)
    if doTable:
        makeYieldTable(yields, stackOrder, yieldOrder, yieldOrderLabels, precision = "int", ANstyle = True, outPostfix = outPostfix)
        
def makeYieldTable(yields, stackOrder, yieldOrder, yieldOrderLabels, precision = "normal", ANstyle = False, outPostfix = "", postfit = False, baseline = False):
    logging.info("Will make yield table")
    if baseline:
        tableOrder = Helper.tableOrderBaseline
    else:
        tableOrder = Helper.tableOrderDatacard
    topLine = ""

    if precision == "normal":
        roundNumbers = False
    elif precision == "int":
        roundNumbers = True
    else:
        raise NotImplementedError("Other precision than normal and int setting are not implemented") 
        
    for cat in yieldOrderLabels:
        topLine += "& {0}".format(cat)
    topLine += "\\\\"
    processLines = []
    for process in tableOrder:
        if not ANstyle:
            thisLine = "{0} ".format(Helper.legnames[process])
        else:
            thisLine = "{0} ".format(Helper.tablenamesAN[process])
        for iCat, cat in enumerate(yieldOrder):
            if not roundNumbers:
                thisLine += " & {0:.2f} $\\pm$ {1:.2f} ".format(yields[process].GetBinContent(iCat+1), yields[process].GetBinError(iCat+1))
            else:
                val, error = int(round(yields[process].GetBinContent(iCat+1))), int(round(yields[process].GetBinError(iCat+1)))
                if error == 0 and val != 0:
                    error = 1
                thisLine += " & {0} $\\pm$ {1} ".format( val, error)
        processLines.append(thisLine+"\\\\")

    logging.debug("================================================================")
    logging.debug(topLine)
    for l in processLines:
        logging.debug(l)
    logging.debug("================================================================")

    fileName = "yield_table_fitDiagnostics{0}.txt".format(outPostfix)
    logging.info("Writing table to output file %s", fileName)
    with open(fileName, "w") as f:
        f.write(topLine+"\n")
        for l in processLines:
            f.write(l+"\n")

        
def makeYieldPlots(yields, stackOrder, yieldOrder, yieldOrderLabels, ttHscale = 1, outPostfix = "", postfit = False):
    #Make stack
    stack = ROOT.THStack("YieldStack", "YieldStack")
    legendObjwText = []
    for process in stackOrder:
        logging.debug("Adding %s to stack", process)
        yields[process].SetLineColor(ROOT.kBlack)
        yields[process].SetFillColor(Helper.colors[process])
        yields[process].SetFillStyle(1001)
        stack.Add(yields[process])
        legendObjwText.append((deepcopy(yields[process]), Helper.legnames[process]))

    yields["data"].SetLineColor(ROOT.kBlack)
    yields["data"].SetMarkerColor(ROOT.kBlack)
    yields["data"].SetMarkerStyle(20)
    yields["data"].SetMarkerSize(1)

    yields["total_background"].SetLineColor(ROOT.kBlack)
    yields["total_background"].SetMarkerColor(ROOT.kBlack)


    yields["total_signal"].SetLineColor(ROOT.kBlue)
    yields["total_signal"].SetLineWidth(2)
    if not postfit:
        yields["total_signal"].Scale(ttHscale)
    legendObjwText.append((
        deepcopy(yields["total_signal"]),
        Helper.legnames["total_signal"]+(" x {0}".format(int(ttHscale)) if not postfit else ""),
        "L"
    ))
    
    for proc in yields:
        yields[proc].GetXaxis().CenterLabels()
        for icat, cat in enumerate(yieldOrderLabels):
            yields[proc].GetXaxis().SetBinLabel(icat+1, cat)
            yields[proc].GetXaxis().SetLabelOffset(0.03)
            yields[proc].GetXaxis().SetLabelSize(1)
            yields[proc].GetYaxis().SetLabelSize(yields[proc].GetYaxis().GetLabelSize()* 1.06)
            yields[proc].GetYaxis().SetLabelOffset(yields[proc].GetYaxis().GetLabelOffset()* 1.01)
            #yields[proc].GetXaxis().LabelsOption("u")
    
    #errorband = ROOT.TGraphErrors(yields["total_background"])
    # for i in range(errorband.GetN()):
    #     x,y = ROOT.Double(0),ROOT.Double(0)
    #     thisGraph.GetPoint(i, x, y)
    #     print i, x, y, errorband.GetErrorYlow(i), errorband.GetErrorYhigh(i)
    # print errorband
    errorband = yields["total_background"].Clone("Errorband")
    errorband.SetFillColor(ROOT.kBlack)
    errorband.SetFillStyle(3645)
    finalOutput = []
    legendObjwText.append((
         errorband, "Uncertainty"
    ))
    
    output = RatioPlot(name = "Yields_normal")
    output.ratioRange =  (0.92,1.08)
    output.CMSscale = (1.4, 1.4)
    output.legColumns = 2
    output.legendSize = (0.6, 0.3, 0.9, 0.77)
    output.yTitleOffset = 0.99
    output.yTitleOffsetRatio = 0.45
    output.addLabel("Post-fit" if postfit else "Pre-fit", 0.125, 0.955, 1.2)
    output.labelScaleX = 1.4
    output.yTitle = "Events / Category"
    output.passHistos([yields["data"], stack])
    finalOutput.append(
            output.drawPlot(
                None,
                "",
                isDataMCwStack = True,
                stacksum=yields["total_background"],
                errorband = errorband,
                drawCMS = True,
                histolegend = legendObjwText,
                addHisto = [(yields["total_signal"],"histo")],
                noErr = True
                
            )
    )

    saveCanvasListAsPDF(finalOutput, "yield_fitDiagnostics{0}".format(outPostfix),".")
    finalOutput = []
    
    output_log = RatioPlot(name = "Yields_log")
    output_log.ratioRange = (0.92,1.08)
    output_log.legColumns = 3
    output_log.legendSize = (0.21, 0.6, 0.7, 0.9)
    output_log.CMSscale = (1.4, 1.4)
    output_log.yTitleOffset = 0.99
    output_log.yTitleOffsetRatio = 0.45
    output_log.labelScaleX = 1.4
    output_log.addLabel("Post-fit" if postfit else "Prefit", 0.125, 0.955, 1.2)
    output_log.yTitle = "Events / Category"
    output_log.passHistos([yields["data"], stack])
    finalOutput.append(
            output_log.drawPlot(
                None,
                "",
                isDataMCwStack = True,
                stacksum=yields["total_background"],
                errorband = errorband,
                drawCMS = True,
                histolegend = legendObjwText,
                addHisto = [(yields["total_signal"],"histo")],
                logscale = True,
                noErr = True
            )
    )

    
    saveCanvasListAsPDF(finalOutput, "yield_log_fitDiagnostics{0}".format(outPostfix),".")
     
if __name__ == "__main__":
    fitDiagnostics = "/mnt/t3nfs01/data01/shome/koschwei/scratch/datacards/HIG-18-030/ResFHforAN/FitDiagnostics_Asimov1p0_PullsAndShapesWithUncs/fitDiagnostics_2017_fh.root"
    fitDiagnostics = "/t3home/koschwei/scratch/ttH/results/test_v8108_190213/outputs/FitDiagnostics_Data_PullsAndShapesWithUncs/fitDiagnostics_2017_fh.root"
    fitDiagnostics = "/work/koschwei/tth/2017Data/v3p3/Datacards/sparsev5_final_v2_unblind_V5_L1Pre/cards/fitsNAF/fitDiagnostics_2017_fh.root"
    fitDiagnostics = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p3/Datacards/sparsev5_final_v3_noDeta_unblind_V5_L1Pre_noDeta/cards/fitDiagnostics_asimov_sig_fh_repoSettings_Tolp1.root"
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--memPerformance",
        action = "store_true",
        help = "If set MEM performance plots (ROC and discriminant comparison) for MC + ddQCD will be produced",
    )
    argumentparser.add_argument(
        "--yieldPlots",
        action = "store_true",
        help = "If set Yield plots will be generated",
    )
    argumentparser.add_argument(
        "--yieldTable",
        action = "store_true",
        help = "If set Yield table will be generated",
    )

    argumentparser.add_argument(
        "--postfit",
        action = "store_true",
        help = "If set Yield plots will be generated",
    )
    argumentparser.add_argument(
        "--signalScale",
        action = "store",
        type = float,
        default = 100,
        help = "Scale for signal histogram in yield plot",
    )
    args = argumentparser.parse_args()
    ##############################################################################################################
    initLogging(args.loglevel)
    logging.info("Starting script")
    histos = getFitDiagnosticsHistos(fitDiagnostics, postfit = args.postfit)
    postfix = "_unblind"
    postfix += "_postfit" if args.postfit else "_prefit"
    if args.memPerformance:
        makeMEMPerformance(histos,  outPostfix = postfix)
    if args.yieldPlots or args.yieldTable:
        makeYield(histos, doPlot = args.yieldPlots, doTable = args.yieldTable, ttHscale = args.signalScale, outPostfix = postfix, postfit = args.postfit)
    
    
    logging.info("Exiting script")

    
