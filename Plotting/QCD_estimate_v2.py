import multiprocessing
import os
import sys
import logging
import numpy as np
import json
from copy import deepcopy

import ROOT
ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetErrorX(0) #turns of horizontal error bars (but also for bkg)
ROOT.gROOT.SetBatch(1)

import Helper
from classes.ratios import RatioPlot
from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos, initLogging, moveOverUnderFlow, project


class Config(object):
    def __init__(self, pathtoConfig):
        logging.info("Loading config %s", pathtoConfig)
        import ConfigParser
        thisconfig = ConfigParser.ConfigParser()
        thisconfig.optionxform = str #Use this so the section names keep Uppercase letters
        thisconfig.read(pathtoConfig)

        logging.debug("============= CONFIG =============")
        self.categories = thisconfig.get("General", "Categories").split(",")
        self.variables = thisconfig.get("General", "Variables").split(",")
        self.lumi = thisconfig.getfloat("General", "lumi")
        self.era = thisconfig.getfloat("General", "era")
        self.data = [thisconfig.get("General", "data")]
        self.signals = thisconfig.get("General", "signals").replace(" ","").split(",")
        self.tt = thisconfig.get("General", "tt").replace(" ","").split(",")
        self.minor = thisconfig.get("General", "minor").replace(" ","").split(",")
        self.qcd = thisconfig.get("General", "qcd").replace(" ","").split(",")
        self.allProcs = self.data+self.signals+self.tt+self.minor
        self.doValidation = thisconfig.getboolean("General", "validation")
        self.drawPulls = thisconfig.getboolean("General", "drawPulls")
        self.doMCValidation = thisconfig.getboolean("General", "useMC")
        self.runBlind = thisconfig.getboolean("General", "blind") # Only relevand if validation is false
        self.includeSignal = thisconfig.getboolean("General", "includeSignal")
        self.printTestVal =  thisconfig.getboolean("General", "testVal")

        self.addSignalHisto = thisconfig.getboolean("General", "addSignalHisto")
        self.signalScale = thisconfig.getint("General", "signalScale")

        if thisconfig.has_option("General", "sideband"):
            self.sidebandVal = thisconfig.getboolean("General", "sideband")
        else:
            self.sidebandVal = False

        if self.doMCValidation:
            self.allProcs += self.qcd
            
        logging.debug("Lumi : %s", self.lumi)
        logging.debug("Data samples: %s", self.data)
        logging.debug("TT samples: %s", self.tt)
        logging.debug("Minor samples: %s", self.minor)
        logging.debug("Signal samples: %s", self.signals)
        logging.debug("-- General Flags --")
        logging.debug("doValidation: %s", self.doValidation)
        logging.debug("doMCValidation: %s", self.doMCValidation)
        if self.doValidation:
            self.runBlind = False
            logging.debug("Blinding is disables because of validation")
        logging.debug("runBlind: %s", self.runBlind)

        logging.debug("-- Files --")
        self.files = {}
        self.xsec = {}
        self.nGen = {}
        self.sampleSelection = {}
        basepath = thisconfig.get("files", "basepath")
        if not basepath.endswith("/"):
            basepath += "/"
        dataFolder = thisconfig.get("files", "dataFolder")
        MCFolder = thisconfig.get("files", "mcFolder")
        for proc in self.allProcs:
            folder = dataFolder if proc == "data" else MCFolder
            if folder != "" and not folder.endswith("/"):
                folder += "/"
            self.files[proc] = basepath + folder + thisconfig.get("files", proc)
            if proc != "data":
                self.xsec[proc] = thisconfig.getfloat("xsecs", proc)
                self.nGen[proc] = thisconfig.getfloat("nGen", proc)
            else:
                self.xsec[proc] = 1.0
                self.nGen[proc] = 1.0
                
            self.sampleSelection[proc] = "1"
            if thisconfig.has_section("sampleSelections"):
                if thisconfig.has_option("sampleSelections", proc):
                    self.sampleSelection[proc] = thisconfig.get("sampleSelections", proc)
                    logging.debug("Found sample selection %s for %s", self.sampleSelection[proc], proc)
            
                
            logging.debug("%s : %s, %s, %s", proc, self.xsec[proc], self.nGen[proc], self.files[proc])

        self.BaselineSel = thisconfig.get("Projection", "Baseline")
        self.SRSelection_general = thisconfig.get("Projection", "SRSelection")
        self.CRSelection_general = thisconfig.get("Projection", "CRSelection")
        self.VRSelection_general = thisconfig.get("Projection", "VRSelection")
        self.CR2Selection_general = thisconfig.get("Projection", "CR2Selection")
        self.weight = thisconfig.get("Projection", "weight")
        self.loadbCorrMacro = thisconfig.getboolean("Projection", "loadbCorrMacro")
        self.applyHTWeight = thisconfig.getboolean("Projection", "applyHTWeight")
        self.bCorrMarco = thisconfig.get("Projection", "bCorrMacro")
        self.bCorr = {}
        self.bCorr["MC"] = thisconfig.get("Projection", "bCorrMC")
        self.bCorr["Data"] = thisconfig.get("Projection", "bCorrData")
        self.loadTriggerMarco = thisconfig.getboolean("Projection", "loadTriggerMacro")
        if self.applyHTWeight and not self.loadTriggerMarco:
            logging.warning("Enabling loadTriggerMarco since applyHTWeight was set to true")
            self.loadTriggerMarco = True
        self.useWeightModule = thisconfig.get("Projection", "useWeightModule").replace(" ","").split(",")
        for imodule, module in enumerate(self.useWeightModule):
            if module == "":
                self.useWeightModule.pop(imodule)
        print len(self.useWeightModule)
        logging.debug("-- Projection Settings --")
        logging.debug("Baseline : %s", self.BaselineSel)
        logging.debug("Weight: %s", self.weight)
        logging.debug("BCorr:")
        logging.debug("  Will oad macro : %s", self.loadbCorrMacro)
        logging.debug("  Loaded from %s",  self.bCorrMarco)
        logging.debug("  MC: %s", self.bCorr["MC"] )
        logging.debug("  Data: %s", self.bCorr["Data"] )
        logging.debug("Will load trigger macro: %s", self.loadTriggerMarco )
        if not self.useWeightModule:
            logging.debug("No weight modules specified")
        else:
            logging.debug("Pyhton weight modules: %s",self.useWeightModule)
        
        self.catCut = {}
        self.catName = {}
        self.regionCuts = {}
        for cat in self.categories:
            self.catCut[cat] = thisconfig.get(cat, "cut")
            self.catName[cat] = thisconfig.get(cat, "name")
            self.regionCuts[cat] = {}
            if "SRSelection" in thisconfig.options(cat):
                self.regionCuts[cat]["SRSelection"] = thisconfig.get(cat, "SRSelection")
            else:
                self.regionCuts[cat]["SRSelection"] = self.SRSelection_general
            if "CRSelection" in thisconfig.options(cat):
                self.regionCuts[cat]["CRSelection"] = thisconfig.get(cat, "CRSelection")
            else:
                self.regionCuts[cat]["CRSelection"] = self.CRSelection_general
            if "VRSelection" in thisconfig.options(cat):
                self.regionCuts[cat]["VRSelection"] = thisconfig.get(cat, "VRSelection")
            else:
                self.regionCuts[cat]["VRSelection"] = self.VRSelection_general
            if "CR2Selection" in thisconfig.options(cat):
                self.regionCuts[cat]["CR2Selection"] = thisconfig.get(cat, "CR2Selection")
            else:
                self.regionCuts[cat]["CR2Selection"] = self.CR2Selection_general

        logging.debug("-- Cat definition --")
        logging.debug("categories : %s", self.categories)
        for cat in self.categories:
            logging.debug("-----------------------")
            logging.debug("Cat : %s", cat)
            logging.debug("Cut : %s", self.catCut[cat])
            logging.debug("Name : %s", self.catName[cat])
            logging.debug("SR: %s", self.regionCuts[cat]["SRSelection"])
            logging.debug("CR: %s", self.regionCuts[cat]["CRSelection"])
            logging.debug("VR: %s", self.regionCuts[cat]["VRSelection"])
            logging.debug("CR2: %s", self.regionCuts[cat]["CR2Selection"])
            
        self.varSettings = {}
        for var in self.variables:
            varforCfg = var
            if var.endswith("]"):
                varforCfg = var[:-1]
            self.varSettings[var] = {}
            self.varSettings[var]["treeExpr"] = thisconfig.get(varforCfg, "tree")
            self.varSettings[var]["nBins"] = thisconfig.getint(varforCfg, "nBins")
            self.varSettings[var]["minBin"] = thisconfig.getfloat(varforCfg, "minBin")
            self.varSettings[var]["maxBin"] = thisconfig.getfloat(varforCfg, "maxBin")
            self.varSettings[var]["xtitle"] = thisconfig.get(varforCfg, "xtitle")
            self.varSettings[var]["ytitle"] = thisconfig.get(varforCfg, "ytitle")
            
        logging.debug("Initialized variables: %s", self.varSettings.keys())

def get_QGNormaliziation(process, category, isSR, isCR, deltaEta = True):
    """
    Function for getting the normalization for the qgWeight factor for all samples
    
    Args:
    category (str) : Analysis category
    isSR, isCR (bool) : isSR will switch between SR and VR and isCR switches if the accomaning CR should be used

    Returns:
    data (dict) : Returns nested dict with scale factors for each sample for analyisis category/region
                  --> Call with data[datasetname]["7j"/"8j"/"9j"]["2t"/"3t"]["SR"/"CR"/"CR2"/"VR"]
    """
    path =os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/"
    if deltaEta:
        fileName = path+"qgNorm_wDetaj_v5-bSF.json"
    else:
        fileName = path+"qgNorm_v5-bSF.json"
    with open(fileName, "r") as normFile:
        data = json.load(normFile)

    if category == "7j4t":
        j,b = "7j", "4t"
    elif category == "8j4t":
        j,b = "8j", "4t"
    elif category == "9j4t":
        j,b = "9j", "4t"
    elif category == "7j3t":
        j,b = "7j", "3t"
    elif category == "8j3t":
        j,b = "8j", "3t"
    elif category == "9j3t":
        j,b = "9j", "3t"
    elif category == "ge7j4t":
        j,b = "ge7j", "4t"
    else:
        raise NotImplementedError

    region = ""
    if isSR:
        if isCR:
            region = "CR"
        else:
            region = "SR"
    elif not isSR:
        if isCR:
            region = "CR2"
        else:
            region = "VR"
    else:
        raise NotImplementedError
    logging.debug("Getting qglNormaluation for [%s][%s][%s][%s]",process,j,b,region)
    
    return data[process][j][b][region]


        
def getMultijet(config, histos, includeSignal=False):
    ddHisto = histos[config.tt[0]].Clone(histos[config.tt[0]].GetName().replace(config.tt[0], "ddQCD"))
    for ibin in range(ddHisto.GetNbinsX()+2):
        ddHisto.SetBinContent(ibin, 0.0)
        ddHisto.SetBinError(ibin, 0.0)

    for dataProc in config.data:
        ddHisto.Add(histos[dataProc])
        
    for proc in config.tt+config.minor:
        ddHisto.Add(histos[proc], -1)

    if includeSignal:
        for proc in config.signals:
            ddHisto.Add(histos[proc], -1)

    return ddHisto

def getEstimate(inputs, config, variable, category, logger = logging):
    logger.info("Starting estimation for %s, %s", variable, category)
    catSel = config.catCut[category]
    eventWeight = config.weight
    eventWeightData = "1"
    
    if config.doValidation:
        CRSelection = config.regionCuts[category]["CR2Selection"]
        SRSelection = config.regionCuts[category]["VRSelection"]
    else:
        CRSelection = config.regionCuts[category]["CRSelection"]
        SRSelection = config.regionCuts[category]["SRSelection"]

        
    EventSelectionCR = "{0} && {1} && {2}".format(config.BaselineSel, catSel, CRSelection)
    EventSelectionSR = "{0} && {1} && {2}".format(config.BaselineSel, catSel, SRSelection)
    logger.debug("CR/CR2 Selection: %s",EventSelectionCR)
    logger.debug("SR/VR Selection: %s",EventSelectionSR)
    logger.info("Projection CR histograms")
    CRHistos = getHistograms("CR_"+category, inputs, config, category, variable, EventSelectionCR, eventWeight, eventWeightData, isSR = False)
    logger.info("Projection SR histograms")
    SRHistos = getHistograms("SR_"+category, inputs, config, category, variable, EventSelectionSR, eventWeight, eventWeightData, isSR = True)
    
    SRHistos["MultiJet"] = CRHistos["ddQCD"].Clone(SRHistos["ddQCD"].GetName().replace("ddQCD", "MultiJet"))
    SRScale_multijet = SRHistos["ddQCD"].Integral()/float(CRHistos["ddQCD"].Integral())
    SRHistos["MultiJet"].Scale(SRScale_multijet)
    CRHistos["MultiJet"] = CRHistos["ddQCD"].Clone(CRHistos["ddQCD"].GetName().replace("ddQCD", "MultiJet"))

    logger.info("Created Multijet for SR/VR: %s (scale %s)", SRHistos["MultiJet"], SRScale_multijet)
    
    return SRHistos, CRHistos
    
def getHistograms(prefix, inputs, config, category, variable, eventSelection, eventWeightMC, eventWeightData, isSR, logger = logging):
    if not isSR:
        eventWeightMC = "{0}*{1}".format(eventWeightMC, config.bCorr["MC"])
        eventWeightData = "{0}*{1}".format(eventWeightData, config.bCorr["Data"])
        
    hName_base = "{0}_{1}_baseHisto".format(prefix, variable)

    nBins = config.varSettings[variable]["nBins"]
    binLow = config.varSettings[variable]["minBin"]
    binHigh = config.varSettings[variable]["maxBin"]
    
    baseHisto = ROOT.TH1F(hName_base, hName_base, nBins, binLow, binHigh)
    baseHisto.GetXaxis().SetTitle(config.varSettings[variable]["xtitle"])
    baseHisto.GetYaxis().SetTitle(config.varSettings[variable]["ytitle"])
    baseHisto.Sumw2()
    logger.debug("Created main histogram with name %s, %s|%s|%s", hName_base, nBins, binLow, binHigh)

    histos = {}
    for proc in config.allProcs:
        histos[proc] = baseHisto.Clone(baseHisto.GetName().replace("baseHisto", proc))
        if proc == "data":
            thisWeight = eventWeightData
        else:
            thisWeight = eventWeightMC

            for module in config.useWeightModule:
                factor = 1.0
                if module == "QGLR":
                    SignalRegion = not config.doValidation
                    ControlRegion = not isSR
                    factor = get_QGNormaliziation(proc, category, SignalRegion, ControlRegion)
                elif module == "QGLRnoDeta":
                    SignalRegion = not config.doValidation
                    ControlRegion = not isSR
                    factor = get_QGNormaliziation(proc, category, SignalRegion, ControlRegion, deltaEta=False)
                else:
                    raise NotImplementedError
                logger.debug("Adding weight factor %s from module %s", factor, module)
                thisWeight += "*"+str(factor)
        if config.applyHTWeight and not isSR:
            logging.warning("Applying HTWeight to %s in CR", proc)
            if str(int(config.era)) == "2016":
                thisWeight += "*HTWeight_2016(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])"
            elif str(int(config.era)) == "2017":
                thisWeight += "*HTWeight_2017(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])"
            elif str(int(config.era)) == "2018":
                thisWeight += "*HTWeight_2018(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])"
            else:
                raise NotImplementedError("Era %s not supperted with HTReighting"%(config.era))
                

        thisEventSelection = "{0} && {1}".format(eventSelection, config.sampleSelection[proc])
        logging.debug("Selection for this process: %s", thisEventSelection)
        project(inputs["tree"][proc], histos[proc].GetName(), config.varSettings[variable]["treeExpr"], thisEventSelection, thisWeight, proc)
        logger.debug("Integral of projected histogram: %s", histos[proc].Integral())
        moveOverUnderFlow(histos[proc], True, True)
        #moveOverUnderFlow(histos[proc], False, True)
        
    for proc in config.allProcs:
        if proc == "data":
            continue
        genSF = config.lumi*1000*float(config.xsec[proc])/float(config.nGen[proc])
        logger.debug("Scaleing %s with %s*1000*%s/%s = %s", proc, config.lumi,config.xsec[proc],config.nGen[proc], genSF)
        histos[proc].Scale(genSF)

    logger.debug("Merging %s",config.tt)
    for iproc, proc in enumerate(config.tt):
        if iproc == 0:
            histos["tt"] = histos[proc].Clone(histos[proc].GetName().replace(proc, "tt"))
        else:
            histos["tt"].Add(histos[proc])

    logger.debug("Merging %s",config.minor)
    for iproc, proc in enumerate(config.minor):
        if iproc == 0:
            histos["minor"] = histos[proc].Clone(histos[proc].GetName().replace(proc, "minor"))
        else:
            histos["minor"].Add(histos[proc])

    logger.debug("Merging %s",config.signals)
    for iproc, proc in enumerate(config.signals):
        if iproc == 0:
            histos["signals"] = histos[proc].Clone(histos[proc].GetName().replace(proc, "signals"))
        else:
            histos["signals"].Add(histos[proc])

    histos["ddQCD"] = getMultijet(config, histos, includeSignal=isSR)

    return histos

def makePlot(config, histos, outdir, category, variable, filePrefix, regionName, addSignal= False, pulls = False, logger = logging, STXSBin = None):
    stackName = histos["MultiJet"].GetName().replace("MultiJet","Stack")
    stack = ROOT.THStack(stackName, stackName)
    
    MultiJet4Stack = histos["MultiJet"].Clone(histos["MultiJet"].GetName()+"_4Stack")
    tt4Stack = histos["tt"].Clone(histos["tt"].GetName()+"_4Stack")
    minor4Stack = histos["minor"].Clone(histos["minor"].GetName()+"_4Stack")
    signals4Plot = histos["signals"].Clone(histos["signals"].GetName()+"_4Stack")
    data4Plot = histos["data"].Clone(histos["data"].GetName()+"_4Stack")

    if addSignal:
       minor4Stack.Add(signals4Plot) 

        
    bkgsum = histos["MultiJet"].Clone(histos["MultiJet"].GetName().replace("MultiJet", "bkgsum"))
    bkgsum.Add(histos["tt"])
    bkgsum.Add(histos["minor"])

    if config.runBlind:
        logging.warning("Blinding data")
        data4Plot = bkgsum.Clone(data4Plot.GetName()+"_blind")
    
    errorband = bkgsum.Clone(bkgsum.GetName().replace("bkgsum", "errorband"))
    errorband.SetLineColor(ROOT.kBlack)
    errorband.SetFillColor(ROOT.kBlack)
    errorband.SetFillStyle(3645)
           
    setHistoStyle(MultiJet4Stack, color=ROOT.kGreen+1)
    setHistoStyle(tt4Stack, color=ROOT.kRed+1)
    setHistoStyle(minor4Stack, color=ROOT.kCyan)
    setHistoStyle(signals4Plot, color=ROOT.kAzure, isSignal = True)
    setHistoStyle(data4Plot, color=ROOT.kBlack, isData = True)
    
    stack.Add(MultiJet4Stack)
    stack.Add(tt4Stack)
    stack.Add(minor4Stack)

    logger.debug("tt4Stack: %s", tt4Stack)
    for ibin in range(tt4Stack.GetNbinsX()):
        logger.debug("  Bin %s: %s +- %s", ibin+1, tt4Stack.GetBinContent(ibin+1), tt4Stack.GetBinError(ibin+1))

    
    # logger.debug("Background sum: %s", bkgsum)
    # for ibin in range(bkgsum.GetNbinsX()):
    #     logger.debug("  Bin %s: %s +- %s", ibin+1, bkgsum.GetBinContent(ibin+1), bkgsum.GetBinError(ibin+1))

    
    # logger.debug("Errorband: %s", errorband)
    # for ibin in range(errorband.GetNbinsX()):
    #     logger.debug("  Bin %s: %s +- %s", ibin+1, errorband.GetBinContent(ibin+1), errorband.GetBinError(ibin+1))        
        
    setHistoStyle(bkgsum, color=ROOT.kBlack, isSignal = True)
 
    
    legendObjwText = []
    legendObjwText.append((deepcopy(MultiJet4Stack), "MultiJet ({0})".format(int(round(MultiJet4Stack.Integral())))))
    legendObjwText.append((deepcopy(tt4Stack), "#bar{t}t"+"({0})".format(int(round(tt4Stack.Integral())))))
    if addSignal:
        legendObjwText.append((deepcopy(minor4Stack), "Minor bkg + #bar{t}tH"+"({0})".format(int(round(minor4Stack.Integral())))))
    else:
        legendObjwText.append((deepcopy(minor4Stack), "Minor bkg ({0})".format(int(round(minor4Stack.Integral())))))
    if config.addSignalHisto:
        logging.info("Will add signal as separate histogram, scale by %s", config.signalScale)
        signals4Plot.Scale(config.signalScale)
        legendObjwText.append((deepcopy(signals4Plot), "Signal (x {0})".format(config.signalScale), "l"))
        toPass = [(signals4Plot,"histo")]
    else:
        toPass = None
    #legendObjwText.append((deepcopy(data4Plot), "Data"))
    legendObjwText.append((deepcopy(errorband), "Uncertainty"))

    if config.drawPulls or pulls:
        filePrefix += "_pulls"
    
    output = RatioPlot(name = filePrefix)
    if config.drawPulls or pulls:
        output.ratioRange =  (-4,4)
    #output.ratioRange =  (0.92,1.08)
    if  config.printTestVal:
        output.legendSize = (0.61, 0.48 , 0.89, 0.88)
        floatingMax = 1.4
    else:
        output.legendSize = (0.55, 0.58 , 0.89, 0.88)
        floatingMax = 1.6
    output.yTitle = config.varSettings[variable]["ytitle"]
    output.passHistos([data4Plot, stack])
    if config.sidebandVal:
        labelText = "FH {0}x ({1})".format(regionName, config.catName[category])
    else:
        addId = ""
        if STXSBin is not None:
            addId = ", STXS Bin %s"%STXSBin
        if not regionName == "SR":
            labelText = "FH {0} ({1}{2})".format(regionName, config.catName[category], addId)
        else:
            labelText = "FH ({0}{1})".format(config.catName[category], addId)
    output.addLabel(labelText, 0.12,0.96)
    output.CMSscale = (1.4, 1.4)
    output.moveCMS = +0.01
    output.addIntegral = True
    output.addChi2 = config.printTestVal
    output.addpVal = config.printTestVal
    output.invertRatio = True
    output.CMSLeft = True
    output.labelScaleX = 1.1
    output.yTitleOffset = 0.9
    output.yTitleOffsetRatio = 0.5
    output.xTitleOffsetScale = 1.1
    output.thisLumi = str(config.lumi)
    thisCanvas = output.drawPlot(None,
                                 xTitle = config.varSettings[variable]["xtitle"],
                                 isDataMCwStack = True,
                                 stacksum=bkgsum,
                                 errorband = errorband,
                                 histolegend = legendObjwText,
                                 drawPulls = (config.drawPulls or pulls),
                                 addHisto = toPass,
                                 noErr = True,
                                 drawCMS = True,
                                 floatingMax = floatingMax
    )

    saveCanvasListAsPDF([thisCanvas], "QCDEstimate_"+filePrefix,outdir)
    
def makeOutput(config, histos, histosCR, outdir, category, variable, filePrefix, writeROOT = False, pulls = False, logger = logging):
    logger.info("Making output")

    if "STXS" in category:
        thisSTXSBin = category.split("_")[-1]
    else:
        thisSTXSBin = None
    
    logger.info("Plotting SR")
    makePlot(config, histos, outdir, category, variable, filePrefix, "SR" if not config.doValidation else "VR",addSignal = config.includeSignal, pulls = pulls, STXSBin=thisSTXSBin)
    logRatios(histos)
    logger.info("Plotting CR")
    makePlot(config, histosCR, outdir, category, variable, filePrefix+"_CR", "CR" if not config.doValidation else "CR2", pulls = pulls, STXSBin=thisSTXSBin)
    logRatios(histosCR)
    
    if writeROOT:
        logger.info("Writing ROOT file with histograms")
        outFile = ROOT.TFile(outdir+"QCDEstimate_"+filePrefix+".root", "RECREATE")
        outFile.cd()
        for key in histos:
            histos[key].Write()
        for key in histosCR:
            histosCR[key].Write()
        outFile.Close
        
    
def logRatios(histos, logger = logging):
    logger.info("=====================")
    logger.info("====== Ratios: ======")
    logger.info("MultiJet : %s", histos["MultiJet"].Integral()/histos["data"].Integral())
    logger.info("tt : %s", histos["tt"].Integral()/histos["data"].Integral())
    logger.info("Minor : %s", histos["minor"].Integral()/histos["data"].Integral())
    logger.info("Signal : %s", histos["signals"].Integral()/histos["data"].Integral())
    logger.info("=====================")

    
def setHistoStyle(histo, color, isData = False, isSignal = False):
    if isData:
        histo.SetLineColor(color)
        histo.SetMarkerColor(color)
        histo.SetMarkerStyle(20)
        histo.SetMarkerSize(1)
    elif isSignal:
        histo.SetLineColor(color)
        histo.SetLineWidth(2)
    else:
        histo.SetLineColor(ROOT.kBlack)
        histo.SetFillColor(color)
        histo.SetFillStyle(1001)
    histo.GetYaxis().SetLabelSize(histo.GetYaxis().GetLabelSize()*1.4)
        
def getOutputInfo(config, categories, variables, tag):
    outputInfo = {}
    for cat in categories:
        outputInfo[cat] = {}
        for var in variables:
            prefix = "{0}_{1}".format(cat, var)
            if config.doValidation:
                prefix += "_VR"
            outputDir = "QCDEst/"+tag+"/"
            outputInfo[cat][var] = [var, prefix, outputDir]

    return outputInfo

def getHistosFromFile(rFileName, isSR, category, variable, logger = logging):
    rFile = ROOT.TFile.Open(rFileName)
    histos = {}
    prefix = "SR" if isSR else "CR"
    for key in rFile.GetListOfKeys():
        if not prefix in key.GetName():
            continue
        fullPrefix = prefix+"_"+category+"_"+variable+"_"
        processName = key.GetName().split(fullPrefix)[1]
        logger.debug("Key: %s - Prefix : %s - proc : %s", key.GetName(), fullPrefix, processName)
        histos[processName] = rFile.Get(key.GetName())

    return deepcopy(histos)

def runEstimationforVar(fileObj, config, var, cat, outputInfo, pulls, isMultiProcess=False):
    cleanvar, prefix, outputDir = outputInfo[cat][var]    
    if isMultiProcess:
        raise NotImplementedError
    #     logFile = logging.FileHandler(outputDir+'/loggerOuput_{0}_{1}.log'.format(cat, var))
    #     logging.getLogger('').addHandler(logFile)
    #     thisoLgger = logging.getLogger('Process logger')
    #     thisLogger.addHandler(logFile)
    # else:
    #     thisLogger = logging
    SRHistos, CRHistos = getEstimate(fileObj, config, var, cat)
    makeOutput(config, SRHistos, CRHistos, outputDir, cat, var, prefix, writeROOT = True, pulls = pulls)
    
    
def main(config, categories, variables, tag, pulls, runMultiProcessing=False, nProcesses=4):
    logging.info("Will run categories: %s", categories)
    logging.info("Will run variables: %s", variables)

    outputInfo = getOutputInfo(config, categories, variables, tag)
            
    logging.info("Getting trees:")
    fileObj = { "rFile" : {},
                "tree" : {}
    }
    for process in config.allProcs:
        logging.debug("Processing process: %s", process)
        fileObj["rFile"][process] = ROOT.TFile.Open(config.files[process])
        fileObj["tree"][process] = fileObj["rFile"][process].Get("tree")
        logging.debug("Got tree at %s", fileObj["tree"][process])

    logging.debug("---------------------------------------")
    #### Load macros used for TTree::Draw. The idea is to deactivate them in the config if they
    #### are part of the tree. In that case they are expected to be listed in the weight option
    if config.loadTriggerMarco:
        logging.debug("Loading macros (classes.h, functions.h, TriggerSR):")
        ROOT.gROOT.LoadMacro("classes.h")
        logging.warning('TriggerSFMulitEra* internalTriggerSF3D = new TriggerSFMulitEra("{0}", "{1}")'.format(int(config.era), "DeepFlav"))
        ROOT.gInterpreter.ProcessLine('TriggerSFMulitEra* internalTriggerSF3D = new TriggerSFMulitEra("{0}", "{1}")'.format(int(config.era), "DeepFlav"))
        ROOT.gROOT.LoadMacro("functions.h")
    if config.loadbCorrMacro:
        logging.debug("Loading bCorr macro")
        ROOT.gROOT.LoadMacro(config.bCorrMarco)

    logging.debug("---------------------------------------")
        
    if runMultiProcessing:
        #Create the pool with nProcesses
        logging.warning("----------- Mulitprocessing enabled -----------")
        #pool = multiprocessing.Pool(nProcesses)
        
    for cat in categories:
        logging.info("Processing category %s", cat)
        for var in variables:
            if not runMultiProcessing:
                logging.info("Processing variables %s", var)
                runEstimationforVar(fileObj, config, var, cat, outputInfo, pulls)
            else:
                raise NotImplementedError
                logging.info("Adding process for variable %s, cat %s to pool")
                # p = multiprocessing.Process(target=runEstimationforVar, args=(fileObj, config, var, cat, outputInfo, pulls, True))
                # p.start()
                
                
    return outputInfo
            
def reRun(config, categories, variables, tag, pulls):
    logging.info("Starting rerun from existing file")
    logging.info("Will check if requested files are present for all variables/categories")
    outputInfo = getOutputInfo(config, categories, variables, tag)
    inputFiles = {}
    for cat in categories:
        inputFiles[cat] = {}
        for var in variables:
            cleanvar, prefix, outputDir = outputInfo[cat][var]
            if not outputDir.endswith("/"):
                outputDir += "/"
            expectedFileName = "./"+outputDir+"QCDEstimate_"+prefix+".root"
            if os.path.exists(expectedFileName):
                logging.info("Found: %s", expectedFileName)
                inputFiles[cat][var] = expectedFileName
            else:
                raise RuntimeError("Could not find file for %s, %s. Expected %s"%(cat,var,expectedFileName))

    logging.info("Rerunning")
    for cat in categories:
        for var in variables:
            SRHistos = getHistosFromFile(inputFiles[cat][var], True, cat, var)
            CRHistos = getHistosFromFile(inputFiles[cat][var], False, cat, var)
            cleanvar, prefix, outputDir = outputInfo[cat][var]
            makeOutput(config, SRHistos, CRHistos, outputDir, cat, var, prefix, pulls = pulls)
        
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        #choice=[10,20,30,40,50,0],
        default=20
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "config file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Will be used for output",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--cats",
        action = "store",
        help = "categories to be run",
        type = str,
        nargs="+",
        default = ["7j4t", "8j4t", "9j4t"]
    )
    argumentparser.add_argument(
        "--vars",
        action = "store",
        help = "variables to be run",
        type = str,
        nargs="+",
        default = ["ht30"]
    )
    argumentparser.add_argument(
        "--rerunFile",
        action = "store_true",
        help = "If set, the script will attempt to read a previously saved output file for the same configuration. Will raise RuntimeError if not found",
    )
    argumentparser.add_argument(
        "--drawPulls",
        action = "store_true",
        help = "If set, Plots will be drawn with pulls instead of ratio",
    )
    argumentparser.add_argument(
        "--doMultiProcessing",
        action = "store_true",
        help = "If set,multiprocessing per cat/var combination will be enabled",
    )
    
    args = argumentparser.parse_args()
    
    initLogging(args.logging, funcLen = 21)

    thisConfig = Config(args.config)
    if args.vars[0] == "all":
        args.vars = thisConfig.variables
    
    #Check if requested categories and variables are defined in config
    for var in args.vars:
        if var not in thisConfig.variables:
            raise RuntimeError("Variable **%s** is not defined in config"%var)
    for cat in args.cats:
        if cat not in thisConfig.categories:
            raise RuntimeError("Category  **%s** is not defined in config"%cat)

    if args.rerunFile:
        reRun(thisConfig, args.cats, args.vars, args.tag, args.drawPulls)
    else:
        main(thisConfig, args.cats, args.vars, args.tag, args.drawPulls, args.doMultiProcessing)

    logging.info("Script finished")
