# MEAnalysis and FHDownstream Implementation details

## Where the MEM is calculated
In order to reduce the computing time, the MEM is not calculated for all events.

### Categories
Generally the MEM is only calculated in event passing certain cuts. This is set for each MEM hypothesis separately. The usual place to set this are the MEM configs in `MEAnalysis/python/MEAnalysis_cfg_heppy.py`. There the `MEMConfig` objects are defined and the `do_calculate` method is a lambda function for event selection

### Region
Furthermore the MEM is not calculated for all samples in all regions used in the analysis. This is mainly necessary for sample with a lot of signal-like events (ttHbb and ttbar --> Hadronic). This is defined in `MEAnalysis/python/BTagLRAnalyzer.py` (around L228 - Look for the `runSR`,`runCR` and `runTR`  variables).      
Per sample settings are realized via the `self.cfg_comp.name` variable. This corresponds to the samples name passed to the `MEAnalysis_heppy.py` on runtime. 
The standard cofiguration is the following:

| Dataset                  | SR | CR | TR |
|--------------------------|----|----|----|
| Data (JetHT and BTagCSV) | x  | x  | x  |
| ttH*                     | x  |    |    |
| ttbar                    | x  | x  |    |
| Other samples            | x  | x  |    |

There is also a a way to overwrite this behaviour via the `RegionSelectionAnalyzer`. For this, set the `"RegionSelection"` in the config from `None` to a list containing `"TR"`,`"CR"` or both. This will 

1. Overwrite the behaviour in `MEAnalysis/python/BTagLRAnalyzer.py` and 
2. reject all event from forhter analysis that are not in the set regions.

Important: The b-tagging requirement for CR and TR is checked using the `jets["btagWP"]` and `jets["looseBWP"]`configuration settings. So make sure to set these correctly.
