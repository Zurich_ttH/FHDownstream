import ROOT
import logging
import array
import time
from glob import glob
import os

from TTH.Plotting.Datacards.MiscClasses import BTagNormalization

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )


def addBTagNormalization(inputFile, skipevents, treeName, outputBranchName, sample, year):
    logging.info("Loading: %s", inputFile)
    fileName = inputFile.split("/")[-1]
    t0 = time.time()
    tdiff = time.time()
    rFile = ROOT.TFile(fileName, "update")
    logging.info("Using tree name %s", treeName)
    if not rFile.GetListOfKeys().Contains(treeName):
        logging.error("File does **not** contains tree: %s",treeName)
        return False
    tree = rFile.Get(treeName)
    logging.info("file %s, tree %s", rFile, tree)

    normArray = array.array("f", [-1])
    branch = tree.Branch(outputBranchName, normArray, outputBranchName+"/F")
    nEvents = tree.GetEntries()

    normalization = BTagNormalization(year)
    
    logging.info("Tree has %s entries", nEvents)
    for iev in range(skipevents, nEvents):
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f} | Time left {2:8f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
            tdiff = time.time()
        logging.debug("New Events - %s/%s", iev, nEvents-1)
        tree.GetEvent(iev)
        nJets =tree.__getattr__("numJets")
        ht = tree.__getattr__("ht30")
        ttCls = tree.__getattr__("ttCls")

        normArray[0] = normalization.getSF(sample, nJets, ht, ttCls)
        logging.debug("Got SF = %s for HT = %s | nJets = %s | ttCLS = %s ",  normArray[0], ht, nJets, ttCls)
        
        branch.Fill()

    logging.info("Saving File")
    rFile.cd()
    tree.Write("",ROOT.TFile.kOverwrite)
    logging.info("Total time to finish: {0:8f}".format(time.time()-t0))
    return True


if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for adding the b-tag reweighting SF in a event loop over a TTree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "File used for the b-tag SF",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--outBranch",
        action = "store",
        help = "Name the new branch",
        type = str,
        default="bTagNorm"
    )    
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--sample",
        action = "store",
        help = "Name of the TTree",
        type = str,
        required = True,
    )
    argumentparser.add_argument(
        "--year",
        action = "store",
        help = "Name of the TTree",
        type = str,
        required = True,
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)

    if args.sample in ["JetHT", "BTagCSV"]:
        logging.warning("Seems to be data sample. Will ignore this")
    else:
        addBTagNormalization(args.inputFile, args.skipevents, args.treeName, args.outBranch, args.sample, args.year )
