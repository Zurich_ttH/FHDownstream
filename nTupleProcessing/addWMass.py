import ROOT
import logging
import array
import time
from glob import glob
import os

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )

def pair_mass(j1, j2):
        """
        Calculates the invariant mass of a two-particle system.
        """
        lv1, lv2 = [j.lv for j in [j1, j2]]
        tot = lv1 + lv2
        return tot.M()
    
def find_best_pair(jets):
    """
    Finds the pair of jets whose invariant mass is closest to mW=80 GeV.
    Returns the sorted vector of [(mass, jet1, jet2)], best first.
    """
    ms = []
     
    #Keep track of index pairs already calculated
    done_pairs = set([])

    #Double loop over all jets
    for i in range(len(jets)):
        for j in range(len(jets)):

            #Make sure we haven't calculated this index pair yet
            if (i,j) not in done_pairs and i!=j:
                m = pair_mass(jets[i], jets[j])
                ms += [(m, jets[i], jets[j])]

                #M(i,j) is symmetric, hence add both pairs
                done_pairs.add((i,j))
                done_pairs.add((j,i))
    ms = sorted(ms, key=lambda x: abs(x[0] - 80.0))
    return ms


class Jet(object):
    def __init__(self, pt, eta, phi, mass, deepcsv, hadronFlavour):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.mass = mass
        self.deepcsv = deepcsv
        self.hadronFlavour = hadronFlavour

        self.lv = ROOT.TLorentzVector()
        self.lv.SetPtEtaPhiM(self.pt, self.eta, self.phi, self.mass)
            

def addWMass(inputFile, output, skipevents, updateInplace, treeName, outputBranchName, lenvar, brPrefix):
    logging.info("Loading: %s", inputFile)
    fileName = inputFile.split("/")[-1]
    t0 = time.time()
    if not updateInplace:
        logging.info("Executing: xrdcp {0} {1}{2}".format(inputFile, outputfolder, fileName.replace(".root","_mod.root")))
        os.system("xrdcp {0} {1}{2}".format(inputFile, outputfolder, fileName.replace(".root","_mod.root")))
        logging.info("Time to copy {0:6f}".format(time.time()-t0))
    tdiff = time.time()
    if not updateInplace:
        rFile = ROOT.TFile("{0}/{1}".format(outputfolder, fileName.replace(".root","_mod.root")), "update")
    else:
        rFile = ROOT.TFile(fileName, "update")
    logging.info("Using tree name %s", treeName)
    if not rFile.GetListOfKeys().Contains(treeName):
        logging.error("File does **not** contains tree: %s",treeName)
        return False
    tree = rFile.Get(treeName)
    logging.info("file %s, tree %s", rFile, tree)
    WmassArray = array.array("f", [-1])
    branch = tree.Branch(outputBranchName, WmassArray, outputBranchName+"/F")
    nEvents = tree.GetEntries()
    logging.info("Tree has %s entries", nEvents)
    for iev in range(skipevents, nEvents):
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f} | Time left {2:8f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
            tdiff = time.time()
        logging.debug("New Events - %s/%s", iev, nEvents-1)
        tree.GetEvent(iev)
        nJets =tree.__getattr__(lenvar)
        jets = []
        for i in range(nJets):
            jets.append( Jet(
                pt = tree.__getattr__(brPrefix+"_pt")[i],
                eta = tree.__getattr__(brPrefix+"_eta")[i],
                phi = tree.__getattr__(brPrefix+"_phi")[i],
                mass = tree.__getattr__(brPrefix+"_mass")[i],
                deepcsv =tree.__getattr__(brPrefix+"_btagDeepCSV")[i],
                hadronFlavour = 1,#Not needed here and like this it can run on data
            ))

        sortedJets = sorted(jets, key=lambda x: x.deepcsv, reverse = True)
        for i in range(nJets):
            logging.debug("Tree: pt %s, btag %s | Sorted: pt %s, btag %s",jets[i].pt,jets[i].deepcsv,sortedJets[i].pt,sortedJets[i].deepcsv)
        jetsForWmass = sortedJets[4:]
        ms = find_best_pair(jetsForWmass)
        logging.debug("Event: nBDeepCSVM %s | nBDeepCSVL %s",  tree.nBDeepCSVM, tree.nBDeepCSVL)
        try:
            ms[0][0]
        except IndexError:
            WmassArray[0] = -99999
        else:
            WmassArray[0] = ms[0][0]
        logging.debug("Found Wmass = %s (Wmass in tree: %s)",  WmassArray[0], tree.Wmass)
        
        branch.Fill()
        
    logging.info("Saving File")
    rFile.cd()
    tree.Write("",ROOT.TFile.kOverwrite)
    logging.info("Total time to finish: {0:8f}".format(time.time()-t0))
    return True
        
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for adding the b-tag reweighting SF in a event loop over a TTree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        help = "path to folder where the output will be saved",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "File used for the b-tag SF",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--inplace",
        action = "store_true",
        help = "Update file inplace --> --output will not be used but is required to set anyways (for convinience)",
    )
    argumentparser.add_argument(
        "--outBranch",
        action = "store",
        help = "Name the new branch",
        type = str,
        default="Wmass4b"
    )    
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--jetLenVar",
        action = "store",
        help = "Lenvar for the jets in the tree",
        type = str,
        default="njets"
    )
    argumentparser.add_argument(
        "--jetPrefix",
        action = "store",
        help = "Prefix of the jet branch (Expecting something like jets_pt, where prefix is before _)",
        type = str,
        default="jets"
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)

    addWMass(args.inputFile, args.output, args.skipevents, args.inplace, args.treeName, args.outBranch, args.jetLenVar, args.jetPrefix,)
