import ROOT
import logging
import array
import time
from glob import glob
import os
import itertools

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )


class Jet(object):
    def __init__(self, pt, mass, eta, phi, bTagValue, bTagIndex, hadronFlavour, matchFlag):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.mass = mass
        self.bTagValue = bTagValue
        self.hadronFlavour = hadronFlavour
        self.matchFlag = matchFlag
        self.bTagIndex = bTagIndex
        
        self.lv = ROOT.TLorentzVector()
        self.lv.SetPtEtaPhiM(self.pt, self.eta, self.phi, self.mass)

        
def addRecoHiggs(inputFile, skipevents, treeName, outputBranchNamePrefix, lenvar, brPrefix, mu, sigma, mediumWP, looseWP, onlyLeading4):
    logging.info("Loading: %s", inputFile)
    fileName = inputFile.split("/")[-1]
    t0 = time.time()
    tdiff = time.time()
    rFile = ROOT.TFile(fileName, "update")
    logging.info("Using tree name %s", treeName)
    if not rFile.GetListOfKeys().Contains(treeName):
        logging.error("File does **not** contains tree: %s",treeName)
        return False
    tree = rFile.Get(treeName)
    logging.info("file %s, tree %s", rFile, tree)

    if onlyLeading4:
        logging.warning("Only using the 4 leading b-jets")
    
    MassArray = array.array("f", [-1])
    branchMass = tree.Branch(outputBranchNamePrefix+"Mass", MassArray, outputBranchNamePrefix+"Mass/F")
    PtArray = array.array("f", [-1])
    branchPt = tree.Branch(outputBranchNamePrefix+"Pt", PtArray, outputBranchNamePrefix+"Pt/F")
    nEvents = tree.GetEntries()
    logging.info("Tree has %s entries", nEvents)
    for iev in range(skipevents, nEvents):
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:.2f} | Diff time {2:.2f} | Time left {2:.2f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
            tdiff = time.time()
        logging.debug("New Events - %s/%s", iev, nEvents-1)
        tree.GetEvent(iev)

        MassArray[0] = -1
        PtArray[0] = -1
        
        nJets =tree.__getattr__(lenvar)
        jets = []
        for i in range(nJets):
            jets.append( Jet(
                tree.__getattr__(brPrefix+"_pt")[i],
                tree.__getattr__(brPrefix+"_mass")[i],
                tree.__getattr__(brPrefix+"_eta")[i],
                tree.__getattr__(brPrefix+"_phi")[i],
                tree.__getattr__(brPrefix+"_btagDeepFlav")[i],
                tree.__getattr__(brPrefix+"_DeepFlavindex")[i],
                0.0,
                0.0,
            )
            )
            
        bJets = []
        for jet in jets:
            if jet.bTagValue >= mediumWP:
                bJets.append(jet)

        bJets = sorted(bJets, key=lambda jet: jet.bTagValue, reverse=True)

        if len(bJets) < 4:
            addbJets = []
            for jet in jets:
                if jet.bTagValue < mediumWP and jet.bTagValue >= looseWP:
                    addbJets.append(jet)
                    
            addbJets = sorted(addbJets, key=lambda jet: jet.bTagValue, reverse=True)
            bJets += addbJets

        if onlyLeading4:
            bJets = bJets[0:4]

        combs = itertools.combinations(range(len(bJets)),2)
        bestMass = 0.0
        bestPt = 0.0
        minimumComb = None
        minimumScore = 999999999.9
        for comb in combs:
            score = pow((bJets[comb[0]].lv+bJets[comb[1]].lv).M() - mu, 2)/pow(sigma,2)
            if score < minimumScore:
                minimumScore = score
                minimumComb = comb
                bestMass = (bJets[comb[0]].lv+bJets[comb[1]].lv).M()
                bestPt = (bJets[comb[0]].lv+bJets[comb[1]].lv).Pt()

        logging.debug("Best comb: %s - Mass = %s | Pt = %s", minimumComb, bestMass, bestPt)
    
        MassArray[0] = bestMass
        PtArray[0] = bestPt
        

        branchPt.Fill()
        branchMass.Fill()

    logging.info("Saving File")
    rFile.cd()
    tree.Write("",ROOT.TFile.kOverwrite)
    logging.info("Total time to finish: {0:8f}".format(time.time()-t0))
    return True


if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for adding the reconsitructed Higgs Mass and Pt to a  TTree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--mu",
        action = "store",
        help = "Mu for the chi2 reco",
        type = float,
        required=True,
    )
    argumentparser.add_argument(
        "--sigma",
        action = "store",
        help = "Sigma for the chi2 reco",
        type = float,
        required=True,
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "File used for the b-tag SF",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--outBranchPrefix",
        action = "store",
        help = "Name the new branch",
        type = str,
        default="RecoHiggs"
    )    
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--jetLenVar",
        action = "store",
        help = "Lenvar for the jets in the tree",
        type = str,
        default="njets"
    )
    argumentparser.add_argument(
        "--jetPrefix",
        action = "store",
        help = "Prefix of the jet branch (Expecting something like jets_pt, where prefix is before _)",
        type = str,
        default="jets"
    )
    argumentparser.add_argument(
        "--mediumWP",
        action = "store",
        help = "Medium working point for b-tagging - Defaultis 2017 DeepFlav",
        type = float,
        default = 0.3033,
    )
    argumentparser.add_argument(
        "--looseWP",
        action = "store",
        help = "Loose working point for b-tagging - Defaultis 2017 DeepFlav",
        type = float,
        default = 0.0521,
    )
    argumentparser.add_argument(
        "--doLeadingBJets",
        action = "store_true",
        help = "If passed, only the 4 jets with highest b-tag value will be used for the calculation",
    )

    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)

    addRecoHiggs(inputFile = args.inputFile, 
                 skipevents = args.skipevents, 
                 treeName = args.treeName, 
                 outputBranchNamePrefix = args.outBranchPrefix, 
                 lenvar = args.jetLenVar, 
                 brPrefix = args.jetPrefix, 
                 mu = args.mu, 
                 sigma = args.sigma, 
                 mediumWP = args.mediumWP, 
                 looseWP = args.looseWP,
                 onlyLeading4 = args.doLeadingBJets)
