import ROOT
import logging
import array
import time 
from testbTagWeights import BtagWeightAnalyzer, Jet
from glob import glob
import os 
def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )


class bTagNorm(object):
    def __init__(self, basePath, sample, hname = "hWeightNJets_central"):
        logging.info("Loading bTagNorm class for sample %s", sample)
        self.hName = hname
        files = glob(basePath+"/*.root")
        self.fileName = None
        for f in files:
            if sample in f:
                logging.info("Will use file %s for normalization factors", f)
                self.fileName = f
        if self.fileName is not None:
            self.rFile = ROOT.TFile.Open(self.fileName)
            self.weightHisto = self.rFile.Get(self.hName)

    def getSF(self, nJets):
        if self.fileName is None:
            return 1.0

        thisBin = -1
        SF = -1
        if nJets <= 12:
            SF = self.weightHisto.GetBinContent(self.weightHisto.FindBin(nJets))
        else:
            logging.debug("Found event with more than 12 jets. Will set SF to 1")
            SF = 1.0
        if SF == -1:
            logging.warning("SF is -1! Will set it back to 1")
            SF = 1.0

        return SF
    
    
def addWeights(inputFile, bTagCSVFile, outputfolder, skipevents, nomOnly, updateInplace, treeName, lenvar, brPrefix, bDiscName, kinVariables, eventDiagnostics, outputBranchName, addNorm, normPath, sample, addSF):
    if addNorm and not nomOnly:
        raise NotImplementedError("Adding normalization currently only implemented for nominal")
    logging.info("Loading: %s", inputFile)
    fileName = inputFile.split("/")[-1]
    t0 = time.time()
    if not updateInplace:
        logging.info("Executing: xrdcp {0} {1}{2}".format(inputFile, outputfolder, fileName.replace(".root","_mod.root")))
        os.system("xrdcp {0} {1}{2}".format(inputFile, outputfolder, fileName.replace(".root","_mod.root")))
        logging.info("Time to copy {0:6f}".format(time.time()-t0))
    tdiff = time.time()
    kinVarPt = kinVariables[0]
    kinVarEta = kinVariables[1]
    kinVarFlav = kinVariables[2]
    logging.info("Will use -- %s -- for PT, -- %s -- for ETA, -- %s -- for FALVOUR", kinVariables[0], kinVariables[1], kinVariables[2])
    logging.info("Will use -- %s -- as b-tag disciminator", bDiscName)
    if not updateInplace:
        rFile = ROOT.TFile("{0}/{1}".format(outputfolder, fileName.replace(".root","_mod.root")), "update")
    else:
        rFile = ROOT.TFile(inputFile, "update")
    logging.info("Using tree name %s", treeName)
    rFile.ls()
    if not rFile.GetListOfKeys().Contains(treeName):
        logging.error("File does **not** contains tree: %s",treeName)
        return False
    tree = rFile.Get(treeName)
    logging.info("file %s, tree %s", rFile, tree)


    #outFile = ROOT.TFile("{0}/{1}".format(outputfolder, fileName.replace(".root","bSF.root")), "RECREATE")
    #outFile.cd()
    #outTree = ROOT.TTree(treeName, treeName)
    if addSF:
        SFArray = array.array("f", [-1])
        branch = tree.Branch(outputBranchName, SFArray, outputBranchName+"/F")
        analyzer = BtagWeightAnalyzer(bTagCSVFile, nomOnly)
        if nomOnly:
            analyzer.bTagSyst = []
        if bDiscName == "btagCSV":
            analyzer.discType = "CSVv2"

    if addNorm:
        nromArray = array.array("f", [-1])
        normBranch = tree.Branch(outputBranchName+"_norm", nromArray, outputBranchName+"_norm/F")
        normFactors = bTagNorm(normPath, sample)    

    nEvents = tree.GetEntries()
    logging.info("Tree has %s entries", nEvents)
    for iev in range(skipevents, nEvents):
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f} | Time left {2:8f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
            tdiff = time.time()
        logging.debug("New Events - %s/%s", iev, nEvents-1)
        tree.GetEvent(iev)
        diagnosticStr = ""
        for var in eventDiagnostics:
            diagnosticStr += "{0}: {1} |".format(var, tree.__getattr__(var))
        logging.debug(diagnosticStr)
        nJets =tree.__getattr__(lenvar)
        jets = []
        for i in range(nJets):
            jets.append( Jet(
                tree.__getattr__(brPrefix+"_"+kinVariables[0])[i],
                tree.__getattr__(brPrefix+"_"+kinVariables[1])[i],
                tree.__getattr__(brPrefix+"_"+bDiscName)[i],
                tree.__getattr__(brPrefix+"_"+kinVariables[2])[i],
            ))
        if addSF:
            eventSFs = analyzer.clacSF(jets)
            SFArray[0] = eventSFs["central"]
            branch.Fill()
        if addNorm:
            nromArray[0] = normFactors.getSF(nJets)
            normBranch.Fill()
        #outTree.Fill()
    logging.info("Saving File")
    rFile.cd()
    tree.Write("",ROOT.TFile.kOverwrite)
    logging.info("Total time to finish: {0:8f}".format(time.time()-t0))
    return True
    
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for adding the b-tag reweighting SF in a event loop over a TTree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        help = "path to folder where the output will be saved",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--SFFile",
        action = "store",
        help = "File used for the b-tag SF",
        type = str,
        default = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/MEAnalysis/data/deepCSV_sfs_JECV32.csv"
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "File used for the b-tag SF",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--onlyNominal",
        action = "store_true",
        help = "If argumentis passed, only then nominal SF will be considered",
    )
    argumentparser.add_argument(
        "--inplace",
        action = "store_true",
        help = "Update file inplace --> --output will not be used but is required to set anyways (for convinience)",
    )
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--jetLenVar",
        action = "store",
        help = "Lenvar for the jets in the tree",
        type = str,
        default="njets"
    )
    argumentparser.add_argument(
        "--jetPrefix",
        action = "store",
        help = "Prefix of the jet branch (Expecting something like jets_pt, where prefix is before _)",
        type = str,
        default="jets"
    )
    argumentparser.add_argument(
        "--disc",
        action = "store",
        help = "Branch postfix of the b-tag discriminator",
        type = str,
        default="btagDeepFlav"
    )

    argumentparser.add_argument(
        "--kinVariables",
        action = "store",
        help = "Branch postfix of pt, eta, flav (in this order!)",
        type = str,
        nargs=3,
        default=["pt", "eta", "hadronFlavour"]
    )
    argumentparser.add_argument(
        "--diagnostics",
        action = "store",
        help = "Branch of that will be shown in for each event",
        type = str,
        nargs="+",
        default=["njets", "is_fh", "nBDeepFlavM"]
    )
    argumentparser.add_argument(
        "--SFBranchName",
        action = "store",
        help = "Name of the event SF output branch",
        type = str,
        default = "bTagSF"
    )
    argumentparser.add_argument(
        "--addNorm",
        action = "store_true",
        help = "Set to also add the b.tag normalization to nTuple",
    )
    argumentparser.add_argument(
        "--addSF",
        action = "store_true",
        help = "Set to addthe b.tag SF to nTuple",
    )
    argumentparser.add_argument(
        "--normPath",
        action = "store",
        help = "Path to b-tag normalization files",
        type = str,
        default = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/MEAnalysis/data/bTagNormv5"
    )
    argumentparser.add_argument(
        "--sample",
        action = "store",
        help = "Only required for normalization",
        type = str,
        default = "NoSample"
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)
    if not args.sample in ["SingleMuon", "JetHT", "BTagCSV"]:
        addWeights(args.inputFile, args.SFFile, args.output, args.skipevents, args.onlyNominal, args.inplace, args.treeName, args.jetLenVar, args.jetPrefix, args.disc, args.kinVariables, args.diagnostics, args.SFBranchName, args.addNorm, args.normPath, args.sample, args.addSF)
    else:
        logging.warning("Skipped addbTagWeights because name is classified as data")
