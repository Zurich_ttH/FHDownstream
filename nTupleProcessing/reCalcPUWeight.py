import ROOT
import logging
import array
import time
from glob import glob
import os

from TTH.Plotting.Datacards.MiscClasses import PUWeightProducer

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )


def reCalcPUWeight(inputFile, outputfolder, skipevents, updateInplace, treeName, outputBranchName, year="2016"):
    logging.info("Loading: %s", inputFile)
    fileName = inputFile.split("/")[-1]
    t0 = time.time()
    if not updateInplace:
        logging.info("Executing: xrdcp {0} {1}{2}".format(inputFile, outputfolder, fileName.replace(".root","_mod.root")))
        os.system("xrdcp {0} {1}{2}".format(inputFile, outputfolder, fileName.replace(".root","_mod.root")))
        logging.info("Time to copy {0:6f}".format(time.time()-t0))
    tdiff = time.time()
    if not updateInplace:
        rFile = ROOT.TFile("{0}/{1}".format(outputfolder, fileName.replace(".root","_mod.root")), "update")
    else:
        rFile = ROOT.TFile(fileName, "update")
    logging.info("Using tree name %s", treeName)
    if not rFile.GetListOfKeys().Contains(treeName):
        logging.error("File does **not** contains tree: %s",treeName)
        return False
    tree = rFile.Get(treeName)
    logging.info("file %s, tree %s", rFile, tree)
    newPUWeightArray = array.array("f", [-1])
    branch = tree.Branch(outputBranchName, newPUWeightArray, outputBranchName+"/F")
    nEvents = tree.GetEntries()
    logging.info("Tree has %s entries", nEvents)
    
    if year == "2016":
        pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupData_GoldenJSON_Full2016.root" % os.environ['CMSSW_BASE']
    elif year == "2017":
        pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupHistogram-goldenJSON-13tev-2018-99bins_withVar.root" % os.environ['CMSSW_BASE']
    elif year == "2018":
        pufile_data = "%s/src/PhysicsTools/NanoAODTools/python/postprocessing/data/pileup/PileupHistogram-goldenJSON-13tev-2018-100bins_withVar.root" % os.environ['CMSSW_BASE']
    else:
        raise RuntimeError("Invalid PUweightEra: %s"%PUweightEra)
    puWeight = PUWeightProducer([inputFile], pufile_data)

    nanoEvents = rFile.Get("nanoAOD/Events")
    nanoEvents.BuildIndex("event")

    
    for iev in range(skipevents, nEvents):
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f} | Time left {2:8f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
            tdiff = time.time()
        logging.debug("New Events - %s/%s", iev, nEvents-1)
        tree.GetEvent(iev)

        if tree.event < 0:
            raise RuntimeError("Found event with number %s --> Probably long issue", tree.event)
        
        nanoEvents.GetEntryWithIndex(tree.event)
        thisEventPuWeights = puWeight.getWeight(nanoEvents.Pileup_nTrueInt)
        nomWeight, _, _ = thisEventPuWeights
        
        newPUWeightArray[0] = nomWeight

        logging.debug("Found Weight = %s (weight in tree in tree: %s)",   newPUWeightArray[0],  tree.puWeight)
        
        branch.Fill()
        
    logging.info("Saving File")
    rFile.cd()
    tree.Write("",ROOT.TFile.kOverwrite)
    logging.info("Total time to finish: {0:8f}".format(time.time()-t0))
    return True



if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for recalculating the PUweight using the true vertices saved in nanoAOD',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        help = "path to folder where the output will be saved",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "File used for the b-tag SF",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--inplace",
        action = "store_true",
        help = "Update file inplace --> --output will not be used but is required to set anyways (for convinience)",
    )
    argumentparser.add_argument(
        "--outBranch",
        action = "store",
        help = "Name the new branch",
        type = str,
        default="weightPURecalc"
    )
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--year",
        action = "store",
        help = "Era",
        type = str,
        default="2016",
        choices = ["2016","2017","2018"]
    )
    argumentparser.add_argument(
        "--dataset",
        action = "store",
        help = "Dataset anem (also used in GC)",
        type = str,
        required = True
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)

    #Qucikly analyzer datasetname to skip data:
    if "JetHT" in args.dataset or "BTagCSV" in args.dataset:
        logging.warning("Skipping %s because identified as data", args.dataset)
    else:
        reCalcPUWeight(args.inputFile, args.output, args.skipevents, args.inplace, args.treeName, args.outBranch, args.year)
