from __future__ import division

import logging
import os
import copy
from array import array
import numpy as np
import math
import scipy
import scipy.special
import itertools
from scipy.special import eval_legendre
from glob import glob
from collections import OrderedDict

import ROOT

from TTH.Plotting.Datacards.MiscClasses import BTagNormalization, TriggerSFCalculator
from TTH.Plotting.Datacards.MiscFunctions import btagCorrData, btagCorrMC, btagCorrDataUp, btagCorrMCUp

class ProcessModule(object):
    """
    Top level module for provessing event inforamtion 
    """
    def __init__(self, branchNames, datatypes, arraylens, initVals):
        assert len(branchNames) == len(datatypes)
        assert len(branchNames) == len(arraylens)
        assert len(branchNames) == len(initVals)

        self.isArrayBranch = False
        self.valArrays = {}
        self.branchNames = OrderedDict()
        self.dataTypes = {}
        self.initVals = {}
        self.arraylens = {}
        self.indexer = {}
        for iBranch in range(len(branchNames)):
            datatype = datatypes[iBranch]
            indexer = None
            if isinstance(datatype, tuple):
                indexer, datatype = datatype
            elif isinstance(datatype, str):
                pass
            else:
                raise TypeError
            arraylen = arraylens[iBranch]
            initVal = initVals[iBranch]
            branchname = branchNames[iBranch]
            
            if datatype == "F":
                arraytype = "f"
            elif datatype == "I":
                arraytype = "i"
            elif datatype == "l":
                arraytype = "l"
            else:
                raise NotImplementedError

            self.valArrays[branchname] = array(arraytype, arraylen*[initVal])
            logging.debug("Set up array %s - Type %s, len %s, initVal %s",
                         self.valArrays[branchNames[iBranch]],
                         arraytype, arraylen, initVal)
            self.initVals[branchname] = initVal
            self.arraylens[branchname] = arraylen
            self.branchNames[branchname] = branchname
            self.dataTypes[branchname] = datatype
            self.indexer[branchname] = indexer

        # print self.valArrays
        # print self.branchNames
        # print self.dataTypes

    def resetArrays(self):
        """
        Should be called in Event loop for before executing modules. Otherwise array can still be filled from last event
        """
        for key in self.branchNames:
            #print key, self.arraylens[key], self.valArrays[key][0]
            for iElem in range(self.arraylens[key]):
                self.valArrays[key][iElem] = self.initVals[key]


    ### Defintion of some memeber function that could be usefull in multiple inheriting classes
    @staticmethod
    def getDeltaR(jets, getDict=False, debug=False):
        allPerm = itertools.permutations(range(len(jets)), 2)
        allDeltaR = []
        allDeltaRDict = {}
        for perm in allPerm:
            i,j = perm
            dR = jets[i].lv.DeltaR(jets[j].lv)
            allDeltaR.append(dR)
            allDeltaRDict[dR] = perm
        return allDeltaR if not getDict else allDeltaRDict

    @staticmethod
    def getJetPairMasses(jets, getDict=False, debug=False):
        allPerm = itertools.permutations(range(len(jets)), 2)
        masses = []
        massesDict = {}
        for perm in allPerm:
            i,j = perm
            mass = (jets[i].lv + jets[j].lv).M()
            masses.append(mass)
            massesDict[mass] = perm

        return masses if not getDict else massesDict
                
class MEM(ProcessModule):
    """
    Recalculate the MEM with new kappa values and save with same name for all hypothesis
    """
    def __init__(self, branchname, datatype = "F", initVal = -1):
        logging.info("Initializing MEM module")
        super(MEM, self).__init__([branchname], [datatype], [1], [initVal])
        
        self.MEMHypos = {7 : "FH_3w2h2t_p",
                         8 : "FH_3w2h2t_p",
                         9 : "FH_4w2h2t_p"}
        self.kappas = {7 : 0.065, 8: 0.065, 9 : 0.065}
        self.nJetsBranch = "njets"
        self.BranchName = branchname

    def getValue(self, nJets, obj, forEventModel=False, debug=False):
        if nJets < 7:
            return -1
        memHypoKey = nJets
        if memHypoKey > 9:
            memHypoKey = 9
        memHypo = self.MEMHypos[memHypoKey]
        memKappa = self.kappas[memHypoKey]

        if not forEventModel:
            hypottH = obj.__getattr__("mem_tth_"+memHypo)
            hypottbb = obj.__getattr__("mem_ttbb_"+memHypo)
        else:
            hypottH = getattr(obj, "mem_tth_"+memHypo)
            hypottbb = getattr(obj, "mem_ttbb_"+memHypo)

        denom = (hypottH + memKappa * hypottbb)
        if denom > 0:
            MEMVal = hypottH / denom
        else:
            MEMVal = 0.0
        if debug:
                logging.debug("Hypo : %s |Result: %s [ttH %s, ttbb %s, njets %s]",
                              memHypoKey,
                              MEMVal,
                              hypottH,
                              hypottbb,
                              nJets
            )
        return MEMVal
    
    def fillArray(self, tree, debug=False):
        nJets = tree.__getattr__(self.nJetsBranch)
        memHypoKey = None
        for hypo in self.MEMHypos.keys():
            if nJets == hypo:
                memHypoKey = hypo
        if memHypoKey is None and nJets > self.MEMHypos.keys()[-1]:
            memHypoKey = self.MEMHypos.keys()[-1]
        if memHypoKey is not None :
            self.valArrays[self.BranchName][0] = self.getValue(nJets, tree, memHypoKey)
            
        else:
            if debug:
                logging.debug("Event did not pass jet selection")


class DEtaJVars(ProcessModule):
    def __init__(self, branchPrefix):
        logging.info("Initializing module calculating DeltaEtaJets variables")
        self.newVariables = ["Detaj_5", "Detaj_6", "Detaj_7"]
        self.newVariables = [branchPrefix+varName for varName in self.newVariables]
        self.branchPrefix = branchPrefix
        self.Detaj = None
        logging.debug("Variables: %s", self.newVariables)
        newVariableTypes = len(self.newVariables)*["F"]
        initVals = len(self.newVariables)*[-1]
        super(DEtaJVars, self).__init__(self.newVariables, newVariableTypes, len(self.newVariables)*[1], initVals)


    def fillArray(self, jets, debug=False):
        """
        Fill all arrays for the variables defined in self.newVariables
        Args:
            jets (list) : This expects a list of jets that are considered for the calculation
        """
        self.calcDetaJs(jets)
        for variable in self.newVariables:
            num = int(variable.split("_")[-1])
            if debug:
                logging.debug("Getting %s from %s (len: %s)", num, self.Detaj, len(self.Detaj))
            if num < len(jets):
                self.valArrays[variable][0] = self.Detaj[num]
            else:
                self.valArrays[variable][0] = -2.0

    def calcDetaJs(self, jets):
        njets = len(jets)
        Detaj = [0.0 for x in range(njets)]
        for j1 in jets:
            detamin = [99 for x in range(njets-1)]
            for j2 in jets:
                if j1==j2:
                    continue
                #calcualte delta Eta variables
                deta = abs(j1.eta - j2.eta)
                for x in range(njets-1):
                    if deta < detamin[x]:
                        for k in range((njets-2),(x-1),-1):
                            if k==x:
                                detamin[k] = deta
                            else:
                                detamin[k] = detamin[k-1]
                        break
            for k in range(njets-1):
                Detaj[k] += detamin[k]
        for k in range(njets-1):
            Detaj[k] = Detaj[k] / njets
        self.Detaj = Detaj

class JetEventVars(ProcessModule):
    """
    Module for calculating jet vent variables

    The idea here is that all variables defined in self.newVariables require class methods with the same name.
    These methods will be called in the fillArray method that runs in the event loop. If a variable has no
    corresponding function, a AttributeError will be raised. All variable class methods are expected to take 
    the same arguments (in the same order) so they can be called from a loop unaware of the exact method.
    """
    def __init__(self, branchPrefix):
        logging.info("Initializing module calculating event b variables")
        self.newVariables = ["HT", "Avg_bVal", "Min_bVal", "Max_bVal","Sq_diffAvgbVal",
                             "DeltaR_jj_max", "DeltaR_jj_min", "DeltaR_jj_avg","Mass_DeltaR_jj_min",
                             "Mass_DeltaR_jj_H", "Mass_jj_avg", "Mass_jj_max", "Mass_jj_min"]
        self.newVariables = [branchPrefix+varName for varName in self.newVariables]
        self.branchPrefix = branchPrefix
        logging.debug("Variables: %s", self.newVariables)
        newVariableTypes = len(self.newVariables)*["F"]
        initVals = len(self.newVariables)*[-1]
        super(JetEventVars, self).__init__(self.newVariables, newVariableTypes, len(self.newVariables)*[1], initVals)

    def fillArray(self, jets, debug=False):
        """
        Fill all arrays for the variables defined in self.newVariables
        Args:
            jets (list) : This expects a list of jets that are considered b-tagged!
        """
        for variable in self.newVariables:
            if len(jets) > 1:
                self.valArrays[variable][0] = getattr(self, variable.replace(self.branchPrefix, ""))(jets, debug)

    @staticmethod
    def HT(jets, debug=False):
        return sum([jet.pt for jet in jets])

    ################ Methods returning the b-tagging variables ################
    @staticmethod
    def Avg_bVal(jets, debug=False):
        return sum([jet.btag for jet in jets])/len(jets)        

    @staticmethod
    def Min_bVal(jets, debug=False):
        return min([jet.btag for jet in jets])

    @staticmethod
    def Max_bVal(jets, debug=False):
        return max([jet.btag for jet in jets])

    @staticmethod
    def Sq_diffAvgbVal(jets, debug=False):
        avg = sum([jet.btag for jet in jets])/len(jets)
        res = 0
        for jet in jets:
            res += (jet.btag - avg)**2

        return res/len(jets)
    ################ Methods returning DeltaR variables ################
    def DeltaR_jj_max(self, jets, debug=False):
        deltaRs = self.getDeltaR(jets)
        return max(deltaRs)

    def DeltaR_jj_min(self, jets, debug=False):
        deltaRs = self.getDeltaR(jets)
        #logging.debug("Getting DeltaR_hjj_min = %s", deltaRs)#TEMP REMOVE
        return min(deltaRs)

    def DeltaR_jj_avg(self, jets, debug=False):
        deltaRs = self.getDeltaR(jets)
        return sum(deltaRs)/len(deltaRs)

    ################ Methods returning Mass variables ################
    ################ Jet Masses ################
    @staticmethod
    def Avg_mass(jets, debug=False):
        return sum([jet.mass for jet in jets])/len(jets)

    ################ Dijet Masses ################
    def Mass_DeltaR_jj_min(self, jets, debug=False):
        deltaRs = self.getDeltaR(jets, getDict=True)
        minDR = min(deltaRs.keys())
        i,j = deltaRs[minDR]
        #logging.debug("Getting Mass_DeltaR_jj_min = %s", (jets[i].lv + jets[j].lv).M())#TEMP REMOVE
        return (jets[i].lv + jets[j].lv).M()

    @staticmethod
    def Mass_DeltaR_jj_H(jets, debug=False):
        allPerm = itertools.permutations(range(len(jets)), 2)
        bestMass = None
        for perm in allPerm:
            i,j = perm
            mij = (jets[i].lv + jets[j].lv).M()
            if bestMass is None:
                bestMass = mij
            else:
                if abs(125-mij) < abs(125-bestMass):
                    bestMass = mij
        return bestMass

    def Mass_jj_avg(self, jets, debug=False):
        masses = self.getJetPairMasses(jets)
        return sum(masses)/len(masses)
        
    def Mass_jj_max(self, jets, debug=False):
        masses = self.getJetPairMasses(jets)
        return max(masses)

    def Mass_jj_min(self, jets, debug=False):
        masses = self.getJetPairMasses(jets)
        #logging.debug("Getting Mass_jj_min = %s", masses)#TEMP REMOVE
        return min(masses)
    
    
class JetVars(ProcessModule):
    """
    Modules for adding new per Jet variables

    Two ways of definint variables: 
    1.) var_num , where var is element in ["pt", "eta", "phi", "mass", "btag"] and num is index of jet collection
    2.) name, where name is a member function of this class (see JetEventVars staticmethods)
    """
    def __init__(self, branchPrefix):
        logging.info("Initializing modules for adding new per jet variables")
        self.newVariables = ["pt_0", "eta_0","pt_1", "eta_1","btag_0","btag_1"]
        self.newVariables = [branchPrefix+varName for varName in self.newVariables]
        self.branchPrefix = branchPrefix
        logging.debug("Variables: %s", self.newVariables)
        newVariableTypes = len(self.newVariables)*["F"]
        initVals = len(self.newVariables)*[-1]
        super(JetVars, self).__init__(self.newVariables, newVariableTypes, len(self.newVariables)*[1], initVals)

    def getValues(self, jets, debug=False):
        results = {}
        for variable in self.newVariables:
            splitVar = variable.replace(self.branchPrefix, "").split("_")
            if len(splitVar) == 2 and splitVar[0] in ["pt", "eta", "phi", "mass", "btag"]:
                #print(jets, splitVar)
                if int(splitVar[1]) > len(jets)-1:
                    results[variable] = -99999999
                else:
                    results[variable] = getattr(jets[int(splitVar[1])], splitVar[0], None)

        return results
    
    def fillArray(self, jets, debug=False):
        defaultVals = self.getValues(jets, debug)
        for variable in self.newVariables:
            if variable in defaultVals:
                result = defaultVals[variable]
                if debug:
                    logging.debug("Filling %s with %s", variable, result)
                if result is not None: #This way the default value of the array stays intact
                    self.valArrays[variable][0] = result
            else:
                self.valArrays[variable][0] = getattr(self, variable.replace(self.branchPrefix, ""))(jets, debug)

class JetEnergy(ProcessModule):
    def __init__(self, branchPrefix, nJets):
        logging.info("Initializing modules for calculating energy per jet from pt, eta, phi and mass")
        self.nJets = nJets
        self.newVariables = [branchPrefix+"_energy_"+str(i) for i in range(nJets)]
        self.branchPrefix = branchPrefix
        logging.debug("Variables: %s", self.newVariables)
        newVariableTypes = len(self.newVariables)*["F"]
        initVals = len(self.newVariables)*[-1]
        super(JetEnergy, self).__init__(self.newVariables, newVariableTypes, len(self.newVariables)*[1], initVals)

    def fillArray(self, jets, debug=False):
        for iJet in range(len(jets)):
            if iJet >= self.nJets:
                continue
            self.valArrays[self.branchPrefix+"_energy_"+str(iJet)][0] = jets[iJet].lv.Energy()


class FoxWolframMoments(ProcessModule):
    """
    Modules for calculating the Fox-Wolfram moments - https://arxiv.org/pdf/1212.4436v1.pdf
    """
    def __init__(self, branchPrefix):
        logging.info("Initializing module calculating Fox-Wolfram moments")
        self.FoxWolframOrders = np.array([0, 1, 2, 3, 4, 5, 6, 7])
        self.foxWolframMoments = None

        self.newVariables = ["H_"+str(order) for order in self.FoxWolframOrders]
        for order in self.FoxWolframOrders:
            if order == 0:
                continue
            self.newVariables.append("R_"+str(order))
        self.newVariables = [branchPrefix+varName for varName in self.newVariables]
        self.branchPrefix = branchPrefix
        
        logging.debug("Variables: %s", self.newVariables)
        newVariableTypes = len(self.newVariables)*["F"]
        initVals = len(self.newVariables)*[-1]
        super(FoxWolframMoments, self).__init__(self.newVariables, newVariableTypes, len(self.newVariables)*[1], initVals)

    def fillArray(self, jets, debug=False):
        """
        Fills the arrays for all Fox-Wolfram Moments defined in self.FoxWolframOrders 
        """
        self.clacFoxWolframMoments(jets, debug)
        for order in self.FoxWolframOrders:
            self.valArrays[self.branchPrefix+"H_"+str(order)][0] = self.foxWolframMoments[order]
            if order == 0:
                continue
            self.valArrays[self.branchPrefix+"R_"+str(order)][0] = self.foxWolframMoments[order] / self.foxWolframMoments[0]
        
    def clacFoxWolframMoments(self, jets, debug=False):
        """
        H_l = sum_{i,j = 1}^{N} W_ij * P_l(cos(Omega_ij))
        """
        self.foxWolframMoments = np.zeros(len(self.FoxWolframOrders))
        for i in range(len(jets)):
            for j in range(len(jets)):
                cos_omega_ij = (jets[i].lv.CosTheta() * jets[j].lv.CosTheta() +
                    math.sqrt((1.0 - jets[i].lv.CosTheta()**2) * (1.0 - jets[j].lv.CosTheta()**2)) *
                    (math.cos(jets[i].lv.Phi() - jets[j].lv.Phi()))
                )
                w_ij = self.weightFunction(jets, jets[i].lv, jets[j].lv)
                vals = np.array([cos_omega_ij]*len(self.foxWolframMoments))

                p_l = np.array(eval_legendre(self.FoxWolframOrders, vals))
                self.foxWolframMoments += w_ij * p_l
                # logging.debug("--- i = %s -- j = %s ---",i,j)
                # logging.debug("vals = %s",list(vals))
                # logging.debug("wij = %s", w_ij)
                # logging.debug("p_l = %s", list(p_l))
        if debug:
            for i in self.FoxWolframOrders:
                logging.debug("Fox-Wolfram Moment %s = %s",i,self.foxWolframMoments[i])

    @staticmethod
    def weightFunction(jets, lv1, lv2):
        """
        Weight based on the products of transverse momenta and normalized to their squared sum 
        """
        lvs = [jet.lv for jet in jets]
        return (lv1.Vect().Mag() * lv2.Vect().Mag()) / (sum(lvs, ROOT.TLorentzVector())).Mag2()

    
class TriggerWeight(ProcessModule):
    """
    Module for calculating the 
    """
    def __init__(self, fileName, hName="h3SF_tot_mod"):
        logging.info("Initializing module for trigger weight")
        self.SFFile = os.environ["CMSSW_BASE"]+"/src/TTH/MEAnalysis/data/"+str(fileName)
        self.hName = hName
        sfFile= ROOT.TFile.Open(self.SFFile)
        self.SFHisto = copy.deepcopy(sfFile.Get(self.hName))
        super(TriggerWeight, self).__init__(["triggerWeight"], ["F"], [1], [1])

    def fillArray(self, ht, nBtags, pt5, debug=False):
        """ Calculate the Trigger SF. Pass list of selected jets """
        _bin = self.SFHisto.FindBin(ht, pt5, nBtags)
        SF = self.SFHisto.GetBinContent(_bin)
        if debug:
            logging.debug("ht=%s | pt5=%s | nBTag=%s | bin=%s | SF=%s",ht, pt5, nBtags, _bin, SF)
        if SF == 0:
            SF = 1
        self.valArrays["triggerWeight"][0] = SF


    
class PUWeightRenaming(ProcessModule):
    """
    Module for calculating the 
    """
    def __init__(self, treeBranchName):
        self.treeName = treeBranchName
        logging.info("Initializing module for pu weight renaming")
        super(PUWeightRenaming, self).__init__(["weight_pu"], ["F"], [1], [1])

    def fillArray(self, tree, debug=False):
        """ Get the PU weight from the tree and write it to the new output name """
        self.valArrays["weight_pu"][0] = getattr(tree, self.treeName)

class BTagNorm(ProcessModule):
    """
    Module for adding the b-tag Sf normalization to the output
    """
    def __init__(self, sample, year = "2018"):
        logging.info("Inizializing module for adding the b-tag SF normalization")
        self.sample = sample
        self.BTagNormClass = BTagNormalization(year)
        
        super(BTagNorm, self).__init__(["btagNorm"], ["F"], [1], [-1])            
            
    def fillArray(self, nJets, ht, ttCls, debug=False):
        SF = self.BTagNormClass.getSF(self.sample, nJets, ht, ttCls)
        if debug:
            logging.debug("nJets=%s | bin=%s | SF=%s", nJets, thisBin, SF)
        self.valArrays["btagNorm"][0] = SF
        
class EventShapes(ProcessModule):
    """
    Module for calculating Event shape variables like sphericity, aplanarity, isotropy etc.
    """
    def __init__(self, branchPrefix):
        logging.info("Initializing module for event shapes - Prefix %s", branchPrefix)
        self.eventShapes = {}
        self.newVariables = ["sphericity","aplanarity"]
        self.newVariables = [branchPrefix+varName for varName in self.newVariables]
        self.branchPrefix = branchPrefix
        logging.debug("Variables: %s", self.newVariables)
        newVariableTypes = ["F","F"]
        initVals = len(self.newVariables)*[-1]
        super(EventShapes, self).__init__(self.newVariables, newVariableTypes, len(self.newVariables)*[1], initVals)

    def fillArray(self, jets, debug=False):
        self.calcEventShapes(jets, debug)
        for variable in self.newVariables:
            self.valArrays[variable][0] = self.eventShapes[variable.replace(self.branchPrefix, "")]

    def calcEventShapes(self, jets, debug):
        CvectorTLorentzVector = getattr(ROOT, "std::vector<TLorentzVector>")
        TMatrixDSym = getattr(ROOT, "TMatrixDSym")
        TMatrixDSymEigen = getattr(ROOT, "TMatrixDSymEigen")

        sumP2 = 0.0
        sumPxx = 0.0
        sumPxy = 0.0
        sumPxz = 0.0
        sumPyy = 0.0
        sumPyz = 0.0
        sumPzz = 0.0
        for jet in jets:
            obj = jet.lv
            sumP2  += obj.P() * obj.P()
            sumPxx += obj.Px() * obj.Px()
            sumPxy += obj.Px() * obj.Py()
            sumPxz += obj.Px() * obj.Pz()
            sumPyy += obj.Py() * obj.Py()
            sumPyz += obj.Py() * obj.Pz()
            sumPzz += obj.Pz() * obj.Pz()

        if sumP2 > 0:
            Txx = sumPxx/sumP2
            Tyy = sumPyy/sumP2
            Tzz = sumPzz/sumP2
            Txy = sumPxy/sumP2
            Txz = sumPxz/sumP2
            Tyz = sumPyz/sumP2
            T = TMatrixDSym(3)
            T[0,0] = Txx
            T[0,1] = Txy
            T[0,2] = Txz
            T[1,0] = Txy
            T[1,1] = Tyy
            T[1,2] = Tyz
            T[2,0] = Txz
            T[2,1] = Tyz
            T[2,2] = Tzz
            TEigen = TMatrixDSymEigen(T)
            eigenValues = TEigen.GetEigenValues()
            self.eventShapes["sphericity"] = 1.5*(eigenValues(1)+eigenValues(2))
            self.eventShapes["aplanarity"] = 1.5*eigenValues(2)
            # self.eventShapes["C"] = 3.0*(eigenValues(0)*eigenValues(1) + eigenValues(0)*eigenValues(2) + eigenValues(1)*eigenValues(2))
            # self.eventShapes["D"] = 27.*eigenValues(0)*eigenValues(1)*eigenValues(2)

        else:
            self.eventShapes["sphericity"] = -99.0
            self.eventShapes["aplanarity"] = -99.0
            # self.eventShapes["C"] = -99.0
            # self.eventShapes["D"] = -99.0

        if debug:
            for var in self.eventShapes:
                logging.debug("%s = %s", var, self.eventShapes[var])

class CRCorrection(ProcessModule):
    """
    Module for calculating the CRcorrection

    Args:
      sampleType (str) : Pass MC or Data
      mediumWP (float) : Current medium Workingpoint
      looseWP (float) : Current loose Workingpoint
      upVariation (bool) : If True the reEta up correction will be used
    """
    def __init__(self, sampleType, mediumWP, looseWP, upVariation=False, year = "2018"):
        logging.info("Initializing module for CRCorrection")
        if sampleType not in ["MC", "Data"]:
            raise RuntimeError("Pass MC or Data")
        self.schema = sampleType
        self.up = upVariation
        self.mediumWP = mediumWP
        self.looseWP = looseWP
        self.year = year
        logging.warning("Using year %s for CR Correction function", self.year)
        super(CRCorrection, self).__init__(["weight_CRCorr"], ["F"], [1], [1])

    def fillArray(self, jets, debug=False):
        self.valArrays["weight_CRCorr"][0] = self.calcWeight(jets, debug)

    def calcWeight(self, jets, debug=False):
        corr = 1.0
        jets_bsorted = sorted(jets,
                              key=lambda x: x.btag,
                              reverse=True)

        bJets_medium = filter(lambda jet: jet.btag > self.mediumWP, jets)
        bJets_loose = filter(lambda jet: jet.btag > self.looseWP, jets)

        nbJets_medium = len(bJets_medium)
        nbJets_loose = len(bJets_loose)            
        
        addbJets = filter(lambda jet: jet.btag < self.mediumWP, bJets_loose)
        addbJets = sorted(addbJets,
                          key=lambda x: x.btag,
                          reverse=True)

        
        if debug:
            logging.debug("MWP = %s | LWP = %s", self.mediumWP, self.looseWP)
            logging.debug("nMedium %s - nLoose %s - nAdd %s", nbJets_medium, nbJets_loose, len(addbJets))
            for ijet, jet in enumerate(addbJets):
                logging.debug("%s : %s", ijet, jet)


        if not nbJets_loose >= 4:
            if debug:
                logging.debug("Skipping because less than 4 loose b-tagged jets")
            return 1.0

        #Calculate the correction for up the the fourth jet (sorted by b-tag)
        if nbJets_medium == 3:
            addbJetsForCalc = addbJets[0:1] #Here we need only one addition loose jet
        elif nbJets_medium == 2:
            addbJetsForCalc = addbJets[0:2] #Here we need two addition loose jets
        else:
            if debug:
                logging.debug("Skipping because not 2 or 3 medium tagged jets")
            return 1.0
        
        for iJet, jet in enumerate(addbJetsForCalc):
            #Recalculate the min DeltaR to the two jets with highest btag value
            drm = min([
                jet.lv.DeltaR(jets_bsorted[0].lv),
                jet.lv.DeltaR(jets_bsorted[1].lv),
            ])
            if self.schema == "data":
                if self.up:
                    thisCorr = self.btagCorrDataUp(jet.pt, jet.eta, drm, self.year)
                else:
                    thisCorr = self.btagCorrData(jet.pt, jet.eta, drm, self.year)
            else:
                if self.up:
                    thisCorr = self.btagCorrMC(jet.pt, jet.eta, drm, self.year)
                else:
                    thisCorr = self.btagCorrMCUp(jet.pt, jet.eta, drm, self.year)
            if debug:
                logging.debug("Calculating correction for iJet %s (Schema: %s, up: %s)", iJet, self.schema, self.up)
                logging.debug("pt = %s; eta = %s, dRmin = %s --> Correction %s", jet.pt, jet.eta, drm, thisCorr)
            corr *= thisCorr

        return corr


    #### Prederived correction function. Just pasting them is not nice but worked. To be reworked at some point ;) 
    @staticmethod
    def btagCorrData(pt, eta, minDeltaRb, year):
        return btagCorrData(pt, eta, minDeltaRb, year)
        
    @staticmethod
    def btagCorrMC(pt, eta, minDeltaRb, year):
        return btagCorrMC(pt, eta, minDeltaRb, year)

    @staticmethod
    def btagCorrDataUp(pt, eta, minDeltaRb, year):
        return btagCorrDataUp(pt, eta, minDeltaRb, year)

    @staticmethod
    def btagCorrMCUp(pt, eta, minDeltaRb, year):
        return btagCorrMCUp(pt, eta, minDeltaRb, year)

    @staticmethod
    def btagCorrMCML(pt, eta, minDeltaRb, year):
        if year == "2016":
            return ((68.5072-0.000178671*pt)*math.erf((pt+220.783)*0.00819782)-67.418) * ((1.05658-0.0167415*eta+0.245148*pow(eta,2)+0.108643*pow(eta,3)-0.746195*pow(eta,4)-0.161072*pow(eta,5)+0.777852*pow(eta,6)+0.0995916*pow(eta,7)-0.435565*pow(eta,8)-0.0308599*pow(eta,9)+0.143063*pow(eta,10)+0.00498937*pow(eta,11)-0.027545*pow(eta,12)-0.000391413*pow(eta,13)+0.00286651*pow(eta,14)+1.11119e-05*pow(eta,15)-0.000124*pow(eta,16))) * ((1.21922-1.12942*minDeltaRb+1.84615*pow(minDeltaRb,2)-1.33816*pow(minDeltaRb,3)+0.480771*pow(minDeltaRb,4)-0.0840672*pow(minDeltaRb,5)+0.00569812*pow(minDeltaRb,6)))
        elif year == "2017":
            return ((68.5288-0.000218126*pt)*math.erf((pt+77.548)*0.0193583)-67.4757) * ((1.074+0.000884261*eta+0.0920831*pow(eta,2)-0.0224394*pow(eta,3)-0.382583*pow(eta,4)+0.0315489*pow(eta,5)+0.389403*pow(eta,6)-0.0253615*pow(eta,7)-0.222554*pow(eta,8)+0.0124495*pow(eta,9)+0.0778877*pow(eta,10)-0.00347231*pow(eta,11)-0.0162586*pow(eta,12)+0.000499151*pow(eta,13)+0.00183453*pow(eta,14)-2.85315e-05*pow(eta,15)-8.54813e-05*pow(eta,16))) * ((1.04869-0.460109*minDeltaRb+0.920204*pow(minDeltaRb,2)-0.732748*pow(minDeltaRb,3)+0.279428*pow(minDeltaRb,4)-0.0515363*pow(minDeltaRb,5)+0.00369105*pow(minDeltaRb,6)))
        elif year == "2018":
            return ((68.479-0.000321147*pt)*math.erf((pt+242.832)*0.00772194)-67.3881) * ((1.06608+0.0129782*eta+0.023247*pow(eta,2)-0.0279394*pow(eta,3)-0.18569*pow(eta,4)+0.0134794*pow(eta,5)+0.161194*pow(eta,6)+0.00156951*pow(eta,7)-0.0781898*pow(eta,8)-0.00290304*pow(eta,9)+0.0252702*pow(eta,10)+0.000861496*pow(eta,11)-0.00525041*pow(eta,12)-0.000109781*pow(eta,13)+0.000608867*pow(eta,14)+5.45874e-06*pow(eta,15)-2.92624e-05*pow(eta,16))) * ((1.2289-1.06918*minDeltaRb+1.73667*pow(minDeltaRb,2)-1.29606*pow(minDeltaRb,3)+0.487842*pow(minDeltaRb,4)-0.0903785*pow(minDeltaRb,5)+0.00654301*pow(minDeltaRb,6)))
        else:
            raise KeyError("Only 2016, 2017 and 2018 are supported years!")

    @staticmethod
    def btagCorrDataML(pt, eta, minDeltaRb, year):
        if year == "2016":
            return ((65.4909+0.00018999*pt)*math.erf((pt+217.526)*0.00829102)-64.4395) * ((1.0583-0.0276397*eta+0.290512*pow(eta,2)+0.184721*pow(eta,3)-0.833685*pow(eta,4)-0.328885*pow(eta,5)+0.915596*pow(eta,6)+0.259827*pow(eta,7)-0.547174*pow(eta,8)-0.105685*pow(eta,9)+0.18822*pow(eta,10)+0.0230672*pow(eta,11)-0.0371191*pow(eta,12)-0.00256884*pow(eta,13)+0.00389615*pow(eta,14)+0.000114655*pow(eta,15)-0.000168615*pow(eta,16))) * ((1.64321-1.81115*minDeltaRb+2.11823*pow(minDeltaRb,2)-1.27212*pow(minDeltaRb,3)+0.405128*pow(minDeltaRb,4)-0.0649275*pow(minDeltaRb,5)+0.00411346*pow(minDeltaRb,6)))
        elif year == "2017":
            return ((65.7936-0.000160034*pt)*math.erf((pt+198.585)*0.00900727)-64.7131) * ((1.08076+0.0223116*eta+0.0759621*pow(eta,2)-0.106477*pow(eta,3)-0.233857*pow(eta,4)+0.16489*pow(eta,5)+0.105699*pow(eta,6)-0.113421*pow(eta,7)-0.000532209*pow(eta,8)+0.0399331*pow(eta,9)-0.0105621*pow(eta,10)-0.00742314*pow(eta,11)+0.00281316*pow(eta,12)+0.000681805*pow(eta,13)-0.000290199*pow(eta,14)-2.3693e-05*pow(eta,15)+1.04992e-05*pow(eta,16))) * ((1.78719-2.31258*minDeltaRb+2.77364*pow(minDeltaRb,2)-1.69127*pow(minDeltaRb,3)+0.546365*pow(minDeltaRb,4)-0.0891166*pow(minDeltaRb,5)+0.00577795*pow(minDeltaRb,6)))
        elif year == "2018":
            return ((64.7707-0.000138897*pt)*math.erf((pt+172.245)*0.0102268)-63.7018) * ((1.07595+0.000762959*eta+0.111017*pow(eta,2)-0.0765021*pow(eta,3)-0.399122*pow(eta,4)+0.186568*pow(eta,5)+0.343623*pow(eta,6)-0.175339*pow(eta,7)-0.158196*pow(eta,8)+0.0807308*pow(eta,9)+0.0452158*pow(eta,10)-0.0195166*pow(eta,11)-0.00808453*pow(eta,12)+0.00238263*pow(eta,13)+0.000820112*pow(eta,14)-0.000115964*pow(eta,15)-3.55289e-05*pow(eta,16))) * ((1.79165-2.36983*minDeltaRb+2.88584*pow(minDeltaRb,2)-1.78524*pow(minDeltaRb,3)+0.584932*pow(minDeltaRb,4)-0.0966247*pow(minDeltaRb,5)+0.00633044*pow(minDeltaRb,6)))
        else:
            raise KeyError("Only 2016, 2017 and 2018 are supported years!")

        
class JetArray(ProcessModule):
    def __init__(self, branchPrefix):
        logging.info("Initializing modules for filling array brnaches with jet information")
        self.indexingVar = "num"+branchPrefix
        self.newVariables = [
            self.indexingVar,
            branchPrefix+"_pt",
            branchPrefix+"_eta",
            branchPrefix+"_phi",
            branchPrefix+"_energy",
            branchPrefix+"_btagDeepFlav",
            branchPrefix+"_hadronFlavour"
        ]
        
        self.branchPrefix = branchPrefix
        logging.debug("Variables: %s", self.newVariables)
        self.isArrayBranch = True
        newVariableTypes = ["I",
                            (self.indexingVar,"F"),#pt
                            (self.indexingVar,"F"),#eta
                            (self.indexingVar,"F"),#phi
                            (self.indexingVar,"F"),#energy
                            (self.indexingVar,"F"),#btag
                            (self.indexingVar,"I"),#hadronFlav

        ]
        arrayLens = [1, 25, 25, 25, 25, 25, 25]
        initVals = len(self.newVariables)*[-99]
        super(JetArray, self).__init__(self.newVariables, newVariableTypes, arrayLens, initVals)

    def fillArray(self, jets, debug=False):        
        self.valArrays[self.indexingVar][0] = len(jets)
        for iJet in range(len(jets)):
            self.valArrays[self.branchPrefix+"_pt"][iJet] = jets[iJet].pt
            self.valArrays[self.branchPrefix+"_eta"][iJet] = jets[iJet].eta
            self.valArrays[self.branchPrefix+"_phi"][iJet] = jets[iJet].phi
            self.valArrays[self.branchPrefix+"_energy"][iJet] = jets[iJet].lv.Energy()
            self.valArrays[self.branchPrefix+"_btagDeepFlav"][iJet] = jets[iJet].btag
            self.valArrays[self.branchPrefix+"_hadronFlavour"][iJet] = jets[iJet].hadronFlav
