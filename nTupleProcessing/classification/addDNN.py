import sys
import os
import logging
import pickle
import time
import glob

from array import array
import numpy as np
import ROOT

from TTH.MEAnalysis.samples_base import getSitePrefix
from utils import Jet
sys.path.insert(0, os.path.abspath('../../utils/'))

from TTH.Plotting.Datacards.fhDNN import DNNPredictor
logger = logging.getLogger(__name__)

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-25s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )


class DNNPredictorSkim(DNNPredictor):
    def getPrediction(self, tree, bTagVar, debug=False):
        logger.debug("Getting priction in %s", self.name)

        ## Make JEts
        self.FoxWolframMoments = None
        self.eventShapes = None
        self.inputData = None
        self.JetVarBSorted = None
        self.eventJets = None
        self.eventbJets = None
        #self.DEtaJVars = None
        selectedJets = []
        for iJet in range(tree.__getattr__("njets")):
            selectedJets.append(
                Jet(
                    tree.__getattr__("jets_pt")[iJet],
                    tree.__getattr__("jets_eta")[iJet],
                    tree.__getattr__("jets_phi")[iJet],
                    tree.__getattr__("jets_mass")[iJet],
                    tree.__getattr__("jets_"+bTagVar)[iJet]
                )
            )
        selectedbJets = sorted(filter(lambda jet : jet.btag > self.bTagWPM, selectedJets),
                               key=lambda x: x.btag,
                               reverse=True)
        selectedloosebJets = sorted(filter(lambda jet : jet.btag > self.bTagWPL, selectedJets),
                               key=lambda x: x.btag,
                               reverse=True)
        #Calculate FoxWolfram Moments:
        self.FoxWolframModule.clacFoxWolframMoments(selectedJets, True)
        self.FoxWolframMoments = self.FoxWolframModule.foxWolframMoments
        self.EventShapesModule.calcEventShapes(selectedJets, False)
        self.JetVarBSorted = self.JetVarBSortedModule.getValues(selectedloosebJets)
        #self.DEtaJVars =self.DEtaJVarsModule.calcDetaJs(selectedJets)
        self.eventJets = selectedJets
        self.eventbJets = selectedloosebJets
        
        self.inputData = []
        DNNInputValues = {}
        for ivar, var in enumerate(self.inputVariables):
            DNNInputValues[var] = -2.0
            if var in self.selfinputFunctions:
                if var == "MEM":
                    varValue = self.selfinputFunctions[var](len(selectedJets), tree)
                elif "_bsort_" in var:
                    varValue = self.selfinputFunctions[var](var, self.JetVarBSorted)
                elif var.startswith("selJets_"):
                    varValue = self.selfinputFunctions[var](var, selectedJets)
                elif var.startswith("bJets_"):
                    varValue = self.selfinputFunctions[var](var, selectedloosebJets)
                elif var.startswith("H_") or var.startswith("R_"):
                    varValue = self.selfinputFunctions[var](self.FoxWolframMoments)
                # elif var.startswith("Detaj"):
                #     varValue = self.selfinputFunctions[var](var, len(selectedJets))
                else:
                    varValue = self.selfinputFunctions[var]()
            else:
                value = getattr(tree, var, "err")
                if value != "err":
                    varValue = value
                elif var.startswith("Detaj"):
                    if var == "Detaj_5":
                        varValue = tree.Detaj[5]
                    elif var == "Detaj_6":
                        varValue = tree.Detaj[6]
                    elif var == "Detaj_7":
                        varValue = tree.Detaj[7]
                    else:
                        raise RuntimeError("There should be no other Detaj variable")
                elif var.startswith("jets"):
                    _, jetVar, jetIdx  = var.split("_")
                    if jetVar == "eta":
                        varValue = selectedJets[int(jetIdx)].eta
                    elif jetVar == "pt":
                        varValue = selectedJets[int(jetIdx)].pt
                    else:
                        raise RuntimeError("There should be no other jet vars than pt and eta")
                else:
                    raise RuntimeError("Variable %s could not be processed"%var)
            #logger.debug("Set %s to %s", var, varValue)
            self.inputData.append(varValue)
            DNNInputValues[var] = varValue

        doPrediction = True
        if "MEM" in self.inputVariables:
            if self.inputData[self.inputVariables.index("MEM")] == -1:
               doPrediction = False
               if debug:
                   logger.debug("MEM = %s - Skipping evaluation",  self.inputData[self.inputVariables.index("MEM")])
        if doPrediction:
            if debug:
                logger.debug("======== Input Data ========")
            for i in range(len(self.inputData)):
                if debug:
                    logger.debug("Pre  transfrom : %s -- %s", self.inputVariables[i], self.inputData[i])
                self.inputData[i] = (self.inputData[i] - self.transformations["unweighted"]["mu"][self.inputVariables[i]])/self.transformations["unweighted"]["std"][self.inputVariables[i]]
                if debug:
                    logger.debug("Post transfrom : %s -- %s (mu=%s, std=%s", self.inputVariables[i], self.inputData[i], self.transformations["unweighted"]["mu"][self.inputVariables[i]], self.transformations["unweighted"]["std"][self.inputVariables[i]])
                    logger.debug("============================")


            inputData = np.array([self.inputData])
            if debug:
                logger.debug(inputData)
                logger.debug("============================")

            DNNPred = self.model.predict(inputData)
            if DNNPred < 0.001 and tree.numJets == 7 and tree.nBDeepFlavM == 4:
                logger.warning("numJets %s | nBDeepFlavM %s | nBDeepFlavL %s",tree.numJets, tree.nBDeepFlavM, tree.nBDeepFlavL)
                #raw_input("waiting.....")
            if debug:
                logger.debug("DNN Prediction : %s", DNNPred)
        else:
            DNNPred = np.array([[-1,-1,-1]])
            
        return DNNInputValues, DNNPred[0]
                
    
def addDNNPrediction(fileName, skipevents, treeName, outputBranchName, model9J, model8J, model7J, signalNode, mediumWP, looseWP, addInputVariables = False):
    t0 = time.time()
    rFile = ROOT.TFile(fileName, "UPDATE")
    tree = rFile.Get(treeName)

    DNNPredictors = {
        "7j" : DNNPredictorSkim("Predictor7J", os.environ["CMSSW_BASE"] + "/src/TTH/" + model7J,  mediumWP, looseWP, "7j"),
        "8j" : DNNPredictorSkim("Predictor8J", os.environ["CMSSW_BASE"] + "/src/TTH/" + model8J,  mediumWP, looseWP, "8j"),
        "9j" : DNNPredictorSkim("Predictor9J", os.environ["CMSSW_BASE"] + "/src/TTH/" + model9J,  mediumWP, looseWP, "9j")
        }

    DNNpredictionArray = array("f", [-1.0])
    branchDNN = tree.Branch(outputBranchName, DNNpredictionArray, outputBranchName+"/F")

    #Also
    branchInput = {}
    InputArrays = {}
    if addInputVariables:
        allInputs = list(set(DNNPredictors["7j"].inputVariables + DNNPredictors["8j"].inputVariables + DNNPredictors["9j"].inputVariables))
        logging.info("Will add branches for %s", allInputs)
        for inputVar in allInputs:
            branchName = "{0}_Input_{1}".format(outputBranchName, inputVar)
            InputArrays[inputVar] = array("f", [-1.0])
            branchInput[inputVar] = tree.Branch(branchName, InputArrays[inputVar], branchName+"/F")
            
        
    nEvents = tree.GetEntries()
    tdiff = time.time()
    logging.info("Tree has %s entries", nEvents)
    for iev in range(skipevents, nEvents):
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f} | Time left {2:8f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
            tdiff = time.time()
        logging.debug("New Events - %s/%s", iev, nEvents-1)

        tree.GetEvent(iev)
        logging.debug("NJets %s | nBDeepFlavM %s | nBDeeoFlavL %s | nDetaj %s | Wmass4b %s", tree.numJets, tree.nBDeepFlavM, tree.nBDeepFlavL, tree.nDetaj, tree.Wmass4b )
        if addInputVariables:
            for inputVar in allInputs:
                InputArrays[inputVar][0] = -1.0
        if tree.Wmass4b < 30 or tree.Wmass4b > 250:
            logging.debug("Skipping event because of WMass") #Can sometimes happen with events that fail the nominal but nt in the systeamtic
            DNNpredictionArray[0] = -9.0
            branchDNN.Fill()
            if addInputVariables:
                for inputVar in allInputs:
                    branchInput[inputVar].Fill()
            continue
        DNNpredictionArray[0] = -1.0

            
        
        if tree.nBDeepFlavM >= 4 or (tree.nBDeepFlavM == 3 and tree.nBDeepFlavL >= 4)  or (tree.nBDeepFlavM == 2 and tree.nBDeepFlavL >= 4):
            if tree.numJets < 7:
                DNNpredictionArray[0] = -33.0
            else:
                if tree.numJets == 7:
                    InputValues, Pred = DNNPredictors["7j"].getPrediction(tree, "btagDeepFlav", debug=True)
                elif tree.numJets == 8:
                    InputValues, Pred = DNNPredictors["8j"].getPrediction(tree, "btagDeepFlav", debug=True)
                elif tree.numJets >= 9:
                    InputValues, Pred = DNNPredictors["9j"].getPrediction(tree, "btagDeepFlav", debug=True)
                else:
                    InputValues, Pred = None, None
                logging.debug("Got DNN prediction %s", Pred)
                if Pred is not None:
                    DNNpredictionArray[0] = float(Pred[signalNode])
                    #raw_input("Got %s for nJets %s"%(DNNpredictionArray[0], tree.numJets))
                else:
                    DNNpredictionArray[0] = -99.0

                if addInputVariables and InputValues is not None:
                    for inputVar in allInputs:
                        if inputVar in InputValues:
                            InputArrays[inputVar][0] = InputValues[inputVar]
        logging.debug("THis event : %s", DNNpredictionArray[0])
            
        branchDNN.Fill()
        if addInputVariables:
            for inputVar in allInputs:
                branchInput[inputVar].Fill()

                
    logging.info("Saving File")
    rFile.cd()
    tree.Write("",ROOT.TFile.kOverwrite)
    logging.info("Total time to finish: {0:8f}".format(time.time()-t0))
    return True

    
    
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for adding DNN from a lookup table to tree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "Events to skip",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--outBranchs",
        action = "store",
        help = "Name the new branch",
        type = str,
        default="DNNPred"
    )    
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--ModelBase",
        action = "store",
        help = "path to folder containing the 9 jet model",
        type = str,
        default = "FHDownstream/data/DNN/dnnTesting_AllVars_Multi_ranking_best15/"
    )
    argumentparser.add_argument(
        "--signalNode",
        action = "store",
        help = "Note that is considered as DNN prediction",
        type = int,
        default = 0,
    )
    argumentparser.add_argument(
        "--mediumWP",
        action = "store",
        help = "Medium working point for b-tagging - Defaultis 2017 DeepFlav",
        type = float,
        default = 0.3033,
    )
    argumentparser.add_argument(
        "--looseWP",
        action = "store",
        help = "Loose working point for b-tagging - Defaultis 2017 DeepFlav",
        type = float,
        default = 0.0521,
    )
    argumentparser.add_argument(
        "--addInputVariables",
        action = "store_true",
        help = "If passes the input variables will also be save es branches"
    )

    
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)

    addDNNPrediction(args.inputFile, args.skipevents, args.treeName, args.outBranchs, args.ModelBase+"9J/", args.ModelBase+"8J/", args.ModelBase+"7J/", args.signalNode, args.mediumWP, args.looseWP, args.addInputVariables)
    
    
