import sys
import os
import logging
import time

import numpy as np
from array import array

import ROOT

from TTH.MEAnalysis.samples_base import getSitePrefix

sys.path.insert(0, os.path.abspath('../../utils/'))
from ConfigReader import ConfigReaderBase
from utils import Jet
import processModules

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-20s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )

class Config(ConfigReaderBase):
    """
    Config for nTuple processor. Inheritance from ConfigReaderBase. This mainly is doen to add generec function to read the config file
    """
    def __init__(self, pathtoConfig):
        super(Config, self).__init__(pathtoConfig)
        #Get General settings
        self.baseSelection = self.readConfig.get("General", "baseSelection")
        if self.readConfig.has_option("General", "baseSelSamples"):
            altSampleSelection =  self.readMulitlineOption("General", "baseSelSamples", "Single")
            self.altSelectionSample = altSampleSelection.keys()
            self.altSelection = altSampleSelection
        else:
            self.altSelectionSample = []
            self.altSelection = {}

        logging.debug("General Selection: %s", self.baseSelection)
        for sample in self.altSelectionSample:
            logging.debug("Alt selection for %s: %s", sample, self.altSelection[sample])


        self.jetBranchPrefix = self.readConfig.get("General", "jetBranchPrefixSelected")
        self.jetBranchPrefixLoose = self.readConfig.get("General", "jetBranchPrefixLoose")
        self.bTagVar = self.readConfig.get("General", "btagBranchPostFix")
        self.bTagMWP = self.readConfig.getfloat("General", "mediumWP")
        self.bTagLWP = self.readConfig.getfloat("General", "looseWP")

        self.triggerFile = self.setOptionWithDefault("General", "triggerFile", "noFileSet.root")
        self.triggerHisto = self.setOptionWithDefault("General", "triggerHisto", "h3SF_tot_mod")
        self.year = self.readConfig.get("General", "year")

        self.btagNormBase =  self.setOptionWithDefault("General", "btagNormBase", None)
        if "$CMSSWBASE" in self.btagNormBase:
            self.btagNormBase = self.btagNormBase.replace("$CMSSWBASE", os.environ["CMSSW_BASE"])
        self.bTagNormName = self.readConfig.get("General", "bTagNormName")
        
        #Get branches to copy
        singleBranches = self.readMulitlineOption("CopyBranches", "singleBranch", "List")
        self.singleBranchCopy = {}
        for br in singleBranches:
            applyFunc = None
            defaultValue = None
            if len(singleBranches[br]) == 1:
                pass
            elif len(singleBranches[br]) == 2:
                applyFunc = singleBranches[br][1]
            elif len(singleBranches[br]) == 3:
                applyFunc = None if singleBranches[br][1] == "None" else singleBranches[br][1]
                defaultValue = singleBranches[br][2]
                if singleBranches[br][0] == "F":
                    defaultValue = float(defaultValue)
                elif singleBranches[br][0] == "l":
                    defaultValue = long(defaultValue)
                else:
                    defaultValue = int(defaultValue)
            else:
                raise RuntimeError("More than 3 elements are not supported. Got %s"%singleBranches[br])
            self.singleBranchCopy[br] = {
                "name" : br,
                "datatype" : singleBranches[br][0],
                "arraydatatype" : self.getArrayType( singleBranches[br][0]),
                "applyFunction" : applyFunc,
                "defaultValue" : defaultValue
            }
        arrayBranches = self.readMulitlineOption("CopyBranches", "arrayBranch", "List")
        self.arrayBranchCopy = {}
        self.arrayLoopVars = []
        self.arrayBranchPerLoopVar = {}
        for br in arrayBranches:
            self.arrayBranchCopy[br] = self.parseArrayBranchDef(br, arrayBranches[br])
            if self.arrayBranchCopy[br]["lenVar"] not in self.arrayLoopVars:
                self.arrayLoopVars.append(self.arrayBranchCopy[br]["lenVar"])
                self.arrayBranchPerLoopVar[self.arrayBranchCopy[br]["lenVar"]] = []
            self.arrayBranchPerLoopVar[self.arrayBranchCopy[br]["lenVar"]].append(br)
        self.modules = {}
        for option in self.readConfig.options("Modules"):
            moduleDefs = self.getList(self.readConfig.get("Modules", option))
            self.modules[option] = {"branchName" : moduleDefs[0] if moduleDefs[0] != "None" else "",
                                    "datatype" : moduleDefs[1]}
            if len(moduleDefs) > 2:
                self.modules[option]["maxElem"] = moduleDefs[2]
        for module in self.modules:
            logging.debug("Module %s -- %s", module, self.modules[module])

    def parseArrayBranchDef(self, name, definiton):
        retDict = {"name" : name}
        retDict.update({"lenVar" : definiton[0]})
        if ".." in definiton[1]:
            indexBorder = definiton[1].split("..")
            assert len(indexBorder) == 2
            indices = range(int(indexBorder[0]), int(indexBorder[1])+1)
        elif "." in definiton[1]:
            indices = [int(x) for x in definiton[1].split(".")]
        else:
            indices = [int(definiton[1])]
        retDict.update({"indices" : indices})
        retDict.update({"datatype" : definiton[2]})
        retDict.update({"arraydatatype" : self.getArrayType(definiton[2])})
        return retDict
        
    @staticmethod
    def getArrayType(datatype):
        if datatype == "F":
            return "f"
        elif datatype == "I":
            return "i"
        elif datatype == "l":
            return "l"
        else:
            raise NotImplementedError
    
def processNTuple(config, inFiles, outFile, skipevents, treeName, sample, debug):
    t0 = time.time()
    logging.info("Using tree name %s", treeName)
    inTree = ROOT.TChain(treeName)
    for inFile in inFiles:
        logging.debug("Adding file %s", inFile)
        inTree.AddFile(inFile)

    if sample in config.altSelectionSample:
        thisSelection = config.altSelection[sample]
    else:
        thisSelection = config.baseSelection
    logging.info("Applying selection: %s", thisSelection)
    inTree = inTree.CopyTree(thisSelection)
    logging.debug("New tree: %s", inTree)
    
    outputFile = ROOT.TFile(outFile, "RECREATE")
    outputFile.cd()
    outTree = ROOT.TTree("tree","tree");
    logging.info("Created output file %s and tree %s", outputFile, outTree)

    #Creating output Branches
    allBranches = {}
    allCopyArrays = {}
    allCopyArraysDefault = {}
    #Create output branches for flat copy branches
    for br in config.singleBranchCopy:
        allCopyArrays[br] = array(config.singleBranchCopy[br]["arraydatatype"], [-1])
        allCopyArraysDefault[br] = -1
        allBranches[br] = outTree.Branch(br,
                                         allCopyArrays[br],
                                         br+"/"+config.singleBranchCopy[br]["datatype"])
    #Create output branches for array copy branches
    for br in config.arrayBranchCopy:
        for index in config.arrayBranchCopy[br]["indices"]:
            thisName = br+"_"+str(index)
            allCopyArrays[thisName] = array(config.arrayBranchCopy[br]["arraydatatype"], [-1])
            allCopyArraysDefault[thisName] = -99
            allBranches[thisName] = outTree.Branch(thisName,
                                                   allCopyArrays[thisName],
                                                   thisName+"/"+config.arrayBranchCopy[br]["datatype"])
    #Modules
    allModules = {}
    for module in config.modules:
        if module == "MEM":
            allModules[module] = processModules.MEM(config.modules[module]["branchName"])
        elif module == "BJetEventVars":
            allModules[module] = processModules.JetEventVars(config.modules[module]["branchName"])
        elif module == "JetEventVars":
            allModules[module] = processModules.JetEventVars(config.modules[module]["branchName"])
        elif module == "JetVarsLoose":
            allModules[module] = processModules.JetEventVars(config.modules[module]["branchName"])
        elif module == "JetEnergySelected":
            allModules[module] = processModules.JetEnergy(config.modules[module]["branchName"], int(config.modules[module]["maxElem"]))
        elif module == "FoxWolframMoments":
            allModules[module] = processModules.FoxWolframMoments(config.modules[module]["branchName"])
        elif module == "DEtaJVars":
            allModules[module] = processModules.DEtaJVars(config.modules[module]["branchName"])
        elif module == "EventShapes":
            allModules[module] = processModules.EventShapes(config.modules[module]["branchName"])
        elif module == "BEventShapes":
            allModules[module] = processModules.EventShapes(config.modules[module]["branchName"])
        elif module == "JetVarsBSorted":
            allModules[module] = processModules.JetVars(config.modules[module]["branchName"])
        elif module == "TriggerWeight":
            allModules[module] = processModules.TriggerWeight(config.triggerFile, config.triggerHisto)
        elif module == "PUWeightRenaming":
            allModules[module] = processModules.PUWeightRenaming(config.modules[module]["branchName"])
        elif module == "BTagNorm":
            allModules[module] = processModules.BTagNorm(sample, year = config.year)

        elif module == "CRCorrection":
            allModules[module] = processModules.CRCorrection(sampleType = "Data" if sample in ["BTagCSV", "JetHT"] else "MC",
                                                             mediumWP = config.bTagMWP,
                                                             looseWP =  config.bTagLWP,
                                                             year = config.year)
        elif module == "SelectedJetArray":
            allModules[module] = processModules.JetArray(config.modules[module]["branchName"])
        else:
            raise RuntimeError("Unsupported module")
        
        for outBr in allModules[module].branchNames:
            if allModules[module].indexer[outBr] is  None:
                #print outBr+"/"+allModules[module].dataTypes[outBr]
                allBranches[outBr] = outTree.Branch(outBr, allModules[module].valArrays[outBr], outBr+"/"+allModules[module].dataTypes[outBr])
            else:
                #print outBr+"["+allModules[module].indexer[outBr]+"]"+"/"+allModules[module].dataTypes[outBr]
                allBranches[outBr] = outTree.Branch(outBr, allModules[module].valArrays[outBr], outBr+"["+allModules[module].indexer[outBr]+"]"+"/"+allModules[module].dataTypes[outBr])

    logging.debug("Output branches")
    for br in allBranches:
        logging.debug("%s : %s", br, allBranches[br])
    tdiff = time.time()
    logging.info("Time untile loop start {0:6f}".format(tdiff-t0))
    
    nEvents = inTree.GetEntries()
    logging.info("Tree has %s entries", nEvents)
    for iev in range(skipevents, nEvents):
        for key in allCopyArrays:
            allCopyArrays[key][0] = allCopyArraysDefault[key]
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f} | Time left {2:8f}".format(iev, time.time()-t0,time.time()-tdiff,(time.time()-tdiff)*(nEvents-iev)))
            tdiff = time.time()
        inTree.GetEvent(iev)
        if "evtNumber" in debug:
            logging.debug("Processing event %s", iev)
        #Copy flat branches to output file
        for br in config.singleBranchCopy:
            try: 
                branchVal = inTree.__getattr__(br)
            except AttributeError as e:
                if config.singleBranchCopy[br]["defaultValue"] is None:
                    raise e
                else:
                    branchVal = config.singleBranchCopy[br]["defaultValue"]
                
            if config.singleBranchCopy[br]["applyFunction"] is None:
                allCopyArrays[br][0] = branchVal
            else:
                if config.singleBranchCopy[br]["applyFunction"] == "sign":
                     allCopyArrays[br][0] = np.sign(branchVal)
                else:
                    raise NotImplementedError
        #Copy array branches
        #Loop over all indexing variables
        for loopVar in config.arrayLoopVars:
            if "array" in debug:
                logging.debug("Looping over %s - Number %s", loopVar, inTree.__getattr__(loopVar))
            #Loop over the len of the indexing vars
            for iVar in range(inTree.__getattr__(loopVar)):
                for br in config.arrayBranchPerLoopVar[loopVar]:
                    thisName = br+"_"+str(iVar)
                    if thisName in allCopyArrays.keys():
                        if "array" in debug:
                            logging.debug("Filling %s[%s] in %s = %s", br, iVar, thisName, inTree.__getattr__(br)[iVar])
                        allCopyArrays[thisName][0] = inTree.__getattr__(br)[iVar]                        
                
            
        #Run the modules
        selectedJets = []
        for iJet in range(inTree.__getattr__("njets")):
            selectedJets.append(
                Jet(
                    inTree.__getattr__(config.jetBranchPrefix+"_pt")[iJet],
                    inTree.__getattr__(config.jetBranchPrefix+"_eta")[iJet],
                    inTree.__getattr__(config.jetBranchPrefix+"_phi")[iJet],
                    inTree.__getattr__(config.jetBranchPrefix+"_mass")[iJet],
                    inTree.__getattr__(config.jetBranchPrefix+"_"+config.bTagVar)[iJet],
                    #inTree.__getattr__(config.jetBranchPrefix+"_hadronFlavour")[iJet],
                )
            )
            if "jets" in debug:
                logging.debug("Added %s jet: %s",iJet, selectedJets[iJet])
        looseJets = [] #Loose jets in tthbb13 are exclusive to selection jets (-->low pt jets)
        for iJet in range(inTree.__getattr__("nloose_jets")):
            looseJets.append(
                Jet(
                    inTree.__getattr__(config.jetBranchPrefixLoose+"_pt")[iJet],
                    inTree.__getattr__(config.jetBranchPrefixLoose+"_eta")[iJet],
                    inTree.__getattr__(config.jetBranchPrefixLoose+"_phi")[iJet],
                    inTree.__getattr__(config.jetBranchPrefixLoose+"_mass")[iJet],
                    inTree.__getattr__(config.jetBranchPrefixLoose+"_"+config.bTagVar)[iJet]
                )
            )
            if "jets" in debug:
                logging.debug("Added %s loose-jet: %s",iJet, looseJets[iJet])

        selectedbJets = sorted(filter(lambda jet : jet.btag > config.bTagMWP, selectedJets),
                               key=lambda x: x.btag,
                               reverse=True)
        selectedloosebJets = sorted(filter(lambda jet : jet.btag > config.bTagLWP, selectedJets),
                                    key=lambda x: x.btag,
                                    reverse=True)
        for module in config.modules:
            allModules[module].resetArrays()
            if "BJetEventVars" in module:
                allModules[module].fillArray(selectedloosebJets, ("module" in debug))
            elif "JetEventVars" in module:
                allModules[module].fillArray(selectedJets, ("module" in debug))
            elif "JetEnergySelected" in module:
                allModules[module].fillArray(selectedJets, ("module" in debug))
            elif "FoxWolframMoments" in module:
                allModules[module].fillArray(selectedJets, ("module" in debug))
            elif "BEventShapes" in module:
                allModules[module].fillArray(selectedloosebJets, ("module" in debug))
            elif "DEtaJVars" in module:
                allModules[module].fillArray(selectedJets, ("module" in debug))
            elif "JetVarsLoose" in module:
                allModules[module].fillArray(selectedJets+looseJets, ("module" in debug))
            elif "EventShapes" in module:
                allModules[module].fillArray(selectedJets, ("module" in debug))
            elif "JetVarsBSorted" in module:
                allModules[module].fillArray(selectedloosebJets, ("module" in debug))
            elif "TriggerWeight" in module and sample not in ["JetHT", "BTagCSV"]:
                if len(selectedJets) >= 6:
                    allModules[module].fillArray(inTree.__getattr__("ht30"),
                                                 inTree.__getattr__("nBCSVM"),
                                                 selectedJets[5].pt, ("weight" in debug))
                    
            elif "BTagNorm" in module and sample not in ["JetHT", "BTagCSV"]:
                allModules[module].fillArray(len(selectedJets),
                                             inTree.__getattr__("ht30"),
                                             inTree.__getattr__("ttCls"),
                                             ("weight" in debug))
            elif "PUWeightRenaming" in module and sample not in ["JetHT", "BTagCSV"]:
                allModules[module].fillArray(inTree, ("weight" in debug))
            elif "CRCorrection" in module:
                allModules[module].fillArray(selectedJets, ("CRCorr" in debug))
            elif "MEM" in module:
                allModules[module].fillArray(inTree, ("module" in debug))
            elif "SelectedJetArray" in module:
                allModules[module].fillArray(selectedJets)
            else:
                logging.debug("Skipping module %s",module)

        if "output" in debug:
            for thisName in allCopyArrays:
                logging.debug("%s = %s", thisName, allCopyArrays[thisName])
            for module in config.modules:
                for outBr in allModules[module].branchNames:
                    logging.debug("%s = %s", outBr, allModules[module].valArrays[outBr])
            raw_input("next")
                                                   
        outTree.Fill()
    outputFile.Write()
    return True
    
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Base script for preprocessing nTuples for classification training',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        default = None
    )
    argumentparser.add_argument(
        "--envInput",
        action = "store",
        help = "If passed inputfile will be ignored and files will be loaded from env. Pass varible name!",
        type = str,
        default = None
    )
    
    argumentparser.add_argument(
        "--output",
        action = "store",
        help = "path to folder where the output will be saved",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "configuration file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "File used for the b-tag SF",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--sample",
        action = "store",
        help = "sample (only really required for BTagCSV or JetHT)",
        type = str,
        default="notSpecified"
    )
    argumentparser.add_argument(
        "--enabledebug",
        action = "store",
        nargs='+',
        help = "Enable debug messages (also requires debug level) per event. Just setting debug will always reveal add. info",
        type = str,
        default=[]
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)
    thisConfig = Config(args.config)

    if args.inputFile is None and args.envInput is None:
        raise RuntimeError("Pass either --inputFile or --envInput (or both but then will ignore --inputFile)")
    inputs = []
    if args.envInput is not None:
        inputs = os.environ[args.envInput].split()
    else:
        inputs.append(args.inputFile)
    
    processNTuple(thisConfig, inputs, args.output, args.skipevents, args.treeName, args.sample, args.enabledebug)
