import ROOT

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

class Jet(object):
    """
    Jet container
    """
    def __init__(self, pt, eta, phi, mass, btag, hadronFlav=-1):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.mass = mass
        self.btag = btag
        self.hadronFlav = hadronFlav
        
        self.lv = ROOT.TLorentzVector()
        self.lv.SetPtEtaPhiM(self.pt, self.eta, self.phi, self.mass)

    def __str__(self):
        return "pt = {0} | eta = {1} | phi = {2} | mass = {3} | btag = {4}".format(self.pt,
                                                                                   self.eta,
                                                                                   self.phi,
                                                                                   self.mass,
                                                                                   self.btag)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            for attr in ["pt", "eta", "phi", "mass", "btag"]:
                #print attr, getattr(self, attr), getattr(other, attr)
                if not isclose(getattr(self, attr), getattr(other, attr)):
                    return False

            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        else:
            return NotImplemented


if __name__ == "__main__":
    jets = []
    jets.append(Jet(207.856307983, 0.198455810547, -2.57373046875, 31.125, 0.97607421875))
    jets.append(Jet(170.32800293, 1.38159179688, 1.9072265625, 23.703125, 0.0712890625))
    jets.append(Jet(164.018829346, -1.734375, 0.0442886352539, 15.4609375, 0.94091796875))


    for ijet, jet in enumerate(jets):
        print ijet, jet == jets[1], jet != jets[1]
