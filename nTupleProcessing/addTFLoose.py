import sys
import os
import logging
import time

import ROOT
from array import array

from classification.processModules import CRCorrection

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-25s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )


class Jet(object):
    def __init__(self, pt, mass, eta, phi, bTagValue, bTagFlag, dRmin):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.mass = mass
        self.bTagValue = bTagValue
        self.btagFlag = bTagFlag
        self.dRmin = dRmin

def calcTFLoose(jets, schema, looseWP, mediumWP, year, validation = False):
    corr = 1.0
    logging.debug("Using: MEDIUM = %s | LOOSE = %s", mediumWP, looseWP)
    for iJet, jet in enumerate(jets):
        pt = jet.pt
        eta = jet.eta
        drm = jet.dRmin
        btagVal = jet.bTagValue
        flag = jet.btagFlag
        logging.debug("Jet pt = %s, btagVal = %s, flag = %s, in btag range %s", pt, btagVal, flag, not(btagVal<looseWP or btagVal>mediumWP))
        if btagVal<looseWP or btagVal>mediumWP or flag<1:
            continue
        if schema == "data":
            if validation:
                thisCorr = CRCorrection.btagCorrDataML(pt, eta, drm, year)
            else:
                thisCorr = CRCorrection.btagCorrData(pt, eta, drm, year)
        else:
            if validation:
                thisCorr = CRCorrection.btagCorrMCML(pt, eta, drm, year)
            else:
                thisCorr = CRCorrection.btagCorrMC(pt, eta, drm, year)
        logging.debug("Calculating correction for iJet %s (Schema: %s)", iJet, schema)
        logging.debug("pt = %s; eta = %s, dRmin = %s --> Correction %s", pt, eta, drm, thisCorr)
        corr *= thisCorr

    if corr != 1.0:
        logging.debug("This Correction = %s", corr)

    return corr
        

def addTFLoose(fileName, skipEvents, treeName, outputBranchName, mediumWP, looseWP, year, isData, output = None, doValidation = False):
    t0 = time.time()

    inplace = False
    if output is None:
        inplace = True        
    
    if inplace:
        logging.warning("Updating file inplace")
        rFile = ROOT.TFile(fileName, "UPDATE")
        tree = rFile.Get(treeName)
    else:
        logging.warning("Will create new file")
        rFile = ROOT.TFile.Open(fileName)
        orig_tree = rFile.Get(treeName)
        # orig_tree = ROOT.TChain("tree")
        # for inFile in fileName.split(" "):
        #     logging.info("Adding %s to chain", inFile)
        #     orig_tree.Add(inFile)
        newReader = ROOT.TTreeReader(treeName, rFile)
        outFile = ROOT.TFile(output+".root", "RECREATE")
        tree = orig_tree.CloneTree()

    lenvar = "njets"
    brPrefix = "jets"

    TFLooseArray = array("f", [-1.0])
    branchTFLoose = tree.Branch(outputBranchName, TFLooseArray, outputBranchName+"/F")

    schema = "data" if isData else "mc"
    
    nEvents = tree.GetEntries()
    tdiff = time.time()
    logging.info("Will process %s events", nEvents)
    for iev in range(nEvents):
        if iev%10000 == 0:
            logging.info("Event {0:10d} | Total time: {1:8f} | Diff time {2:8f}".format(iev, time.time()-t0,time.time()-tdiff))
            tdiff = time.time()
        logging.debug("New Events - %s/%s", iev, nEvents-1)


        tree.GetEvent(iev)
        nJets =tree.__getattr__(lenvar)
        jets = []
        logging.debug("nBDeepFlavM = %s | bBDeepFlavL = %s | numJets = %s", tree.nBDeepFlavM, tree.nBDeepFlavL, nJets)
        for i in range(nJets):
            jets.append( Jet(
                tree.__getattr__(brPrefix+"_pt")[i],
                tree.__getattr__(brPrefix+"_mass")[i],
                tree.__getattr__(brPrefix+"_eta")[i],
                tree.__getattr__(brPrefix+"_phi")[i],
                tree.__getattr__(brPrefix+"_btagDeepFlav")[i],
                tree.__getattr__(brPrefix+"_btagFlag")[i],
                tree.__getattr__(brPrefix+"_dRmin")[i],
            ))


        if tree.nBDeepFlavL >= 4 and nJets >= 7:
            TFLooseArray[0] = float(calcTFLoose(jets, schema, looseWP, mediumWP, year, validation=doValidation))
            logging.debug("THis event : %s", TFLooseArray[0])
        else:
            TFLooseArray[0] = 1.0
        branchTFLoose.Fill()

    logging.info("Saving File")
    if inplace:
        rFile.cd()
        tree.Write("",ROOT.TFile.kOverwrite)
    else:
        tree.Write()
        
    logging.info("Total time to finish: {0:8f}".format(time.time()-t0))
    return True


if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for adding DNN from a lookup table to tree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--log",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outputFile",
        action = "store",
        help = "Input file",
        type = str,
        default = None
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "Events to skip",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--outBranchs",
        action = "store",
        help = "Name the new branch",
        type = str,
        default="DNNPred"
    )    
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--mediumWP",
        action = "store",
        help = "Medium working point for b-tagging - Defaultis 2017 DeepFlav",
        type = float,
        default = 0.3033,
    )
    argumentparser.add_argument(
        "--looseWP",
        action = "store",
        help = "Loose working point for b-tagging - Defaultis 2017 DeepFlav",
        type = float,
        default = 0.0521,
    )
    argumentparser.add_argument(
        "--year",
        action = "store",
        help = "Year for processing",
        default = "2017",
        type = str,
    )
    argumentparser.add_argument(
        "--sample",
        action = "store",
        help = "SampleName - Only checks if JetHT or BTagCSV",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--doValidation",
        action = "store_true",
        help = "If passed, the sideband validation correction will be calculated",
    )
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.log)

    logging.info("Medium WP %s", args.mediumWP)
    logging.info("Loose WP %s", args.looseWP)
    logging.info("Year %s", args.year)
    logging.info("Sample %s", args.sample)
    
    isData = False
    if args.sample == "JetHT" or args.sample == "BTagCSV":
        isData = True

    logging.info("isData %s", isData)
    
    addTFLoose(args.inputFile, args.skipevents, args.treeName, args.outBranchs, args.mediumWP, args.looseWP, args.year, isData, args.outputFile, args.doValidation)
