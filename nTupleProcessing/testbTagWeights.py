import ROOT
import os
import logging

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
    )

class BtagWeightAnalyzer(object):
    def __init__(self, fileName, onlyNominal = False, systs = None, systs_b = None, systs_c = None, systs_l = None, discType="deepcsv"):
        SFFile = fileName
        ROOT.gSystem.Load('libCondFormatsBTauObjects') 
        ROOT.gSystem.Load('libCondToolsBTau')

        if systs is None:
            self.bTagSyst = ["lf", "hf", "hfstats1", "hfstats2", "lfstats1", "lfstats2", "cferr1", "cferr2", "jesAbsoluteMPFBias", "jesAbsoluteScale", "jesAbsoluteStat", "jesFlavorQCD", "jesFragmentation", "jesPileUpDataMC", "jesPileUpPtBB", "jesPileUpPtEC1", "jesPileUpPtEC2", "jesPileUpPtHF", "jesPileUpPtRef", "jesRelativeBal", "jesRelativeFSR", "jesRelativeJEREC1", "jesRelativeJEREC2", "jesRelativeJERHF", "jesRelativePtBB", "jesRelativePtEC1", "jesRelativePtEC2", "jesRelativePtHF", "jesRelativeStatFSR", "jesRelativeStatEC", "jesRelativeStatHF", "jesSinglePionECAL", "jesSinglePionHCAL", "jesTimePtEta"]
        else:
            self.bTagSyst = systs
        logging.debug("B-tag systematics: %s",self.bTagSyst)
        if systs_b is None:
            self.bTagSyst_bFlav =  ["central", "up_lf", "down_lf", "up_hfstats1", "down_hfstats1", "up_hfstats2", "down_hfstats2"]
        else:
            self.bTagSyst_bFlav = systs_b
        logging.debug("B -- B-tag systematics: %s",self.bTagSyst_bFlav)
        if systs_c is None:
            self.bTagSyst_cFlav = ["central", "up_cferr1", "down_cferr1",  "up_cferr2", "down_cferr2"]
        else:
            self.bTagSyst_cFlav = systs_c
        logging.debug("C -- B-tag systematics: %s",self.bTagSyst_cFlav )
        if systs_l is None:
            self.bTagSyst_udsgFlav = ["central", "up_hf", "down_hf", "up_lfstats1", "down_lfstats1", "up_lfstats2", "down_lfstats2"]
        else:
            self.bTagSyst_udsgFlav = systs_l
        logging.debug("UDSG -- B-tag systematics: %s",self.bTagSyst_udsgFlav)

        
        self.v_sys = getattr(ROOT, 'vector<string>')()
        if not onlyNominal:
            for syst in self.bTagSyst:
                self.v_sys.push_back('up_'+syst)
                self.v_sys.push_back('down_'+syst)
        logging.info("Loading b-tag systemtics from %s (type %s)",SFFile,type(SFFile))
        self.discType = discType
        self.calib = ROOT.BTagCalibration(self.discType, SFFile)
        self.reader = ROOT.BTagCalibrationReader(3, "central", self.v_sys)

        self.reader.load(self.calib, 0, "iterativefit")# 0: BTagEntry::FLAV_B
        self.reader.load(self.calib, 1, "iterativefit")# 1: BTagEntry::FLAV_C
        self.reader.load(self.calib, 2, "iterativefit")# 2: BTagEntry::FLAV_UDSG

    def clacSF(self, jets):
        allCorr= {}
        centralValue = None
        for syst in ["central"] + self.bTagSyst:
            for sdir in ["up", "down"]:
                if syst == "central" and sdir == "up":
                    thisSyst = "central"
                elif syst == "central" and sdir == "down":
                    continue
                else:
                    thisSyst = sdir+"_"+syst

                varName = "btagSF_shape"
                if syst != "central":
                    varName = varName+"_"+syst+"_"+sdir

                varNameEv = "btagWeight_shape"
                if syst != "central":
                    varNameEv = varNameEv+"_"+syst+"_"+sdir

                evCorrection = 1.0
                logging.debug("==== %s ====", thisSyst)
                externalSystName = syst+"Up" if sdir == "up" else "Down"
                for iJet, jet in enumerate(jets):
                    jetSyst = thisSyst
                    if jet.hadronFlavour == 0:
                        if not (jetSyst in self.bTagSyst_udsgFlav or "jes" in jetSyst):
                            jetSyst = "central"
                        BTagEntry = 2
                    elif jet.hadronFlavour == 5:
                        if not (jetSyst in self.bTagSyst_bFlav or "jes" in jetSyst):
                            jetSyst = "central"
                        BTagEntry = 0
                    elif jet.hadronFlavour == 4:
                        if not (jetSyst in self.bTagSyst_cFlav):
                            jetSyst = "central"
                        BTagEntry = 1
                    else:
                        logging.error("Found a jet with invalid Flavour")
                        raise RuntimeError
                    logging.debug("Jet %s : flav %s (%s) | eta %s | pt %s | disc %s", iJet, jet.hadronFlavour,str(BTagEntry),abs(jet.eta),jet.getPt(externalSystName),jet.bTagValue)
                    if jet.passesCut(externalSystName):
                        thisJetCorr = self.reader.eval_auto_bounds(jetSyst, BTagEntry, abs(jet.eta), jet.getPt(externalSystName), jet.bTagValue)
                        logging.debug("Correction %s (%s)",thisJetCorr, jetSyst)
                        evCorrection *= thisJetCorr
                    else:
                        logging.debug("Jet %s did not pass selection for systematic %s", iJet, externalSystName)
                if thisSyst == "central":
                    centralValue = evCorrection
                allCorr[thisSyst] = evCorrection
                logging.debug("Set %s to %s (%s - ratio %s)",varNameEv, evCorrection, centralValue, evCorrection/centralValue)
        return allCorr

class Jet(object):
    def __init__(self, pt, eta, deepcsv, hadronFlavour):
        self.pt = pt
        self.eta = eta
        self.deepcsv = deepcsv
        self.hadronFlavour = hadronFlavour
    
def testbTagWeights(inputFile, bTagCSVFile, skipevents, treeName, lenvar, brPrefix, bDiscName, kinVariables, eventDiagnostics):
    logging.info("Loading: %s", inputFile)
    kinVarPt = kinVariables[0]
    kinVarEta = kinVariables[1]
    kinVarFlav = kinVariables[2]
    logging.info("Will use -- %s -- for PT, -- %s -- for ETA, -- %s -- for FALVOUR", kinVariables[0], kinVariables[1], kinVariables[2])
    logging.info("Will use -- %s -- as b-tag disciminator", bDiscName)
    rFile = ROOT.TFile.Open(inputFile)
    tree = rFile.Get(treeName)

    analyzer = BtagWeightAnalyzer(bTagCSVFile)
    
    nEvents = tree.GetEntries()
    logging.info("Tree has %s entries", nEvents)
    for iev in range(skipevents, nEvents):
        logging.info("New Events - %s/%s", iev, nEvents-1)
        tree.GetEvent(iev)
        diagnosticStr = ""
        for var in eventDiagnostics:
            diagnosticStr += "{0}: {1} |".format(var, tree.__getattr__(var))
        logging.info(diagnosticStr)
        nJets =tree.__getattr__(lenvar)
        jets = []
        for i in range(nJets):
            jets.append( Jet(
                tree.__getattr__(brPrefix+"_"+kinVariables[0])[i],
                tree.__getattr__(brPrefix+"_"+kinVariables[1])[i],
                tree.__getattr__(brPrefix+"_"+bDiscName)[i],
                tree.__getattr__(brPrefix+"_"+kinVariables[2])[i],
            ))
            
        analyzer.clacSF(jets)
        
        

    logging.info("Finished Event loop")
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Script for looking at the b-tag reweighting SF in a event loop over a TTree',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputFile",
        action = "store",
        help = "Input file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--SFFile",
        action = "store",
        help = "File used for the b-tag SF",
        type = str,
        default = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/MEAnalysis/data/deepCSV_sfs_v1.csv"
    )
    argumentparser.add_argument(
        "--skipevents",
        action = "store",
        help = "File used for the b-tag SF",
        type = int,
        default = 0
    )
    argumentparser.add_argument(
        "--treeName",
        action = "store",
        help = "Name of the TTree",
        type = str,
        default="tree"
    )
    argumentparser.add_argument(
        "--jetLenVar",
        action = "store",
        help = "Lenvar for the jets in the tree",
        type = str,
        default="njets"
    )
    argumentparser.add_argument(
        "--jetPrefix",
        action = "store",
        help = "Prefix of the jet branch (Expecting something like jets_pt, where prefix is before _)",
        type = str,
        default="jets"
    )
    argumentparser.add_argument(
        "--disc",
        action = "store",
        help = "Branch postfix of the b-tag discriminator",
        type = str,
        default="btagDeepCSV"
    )

    argumentparser.add_argument(
        "--kinVariables",
        action = "store",
        help = "Branch postfix of pt, eta, flav (in this order!)",
        type = str,
        nargs=3,
        default=["pt", "eta", "hadronFlavour"]
    )
    argumentparser.add_argument(
        "--diagnostics",
        action = "store",
        help = "Branch of that will be shown in for each event",
        type = str,
        nargs="+",
        default=["njets", "is_fh", "nBDeepCSVM"]
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging)
    testbTagWeights(args.inputFile, args.SFFile, args.skipevents, args.treeName, args.jetLenVar, args.jetPrefix, args.disc, args.kinVariables, args.diagnostics)
