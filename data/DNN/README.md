# DNN Data
Copy full model output folder from TF4ttHFH.

For Bookkeeping:
- network_report.txt
- usedConfig.cfg
- variableRanking.txt

For running the DNN inside the framework:
- network_attributes.json
- network_inputTransformation.json
- trainedModel.h5py
- trainedModel_weights.h5

Remove the rest:

```bash
rm *.pdf
rm evalMetrics.txt
rm testDataArrays.pkl
```


