# Configuration foles for the LegacyRun2 analysis 

## Preapproval (Updated: 03.12.19)

|                      | 2016 | 2017 | 2018 |
|----------------------|------|------|------|
| Dataset              | gc/datasets/LegacyRun2\_2016\_AH\_v3/ | gc/datasets/LegacyRun2\_2017\_AH_v3/ |      |
| nGen                 | data/nGen/nGen\_LegacyRun2\_AH\_v3\_2016.md |      |      |
| TF Loose             | data/TFLoose/getCorrection\_LegacyRun2\_2016.cfg |      |      |
| QCD Estimate (skims) |      |      |      |
| DNN                  |      |      |      |
| Datacards            |      |      |      |

