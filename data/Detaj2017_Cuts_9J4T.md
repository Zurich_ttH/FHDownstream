# ttH vs. QCD MC - 9J4T 

## Table Detaj[4] 

| cut | Signal Eff | Background Eff |
| --- | ---------- | -------------- |
| 0.0 | 0.0000 | 0.0000 |
| 0.075 | 0.0000 | 0.0000 |
| 0.15 | 0.0000 | 0.0000 |
| 0.225 | 0.0000 | 0.0000 |
| 0.3 | 0.0000 | 0.0000 |
| 0.375 | 0.0001 | 0.0000 |
| 0.45 | 0.0014 | 0.0001 |
| 0.525 | 0.0055 | 0.0001 |
| 0.6 | 0.0202 | 0.0094 |
| 0.675 | 0.0494 | 0.0103 |
| 0.75 | 0.0975 | 0.0291 |
| 0.825 | 0.1715 | 0.0334 |
| 0.9 | 0.2628 | 0.0524 |
| 0.975 | 0.3619 | 0.0665 |
| 1.05 | 0.4682 | 0.0897 |
| 1.125 | 0.5673 | 0.1876 |
| 1.2 | 0.6595 | 0.2633 |
| 1.275 | 0.7365 | 0.3358 |
| 1.35 | 0.8043 | 0.4272 |
| 1.425 | 0.8557 | 0.5399 |
| 1.5 | 0.8972 | 0.5887 |
| 1.575 | 0.9266 | 0.6725 |
| 1.65 | 0.9482 | 0.7828 |
| 1.725 | 0.9640 | 0.8039 |
| 1.8 | 0.9745 | 0.8829 |
| 1.875 | 0.9827 | 0.9258 |
| 1.95 | 0.9882 | 0.9292 |
| 2.025 | 0.9919 | 0.9413 |
| 2.1 | 0.9945 | 0.9580 |
| 2.175 | 0.9959 | 0.9702 |
| 2.25 | 0.9974 | 0.9704 |
| 2.325 | 0.9982 | 0.9770 |
| 2.4 | 0.9987 | 0.9770 |
| 2.475 | 0.9991 | 0.9770 |
| 2.55 | 0.9995 | 0.9954 |
| 2.625 | 0.9997 | 0.9954 |
| 2.7 | 0.9998 | 0.9954 |
| 2.775 | 0.9999 | 0.9954 |
| 2.85 | 1.0000 | 0.9954 |
| 2.925 | 1.0000 | 0.9954 |
| 3.0 | 1.0000 | 1.0000 |
| 3.075 | 1.0000 | 1.0000 |

## Table Detaj[5] 

| cut | Signal Eff | Background Eff |
| --- | ---------- | -------------- |
| 0.0 | 0.0000 | 0.0000 |
| 0.0875 | 0.0000 | 0.0000 |
| 0.175 | 0.0000 | 0.0000 |
| 0.2625 | 0.0000 | 0.0000 |
| 0.35 | 0.0000 | 0.0000 |
| 0.4375 | 0.0001 | 0.0000 |
| 0.525 | 0.0009 | 0.0001 |
| 0.6125 | 0.0041 | 0.0001 |
| 0.7 | 0.0148 | 0.0082 |
| 0.7875 | 0.0374 | 0.0133 |
| 0.875 | 0.0784 | 0.0216 |
| 0.9625 | 0.1404 | 0.0344 |
| 1.05 | 0.2191 | 0.0487 |
| 1.1375 | 0.3128 | 0.0574 |
| 1.225 | 0.4120 | 0.0757 |
| 1.3125 | 0.5092 | 0.1474 |
| 1.4 | 0.6018 | 0.1863 |
| 1.4875 | 0.6843 | 0.2768 |
| 1.575 | 0.7535 | 0.3251 |
| 1.6625 | 0.8115 | 0.3898 |
| 1.75 | 0.8568 | 0.4559 |
| 1.8375 | 0.8943 | 0.5089 |
| 1.925 | 0.9226 | 0.5809 |
| 2.0125 | 0.9441 | 0.6584 |
| 2.1 | 0.9597 | 0.7573 |
| 2.1875 | 0.9703 | 0.8610 |
| 2.275 | 0.9789 | 0.8696 |
| 2.3625 | 0.9848 | 0.8992 |
| 2.45 | 0.9890 | 0.9086 |
| 2.5375 | 0.9923 | 0.9156 |
| 2.625 | 0.9945 | 0.9296 |
| 2.7125 | 0.9962 | 0.9561 |
| 2.8 | 0.9977 | 0.9818 |
| 2.8875 | 0.9986 | 0.9952 |
| 2.975 | 0.9991 | 0.9952 |
| 3.0625 | 0.9994 | 0.9952 |
| 3.15 | 0.9997 | 1.0000 |
| 3.2375 | 0.9998 | 1.0000 |
| 3.325 | 0.9998 | 1.0000 |
| 3.4125 | 0.9999 | 1.0000 |
| 3.5 | 1.0000 | 1.0000 |
| 3.5875 | 1.0000 | 1.0000 |

## Table Detaj[6] 

| cut | Signal Eff | Background Eff |
| --- | ---------- | -------------- |
| 0.5 | 0.0000 | 0.0000 |
| 0.5875 | 0.0005 | 0.0001 |
| 0.675 | 0.0017 | 0.0001 |
| 0.7625 | 0.0066 | 0.0006 |
| 0.85 | 0.0167 | 0.0133 |
| 0.9375 | 0.0375 | 0.0136 |
| 1.025 | 0.0691 | 0.0208 |
| 1.1125 | 0.1149 | 0.0283 |
| 1.2 | 0.1735 | 0.0388 |
| 1.2875 | 0.2418 | 0.0468 |
| 1.375 | 0.3166 | 0.0552 |
| 1.4625 | 0.3980 | 0.0738 |
| 1.55 | 0.4764 | 0.1119 |
| 1.6375 | 0.5540 | 0.1515 |
| 1.725 | 0.6244 | 0.2075 |
| 1.8125 | 0.6882 | 0.2661 |
| 1.9 | 0.7454 | 0.3319 |
| 1.9875 | 0.7945 | 0.3659 |
| 2.075 | 0.8337 | 0.4075 |
| 2.1625 | 0.8690 | 0.5059 |
| 2.25 | 0.8983 | 0.5631 |
| 2.3375 | 0.9220 | 0.7055 |
| 2.425 | 0.9398 | 0.7270 |
| 2.5125 | 0.9549 | 0.8288 |
| 2.6 | 0.9657 | 0.8620 |
| 2.6875 | 0.9743 | 0.8647 |
| 2.775 | 0.9811 | 0.9164 |
| 2.8625 | 0.9864 | 0.9244 |
| 2.95 | 0.9895 | 0.9400 |
| 3.0375 | 0.9930 | 0.9492 |
| 3.125 | 0.9956 | 0.9737 |
| 3.2125 | 0.9974 | 0.9762 |
| 3.3 | 0.9983 | 0.9764 |
| 3.3875 | 0.9991 | 0.9787 |
| 3.475 | 0.9997 | 0.9787 |
| 3.5625 | 1.0001 | 1.0001 |
| 3.65 | 1.0003 | 1.0001 |
| 3.7375 | 1.0005 | 1.0001 |
| 3.825 | 1.0005 | 1.0001 |
| 3.9125 | 1.0005 | 1.0001 |
| 4.0 | 1.0005 | 1.0001 |
| 4.0875 | 1.0005 | 1.0001 |

## Table Detaj[7] 

| cut | Signal Eff | Background Eff |
| --- | ---------- | -------------- |
| 0.5 | 0.0000 | 0.0000 |
| 0.5875 | 0.0000 | 0.0000 |
| 0.675 | 0.0002 | 0.0000 |
| 0.7625 | 0.0008 | 0.0001 |
| 0.85 | 0.0027 | 0.0001 |
| 0.9375 | 0.0066 | 0.0016 |
| 1.025 | 0.0153 | 0.0061 |
| 1.1125 | 0.0294 | 0.0128 |
| 1.2 | 0.0502 | 0.0240 |
| 1.2875 | 0.0783 | 0.0265 |
| 1.375 | 0.1150 | 0.0333 |
| 1.4625 | 0.1593 | 0.0371 |
| 1.55 | 0.2095 | 0.0468 |
| 1.6375 | 0.2610 | 0.0503 |
| 1.725 | 0.3179 | 0.0585 |
| 1.8125 | 0.3780 | 0.0769 |
| 1.9 | 0.4351 | 0.1080 |
| 1.9875 | 0.4942 | 0.1388 |
| 2.075 | 0.5512 | 0.1583 |
| 2.1625 | 0.6056 | 0.2011 |
| 2.25 | 0.6551 | 0.2646 |
| 2.3375 | 0.7017 | 0.3402 |
| 2.425 | 0.7434 | 0.4019 |
| 2.5125 | 0.7830 | 0.4882 |
| 2.6 | 0.8183 | 0.5141 |
| 2.6875 | 0.8479 | 0.6055 |
| 2.775 | 0.8762 | 0.6442 |
| 2.8625 | 0.9000 | 0.6992 |
| 2.95 | 0.9209 | 0.7255 |
| 3.0375 | 0.9376 | 0.7717 |
| 3.125 | 0.9530 | 0.8019 |
| 3.2125 | 0.9648 | 0.8242 |
| 3.3 | 0.9742 | 0.8433 |
| 3.3875 | 0.9824 | 0.9439 |
| 3.475 | 0.9885 | 0.9511 |
| 3.5625 | 0.9923 | 0.9736 |
| 3.65 | 0.9953 | 0.9924 |
| 3.7375 | 0.9974 | 1.0000 |
| 3.825 | 0.9984 | 1.0000 |
| 3.9125 | 0.9990 | 1.0000 |
| 4.0 | 0.9996 | 1.0000 |
| 4.0875 | 1.0000 | 1.0000 |
