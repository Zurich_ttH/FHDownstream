# All Samples
| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 15407463.0 | 15466225.0 |
| ` QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 10887444.0 | 10955087.0 |
| ` QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 5397719.0 | 5459033.0 |
| ` QCD_HT200to300_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 54251448.0 | 54289440.0 |
| ` QCD_HT300to500_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 54599976.0 | 54661568.0 |
| ` QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 55054320.0 | 55152948.0 |
| ` QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 48037412.0 | 48158740.0 |
| ` ST_s-channel_4f_hadronicDecays_TuneCP5_13TeV-madgraph-pythia8.root ` | 6049577.5 | 9706000.0 |
| ` ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-madgraph-pythia8.root ` | 12453239.0 | 19965000.0 |
| ` ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8.root ` | 74203680.0 | 79090800.0 |
| ` ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8.root ` | 143035536.0 | 153210944.0 |
| ` ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8.root ` | 7584647.0 | 7623000.0 |
| ` ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8.root ` | 9549626.0 | 9598000.0 |
| ` THQ_ctcvcp_4f_Hincl_13TeV_madgraph_pythia8.root ` | 29521202.0 | 29683984.0 |
| ` THW_ctcvcp_5f_Hincl_13TeV_madgraph_pythia8.root ` | 14920682.0 | 14963988.0 |
| ` TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root ` | 63775924.0 | 64310000.0 |
| ` TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root ` | 124327280.0 | 125496000.0 |
| ` TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root ` | 100444536.0 | 101310000.0 |
| ` TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8.root ` | 456634.1875 | 833285.0 |
| ` TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8.root ` | 346389.8125 | 731000.0 |
| ` TTbb_4f_TTTo2l2nu_TuneCP5-Powheg-Openloops-Pythia8.root ` | 3149226.5 | 3574100.0 |
| ` TTbb_4f_TTToHadronic_TuneCP5-Powheg-Openloops-Pythia8.root ` | 5067327.0 | 5762900.0 |
| ` TTbb_4f_TTToSemiLeptonic_TuneCP5-Powheg-Openloops-Pythia8.root ` | 5350378.5 | 6075100.0 |
| ` WJetsToQQ_HT-800toInf_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 14535012.0 | 14601374.0 |
| ` WJetsToQQ_HT400to600_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 10028188.0 | 10049620.0 |
| ` WJetsToQQ_HT600to800_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 15253423.0 | 15298056.0 |
| ` WW_TuneCP5_PSweights_13TeV-pythia8.root ` | 7957480.5 | 7958000.0 |
| ` WZ_TuneCP5_PSweights_13TeV-pythia8.root ` | 3799370.0 | 3800000.0 |
| ` ZJetsToQQ_HT-800toInf_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 10515409.0 | 10561192.0 |
| ` ZJetsToQQ_HT400to600_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 16624282.0 | 16656544.0 |
| ` ZJetsToQQ_HT600to800_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 14603549.0 | 14642701.0 |
| ` ZZ_TuneCP5_13TeV-pythia8.root ` | 1883258.375 | 1883000.0 |
| ` ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8.root ` | 7353058.0 | 7525991.0 |
| ` ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root ` | 9261470.0 | 9490000.0 |
| ` TTTo2L2Nu_TuneCP5down_13TeV-powheg-pythia8.root ` | 4912450.0 | 4954000.0 |
| ` TTTo2L2Nu_TuneCP5up_13TeV-powheg-pythia8.root ` | 5399973.5 | 5446000.0 |
| ` TTTo2L2Nu_hdampDOWN_TuneCP5_13TeV-powheg-pythia8.root ` | 5366402.0 | 5458000.0 |
| ` TTTo2L2Nu_hdampUP_TuneCP5_13TeV-powheg-pythia8.root ` | 5234081.0 | 5260000.0 |
| ` TTToHadronic_TuneCP5down_13TeV-powheg-pythia8.root ` | 26419534.0 | 26675000.0 |
| ` TTToHadronic_TuneCP5up_13TeV-powheg-pythia8.root ` | 23262516.0 | 23480000.0 |
| ` TTToHadronic_hdampDOWN_TuneCP5_13TeV-powheg-pythia8.root ` | 25933676.0 | 26415000.0 |
| ` TTToHadronic_hdampUP_TuneCP5_13TeV-powheg-pythia8.root ` | 24799240.0 | 24955000.0 |
| ` TTToSemiLeptonic_TuneCP5down_13TeV-powheg-pythia8.root ` | 20307094.0 | 20483000.0 |
| ` TTToSemiLeptonic_TuneCP5up_13TeV-powheg-pythia8.root ` | 26717514.0 | 26948000.0 |
| ` TTToSemiLeptonic_hdampDOWN_TuneCP5_13TeV-powheg-pythia8.root ` | 25464554.0 | 25904000.0 |
| ` TTToSemiLeptonic_hdampUP_TuneCP5_13TeV-powheg-pythia8.root ` | 26771884.0 | 26908000.0 |
| ` TTbb_4f_TTTo2l2nu_hdampDOWN_TuneCP5-Powheg-Openloops-Pythia8.root ` | 1693282.625 | 2017500.0 |
| ` TTbb_4f_TTTo2l2nu_hdampUP_TuneCP5-Powheg-Openloops-Pythia8.root ` | 2107526.25 | 2374000.0 |
| ` TTbb_4f_TTToHadronic_hdampDOWN_TuneCP5-Powheg-Openloops-Pythia8.root ` | 1931085.625 | 2307693.0 |
| ` TTbb_4f_TTToHadronic_hdampUP_TuneCP5-Powheg-Openloops-Pythia8.root ` | 1878279.625 | 2123600.0 |
| ` TTbb_4f_TTToSemiLeptonic_hdampDOWN_TuneCP5-Powheg-Openloops-Pythia8.root ` | 1601521.25 | 1910400.0 |
| ` TTbb_4f_TTToSemiLeptonic_hdampUP_TuneCP5-Powheg-Openloops-Pythia8.root ` | 1826291.125 | 2061000.0 |
| ` ttH_4f_ctcvcp_TuneCP5_13TeV_madgraph_pythia8.root ` | 28973158.0 | 29069000.0 |



# Cleaned for XTCVCP samples
| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` THW_ctcvcp_HIncl_M125_TuneCP5_13TeV-madgraph-pythia8.root ` | 14875796.0 | 14918988.0 |
