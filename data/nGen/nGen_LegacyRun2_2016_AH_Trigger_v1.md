| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 67107308.0 | 67652200.0 |
| ` TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 65147400.0 | 65679200.0 |
| ` TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 105854800.0 | 106716400.0 |
