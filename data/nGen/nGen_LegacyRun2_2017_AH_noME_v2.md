| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` ST_s-channel_4f_hadronicDecays_TuneCP5_13TeV-amcatnlo-pythia8.root ` | 6017510.0 | 9652000.0 |
| ` ST_s-channel_4f_leptonDecays_TuneCP5_PSweights_13TeV-amcatnlo-pythia8.root ` | 6173214.5 | 9897334.0 |
| ` ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 59301452.0 | 59345480.0 |
| ` ST_t-channel_top_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 122395272.0 | 122451448.0 |
| ` ST_tW_antitop_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 7144673.5 | 7199860.0 |
| ` ST_tW_top_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 7884144.5 | 7945242.0 |
| ` TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 64586736.0 | 65123488.0 |
| ` TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 128541048.0 | 129590304.0 |
| ` TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 105361712.0 | 106229168.0 |
| ` ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8.root ` | 4491749.5 | 4580223.0 |
| ` ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root ` | 7264144.0 | 7418236.0 |
