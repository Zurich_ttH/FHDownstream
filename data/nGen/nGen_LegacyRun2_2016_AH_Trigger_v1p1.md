| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 67145424.0 | 67691000.0 |
| ` TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 67251464.0 | 67801744.0 |
| ` TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 104807376.0 | 105658512.0 |
