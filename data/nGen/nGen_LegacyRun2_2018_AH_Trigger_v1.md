| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` SingleMuon.root ` | 0.0 | 916531328.0 |
| ` TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root ` | 118557552.0 | 119540000.0 |
| ` TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root ` | 112763456.0 | 113715200.0 |
| ` TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root ` | 96052152.0 | 96850000.0 |
