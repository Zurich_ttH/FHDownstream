| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 15343056.0 | 15401823.0 |
| ` QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 10800316.0 | 10867396.0 |
| ` QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 5233958.0 | 5293568.0 |
| ` QCD_HT200to300_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 54050588.0 | 54088080.0 |
| ` QCD_HT300to500_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 54554128.0 | 54615992.0 |
| ` QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 54917960.0 | 55016252.0 |
| ` QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8.root ` | 47876744.0 | 47997888.0 |
| ` TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root ` | 62553428.0 | 63065000.0 |
| ` TTToHadronic_TuneCP5_13TeV-powheg-pythia8.root ` | 129702336.0 | 130768000.0 |
| ` TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root ` | 98275640.0 | 99085000.0 |
| ` ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root ` | 11589902.0 | 11835999.0 |
