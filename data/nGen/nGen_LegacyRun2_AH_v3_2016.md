| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 4843592.0 | 4843609.0 |
| ` QCD_HT1500to2000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 3970835.5 | 3970819.0 |
| ` QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 1991172.875 | 1991645.0 |
| ` QCD_HT200to300_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 18117488.0 | 18117472.0 |
| ` QCD_HT300to500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 16941994.0 | 16941978.0 |
| ` QCD_HT500to700_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 18560666.0 | 18560542.0 |
| ` QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 15475317.0 | 15475247.0 |
| ` ST_s-channel_4f_hadronicDecays_TuneCP5_PSweights_13TeV-amcatnlo-pythia8.root ` | 3042982.0 | 4876400.0 |
| ` ST_s-channel_4f_leptonDecays_TuneCP5_PSweights_13TeV-amcatnlo-pythia8.root ` | 6094470.5 | 9772699.0 |
| ` ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 17780612.0 | 17780700.0 |
| ` ST_t-channel_top_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 31757844.0 | 31758000.0 |
| ` ST_tW_antitop_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 4900525.0 | 4938400.0 |
| ` ST_tW_top_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 4883443.5 | 4920600.0 |
| ` TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 66821604.0 | 67366800.0 |
| ` TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 66158720.0 | 66698800.0 |
| ` TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 106707312.0 | 107575792.0 |
| ` TTWJetsToQQ_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8.root ` | 424727.84375 | 822073.0 |
| ` TTZToQQ_TuneCUETP8M1_13TeV-amcatnlo-pythia8.root ` | 307279.5625 | 656116.0 |
| ` WJetsToQQ_HT-600ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root ` | 1026582.75 | 1026587.0 |
| ` WJetsToQQ_HT180_13TeV-madgraphMLM-pythia8.root ` | 18187006.0 | 18187336.0 |
| ` WW_TuneCUETP8M1_13TeV-pythia8.root ` | 7916793.5 | 7916164.0 |
| ` WZ_TuneCUETP8M1_13TeV-pythia8.root ` | 3959727.0 | 3959737.0 |
| ` ZJetsToQQ_HT600toInf_13TeV-madgraph.root ` | 944147.1875 | 944150.0 |
| ` ZZ_TuneCUETP8M1_13TeV-pythia8.root ` | 1988321.0 | 1988098.0 |
| ` ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8.root ` | 9705913.0 | 9913577.0 |
| ` ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root ` | 9492406.0 | 9694000.0 |
