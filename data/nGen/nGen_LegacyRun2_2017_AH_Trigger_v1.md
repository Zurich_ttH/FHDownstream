| Dataset | nGen (weighted) | nGen |
|---------|-----------------|------|
| ` TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 66988472.0 | 67536704.0 |
| ` TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 128413808.0 | 129458696.0 |
| ` TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root ` | 108435776.0 | 109318352.0 |
